---
cascade:
  title: Dummy Title
  lastmod: "2015-01-01"
---
Automatiskt inhämtad dokumentation från Arbetsförmedlingens [källkodsrepon på Gitlab](https://gitlab.com/arbetsformedlingen).
