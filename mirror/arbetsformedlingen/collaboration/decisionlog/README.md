---
title: Decisionlog
gitlaburl: https://gitlab.com/arbetsformedlingen/collaboration/decisionlog/-/blob/main//README.md
gitdir: /arbetsformedlingen/collaboration/decisionlog
gitdir-file-path: /README.md
date: '2023-10-12 16:26:13'
path: /arbetsformedlingen/collaboration/decisionlog/README.md
tags:
- README.md
---
# Decisionlog

This repository is used by the development team to document all their decisions in a markdown file and check it into this repository. Each decision should contain the following headers: "Title", "Context", "Decision", "Status", and "Consequences".

## Log

1. [Start logging](2023-03-28-start-logging.md)
2. [Automatic Vulnerability Detection for GitLab Repositories](2023-03-29-Automatic-Vulnerability-Detection-for-GitLab-Repositories.md)
3. [Publish Daily Operations Metrics](2023-03-29-Publish-Daily-Operations-Metrics.md)
4. [Allowing Anonymous Access to Open Data](2023-03-30-Allowing-Anonymous-Access-to-Open-Data.md)
5. [Preventing Sensitive Information from Being Committed to Source Control](2023-03-30-Preventing-Sensitive-Information-from-Being-Committed-to-Source-Control.md)
6. [Ensuring Reproducibility of Published Software](2023-03-31-Ensuring-Reproducibility-of-Published-Software.md)
7. [Enhancing Security and Standardizing User Interface Development with Static Web Applications](2023-08-09-Static-Webapps.md)
8. [Using External Secrets Operator with Bitwarden Backend for Secret Management and Deploys](2023-10-12-Secret-management-for-deploys.md)
9. [Password Management Tool](2023-10-12-Password-Management-Tool.md)

## How to document a decision
When documenting a decision, please follow the format below:

### Title
The title should be brief and descriptive of the decision made.

### Context
The context should explain the problem or situation that led to the decision. This section should be detailed enough for someone new to the project to understand the problem being solved.

### Decision
The decision section should clearly explain what decision was made and why. This should include any options that were considered and why they were not chosen. It's important to include enough information for others to understand the reasoning behind the decision.

### Status
The status section should explain the current state of the decision. This should include any implementation details and timelines.

### Consequences
The consequences section should list any potential positive or negative consequences of the decision. This should include any risks or tradeoffs that were considered.

## Contributing
To contribute to the decision log, please fork this repository and create a new branch for your changes. Once your changes are complete, create a pull request back to the main branch. All contributions are welcome!

## License
This repository is licensed under the MIT license. See the LICENSE file for details.
