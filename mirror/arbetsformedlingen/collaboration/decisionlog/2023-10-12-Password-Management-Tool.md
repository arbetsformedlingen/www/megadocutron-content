---
title: Title
gitlaburl: https://gitlab.com/arbetsformedlingen/collaboration/decisionlog/-/blob/main//2023-10-12-Password-Management-Tool.md
gitdir: /arbetsformedlingen/collaboration/decisionlog
gitdir-file-path: /2023-10-12-Password-Management-Tool.md
date: '2023-10-12 16:26:13'
path: /arbetsformedlingen/collaboration/decisionlog/2023-10-12-Password-Management-Tool.md
tags:
- 2023-10-12-Password-Management-Tool.md
---
# Title
Password Management Tool

## Context
Our organization handles a large number of login credentials, passwords, and sensitive information on a daily basis. Ensuring the security and efficiency of managing these credentials is essential. Several options for password management tools have been considered as potential solutions for this need. Among these options are Bitwarden, LastPass, 1Password, and Keeper Security. Overall, the open-source alternative has been preferred without evaluating the other alternatives. In this context, open source means that the organization gains the most control over how the data in the tool can be managed.

**Our primary evaluation criteria:**

1. Ability to export data from the tool
2. Flexibility in determine where data is stored
3. The tool should be open source
4. Share secrets between developers.

## Decision
Adopt Bitwarden as our official tool for password management and secure storage of secrets.

## Status
Take effect 2023-11-01. Request for comments.

## Consequences
The decision to adopt Bitwarden as our official password management tool has several important consequences.

### Effect

1. **Increased Security:** By using Bitwarden and other reliable password management tools, we enhance the security of our passwords and sensitive data through strong encryption and secure storage.
2. **User Choice:** With the option to use multiple password management tools, we provide users with choice and flexibility to use the tool they prefer.
3. **Security Awareness:** Training and security guidelines will raise awareness of password security among our users, reducing the risks of insecure management.
4. **Increased cost of operations:** Using proven, open-source password management tools reduces the risks of costly security incidents. However, it requires the organization to host the software.

### Work

1. **Implementation of Bitwarden:** We will implement Bitwarden as the recommended and approved tool for securely storing and managing passwords and sensitive information within the organization.
2. **Alternative Password Managers:** To accommodate individual preferences, we will allow users to continue using other password management tools, especially the option offered by the authority. This provides users with flexibility to choose the tool they prefer, while the organization promotes the use of Bitwarden.
3. **Training and User Support:** We will provide training and support (list of links from the community) to all users to ensure they can effectively and securely use their chosen password management tool.
4. **Security Guidelines:** Decisions regarding password management tools include recommendations for password complexity, two-factor authentication, and other security features.
