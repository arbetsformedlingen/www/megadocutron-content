---
title: Title
gitlaburl: https://gitlab.com/arbetsformedlingen/collaboration/decisionlog/-/blob/main//2023-03-28-start-logging.md
gitdir: /arbetsformedlingen/collaboration/decisionlog
gitdir-file-path: /2023-03-28-start-logging.md
date: '2023-10-12 16:26:13'
path: /arbetsformedlingen/collaboration/decisionlog/2023-03-28-start-logging.md
tags:
- 2023-03-28-start-logging.md
---
## Title
Decisions logged to files in a repo.

## Context
The lack of traceability is a problem in any organisation including
JobTech Dev. With a logging system things may improve, so we have
decided to start. This file consitutes the founding decision and start
of the initiative.

## Decision
We, Jonas Södergren, Joakim Verona, Oleg Lyalin and Per Weijnitz, will
start to log decisions in this repo.

## Status
agreed

## Consequences
 + Traceability and accountability may improve
 - More red tape work
