---
title: Title
gitlaburl: https://gitlab.com/arbetsformedlingen/collaboration/decisionlog/-/blob/main//2023-08-09-Static-Webapps.md
gitdir: /arbetsformedlingen/collaboration/decisionlog
gitdir-file-path: /2023-08-09-Static-Webapps.md
date: '2023-10-12 16:26:13'
path: /arbetsformedlingen/collaboration/decisionlog/2023-08-09-Static-Webapps.md
tags:
- 2023-08-09-Static-Webapps.md
---
### Title
Enhancing Security and Standardizing User Interface Development with Static Web Applications

### Context
Our organization develops some user interfaces to interact with our software and services. These user interfaces are built using different technologies and frameworks, that´s OK, but it leads to some inconsistencies and potential security vulnerabilities we need to address. We want to simplify our maintenance by introducing a consistent approach for building and distributing user interfaces.

### Decision
We will implement a policy that mandates the development of all user interfaces as static web applications. This decision places a strong emphasis on standardizing the distribution of our applications. By utilizing HTML, CSS, and JavaScript to create static web applications, we ensure a consistent method of delivery across all our software offerings.

This standardized distribution approach will simplify maintenance, updates, and deployment processes, enabling us to efficiently manage user interfaces across our software ecosystem.

Transitioning to static web applications offers the advantage of built-in security benefits. As these applications are executed in a user's browser rather than relying heavily on server-side processing, the potential attack surface diminishes, reducing the risk of various security vulnerabilities.

### Status
The implementation of the policy is currently underway. We are collaborating with development teams to ensure they are well-versed in the technologies and practices required for building static web applications. Concurrently, we are evaluating various tools and frameworks that support the secure development and distribution of these applications.

Not formal decided yet. **Request for comments from teams.**

### Consequences
The transition to developing static web applications for user interfaces will yield several significant consequences for our organization. The foremost advantage lies in the standardization of application distribution. All software and services will be accessed through a consistent delivery method and a streamlined maintenance.

One prominent advantages is the enhancement of security. By reducing the reliance on server-side processing, we decrease the exposure to potential security threats, making our applications more resilient against attacks.

Static web applications inherently offer faster loading times and improved performance, further enhancing the user experience.

Teams doesn´t need to build containers and handle operations and procedures for deploys. If a team builds a catalog with static resources the devops team manage an platform with ready to go webservers.

However, it's important to note that while the standardized distribution benefits are substantial, certain dynamic features that rely heavily on server-side processing may require adaptation or reconfiguration.

In conclusion, our decision to shift to static web applications for user interfaces not only contributes to a simplified maintainence but also bolsters our security posture. By adhering to standardized practices and minimizing the attack surface, we ensure that our software remains secure and reliable for our users.

### Definition of static webapp

The request cycle of a static web application. From https://www.staticapps.org/.

1. The user agent (browser) sends a request to a static web server at a specified address.
2. The server receives the network request and maps the address to a "barebones" HTML file stored on the server.
3. The server sends the HTML file to the user agent, which will then render it and display it to the user.
4. The JavaScript in the HTML page uses the browser to connect to data services and fetch the information it needs to construct the content of the page.
5. The JavaScript in the page takes the data and manipulates the page's HTML, updating it with the data fetched above.

The primary difference here is that in a traditional web app, the server is responsible for fetching data and compiling it into HTML that the user can see, while in a static web app the browser is responsible for doing so.
