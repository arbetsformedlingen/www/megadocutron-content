---
title: Title
gitlaburl: https://gitlab.com/arbetsformedlingen/collaboration/decisionlog/-/blob/main//2023-03-31-Ensuring-Reproducibility-of-Published-Software.md
gitdir: /arbetsformedlingen/collaboration/decisionlog
gitdir-file-path: /2023-03-31-Ensuring-Reproducibility-of-Published-Software.md
date: '2023-10-12 16:26:13'
path: /arbetsformedlingen/collaboration/decisionlog/2023-03-31-Ensuring-Reproducibility-of-Published-Software.md
tags:
- 2023-03-31-Ensuring-Reproducibility-of-Published-Software.md
---
## Title
Ensuring Reproducibility of Published Software

## Context
Our organization publishes software on Gitlab for both internal and external use. We want to ensure that all software published on Gitlab is fully reproducible, meaning that it can be built and deployed in a consistent and predictable manner. Reference: https://en.wikipedia.org/wiki/Reproducible_builds

## Decision
Proposal. We will implement a policy that requires all software published on Gitlab to be fully reproducible. This means that all software must be built and deployed in a consistent and predictable manner, and that this process can be reproduced by others. To achieve this, we will place a particular emphasis on managing dependencies, especially third-party libraries.

In addition to managing dependencies, we will also ensure that all software published on Gitlab has clear build instructions and documentation. This will help to ensure that the software can be easily reproduced by others, and that any issues or discrepancies can be quickly identified and resolved.

In particular, we recognize that dynamically typed languages like Python can be especially challenging to make reproducible due to their dependency management systems. Therefore, we will take extra care to ensure that all dependencies used in Python projects are fully specified and versioned, and that we use tools like virtual environments to manage dependencies and prevent conflicts.

## Status
The implementation of the policy is currently in progress. We are reviewing our existing software on Gitlab to ensure that it is fully reproducible, and we are updating it as necessary. We are also working with our development teams to ensure that any new software that is published on Gitlab is designed to be fully reproducible and includes clear build instructions and documentation.

## Consequences
Ensuring reproducibility of software published on Gitlab will help to promote transparency and consistency in our development process. It will also help to ensure that our software is reliable and can be trusted by our users. By including clear build instructions and documentation, we can also make it easier for others to reproduce our software and to identify and resolve any issues that may arise. However, achieving reproducibility may require additional effort and attention to detail, especially when managing dependencies. We believe that this is a worthwhile investment in the quality and trustworthiness of our software, and we will work to ensure that all software published on Gitlab is fully reproducible and well-documented.
