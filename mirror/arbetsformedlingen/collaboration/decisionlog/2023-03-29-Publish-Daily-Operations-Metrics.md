---
title: Title
gitlaburl: https://gitlab.com/arbetsformedlingen/collaboration/decisionlog/-/blob/main//2023-03-29-Publish-Daily-Operations-Metrics.md
gitdir: /arbetsformedlingen/collaboration/decisionlog
gitdir-file-path: /2023-03-29-Publish-Daily-Operations-Metrics.md
date: '2023-10-12 16:26:13'
path: /arbetsformedlingen/collaboration/decisionlog/2023-03-29-Publish-Daily-Operations-Metrics.md
tags:
- 2023-03-29-Publish-Daily-Operations-Metrics.md
---
## Title
Publish Daily Operations Metrics

## Context
Our organization is committed to transparency and sharing information with the public. We track several key performance indicators (KPIs) to monitor our operations and ensure that we are meeting our goals. We want to make this information available to the public in a way that is easily accessible and usable.

## Decision
We will share our daily operations KPIs as open data (report). The report will be available for download here https://data.arbetsformedlingen.se/jobtechdev-daily-report.

## Status
Decided. We are working with our IT team to ensure that the data is available for every product.

## Consequences
Sharing our KPIs as open data will provide the public with greater visibility into our operations and demonstrate our commitment to transparency. It will also provide researchers, journalists, and other stakeholders with valuable data that can be used for analysis and research. However, there may be some risks associated with sharing this information, such as exposing ip-numbers or other information revealing internal structure. To mitigate these risks, we will carefully review the report before adding new information. Overall, we believe that sharing our KPIs as open data is a positive step for our organization and will help us to build trust with the public.
