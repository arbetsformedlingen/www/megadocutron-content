---
title: Minnesanteckningar
gitlaburl: https://gitlab.com/arbetsformedlingen/collaboration/minnesanteckningar/-/blob/main//README.md
gitdir: /arbetsformedlingen/collaboration/minnesanteckningar
gitdir-file-path: /README.md
date: '2022-01-24 11:10:47'
path: /arbetsformedlingen/collaboration/minnesanteckningar/README.md
tags:
- README.md
---
# Minnesanteckningar

Blandade anteckningar från presentationer vi har hört. 

