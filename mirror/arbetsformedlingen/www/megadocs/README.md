---
title: Megadocs
gitlaburl: https://gitlab.com/arbetsformedlingen/www/megadocs/-/blob/main//README.md
gitdir: /arbetsformedlingen/www/megadocs
gitdir-file-path: /README.md
date: '2023-11-11 20:28:23'
path: /arbetsformedlingen/www/megadocs/README.md
tags:
- README.md
---
# Megadocs

Ett [gohugo tema](https://themes.gohugo.io/) för minimalistisk dokumentation. Version 0.1.

## Livedemo

[Sajt med demoinnehåll](https://megadocs-livedemo-arbetsformedlingen-www-fb6574a93b17ba75180dcf.gitlab.io/)

## TL;DR

1. Inget extra. Stödjer CommonMarkdown.
2. Minimalt med CSS (1.5KB)
3. Några få rader javscript (under 1KB). Fungerar utan.
4. Alla bilder laddas ner och komprimeras.

## Use-case

Stödja olika redaktionella arbetsflöden genom att tillåta olika datakällor. Gohugos katalog **content** kan innehålla

1. **WIKI** - Göra om en wiki till en hemsida. Redaktörer kan arbete direkt i en WIKI med markdown.
2. **GIT** - Utvecklare publicerar gärna README för sina källkodsrepon nära källkoden. (kräver ett insamlingssteg som ej är inkluderat i mallen)
3. **Basic CMS** - Publicera innehåll direkt till sajten.

## Getting started

[Installera gohugo](https://gohugo.io/installation/).

### 1. Vill använda temat för en ny sajt

#### Skapa en ny sajt
hugo new site blog-ex

#### Lägg till temat megadocs
git submodule add https://gitlab.com/arbetsformedlingen/www/megadocs themes/megadocs

#### Konfigurera vilket tema sajten använder
echo "theme = 'megadocs'" >> hugo.toml

## LICENS
MIT
