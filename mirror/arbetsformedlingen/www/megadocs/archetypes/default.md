---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/www/megadocs/-/blob/main//archetypes/default.md
gitdir: /arbetsformedlingen/www/megadocs
gitdir-file-path: /archetypes/default.md
date: '2023-11-11 20:28:23'
path: /arbetsformedlingen/www/megadocs/archetypes/default.md
tags:
- archetypes::default.md
- default.md
---
+++
title = '{{ replace .File.ContentBaseName "-" " " | title }}'
date = {{ .Date }}
draft = false
+++
