---
title: newsletter
custom:
  short: Prenumera på våra utskick och ta del av allt vi har att erbjuda.
  content: |+
    Du kommer att få ta del av inbjudningar, nyheter, inspiration och tips via e-post.
    Du kan när som helst avregistrera dig.

routes:
  default: /newsletter
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/11.info/newsletter/info.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/11.info/newsletter/info.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/11.info/newsletter/info.sv.md
tags:
- user::pages::11.info::newsletter::info.sv.md
- pages::11.info::newsletter::info.sv.md
- 11.info::newsletter::info.sv.md
- newsletter::info.sv.md
- info.sv.md
---
---
title: newsletter
custom:
    short: 'Prenumera på våra utskick och ta del av allt vi har att erbjuda.'
    content: "Du kommer att få ta del av inbjudningar, nyheter, inspiration och tips via e-post.\nDu kan när som helst avregistrera dig.\n\n"
routes:
    default: /newsletter
---

