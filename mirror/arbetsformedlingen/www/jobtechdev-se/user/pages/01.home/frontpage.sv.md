---
simplesearch:
  process: false
title: Hem
body_classes: title-center title-h1h2
custom:
  topButtonLink: /about
  topcomponent: /info/newsletter
  news:
  - page: /news/tre-pilotprojekt
  - page: /news/sa-foerenklar-allaloener-se-loenefragan-genom-transparens
  - page: /news/ny-teknik-stoettar-det-livslanga-laerandet
  events:
  - page: /news/webbdagarna-stockholm-18-19-april
  - page: /news/digitalisering-and-transformation-i-offentlig-sektor-2023
  - page: /news/inrupt-solid-hackathon
  backgroundimage: 2annie-spratt-gq5PECP8pHE-unsplash kopiera.jpg
  topcomponentimage: atlas.jpg
  header: |-
    En bättre
    arbetsmarknad
    genom samarbete
    och delad data
  text: JobTech Development finns till för dig som utvecklar digitala tjänster, vill hitta nya spännande samarbeten eller få djupare insikter om arbetsmarknaden. På plattformen hittar du API:er, dataset, standarder och öppen källkod, allt är fritt tillgängligt för vem som helst att använda. Här kan du också dela med dig av data och källkod som andra kan ha nytta av.
  components:
  - page: /components/jobsearch
  - page: /components/ekosystem_foer_annonser
  - page: /components/jobstream
  - page: /components/historical-ads
  first: sdfhjdhjghjg
  second: |-
    gjjgjkbgjbgjkdbgadf
    hdfhfkshaf
    gkhjakd
  third: jjjj
  title_first: TRR
  title: LLL
  title_second: arbetsförmedlingen
  title_third: UHR
metadata:
  description: öppna data
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/01.home/frontpage.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/01.home/frontpage.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/01.home/frontpage.sv.md
tags:
- user::pages::01.home::frontpage.sv.md
- pages::01.home::frontpage.sv.md
- 01.home::frontpage.sv.md
- frontpage.sv.md
---
---
simplesearch:
    process: false
title: Hem
body_classes: 'title-center title-h1h2'
custom:
    topButtonLink: /about
    topcomponent: /info/newsletter
    news:
        -
            page: /news/tre-pilotprojekt
        -
            page: /news/sa-foerenklar-allaloener-se-loenefragan-genom-transparens
        -
            page: /news/ny-teknik-stoettar-det-livslanga-laerandet
    events:
        -
            page: /news/webbdagarna-stockholm-18-19-april
        -
            page: /news/digitalisering-and-transformation-i-offentlig-sektor-2023
        -
            page: /news/inrupt-solid-hackathon
    backgroundimage: '2annie-spratt-gq5PECP8pHE-unsplash kopiera.jpg'
    topcomponentimage: atlas.jpg
    header: "En bättre\narbetsmarknad\ngenom samarbete\noch delad data"
    text: 'JobTech Development finns till för dig som utvecklar digitala tjänster, vill hitta nya spännande samarbeten eller få djupare insikter om arbetsmarknaden. På plattformen hittar du API:er, dataset, standarder och öppen källkod, allt är fritt tillgängligt för vem som helst att använda. Här kan du också dela med dig av data och källkod som andra kan ha nytta av.'
    components:
        -
            page: /components/jobsearch
        -
            page: /components/ekosystem_foer_annonser
        -
            page: /components/jobstream
        -
            page: /components/historical-ads
    first: sdfhjdhjghjg
    second: "gjjgjkbgjbgjkdbgadf\nhdfhfkshaf\ngkhjakd"
    third: jjjj
    title_first: TRR
    title: LLL
    title_second: arbetsförmedlingen
    title_third: UHR
metadata:
    description: 'öppna data'
---

# Say Hello to Grav!
## installation successful...

Congratulations! You have installed the **Base Grav Package** that provides a **simple page** and the default **Quark** theme to get you started.

!! If you see a **404 Error** when you click `Typography` in the menu, please refer to the [troubleshooting guide](http://learn.getgrav.org/troubleshooting/page-not-found).

### Find out all about Grav

* Learn about **Grav** by checking out our dedicated [Learn Grav](http://learn.getgrav.org) site.
* Download **plugins**, **themes**, as well as other Grav **skeleton** packages from the [Grav Downloads](http://getgrav.org/downloads) page.
* Check out our [Grav Development Blog](http://getgrav.org/blog) to find out the latest goings on in the Grav-verse.

!!! If you want a more **full-featured** base install, you should check out [**Skeleton** packages available in the downloads](http://getgrav.org/downloads).

### Edit this Page

To edit this page, simply navigate to the folder you installed **Grav** into, and then browse to the `user/pages/01.home` folder and open the `default.md` file in your [editor of choice](http://learn.getgrav.org/basics/requirements).  You will see the content of this page in [Markdown format](http://learn.getgrav.org/content/markdown).

### Create a New Page

Creating a new page is a simple affair in **Grav**.  Simply follow these simple steps:

1. Navigate to your pages folder: `user/pages/` and create a new folder.  In this example, we will use [explicit default ordering](http://learn.getgrav.org/content/content-pages) and call the folder `03.mypage`.
2. Launch your text editor and paste in the following sample code:

        ---
        title: My New Page
        ---
        # My New Page!

        This is the body of **my new page** and I can easily use _Markdown_ syntax here.

3. Save this file in the `user/pages/03.mypage/` folder as `default.md`. This will tell **Grav** to render the page using the **default** template.
4. That is it! Reload your browser to see your new page in the menu.

! NOTE: The page will automatically show up in the Menu after the "Typography" menu item. If you wish to change the name that shows up in the Menu, simple add: `menu: My Page` between the dashes in the page content. This is called the YAML front matter, and it is where you configure page-specific options.
