---
title: Yrkesprognoser
custom:
  title: Yrkesprognoser
  description: Prognoser för rekryteringsbehov och konkurrenssituation inom olika yrken.
  block_1: "Yrkesprognoser erbjuder data avseende bedömt arbetsmarknadsläge för yrken i termer av konkurrens och möjligheter till jobb om ett och fem år i riket. Prognoserna bygger på en modell med registerdata från SCB på Standard för svensk yrkesklassificering (SSYK2012), sysselsättningsstatus, utbildning och ålder samt Arbetsförmedlingens verksamhetsstatistik på platsannonser. Prognosen justeras med hjälp av SCB:s befolkningsprognoser samt konjunkturbedömningar för näringsgrenarna. Prognosen redovisas för ca 170 prognosyrken, där ett prognosyrke består av en eller flera SSYK-yrkesgrupper.\n\n \n## Vem kan ha nytta av produkten?\nDatasetet är användbart för alla företag och organisationer som erbjuder tjänster inom yrkes- och studievägledning och som vill hjälpa sina målgrupper att fatta välgrundade beslut inför yrkes- och studieval. Aktörer som vill ha fördjupade insikter om utvecklingen på arbetsmarknaden har också nytta av datat.\n\n\n"
  menu:
  - title: Dataset
    url: https://data.jobtechdev.se/yrkesprognoser/
    showInShort: null
  - title: Kom igång
    url: https://github.com/Jobtechdev-content/Yrkesprognoser-content/blob/develop/gettingstartedOccupationalForecastSE.md
    showInShort: null
  - title: Metodbeskrivning yrkesprognoser
    url: /sv/components/yrkesprognoser/metodbeskrivning-yrkesprognoser
    showInShort: null
  product_info:
  - title: Version
    value: 1.0.5
  - title: Licens
    value: CC0
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
  block_2: "### Viktig information\n\nArbetsförmedlingen utvecklar metoden för yrkesprognoser under hösten 2022 samt under 2023. En utveckling som kommer att förändra formatet och presentationen av resultaten. Exempelvis kommer utsikterna inom ett yrke inte endast beskrivas med en bedömningsnivå utan genom ett urval av faktorer som styr yrkets utveckling. \n\nDet är därför viktigt att ha i beaktande att informationens form kommer att förändras och se annorlunda ut vid framtida uppdateringar inom en period om cirka ett till två år.\n\n"
taxonomy:
  category:
  - Dataset
  type:
  - Öppna data
published: true
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/02.components/06.yrkesprognoser/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/02.components/06.yrkesprognoser/component_page.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/06.yrkesprognoser/component_page.sv.md
tags:
- user::pages::02.components::06.yrkesprognoser::component_page.sv.md
- pages::02.components::06.yrkesprognoser::component_page.sv.md
- 02.components::06.yrkesprognoser::component_page.sv.md
- 06.yrkesprognoser::component_page.sv.md
- component_page.sv.md
---
---
title: Yrkesprognoser
custom:
    title: Yrkesprognoser
    description: 'Prognoser för rekryteringsbehov och konkurrenssituation inom olika yrken.'
    block_1: "Yrkesprognoser erbjuder data avseende bedömt arbetsmarknadsläge för yrken i termer av konkurrens och möjligheter till jobb om ett och fem år i riket. Prognoserna bygger på en modell med registerdata från SCB på Standard för svensk yrkesklassificering (SSYK2012), sysselsättningsstatus, utbildning och ålder samt Arbetsförmedlingens verksamhetsstatistik på platsannonser. Prognosen justeras med hjälp av SCB:s befolkningsprognoser samt konjunkturbedömningar för näringsgrenarna. Prognosen redovisas för ca 170 prognosyrken, där ett prognosyrke består av en eller flera SSYK-yrkesgrupper.\n\n \n## Vem kan ha nytta av produkten?\nDatasetet är användbart för alla företag och organisationer som erbjuder tjänster inom yrkes- och studievägledning och som vill hjälpa sina målgrupper att fatta välgrundade beslut inför yrkes- och studieval. Aktörer som vill ha fördjupade insikter om utvecklingen på arbetsmarknaden har också nytta av datat.\n\n\n"
    menu:
        -
            title: Dataset
            url: 'https://data.jobtechdev.se/yrkesprognoser/'
            showInShort: null
        -
            title: 'Kom igång'
            url: 'https://github.com/Jobtechdev-content/Yrkesprognoser-content/blob/develop/gettingstartedOccupationalForecastSE.md'
            showInShort: null
        -
            title: 'Metodbeskrivning yrkesprognoser'
            url: /sv/components/yrkesprognoser/metodbeskrivning-yrkesprognoser
            showInShort: null
    product_info:
        -
            title: Version
            value: 1.0.5
        -
            title: Licens
            value: CC0
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    block_2: "### Viktig information\n\nArbetsförmedlingen utvecklar metoden för yrkesprognoser under hösten 2022 samt under 2023. En utveckling som kommer att förändra formatet och presentationen av resultaten. Exempelvis kommer utsikterna inom ett yrke inte endast beskrivas med en bedömningsnivå utan genom ett urval av faktorer som styr yrkets utveckling. \n\nDet är därför viktigt att ha i beaktande att informationens form kommer att förändras och se annorlunda ut vid framtida uppdateringar inom en period om cirka ett till två år.\n\n"
taxonomy:
    category:
        - Dataset
    type:
        - 'Öppna data'
published: true
---

