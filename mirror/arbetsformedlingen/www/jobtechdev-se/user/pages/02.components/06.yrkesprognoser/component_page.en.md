---
title: Occupational Forecasts
custom:
  title: Occupational Forecasts
  description: Forecasts for recruitment needs and competitive situation in different occupations.
  block_1: |-
    Occupational forecasts provide data, referring to a comprehensive labour market assessment for occupations in terms of competition and job opportunities in one to five years on a national level. The forecast model is designed on occupational registration data from Statistics Sweden (SCB) based on the Swedish Standard Classification of Occupations (SSYK 2012), employment status, education, age and the Swedish Public Employment Service's statistics from job advertisements. The forecast is aligned with SCBs population projections and the economic assessments for the industries. The forecast is reported for approximately 170 occupations, where an occupation consists of one or more SSYK occupational groups.

    ## For whom is the product created?
    The dataset could be useful for companies and organisations that provide vocational, study and career guidance. In particular, if they would like to encourage students to make informed decisions, which will help them find educational and career options that match up with their goals and the market's needs.

    The data is also intended for players, who would like to gain in-depth insights, while following the development of the labour market.
  menu:
  - title: 'Dataset '
    url: https://data.jobtechdev.se/yrkesprognoser/
    showInShort: null
  - title: Getting Started
    url: https://github.com/Jobtechdev-content/Yrkesprognoser-content/blob/develop/gettingstartedOccupationalForecastEN.md
    showInShort: '1'
  - title: Method Description
    url: /en/components/yrkesprognoser/metodbeskrivning-yrkesprognoser
    showInShort: null
  product_info:
  - title: Version
    value: 1.0.5
  - title: License
    value: CC0
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: Jobtech Development
  block_2: "### Important information\nThe Swedish Public Employment Service is developing the method for occupational forecasts on an ongoing basis during 2022 and 2023. The outcome may result in changes in the format and the presentation of future results. For example, the outlook within an occupation will not only be described by the assessment level, but through a selection of factors that outline the further development of a specific occupation.  \n\nTherefore, it is important to consider that the information format will be changing constantly and look differently in future updates within a period of approximately one to two years."
taxonomy:
  category:
  - Dataset
  type:
  - Open data
published: true
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/02.components/06.yrkesprognoser/component_page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/02.components/06.yrkesprognoser/component_page.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/06.yrkesprognoser/component_page.en.md
tags:
- user::pages::02.components::06.yrkesprognoser::component_page.en.md
- pages::02.components::06.yrkesprognoser::component_page.en.md
- 02.components::06.yrkesprognoser::component_page.en.md
- 06.yrkesprognoser::component_page.en.md
- component_page.en.md
---
---
title: 'Occupational Forecasts'
custom:
    title: 'Occupational Forecasts'
    description: 'Forecasts for recruitment needs and competitive situation in different occupations.'
    block_1: "Occupational forecasts provide data, referring to a comprehensive labour market assessment for occupations in terms of competition and job opportunities in one to five years on a national level. The forecast model is designed on occupational registration data from Statistics Sweden (SCB) based on the Swedish Standard Classification of Occupations (SSYK 2012), employment status, education, age and the Swedish Public Employment Service's statistics from job advertisements. The forecast is aligned with SCBs population projections and the economic assessments for the industries. The forecast is reported for approximately 170 occupations, where an occupation consists of one or more SSYK occupational groups.\n\n## For whom is the product created?\nThe dataset could be useful for companies and organisations that provide vocational, study and career guidance. In particular, if they would like to encourage students to make informed decisions, which will help them find educational and career options that match up with their goals and the market's needs.\n\nThe data is also intended for players, who would like to gain in-depth insights, while following the development of the labour market."
    menu:
        -
            title: 'Dataset '
            url: 'https://data.jobtechdev.se/yrkesprognoser/'
            showInShort: null
        -
            title: 'Getting Started'
            url: 'https://github.com/Jobtechdev-content/Yrkesprognoser-content/blob/develop/gettingstartedOccupationalForecastEN.md'
            showInShort: '1'
        -
            title: 'Method Description'
            url: /en/components/yrkesprognoser/metodbeskrivning-yrkesprognoser
            showInShort: null
    product_info:
        -
            title: Version
            value: 1.0.5
        -
            title: License
            value: CC0
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'Jobtech Development'
    block_2: "### Important information\nThe Swedish Public Employment Service is developing the method for occupational forecasts on an ongoing basis during 2022 and 2023. The outcome may result in changes in the format and the presentation of future results. For example, the outlook within an occupation will not only be described by the assessment level, but through a selection of factors that outline the further development of a specific occupation.  \n\nTherefore, it is important to consider that the information format will be changing constantly and look differently in future updates within a period of approximately one to two years."
taxonomy:
    category:
        - Dataset
    type:
        - 'Open data'
published: true
---

