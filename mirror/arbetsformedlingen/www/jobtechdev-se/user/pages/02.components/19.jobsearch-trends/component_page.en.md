---
title: JobSearch Trends
custom:
  title: JobSearch Trends
  description: It shows search behavior on the Swedish Public Employment Service’s Job Board Platsbanken over time.
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
  product_info:
  - title: Data format
    value: JSON
  - title: Version
    value: 0.1.0 Beta
  menu:
  - title: Dataset
    url: https://data.jobtechdev.se/annonser/search-trends/index.html
    showInShort: '1'
  - title: Getting Started
    url: https://gitlab.com/arbetsformedlingen/job-ads/search-trends
    showInShort: '1'
  - title: Discuss
    url: https://forum.jobtechdev.se/t/jobsearch-trends/629
    showInShort: null
  block_1: "JobSearch Trends is a data set with files that contain top search keywords on the Swedish Public Employment Service’s Job Board [Platsbanken](https://arbetsformedlingen.se/platsbanken/). New files are to be generated daily with the most common keywords within profession, language, skills, location, and employer, among others. The dataset consists today of JSON files, which make raw data available in a structured way. The available raw data goes through a quality assurance process. The dataset can be used to simplify processes for building and training algorithms concerning machine learning.\n\n## Which solutions does the product provide? \nBy using the dataset, it is possible to follow trends and analyze search behaviors within the labour market area.\n\n## For whom the product is created? \nThe dataset can be useful for companies, organisations, and even private persons, who would like to understand better the search behavior within the labour market area, by using structured and aggregated raw data."
  block_2: "### The dataset is free of charge and available for anyone to use. \n\nThe dataset is under ongoing development, and we are happy to get feedback and suggestions for improvements from our users. \nDo you want to more about JobSearch Trends, feel free to contact us or discuss it in our Community Forum. "
taxonomy:
  category:
  - Dataset
  type:
  - Open data
  status:
  - Beta
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/02.components/19.jobsearch-trends/component_page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/02.components/19.jobsearch-trends/component_page.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/19.jobsearch-trends/component_page.en.md
tags:
- user::pages::02.components::19.jobsearch-trends::component_page.en.md
- pages::02.components::19.jobsearch-trends::component_page.en.md
- 02.components::19.jobsearch-trends::component_page.en.md
- 19.jobsearch-trends::component_page.en.md
- component_page.en.md
---
---
title: 'JobSearch Trends'
custom:
    title: 'JobSearch Trends'
    description: 'It shows search behavior on the Swedish Public Employment Service’s Job Board Platsbanken over time.'
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    product_info:
        -
            title: 'Data format'
            value: JSON
        -
            title: Version
            value: '0.1.0 Beta'
    menu:
        -
            title: Dataset
            url: 'https://data.jobtechdev.se/annonser/search-trends/index.html'
            showInShort: '1'
        -
            title: 'Getting Started'
            url: 'https://gitlab.com/arbetsformedlingen/job-ads/search-trends'
            showInShort: '1'
        -
            title: Discuss
            url: 'https://forum.jobtechdev.se/t/jobsearch-trends/629'
            showInShort: null
    block_1: "JobSearch Trends is a data set with files that contain top search keywords on the Swedish Public Employment Service’s Job Board [Platsbanken](https://arbetsformedlingen.se/platsbanken/). New files are to be generated daily with the most common keywords within profession, language, skills, location, and employer, among others. The dataset consists today of JSON files, which make raw data available in a structured way. The available raw data goes through a quality assurance process. The dataset can be used to simplify processes for building and training algorithms concerning machine learning.\n\n## Which solutions does the product provide? \nBy using the dataset, it is possible to follow trends and analyze search behaviors within the labour market area.\n\n## For whom the product is created? \nThe dataset can be useful for companies, organisations, and even private persons, who would like to understand better the search behavior within the labour market area, by using structured and aggregated raw data."
    block_2: "### The dataset is free of charge and available for anyone to use. \n\nThe dataset is under ongoing development, and we are happy to get feedback and suggestions for improvements from our users. \nDo you want to more about JobSearch Trends, feel free to contact us or discuss it in our Community Forum. "
taxonomy:
    category:
        - Dataset
    type:
        - 'Open data'
    status:
        - Beta
---

