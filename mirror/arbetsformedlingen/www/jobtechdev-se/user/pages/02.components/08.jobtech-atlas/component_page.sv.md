---
title: Jobtech Atlas
custom:
  title: Jobtech Atlas
  description: Ett gränssnitt som visar den data som finns i produkten Jobtech Taxonomy.
  block_1: "Jobtech Atlas är en webbplats för användare att utforska databasen bakom Jobtech Taxonomy. Databasen innehåller strukturer, begrepp och relationer som används för att beskriva arbetsmarknaden.\nDär kan du söka bland listor av begrepp som ofta förekommer i matchning på arbetsmarknaden, exempelvis kompetensbegrepp, yrkesbenämningar med koppling till SSYK-strukturen (Standard för svensk yrkesklassificering) och sökbegrepp som kan användas för att underlätta vid sökning och filtrering.  Taxonomi-databasen förvaltas av Arbetsförmedlingen och bygger på information inhämtad från bland annat branschorganisationer, arbetsgivare och arbetssökande. Informationen finns även tillgänglig via [Jobtech Taxonomy-API:et](https://jobtechdev.se/sv/komponenter/jobtech-taxonomy). \n\n## Vilket problem löser produkten?\nEn viktig del i arbetet med att producera och leverera data är att göra det lättillgängligt. Med Jobtech Atlas tillgängliggörs den information som finns i Jobtech Taxonomy i ett format som användare har enklare att läsa och förstå än exempelvis ett API. \n\n## För vem är produkten skapad?\nAlla ska kunna se och bidra till det språk som används på arbetsmarknaden. Dessa uppgifter har tidigare varit svåra att komma åt för icke-utvecklare eller personer utanför Arbetsförmedlingen. För att lösa problemet och göra informationen tillgänglig för alla har vi byggt Jobtech Atlas."
  block_2: |-
    ### Notera
    Projektet är under utveckling och bör inte ses som produktionsklart. Funktioner kan komma att läggas till, tas bort eller modifieras. Designen är inte finjusterad och kommer sannolikt att ändras / förbättras.
    För tillfället har vi fokuserat på att göra kompetenser och yrkesdata tillgängliga. Resterande data kommer snart också att finnas tillgängliga.

    Vi välkomnar all eventuell feedback.
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
  product_info:
  - title: Version
    value: Beta
  menu:
  - title: Applikation
    url: https://atlas.jobtechdev.se/page/taxonomy.html
    showInShort: '1'
  - title: Källkod
    url: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-atlas
    showInShort: '1'
taxonomy:
  category:
  - Applikation
  type:
  - Öppna data
  status:
  - Beta
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/02.components/08.jobtech-atlas/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/02.components/08.jobtech-atlas/component_page.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/08.jobtech-atlas/component_page.sv.md
tags:
- user::pages::02.components::08.jobtech-atlas::component_page.sv.md
- pages::02.components::08.jobtech-atlas::component_page.sv.md
- 02.components::08.jobtech-atlas::component_page.sv.md
- 08.jobtech-atlas::component_page.sv.md
- component_page.sv.md
---
---
title: 'Jobtech Atlas'
custom:
    title: 'Jobtech Atlas'
    description: 'Ett gränssnitt som visar den data som finns i produkten Jobtech Taxonomy.'
    block_1: "Jobtech Atlas är en webbplats för användare att utforska databasen bakom Jobtech Taxonomy. Databasen innehåller strukturer, begrepp och relationer som används för att beskriva arbetsmarknaden.\nDär kan du söka bland listor av begrepp som ofta förekommer i matchning på arbetsmarknaden, exempelvis kompetensbegrepp, yrkesbenämningar med koppling till SSYK-strukturen (Standard för svensk yrkesklassificering) och sökbegrepp som kan användas för att underlätta vid sökning och filtrering.  Taxonomi-databasen förvaltas av Arbetsförmedlingen och bygger på information inhämtad från bland annat branschorganisationer, arbetsgivare och arbetssökande. Informationen finns även tillgänglig via [Jobtech Taxonomy-API:et](https://jobtechdev.se/sv/komponenter/jobtech-taxonomy). \n\n## Vilket problem löser produkten?\nEn viktig del i arbetet med att producera och leverera data är att göra det lättillgängligt. Med Jobtech Atlas tillgängliggörs den information som finns i Jobtech Taxonomy i ett format som användare har enklare att läsa och förstå än exempelvis ett API. \n\n## För vem är produkten skapad?\nAlla ska kunna se och bidra till det språk som används på arbetsmarknaden. Dessa uppgifter har tidigare varit svåra att komma åt för icke-utvecklare eller personer utanför Arbetsförmedlingen. För att lösa problemet och göra informationen tillgänglig för alla har vi byggt Jobtech Atlas."
    block_2: "### Notera\nProjektet är under utveckling och bör inte ses som produktionsklart. Funktioner kan komma att läggas till, tas bort eller modifieras. Designen är inte finjusterad och kommer sannolikt att ändras / förbättras.\nFör tillfället har vi fokuserat på att göra kompetenser och yrkesdata tillgängliga. Resterande data kommer snart också att finnas tillgängliga.\n\nVi välkomnar all eventuell feedback."
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    product_info:
        -
            title: Version
            value: Beta
    menu:
        -
            title: Applikation
            url: 'https://atlas.jobtechdev.se/page/taxonomy.html'
            showInShort: '1'
        -
            title: Källkod
            url: 'https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-atlas'
            showInShort: '1'
taxonomy:
    category:
        - Applikation
    type:
        - 'Öppna data'
    status:
        - Beta
---

