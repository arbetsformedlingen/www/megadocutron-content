---
title: JobSearch
custom:
  title: JobSearch
  block_1: "JobSearch möjliggör att söka bland Arbetsförmedlingens platsannonser på olika sätt. Dels med fritextsökning, men även med strukturerade sökbegrepp baserade på till exempel yrken eller geografi.\n\n\n\n## \n"
  description: Sökmotor för jobbannonser på Arbetsförmedlingens annonsplattform Platsbanken.
  menu:
  - title: API
    url: https://jobsearch.api.jobtechdev.se
    showInShort: '1'
  - title: Dokumentation
    url: https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis/-/blob/main/docs/GettingStartedJobSearchSE.md
    showInShort: null
  - title: Källkod
    url: https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis
    showInShort: '1'
  - title: Diskutera
    url: https://forum.jobtechdev.se/c/vara-api-er-dataset/job-search/28
    showInShort: null
  product_info:
  - title: Version
    value: '1.0'
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
  block_2: |
    API:et JobSearch gör det enkelt att skapa en egen söktjänst i appar eller webbsidor där sökningen sker mot JobSearch och sökresultatet kommer tillbaka i lättanvänt json-format.
  title_block_2: Vilket problem löser produkten?
  title_block_3: Vem kan ha nytta av produkten?
  block_4: "\nUnder september kommer endpointen Taxonomy/Search tas bort. För att kunna göra sökningar i taxonomin hänvisar vi till API:et [JobTech Taxonomy](https://jobtechdev.se/sv/komponenter/jobtech-taxonomy), där även möjlighet att använda GraphQL finns.\n\nOm det finns frågor kring detta, ställ dem gärna i [forumet](https://forum.jobtechdev.se/c/vara-api-er-dataset/job-search/28). "
  block_3: |-
    JobSearch är användbart för alla företag och organisationer som vill använda jobbannonser från [Platsbanken](https://arbetsformedlingen.se/platsbanken/) men som saknar en egen sökmotor.
    Det är också användbart för aktörer som har tillgång till stora mängder strukturerad data och vill kunna erbjuda specialsökfunktioner eller nischade annonsplattformar.

    Annonserna kompletteras med data från [JobAdEnrichment](https://www.jobtechdev.se/sv/komponenter/jobad-enrichments) för att förbättra sökningen.

    Den som vill ha en egen kopia av alla Arbetsförmedlingens annonser skall använda [JobStream.](https://www.jobtechdev.se/sv/komponenter/jobstream)
  title_block_4: Viktig information om API:et JobSearch
taxonomy:
  category:
  - API
  type:
  - Öppna data
  - Öppen källkod
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/02.components/01.jobsearch/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/02.components/01.jobsearch/component_page.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/01.jobsearch/component_page.sv.md
tags:
- user::pages::02.components::01.jobsearch::component_page.sv.md
- pages::02.components::01.jobsearch::component_page.sv.md
- 02.components::01.jobsearch::component_page.sv.md
- 01.jobsearch::component_page.sv.md
- component_page.sv.md
---
---
title: JobSearch
custom:
    title: JobSearch
    block_1: "JobSearch möjliggör att söka bland Arbetsförmedlingens platsannonser på olika sätt. Dels med fritextsökning, men även med strukturerade sökbegrepp baserade på till exempel yrken eller geografi.\n\n\n\n## \n"
    description: 'Sökmotor för jobbannonser på Arbetsförmedlingens annonsplattform Platsbanken.'
    menu:
        -
            title: API
            url: 'https://jobsearch.api.jobtechdev.se'
            showInShort: '1'
        -
            title: Dokumentation
            url: 'https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis/-/blob/main/docs/GettingStartedJobSearchSE.md'
            showInShort: null
        -
            title: Källkod
            url: 'https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis'
            showInShort: '1'
        -
            title: Diskutera
            url: 'https://forum.jobtechdev.se/c/vara-api-er-dataset/job-search/28'
            showInShort: null
    product_info:
        -
            title: Version
            value: '1.0'
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    block_2: "API:et JobSearch gör det enkelt att skapa en egen söktjänst i appar eller webbsidor där sökningen sker mot JobSearch och sökresultatet kommer tillbaka i lättanvänt json-format.\n"
    title_block_2: 'Vilket problem löser produkten?'
    title_block_3: 'Vem kan ha nytta av produkten?'
    block_4: "\nUnder september kommer endpointen Taxonomy/Search tas bort. För att kunna göra sökningar i taxonomin hänvisar vi till API:et [JobTech Taxonomy](https://jobtechdev.se/sv/komponenter/jobtech-taxonomy), där även möjlighet att använda GraphQL finns.\n\nOm det finns frågor kring detta, ställ dem gärna i [forumet](https://forum.jobtechdev.se/c/vara-api-er-dataset/job-search/28). "
    block_3: "JobSearch är användbart för alla företag och organisationer som vill använda jobbannonser från [Platsbanken](https://arbetsformedlingen.se/platsbanken/) men som saknar en egen sökmotor.\nDet är också användbart för aktörer som har tillgång till stora mängder strukturerad data och vill kunna erbjuda specialsökfunktioner eller nischade annonsplattformar.\n\nAnnonserna kompletteras med data från [JobAdEnrichment](https://www.jobtechdev.se/sv/komponenter/jobad-enrichments) för att förbättra sökningen.\n\nDen som vill ha en egen kopia av alla Arbetsförmedlingens annonser skall använda [JobStream.](https://www.jobtechdev.se/sv/komponenter/jobstream)"
    title_block_4: 'Viktig information om API:et JobSearch'
taxonomy:
    category:
        - API
    type:
        - 'Öppna data'
        - 'Öppen källkod'
---

