---
title: JobSearch
custom:
  title: JobSearch
  block_1: "The API JobSearch makes it possible to search among the Swedish Public Employment Service's job advertisements in different ways. It provides information through free text search, but also through structured search terms, based on, for example, professions or themes of geography. \n\n## What problem does the product solve?\nThe API JobSearch makes it easier to create own search engine tool in apps and/or webpages, when the search process goes through JobSearch, and the search result is returning in an easy-to-use json format.\n\n## For whom is the product created? \nJobSearch can be useful for companies and organisations, which would like to use job advertisements from the [Swedish Public Employment Service's Job Board (Platsbanken)](https://arbetsformedlingen.se/platsbanken) but are missing an own search engine tool.\n\nIt can be useful as well for players, which are having access to large amounts of structured data and are planning to offer special search functions or niche advertising platforms.\n\nThe ads are enriched with data from [JobAdEnrichment](https://www.jobtechdev.se/en/components/jobad-enrichments) in order to improve the search result. In addition, anyone, who whould like to make an own copy of all the job advertisements on the Swedish Public Employment Service's Job Board (Platsbanken), are adviced to use the [API JobStream.](https://www.jobtechdev.se/en/components/jobstream)\n"
  description: Search engine for job ads on Arbetsförmedlingens job board Platsbanken.
  menu:
  - title: API
    url: https://jobsearch.api.jobtechdev.se
    showInShort: '1'
  - title: 'Getting Started '
    url: https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis/-/blob/main/docs/GettingStartedJobSearchEN.md
    showInShort: '1'
  - title: Source code
    url: https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis
    showInShort: null
  - title: Forum
    url: https://forum.jobtechdev.se/c/vara-api-er-dataset/job-search/28
    showInShort: null
  product_info:
  - title: Version
    value: '1.0'
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
  block_2: |-
    The endpoint Taxonomy/Search will be removed by the end of September 2022. To be able to browse and search in the nomenclatures (taxonomies), we are kindly refering to the [API JobTech Taxonomy](https://jobtechdev.se/en/components/jobtech-taxonomy), where it is also possible to use GraphQL.

    If any further questions should appear, feel free to ask and comment in our [Community Forum](https://forum.jobtechdev.se/c/vara-api-er-dataset/job-search/28).
taxonomy:
  category:
  - API
  type:
  - Open data
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/02.components/01.jobsearch/component_page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/02.components/01.jobsearch/component_page.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/01.jobsearch/component_page.en.md
tags:
- user::pages::02.components::01.jobsearch::component_page.en.md
- pages::02.components::01.jobsearch::component_page.en.md
- 02.components::01.jobsearch::component_page.en.md
- 01.jobsearch::component_page.en.md
- component_page.en.md
---
---
title: JobSearch
custom:
    title: JobSearch
    block_1: "The API JobSearch makes it possible to search among the Swedish Public Employment Service's job advertisements in different ways. It provides information through free text search, but also through structured search terms, based on, for example, professions or themes of geography. \n\n## What problem does the product solve?\nThe API JobSearch makes it easier to create own search engine tool in apps and/or webpages, when the search process goes through JobSearch, and the search result is returning in an easy-to-use json format.\n\n## For whom is the product created? \nJobSearch can be useful for companies and organisations, which would like to use job advertisements from the [Swedish Public Employment Service's Job Board (Platsbanken)](https://arbetsformedlingen.se/platsbanken) but are missing an own search engine tool.\n\nIt can be useful as well for players, which are having access to large amounts of structured data and are planning to offer special search functions or niche advertising platforms.\n\nThe ads are enriched with data from [JobAdEnrichment](https://www.jobtechdev.se/en/components/jobad-enrichments) in order to improve the search result. In addition, anyone, who whould like to make an own copy of all the job advertisements on the Swedish Public Employment Service's Job Board (Platsbanken), are adviced to use the [API JobStream.](https://www.jobtechdev.se/en/components/jobstream)\n"
    description: 'Search engine for job ads on Arbetsförmedlingens job board Platsbanken.'
    menu:
        -
            title: API
            url: 'https://jobsearch.api.jobtechdev.se'
            showInShort: '1'
        -
            title: 'Getting Started '
            url: 'https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis/-/blob/main/docs/GettingStartedJobSearchEN.md'
            showInShort: '1'
        -
            title: 'Source code'
            url: 'https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis'
            showInShort: null
        -
            title: Forum
            url: 'https://forum.jobtechdev.se/c/vara-api-er-dataset/job-search/28'
            showInShort: null
    product_info:
        -
            title: Version
            value: '1.0'
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    block_2: "The endpoint Taxonomy/Search will be removed by the end of September 2022. To be able to browse and search in the nomenclatures (taxonomies), we are kindly refering to the [API JobTech Taxonomy](https://jobtechdev.se/en/components/jobtech-taxonomy), where it is also possible to use GraphQL.\n\nIf any further questions should appear, feel free to ask and comment in our [Community Forum](https://forum.jobtechdev.se/c/vara-api-er-dataset/job-search/28)."
taxonomy:
    category:
        - API
    type:
        - 'Open data'
---

