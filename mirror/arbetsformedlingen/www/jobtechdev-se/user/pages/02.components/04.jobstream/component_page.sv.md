---
title: JobStream
custom:
  title: JobStream
  description: Realtidsdata från alla jobbannonser som är publicerade hos Arbetsförmedlingen.
  block_1: "JobStream ger tillgång till alla jobbannonser som är publicerade i Platsbanken, inklusive realtidsinformation om förändringar som sker runt eller i dessa annonser, t.ex. publiceringar, avpubliceringar eller uppdateringar av annonstexter.\n\n## Vilket problem löser produkten?\nDet ger möjlighet att ha en egen kopia av alla annonser som publiceras via Arbetsförmedlingen. Det går även att filtrera på yrkes- eller geografiska begrepp.\nDet kan användas för att skapa en egen sökmotor, eller för olika typer av analys. Annonserna kan hållas uppdaterade via JobStream så att ändringar sparas i den egna databasen, och så att avpublicerade annonser kan hanteras.\n \n## Vem kan ha nytta av produkten?\nJobStream är användbart för alla företag och organisationer som har en egen sökmotor och vill förbättra sin tjänst med realtidsuppdaterad annonsdata. API:et kan också användas för att bygga och träna algoritmer i samband med maskininlärning eller för att identifiera och analysera trender på arbetsmarknaden utifrån annonsdata.\n\nFör att söka och få fram enstaka annonser används [JobSearch. ](https://www.jobtechdev.se/sv/komponenter/jobsearch)"
  menu:
  - title: API
    url: https://jobstream.api.jobtechdev.se
    showInShort: '1'
  - title: Dokumentation
    url: ' https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis/-/blob/main/docs/GettingStartedJobStreamSE.md'
    showInShort: '1'
  - title: Källkod
    url: https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis
    showInShort: '1'
  - title: Diskutera
    url: https://forum.jobtechdev.se/c/vara-api-er-dataset/job-stream/25
    showInShort: null
  product_info:
  - title: Version
    value: 1.17.1
  - title: Dataformat
    value: JSON
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
taxonomy:
  category:
  - API
  type:
  - Öppna data
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/02.components/04.jobstream/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/02.components/04.jobstream/component_page.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/04.jobstream/component_page.sv.md
tags:
- user::pages::02.components::04.jobstream::component_page.sv.md
- pages::02.components::04.jobstream::component_page.sv.md
- 02.components::04.jobstream::component_page.sv.md
- 04.jobstream::component_page.sv.md
- component_page.sv.md
---
---
title: JobStream
custom:
    title: JobStream
    description: 'Realtidsdata från alla jobbannonser som är publicerade hos Arbetsförmedlingen.'
    block_1: "JobStream ger tillgång till alla jobbannonser som är publicerade i Platsbanken, inklusive realtidsinformation om förändringar som sker runt eller i dessa annonser, t.ex. publiceringar, avpubliceringar eller uppdateringar av annonstexter.\n\n## Vilket problem löser produkten?\nDet ger möjlighet att ha en egen kopia av alla annonser som publiceras via Arbetsförmedlingen. Det går även att filtrera på yrkes- eller geografiska begrepp.\nDet kan användas för att skapa en egen sökmotor, eller för olika typer av analys. Annonserna kan hållas uppdaterade via JobStream så att ändringar sparas i den egna databasen, och så att avpublicerade annonser kan hanteras.\n \n## Vem kan ha nytta av produkten?\nJobStream är användbart för alla företag och organisationer som har en egen sökmotor och vill förbättra sin tjänst med realtidsuppdaterad annonsdata. API:et kan också användas för att bygga och träna algoritmer i samband med maskininlärning eller för att identifiera och analysera trender på arbetsmarknaden utifrån annonsdata.\n\nFör att söka och få fram enstaka annonser används [JobSearch. ](https://www.jobtechdev.se/sv/komponenter/jobsearch)"
    menu:
        -
            title: API
            url: 'https://jobstream.api.jobtechdev.se'
            showInShort: '1'
        -
            title: Dokumentation
            url: ' https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis/-/blob/main/docs/GettingStartedJobStreamSE.md'
            showInShort: '1'
        -
            title: Källkod
            url: 'https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis'
            showInShort: '1'
        -
            title: Diskutera
            url: 'https://forum.jobtechdev.se/c/vara-api-er-dataset/job-stream/25'
            showInShort: null
    product_info:
        -
            title: Version
            value: 1.17.1
        -
            title: Dataformat
            value: JSON
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
taxonomy:
    category:
        - API
    type:
        - 'Öppna data'
---

