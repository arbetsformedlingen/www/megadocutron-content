---
title: AllJobAds
custom:
  title: AllJobAds
  description: En widget som hämtar och visar utvalda jobb från Arbetsförmedlingens plattform Platsbanken.
  block_1: |-
    AllJobAds är en plug-and-play-widget som hämtar och visar jobb från Arbetsförmedlingens plattform Platsbanken. Widgeten kan ställas in för definierade områden och möjlighet finns att filtrera resultat.

    ## Vilket problem löser produkten?
    Många företag och organisationer önskar att få en en enkel lista med tillgängliga jobb på sin webbplats. Widgeten är utformad för att lätt kunna ändras och skräddarsys utifrån specifika önskemål och skapa mervärde för webbplatsens besökare.

    ## För vem är produkten skapad?
    AllJobAds är huvudsakligen avsedd för företag och organisationer som vill guida personer till jobb inom ett specifikt område, t.ex. kommun, företag eller yrke.
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
  menu:
  - title: Applikation
    url: https://widgets.jobtechdev.se/alljobads/
    showInShort: '1'
  - title: Källkod
    url: https://gitlab.com/arbetsformedlingen/www/allJobAdsWidget
    showInShort: '1'
taxonomy:
  category:
  - Applikation
  type:
  - Öppen källkod
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/02.components/10.alljobads/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/02.components/10.alljobads/component_page.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/10.alljobads/component_page.sv.md
tags:
- user::pages::02.components::10.alljobads::component_page.sv.md
- pages::02.components::10.alljobads::component_page.sv.md
- 02.components::10.alljobads::component_page.sv.md
- 10.alljobads::component_page.sv.md
- component_page.sv.md
---
---
title: AllJobAds
custom:
    title: AllJobAds
    description: 'En widget som hämtar och visar utvalda jobb från Arbetsförmedlingens plattform Platsbanken.'
    block_1: "AllJobAds är en plug-and-play-widget som hämtar och visar jobb från Arbetsförmedlingens plattform Platsbanken. Widgeten kan ställas in för definierade områden och möjlighet finns att filtrera resultat.\n\n## Vilket problem löser produkten?\nMånga företag och organisationer önskar att få en en enkel lista med tillgängliga jobb på sin webbplats. Widgeten är utformad för att lätt kunna ändras och skräddarsys utifrån specifika önskemål och skapa mervärde för webbplatsens besökare.\n\n## För vem är produkten skapad?\nAllJobAds är huvudsakligen avsedd för företag och organisationer som vill guida personer till jobb inom ett specifikt område, t.ex. kommun, företag eller yrke."
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    menu:
        -
            title: Applikation
            url: 'https://widgets.jobtechdev.se/alljobads/'
            showInShort: '1'
        -
            title: Källkod
            url: 'https://gitlab.com/arbetsformedlingen/www/allJobAdsWidget'
            showInShort: '1'
taxonomy:
    category:
        - Applikation
    type:
        - 'Öppen källkod'
---

