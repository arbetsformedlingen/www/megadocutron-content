---
title: AllJobAds
custom:
  title: AllJobAds
  description: A widget that shows selected jobs from the Swedish Public Employment Service's Job Board (Platsbanken).
  block_1: |-
    AllJobAds is a plug-and-play widget that displays and shows jobs from the [Swedish Public Employment Service's Job Board (Platsbanken)](https://arbetsformedlingen.se/platsbanken/). The widget can be set for specific areas of interest, and it is even possible to filter results.

    ## What problem does the product solve?
    Many companies and organizations prefer to have a simple list of job vacancies available on their website. The widget is designed to be easily adjusted and it is tailored based on specific requests, while creating added value for the website's visitors.

    ## For whom is the product created?
    AllJobAds is mainly targeted to companies and organizations that have a goal to guide people to get hired in a specific area, regardless of if it refers for example to municipality, company or profession.
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
  menu:
  - title: Application
    url: https://widgets.jobtechdev.se/alljobads/
    showInShort: '1'
  - title: Getting Started
    url: https://gitlab.com/arbetsformedlingen/www/allJobAdsWidget
    showInShort: '1'
taxonomy:
  category:
  - Application
  type:
  - Open source
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/02.components/10.alljobads/component_page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/02.components/10.alljobads/component_page.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/10.alljobads/component_page.en.md
tags:
- user::pages::02.components::10.alljobads::component_page.en.md
- pages::02.components::10.alljobads::component_page.en.md
- 02.components::10.alljobads::component_page.en.md
- 10.alljobads::component_page.en.md
- component_page.en.md
---
---
title: AllJobAds
custom:
    title: AllJobAds
    description: 'A widget that shows selected jobs from the Swedish Public Employment Service''s Job Board (Platsbanken).'
    block_1: "AllJobAds is a plug-and-play widget that displays and shows jobs from the [Swedish Public Employment Service's Job Board (Platsbanken)](https://arbetsformedlingen.se/platsbanken/). The widget can be set for specific areas of interest, and it is even possible to filter results.\n\n## What problem does the product solve?\nMany companies and organizations prefer to have a simple list of job vacancies available on their website. The widget is designed to be easily adjusted and it is tailored based on specific requests, while creating added value for the website's visitors.\n\n## For whom is the product created?\nAllJobAds is mainly targeted to companies and organizations that have a goal to guide people to get hired in a specific area, regardless of if it refers for example to municipality, company or profession."
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    menu:
        -
            title: Application
            url: 'https://widgets.jobtechdev.se/alljobads/'
            showInShort: '1'
        -
            title: 'Getting Started'
            url: 'https://gitlab.com/arbetsformedlingen/www/allJobAdsWidget'
            showInShort: '1'
taxonomy:
    category:
        - Application
    type:
        - 'Open source'
---

