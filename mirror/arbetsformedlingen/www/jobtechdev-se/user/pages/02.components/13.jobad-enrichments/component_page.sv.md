---
title: JobAd Enrichments
custom:
  description: AI-lösning som identifierar och extraherar relevant information i ostrukturerade jobbannonser.
  version: '1.0'
  format: JSON
  licens: MIT
  publishdate: 19-05-2021 10:00
  block_1: |+
    JobAd Enrichments är en AI-lösning som automatiskt hämtar ut relevanta ord och uttryck i platsannonser samtidigt som överflödig information filtreras bort. API:et bidrar till en mer träffsäker matchning mellan arbetsgivare och arbetssökande, och gör det lättare att navigera och snabbt hitta rätt på digitala annonsplattformar.





  title: JobAd Enrichments
  product_info:
  - title: Version
    value: '1.0'
  - title: Format
    value: JSON
  - title: Licens
    value: Creative Commons
  menu:
  - title: API
    url: https://jobad-enrichments-api.jobtechdev.se
    showInShort: '1'
  - title: Dokumentation (gitlab)
    url: https://github.com/Jobtechdev-content/JobAdEnrichments-content/blob/master/GettingstartedJobAdEnrichmentsSE.md
    showInShort: '1'
  - title: Diskutera
    url: https://forum.jobtechdev.se/c/vara-api-er-dataset/jobad-enrichments/26
    showInShort: null
  title_block_2: Vilket problem löser produkten?
  block_2: Irreleventa sökträffar är ett vanligt problem bland användare av digitala matchningstjänster och annonsplattformar. Att tvingas lägga tid på att rensa och sortera bland sökträffar försvårar jobbsökandet. Med JobAd Enrichments kan problemen minimeras, och aktörer som erbjuder digitala tjänster slipper lägga tid och resurser på manuell hantering.
  title_block_3: 'För vem är produkten skapad? '
  block_3: JobAd Enrichments är användbart för alla företag och organisationer som erbjuder en digital matchningstjänst eller annonsplattform, och som önskar förbättra den. API:et kan också användas för utveckling av nya innovativa digitala tjänster eller för att få fördjupad insikt om arbetsmarknaden.
  title_block_4: Bra att veta
  block_4: |-
    Vi rekommenderar att du kontaktar oss innan du
    implementerar API:et direkt mot ditt system så att vi kan ge råd och
    vägledning runt API:ets möjligheter och begränsningar.
taxonomy:
  category:
  - API
  type:
  - Öppna data
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/02.components/13.jobad-enrichments/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/02.components/13.jobad-enrichments/component_page.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/13.jobad-enrichments/component_page.sv.md
tags:
- user::pages::02.components::13.jobad-enrichments::component_page.sv.md
- pages::02.components::13.jobad-enrichments::component_page.sv.md
- 02.components::13.jobad-enrichments::component_page.sv.md
- 13.jobad-enrichments::component_page.sv.md
- component_page.sv.md
---
---
title: 'JobAd Enrichments'
custom:
    description: 'AI-lösning som identifierar och extraherar relevant information i ostrukturerade jobbannonser.'
    version: '1.0'
    format: JSON
    licens: MIT
    publishdate: '19-05-2021 10:00'
    block_1: "JobAd Enrichments är en AI-lösning som automatiskt hämtar ut relevanta ord och uttryck i platsannonser samtidigt som överflödig information filtreras bort. API:et bidrar till en mer träffsäker matchning mellan arbetsgivare och arbetssökande, och gör det lättare att navigera och snabbt hitta rätt på digitala annonsplattformar.\n\n\n\n\n\n"
    title: 'JobAd Enrichments'
    product_info:
        -
            title: Version
            value: '1.0'
        -
            title: Format
            value: JSON
        -
            title: Licens
            value: 'Creative Commons'
    menu:
        -
            title: API
            url: 'https://jobad-enrichments-api.jobtechdev.se'
            showInShort: '1'
        -
            title: 'Dokumentation (gitlab)'
            url: 'https://github.com/Jobtechdev-content/JobAdEnrichments-content/blob/master/GettingstartedJobAdEnrichmentsSE.md'
            showInShort: '1'
        -
            title: Diskutera
            url: 'https://forum.jobtechdev.se/c/vara-api-er-dataset/jobad-enrichments/26'
            showInShort: null
    title_block_2: 'Vilket problem löser produkten?'
    block_2: 'Irreleventa sökträffar är ett vanligt problem bland användare av digitala matchningstjänster och annonsplattformar. Att tvingas lägga tid på att rensa och sortera bland sökträffar försvårar jobbsökandet. Med JobAd Enrichments kan problemen minimeras, och aktörer som erbjuder digitala tjänster slipper lägga tid och resurser på manuell hantering.'
    title_block_3: 'För vem är produkten skapad? '
    block_3: 'JobAd Enrichments är användbart för alla företag och organisationer som erbjuder en digital matchningstjänst eller annonsplattform, och som önskar förbättra den. API:et kan också användas för utveckling av nya innovativa digitala tjänster eller för att få fördjupad insikt om arbetsmarknaden.'
    title_block_4: 'Bra att veta'
    block_4: "Vi rekommenderar att du kontaktar oss innan du\nimplementerar API:et direkt mot ditt system så att vi kan ge råd och\nvägledning runt API:ets möjligheter och begränsningar."
taxonomy:
    category:
        - API
    type:
        - 'Öppna data'
---

