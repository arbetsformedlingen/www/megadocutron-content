---
title: Kompetensmatchning
custom:
  title: Kompetensmatchning.se
  description: Test- och demonstrationsplattform för kompetensutveckling
  block_1: "Plattformen utvecklades initialt med fokus på fordonsindustrin i Göteborg, och de behov av kompetensutveckling som identifierades på grund av omställningen till elfordon. Efter det initiala projektet har plattformen pivoterat till att bli en testbädd för den digitala infrastrukturen, för att testa automatiserade lösningar och ha en skarp testyta för pilotprojekt.\n\n## Vilket problem löser produkten?\nKompetensmatchning.se erbjuder en marknadsplats för kompetensutveckling där arbetsgivare och utbildningsanordnare kan samverka kring kompetens- och utbildningsbehov för yrkesverksamma.\n\n## Vem kan ha nytta av produkten? \nKompetensmatchning.se är användbart för företag eller organisationer som har behov av eller jobbar med kompetensutveckling för yrkesverksamma. Den är också användbar för aktörer som har tillgång till stora mängder strukturerad data och vill kunna testa idéer och behov i en skarp miljö."
  block_2: Kompetensmatchning.se är utvecklat av det statligt ägda forskningsinstitutet RISE (Research Institutes of Sweden AB) i samverkan med Västra Götalandsregionen och Business Region Göteborg. RISE erbjuder unik expertis och ett 100-tal test- och demonstrationsmiljöer för framtidssäkra teknologier, produkter och tjänster.
  menu:
  - title: Applikation
    url: https://kompetensmatchning.se/
    showInShort: '1'
  - title: Källkod
    url: https://github.com/LearningArena/competence-hub
    showInShort: '1'
  product_info:
  - title: Licens
    value: EUPL v1.2
  contact_email: olle.nyman@ri.se
  contact_name: Olle Nyman
taxonomy:
  category:
  - Applikation
  type:
  - Öppen källkod
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/02.components/21.kompetensmatchning/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/02.components/21.kompetensmatchning/component_page.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/21.kompetensmatchning/component_page.sv.md
tags:
- user::pages::02.components::21.kompetensmatchning::component_page.sv.md
- pages::02.components::21.kompetensmatchning::component_page.sv.md
- 02.components::21.kompetensmatchning::component_page.sv.md
- 21.kompetensmatchning::component_page.sv.md
- component_page.sv.md
---
---
title: Kompetensmatchning
custom:
    title: Kompetensmatchning.se
    description: 'Test- och demonstrationsplattform för kompetensutveckling'
    block_1: "Plattformen utvecklades initialt med fokus på fordonsindustrin i Göteborg, och de behov av kompetensutveckling som identifierades på grund av omställningen till elfordon. Efter det initiala projektet har plattformen pivoterat till att bli en testbädd för den digitala infrastrukturen, för att testa automatiserade lösningar och ha en skarp testyta för pilotprojekt.\n\n## Vilket problem löser produkten?\nKompetensmatchning.se erbjuder en marknadsplats för kompetensutveckling där arbetsgivare och utbildningsanordnare kan samverka kring kompetens- och utbildningsbehov för yrkesverksamma.\n\n## Vem kan ha nytta av produkten? \nKompetensmatchning.se är användbart för företag eller organisationer som har behov av eller jobbar med kompetensutveckling för yrkesverksamma. Den är också användbar för aktörer som har tillgång till stora mängder strukturerad data och vill kunna testa idéer och behov i en skarp miljö."
    block_2: 'Kompetensmatchning.se är utvecklat av det statligt ägda forskningsinstitutet RISE (Research Institutes of Sweden AB) i samverkan med Västra Götalandsregionen och Business Region Göteborg. RISE erbjuder unik expertis och ett 100-tal test- och demonstrationsmiljöer för framtidssäkra teknologier, produkter och tjänster.'
    menu:
        -
            title: Applikation
            url: 'https://kompetensmatchning.se/'
            showInShort: '1'
        -
            title: Källkod
            url: 'https://github.com/LearningArena/competence-hub'
            showInShort: '1'
    product_info:
        -
            title: Licens
            value: 'EUPL v1.2'
    contact_email: olle.nyman@ri.se
    contact_name: 'Olle Nyman'
taxonomy:
    category:
        - Applikation
    type:
        - 'Öppen källkod'
---

