---
title: Connect Once
custom:
  title: Connect Once
  description: 'Transfer CV data, stored at the Swedish Public Employment Service, to external systems with the consent of the individual. '
  block_1: |-
    Connect Once is a data transfer service that exchanges structured data between two systems. The service gives the possibility to individuals to automatically fill in a job application with the help of the CV details and information, which once have been stored at My Page at the Swedish Public Employment Service (Arbetsförmedlingen).

    ## What problem does the product solve?
    Recruiters and employers experience that job candidates are abandoning online applications or quitting in the middle of the filling out process because of their length or complexity. Connect Once simplifies the job application by allowing job seekers to use their CV data, stored at Arbetsförmedlingen, to fill in an application form in a simple way with just one click. By shortening the time required to apply for a job, the conversation rates of completed applications to the employer will increase and the job seeker's experience of the entire recruitment process will improve.

    ## For whom is the product created?
    Connect Once is intended for recruiters and employers who are willing to increase the conversation rates of completed applications. The product can also be used to exchange other structured data between two systems such as registration form for online services.
  product_info:
  - title: License
    value: Apache License 2.0
  - title: Version
    value: 1.1.0-beta
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
  menu:
  - title: Getting Started
    url: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-connect/-/blob/master/README.md
    showInShort: '1'
  - title: Forum
    url: https://forum.jobtechdev.se/c/vara-api-er-dataset/connect-once/24
    showInShort: null
  block_2: |+
    ### Connect Once API
    The API Connect Once is free of charge and is available for everyone. Do you want to know more? Feel free to contact us.



taxonomy:
  category:
  - API
  type:
  - Open data
  - Open source
  status:
  - Beta
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/02.components/07.connect-once/component_page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/02.components/07.connect-once/component_page.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/07.connect-once/component_page.en.md
tags:
- user::pages::02.components::07.connect-once::component_page.en.md
- pages::02.components::07.connect-once::component_page.en.md
- 02.components::07.connect-once::component_page.en.md
- 07.connect-once::component_page.en.md
- component_page.en.md
---
---
title: 'Connect Once'
custom:
    title: 'Connect Once'
    description: 'Transfer CV data, stored at the Swedish Public Employment Service, to external systems with the consent of the individual. '
    block_1: "Connect Once is a data transfer service that exchanges structured data between two systems. The service gives the possibility to individuals to automatically fill in a job application with the help of the CV details and information, which once have been stored at My Page at the Swedish Public Employment Service (Arbetsförmedlingen).\n\n## What problem does the product solve?\nRecruiters and employers experience that job candidates are abandoning online applications or quitting in the middle of the filling out process because of their length or complexity. Connect Once simplifies the job application by allowing job seekers to use their CV data, stored at Arbetsförmedlingen, to fill in an application form in a simple way with just one click. By shortening the time required to apply for a job, the conversation rates of completed applications to the employer will increase and the job seeker's experience of the entire recruitment process will improve.\n\n## For whom is the product created?\nConnect Once is intended for recruiters and employers who are willing to increase the conversation rates of completed applications. The product can also be used to exchange other structured data between two systems such as registration form for online services."
    product_info:
        -
            title: License
            value: 'Apache License 2.0'
        -
            title: Version
            value: 1.1.0-beta
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    menu:
        -
            title: 'Getting Started'
            url: 'https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-connect/-/blob/master/README.md'
            showInShort: '1'
        -
            title: Forum
            url: 'https://forum.jobtechdev.se/c/vara-api-er-dataset/connect-once/24'
            showInShort: null
    block_2: "### Connect Once API\nThe API Connect Once is free of charge and is available for everyone. Do you want to know more? Feel free to contact us.\n\n\n\n"
taxonomy:
    category:
        - API
    type:
        - 'Open data'
        - 'Open source'
    status:
        - Beta
---

