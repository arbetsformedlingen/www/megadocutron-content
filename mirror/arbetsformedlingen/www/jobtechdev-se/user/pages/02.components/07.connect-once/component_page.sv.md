---
title: Connect Once
custom:
  title: Connect Once
  description: Överför CV-data lagrade hos Arbetsförmedlingen till externa system med individens samtycke.
  block_1: |-
    Connect Once är en tjänst för överföring av data som skickar strukturerad data mellan två system. Tjänsten gör det möjligt för en person att automatiskt fylla i en arbetsansökan med hjälp av CV-uppgifterna som lagras hos Arbetsförmedlingen.

    ## Vilket problem löser produkten?
    Rekryterare och arbetsgivare upplever att jobbsökare avstår eller inte fullföljer ansökningar på grund av komplexitet och längd på ansökningsformulär. Connect Once förenkla jobbansökningen genom att jobbsökare kan använda sin CV-data som lagras hos Arbetsförmedlingen för att fylla i sitt formulär med ett klick. Genom att förkorta den tid som krävs för att ansöka om ett jobb kommer volymen av fullföljda ansökningar till arbetsgivaren öka samtidigt som jobbsökarens upplevelse av processen förbättras.

    ## För vem är produkten skapad?
    Connect Once är avsett för rekryterare och arbetsgivare som vill öka andelen fullföljda ansökningar. Produkten kan också användas för att skicka annan strukturerad data mellan två system som t.ex. registreringsformulär för onlinetjänster.
  block_2: "### Connect Once API\nAPI:et betatestas nu på ett antal rekryteringssytem. Vill du också testa API:et? Kontakta oss så hjälper vi dig igång! \n\n"
  product_info:
  - title: Licens
    value: Apache License 2.0
  - title: Version
    value: 1.1.0-beta
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
  menu:
  - title: Källkod
    url: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-connect
    showInShort: '1'
  - title: Diskutera
    url: https://forum.jobtechdev.se/c/vara-api-er-dataset/connect-once/24
    showInShort: null
taxonomy:
  category:
  - API
  type:
  - Öppna data
  - Öppen källkod
  status:
  - Beta
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/02.components/07.connect-once/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/02.components/07.connect-once/component_page.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/07.connect-once/component_page.sv.md
tags:
- user::pages::02.components::07.connect-once::component_page.sv.md
- pages::02.components::07.connect-once::component_page.sv.md
- 02.components::07.connect-once::component_page.sv.md
- 07.connect-once::component_page.sv.md
- component_page.sv.md
---
---
title: 'Connect Once'
custom:
    title: 'Connect Once'
    description: 'Överför CV-data lagrade hos Arbetsförmedlingen till externa system med individens samtycke.'
    block_1: "Connect Once är en tjänst för överföring av data som skickar strukturerad data mellan två system. Tjänsten gör det möjligt för en person att automatiskt fylla i en arbetsansökan med hjälp av CV-uppgifterna som lagras hos Arbetsförmedlingen.\n\n## Vilket problem löser produkten?\nRekryterare och arbetsgivare upplever att jobbsökare avstår eller inte fullföljer ansökningar på grund av komplexitet och längd på ansökningsformulär. Connect Once förenkla jobbansökningen genom att jobbsökare kan använda sin CV-data som lagras hos Arbetsförmedlingen för att fylla i sitt formulär med ett klick. Genom att förkorta den tid som krävs för att ansöka om ett jobb kommer volymen av fullföljda ansökningar till arbetsgivaren öka samtidigt som jobbsökarens upplevelse av processen förbättras.\n\n## För vem är produkten skapad?\nConnect Once är avsett för rekryterare och arbetsgivare som vill öka andelen fullföljda ansökningar. Produkten kan också användas för att skicka annan strukturerad data mellan två system som t.ex. registreringsformulär för onlinetjänster."
    block_2: "### Connect Once API\nAPI:et betatestas nu på ett antal rekryteringssytem. Vill du också testa API:et? Kontakta oss så hjälper vi dig igång! \n\n"
    product_info:
        -
            title: Licens
            value: 'Apache License 2.0'
        -
            title: Version
            value: 1.1.0-beta
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    menu:
        -
            title: Källkod
            url: 'https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-connect'
            showInShort: '1'
        -
            title: Diskutera
            url: 'https://forum.jobtechdev.se/c/vara-api-er-dataset/connect-once/24'
            showInShort: null
taxonomy:
    category:
        - API
    type:
        - 'Öppna data'
        - 'Öppen källkod'
    status:
        - Beta
---

