---
title: About the website
custom:
  content: "## This website uses responsive design, and works well in modern browsers that support web standards set by W3C. Modern browsers include Safari, Chrome and Firefox.\n## \n\nOur ambition for the website is to fully meet the criteria and comply with the Web Content Accessibility Guidelines (WCAG 2.1), level AA. It means that we follow the same guidelines for accessibility adaptation as the legal requirements for authorities and other government agencies."
  menu:
  - title: Accessibility
    url: /en/about-the-website/availability
  - title: Cookie policy (in Swedish)
    url: /en/about-the-website/cookies
  title: About the website
routes:
  default: /about-the-website
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/08.about/page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/08.about/page.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/08.about/page.en.md
tags:
- user::pages::08.about::page.en.md
- pages::08.about::page.en.md
- 08.about::page.en.md
- page.en.md
---
---
title: 'About the website'
custom:
    content: "## This website uses responsive design, and works well in modern browsers that support web standards set by W3C. Modern browsers include Safari, Chrome and Firefox.\n## \n\nOur ambition for the website is to fully meet the criteria and comply with the Web Content Accessibility Guidelines (WCAG 2.1), level AA. It means that we follow the same guidelines for accessibility adaptation as the legal requirements for authorities and other government agencies."
    menu:
        -
            title: Accessibility
            url: /en/about-the-website/availability
        -
            title: 'Cookie policy (in Swedish)'
            url: /en/about-the-website/cookies
    title: 'About the website'
routes:
    default: /about-the-website
---

