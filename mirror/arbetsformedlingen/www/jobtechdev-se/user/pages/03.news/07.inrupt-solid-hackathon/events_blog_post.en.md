---
title: Inrupt Solid Hackathon, April 10th-28th
custom:
  title: Inrupt Solid Hackathon, April 10th-28th
  date: 2023-04-10 09:00
  endtime: 2023-04-28 17:00
  short: The hackathon presents developers with the opportunity to try out Inrupt's new enterprise-grade client libraries for Java application development. First- and second-place winners are eligible for monetary prizes and an opportunity to present their projects on an upcoming Solid World.
  content: |
    **The second edition of Inrupt Solid Hachathon will be taking place between Apil, 10th-28th 2023 as a part of Inrupt’s commitment to empower developers everywhere to do their best work and build amazing new experiences.**

    If you’re interested in building new Solid experiences for your users, kickstart your new app idea by participating in the Inrupt hackathon alongside a great community of developers.

    The hackathon presents developers with the opportunity to try out Inrupt's new enterprise-grade client libraries for Java application development. However, participants are welcome to use the organizers' JavaScript or Java libraries to create their project.

    First- and second-place winners are eligible for monetary prizes and an opportunity to present their projects on an upcoming Solid World.

    [For details and registration](https://www.inrupt.com/event/solid-hackathon/home)
date: 2023-03-28 14:01
taxonomy:
  category:
  - Event
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/07.inrupt-solid-hackathon/events_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/07.inrupt-solid-hackathon/events_blog_post.en.md
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/07.inrupt-solid-hackathon/events_blog_post.en.md
tags:
- user::pages::03.news::07.inrupt-solid-hackathon::events_blog_post.en.md
- pages::03.news::07.inrupt-solid-hackathon::events_blog_post.en.md
- 03.news::07.inrupt-solid-hackathon::events_blog_post.en.md
- 07.inrupt-solid-hackathon::events_blog_post.en.md
- events_blog_post.en.md
---
---
title: 'Inrupt Solid Hackathon, April 10th-28th'
custom:
    title: 'Inrupt Solid Hackathon, April 10th-28th'
    date: '2023-04-10 09:00'
    endtime: '2023-04-28 17:00'
    short: 'The hackathon presents developers with the opportunity to try out Inrupt''s new enterprise-grade client libraries for Java application development. First- and second-place winners are eligible for monetary prizes and an opportunity to present their projects on an upcoming Solid World.'
    content: "**The second edition of Inrupt Solid Hachathon will be taking place between Apil, 10th-28th 2023 as a part of Inrupt’s commitment to empower developers everywhere to do their best work and build amazing new experiences.**\n\nIf you’re interested in building new Solid experiences for your users, kickstart your new app idea by participating in the Inrupt hackathon alongside a great community of developers.\n\nThe hackathon presents developers with the opportunity to try out Inrupt's new enterprise-grade client libraries for Java application development. However, participants are welcome to use the organizers' JavaScript or Java libraries to create their project.\n\nFirst- and second-place winners are eligible for monetary prizes and an opportunity to present their projects on an upcoming Solid World.\n\n[For details and registration](https://www.inrupt.com/event/solid-hackathon/home)\n"
date: '2023-03-28 14:01'
taxonomy:
    category:
        - Event
---

