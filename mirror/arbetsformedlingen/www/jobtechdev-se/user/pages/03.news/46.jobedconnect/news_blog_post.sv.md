---
title: 'Nytt API: JobEd Connect '
custom:
  title: 'Nytt API: JobEd Connect '
  short: I dag lanserar vi JobEd Connect, ett öppet API som hjälper till att navigera mellan utbildningar och yrken och hitta rätt skolning för rätt jobb.
  content: |
    **I dag lanserar vi JobEd Connect, ett öppet API som hjälper till att navigera mellan utbildningar och yrken och hitta rätt skolning för rätt jobb.**

    Genom att sammanföra två dataområden – arbetsmarknad och utbildning – har vi identifierat ett nytt teknikområde mellan jobtech och edtech, där vi kopplar samman kompetensförsörjning med livslångt lärande för att främja långsiktig och hållbar matchning på arbetsmarknaden. Vi kallar det JobEdTech.

    Det nya API:et JobEd Connect kopplar automatiskt ihop utbildningar med de kompetenser som arbetsgivare oftast frågar efter för ett visst yrke. Det går också att få fram vilka utbildningar som bäst matchar ett visst yrke och dess mest efterfrågade kompetenser. Lösningen bygger på data från utbildningsdatabasen SUSA-navet och från stora mängder berikade jobbannonser.

    JobTech Development har utvecklat JobEd Connect med hjälp av öppna data för att gynna morgondagens snabbrörliga jobbmarknad, där rätt utbildning och kontinuerlig kompetensutveckling är nyckeln till framgång – för alla.

    Läs mer och [testa JobEd Connect här. ](https://jobtechdev.se/sv/komponenter/jobed-connect)
date: 2023-03-14 10:40
taxonomy:
  category:
  - Nyhet
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/46.jobedconnect/news_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/46.jobedconnect/news_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/46.jobedconnect/news_blog_post.sv.md
tags:
- user::pages::03.news::46.jobedconnect::news_blog_post.sv.md
- pages::03.news::46.jobedconnect::news_blog_post.sv.md
- 03.news::46.jobedconnect::news_blog_post.sv.md
- 46.jobedconnect::news_blog_post.sv.md
- news_blog_post.sv.md
---
---
title: 'Nytt API: JobEd Connect '
custom:
    title: 'Nytt API: JobEd Connect '
    short: 'I dag lanserar vi JobEd Connect, ett öppet API som hjälper till att navigera mellan utbildningar och yrken och hitta rätt skolning för rätt jobb.'
    content: "**I dag lanserar vi JobEd Connect, ett öppet API som hjälper till att navigera mellan utbildningar och yrken och hitta rätt skolning för rätt jobb.**\n\nGenom att sammanföra två dataområden – arbetsmarknad och utbildning – har vi identifierat ett nytt teknikområde mellan jobtech och edtech, där vi kopplar samman kompetensförsörjning med livslångt lärande för att främja långsiktig och hållbar matchning på arbetsmarknaden. Vi kallar det JobEdTech.\n\nDet nya API:et JobEd Connect kopplar automatiskt ihop utbildningar med de kompetenser som arbetsgivare oftast frågar efter för ett visst yrke. Det går också att få fram vilka utbildningar som bäst matchar ett visst yrke och dess mest efterfrågade kompetenser. Lösningen bygger på data från utbildningsdatabasen SUSA-navet och från stora mängder berikade jobbannonser.\n\nJobTech Development har utvecklat JobEd Connect med hjälp av öppna data för att gynna morgondagens snabbrörliga jobbmarknad, där rätt utbildning och kontinuerlig kompetensutveckling är nyckeln till framgång – för alla.\n\nLäs mer och [testa JobEd Connect här. ](https://jobtechdev.se/sv/komponenter/jobed-connect)\n"
date: '2023-03-14 10:40'
taxonomy:
    category:
        - Nyhet
---

