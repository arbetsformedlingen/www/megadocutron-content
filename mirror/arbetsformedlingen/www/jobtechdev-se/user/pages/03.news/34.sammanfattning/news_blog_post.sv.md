---
title: Open Space 2 - en sammanfattning
custom:
  title: Open Space 2 - en sammanfattning
  short: 'Stockholmsmötet var en fortsättning av serien workshops om digitalisering, kompetensförsörjning och livslångt lärande, arrangerat av Jobtech och Sunet. '
  content: "Under två intensiva dagar i mars 2022 har engagerade representanter från en lång rad svenska myndigheter och organisationer samlats för den andra upplagan av Open Space om digitalisering, kompetensförsörjning och livslångt lärande. Stockholmsmötet var en fortsättning av serien workshops i ämnet, arrangerat av Jobtech (Arbetsförmedlingen) och Sunet (Vetenskapsrådet). \n\nAgendan skapades på plats och ledde till samverkan och diskussioner kring det fortsatta arbetet med [regeringsuppdraget att utveckla en sammanhållen datainfrastruktur för kompetensförsörjning och livslångt lärande (RU KLL)](https://www.regeringen.se/regeringsuppdrag/2021/06/uppdrag-att-utveckla-en-sammanhallen-datainfrastruktur-for-kompetensforsorjning-och-livslangt-larande/). Deltagarna lyfte frågor och ämnen utifrån sina respektive områden på regional, nationell och EU-nivå. Genom mötestekniken Open Space (öppet forum) kunde man presentera pilotprojekt och öppet dela kunskap och insikter på ett brett plan i samband med det fortsatta arbetet kring en sammanhållen digital infrastruktur, där livslångt lärande har en central plats. \n\n**Exempel på ämnen som lyftes under Open Space i mars 2022:**\n\n* Mikromeriter\n* Yrkesprognoser \n* EU-perspektivet och dataportabilitet \n* Individdata och EU:s digital plånbok \n* Informations- och cybersäkerhet \n* Begreppsstruktur och gemensamt språk\n* Kompetensbehov och kvalifikationsvalidering \n* Ekosystemet för svensk arbetsmarknad\n* API:er \n\n\nHär hittar du ett [utdrag från deltagarnas diskussioner och frågeställningar](https://gitlab.com/arbetsformedlingen/open-space-digitalisering-kompetensforsorjning-och-livslangt-larande/-/wikis/Open-space-2:-digitalisering,-kompetensf%C3%B6rs%C3%B6rjning-och-livsl%C3%A5ngt-l%C3%A4rande,-16-och-17-mars-2022). \n\n[Boka in](https://jobtechdev.se/sv/nyheter/open-space-del-3) nästa Open Space-tillfälle i din kalender! Den 1-2 juni 2022 ses vi igen och fortsätter att bygga framtidens community för kompetensförsörjning och livslångt lärande. \n\nSe även [sammanfattning och anteckningar](https://gitlab.com/arbetsformedlingen/open-space-digitalisering-kompetensforsorjning-och-livslangt-larande/-/wikis/Open-Space-1:-Digitalisering,-Kompetensf%C3%B6rs%C3%B6rjning-och-Livsl%C3%A5ngt-L%C3%A4rande,-8-december-2021) från första mötet i december 2021.\n"
date: 2022-03-29 09:41
taxonomy:
  category:
  - Nyhet
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/34.sammanfattning/news_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/34.sammanfattning/news_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/34.sammanfattning/news_blog_post.sv.md
tags:
- user::pages::03.news::34.sammanfattning::news_blog_post.sv.md
- pages::03.news::34.sammanfattning::news_blog_post.sv.md
- 03.news::34.sammanfattning::news_blog_post.sv.md
- 34.sammanfattning::news_blog_post.sv.md
- news_blog_post.sv.md
---
---
title: 'Open Space 2 - en sammanfattning'
custom:
    title: 'Open Space 2 - en sammanfattning'
    short: 'Stockholmsmötet var en fortsättning av serien workshops om digitalisering, kompetensförsörjning och livslångt lärande, arrangerat av Jobtech och Sunet. '
    content: "Under två intensiva dagar i mars 2022 har engagerade representanter från en lång rad svenska myndigheter och organisationer samlats för den andra upplagan av Open Space om digitalisering, kompetensförsörjning och livslångt lärande. Stockholmsmötet var en fortsättning av serien workshops i ämnet, arrangerat av Jobtech (Arbetsförmedlingen) och Sunet (Vetenskapsrådet). \n\nAgendan skapades på plats och ledde till samverkan och diskussioner kring det fortsatta arbetet med [regeringsuppdraget att utveckla en sammanhållen datainfrastruktur för kompetensförsörjning och livslångt lärande (RU KLL)](https://www.regeringen.se/regeringsuppdrag/2021/06/uppdrag-att-utveckla-en-sammanhallen-datainfrastruktur-for-kompetensforsorjning-och-livslangt-larande/). Deltagarna lyfte frågor och ämnen utifrån sina respektive områden på regional, nationell och EU-nivå. Genom mötestekniken Open Space (öppet forum) kunde man presentera pilotprojekt och öppet dela kunskap och insikter på ett brett plan i samband med det fortsatta arbetet kring en sammanhållen digital infrastruktur, där livslångt lärande har en central plats. \n\n**Exempel på ämnen som lyftes under Open Space i mars 2022:**\n\n* Mikromeriter\n* Yrkesprognoser \n* EU-perspektivet och dataportabilitet \n* Individdata och EU:s digital plånbok \n* Informations- och cybersäkerhet \n* Begreppsstruktur och gemensamt språk\n* Kompetensbehov och kvalifikationsvalidering \n* Ekosystemet för svensk arbetsmarknad\n* API:er \n\n\nHär hittar du ett [utdrag från deltagarnas diskussioner och frågeställningar](https://gitlab.com/arbetsformedlingen/open-space-digitalisering-kompetensforsorjning-och-livslangt-larande/-/wikis/Open-space-2:-digitalisering,-kompetensf%C3%B6rs%C3%B6rjning-och-livsl%C3%A5ngt-l%C3%A4rande,-16-och-17-mars-2022). \n\n[Boka in](https://jobtechdev.se/sv/nyheter/open-space-del-3) nästa Open Space-tillfälle i din kalender! Den 1-2 juni 2022 ses vi igen och fortsätter att bygga framtidens community för kompetensförsörjning och livslångt lärande. \n\nSe även [sammanfattning och anteckningar](https://gitlab.com/arbetsformedlingen/open-space-digitalisering-kompetensforsorjning-och-livslangt-larande/-/wikis/Open-Space-1:-Digitalisering,-Kompetensf%C3%B6rs%C3%B6rjning-och-Livsl%C3%A5ngt-L%C3%A4rande,-8-december-2021) från första mötet i december 2021.\n"
date: '2022-03-29 09:41'
taxonomy:
    category:
        - Nyhet
---

