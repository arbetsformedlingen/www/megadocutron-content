---
custom:
  date: 2022-12-15 09:00
  endtime: 2022-12-15 16:00
  content: "DIGG arrangerar konferensen tillsammans med AI Sweden, RISE och Vinnova som en del i det gemensamma arbetet att främja svenskt deltagande i programmet för ett digitalt Europa.\n\nPå konferensen Sverige i Europas dataekosystem möts näringsliv, akademi och offentlig sektor kring EU:s dataekosystem och tillsammans utforskar vägen framåt. \n\nEuropas inre marknad för data fördjupas genom allt tätare samarbeten. Det innebär strategiska möjligheter för Sverige att engagera sig och påverka utvecklingen av EU:s AI- och dataekosystem.\n\n[För mer information](https://www.vinnova.se/kalenderhandelser/20222/12/sverige-i-europas-dataekosystem/)\n"
  short: DIGG arrangerar konferensen tillsammans med AI Sweden, RISE och Vinnova i det gemensamma arbete att främja svenskt deltagande i programmet för ett digitalt Europa.
  title: Sverige i Europas dataekosystem
title: Sverige i Europas dataekosystem 15 dec.
taxonomy:
  category:
  - Event
  status:
  - Avslutad
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/sverige-i-europas-dataekosystem/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/sverige-i-europas-dataekosystem/events_blog_post.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/sverige-i-europas-dataekosystem/events_blog_post.sv.md
tags:
- user::pages::03.news::sverige-i-europas-dataekosystem::events_blog_post.sv.md
- pages::03.news::sverige-i-europas-dataekosystem::events_blog_post.sv.md
- 03.news::sverige-i-europas-dataekosystem::events_blog_post.sv.md
- sverige-i-europas-dataekosystem::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
custom:
    date: '2022-12-15 09:00'
    endtime: '2022-12-15 16:00'
    content: "DIGG arrangerar konferensen tillsammans med AI Sweden, RISE och Vinnova som en del i det gemensamma arbetet att främja svenskt deltagande i programmet för ett digitalt Europa.\n\nPå konferensen Sverige i Europas dataekosystem möts näringsliv, akademi och offentlig sektor kring EU:s dataekosystem och tillsammans utforskar vägen framåt. \n\nEuropas inre marknad för data fördjupas genom allt tätare samarbeten. Det innebär strategiska möjligheter för Sverige att engagera sig och påverka utvecklingen av EU:s AI- och dataekosystem.\n\n[För mer information](https://www.vinnova.se/kalenderhandelser/20222/12/sverige-i-europas-dataekosystem/)\n"
    short: 'DIGG arrangerar konferensen tillsammans med AI Sweden, RISE och Vinnova i det gemensamma arbete att främja svenskt deltagande i programmet för ett digitalt Europa.'
    title: 'Sverige i Europas dataekosystem'
title: 'Sverige i Europas dataekosystem 15 dec.'
taxonomy:
    category:
        - Event
    status:
        - Avslutad
---

