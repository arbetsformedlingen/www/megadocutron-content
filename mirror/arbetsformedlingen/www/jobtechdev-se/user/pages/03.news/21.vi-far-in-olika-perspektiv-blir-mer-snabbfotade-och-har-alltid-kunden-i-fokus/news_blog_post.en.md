---
title: 'Innovation and Collaboration '
custom:
  title: Innovation is Created when Authorities and Private Actors Work Together
  date: 2021-11-22 13:23
  short: 'Interview with Gregory Golding, Head of JobTech Unit '
  content: "**Five questions** to Gregory Golding from JobTech Development, who will be participating at the yearly Internet Days Conference on November 22nd, talking about diverse collaborations between public and private actors, and how this can contribute to a better labour market.\n\n###### \"Innovation in the Service of Society\" is the topic for JobTech Development's participation at the Internet Days Conference. What is it about?\n“With the help of tax funds, we from JobTech Development will be talking about how we can create and contribute to better conditions in the labour market through investments in new, innovative digital solutions. We will be also talking about why it is important for diverse authorities to work more externally and become better at supporting and facilitating the work of other actors, both private and public, through different types of collaborations.\"\n\n###### In what way does innovation occur when authorities and private actors work together? \n”We get different perspectives, become more fast-paced in what we do, while having the customer in focus. It is about creating solutions for the whole society, but we divide the cost and the effort so that everyone can do what they are best at. As a result, the available resources will be used in a more correct and relevant way.”\n\n###### Jobtech Development has some extensive experience in this area, can you give us an example?\n“A very successful example is our collaboration with one of the world's largest insurance companies, AXA. We created a blockchain solution that facilitated the contacts between the insurance policyholders with the Swedish Public Employment Service, while at the same time it gave the individuals increased power and control over their own data. Together with AXA, we have tackled a common problem, and our collaboration has benefited both our customer groups.”\n\n###### To your mind, what is the biggest challenge in this area, in general?\n“Policy regulations, jurisprudence, legislation, which do not catch up with the rapid development, are the biggest challenge. Digitization is described as the new industrialization, but it becomes difficult to create structural changes when the existing regulations and legislation do not catch up with what is happening. To my modest opinion, I see this as a major obstacle to foster innovation and create new collaborations between public and private actors.\"\n\n###### Finally, what will be your most important message to the audience at the Internet Days Conference on November 22nd?\n\"Decision-and-policy makers, jurisprudence officers have to work together and to ensure better conditions, fostering innovative solutions that private and public actors can develop together.\""
date: 2021-11-22 13:23
taxonomy:
  category:
  - News
  type:
  - Interview
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/21.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus/news_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/21.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus/news_blog_post.en.md
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/21.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus/news_blog_post.en.md
tags:
- user::pages::03.news::21.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus::news_blog_post.en.md
- pages::03.news::21.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus::news_blog_post.en.md
- 03.news::21.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus::news_blog_post.en.md
- 21.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus::news_blog_post.en.md
- news_blog_post.en.md
---
---
title: 'Innovation and Collaboration '
custom:
    title: 'Innovation is Created when Authorities and Private Actors Work Together'
    date: '2021-11-22 13:23'
    short: 'Interview with Gregory Golding, Head of JobTech Unit '
    content: "**Five questions** to Gregory Golding from JobTech Development, who will be participating at the yearly Internet Days Conference on November 22nd, talking about diverse collaborations between public and private actors, and how this can contribute to a better labour market.\n\n###### \"Innovation in the Service of Society\" is the topic for JobTech Development's participation at the Internet Days Conference. What is it about?\n“With the help of tax funds, we from JobTech Development will be talking about how we can create and contribute to better conditions in the labour market through investments in new, innovative digital solutions. We will be also talking about why it is important for diverse authorities to work more externally and become better at supporting and facilitating the work of other actors, both private and public, through different types of collaborations.\"\n\n###### In what way does innovation occur when authorities and private actors work together? \n”We get different perspectives, become more fast-paced in what we do, while having the customer in focus. It is about creating solutions for the whole society, but we divide the cost and the effort so that everyone can do what they are best at. As a result, the available resources will be used in a more correct and relevant way.”\n\n###### Jobtech Development has some extensive experience in this area, can you give us an example?\n“A very successful example is our collaboration with one of the world's largest insurance companies, AXA. We created a blockchain solution that facilitated the contacts between the insurance policyholders with the Swedish Public Employment Service, while at the same time it gave the individuals increased power and control over their own data. Together with AXA, we have tackled a common problem, and our collaboration has benefited both our customer groups.”\n\n###### To your mind, what is the biggest challenge in this area, in general?\n“Policy regulations, jurisprudence, legislation, which do not catch up with the rapid development, are the biggest challenge. Digitization is described as the new industrialization, but it becomes difficult to create structural changes when the existing regulations and legislation do not catch up with what is happening. To my modest opinion, I see this as a major obstacle to foster innovation and create new collaborations between public and private actors.\"\n\n###### Finally, what will be your most important message to the audience at the Internet Days Conference on November 22nd?\n\"Decision-and-policy makers, jurisprudence officers have to work together and to ensure better conditions, fostering innovative solutions that private and public actors can develop together.\""
date: '2021-11-22 13:23'
taxonomy:
    category:
        - News
    type:
        - Interview
---

