---
title: New Research Report about Open Data Ecosystems
custom:
  title: New Research Report about Open Data Ecosystems
  date: 2022-02-18 09:15
  short: 'During the last 2 years, JobTech Development''s work has been evaluated on an ongoing basis by the researcher Johan Linåker from Lund University. '
  content: "During the last 2 years, JobTech Development's work has been evaluated on an ongoing basis by the researcher Johan Linåker from Lund University. He has followed JobTech together with his colleague Per Runeson and researched the growing community that has been created in relation to collaborative initiatives and sharing of open data. The project’s purpose was to create value, while increasing the usage of open government data. The duo have now summarized lessons learned and the research’s findings in a newly published report \"Recommendations for collaboration and sharing of data, technology and knowledge within government-driven open data ecosystems\". (In Swedish)\n\n> “The ongoing research within JobTech Dev Research Project has contributed to highlighting and promoting collaborative initiatives in relation to knowledge in addition to what the project itself has produced. Knowledge and expanding network have created direct benefit for the work, conducted within JobTech Development. ” the researchers state in the report. \n\nData is considered today as a critical resource, which through processing and making it accessible, can accelerate innovation, contribute to efficiency, and a more open, and democratic society. Furthermore, compatible systems and portability can enable long-term and sustainable management of this resource.\n\nFrom a scientific perspective, the report raises awareness of how knowledge and technology are intertwined. Within the same research project, several scientific articles have been written with concrete case studies on government collaboration on open data.\n\nThe report is written in Swedish and is accessible [here](https://portal.research.lu.se/en/publications/rekommendationer-f%C3%B6r-samverkan-och-delning-av-data-teknik-och-kun ). \n \nOther scientific articles related to the topic could be found on [Lund University’s Research Portal](https://portal.research.lu.se/) or at our [forum.](https://forum.jobtechdev.se/t/jobtech-och-det-oppna-data-ekosystemet-ny-forskningsrapport-visar-vagen-framat/553)\n\n![Governance Structure within Open Data Ecosystems](./eco_eng.png)\n\n_Figure 1: A governance model that illustrates and classifies actors within an ecosystem based on the level of influence and contribution. The actors in the closest layers are supposed to have increased contribution and influence on the ecosystem's direction, the platform's development, and the shared data._\n\n"
taxonomy:
  category:
  - News
  type:
  - Research
date: 2022-02-18 11:01
media_order: Styrningsstrukturer.png,eco_eng.png
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/23.ny-forskningsrapport-om-oeppna-data-ekosystem/news_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/23.ny-forskningsrapport-om-oeppna-data-ekosystem/news_blog_post.en.md
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/23.ny-forskningsrapport-om-oeppna-data-ekosystem/news_blog_post.en.md
tags:
- user::pages::03.news::23.ny-forskningsrapport-om-oeppna-data-ekosystem::news_blog_post.en.md
- pages::03.news::23.ny-forskningsrapport-om-oeppna-data-ekosystem::news_blog_post.en.md
- 03.news::23.ny-forskningsrapport-om-oeppna-data-ekosystem::news_blog_post.en.md
- 23.ny-forskningsrapport-om-oeppna-data-ekosystem::news_blog_post.en.md
- news_blog_post.en.md
---
---
title: 'New Research Report about Open Data Ecosystems'
custom:
    title: 'New Research Report about Open Data Ecosystems'
    date: '2022-02-18 09:15'
    short: 'During the last 2 years, JobTech Development''s work has been evaluated on an ongoing basis by the researcher Johan Linåker from Lund University. '
    content: "During the last 2 years, JobTech Development's work has been evaluated on an ongoing basis by the researcher Johan Linåker from Lund University. He has followed JobTech together with his colleague Per Runeson and researched the growing community that has been created in relation to collaborative initiatives and sharing of open data. The project’s purpose was to create value, while increasing the usage of open government data. The duo have now summarized lessons learned and the research’s findings in a newly published report \"Recommendations for collaboration and sharing of data, technology and knowledge within government-driven open data ecosystems\". (In Swedish)\n\n> “The ongoing research within JobTech Dev Research Project has contributed to highlighting and promoting collaborative initiatives in relation to knowledge in addition to what the project itself has produced. Knowledge and expanding network have created direct benefit for the work, conducted within JobTech Development. ” the researchers state in the report. \n\nData is considered today as a critical resource, which through processing and making it accessible, can accelerate innovation, contribute to efficiency, and a more open, and democratic society. Furthermore, compatible systems and portability can enable long-term and sustainable management of this resource.\n\nFrom a scientific perspective, the report raises awareness of how knowledge and technology are intertwined. Within the same research project, several scientific articles have been written with concrete case studies on government collaboration on open data.\n\nThe report is written in Swedish and is accessible [here](https://portal.research.lu.se/en/publications/rekommendationer-f%C3%B6r-samverkan-och-delning-av-data-teknik-och-kun ). \n \nOther scientific articles related to the topic could be found on [Lund University’s Research Portal](https://portal.research.lu.se/) or at our [forum.](https://forum.jobtechdev.se/t/jobtech-och-det-oppna-data-ekosystemet-ny-forskningsrapport-visar-vagen-framat/553)\n\n![Governance Structure within Open Data Ecosystems](./eco_eng.png)\n\n_Figure 1: A governance model that illustrates and classifies actors within an ecosystem based on the level of influence and contribution. The actors in the closest layers are supposed to have increased contribution and influence on the ecosystem's direction, the platform's development, and the shared data._\n\n"
taxonomy:
    category:
        - News
    type:
        - Research
date: '2022-02-18 11:01'
media_order: 'Styrningsstrukturer.png,eco_eng.png'
---

