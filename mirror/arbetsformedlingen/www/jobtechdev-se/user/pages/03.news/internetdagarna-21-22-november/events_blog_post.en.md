---
title: 'Annual Internet Days Conference '
custom:
  title: 2022 Internet Days Conference
  date: 2022-11-21 08:30
  endtime: 2022-11-22 15:45
  short: 'The Internet Days is Sweden''s annual two-day conference, which is focusing on internet and the impact the digitalisation has on the society. '
  content: "###### The Internet Days is Sweden's annual two-day conference taking place on November 21st-22nd, which is focusing on internet and the impact the digitalisation has on the society.  <BR><BR>\n\nDuring two days, the conference will offer the latest news in technology and internet, knowledge about digitalisation and its impact on the individual and the society. Topics related to open-source software and open data are taking place in a series of sessions. \n    \nThis year's world-leading keynote speakers will be sharing about their work to prevent disinformation, protect individual's integrity and how to reach out to even the youngest internet users on Tiktok.\n\n[Learn more and register here.](https://internetdagarna.se/)\n"
taxonomy:
  category:
  - Event
  status:
  - Completed
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/internetdagarna-21-22-november/events_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/internetdagarna-21-22-november/events_blog_post.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/internetdagarna-21-22-november/events_blog_post.en.md
tags:
- user::pages::03.news::internetdagarna-21-22-november::events_blog_post.en.md
- pages::03.news::internetdagarna-21-22-november::events_blog_post.en.md
- 03.news::internetdagarna-21-22-november::events_blog_post.en.md
- internetdagarna-21-22-november::events_blog_post.en.md
- events_blog_post.en.md
---
---
title: 'Annual Internet Days Conference '
custom:
    title: '2022 Internet Days Conference'
    date: '2022-11-21 08:30'
    endtime: '2022-11-22 15:45'
    short: 'The Internet Days is Sweden''s annual two-day conference, which is focusing on internet and the impact the digitalisation has on the society. '
    content: "###### The Internet Days is Sweden's annual two-day conference taking place on November 21st-22nd, which is focusing on internet and the impact the digitalisation has on the society.  <BR><BR>\n\nDuring two days, the conference will offer the latest news in technology and internet, knowledge about digitalisation and its impact on the individual and the society. Topics related to open-source software and open data are taking place in a series of sessions. \n    \nThis year's world-leading keynote speakers will be sharing about their work to prevent disinformation, protect individual's integrity and how to reach out to even the youngest internet users on Tiktok.\n\n[Learn more and register here.](https://internetdagarna.se/)\n"
taxonomy:
    category:
        - Event
    status:
        - Completed
---

