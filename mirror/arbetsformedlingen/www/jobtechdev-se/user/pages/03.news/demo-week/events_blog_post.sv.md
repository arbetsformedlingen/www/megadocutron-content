---
title: Demo Week 9-12 maj 2022
custom:
  title: Demo Week 9-12 maj 2022
  date: 2022-05-09 11:00
  endtime: 2022-05-12 13:30
  short: Nu erbjuder vi alla en möjlighet att lära sig mer om de pågående projekten i regeringsuppdraget om en sammanhållen datainfrastruktur för kompetensförsörjning och livslångt lärande.
  content: "**Välkommen till Demo Week!**\n\nNu erbjuder vi alla en möjlighet att lära sig mer om de pågående projekten i [regeringsuppdraget om en sammanhållen datainfrastruktur för kompetensförsörjning och livslångt lärande](https://www.regeringen.se/regeringsuppdrag/2021/06/uppdrag-att-utveckla-en-sammanhallen-datainfrastruktur-for-kompetensforsorjning-och-livslangt-larande/).\n\nVarje dag mellan **den 9 och 12 maj** sker halvtimmeslånga digitala presentationer av spännande pilotprojekt och initiativ, till exempel om: \n\n* Individdata och dataportabilitet\n* Matchning av utbildningar mot jobb och jobb mot utbildningar\n* Skolverkets Susa-nav\n* Yrkeshögskolans kvalifikationsdatabas, mm\n\nAlla som är intresserade är välkomna att delta kostnadsfritt. \n\n[Till anmälan](https://www.eventbrite.se/e/demo-week-digitalisering-kompetensforsorjning-och-livslangt-larande-biljetter-328155290307?aff=ebdssbdestsearch&keep_tld=1) \n\nVälkommen att samverka i ett engagerat community kring digitalisering, livslångt lärande och framtidens arbetsmarknad. "
taxonomy:
  category:
  - Event
  status:
  - Avslutad
metadata:
  description: Demoweek
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/demo-week/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/demo-week/events_blog_post.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/demo-week/events_blog_post.sv.md
tags:
- user::pages::03.news::demo-week::events_blog_post.sv.md
- pages::03.news::demo-week::events_blog_post.sv.md
- 03.news::demo-week::events_blog_post.sv.md
- demo-week::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'Demo Week 9-12 maj 2022'
custom:
    title: 'Demo Week 9-12 maj 2022'
    date: '2022-05-09 11:00'
    endtime: '2022-05-12 13:30'
    short: 'Nu erbjuder vi alla en möjlighet att lära sig mer om de pågående projekten i regeringsuppdraget om en sammanhållen datainfrastruktur för kompetensförsörjning och livslångt lärande.'
    content: "**Välkommen till Demo Week!**\n\nNu erbjuder vi alla en möjlighet att lära sig mer om de pågående projekten i [regeringsuppdraget om en sammanhållen datainfrastruktur för kompetensförsörjning och livslångt lärande](https://www.regeringen.se/regeringsuppdrag/2021/06/uppdrag-att-utveckla-en-sammanhallen-datainfrastruktur-for-kompetensforsorjning-och-livslangt-larande/).\n\nVarje dag mellan **den 9 och 12 maj** sker halvtimmeslånga digitala presentationer av spännande pilotprojekt och initiativ, till exempel om: \n\n* Individdata och dataportabilitet\n* Matchning av utbildningar mot jobb och jobb mot utbildningar\n* Skolverkets Susa-nav\n* Yrkeshögskolans kvalifikationsdatabas, mm\n\nAlla som är intresserade är välkomna att delta kostnadsfritt. \n\n[Till anmälan](https://www.eventbrite.se/e/demo-week-digitalisering-kompetensforsorjning-och-livslangt-larande-biljetter-328155290307?aff=ebdssbdestsearch&keep_tld=1) \n\nVälkommen att samverka i ett engagerat community kring digitalisering, livslångt lärande och framtidens arbetsmarknad. "
taxonomy:
    category:
        - Event
    status:
        - Avslutad
metadata:
    description: Demoweek
---

