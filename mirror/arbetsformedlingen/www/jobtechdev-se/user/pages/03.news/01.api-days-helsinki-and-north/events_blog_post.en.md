---
title: JobTechDev Joins API Days Helsinki & North 2023
custom:
  title: JobTechDev Joins API Days Helsinki & North 2023
  date: 2023-06-05 08:00
  endtime: 2023-06-06 17:00
  short: 'Apidays Helsinki & North joins the tradition of hugely popular global apidays events. The conference is brought together with the local APIOps community. Our Product Lead, Linked Data, Carl Lernberg will be presenting JobTech Development''s latest API JobEd Connect. '
  content: |-
    **JobTech Development will participate in API Days Helsinki & North on June 5th-6th, 2023. The event joins the tradition of hugely popular global apidays events.**

    This year, Carl Lernberg, Product Lead, Linked Data at JobTech Development, will be presenting one of our latest developed APIs [JobEd Connect](https://jobtechdev.se/en/components/jobed-connect). The API is a matching solution that automatically connects educational programs to job occupations, based on the labour market's demands. The data is sourced from National Education Database SUSA Hub (_in Swedish SUSA-navet_) and the Swedish Public Employment Service's labour market data.

    His presentation **"Open matching solution linking educations to occupations"** will take place on June 5th, 2023 at 1 pm EEST (12 pm CET).

    [Agenda and registration](https://www.apidays.global/helsinki_and_north/)
date: 2023-05-23 13:05
taxonomy:
  category:
  - Event
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/01.api-days-helsinki-and-north/events_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/01.api-days-helsinki-and-north/events_blog_post.en.md
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/01.api-days-helsinki-and-north/events_blog_post.en.md
tags:
- user::pages::03.news::01.api-days-helsinki-and-north::events_blog_post.en.md
- pages::03.news::01.api-days-helsinki-and-north::events_blog_post.en.md
- 03.news::01.api-days-helsinki-and-north::events_blog_post.en.md
- 01.api-days-helsinki-and-north::events_blog_post.en.md
- events_blog_post.en.md
---
---
title: 'JobTechDev Joins API Days Helsinki & North 2023'
custom:
    title: 'JobTechDev Joins API Days Helsinki & North 2023'
    date: '2023-06-05 08:00'
    endtime: '2023-06-06 17:00'
    short: 'Apidays Helsinki & North joins the tradition of hugely popular global apidays events. The conference is brought together with the local APIOps community. Our Product Lead, Linked Data, Carl Lernberg will be presenting JobTech Development''s latest API JobEd Connect. '
    content: "**JobTech Development will participate in API Days Helsinki & North on June 5th-6th, 2023. The event joins the tradition of hugely popular global apidays events.**\n\nThis year, Carl Lernberg, Product Lead, Linked Data at JobTech Development, will be presenting one of our latest developed APIs [JobEd Connect](https://jobtechdev.se/en/components/jobed-connect). The API is a matching solution that automatically connects educational programs to job occupations, based on the labour market's demands. The data is sourced from National Education Database SUSA Hub (_in Swedish SUSA-navet_) and the Swedish Public Employment Service's labour market data.\n\nHis presentation **\"Open matching solution linking educations to occupations\"** will take place on June 5th, 2023 at 1 pm EEST (12 pm CET).\n\n[Agenda and registration](https://www.apidays.global/helsinki_and_north/)"
date: '2023-05-23 13:05'
taxonomy:
    category:
        - Event
---

