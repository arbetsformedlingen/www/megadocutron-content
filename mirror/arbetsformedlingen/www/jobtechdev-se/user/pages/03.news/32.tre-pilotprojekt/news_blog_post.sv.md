---
title: Tre nya pilotprojekt
custom:
  title: Tre nya pilotprojekt
  date: 2022-02-28 13:37
  short: JobTech Development arbetar med tre större pilotprojekt inom området kompetensförsörjning och livslångt lärande.
  content: "JobTech Development arbetar med tre större pilotprojekt inom området kompetensförsörjning och livslångt lärande. Syftet är att möjliggöra transformationen och bidra till en mer inkluderande, kompetensbaserad och datadriven arbets- och utbildningsmarknad. \n\nDet handlar om: \n1. utveckling av digital vägledning genom länkad och berikad data \n2. gemensam begreppsstruktur för utbildning och arbetsmarknad \n3. digital infrastruktur som ger individen kontroll över sin egen data\n\nHär kan du lära dig mer om våra projekt: \n* [Digital vägledning och länkad data](https://jobtechdev.se/sv/komponenter/digital-vaegledning-och-laenkad-data)  \n* [Taxonomi och begreppsstruktur](https://jobtechdev.se/sv/komponenter/taxonomi-och-begreppsstruktur)\n* [Individdata och dataportabilitet](https://jobtechdev.se/sv/komponenter/individdata-och-dataportabilitet)"
  minititle: Så funkar det
date: 2022-02-28 13:39
taxonomy:
  category:
  - Nyhet
  type:
  - Forskning
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/32.tre-pilotprojekt/news_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/32.tre-pilotprojekt/news_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/32.tre-pilotprojekt/news_blog_post.sv.md
tags:
- user::pages::03.news::32.tre-pilotprojekt::news_blog_post.sv.md
- pages::03.news::32.tre-pilotprojekt::news_blog_post.sv.md
- 03.news::32.tre-pilotprojekt::news_blog_post.sv.md
- 32.tre-pilotprojekt::news_blog_post.sv.md
- news_blog_post.sv.md
---
---
title: 'Tre nya pilotprojekt'
custom:
    title: 'Tre nya pilotprojekt'
    date: '2022-02-28 13:37'
    short: 'JobTech Development arbetar med tre större pilotprojekt inom området kompetensförsörjning och livslångt lärande.'
    content: "JobTech Development arbetar med tre större pilotprojekt inom området kompetensförsörjning och livslångt lärande. Syftet är att möjliggöra transformationen och bidra till en mer inkluderande, kompetensbaserad och datadriven arbets- och utbildningsmarknad. \n\nDet handlar om: \n1. utveckling av digital vägledning genom länkad och berikad data \n2. gemensam begreppsstruktur för utbildning och arbetsmarknad \n3. digital infrastruktur som ger individen kontroll över sin egen data\n\nHär kan du lära dig mer om våra projekt: \n* [Digital vägledning och länkad data](https://jobtechdev.se/sv/komponenter/digital-vaegledning-och-laenkad-data)  \n* [Taxonomi och begreppsstruktur](https://jobtechdev.se/sv/komponenter/taxonomi-och-begreppsstruktur)\n* [Individdata och dataportabilitet](https://jobtechdev.se/sv/komponenter/individdata-och-dataportabilitet)"
    minititle: 'Så funkar det'
date: '2022-02-28 13:39'
taxonomy:
    category:
        - Nyhet
    type:
        - Forskning
---

