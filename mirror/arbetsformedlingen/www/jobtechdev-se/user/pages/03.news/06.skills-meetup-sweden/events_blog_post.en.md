---
title: Skills Meetup Sweden, April 19th-21st
custom:
  title: Skills Meetup Sweden, April 19th-21st
  date: 2023-04-19 11:00
  endtime: 2023-04-21 16:00
  short: Welcome to a meeting place for exchanging experiences, networking, and shared learning, addressing challenges for successful skills supply.
  content: "**The three-days conference \"Skills Meetup Sweden\" is taking place between April 19th-21st, 2023 as part of the current Swedish Presidency of the European Council. It is a meeting place for exchanging experiences, networking, and shared learning, addressing challenges for successful skills supply.\n**\n\nHow can we solve the competence shortage in the Swedish labour market? How can we build sustainable infrastructure for lifelong learning? What is needed to strengthen the workplace and support the individuals for increased competitiveness? These questions will be broadly discussed at Skills Meetup Sweden, brinding participants from different fields and sectors. \n\nThe meeting place is for those who work with topics related to skills supply and lifelong learning at national, regional, and local levels. Qualifications, micro-credentials, vocational training, and validation are at a glance referring to fields of education and the workplace. The event will grasp the international perspective related to the EU's Skills Agenda.\n\n**Venue:** Quality Hotel View, Hyllie, Malmö.\n\n[Details and registration](https://app.bwz.se/myh/b/v?event=19&ucrc=2ECD33F0F9)"
date: 2023-03-28 11:58
taxonomy:
  category:
  - Event
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/06.skills-meetup-sweden/events_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/06.skills-meetup-sweden/events_blog_post.en.md
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/06.skills-meetup-sweden/events_blog_post.en.md
tags:
- user::pages::03.news::06.skills-meetup-sweden::events_blog_post.en.md
- pages::03.news::06.skills-meetup-sweden::events_blog_post.en.md
- 03.news::06.skills-meetup-sweden::events_blog_post.en.md
- 06.skills-meetup-sweden::events_blog_post.en.md
- events_blog_post.en.md
---
---
title: 'Skills Meetup Sweden, April 19th-21st'
custom:
    title: 'Skills Meetup Sweden, April 19th-21st'
    date: '2023-04-19 11:00'
    endtime: '2023-04-21 16:00'
    short: 'Welcome to a meeting place for exchanging experiences, networking, and shared learning, addressing challenges for successful skills supply.'
    content: "**The three-days conference \"Skills Meetup Sweden\" is taking place between April 19th-21st, 2023 as part of the current Swedish Presidency of the European Council. It is a meeting place for exchanging experiences, networking, and shared learning, addressing challenges for successful skills supply.\n**\n\nHow can we solve the competence shortage in the Swedish labour market? How can we build sustainable infrastructure for lifelong learning? What is needed to strengthen the workplace and support the individuals for increased competitiveness? These questions will be broadly discussed at Skills Meetup Sweden, brinding participants from different fields and sectors. \n\nThe meeting place is for those who work with topics related to skills supply and lifelong learning at national, regional, and local levels. Qualifications, micro-credentials, vocational training, and validation are at a glance referring to fields of education and the workplace. The event will grasp the international perspective related to the EU's Skills Agenda.\n\n**Venue:** Quality Hotel View, Hyllie, Malmö.\n\n[Details and registration](https://app.bwz.se/myh/b/v?event=19&ucrc=2ECD33F0F9)"
date: '2023-03-28 11:58'
taxonomy:
    category:
        - Event
---

