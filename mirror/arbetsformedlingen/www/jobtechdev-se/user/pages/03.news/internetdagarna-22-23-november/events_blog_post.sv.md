---
title: Internetdagarna
custom:
  title: Internetdagarna 22-23 november
  short: Greg Golding från JobTech kommer att tala om Morgondagens arbets- och utbildningssystem med fokus på kompetensmatchning.
  date: 2021-11-22 09:00
  endtime: 2021-11-23 17:00
  content: "Har du inte anmält dig till Internetdagarna är det dags nu.  Massa spännande ämnen under temat – ”Bredda ditt digitala perspektiv”.  Eventet är digitalt och pågår mellan 22-23 november.  JobTechs enhetschef Greg Golding talar i spåret \"Framtidens arbetsliv: hybrider, gig och distans\" den 22 november kl 10:25-10:40. Greg kommer göra en djupdykning i morgondagens arbets- och utbildningssystem med fokus på kompetensmatchning.   \n\nHoppas vi ses. "
taxonomy:
  category:
  - Event
  status:
  - Avslutad
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/internetdagarna-22-23-november/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/internetdagarna-22-23-november/events_blog_post.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/internetdagarna-22-23-november/events_blog_post.sv.md
tags:
- user::pages::03.news::internetdagarna-22-23-november::events_blog_post.sv.md
- pages::03.news::internetdagarna-22-23-november::events_blog_post.sv.md
- 03.news::internetdagarna-22-23-november::events_blog_post.sv.md
- internetdagarna-22-23-november::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: Internetdagarna
custom:
    title: 'Internetdagarna 22-23 november'
    short: 'Greg Golding från JobTech kommer att tala om Morgondagens arbets- och utbildningssystem med fokus på kompetensmatchning.'
    date: '2021-11-22 09:00'
    endtime: '2021-11-23 17:00'
    content: "Har du inte anmält dig till Internetdagarna är det dags nu.  Massa spännande ämnen under temat – ”Bredda ditt digitala perspektiv”.  Eventet är digitalt och pågår mellan 22-23 november.  JobTechs enhetschef Greg Golding talar i spåret \"Framtidens arbetsliv: hybrider, gig och distans\" den 22 november kl 10:25-10:40. Greg kommer göra en djupdykning i morgondagens arbets- och utbildningssystem med fokus på kompetensmatchning.   \n\nHoppas vi ses. "
taxonomy:
    category:
        - Event
    status:
        - Avslutad
---

