---
title: OS Driving Digital Decade 18 april
custom:
  title: OS Driving Digital Decade 18 april
  date: 2023-04-18 10:00
  endtime: 2023-04-18 17:30
  short: 'Vi samlar europeiska aktörer i Nordens huvudstad för att fortsätta diskussionen om rollen för öppna teknologier i att forma vår gemensamma digitala framtid. JobTech Developments Maria Dalhage som är NOSAD:s projektägare kommer delta i en av panelerna under dagen '
  content: |-
    Den 18 april 2023 anordnar OpenForum Europe, Stiftelsen för internetinfrastruktur i Sverige, NOSAD (Nätverk för öppen källkod och data), Open Source Sweden, Red Hat och Stiftelsen för offentlig kod en heldagskonferens i Stockholm, Sverige. Efter evenemangen i Toulouse, Frankrike och Brno, Tjeckien, samlar vi nu europeiska aktörer i Nordens huvudstad för att fortsätta diskussionen om rollen för öppna teknologier i att forma vår gemensamma digitala framtid.

    JobTech Developments Maria Dalhage som är NOSAD:s projektägare kommer delta i panelen:

    Öppna källkods-gemenskaper och möjliggörande

    "Vad behövs för att offentliga myndigheter och privata aktörer ska kunna samarbeta kring gemensamma öppna källkodsprojekt?"

    Panelen kommer att diskutera behovet av nya färdigheter, OSPO:er och andra typer av nätverk och gemenskaper för att skapa förutsättningar för ett ökat samarbete.

    Panel:
    Maria Dalhage, Project Manager Open Data and Open Source, Arbetsförmedlingen
    Gjis Hillenius, Open Source Programme Office, European Commission
    Jan Ainali, Codebase Steward, Foundation for Public Code
    Gabriele Columbro, General Manager, Linux Foundation Europe

    Moderator:
    Leslie Hawthorne, Senior Manager,
    Open Source Program Office, Red Hat


    Plats: Internetstiftelsen, Hammarby kaj 10D, Stockholm, Sverige.

    [för mer info och anmälan (eng)](https://openforumeurope.org/event/oss-swedish-eu-presidency/)
date: 2023-03-28 13:25
taxonomy:
  category:
  - Event
publish_date: 2023-03-28 13:38
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/08.open-source-driving-the-european-digital-decade-april-18/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/08.open-source-driving-the-european-digital-decade-april-18/events_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/08.open-source-driving-the-european-digital-decade-april-18/events_blog_post.sv.md
tags:
- user::pages::03.news::08.open-source-driving-the-european-digital-decade-april-18::events_blog_post.sv.md
- pages::03.news::08.open-source-driving-the-european-digital-decade-april-18::events_blog_post.sv.md
- 03.news::08.open-source-driving-the-european-digital-decade-april-18::events_blog_post.sv.md
- 08.open-source-driving-the-european-digital-decade-april-18::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'OS Driving Digital Decade 18 april'
custom:
    title: 'OS Driving Digital Decade 18 april'
    date: '2023-04-18 10:00'
    endtime: '2023-04-18 17:30'
    short: 'Vi samlar europeiska aktörer i Nordens huvudstad för att fortsätta diskussionen om rollen för öppna teknologier i att forma vår gemensamma digitala framtid. JobTech Developments Maria Dalhage som är NOSAD:s projektägare kommer delta i en av panelerna under dagen '
    content: "Den 18 april 2023 anordnar OpenForum Europe, Stiftelsen för internetinfrastruktur i Sverige, NOSAD (Nätverk för öppen källkod och data), Open Source Sweden, Red Hat och Stiftelsen för offentlig kod en heldagskonferens i Stockholm, Sverige. Efter evenemangen i Toulouse, Frankrike och Brno, Tjeckien, samlar vi nu europeiska aktörer i Nordens huvudstad för att fortsätta diskussionen om rollen för öppna teknologier i att forma vår gemensamma digitala framtid.\n\nJobTech Developments Maria Dalhage som är NOSAD:s projektägare kommer delta i panelen:\n\nÖppna källkods-gemenskaper och möjliggörande\n\n\"Vad behövs för att offentliga myndigheter och privata aktörer ska kunna samarbeta kring gemensamma öppna källkodsprojekt?\"\n\nPanelen kommer att diskutera behovet av nya färdigheter, OSPO:er och andra typer av nätverk och gemenskaper för att skapa förutsättningar för ett ökat samarbete.\n\nPanel:\nMaria Dalhage, Project Manager Open Data and Open Source, Arbetsförmedlingen\nGjis Hillenius, Open Source Programme Office, European Commission\nJan Ainali, Codebase Steward, Foundation for Public Code\nGabriele Columbro, General Manager, Linux Foundation Europe\n\nModerator:\nLeslie Hawthorne, Senior Manager,\nOpen Source Program Office, Red Hat\n\n\nPlats: Internetstiftelsen, Hammarby kaj 10D, Stockholm, Sverige.\n\n[för mer info och anmälan (eng)](https://openforumeurope.org/event/oss-swedish-eu-presidency/)"
date: '2023-03-28 13:25'
taxonomy:
    category:
        - Event
publish_date: '2023-03-28 13:38'
---

