---
title: Kom-igång och öppet forum 27 maj
custom:
  title: Kom-igång och öppet forum 27 maj
  date: 2022-05-27 10:00
  endtime: 2022-05-27 11:00
  short: Vi vill göra det enkelt att använda våra apier, därför bjuder vi in dig och alla andra som är nyfikna på vad vi gör, till ett kom-igång webinarie.
  content: "###### Välkommen till maj månads kom-igång och öppet forum webbinarium \nVi vill göra det enkelt att använda våra apier, därför bjuder vi in dig och alla andra som är nyfikna på vad vi gör, till ett kom-igång webinarie.\n\nGenom att använda våra api:er bidrar du till att vi tillsammans kan jobba för en bättre och effektivare arbetsmarknad. \nOavsett om syftet är att labba på egen kammare, jobba med att utveckla nästa optimala matchningstjänst eller förbättra en befintlig produkt – är du varmt välkommen till oss.\nPå mötet kommer du att träffa utvecklarna bakom Jobtech Search/Jobtech Stream, Historiska annonser, Jobtech Taxonomy, JobAd Enrichments och Individdata, där vi driver ett spännande pilotprojekt.\nEfter presentationen finns det möjlighet att ställa frågor, ge feedback och framföra idéer och önskemål för nya tekniska lösningar från JobTech Development.\n\nVi serverar ingen färdig agenda för mötet utan innehållet styrs utifrån dina specifika intressen och behov enligt sk. ”open space”-modell.\nVarmt välkommen!\nJobTech Development team"
login:
  visibility_requires_access: false
published: true
taxonomy:
  category:
  - Event
  status:
  - Avslutad
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/kom-igang-och-oeppet-forum-webbinarium-27-maj/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/kom-igang-och-oeppet-forum-webbinarium-27-maj/events_blog_post.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/kom-igang-och-oeppet-forum-webbinarium-27-maj/events_blog_post.sv.md
tags:
- user::pages::03.news::kom-igang-och-oeppet-forum-webbinarium-27-maj::events_blog_post.sv.md
- pages::03.news::kom-igang-och-oeppet-forum-webbinarium-27-maj::events_blog_post.sv.md
- 03.news::kom-igang-och-oeppet-forum-webbinarium-27-maj::events_blog_post.sv.md
- kom-igang-och-oeppet-forum-webbinarium-27-maj::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'Kom-igång och öppet forum 27 maj'
custom:
    title: 'Kom-igång och öppet forum 27 maj'
    date: '2022-05-27 10:00'
    endtime: '2022-05-27 11:00'
    short: 'Vi vill göra det enkelt att använda våra apier, därför bjuder vi in dig och alla andra som är nyfikna på vad vi gör, till ett kom-igång webinarie.'
    content: "###### Välkommen till maj månads kom-igång och öppet forum webbinarium \nVi vill göra det enkelt att använda våra apier, därför bjuder vi in dig och alla andra som är nyfikna på vad vi gör, till ett kom-igång webinarie.\n\nGenom att använda våra api:er bidrar du till att vi tillsammans kan jobba för en bättre och effektivare arbetsmarknad. \nOavsett om syftet är att labba på egen kammare, jobba med att utveckla nästa optimala matchningstjänst eller förbättra en befintlig produkt – är du varmt välkommen till oss.\nPå mötet kommer du att träffa utvecklarna bakom Jobtech Search/Jobtech Stream, Historiska annonser, Jobtech Taxonomy, JobAd Enrichments och Individdata, där vi driver ett spännande pilotprojekt.\nEfter presentationen finns det möjlighet att ställa frågor, ge feedback och framföra idéer och önskemål för nya tekniska lösningar från JobTech Development.\n\nVi serverar ingen färdig agenda för mötet utan innehållet styrs utifrån dina specifika intressen och behov enligt sk. ”open space”-modell.\nVarmt välkommen!\nJobTech Development team"
login:
    visibility_requires_access: false
published: true
taxonomy:
    category:
        - Event
    status:
        - Avslutad
---

