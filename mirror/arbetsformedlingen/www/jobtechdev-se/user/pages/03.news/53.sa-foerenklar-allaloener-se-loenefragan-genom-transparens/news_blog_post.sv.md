---
title: Så förenklar allalöner.se lönefrågan genom transparens
custom:
  title: Så förenklar allalöner.se lönefrågan genom transparens
  short: 'JobTech Developments API:er och öppna data har hjälpt allalöner.se att skapa en struktur för yrkesbenämningar som speglar arbetsmarknaden. "Våra användare efterfrågar en större lönetransparens vilket vi jobbar för att uppnå.", säger Lennart Johansson, grundare av allalöner.se och antagningspoäng.se. '
  content: "![Lennart Johansson](./Lennart.jpg)\n_Lennart Johansson, grundare av allalöner.se och antagningspoäng.se_ \n\n**Vilken typ av data och öppna API:er använder ni från JobTech Development?** <BR>\n\"Vi använder oss av flera av era API:er och datamängder. [Jobtech Taxonomy](https://jobtechdev.se/sv/komponenter/jobtech-taxonomy)  har hjälpt oss att skapa en grundstruktur för yrkesbenämningar som blivit grundstommen i [allalöner.se](https://allaloner.se/).\nTaxonomin har även gjort att vi snabbt kan dra in relaterad information utifrån en SSYK4-kod och koppla ihop lönestatistik från SCB med våra yrken.\n\nMed [allalöner.se](https://allaloner.se/) vill vi skapa en ny typ av lönetransparens. SCB:s statistik fungerar som grunddata, men vi går även in på djupet och undersöker specifika löner för specifika arbetsgivare. Till en början har vi fokuserat på vårdbranschen och offentlig sektor. Taxonomin har hjälpt till att matcha yrkesbenämningar från Sveriges kommuner och regioner med SSYK4. En bra struktur för yrkesbenämningar hjälper oss att hålla ordning på den stora mängd löner vi själva samlar in och som skickas in till oss.\n\nVi använder även [Historiska annonser](https://jobtechdev.se/sv/komponenter/historical-ads) och [JobSearch Trends](https://jobtechdev.se/sv/komponenter/jobsearch-trends). De är intressanta datamängder som säger mycket om vilka yrken som är heta samt hur arbetsgivare sökt kandidater historiskt.”\n\n**På vilket sätt har API:erna hjälpt till att förbättra er tjänst?**<BR>\n\"Jag skulle säga att JobTech Developments API:er gör våra tjänster mer verklighetstrogna. I och med att de data ni har i era API:er är baserade på verkliga jobbannonser från arbetsmarknaden så speglar de verkligheten. För oss innebär det en bättre struktur av yrken som bättre matchar det användaren letar efter. API:erna hjälper oss också att berika yrken med närliggande information.\n\nIdag har vi kommit till ett läge där vi för vissa arbetsgivare kan förutse vilken lön en person bör få vid tillsättning i en roll. Vi tror att vi på sikt, när vi har en tillräckligt stor datamängd, kommer att kunna erbjuda våra användare en unik typ av lönetransparens som kommer att förenkla lönefrågan för arbetssökande. Ingen ska behöva känna sig lurad när man söker jobb, helt enkelt.\"\n\n**Hur ser du på framtida samarbeten med JobTech Development?**<BR>\n\"Jag hoppas att ni fortsätter det fantastiska arbete ni gör! Både i form av nya funktioner och när det kommer till stabilitet och dokumentation. Vi kommer att börja använda det nya API:et [JobEd Connect](https://jobtechdev.se/sv/komponenter/jobed-connect) för vår tjänst [antagningspoäng.se](https://antagningspoang.se/). Tjänsten närmar sig nu tio år i drift, och utöver antagningspoäng kommer vi nu även satsa på mer generell vägledning, där JobEd Connect blir relevant för att koppla ihop yrken och utbildningar.\n\nFör [allalöner.se](https://allaloner.se/) vill vi använda [JobSearch](https://jobtechdev.se/sv/komponenter/jobsearch)/[JobStream](https://jobtechdev.se/sv/komponenter/jobstream) fullt ut. Vi testar just nu med att ta fram den mest relevanta lönestatistiken för nya annonser som dyker upp och det ser mycket intressant ut. Våra användare efterfrågar en större lönetransparens vilket vi jobbar för att uppnå.\"\n\n**Besök [lönestatistik för yrken och företag - allalöner.se](https://allaloner.se/) och [antagningspoäng till högskoleutbildningar i Sverige](https://antagningspoang.se/). **\n"
  minititle: All löner.se
  title_block_2: h
  title_block_3: h
  ingress: 'JobTech Developments API:er och öppna data har hjälpt allalöner.se att skapa en struktur för yrkesbenämningar som speglar arbetsmarknaden. ”Våra användare efterfrågar en större lönetransparens vilket vi jobbar för att uppnå.", säger Lennart Johansson, grundare av allalöner.se och antagningspoäng.se. '
date: 2023-05-22 11:51
taxonomy:
  category:
  - Nyhet
  type:
  - Intervju
  status:
  - Avslutad
media_order: Lennart.jpg
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/53.sa-foerenklar-allaloener-se-loenefragan-genom-transparens/news_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/53.sa-foerenklar-allaloener-se-loenefragan-genom-transparens/news_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/53.sa-foerenklar-allaloener-se-loenefragan-genom-transparens/news_blog_post.sv.md
tags:
- user::pages::03.news::53.sa-foerenklar-allaloener-se-loenefragan-genom-transparens::news_blog_post.sv.md
- pages::03.news::53.sa-foerenklar-allaloener-se-loenefragan-genom-transparens::news_blog_post.sv.md
- 03.news::53.sa-foerenklar-allaloener-se-loenefragan-genom-transparens::news_blog_post.sv.md
- 53.sa-foerenklar-allaloener-se-loenefragan-genom-transparens::news_blog_post.sv.md
- news_blog_post.sv.md
---
---
title: 'Så förenklar allalöner.se lönefrågan genom transparens'
custom:
    title: 'Så förenklar allalöner.se lönefrågan genom transparens'
    short: 'JobTech Developments API:er och öppna data har hjälpt allalöner.se att skapa en struktur för yrkesbenämningar som speglar arbetsmarknaden. "Våra användare efterfrågar en större lönetransparens vilket vi jobbar för att uppnå.", säger Lennart Johansson, grundare av allalöner.se och antagningspoäng.se. '
    content: "![Lennart Johansson](./Lennart.jpg)\n_Lennart Johansson, grundare av allalöner.se och antagningspoäng.se_ \n\n**Vilken typ av data och öppna API:er använder ni från JobTech Development?** <BR>\n\"Vi använder oss av flera av era API:er och datamängder. [Jobtech Taxonomy](https://jobtechdev.se/sv/komponenter/jobtech-taxonomy)  har hjälpt oss att skapa en grundstruktur för yrkesbenämningar som blivit grundstommen i [allalöner.se](https://allaloner.se/).\nTaxonomin har även gjort att vi snabbt kan dra in relaterad information utifrån en SSYK4-kod och koppla ihop lönestatistik från SCB med våra yrken.\n\nMed [allalöner.se](https://allaloner.se/) vill vi skapa en ny typ av lönetransparens. SCB:s statistik fungerar som grunddata, men vi går även in på djupet och undersöker specifika löner för specifika arbetsgivare. Till en början har vi fokuserat på vårdbranschen och offentlig sektor. Taxonomin har hjälpt till att matcha yrkesbenämningar från Sveriges kommuner och regioner med SSYK4. En bra struktur för yrkesbenämningar hjälper oss att hålla ordning på den stora mängd löner vi själva samlar in och som skickas in till oss.\n\nVi använder även [Historiska annonser](https://jobtechdev.se/sv/komponenter/historical-ads) och [JobSearch Trends](https://jobtechdev.se/sv/komponenter/jobsearch-trends). De är intressanta datamängder som säger mycket om vilka yrken som är heta samt hur arbetsgivare sökt kandidater historiskt.”\n\n**På vilket sätt har API:erna hjälpt till att förbättra er tjänst?**<BR>\n\"Jag skulle säga att JobTech Developments API:er gör våra tjänster mer verklighetstrogna. I och med att de data ni har i era API:er är baserade på verkliga jobbannonser från arbetsmarknaden så speglar de verkligheten. För oss innebär det en bättre struktur av yrken som bättre matchar det användaren letar efter. API:erna hjälper oss också att berika yrken med närliggande information.\n\nIdag har vi kommit till ett läge där vi för vissa arbetsgivare kan förutse vilken lön en person bör få vid tillsättning i en roll. Vi tror att vi på sikt, när vi har en tillräckligt stor datamängd, kommer att kunna erbjuda våra användare en unik typ av lönetransparens som kommer att förenkla lönefrågan för arbetssökande. Ingen ska behöva känna sig lurad när man söker jobb, helt enkelt.\"\n\n**Hur ser du på framtida samarbeten med JobTech Development?**<BR>\n\"Jag hoppas att ni fortsätter det fantastiska arbete ni gör! Både i form av nya funktioner och när det kommer till stabilitet och dokumentation. Vi kommer att börja använda det nya API:et [JobEd Connect](https://jobtechdev.se/sv/komponenter/jobed-connect) för vår tjänst [antagningspoäng.se](https://antagningspoang.se/). Tjänsten närmar sig nu tio år i drift, och utöver antagningspoäng kommer vi nu även satsa på mer generell vägledning, där JobEd Connect blir relevant för att koppla ihop yrken och utbildningar.\n\nFör [allalöner.se](https://allaloner.se/) vill vi använda [JobSearch](https://jobtechdev.se/sv/komponenter/jobsearch)/[JobStream](https://jobtechdev.se/sv/komponenter/jobstream) fullt ut. Vi testar just nu med att ta fram den mest relevanta lönestatistiken för nya annonser som dyker upp och det ser mycket intressant ut. Våra användare efterfrågar en större lönetransparens vilket vi jobbar för att uppnå.\"\n\n**Besök [lönestatistik för yrken och företag - allalöner.se](https://allaloner.se/) och [antagningspoäng till högskoleutbildningar i Sverige](https://antagningspoang.se/). **\n"
    minititle: 'All löner.se'
    title_block_2: h
    title_block_3: h
    ingress: 'JobTech Developments API:er och öppna data har hjälpt allalöner.se att skapa en struktur för yrkesbenämningar som speglar arbetsmarknaden. ”Våra användare efterfrågar en större lönetransparens vilket vi jobbar för att uppnå.", säger Lennart Johansson, grundare av allalöner.se och antagningspoäng.se. '
date: '2023-05-22 11:51'
taxonomy:
    category:
        - Nyhet
    type:
        - Intervju
    status:
        - Avslutad
media_order: Lennart.jpg
---

