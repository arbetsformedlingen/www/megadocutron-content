---
title: 'Ny teknik stöttar det livslånga lärandet  '
custom:
  title: 'Ny teknik stöttar det livslånga lärandet  '
  short: Arbetsförmedlingens generaldirektör Maria Mindhammar deltog den 9 maj i ett panelsamtal på temat Jobtech vs Edtech, anordnat av intresseföreningen Swedish JobTech och branschorganisationen Swedish Edtech Industry. Vi passade på att ställa några frågor till generaldirektören.
  content: "**Arbetsförmedlingens generaldirektör Maria Mindhammar deltog den 9 maj i ett panelsamtal på temat Jobtech vs Edtech, anordnat av intresseföreningen Swedish JobTech och branschorganisationen Swedish Edtech Industry. Vi passade på att ställa några frågor till generaldirektören.**\n\n![Generaldirektör Maria Mindhammar](./maria-mindhammar-2278.jpg)\n_Generaldirektör Maria Mindhammar_\n\n**Hur arbetar Arbetsförmedlingen för att lösa utmaningar inom kompetensförsörjning och kompetensbrist på arbetsmarknaden med hjälp av ny teknik?** <BR>\n\"Arbetsförmedlingen har jobbat länge med datadrivna metoder för att ge bättre förutsättningar till matchningsaktörerna i utvecklingen av matchningstjänster. För mer än fem år sedan bildade vi enheten Jobtech som förvaltar den öppna plattformen JobTech Development där arbetsmarknadsdata tillgängliggörs, delas och nyttiggörs, helt kostnadsfritt, för att gynna alla aktörer på arbetsmarknaden. Plattformen samlar idag drygt 200 företag och organisationer i ett digitalt ekosystem.\"\n\n**Hur ser utvecklingen ut?** <BR>\n\"I dag har Arbetsförmedlingen en samordnande roll i ett unikt regeringsuppdrag som sträcker sig över tre politikområden, för att utveckla en sammanhållen datainfrastruktur för kompetensförsörjning och livslångt lärande. Målet är att stärka förutsättningarna för myndigheter och andra aktörer att skapa och tillhandahålla digitala tjänster som stärker individens ställning på arbetsmarknaden, samtidigt som privat och offentlig sektors kompetensbehov tillgodoses.\" \n  \n**Hur kan kopplingen av data mellan två områden – arbetsmarknad och utbildning – ge bättre förutsättningar för en hållbar matchning på arbetsmarknaden? **<BR>\n\"Genom att sammanföra de två dataområdena har vi identifierat ett nytt teknikområde som vi kallar JobEdTech. Genom att koppla utbildningar och kompetenser till olika yrken kan vi stötta det livslånga lärandet, som är en förutsättning för Sveriges framtida kompetensförsörjning.\"\n\n**Varför är just Arbetsförmedlingen plattformsledare för jobtechdev.se?**<BR>\n\"Arbetsförmedlingen har, som den största aktören på marknaden, förtroendet som krävs för att leda arbetet. Vi har som myndighet ett långsiktigt perspektiv och kan hjälpa andra att utveckla den öppna digitala infrastrukturen med flera gemensamma tekniska strukturer, lösningar och standarder. Vi kan också uppmuntra till samarbeten, datadelning och idéer för innovation. Genom att samverka och dela data kan vi tillsammans skapa en hållbar arbetsmarknad för alla.\"\n\n[Här kan du se panelsamtalet ”Jobtech vs Edtech” i sin helhet.](https://www.youtube.com/watch?app=desktop&v=3V2SSbXUlIg)\n\n"
  infobox: "**Läs mer om JobTech Developments pågående projekt:**\n\n* [**Individdata och dataportabilitet**](https://jobtechdev.se/sv/komponenter/individdata-och-dataportabilitet)\n* [**Taxonomi och begreppsstruktur**](https://jobtechdev.se/sv/komponenter/taxonomi-och-begreppsstruktur)\n* [**Digital vägledning och länkade data**](https://jobtechdev.se/sv/komponenter/digital-vaegledning-och-laenkad-data) \n* [**Digital yrkesvägledning**](https://jobtechdev.se/sv/komponenter/digital-yrkesvaegledning)\n* [**JobEd Connect (API)**](https://jobtechdev.se/sv/komponenter/jobed-connect)"
  minititle: Ny teknink
date: 2023-05-15 11:52
taxonomy:
  category:
  - Nyhet
  type:
  - Intervju
media_order: maria-mindhammar-2278.jpg
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/52.ny-teknik-stoettar-det-livslanga-laerandet/news_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/52.ny-teknik-stoettar-det-livslanga-laerandet/news_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/52.ny-teknik-stoettar-det-livslanga-laerandet/news_blog_post.sv.md
tags:
- user::pages::03.news::52.ny-teknik-stoettar-det-livslanga-laerandet::news_blog_post.sv.md
- pages::03.news::52.ny-teknik-stoettar-det-livslanga-laerandet::news_blog_post.sv.md
- 03.news::52.ny-teknik-stoettar-det-livslanga-laerandet::news_blog_post.sv.md
- 52.ny-teknik-stoettar-det-livslanga-laerandet::news_blog_post.sv.md
- news_blog_post.sv.md
---
---
title: 'Ny teknik stöttar det livslånga lärandet  '
custom:
    title: 'Ny teknik stöttar det livslånga lärandet  '
    short: 'Arbetsförmedlingens generaldirektör Maria Mindhammar deltog den 9 maj i ett panelsamtal på temat Jobtech vs Edtech, anordnat av intresseföreningen Swedish JobTech och branschorganisationen Swedish Edtech Industry. Vi passade på att ställa några frågor till generaldirektören.'
    content: "**Arbetsförmedlingens generaldirektör Maria Mindhammar deltog den 9 maj i ett panelsamtal på temat Jobtech vs Edtech, anordnat av intresseföreningen Swedish JobTech och branschorganisationen Swedish Edtech Industry. Vi passade på att ställa några frågor till generaldirektören.**\n\n![Generaldirektör Maria Mindhammar](./maria-mindhammar-2278.jpg)\n_Generaldirektör Maria Mindhammar_\n\n**Hur arbetar Arbetsförmedlingen för att lösa utmaningar inom kompetensförsörjning och kompetensbrist på arbetsmarknaden med hjälp av ny teknik?** <BR>\n\"Arbetsförmedlingen har jobbat länge med datadrivna metoder för att ge bättre förutsättningar till matchningsaktörerna i utvecklingen av matchningstjänster. För mer än fem år sedan bildade vi enheten Jobtech som förvaltar den öppna plattformen JobTech Development där arbetsmarknadsdata tillgängliggörs, delas och nyttiggörs, helt kostnadsfritt, för att gynna alla aktörer på arbetsmarknaden. Plattformen samlar idag drygt 200 företag och organisationer i ett digitalt ekosystem.\"\n\n**Hur ser utvecklingen ut?** <BR>\n\"I dag har Arbetsförmedlingen en samordnande roll i ett unikt regeringsuppdrag som sträcker sig över tre politikområden, för att utveckla en sammanhållen datainfrastruktur för kompetensförsörjning och livslångt lärande. Målet är att stärka förutsättningarna för myndigheter och andra aktörer att skapa och tillhandahålla digitala tjänster som stärker individens ställning på arbetsmarknaden, samtidigt som privat och offentlig sektors kompetensbehov tillgodoses.\" \n  \n**Hur kan kopplingen av data mellan två områden – arbetsmarknad och utbildning – ge bättre förutsättningar för en hållbar matchning på arbetsmarknaden? **<BR>\n\"Genom att sammanföra de två dataområdena har vi identifierat ett nytt teknikområde som vi kallar JobEdTech. Genom att koppla utbildningar och kompetenser till olika yrken kan vi stötta det livslånga lärandet, som är en förutsättning för Sveriges framtida kompetensförsörjning.\"\n\n**Varför är just Arbetsförmedlingen plattformsledare för jobtechdev.se?**<BR>\n\"Arbetsförmedlingen har, som den största aktören på marknaden, förtroendet som krävs för att leda arbetet. Vi har som myndighet ett långsiktigt perspektiv och kan hjälpa andra att utveckla den öppna digitala infrastrukturen med flera gemensamma tekniska strukturer, lösningar och standarder. Vi kan också uppmuntra till samarbeten, datadelning och idéer för innovation. Genom att samverka och dela data kan vi tillsammans skapa en hållbar arbetsmarknad för alla.\"\n\n[Här kan du se panelsamtalet ”Jobtech vs Edtech” i sin helhet.](https://www.youtube.com/watch?app=desktop&v=3V2SSbXUlIg)\n\n"
    infobox: "**Läs mer om JobTech Developments pågående projekt:**\n\n* [**Individdata och dataportabilitet**](https://jobtechdev.se/sv/komponenter/individdata-och-dataportabilitet)\n* [**Taxonomi och begreppsstruktur**](https://jobtechdev.se/sv/komponenter/taxonomi-och-begreppsstruktur)\n* [**Digital vägledning och länkade data**](https://jobtechdev.se/sv/komponenter/digital-vaegledning-och-laenkad-data) \n* [**Digital yrkesvägledning**](https://jobtechdev.se/sv/komponenter/digital-yrkesvaegledning)\n* [**JobEd Connect (API)**](https://jobtechdev.se/sv/komponenter/jobed-connect)"
    minititle: 'Ny teknink'
date: '2023-05-15 11:52'
taxonomy:
    category:
        - Nyhet
    type:
        - Intervju
media_order: maria-mindhammar-2278.jpg
---

