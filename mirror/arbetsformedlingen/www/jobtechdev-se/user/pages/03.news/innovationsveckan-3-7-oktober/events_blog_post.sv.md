---
title: Innovationsveckan 3-7 oktober
custom:
  date: 2022-10-03 09:00
  endtime: 2022-10-07 16:30
  short: 'Programmet är späckat med presentationer, föreläsningar och seminarium – de flesta är digitala – från aktörer över hela Sverige. '
  content: "###### Missa inte att anmäla dig till Innovationveckan den 3-7 oktobr som arrangeras av DIGG, Vinnova och SKR bland flera. <BR><BR>\n\nProgrammet, 3–7 oktober, är späckat med presentationer, föreläsningar och seminarium – de flesta är digitala – från aktörer över hela Sverige. \n\nInnovationsveckan är vigd åt innovation av och för offentlig sektor. Under denna vecka fokuseras det på att dela och sprida smarta lösningar mellan olika branscher och aktörer. \n\nEtt smakprov är \"Ena och byggblocken Mina ärenden och Mina ombud skapa konkret nytta för kommuner och dess invånare\" [seminariespår.](https://innovationsveckan.nu/program/sa-kan-ena-och-byggblocken-mina-arenden-och-mina-ombud-skapa-konkret-nytta-for-kommuner-och-dess-invanare/)\n\nVarmt välkommen att delta. \n\n[Till anmälan](https://innovationsveckan.nu/program/?tribe_paged=1&tribe_event_display=list)\n\n"
  title: Innovationsveckan 3-7 oktober
taxonomy:
  category:
  - Event
  status:
  - Avslutad
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/innovationsveckan-3-7-oktober/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/innovationsveckan-3-7-oktober/events_blog_post.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/innovationsveckan-3-7-oktober/events_blog_post.sv.md
tags:
- user::pages::03.news::innovationsveckan-3-7-oktober::events_blog_post.sv.md
- pages::03.news::innovationsveckan-3-7-oktober::events_blog_post.sv.md
- 03.news::innovationsveckan-3-7-oktober::events_blog_post.sv.md
- innovationsveckan-3-7-oktober::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'Innovationsveckan 3-7 oktober'
custom:
    date: '2022-10-03 09:00'
    endtime: '2022-10-07 16:30'
    short: 'Programmet är späckat med presentationer, föreläsningar och seminarium – de flesta är digitala – från aktörer över hela Sverige. '
    content: "###### Missa inte att anmäla dig till Innovationveckan den 3-7 oktobr som arrangeras av DIGG, Vinnova och SKR bland flera. <BR><BR>\n\nProgrammet, 3–7 oktober, är späckat med presentationer, föreläsningar och seminarium – de flesta är digitala – från aktörer över hela Sverige. \n\nInnovationsveckan är vigd åt innovation av och för offentlig sektor. Under denna vecka fokuseras det på att dela och sprida smarta lösningar mellan olika branscher och aktörer. \n\nEtt smakprov är \"Ena och byggblocken Mina ärenden och Mina ombud skapa konkret nytta för kommuner och dess invånare\" [seminariespår.](https://innovationsveckan.nu/program/sa-kan-ena-och-byggblocken-mina-arenden-och-mina-ombud-skapa-konkret-nytta-for-kommuner-och-dess-invanare/)\n\nVarmt välkommen att delta. \n\n[Till anmälan](https://innovationsveckan.nu/program/?tribe_paged=1&tribe_event_display=list)\n\n"
    title: 'Innovationsveckan 3-7 oktober'
taxonomy:
    category:
        - Event
    status:
        - Avslutad
---

