---
title: TRR’s Clients Benefit from JobTech's APIs
custom:
  title: TRR’s Clients Benefit from JobTech's APIs
  short: 'Sweden''s largest job security council TRR is using JobTech Development''s APIs to improve the accuracy of the matching rates between clinents and hiring managers. We talked to Catherine Ewerman, an IT and Digitalisation Product Owner at TRR, about the benefits of this mutual collaboration and her insights on how it can be further improved. '
  content: |-
    **Sweden's largest job security council TRR (in Swedish: Trygghetsrådet) is using JobTech Development's APIs to improve the accuracy of the matching rates between clients and recruiting employers. We have had the opportunity to talk to Catherine Ewerman, an IT and Digitalisation Product Owner at TRR, about the benefits of this mutual collaboration and her insights on its further development. **

    > "We have had a collaboration with JobTech Development for over a year. It has turned out to be both a good and beneficial collaboration, which we can see developing in a long-term perspective, bringing improvements, and added value for both parties. One of the strongest advantages with the JobTech's APIs is that they are developed of very high quality, which gives us good prerequisites to enrich our services with diverse functions.", says Catherine Ewerman.


    ![Catherine Ewerman, IT and Digitalisation Product Owner at TRR](Catherine%20Ewerman.TRR-3.PNG)
    _Catherine Ewerman, IT and Digitalisation Product Owner at TRR_ <BR>

    **What kind of data do you use from JobTech Development?** <BR>
    We have implemented the occupational structure, i.e., [JobTech Taxonomy](https://jobtechdev.se/en/components/jobtech-taxonomy), and we are also using [JobAd Enrichments](https://jobtechdev.se/en/components/jobad-enrichments), which displays job vacancies in our service tool. Shortly, we are planning to expand the usage of the API [JobSearch](https://jobtechdev.se/en/components/jobsearch).

    **How have the APIs helped to improve your service tool?** <BR>
    By receiving good quality data about professional categories and occupational roles, we have made it easier for our clients to create a broader and more accurate profile in the occupational group they want to be found and matched to by employers. In our matching service, the nomenclature* facilitates our work to match candidates in getting in touch with hiring managers in different ways. Using the same nomenclature, we also gain knowledge in a long-term perspective about civil servants’ employment transition in the labour market. <BR>
    At the same time, we gain valuable knowledge about our clients’ (job seekers) employment transition journey: from the moment they apply for our counselling and support through the possible “re-skilling” period and career transition journey until the moment they land a new job. Thus, our awareness raises about the career transition journeys in the labour market. It contributes further to improving our analytical skills to address such changes better.

    **Are there other areas where occupational classification comes in handy?** <BR>
    We are often getting inquiries about ongoing trends, referring to various occupational groups, f.e., which industries are recruiting engineers, where journalists can find new solutions, and whether a significant number of economists has been made redundant lately. Previously, we could not answer these questions since we did not have the prerequisites to organize the occupational classification in a structured and categorized way. Now, we can do that by using the new occupational classification. Another advantage is that TRR can use data about occupations during conversations with clients (job seekers) for various purposes. It can indicate how it has gone for other clients in the same professional categories, what kind of companies are hiring from the occupational group the client belongs to, and mapping trends for various occupational groups in demand.

    **What are you hoping to achieve with the new service?** <BR>
    We would like to improve the user experience so that our clients (job seekers) can more easily find relevant job advertisements. In addition, we would like to get more in-depth insights into which competencies and skills are in demand by employers in a specific occupation.

    **How do you look at future collaborations with JobTech Development?** <BR>
    TRR and JobTech have a mutual interest in continuing working towards establishing a common terminology in the labour market. Our main goal is to facilitate and improve the matching between our clients (job seekers) and job vacancies. We would like, of course, to contribute with our expertise about career transitions in the labour market. Identifying changes in our field will lead to nomenclature updates on an ongoing basis. We will continue further digitalizing our services and operations by transferring knowledge about occupational roles, which our clients lack today. It will help us early identify new occupational roles and get in-depth insights into changes occurring among certain occupational categories in the labour market.


    _[TRR](https://www.trr.se/summary-in-english/about-us/this-is-trr/#TRRinbrief) is a non-profit foundation based on a collective agreement between the Confederation of Swedish Enterprise and PTK (a joint organisation that negotiates collective agreements and provides training on behalf of 25 member unions). TRR has been operating successfully since 1974. TRR works with labour market transitions contributing to learning and mobility in the Swedish labour market. Its activities are aimed at companies in the private labour market, trade union representatives, and company employees._


    _* Nomenclature refers broadly to taxonomy. _
date: 2022-11-15 13:13
taxonomy:
  category:
  - News
  type:
  - Interview
media_order: Catherine Ewerman.TRR-3.PNG
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/16.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er/news_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/16.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er/news_blog_post.en.md
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/16.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er/news_blog_post.en.md
tags:
- user::pages::03.news::16.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er::news_blog_post.en.md
- pages::03.news::16.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er::news_blog_post.en.md
- 03.news::16.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er::news_blog_post.en.md
- 16.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er::news_blog_post.en.md
- news_blog_post.en.md
---
---
title: 'TRR’s Clients Benefit from JobTech''s APIs'
custom:
    title: 'TRR’s Clients Benefit from JobTech''s APIs'
    short: 'Sweden''s largest job security council TRR is using JobTech Development''s APIs to improve the accuracy of the matching rates between clinents and hiring managers. We talked to Catherine Ewerman, an IT and Digitalisation Product Owner at TRR, about the benefits of this mutual collaboration and her insights on how it can be further improved. '
    content: "**Sweden's largest job security council TRR (in Swedish: Trygghetsrådet) is using JobTech Development's APIs to improve the accuracy of the matching rates between clients and recruiting employers. We have had the opportunity to talk to Catherine Ewerman, an IT and Digitalisation Product Owner at TRR, about the benefits of this mutual collaboration and her insights on its further development. **\n\n> \"We have had a collaboration with JobTech Development for over a year. It has turned out to be both a good and beneficial collaboration, which we can see developing in a long-term perspective, bringing improvements, and added value for both parties. One of the strongest advantages with the JobTech's APIs is that they are developed of very high quality, which gives us good prerequisites to enrich our services with diverse functions.\", says Catherine Ewerman.\n\n\n![Catherine Ewerman, IT and Digitalisation Product Owner at TRR](Catherine%20Ewerman.TRR-3.PNG)\n_Catherine Ewerman, IT and Digitalisation Product Owner at TRR_ <BR>\n\n**What kind of data do you use from JobTech Development?** <BR>\nWe have implemented the occupational structure, i.e., [JobTech Taxonomy](https://jobtechdev.se/en/components/jobtech-taxonomy), and we are also using [JobAd Enrichments](https://jobtechdev.se/en/components/jobad-enrichments), which displays job vacancies in our service tool. Shortly, we are planning to expand the usage of the API [JobSearch](https://jobtechdev.se/en/components/jobsearch).\n\n**How have the APIs helped to improve your service tool?** <BR>\nBy receiving good quality data about professional categories and occupational roles, we have made it easier for our clients to create a broader and more accurate profile in the occupational group they want to be found and matched to by employers. In our matching service, the nomenclature* facilitates our work to match candidates in getting in touch with hiring managers in different ways. Using the same nomenclature, we also gain knowledge in a long-term perspective about civil servants’ employment transition in the labour market. <BR>\nAt the same time, we gain valuable knowledge about our clients’ (job seekers) employment transition journey: from the moment they apply for our counselling and support through the possible “re-skilling” period and career transition journey until the moment they land a new job. Thus, our awareness raises about the career transition journeys in the labour market. It contributes further to improving our analytical skills to address such changes better.\n\n**Are there other areas where occupational classification comes in handy?** <BR>\nWe are often getting inquiries about ongoing trends, referring to various occupational groups, f.e., which industries are recruiting engineers, where journalists can find new solutions, and whether a significant number of economists has been made redundant lately. Previously, we could not answer these questions since we did not have the prerequisites to organize the occupational classification in a structured and categorized way. Now, we can do that by using the new occupational classification. Another advantage is that TRR can use data about occupations during conversations with clients (job seekers) for various purposes. It can indicate how it has gone for other clients in the same professional categories, what kind of companies are hiring from the occupational group the client belongs to, and mapping trends for various occupational groups in demand.\n\n**What are you hoping to achieve with the new service?** <BR>\nWe would like to improve the user experience so that our clients (job seekers) can more easily find relevant job advertisements. In addition, we would like to get more in-depth insights into which competencies and skills are in demand by employers in a specific occupation.\n\n**How do you look at future collaborations with JobTech Development?** <BR>\nTRR and JobTech have a mutual interest in continuing working towards establishing a common terminology in the labour market. Our main goal is to facilitate and improve the matching between our clients (job seekers) and job vacancies. We would like, of course, to contribute with our expertise about career transitions in the labour market. Identifying changes in our field will lead to nomenclature updates on an ongoing basis. We will continue further digitalizing our services and operations by transferring knowledge about occupational roles, which our clients lack today. It will help us early identify new occupational roles and get in-depth insights into changes occurring among certain occupational categories in the labour market.\n\n\n_[TRR](https://www.trr.se/summary-in-english/about-us/this-is-trr/#TRRinbrief) is a non-profit foundation based on a collective agreement between the Confederation of Swedish Enterprise and PTK (a joint organisation that negotiates collective agreements and provides training on behalf of 25 member unions). TRR has been operating successfully since 1974. TRR works with labour market transitions contributing to learning and mobility in the Swedish labour market. Its activities are aimed at companies in the private labour market, trade union representatives, and company employees._\n\n\n_* Nomenclature refers broadly to taxonomy. _"
date: '2022-11-15 13:13'
taxonomy:
    category:
        - News
    type:
        - Interview
media_order: 'Catherine Ewerman.TRR-3.PNG'
---

