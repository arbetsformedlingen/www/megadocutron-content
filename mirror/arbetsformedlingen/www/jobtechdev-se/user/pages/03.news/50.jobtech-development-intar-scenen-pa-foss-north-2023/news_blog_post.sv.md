---
title: JobTech Development intar scenen på Foss-north 2023
custom:
  title: JobTech Development intar scenen på Foss-north 2023
  short: Årets konferens kommer att ägnas åt bland annat de sociala och juridiska aspekterna av fri programvara, samt presentera praxis från Skandinavien.
  content: "**JobTech Development kommer att delta vid årets Foss-north 2023. Tvådagarskonferensen äger rum 24–25 april på Chalmers Conference Centre i Göteborg, och föregås av community-dagen 23 april. Årets evenemang kommer bland annat att fokusera på politiska, juridiska och sociala utmaningar relaterade till fri programvara, samt presentera praxis från Skandinavien.**\n\nJonas Södergren, teknikansvarig och utvecklare på JobTech Development, talar på årets Foss-north den 25 april, 11.00–12.00 på temat **”Arbetsförmedlingens erfarenheter av jobtech och öppen källkod:  att dela 400 källkodsrepon och exempel på nya samarbeten”**.\n\n> \"Under de senaste åren har vi delat 400 olika källkodsrepon och utforskat möjligheterna med öppen källkod för att förbättra våra leveranser och verksamhet genom användningen öppna data API:er. Lärdomarna på vägen, från arbetet med öppen programvara inom området jobtech, har hjälpt oss att förbättra vårt arbetssätt och uppnå bättre resultat. All vår kod finns fritt tillgänglig på [Gitlab](https://gitlab.com/arbetsformedlingen).\" säger Jonas Södergren.\n> \n\nJonas kommer att dela av med sig av insikter från framgångsrika initiativ, utmaningar och lyckade samarbeten med andra organisationer inom området jobtech och öppen källkod. Genom att visa praktiska exempel ges publiken en tydlig bild av hur öppen källkod kan användas för att göra offentlig sektor mer öppen och effektiv."
  infobox: "**Om Foss-north ** <BR>\nFoss-north är en kostnadsfri konferens med fokus på öppen källkod ur ett tekniskt perspektiv. Evenemanget äger rum i Göteborg och samlar nordiska communityer kring fri programvara och öppen källkod för att dela kunskap och utbyta idéer. <BR>\n[Mer om konferensen Foss-north 2023.](https://foss-north.se/2023/index.html)\n\n**Om Jonas Södergren** <BR>\nJonas Södergren har en lång bakgrund på Arbetsförmedlingen, de senaste åren som tekniskt ansvarig på JobTech Development. Med sin långa erfarenhet har han haft en avgörande roll för Arbetsförmedlingens arbete för en datadriven arbetsmarknad, genom leveranser av öppna data, öppen källkod och öppna API:er. "
date: 2023-04-18 14:51
taxonomy:
  category:
  - Nyhet
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/03.news/50.jobtech-development-intar-scenen-pa-foss-north-2023/news_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/03.news/50.jobtech-development-intar-scenen-pa-foss-north-2023/news_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/50.jobtech-development-intar-scenen-pa-foss-north-2023/news_blog_post.sv.md
tags:
- user::pages::03.news::50.jobtech-development-intar-scenen-pa-foss-north-2023::news_blog_post.sv.md
- pages::03.news::50.jobtech-development-intar-scenen-pa-foss-north-2023::news_blog_post.sv.md
- 03.news::50.jobtech-development-intar-scenen-pa-foss-north-2023::news_blog_post.sv.md
- 50.jobtech-development-intar-scenen-pa-foss-north-2023::news_blog_post.sv.md
- news_blog_post.sv.md
---
---
title: 'JobTech Development intar scenen på Foss-north 2023'
custom:
    title: 'JobTech Development intar scenen på Foss-north 2023'
    short: 'Årets konferens kommer att ägnas åt bland annat de sociala och juridiska aspekterna av fri programvara, samt presentera praxis från Skandinavien.'
    content: "**JobTech Development kommer att delta vid årets Foss-north 2023. Tvådagarskonferensen äger rum 24–25 april på Chalmers Conference Centre i Göteborg, och föregås av community-dagen 23 april. Årets evenemang kommer bland annat att fokusera på politiska, juridiska och sociala utmaningar relaterade till fri programvara, samt presentera praxis från Skandinavien.**\n\nJonas Södergren, teknikansvarig och utvecklare på JobTech Development, talar på årets Foss-north den 25 april, 11.00–12.00 på temat **”Arbetsförmedlingens erfarenheter av jobtech och öppen källkod:  att dela 400 källkodsrepon och exempel på nya samarbeten”**.\n\n> \"Under de senaste åren har vi delat 400 olika källkodsrepon och utforskat möjligheterna med öppen källkod för att förbättra våra leveranser och verksamhet genom användningen öppna data API:er. Lärdomarna på vägen, från arbetet med öppen programvara inom området jobtech, har hjälpt oss att förbättra vårt arbetssätt och uppnå bättre resultat. All vår kod finns fritt tillgänglig på [Gitlab](https://gitlab.com/arbetsformedlingen).\" säger Jonas Södergren.\n> \n\nJonas kommer att dela av med sig av insikter från framgångsrika initiativ, utmaningar och lyckade samarbeten med andra organisationer inom området jobtech och öppen källkod. Genom att visa praktiska exempel ges publiken en tydlig bild av hur öppen källkod kan användas för att göra offentlig sektor mer öppen och effektiv."
    infobox: "**Om Foss-north ** <BR>\nFoss-north är en kostnadsfri konferens med fokus på öppen källkod ur ett tekniskt perspektiv. Evenemanget äger rum i Göteborg och samlar nordiska communityer kring fri programvara och öppen källkod för att dela kunskap och utbyta idéer. <BR>\n[Mer om konferensen Foss-north 2023.](https://foss-north.se/2023/index.html)\n\n**Om Jonas Södergren** <BR>\nJonas Södergren har en lång bakgrund på Arbetsförmedlingen, de senaste åren som tekniskt ansvarig på JobTech Development. Med sin långa erfarenhet har han haft en avgörande roll för Arbetsförmedlingens arbete för en datadriven arbetsmarknad, genom leveranser av öppna data, öppen källkod och öppna API:er. "
date: '2023-04-18 14:51'
taxonomy:
    category:
        - Nyhet
---

