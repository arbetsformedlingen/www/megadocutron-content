---
simplesearch:
  process: false
title: Email sent
cache_enable: false
process:
  twig: true
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/09.form/form/thankyou/email-sent/formdata.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/09.form/form/thankyou/email-sent/formdata.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/09.form/form/thankyou/email-sent/formdata.sv.md
tags:
- user::pages::09.form::form::thankyou::email-sent::formdata.sv.md
- pages::09.form::form::thankyou::email-sent::formdata.sv.md
- 09.form::form::thankyou::email-sent::formdata.sv.md
- form::thankyou::email-sent::formdata.sv.md
- thankyou::email-sent::formdata.sv.md
- email-sent::formdata.sv.md
- formdata.sv.md
---
---
simplesearch:
    process: false
title: 'Email sent'
cache_enable: false
process:
    twig: true
---

