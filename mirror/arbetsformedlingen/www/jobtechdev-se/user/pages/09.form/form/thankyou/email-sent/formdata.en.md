---
simplesearch:
  process: false
title: Email sent
cache_enable: false
process:
  twig: true
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/09.form/form/thankyou/email-sent/formdata.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/09.form/form/thankyou/email-sent/formdata.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/09.form/form/thankyou/email-sent/formdata.en.md
tags:
- user::pages::09.form::form::thankyou::email-sent::formdata.en.md
- pages::09.form::form::thankyou::email-sent::formdata.en.md
- 09.form::form::thankyou::email-sent::formdata.en.md
- form::thankyou::email-sent::formdata.en.md
- thankyou::email-sent::formdata.en.md
- email-sent::formdata.en.md
- formdata.en.md
---
---
simplesearch:
    process: false
title: 'Email sent'
cache_enable: false
process:
    twig: true
---

