---
simplesearch:
  process: false
title: Form
form:
  id: newsletter
  layout: newsletter
  name: newsletter
  fields:
    honeypot:
      type: honeypot
    email:
      label: Email address
      placeholder: Email address
      type: email
      validate:
        required: true
    checkbox:
      label: I agree that Arbetsförmedlingen stores the above information and shares information to me via email
      type: checkbox
      validate:
        required: true
  buttons:
    submit:
      type: submit
      value: Subscribe
  process:
    email:
      from: '{{ config.plugins.email.from }}'
      to:
      - '{{ config.plugins.email.to }}'
      - '{{ form.value.email }}'
      subject: '[Newsletter] {{ form.value.email|e }}'
      body: '{% include ''partials/mailnewslettercontent.html.twig'' %} {% include ''forms/data.html.twig'' %} {{form|s}}'
    save:
      fileprefix: feedback-
      dateformat: Ymd-His-u
      extension: txt
      body: '{% include ''forms/data.txt.twig'' %}}'
    display: thankyou
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/09.form/form/formdata.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/09.form/form/formdata.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/09.form/form/formdata.en.md
tags:
- user::pages::09.form::form::formdata.en.md
- pages::09.form::form::formdata.en.md
- 09.form::form::formdata.en.md
- form::formdata.en.md
- formdata.en.md
---
---
simplesearch:
    process: false
title: Form
form:
    id: newsletter
    layout: newsletter
    name: newsletter
    fields:
        honeypot:
            type: honeypot
        email:
            label: 'Email address'
            placeholder: 'Email address'
            type: email
            validate:
                required: true
        checkbox:
            label: 'I agree that Arbetsförmedlingen stores the above information and shares information to me via email'
            type: checkbox
            validate:
                required: true
    buttons:
        submit:
            type: submit
            value: Subscribe
    process:
        email:
            from: '{{ config.plugins.email.from }}'
            to:
                - '{{ config.plugins.email.to }}'
                - '{{ form.value.email }}'
            subject: '[Newsletter] {{ form.value.email|e }}'
            body: '{% include ''partials/mailnewslettercontent.html.twig'' %} {% include ''forms/data.html.twig'' %} {{form|s}}'
        save:
            fileprefix: feedback-
            dateformat: Ymd-His-u
            extension: txt
            body: '{% include ''forms/data.txt.twig'' %}}'
        display: thankyou
---

