---
title: Tillgänglighet
custom:
  content: "**Hur tillgänglig är webbplatsen?**  \nVi är medvetna om att delar av webbplatsen inte är helt tillgängliga.\nI avsnittet om innehåll som inte är tillgängligt listar vi kända brister i tillgänglighet.\n\n**Kontakta oss om du hittar fler brister**  \nVi strävar hela tiden efter att förbättra webbplatsens tillgänglighet. Om du upptäcker problem som inte är beskrivna, eller om du anser att vi inte uppfyller lagens krav, meddela oss så att vi får veta att problemet finns.\n[Lämna synpunkter på digital tillgänglighet här](mailto:jobtechdev@arbetsformedlingen.se)\n\n**Kontakta tillsynsmyndigheten**  \nMyndigheten för digital förvaltning har ansvaret för tillsyn för lagen om tillgänglighet till digital offentlig service. Om du inte är nöjd med hur vi hanterar dina synpunkter kan du [kontakta Myndigheten för digital förvaltning](https://www.digg.se/tdosanmalan) och berätta det.\n\n**Teknisk information om webbplatsens tillgänglighet**  \nDen här webbplatsen är inte förenlig med [lagen om tillgänglighet till digital offentlig service](https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/lag-20181937-om-tillganglighet-till-digital_sfs-2018-1937), på grund av de brister som beskrivs nedan.\n\n**Innehåll som inte är tillgängligt**  \nVi är medvetna om följande brister som inte följer lagkraven:\n* Allt innehåll är inte responsivt\n* Det finns brister i kontrast\n\n**Hur vi har testat webbplatsen**  \nVi har gjort en självskattning (intern testning) av jobtechdev.se.\n\n**Hur vi jobbar med digital tillgänglighet**  \nVi strävar efter att webbplatsen ska kunna uppfattas, hanteras och förstås av alla användare, oavsett behov eller funktionsnedsättning och oberoende av vilka hjälpmedel du använder. Vi ska åtminstone uppnå grundläggande tillgänglighet genom att följa WCAG 2.1 på nivå AA.\n\n"
  menu:
  - title: Tillgänglighet
    url: /sv/om-webbplatsen/availability
  - title: Cookie policy
    url: /sv/om-webbplatsen/cookies
  title: Tillgänglighetsredogörelse
  ingress: Arbetsförmedlingen (JobTech Development) är ägare till den här webbplatsen. Vi vill att alla ska kunna använda webbplatsen, oavsett behov. Här redogör vi hur jobtechdev.se uppfyller lagen om tillgänglighet till digital offentlig service, eventuella kända tillgänglighetsproblem och hur du kan rapportera brister till oss så att vi kan åtgärda dem.
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/07.about/01.availability/page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/07.about/01.availability/page.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/07.about/01.availability/page.sv.md
tags:
- user::pages::07.about::01.availability::page.sv.md
- pages::07.about::01.availability::page.sv.md
- 07.about::01.availability::page.sv.md
- 01.availability::page.sv.md
- page.sv.md
---
---
title: Tillgänglighet
custom:
    content: "**Hur tillgänglig är webbplatsen?**  \nVi är medvetna om att delar av webbplatsen inte är helt tillgängliga.\nI avsnittet om innehåll som inte är tillgängligt listar vi kända brister i tillgänglighet.\n\n**Kontakta oss om du hittar fler brister**  \nVi strävar hela tiden efter att förbättra webbplatsens tillgänglighet. Om du upptäcker problem som inte är beskrivna, eller om du anser att vi inte uppfyller lagens krav, meddela oss så att vi får veta att problemet finns.\n[Lämna synpunkter på digital tillgänglighet här](mailto:jobtechdev@arbetsformedlingen.se)\n\n**Kontakta tillsynsmyndigheten**  \nMyndigheten för digital förvaltning har ansvaret för tillsyn för lagen om tillgänglighet till digital offentlig service. Om du inte är nöjd med hur vi hanterar dina synpunkter kan du [kontakta Myndigheten för digital förvaltning](https://www.digg.se/tdosanmalan) och berätta det.\n\n**Teknisk information om webbplatsens tillgänglighet**  \nDen här webbplatsen är inte förenlig med [lagen om tillgänglighet till digital offentlig service](https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/lag-20181937-om-tillganglighet-till-digital_sfs-2018-1937), på grund av de brister som beskrivs nedan.\n\n**Innehåll som inte är tillgängligt**  \nVi är medvetna om följande brister som inte följer lagkraven:\n* Allt innehåll är inte responsivt\n* Det finns brister i kontrast\n\n**Hur vi har testat webbplatsen**  \nVi har gjort en självskattning (intern testning) av jobtechdev.se.\n\n**Hur vi jobbar med digital tillgänglighet**  \nVi strävar efter att webbplatsen ska kunna uppfattas, hanteras och förstås av alla användare, oavsett behov eller funktionsnedsättning och oberoende av vilka hjälpmedel du använder. Vi ska åtminstone uppnå grundläggande tillgänglighet genom att följa WCAG 2.1 på nivå AA.\n\n"
    menu:
        -
            title: Tillgänglighet
            url: /sv/om-webbplatsen/availability
        -
            title: 'Cookie policy'
            url: /sv/om-webbplatsen/cookies
    title: Tillgänglighetsredogörelse
    ingress: 'Arbetsförmedlingen (JobTech Development) är ägare till den här webbplatsen. Vi vill att alla ska kunna använda webbplatsen, oavsett behov. Här redogör vi hur jobtechdev.se uppfyller lagen om tillgänglighet till digital offentlig service, eventuella kända tillgänglighetsproblem och hur du kan rapportera brister till oss så att vi kan åtgärda dem.'
---

