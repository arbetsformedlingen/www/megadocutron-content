---
title: projekt
custom:
  title: Utvecklingsprojekt
  text: Här hittar du de utvecklingsprojekt som plattformen driver just nu
  showLeftMenu: false
  projects:
  - page: /projekt/individdata-och-dataportabilitet
  - page: /projekt/taxonomi-och-begreppsstruktur
  - page: /projekt/test
  - page: /projekt/digital-yrkesvaegledning
  - page: /projekt/test2
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/15.projekt/project_list.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/15.projekt/project_list.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/15.projekt/project_list.sv.md
tags:
- user::pages::15.projekt::project_list.sv.md
- pages::15.projekt::project_list.sv.md
- 15.projekt::project_list.sv.md
- project_list.sv.md
---
---
title: projekt
custom:
    title: Utvecklingsprojekt
    text: 'Här hittar du de utvecklingsprojekt som plattformen driver just nu'
    showLeftMenu: false
    projects:
        -
            page: /projekt/individdata-och-dataportabilitet
        -
            page: /projekt/taxonomi-och-begreppsstruktur
        -
            page: /projekt/test
        -
            page: /projekt/digital-yrkesvaegledning
        -
            page: /projekt/test2
---

