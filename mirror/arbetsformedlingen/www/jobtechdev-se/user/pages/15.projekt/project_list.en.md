---
title: project
custom:
  title: developing project
  text: 'Here you will find developing projects '
  showLeftMenu: false
  projects:
  - page: /projects/test
  - page: /projects/individdata-och-dataportabilitet
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/15.projekt/project_list.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/15.projekt/project_list.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/15.projekt/project_list.en.md
tags:
- user::pages::15.projekt::project_list.en.md
- pages::15.projekt::project_list.en.md
- 15.projekt::project_list.en.md
- project_list.en.md
---
---
title: project
custom:
    title: 'developing project'
    text: 'Here you will find developing projects '
    showLeftMenu: false
    projects:
        -
            page: /projects/test
        -
            page: /projects/individdata-och-dataportabilitet
---

