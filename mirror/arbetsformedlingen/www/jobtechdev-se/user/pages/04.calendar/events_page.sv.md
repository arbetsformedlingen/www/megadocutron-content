---
simplesearch:
  process: false
title: Kalender
routes:
  default: /kalender
custom:
  showLeftMenu: false
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pages/04.calendar/events_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pages/04.calendar/events_page.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pages/04.calendar/events_page.sv.md
tags:
- user::pages::04.calendar::events_page.sv.md
- pages::04.calendar::events_page.sv.md
- 04.calendar::events_page.sv.md
- events_page.sv.md
---
---
simplesearch:
    process: false
title: Kalender
routes:
    default: /kalender
custom:
    showLeftMenu: false
---

