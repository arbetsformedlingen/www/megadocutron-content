---
title: Contact us
custom:
  menu:
  - title: Cooperate with us
    url: /en/about-jobtech-development/samarbeta-med-oss
  - title: Get inspired by others
    url: /en/about-jobtech-development/inspireras-av-andra
  - title: Contact us
    url: /en/about-jobtech-development/kontakta-oss
  content: "### Hi!\n## We are happy that you want to get in touch with us. We try to be as quick as possible in answering all kinds of questions, thoughts or feedback. But it always makes it easier if the contact comes through the right channels.\n \n- If you want any _help or support_ related to our products, we recommend that you use our [community](https://forum.jobtechdev.se) so one of our developers or users can answer. If you do not want to use the forum, we also accept questions [via mail](mailto:jobtechdev@arbetsformedlingen.se).\n\n- If you are interested in _working with us_, we will post all our ads [here](https://jobtechdev.se/en/work-with-us), but we also accept open applications [via mail](mailto:recruitment-jobtech@arbetsformedlingen.se).\n\n- If you have questions about _cooperation_ or want to contribute something to the ecosystem, you can always email ours [Community manager](mailto:josefin.berdtson@arbetsformedlingen.se) and we will help you out. \n\n- If you want to _meet us_, most of us are working remote, but we also have office space in central [Stockholm](https://www.google.com/maps/place/Slottsbacken+4,+111+30+Stockholm/@59.3258895,18.0706078,17z/data=!3m1!4b1!4m5!3m4!1s0x465f9d587818d6bb:0x6a3563e7fe09a3bd!8m2!3d59.3258895!4d18.0727965) and Visby where we can book a meeting.\n  \n<BR><BR><BR><BR>"
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/09.om-jobtech-development/02.kontakta-oss/page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/09.om-jobtech-development/02.kontakta-oss/page.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/09.om-jobtech-development/02.kontakta-oss/page.en.md
tags:
- user::pagesz::09.om-jobtech-development::02.kontakta-oss::page.en.md
- pagesz::09.om-jobtech-development::02.kontakta-oss::page.en.md
- 09.om-jobtech-development::02.kontakta-oss::page.en.md
- 02.kontakta-oss::page.en.md
- page.en.md
---
---
title: 'Contact us'
custom:
    menu:
        -
            title: 'Cooperate with us'
            url: /en/about-jobtech-development/samarbeta-med-oss
        -
            title: 'Get inspired by others'
            url: /en/about-jobtech-development/inspireras-av-andra
        -
            title: 'Contact us'
            url: /en/about-jobtech-development/kontakta-oss
    content: "### Hi!\n## We are happy that you want to get in touch with us. We try to be as quick as possible in answering all kinds of questions, thoughts or feedback. But it always makes it easier if the contact comes through the right channels.\n \n- If you want any _help or support_ related to our products, we recommend that you use our [community](https://forum.jobtechdev.se) so one of our developers or users can answer. If you do not want to use the forum, we also accept questions [via mail](mailto:jobtechdev@arbetsformedlingen.se).\n\n- If you are interested in _working with us_, we will post all our ads [here](https://jobtechdev.se/en/work-with-us), but we also accept open applications [via mail](mailto:recruitment-jobtech@arbetsformedlingen.se).\n\n- If you have questions about _cooperation_ or want to contribute something to the ecosystem, you can always email ours [Community manager](mailto:josefin.berdtson@arbetsformedlingen.se) and we will help you out. \n\n- If you want to _meet us_, most of us are working remote, but we also have office space in central [Stockholm](https://www.google.com/maps/place/Slottsbacken+4,+111+30+Stockholm/@59.3258895,18.0706078,17z/data=!3m1!4b1!4m5!3m4!1s0x465f9d587818d6bb:0x6a3563e7fe09a3bd!8m2!3d59.3258895!4d18.0727965) and Visby where we can book a meeting.\n  \n<BR><BR><BR><BR>"
---

