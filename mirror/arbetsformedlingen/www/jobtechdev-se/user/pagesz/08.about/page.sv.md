---
title: Om webbplatsen
custom:
  menu:
  - title: Tillgänglighet
    url: /sv/om-webbplatsen/availability
  - title: Cookie policy
    url: /sv/om-webbplatsen/cookie-policy
  content: |-
    ### Om webbplatsen
    ## Webbplatsen använder responsiv design, och fungerar bra i moderna webbläsare som stöder webbstandarder satta av W3C. Moderna webbläsare är bland andra Safari, Chrome och Firefox.

    Vårt mål för webbplatsen är att fullt ut uppfylla kriterierna i WCAG2.1-AA-standarden, vilket innebär att vi följer samma riktlinjer för tillgänglighetsanpassning som är lagkrav för myndigheter och andra offentliga organ.
    <br><br><br><br>
routes:
  default: /om-webbplatsen
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/08.about/page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/08.about/page.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/08.about/page.sv.md
tags:
- user::pagesz::08.about::page.sv.md
- pagesz::08.about::page.sv.md
- 08.about::page.sv.md
- page.sv.md
---
---
title: 'Om webbplatsen'
custom:
    menu:
        -
            title: Tillgänglighet
            url: /sv/om-webbplatsen/availability
        -
            title: 'Cookie policy'
            url: /sv/om-webbplatsen/cookie-policy
    content: "### Om webbplatsen\n## Webbplatsen använder responsiv design, och fungerar bra i moderna webbläsare som stöder webbstandarder satta av W3C. Moderna webbläsare är bland andra Safari, Chrome och Firefox.\n\nVårt mål för webbplatsen är att fullt ut uppfylla kriterierna i WCAG2.1-AA-standarden, vilket innebär att vi följer samma riktlinjer för tillgänglighetsanpassning som är lagkrav för myndigheter och andra offentliga organ.\n<br><br><br><br>"
routes:
    default: /om-webbplatsen
---

