---
title: About the website
custom:
  content: |-
    ### About the website
    ## This website uses responsive design, and works well in modern browsers that support web standards set by W3C. Modern browsers include Safari, Chrome and Firefox.

    Our goal for the website is to fully meet the criteria of the WCAG2.1-AA standard, which means that we follow the same guidelines for accessibility adaptation that are legal requirements for authorities and other government agencies.
  menu:
  - title: Availability (in Swedish)
    url: /en/about-the-website/availability
  - title: Cookie policy (in Swedish)
    url: /en/about-the-website/cookies
routes:
  default: /about-the-website
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/08.about/page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/08.about/page.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/08.about/page.en.md
tags:
- user::pagesz::08.about::page.en.md
- pagesz::08.about::page.en.md
- 08.about::page.en.md
- page.en.md
---
---
title: 'About the website'
custom:
    content: "### About the website\n## This website uses responsive design, and works well in modern browsers that support web standards set by W3C. Modern browsers include Safari, Chrome and Firefox.\n\nOur goal for the website is to fully meet the criteria of the WCAG2.1-AA standard, which means that we follow the same guidelines for accessibility adaptation that are legal requirements for authorities and other government agencies."
    menu:
        -
            title: 'Availability (in Swedish)'
            url: /en/about-the-website/availability
        -
            title: 'Cookie policy (in Swedish)'
            url: /en/about-the-website/cookies
routes:
    default: /about-the-website
---

