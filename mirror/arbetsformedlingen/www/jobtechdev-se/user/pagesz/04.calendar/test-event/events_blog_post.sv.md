---
title: Kom igång och öppet forum
custom:
  title: Kom igång och öppet forum
  date: 27-08-2021 10:00
  endtime: '11:30'
  description: Under hösten arrangerar JobTech Development öppna webbinarier för alla som vill ha hjälp att komma igång med våra API:er.
  short: För användare av JobTech Developments API:er
  register: https://jobtechdev.se/news/news_head/210316/
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/04.calendar/test-event/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/04.calendar/test-event/events_blog_post.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/04.calendar/test-event/events_blog_post.sv.md
tags:
- user::pagesz::04.calendar::test-event::events_blog_post.sv.md
- pagesz::04.calendar::test-event::events_blog_post.sv.md
- 04.calendar::test-event::events_blog_post.sv.md
- test-event::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'Kom igång och öppet forum'
custom:
    title: 'Kom igång och öppet forum'
    date: '27-08-2021 10:00'
    endtime: '11:30'
    description: 'Under hösten arrangerar JobTech Development öppna webbinarier för alla som vill ha hjälp att komma igång med våra API:er.'
    short: 'För användare av JobTech Developments API:er'
    register: 'https://jobtechdev.se/news/news_head/210316/'
---

