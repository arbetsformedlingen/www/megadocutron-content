---
title: eForvaltningsdagarna October 27-28 2021
custom:
  title: eFörvaltningsdagarna
  date: 27-10-2021 09:00
  short: JobTech Development will participate in the eFörvaltningsdagarna. e-Förvaltningsdagarna is Sweden's largest conference and meeting place for e-government and digital transformation. More information to follow!
  description: JobTech Development will participate in the eGovernment Days. eGovernment Days is Sweden's largest conference and meeting place for e-government and digital transformation. More information to follow!
  endtime: '18:00'
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/04.calendar/e-forvaltningsdagarna/events_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/04.calendar/e-forvaltningsdagarna/events_blog_post.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/04.calendar/e-forvaltningsdagarna/events_blog_post.en.md
tags:
- user::pagesz::04.calendar::e-forvaltningsdagarna::events_blog_post.en.md
- pagesz::04.calendar::e-forvaltningsdagarna::events_blog_post.en.md
- 04.calendar::e-forvaltningsdagarna::events_blog_post.en.md
- e-forvaltningsdagarna::events_blog_post.en.md
- events_blog_post.en.md
---
---
title: 'eForvaltningsdagarna October 27-28 2021'
custom:
    title: eFörvaltningsdagarna
    date: '27-10-2021 09:00'
    short: 'JobTech Development will participate in the eFörvaltningsdagarna. e-Förvaltningsdagarna is Sweden''s largest conference and meeting place for e-government and digital transformation. More information to follow!'
    description: 'JobTech Development will participate in the eGovernment Days. eGovernment Days is Sweden''s largest conference and meeting place for e-government and digital transformation. More information to follow!'
    endtime: '18:00'
---

