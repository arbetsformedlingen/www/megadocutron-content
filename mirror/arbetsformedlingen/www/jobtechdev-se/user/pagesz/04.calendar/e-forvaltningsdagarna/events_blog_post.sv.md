---
title: eForvaltningsdagarna 27-28 oktober 2021
custom:
  title: eFörvaltningsdagarna
  date: 27-10-2021 09:00
  short: JobTech Development kommer delta på eFörvaltningsdagarna. eFörvaltningsdagarna är Sveriges största konferens och mötesplats om e-förvaltning och digital transformation. Mer information kommer!
  description: JobTech Development kommer delta på eFörvaltningsdagarna. eFörvaltningsdagarna är Sveriges största konferens och mötesplats om e-förvaltning och digital transformation. Mer information kommer!
  endtime: '18:00'
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/04.calendar/e-forvaltningsdagarna/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/04.calendar/e-forvaltningsdagarna/events_blog_post.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/04.calendar/e-forvaltningsdagarna/events_blog_post.sv.md
tags:
- user::pagesz::04.calendar::e-forvaltningsdagarna::events_blog_post.sv.md
- pagesz::04.calendar::e-forvaltningsdagarna::events_blog_post.sv.md
- 04.calendar::e-forvaltningsdagarna::events_blog_post.sv.md
- e-forvaltningsdagarna::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'eForvaltningsdagarna 27-28 oktober 2021'
custom:
    title: eFörvaltningsdagarna
    date: '27-10-2021 09:00'
    short: 'JobTech Development kommer delta på eFörvaltningsdagarna. eFörvaltningsdagarna är Sveriges största konferens och mötesplats om e-förvaltning och digital transformation. Mer information kommer!'
    description: 'JobTech Development kommer delta på eFörvaltningsdagarna. eFörvaltningsdagarna är Sveriges största konferens och mötesplats om e-förvaltning och digital transformation. Mer information kommer!'
    endtime: '18:00'
---

