---
title: Jobba hos oss
custom:
  content: |-
    ## Lediga tjänster / spontanansökan.
    Just nu har vi inga lediga tjänster, men skicka gärna en [spontanansökan](mailto:recruitment-jobtech@arbetsformedlingen.se) med en kort beskrivning av dig själv och vad du kan, så kontaktar vi dig när behov finns.
routes:
  default: /jobba-hos-oss
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/05.work/page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/05.work/page.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/05.work/page.sv.md
tags:
- user::pagesz::05.work::page.sv.md
- pagesz::05.work::page.sv.md
- 05.work::page.sv.md
- page.sv.md
---
---
title: 'Jobba hos oss'
custom:
    content: "## Lediga tjänster / spontanansökan.\nJust nu har vi inga lediga tjänster, men skicka gärna en [spontanansökan](mailto:recruitment-jobtech@arbetsformedlingen.se) med en kort beskrivning av dig själv och vad du kan, så kontaktar vi dig när behov finns."
routes:
    default: /jobba-hos-oss
---

