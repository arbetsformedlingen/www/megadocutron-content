---
title: Get started with JobTech Development APIs
custom:
  title: Get started with JobTech Development APIs
  date: 21-06-2021 10:06
  content: |-
    ###### During the autumn, JobTech Development arranges open webinars for anyone who wants help getting started with our APIs.

    To make it even easier to use our APIs, we are organizing four webinars during the autumn where we provide a technical introduction and the opportunity to ask questions directly to our developers. The webinars are primarily targeting at new and existing users, but are open to anyone who wants to know more about how our APIs can be used for the development of digital services for the labor market.

    At the webinars, our development team, represented by Johan Brymér Dahlhielm and Henrik Suzuki, will provide information and practical guidance for APIs such as "JobSearch", "JobStream" and "JobTech Taxonomy". This is followed by an open conversation with the opportunity to ask questions, give feedback and present ideas and wishes for new technical solutions from JobTech Development.

    “We look forward to helping and inspiring new users through this autumn's webinars. Through more dialogue and feedback, we also get a greater opportunity to use our products and become more accurate when we develop new technical solutions for the labor market, ”says Henrik Suzuki.

    All webinars will be held according to the "open space" model, which means that the agenda and topics of conversation are largely governed by the participants' specific interests and needs.
  short: To make it even easier to use our APIs, we are organizing four webinars during the autumn where we provide a technical introduction and the opportunity to ask questions directly to our developers. The webinars are primarily targeting at new and existing users, but are open to anyone who wants to know more about how our APIs can be used for the development of digital services for the labor market.
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/03.news/kom-igang-med-jobtech-developments-api-er/news_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/03.news/kom-igang-med-jobtech-developments-api-er/news_blog_post.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/kom-igang-med-jobtech-developments-api-er/news_blog_post.en.md
tags:
- user::pagesz::03.news::kom-igang-med-jobtech-developments-api-er::news_blog_post.en.md
- pagesz::03.news::kom-igang-med-jobtech-developments-api-er::news_blog_post.en.md
- 03.news::kom-igang-med-jobtech-developments-api-er::news_blog_post.en.md
- kom-igang-med-jobtech-developments-api-er::news_blog_post.en.md
- news_blog_post.en.md
---
---
title: 'Get started with JobTech Development APIs'
custom:
    title: 'Get started with JobTech Development APIs'
    date: '21-06-2021 10:06'
    content: "###### During the autumn, JobTech Development arranges open webinars for anyone who wants help getting started with our APIs.\n\nTo make it even easier to use our APIs, we are organizing four webinars during the autumn where we provide a technical introduction and the opportunity to ask questions directly to our developers. The webinars are primarily targeting at new and existing users, but are open to anyone who wants to know more about how our APIs can be used for the development of digital services for the labor market.\n\nAt the webinars, our development team, represented by Johan Brymér Dahlhielm and Henrik Suzuki, will provide information and practical guidance for APIs such as \"JobSearch\", \"JobStream\" and \"JobTech Taxonomy\". This is followed by an open conversation with the opportunity to ask questions, give feedback and present ideas and wishes for new technical solutions from JobTech Development.\n\n“We look forward to helping and inspiring new users through this autumn's webinars. Through more dialogue and feedback, we also get a greater opportunity to use our products and become more accurate when we develop new technical solutions for the labor market, ”says Henrik Suzuki.\n\nAll webinars will be held according to the \"open space\" model, which means that the agenda and topics of conversation are largely governed by the participants' specific interests and needs."
    short: 'To make it even easier to use our APIs, we are organizing four webinars during the autumn where we provide a technical introduction and the opportunity to ask questions directly to our developers. The webinars are primarily targeting at new and existing users, but are open to anyone who wants to know more about how our APIs can be used for the development of digital services for the labor market.'
---

