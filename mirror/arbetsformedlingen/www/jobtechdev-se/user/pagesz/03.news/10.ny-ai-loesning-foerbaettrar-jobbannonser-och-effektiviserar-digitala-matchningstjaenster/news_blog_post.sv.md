---
title: Ny AI-lösning förbättrar jobbannonser och effektiviserar digitala matchningstjänster
custom:
  title: Ny AI-lösning förbättrar jobbannonser och effektiviserar digitala matchningstjänster
  date: 14-09-2020 11:25
  short: Ny AI-lösning förbättrar jobbannonser och effektiviserar digitala matchningstjänster
  content: |-
    Irrelevanta sökträffar och märkliga jobbförslag är något många arbetssökande känner igen – och gärna slipper. Med AI-lösningen JobAd Enrichments, som är fritt för alla matchningsaktörer att använda, kan problemet undvikas, samtidigt som det bidrar till fördjupade insikter om arbetsmarknaden.

    API:et JobAd Enrichments, som bygger på AI-teknik, identifierar automatiskt ord i annonser som har verklig betydelse i en söksituation, samtidigt som överflödig information filtreras bort.

    Med hjälp av JobAd Enrichments blir det betydligt lättare att navigera och snabbt hitta rätt bland platsannonser i digitala annonsplattformar. Den arbetssökande slipper lägga tid på att sortera sökträffar och detaljgranska långa annonstexter för att se om de innehåller information som är intressant och relevant.

    API:et har testats sedan drygt ett år tillbaka i Arbetsförmedlingens egen plattform Platsbanken. Nu är det dessutom fritt tillgängligt för alla aktörer som vill förbättra söktjänster eller annonsplattformar.

    Utöver förbättrad matchning kan JobAd Enrichment användas för att få fördjupade insikter om arbetsmarknaden och för att vägleda arbetssökande. Genom att sammanställa och analysera strukturerad annonsdata blir det till exempel möjligt att identifiera:
    * Kompetenser och förmågor som efterfrågas för olika yrkesroller.
    * Yrken som är lämpliga för den som har vissa kompetenser/förmågor, eller vilka kompetenser man bör satsa på för att bli mer efterfrågad inom ett visst yrke.
    * Om vissa kompetenser/förmågor överhuvudtaget efterfrågas, och huruvida intresset för dem ökar eller minskar.

    API:et kan också användas vid utveckling av digitala tjänster som vänder sig till arbetsgivare. Det kan till exempel vara tjänster som analyserar och förbättrar innehåll i platsannonser:
    * Hur stor del av annonsen består egentligen av relevanta och efterfrågade ord?
    * Vilka begrepp efterfrågas vanligtvis för ett yrke? Saknas viktiga ord i vår annons?
    * Hur unik är vår annons jämfört med andra annonser inom samma område? Använder vi ord och uttryck som det går inflation i och därmed inte är så användbara.

    Är du utvecklare och vill använda vår nya AI-lösning? Här kommer du igång med JobAd Enrichments,som är ett komplement till vårt övriga utbud av öppna API:er för språk-, individ- och arbetsmarknadsdata.

    I vårt forum kan du ställa frågor och delta i diskussioner om JobAd Enrichments eller någon av våra andra våra API:er.
    Välkommen in!

    Det går även bra att kontakta oss via Epost
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/03.news/10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/03.news/10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.sv.md
tags:
- user::pagesz::03.news::10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster::news_blog_post.sv.md
- pagesz::03.news::10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster::news_blog_post.sv.md
- 03.news::10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster::news_blog_post.sv.md
- 10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster::news_blog_post.sv.md
- news_blog_post.sv.md
---
---
title: 'Ny AI-lösning förbättrar jobbannonser och effektiviserar digitala matchningstjänster'
custom:
    title: 'Ny AI-lösning förbättrar jobbannonser och effektiviserar digitala matchningstjänster'
    date: '14-09-2020 11:25'
    short: 'Ny AI-lösning förbättrar jobbannonser och effektiviserar digitala matchningstjänster'
    content: "Irrelevanta sökträffar och märkliga jobbförslag är något många arbetssökande känner igen – och gärna slipper. Med AI-lösningen JobAd Enrichments, som är fritt för alla matchningsaktörer att använda, kan problemet undvikas, samtidigt som det bidrar till fördjupade insikter om arbetsmarknaden.\n\nAPI:et JobAd Enrichments, som bygger på AI-teknik, identifierar automatiskt ord i annonser som har verklig betydelse i en söksituation, samtidigt som överflödig information filtreras bort.\n\nMed hjälp av JobAd Enrichments blir det betydligt lättare att navigera och snabbt hitta rätt bland platsannonser i digitala annonsplattformar. Den arbetssökande slipper lägga tid på att sortera sökträffar och detaljgranska långa annonstexter för att se om de innehåller information som är intressant och relevant.\n\nAPI:et har testats sedan drygt ett år tillbaka i Arbetsförmedlingens egen plattform Platsbanken. Nu är det dessutom fritt tillgängligt för alla aktörer som vill förbättra söktjänster eller annonsplattformar.\n\nUtöver förbättrad matchning kan JobAd Enrichment användas för att få fördjupade insikter om arbetsmarknaden och för att vägleda arbetssökande. Genom att sammanställa och analysera strukturerad annonsdata blir det till exempel möjligt att identifiera:\n* Kompetenser och förmågor som efterfrågas för olika yrkesroller.\n* Yrken som är lämpliga för den som har vissa kompetenser/förmågor, eller vilka kompetenser man bör satsa på för att bli mer efterfrågad inom ett visst yrke.\n* Om vissa kompetenser/förmågor överhuvudtaget efterfrågas, och huruvida intresset för dem ökar eller minskar.\n\nAPI:et kan också användas vid utveckling av digitala tjänster som vänder sig till arbetsgivare. Det kan till exempel vara tjänster som analyserar och förbättrar innehåll i platsannonser:\n* Hur stor del av annonsen består egentligen av relevanta och efterfrågade ord?\n* Vilka begrepp efterfrågas vanligtvis för ett yrke? Saknas viktiga ord i vår annons?\n* Hur unik är vår annons jämfört med andra annonser inom samma område? Använder vi ord och uttryck som det går inflation i och därmed inte är så användbara.\n\nÄr du utvecklare och vill använda vår nya AI-lösning? Här kommer du igång med JobAd Enrichments,som är ett komplement till vårt övriga utbud av öppna API:er för språk-, individ- och arbetsmarknadsdata.\n\nI vårt forum kan du ställa frågor och delta i diskussioner om JobAd Enrichments eller någon av våra andra våra API:er.\nVälkommen in!\n\nDet går även bra att kontakta oss via Epost"
---

