---
title: JobTech Development - a cutting-edge project that many can learn from
custom:
  title: JobTech Development - a cutting-edge project that many can learn from
  date: 20-01-2021 09:10
  short: Interview with Johan Linåker, Lund University, who is a follow-up researcher to JobTech Development and has reached halfway in his work to study how we, through collaboration and open data, build a digital ecosystem for the Swedish labor market
  content: |+
    Five more questions ...
    Johan Linåker, Lund University, who is a follow-up researcher to JobTech Development and has reached halfway in his work to study how we, through collaboration and open data, build a digital ecosystem for the Swedish labor market

    ###### Why is it interesting to follow JobTech Development?
    ”It is very exciting to follow how to work with open data and open software, and above all how you collaborate with actors within one and the same ecosystem around both the data and the software. With the research hat on, I can say that it is unique and forward looking, and so far there is little reported and written in the field. The general debate in the public sector is about opening up and publishing your data, but there is very little focus on collaboration and the connection to open software. That is why JobTech Development and the ecosystem you are building is a cutting-edge project that many can learn from. ”

    ###### What is your role as a companion researcher?
    “As a companion researcher, I have two different hats. With the "collaboration hat", I help JobTech Development to build a sustainable ecosystem. I support, coach and provide input from research on how to collaborate on open data and open source software. With the "researcher hat", I follow how the ecosystem develops and compare with other examples in the outside world and give feedback to JobTech. I have, for example, compared with Trafiklab, which is Samtrafiken's platform for open data connected to public transport. I have also taken a closer look at open data ecosystems in Finland and the USA. In addition, I work to spread knowledge about how authorities can use and collaborate on open data and open source software. A concrete result is the newly formed Network for Open Software and Data (NOSAD.se) where we arrange workshops and collect materials that can help authorities in their work with these issues. ”

    ###### You have come halfway into your work. What have you come up with so far?
    One insight is that authorities can collaborate on the data they open up in many different ways. JobTech Development is one example, Trafiklab another. What fits best depends on the individual case. It has also been fun to see the strong synergy effect that exists between open source software and open data. Partly as a way to inspire how the data can be used and simplify for developers, partly to create transparency about how the data is used, and partly to enable the provision of the data. If a municipality is in a closed system, it can be as bad they dont even have access to the information. "

    ###### What should your work as a companion researcher lead up to?
    “I hope that it contributes to a stronger ecosystem, that more actors are added and that the data comes into use, and that this in turn leads to better matching between employers and jobseekers. I also hope that the work and research can help authorities, both in Sweden and internationally, to improve their work with open data and open software. ”

    ###### How can those who are interested take part in your work?
    “A tip is to go to NOSAD where you can take part in research articles, open source policy, reports and training materials. I also share material via my Twitter and LinkedIn accounts. [(@ Johanlinaker)] (). ”

    In our forum, you can follow what is happening within NOSAD and participate in discussions within the jobtech area.

gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/03.news/jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av/news_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/03.news/jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av/news_blog_post.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av/news_blog_post.en.md
tags:
- user::pagesz::03.news::jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av::news_blog_post.en.md
- pagesz::03.news::jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av::news_blog_post.en.md
- 03.news::jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av::news_blog_post.en.md
- jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av::news_blog_post.en.md
- news_blog_post.en.md
---
---
title: 'JobTech Development - a cutting-edge project that many can learn from'
custom:
    title: 'JobTech Development - a cutting-edge project that many can learn from'
    date: '20-01-2021 09:10'
    short: 'Interview with Johan Linåker, Lund University, who is a follow-up researcher to JobTech Development and has reached halfway in his work to study how we, through collaboration and open data, build a digital ecosystem for the Swedish labor market'
    content: "Five more questions ...\nJohan Linåker, Lund University, who is a follow-up researcher to JobTech Development and has reached halfway in his work to study how we, through collaboration and open data, build a digital ecosystem for the Swedish labor market\n\n###### Why is it interesting to follow JobTech Development?\n”It is very exciting to follow how to work with open data and open software, and above all how you collaborate with actors within one and the same ecosystem around both the data and the software. With the research hat on, I can say that it is unique and forward looking, and so far there is little reported and written in the field. The general debate in the public sector is about opening up and publishing your data, but there is very little focus on collaboration and the connection to open software. That is why JobTech Development and the ecosystem you are building is a cutting-edge project that many can learn from. ”\n\n###### What is your role as a companion researcher?\n“As a companion researcher, I have two different hats. With the \"collaboration hat\", I help JobTech Development to build a sustainable ecosystem. I support, coach and provide input from research on how to collaborate on open data and open source software. With the \"researcher hat\", I follow how the ecosystem develops and compare with other examples in the outside world and give feedback to JobTech. I have, for example, compared with Trafiklab, which is Samtrafiken's platform for open data connected to public transport. I have also taken a closer look at open data ecosystems in Finland and the USA. In addition, I work to spread knowledge about how authorities can use and collaborate on open data and open source software. A concrete result is the newly formed Network for Open Software and Data (NOSAD.se) where we arrange workshops and collect materials that can help authorities in their work with these issues. ”\n\n###### You have come halfway into your work. What have you come up with so far?\nOne insight is that authorities can collaborate on the data they open up in many different ways. JobTech Development is one example, Trafiklab another. What fits best depends on the individual case. It has also been fun to see the strong synergy effect that exists between open source software and open data. Partly as a way to inspire how the data can be used and simplify for developers, partly to create transparency about how the data is used, and partly to enable the provision of the data. If a municipality is in a closed system, it can be as bad they dont even have access to the information. \"\n\n###### What should your work as a companion researcher lead up to?\n“I hope that it contributes to a stronger ecosystem, that more actors are added and that the data comes into use, and that this in turn leads to better matching between employers and jobseekers. I also hope that the work and research can help authorities, both in Sweden and internationally, to improve their work with open data and open software. ”\n\n###### How can those who are interested take part in your work?\n“A tip is to go to NOSAD where you can take part in research articles, open source policy, reports and training materials. I also share material via my Twitter and LinkedIn accounts. [(@ Johanlinaker)] (). ”\n\nIn our forum, you can follow what is happening within NOSAD and participate in discussions within the jobtech area.\n\n"
---

