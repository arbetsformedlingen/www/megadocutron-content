---
title: Easier for job seekers to find the right one
custom:
  title: Easier for job seekers to find the right one
  date: 13-09-2020 11:37
  content: |-
    Five questions to Mattias Persson, developer responsible for the AI solution JobAd Enrichments.

    ###### What is behind the investment in JobAd Enrichments?
    “Anyone looking for a job via digital advertising platforms today often encounters problems with irrelevant search results and job proposals, or search results that only partially match what they are looking for. This leads to frustration and to being forced to spend time reviewing texts to see if they really contain anything interesting. With JobAd Enrichments, we make it easier for jobseekers to navigate and quickly find the right job advertisements. The API is based on an AI solution that automatically identifies relevant words in job advertisements, and filters out information that is not directly related to the service itself. There is also a synonym function that allows you to get relevant hits even if keywords in the ad or search query are misspelled. ”

    ###### Who will be able to benefit from JobAd Enrichments?
    "All matching actors, private and public, who want to extract structured data from ad texts so that the services or build new innovative solutions will be able to benefit from it. This in turn will benefit jobseekers and employers who use these services. Over the past year, JobAd Enrichments has been tested in Platsbanken to increase the quality of search results and make it easier for jobseekers to find the right ads, a concrete example of an API that is useful. Others who will be able to benefit from being able to extract structured advertising data are researchers and experts who want to learn more about how the labor market works. ”

    ###### How has the development itself been working?
    “We started building a glossary of the actual terms used in the ads, to then be able to identify them when we automatically interpret the ad texts. In order to be able to assess whether, for example, a competence is in demand or not by the employer, we used Deep learning / AI. To create the AI models, we created training data using the Snorkel framework and then we trained the AI models with Google's Tensorflow. ”

    ###### What has been the biggest challenge?
    “Finding the optimal configuration for the AI models to work. It took a long time and many failed attempts to find an optimal solution. Without exaggerating, I can say that it has been a patient test. "

    ###### How can JobAd Enrichments contribute to a better labor market?
    “In addition to streamlining matching, the API provides us with completely new information about the labor market, and with the in-depth knowledge, conditions are created for new digital matching and guidance services. It will be exciting to see how the API is received by developers and other actors who follow our work, and I hope to see many innovative services with JobAd Enrichments as part of the solution. ”
  short: Five questions to… Mattias Persson, developer responsible for the AI ​​solution JobAd Enrichments.
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/03.news/intervju_mattias/news_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/03.news/intervju_mattias/news_blog_post.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/intervju_mattias/news_blog_post.en.md
tags:
- user::pagesz::03.news::intervju_mattias::news_blog_post.en.md
- pagesz::03.news::intervju_mattias::news_blog_post.en.md
- 03.news::intervju_mattias::news_blog_post.en.md
- intervju_mattias::news_blog_post.en.md
- news_blog_post.en.md
---
---
title: 'Easier for job seekers to find the right one'
custom:
    title: 'Easier for job seekers to find the right one'
    date: '13-09-2020 11:37'
    content: "Five questions to Mattias Persson, developer responsible for the AI solution JobAd Enrichments.\n\n###### What is behind the investment in JobAd Enrichments?\n“Anyone looking for a job via digital advertising platforms today often encounters problems with irrelevant search results and job proposals, or search results that only partially match what they are looking for. This leads to frustration and to being forced to spend time reviewing texts to see if they really contain anything interesting. With JobAd Enrichments, we make it easier for jobseekers to navigate and quickly find the right job advertisements. The API is based on an AI solution that automatically identifies relevant words in job advertisements, and filters out information that is not directly related to the service itself. There is also a synonym function that allows you to get relevant hits even if keywords in the ad or search query are misspelled. ”\n\n###### Who will be able to benefit from JobAd Enrichments?\n\"All matching actors, private and public, who want to extract structured data from ad texts so that the services or build new innovative solutions will be able to benefit from it. This in turn will benefit jobseekers and employers who use these services. Over the past year, JobAd Enrichments has been tested in Platsbanken to increase the quality of search results and make it easier for jobseekers to find the right ads, a concrete example of an API that is useful. Others who will be able to benefit from being able to extract structured advertising data are researchers and experts who want to learn more about how the labor market works. ”\n\n###### How has the development itself been working?\n“We started building a glossary of the actual terms used in the ads, to then be able to identify them when we automatically interpret the ad texts. In order to be able to assess whether, for example, a competence is in demand or not by the employer, we used Deep learning / AI. To create the AI models, we created training data using the Snorkel framework and then we trained the AI models with Google's Tensorflow. ”\n\n###### What has been the biggest challenge?\n“Finding the optimal configuration for the AI models to work. It took a long time and many failed attempts to find an optimal solution. Without exaggerating, I can say that it has been a patient test. \"\n\n###### How can JobAd Enrichments contribute to a better labor market?\n“In addition to streamlining matching, the API provides us with completely new information about the labor market, and with the in-depth knowledge, conditions are created for new digital matching and guidance services. It will be exciting to see how the API is received by developers and other actors who follow our work, and I hope to see many innovative services with JobAd Enrichments as part of the solution. ”"
    short: 'Five questions to… Mattias Persson, developer responsible for the AI ​​solution JobAd Enrichments.'
---

