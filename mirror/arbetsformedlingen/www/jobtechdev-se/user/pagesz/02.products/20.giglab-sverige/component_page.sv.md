---
title: Giglab Sverige
custom:
  title: Giglab Sverige
  description: Ett nationellt policylab och forum med syfte att belysa utmaningar från flera perspektiv inom gigekonomin och framtidens arbetsmarknad.
  block_1: |-
    Giglab Sverige är ett nationellt policylabb och ett samarbete mellan Jobtech/Arbetsförmedlingen, Skatteverket, Coompanion och Handelshögskolan tillsammans med SVID, Stiftelsen Svensk Industridesign och svenska Labbnätverket. Del av labbet ingår i Vinnova-utlysningen Utmaningsdriven Innovation. Vår ambition är att tillsammans verka för en hållbar framväxt av gigekonomin i Sverige.

    ## Målsättning
    - Underlag för en testbädd för nya prototypa hållbara lösningar inom gigekonomin.
    - Underlag för politiken för en fortsatt diskussion kring behov av policyförändringar och lagförändringar kopplat till gigekonomin.
    - Inspel i pågående arbete mot Agenda 2030, fokus på bättre arbetsvillkor samt digital infrastruktur kopplad till gigekonomin.
    - Underlag för fortsatt internt arbete på myndigheterna kopplat till gigekonomin
    - Empiriskt underlag för forskning inom området gigekonomin, som bl.a. bedrivs av Postdok-forskaren Claire Ingram Bogusz vid Handelshögskolan i Stockholm

    ## Metod
    Giglab Sverige är ett första gemensamt lär-projekt för att undersöka hur vi kan bidra till att skapa en hållbar framtida arbetsmarknad som inkluderar ett hållbart giggande, som skapar värde för samhället, den enskilde och för olika affärsmodeller i en digital och global värld.
  menu:
  - title: Webbplats
    url: https://www.giglabsverige.se
    showInShort: '1'
  - title: Rapport
    url: https://assets.website-files.com/5e95f95bba7c1422fb23179c/60801ba984287bfc458ad010_GiglabSverige-Kartläggning%20av%20utmaningar%20inom%20gigekonomin%20i%20sverige.pdf
    showInShort: null
  - title: Enkät om Gigekonomin
    url: https://hhs.qualtrics.com/jfe/form/SV_1SNSZEQJELsfI1v
    showInShort: null
  - title: Initiativ och experiment
    url: https://www.giglabsverige.se/initiativ
    showInShort: '1'
  block_2: |-
    ### Är du Giggare, plattformsägare eller på något annat sätt i kontakt med gigekonomin?
    Vi vill gärna ha din hjälp med insikter och utmaningar till vår kartläggning av gigekonomin i Sverige. Läs mer om hur du kan hjälpa till på vår [webbplats](https://www.giglabsverige.se).
  contact_email: lisa.hemph@arbetsformedlingen.se
  contact_name: Lisa Hemph
taxonomy:
  category:
  - Samarbete
  status:
  - Pågående
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/02.products/20.giglab-sverige/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/02.products/20.giglab-sverige/component_page.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/20.giglab-sverige/component_page.sv.md
tags:
- user::pagesz::02.products::20.giglab-sverige::component_page.sv.md
- pagesz::02.products::20.giglab-sverige::component_page.sv.md
- 02.products::20.giglab-sverige::component_page.sv.md
- 20.giglab-sverige::component_page.sv.md
- component_page.sv.md
---
---
title: 'Giglab Sverige'
custom:
    title: 'Giglab Sverige'
    description: 'Ett nationellt policylab och forum med syfte att belysa utmaningar från flera perspektiv inom gigekonomin och framtidens arbetsmarknad.'
    block_1: "Giglab Sverige är ett nationellt policylabb och ett samarbete mellan Jobtech/Arbetsförmedlingen, Skatteverket, Coompanion och Handelshögskolan tillsammans med SVID, Stiftelsen Svensk Industridesign och svenska Labbnätverket. Del av labbet ingår i Vinnova-utlysningen Utmaningsdriven Innovation. Vår ambition är att tillsammans verka för en hållbar framväxt av gigekonomin i Sverige.\n\n## Målsättning\n- Underlag för en testbädd för nya prototypa hållbara lösningar inom gigekonomin.\n- Underlag för politiken för en fortsatt diskussion kring behov av policyförändringar och lagförändringar kopplat till gigekonomin.\n- Inspel i pågående arbete mot Agenda 2030, fokus på bättre arbetsvillkor samt digital infrastruktur kopplad till gigekonomin.\n- Underlag för fortsatt internt arbete på myndigheterna kopplat till gigekonomin\n- Empiriskt underlag för forskning inom området gigekonomin, som bl.a. bedrivs av Postdok-forskaren Claire Ingram Bogusz vid Handelshögskolan i Stockholm\n\n## Metod\nGiglab Sverige är ett första gemensamt lär-projekt för att undersöka hur vi kan bidra till att skapa en hållbar framtida arbetsmarknad som inkluderar ett hållbart giggande, som skapar värde för samhället, den enskilde och för olika affärsmodeller i en digital och global värld."
    menu:
        -
            title: Webbplats
            url: 'https://www.giglabsverige.se'
            showInShort: '1'
        -
            title: Rapport
            url: 'https://assets.website-files.com/5e95f95bba7c1422fb23179c/60801ba984287bfc458ad010_GiglabSverige-Kartläggning%20av%20utmaningar%20inom%20gigekonomin%20i%20sverige.pdf'
            showInShort: null
        -
            title: 'Enkät om Gigekonomin'
            url: 'https://hhs.qualtrics.com/jfe/form/SV_1SNSZEQJELsfI1v'
            showInShort: null
        -
            title: 'Initiativ och experiment'
            url: 'https://www.giglabsverige.se/initiativ'
            showInShort: '1'
    block_2: "### Är du Giggare, plattformsägare eller på något annat sätt i kontakt med gigekonomin?\nVi vill gärna ha din hjälp med insikter och utmaningar till vår kartläggning av gigekonomin i Sverige. Läs mer om hur du kan hjälpa till på vår [webbplats](https://www.giglabsverige.se)."
    contact_email: lisa.hemph@arbetsformedlingen.se
    contact_name: 'Lisa Hemph'
taxonomy:
    category:
        - Samarbete
    status:
        - Pågående
---

