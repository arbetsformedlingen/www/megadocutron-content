---
title: JobSearch
custom:
  title: JobSearch
  block_1: "JobSearch är användbart för alla företag och organisationer som vill använda jobbannonser från Platsbanken men som saknar en egen sökmotor. \n\nDet är också användbart för aktörer som har tillgång till stora mängder strukturerad data och vill kunna erbjuda specialsökfunktioner eller nischade annonsplattformar."
  description: Sökmotor som gör det möjligt att söka och filtrera bland alla jobbannonser på Arbetsförmedlingens annonsplattform Platsbanken.
  menu:
  - title: Användargränssnitt
    url: https://jobsearch.api.jobtechdev.se
    showInShort: '1'
  - title: API-nyckel
    url: https://apirequest.jobtechdev.se
    showInShort: '1'
  - title: Kom igång
    url: https://github.com/Jobtechdev-content/Jobsearch-content/blob/master/GettingStartedJobSearchSE.md
    showInShort: '1'
  block_2: |-
    ### Viktigt
    För detta API behöver du registrera en API-nyckel. Vi kan komma att ogiltigförklara din nyckel om du gör stora mängder anrop som inte passar det avsedda syftet med JobSearch.
  product_info:
  - title: Version
    value: '1.0'
  contact_email: josefin.berndtson@arbetsformedlingen.se
  contact_name: Josefin Berndtson
taxonomy:
  category:
  - API
  type:
  - Öppna data
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/02.products/11.jobsearch/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/02.products/11.jobsearch/component_page.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/11.jobsearch/component_page.sv.md
tags:
- user::pagesz::02.products::11.jobsearch::component_page.sv.md
- pagesz::02.products::11.jobsearch::component_page.sv.md
- 02.products::11.jobsearch::component_page.sv.md
- 11.jobsearch::component_page.sv.md
- component_page.sv.md
---
---
title: JobSearch
custom:
    title: JobSearch
    block_1: "JobSearch är användbart för alla företag och organisationer som vill använda jobbannonser från Platsbanken men som saknar en egen sökmotor. \n\nDet är också användbart för aktörer som har tillgång till stora mängder strukturerad data och vill kunna erbjuda specialsökfunktioner eller nischade annonsplattformar."
    description: 'Sökmotor som gör det möjligt att söka och filtrera bland alla jobbannonser på Arbetsförmedlingens annonsplattform Platsbanken.'
    menu:
        -
            title: Användargränssnitt
            url: 'https://jobsearch.api.jobtechdev.se'
            showInShort: '1'
        -
            title: API-nyckel
            url: 'https://apirequest.jobtechdev.se'
            showInShort: '1'
        -
            title: 'Kom igång'
            url: 'https://github.com/Jobtechdev-content/Jobsearch-content/blob/master/GettingStartedJobSearchSE.md'
            showInShort: '1'
    block_2: "### Viktigt\nFör detta API behöver du registrera en API-nyckel. Vi kan komma att ogiltigförklara din nyckel om du gör stora mängder anrop som inte passar det avsedda syftet med JobSearch."
    product_info:
        -
            title: Version
            value: '1.0'
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berndtson'
taxonomy:
    category:
        - API
    type:
        - 'Öppna data'
---

