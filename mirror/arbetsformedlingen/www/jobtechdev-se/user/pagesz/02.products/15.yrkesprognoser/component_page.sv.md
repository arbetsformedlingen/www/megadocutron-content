---
title: Yrkesprognoser
custom:
  title: Yrkesprognoser
  description: Prognoser för rekryteringsbehov och konkurrenssituation inom olika yrken.
  block_1: "Yrkesprognoser gör det möjligt att visa prognoser över rekryteringsbehovet inom olika yrken och yrkesgrupper på ett respektive fem års sikt. Prognoserna bygger på intervjuer med arbetsgivare som genomförs två gånger per år av Statistiska Centralbyrån och Arbetsförmedlingen. API:et ger även information om vilken konkurrenssituation en arbetssökande kan vänta sig inom olika yrken.\n \n## Vem kan ha nytta av produkten?\nAPI:et är användbart för alla företag och organisationer som erbjuder tjänster inom yrkes- och studievägledning och som vill hjälpa sina målgrupper att fatta välgrundade beslut inför yrkes- och studieval. Aktörer som vill ha fördjupade insikter om utvecklingen på arbetsmarknaden har också nytta av API:et.\n"
  block_2: |-
    ### Viktigt!
    Yrkesprognoser API avvecklas, inga nya prognoser kommer att publiceras genom detta API. Det pågår ett utvecklingsarbete med att ta fram yrkesprognoser för 2021. Publiceringsdatum för nya bedömningar är i nuläget inte fastställt. Yrkesprognoser för år 2021 kommer att tillgängliggöras som nedladdningsbar fil (nedladdningslänk publiceras här).

    COVID 19: Covid-19 har förändrat arbetsmarknadsläget för vissa yrken. De prognoser som ska tolkas med försiktighet har markerats i filen [Bedömningar att flagga](https://www.jobtechdev.se/user/pages/files/covid.csv)
  menu:
  - title: Användargränssnitt
    url: https://api.arbetsformedlingen.se/af/v2/forecasts/api/#!/forecasts/
    showInShort: '1'
  - title: Kom igång
    url: https://github.com/Jobtechdev-content/Yrkesprognoser-content/blob/develop/GettingStartedOccupationForecastSE.md
    showInShort: '1'
  product_info:
  - title: Version
    value: 1.0.84
  - title: Licens
    value: CC
  contact_email: josefin.berndtson@arbetsformedlingen.se
  contact_name: Josefin Berndtson
taxonomy:
  category:
  - API
  type:
  - Öppna data
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/02.products/15.yrkesprognoser/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/02.products/15.yrkesprognoser/component_page.sv.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/15.yrkesprognoser/component_page.sv.md
tags:
- user::pagesz::02.products::15.yrkesprognoser::component_page.sv.md
- pagesz::02.products::15.yrkesprognoser::component_page.sv.md
- 02.products::15.yrkesprognoser::component_page.sv.md
- 15.yrkesprognoser::component_page.sv.md
- component_page.sv.md
---
---
title: Yrkesprognoser
custom:
    title: Yrkesprognoser
    description: 'Prognoser för rekryteringsbehov och konkurrenssituation inom olika yrken.'
    block_1: "Yrkesprognoser gör det möjligt att visa prognoser över rekryteringsbehovet inom olika yrken och yrkesgrupper på ett respektive fem års sikt. Prognoserna bygger på intervjuer med arbetsgivare som genomförs två gånger per år av Statistiska Centralbyrån och Arbetsförmedlingen. API:et ger även information om vilken konkurrenssituation en arbetssökande kan vänta sig inom olika yrken.\n \n## Vem kan ha nytta av produkten?\nAPI:et är användbart för alla företag och organisationer som erbjuder tjänster inom yrkes- och studievägledning och som vill hjälpa sina målgrupper att fatta välgrundade beslut inför yrkes- och studieval. Aktörer som vill ha fördjupade insikter om utvecklingen på arbetsmarknaden har också nytta av API:et.\n"
    block_2: "### Viktigt!\nYrkesprognoser API avvecklas, inga nya prognoser kommer att publiceras genom detta API. Det pågår ett utvecklingsarbete med att ta fram yrkesprognoser för 2021. Publiceringsdatum för nya bedömningar är i nuläget inte fastställt. Yrkesprognoser för år 2021 kommer att tillgängliggöras som nedladdningsbar fil (nedladdningslänk publiceras här).\n\nCOVID 19: Covid-19 har förändrat arbetsmarknadsläget för vissa yrken. De prognoser som ska tolkas med försiktighet har markerats i filen [Bedömningar att flagga](https://www.jobtechdev.se/user/pages/files/covid.csv)"
    menu:
        -
            title: Användargränssnitt
            url: 'https://api.arbetsformedlingen.se/af/v2/forecasts/api/#!/forecasts/'
            showInShort: '1'
        -
            title: 'Kom igång'
            url: 'https://github.com/Jobtechdev-content/Yrkesprognoser-content/blob/develop/GettingStartedOccupationForecastSE.md'
            showInShort: '1'
    product_info:
        -
            title: Version
            value: 1.0.84
        -
            title: Licens
            value: CC
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berndtson'
taxonomy:
    category:
        - API
    type:
        - 'Öppna data'
---

