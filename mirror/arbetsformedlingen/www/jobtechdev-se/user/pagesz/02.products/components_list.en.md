---
title: Products
custom:
  showLeftMenu: false
  components:
  - page: /products/alljobads
  - page: /products/connect-once
  - page: /products/ekosystem_foer_annonser
  - page: /products/etik-och-digital-matchning
  - page: /products/giglab-sverige
  - page: /products/historical-jobs
  - page: /products/jobad-enrichments
  - page: /products/jobsearch
  - page: /products/jobstream
  - page: /products/jobtech-atlas
  - page: /products/jobtech-taxonomy
  - page: /products/yrkesprognoser
  - page: /products/open-plattforms
  text: '##### Here we present the open data and technical solutions that are freely available for anyone to use and create benefits for more.'
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/02.products/components_list.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/02.products/components_list.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/components_list.en.md
tags:
- user::pagesz::02.products::components_list.en.md
- pagesz::02.products::components_list.en.md
- 02.products::components_list.en.md
- components_list.en.md
---
---
title: Products
custom:
    showLeftMenu: false
    components:
        -
            page: /products/alljobads
        -
            page: /products/connect-once
        -
            page: /products/ekosystem_foer_annonser
        -
            page: /products/etik-och-digital-matchning
        -
            page: /products/giglab-sverige
        -
            page: /products/historical-jobs
        -
            page: /products/jobad-enrichments
        -
            page: /products/jobsearch
        -
            page: /products/jobstream
        -
            page: /products/jobtech-atlas
        -
            page: /products/jobtech-taxonomy
        -
            page: /products/yrkesprognoser
        -
            page: /products/open-plattforms
    text: '##### Here we present the open data and technical solutions that are freely available for anyone to use and create benefits for more.'
---

