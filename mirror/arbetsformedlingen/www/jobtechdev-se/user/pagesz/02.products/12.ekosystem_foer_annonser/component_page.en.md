---
title: Ecosystem for job ads
custom:
  title: Ecosystem for job ads
  contact_email: maria.dalhage@arbetsformedlingen.se
  contact_name: Maria Dalhage
  block_1: |-
    Samarbetsprojektet ”Ekosystem för annonser” innebär att Sveriges största annons- och matchningsaktörer öppnar upp och delar med sig av jobbannonsdata i ett gemensamt digitalt ekosystem. Målsättningen är att göra det lättare för arbetssökande att navigera bland platsannonser och hitta jobb, oavsett plattform, samt att skapa ett större underlag till SCB:s arbetsmarknadsstatistik. Sedan februari 2021 testas den tekniska lösningen genom en pilot i Platsbanken.

    Vinsterna med samarbetet är många:

    - Vi skapar bättre statistik på svensk arbetsmarknad
    - Vi gör det enklare för våra gemensamma användare att söka bland hela arbetsmarknadens utbud av annonser.
    - Vi lotsar användare till de externa webbplatser som deltar i ekosystemet.
  block_2: |-
    ### Vill du vara med?
    Är du och din organisation intresserad av att ansluta till samarbetet? Läs mer på Wiki-sidan eller kontakta oss för mer information.
  description: Collects job advertisement data from private and public actors in a common digital ecosystem for better labor market statistics.
  menu:
  - title: Wiki
    url: https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home
    showInShort: '1'
taxonomy:
  category:
  - Cooperation
  type:
  - Open data
  status:
  - Ongoing
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/blob/main//user/pagesz/02.products/12.ekosystem_foer_annonser/component_page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se
gitdir-file-path: /user/pagesz/02.products/12.ekosystem_foer_annonser/component_page.en.md
date: '2023-11-10 14:25:58'
path: /arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/12.ekosystem_foer_annonser/component_page.en.md
tags:
- user::pagesz::02.products::12.ekosystem_foer_annonser::component_page.en.md
- pagesz::02.products::12.ekosystem_foer_annonser::component_page.en.md
- 02.products::12.ekosystem_foer_annonser::component_page.en.md
- 12.ekosystem_foer_annonser::component_page.en.md
- component_page.en.md
---
---
title: 'Ecosystem for job ads'
custom:
    title: 'Ecosystem for job ads'
    contact_email: maria.dalhage@arbetsformedlingen.se
    contact_name: 'Maria Dalhage'
    block_1: "Samarbetsprojektet ”Ekosystem för annonser” innebär att Sveriges största annons- och matchningsaktörer öppnar upp och delar med sig av jobbannonsdata i ett gemensamt digitalt ekosystem. Målsättningen är att göra det lättare för arbetssökande att navigera bland platsannonser och hitta jobb, oavsett plattform, samt att skapa ett större underlag till SCB:s arbetsmarknadsstatistik. Sedan februari 2021 testas den tekniska lösningen genom en pilot i Platsbanken.\n\nVinsterna med samarbetet är många:\n\n- Vi skapar bättre statistik på svensk arbetsmarknad\n- Vi gör det enklare för våra gemensamma användare att söka bland hela arbetsmarknadens utbud av annonser.\n- Vi lotsar användare till de externa webbplatser som deltar i ekosystemet."
    block_2: "### Vill du vara med?\nÄr du och din organisation intresserad av att ansluta till samarbetet? Läs mer på Wiki-sidan eller kontakta oss för mer information."
    description: 'Collects job advertisement data from private and public actors in a common digital ecosystem for better labor market statistics.'
    menu:
        -
            title: Wiki
            url: 'https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home'
            showInShort: '1'
taxonomy:
    category:
        - Cooperation
    type:
        - 'Open data'
    status:
        - Ongoing
---

