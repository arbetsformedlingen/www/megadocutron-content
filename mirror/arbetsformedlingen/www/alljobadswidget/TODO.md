---
title: All Job Ads Widget
gitlaburl: https://gitlab.com/arbetsformedlingen/www/alljobadswidget/-/blob/main//TODO.md
gitdir: /arbetsformedlingen/www/alljobadswidget
gitdir-file-path: /TODO.md
date: '2023-08-28 19:27:25'
path: /arbetsformedlingen/www/alljobadswidget/TODO.md
tags:
- TODO.md
---
# All Job Ads Widget

TODO (not in priority order)
----
- Unit testing.
- Loading indicators for all ajax requests. 
- Better and more: Error/Exception handling
- Ability to change/adjust looks and feels
- Add more and detailed usage description

Bugs
----
- I'm sure there is some bugs...
- Microsoft Edge does not display the modal window properly.

New Features ?
--------------
