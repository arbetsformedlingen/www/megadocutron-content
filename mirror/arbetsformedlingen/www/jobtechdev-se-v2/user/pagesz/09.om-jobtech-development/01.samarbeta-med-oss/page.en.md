---
title: Cooperate with us
custom:
  content: "### Cooperate with us<BR>\n## By collaborating, we make it easier for jobseekers and employers to find each other in digital channels and thus contribute to a better labor market. That is the logic behind JobTech Development. The collaboration can look in many different ways and take place at different levels.\n\n###### Use APIs and open data\nOne way to collaborate with JobTech Development is to use the APIs, technology and open data that are available via our platform. Anyone is welcome to use everything on the platform completely free of charge.\n- Check out our APIs\n- Contact us if you need help getting started\n    \n    \n###### Share knowledge, experience, technology or data  \nAnother way of working with us is for you and your organization to share your challenges, opportunities and wishes, so that we have a better understanding of what needs exist and can meet them. You can also share technology or data with others via the platform. No compensation is paid for collaborations.\n\n- Contact our [Community manager](mailto:josefin.berndtson@arbetsformedlingen.se) so we can discuss further\n\n###### Feedback\nGiving us feedback on what works well / less well is also an excellent collaboration and an important contribution to the continued development of JobTech Development. \n\n- Report a bug or make a suggestion directly to the team via [Gitlab](https://gitlab.com/groups/arbetsformedlingen/-/issues)\n- Tell us what you miss or want in ours [Community](https://forum.jobtechdev.se) or [form](https://link.webropolsurveys.com/S/C1F32BE6730E1171).\n<br><br><br><br>"
  menu:
  - title: Cooperate with us
    url: /en/about-jobtech-development/samarbeta-med-oss
  - title: Get inspired by others
    url: /en/about-jobtech-development/inspireras-av-andra
  - title: Contact us
    url: /en/about-jobtech-development/kontakta-oss
routes: {}
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pagesz/09.om-jobtech-development/01.samarbeta-med-oss/page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pagesz/09.om-jobtech-development/01.samarbeta-med-oss/page.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/09.om-jobtech-development/01.samarbeta-med-oss/page.en.md
tags:
- user::pagesz::09.om-jobtech-development::01.samarbeta-med-oss::page.en.md
- pagesz::09.om-jobtech-development::01.samarbeta-med-oss::page.en.md
- 09.om-jobtech-development::01.samarbeta-med-oss::page.en.md
- 01.samarbeta-med-oss::page.en.md
- page.en.md
---
---
title: 'Cooperate with us'
custom:
    content: "### Cooperate with us<BR>\n## By collaborating, we make it easier for jobseekers and employers to find each other in digital channels and thus contribute to a better labor market. That is the logic behind JobTech Development. The collaboration can look in many different ways and take place at different levels.\n\n###### Use APIs and open data\nOne way to collaborate with JobTech Development is to use the APIs, technology and open data that are available via our platform. Anyone is welcome to use everything on the platform completely free of charge.\n- Check out our APIs\n- Contact us if you need help getting started\n    \n    \n###### Share knowledge, experience, technology or data  \nAnother way of working with us is for you and your organization to share your challenges, opportunities and wishes, so that we have a better understanding of what needs exist and can meet them. You can also share technology or data with others via the platform. No compensation is paid for collaborations.\n\n- Contact our [Community manager](mailto:josefin.berndtson@arbetsformedlingen.se) so we can discuss further\n\n###### Feedback\nGiving us feedback on what works well / less well is also an excellent collaboration and an important contribution to the continued development of JobTech Development. \n\n- Report a bug or make a suggestion directly to the team via [Gitlab](https://gitlab.com/groups/arbetsformedlingen/-/issues)\n- Tell us what you miss or want in ours [Community](https://forum.jobtechdev.se) or [form](https://link.webropolsurveys.com/S/C1F32BE6730E1171).\n<br><br><br><br>"
    menu:
        -
            title: 'Cooperate with us'
            url: /en/about-jobtech-development/samarbeta-med-oss
        -
            title: 'Get inspired by others'
            url: /en/about-jobtech-development/inspireras-av-andra
        -
            title: 'Contact us'
            url: /en/about-jobtech-development/kontakta-oss
routes: {  }
---

