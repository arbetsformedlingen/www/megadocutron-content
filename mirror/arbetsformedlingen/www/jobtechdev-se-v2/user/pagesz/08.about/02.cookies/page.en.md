---
title: Cookie policy
custom:
  content: |-
    Arbetsförmedlingen, organisationsnummer 202100-2114, är ansvarig för webbplatsen jobtechdev.se.

    Nedan beskrivs hur Arbetsförmedlingen hanterar webbkakor (kakor) på webbplatsen.

    Policyn omfattar samtliga användare av webbplatsen jobtechdev.se.

    **Vad innebär det att en webbplats använder kakor?**

    Alla som besöker en webbplats som använder kakor måste enligt lag få information om att webbplatsen innehåller kakor, vad de används till och hur du kan välja bort dem.

    En kaka är en liten textfil som lagras på användarens dator och innehåller information. Kakor används normalt för att förbättra webbplatsen för dig som besökare. Det finns två typer av kakor:

    En permanent kaka som ligger kvar på din dator under en bestämd tid
    Sessionskaka som lagras tillfälligt i datorns minne under en viss tid och som försvinner när du stänger webbläsaren
    Mer information om kakor och den nya lagen om elektronisk kommunikation finns på Post och Telestyrelsens website.

    **Godkännande av användandet av kakor på webbplatsen jobtechdev.se**

    Användandet av kakor på jobtechdev.se kräver samtycke av användaren. Genom ditt uttryckliga samtycke godkänner du användningen av kakor på webbplatsen samt behandlingen av personuppgifter som sparas genom användandet av kakor. Det finns ingen möjlighet att neka samtycke till någon eller några kakor utan användningen godkänns i sin helhet.

    **Vad händer om du tackar nej till kakor på jobtechdev.se?**

    Arbetsförmedlingen använder kakor för att webbplatsen ska fungera ändamålsenligt. Om du via dina browserinställningar väljer att tacka nej till kakor, kan vissa tjänster och funktioner på webbplatsen påverkas.

    Om du inte vill att kakor används på din dator, kan du ändra inställningen i webbläsarens säkerhetsinställningar. Då lagras inga kakor men du kommer inte heller att kunna använda vissa funktioner på webbplatsen. Viktigt att tänka på är att du då inte kan använda de tjänster som kräver inloggning.

    **Vilka kakor används då du besöker jobtechdev.se?**

    Vi använder sig av kakor för följande ändamål:

    För att förbättra användandet av jobtechdev.se. Kakor som används för detta ändamål är uteslutande så kallade sessionskakor. Syftet med dessa kakor är för att få applikationen att fungera ändamålsenligt, till exempel för att veta om en användare är inloggad och att hålla sessionen igång eller för att minnas att användaren har godkänt kakanvändningen eller för att möjliggöra en säker inloggning till programvaran. Dessa kakor kan innehålla personuppgifter (t ex för inloggningskakor skrivs användarens användarnamn, normalt dennes e-postadress, ner i den tillfälliga kakan och står där så länge personen är inloggad).
    Därutöver förekommer är ett antal kakor när jobtechdev.se är aktivt, men som inte vi själva skapar eller använder:

    Vid användandet av tredjeparts programvaror (Youtube, Minio etc.) används så kallade tredjepartskakor av både karaktären permanenta kakor och sessionskakor. De används exempelvis för att möjliggöra att lagra logotyper om inloggade företag som väljer att publicera loggor i jobtechdev.se. De används också vid användandet av länkning till sociala medier om ett företag väljer att presentera sin tjänst via en sådan.
    I annonseringssyfte. Google (inte jobtechdev.se) sätter kakor som i regel är av permanent karaktär.

    **Behandling av personuppgifter på jobtechdev.se**

    Arbetsförmedlingen behandlar den information som skapas av de kakor som används när du besöker jobtechdev.se. Det är uppgifter så som din IP-adress.

    Dina personuppgifter behandlas för ändamålen att användarna av jobtechdev.se ska kunna använda tjänsten på ett funktionellt och avsett sätt och för att utveckla jobtechdev.se. Vi behandlar dina personuppgifter som sparas genom användningen av kakor med stöd av ditt uttryckliga samtycke.

    Vem är Personuppgiftsansvarig för behandlingen av dina personuppgifter vid användningen av cookies?

    Arbetsförmedlingen, organisationsnummer 202100-2114, Hälsingegatan 38, 113 99 Stockholm, är personuppgiftsansvarig för behandlingen av dina personuppgifter.
  menu:
  - title: Availability (in Swedish)
    url: /en/about-the-website/availability
  - title: Cookie policy (in Swedish)
    url: /en/about-the-website/cookies
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pagesz/08.about/02.cookies/page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pagesz/08.about/02.cookies/page.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/08.about/02.cookies/page.en.md
tags:
- user::pagesz::08.about::02.cookies::page.en.md
- pagesz::08.about::02.cookies::page.en.md
- 08.about::02.cookies::page.en.md
- 02.cookies::page.en.md
- page.en.md
---
---
title: 'Cookie policy'
custom:
    content: "Arbetsförmedlingen, organisationsnummer 202100-2114, är ansvarig för webbplatsen jobtechdev.se.\n\nNedan beskrivs hur Arbetsförmedlingen hanterar webbkakor (kakor) på webbplatsen.\n\nPolicyn omfattar samtliga användare av webbplatsen jobtechdev.se.\n\n**Vad innebär det att en webbplats använder kakor?**\n\nAlla som besöker en webbplats som använder kakor måste enligt lag få information om att webbplatsen innehåller kakor, vad de används till och hur du kan välja bort dem.\n\nEn kaka är en liten textfil som lagras på användarens dator och innehåller information. Kakor används normalt för att förbättra webbplatsen för dig som besökare. Det finns två typer av kakor:\n\nEn permanent kaka som ligger kvar på din dator under en bestämd tid\nSessionskaka som lagras tillfälligt i datorns minne under en viss tid och som försvinner när du stänger webbläsaren\nMer information om kakor och den nya lagen om elektronisk kommunikation finns på Post och Telestyrelsens website.\n\n**Godkännande av användandet av kakor på webbplatsen jobtechdev.se**\n\nAnvändandet av kakor på jobtechdev.se kräver samtycke av användaren. Genom ditt uttryckliga samtycke godkänner du användningen av kakor på webbplatsen samt behandlingen av personuppgifter som sparas genom användandet av kakor. Det finns ingen möjlighet att neka samtycke till någon eller några kakor utan användningen godkänns i sin helhet.\n\n**Vad händer om du tackar nej till kakor på jobtechdev.se?**\n\nArbetsförmedlingen använder kakor för att webbplatsen ska fungera ändamålsenligt. Om du via dina browserinställningar väljer att tacka nej till kakor, kan vissa tjänster och funktioner på webbplatsen påverkas.\n\nOm du inte vill att kakor används på din dator, kan du ändra inställningen i webbläsarens säkerhetsinställningar. Då lagras inga kakor men du kommer inte heller att kunna använda vissa funktioner på webbplatsen. Viktigt att tänka på är att du då inte kan använda de tjänster som kräver inloggning.\n\n**Vilka kakor används då du besöker jobtechdev.se?**\n\nVi använder sig av kakor för följande ändamål:\n\nFör att förbättra användandet av jobtechdev.se. Kakor som används för detta ändamål är uteslutande så kallade sessionskakor. Syftet med dessa kakor är för att få applikationen att fungera ändamålsenligt, till exempel för att veta om en användare är inloggad och att hålla sessionen igång eller för att minnas att användaren har godkänt kakanvändningen eller för att möjliggöra en säker inloggning till programvaran. Dessa kakor kan innehålla personuppgifter (t ex för inloggningskakor skrivs användarens användarnamn, normalt dennes e-postadress, ner i den tillfälliga kakan och står där så länge personen är inloggad).\nDärutöver förekommer är ett antal kakor när jobtechdev.se är aktivt, men som inte vi själva skapar eller använder:\n\nVid användandet av tredjeparts programvaror (Youtube, Minio etc.) används så kallade tredjepartskakor av både karaktären permanenta kakor och sessionskakor. De används exempelvis för att möjliggöra att lagra logotyper om inloggade företag som väljer att publicera loggor i jobtechdev.se. De används också vid användandet av länkning till sociala medier om ett företag väljer att presentera sin tjänst via en sådan.\nI annonseringssyfte. Google (inte jobtechdev.se) sätter kakor som i regel är av permanent karaktär.\n\n**Behandling av personuppgifter på jobtechdev.se**\n\nArbetsförmedlingen behandlar den information som skapas av de kakor som används när du besöker jobtechdev.se. Det är uppgifter så som din IP-adress.\n\nDina personuppgifter behandlas för ändamålen att användarna av jobtechdev.se ska kunna använda tjänsten på ett funktionellt och avsett sätt och för att utveckla jobtechdev.se. Vi behandlar dina personuppgifter som sparas genom användningen av kakor med stöd av ditt uttryckliga samtycke.\n\nVem är Personuppgiftsansvarig för behandlingen av dina personuppgifter vid användningen av cookies?\n\nArbetsförmedlingen, organisationsnummer 202100-2114, Hälsingegatan 38, 113 99 Stockholm, är personuppgiftsansvarig för behandlingen av dina personuppgifter."
    menu:
        -
            title: 'Availability (in Swedish)'
            url: /en/about-the-website/availability
        -
            title: 'Cookie policy (in Swedish)'
            url: /en/about-the-website/cookies
---

