---
title: Tillgänglighet
custom:
  content: "### Tillgänglighetsredogörelse\n## Arbetsförmedlingen är ägare till den här webbplatsen. Vi vill att alla ska kunna använda webbplatsen, oavsett behov. Här redogör vi hur jobtechdev.se uppfyller lagen om tillgänglighet till digital offentlig service, eventuella kända tillgänglighetsproblem och hur du kan rapportera brister till oss så att vi kan åtgärda dem.\n\n**Hur tillgänglig är webbplatsen?**  \nVi är medvetna om att delar av webbplatsen inte är helt tillgängliga. På den här sidan hittar du listor där vi redovisar kända brister i tillgänglighet.\n\n**Kontakta oss om du hittar fler brister**  \nVi strävar hela tiden efter att förbättra webbplatsens tillgänglighet. Om du upptäcker problem som inte är beskrivna, eller om du anser att vi inte uppfyller lagens krav, meddela oss så att vi får veta att problemet finns.\n\n**Kontakta tillsynsmyndigheten**  \nMyndigheten för digital förvaltning har ansvaret för tillsyn för lagen om tillgänglighet till digital offentlig service. Om du inte är nöjd med hur vi hanterar dina synpunkter kan du kontakta Myndigheten för digital förvaltning och berätta det.\n\n**Teknisk information om webbplatsens tillgänglighet**  \nDen här webbplatsen är inte förenlig med lagen om tillgänglighet till digital offentlig service, på grund av de brister som beskrivs nedan.\n\n**Innehåll som inte är tillgängligt**  \nVi är medvetna om följande brister som inte följer lagkraven:\n\n**Hur vi har testat webbplatsen**  \nVi har gjort en självskattning (intern testning) av jobtechdev.se.<br><br><br><br>"
  menu:
  - title: Tillgänglighet
    url: /sv/om-webbplatsen/availability
  - title: Cookie policy
    url: /sv/om-webbplatsen/cookies
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pagesz/08.about/01.availability/page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pagesz/08.about/01.availability/page.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/08.about/01.availability/page.sv.md
tags:
- user::pagesz::08.about::01.availability::page.sv.md
- pagesz::08.about::01.availability::page.sv.md
- 08.about::01.availability::page.sv.md
- 01.availability::page.sv.md
- page.sv.md
---
---
title: Tillgänglighet
custom:
    content: "### Tillgänglighetsredogörelse\n## Arbetsförmedlingen är ägare till den här webbplatsen. Vi vill att alla ska kunna använda webbplatsen, oavsett behov. Här redogör vi hur jobtechdev.se uppfyller lagen om tillgänglighet till digital offentlig service, eventuella kända tillgänglighetsproblem och hur du kan rapportera brister till oss så att vi kan åtgärda dem.\n\n**Hur tillgänglig är webbplatsen?**  \nVi är medvetna om att delar av webbplatsen inte är helt tillgängliga. På den här sidan hittar du listor där vi redovisar kända brister i tillgänglighet.\n\n**Kontakta oss om du hittar fler brister**  \nVi strävar hela tiden efter att förbättra webbplatsens tillgänglighet. Om du upptäcker problem som inte är beskrivna, eller om du anser att vi inte uppfyller lagens krav, meddela oss så att vi får veta att problemet finns.\n\n**Kontakta tillsynsmyndigheten**  \nMyndigheten för digital förvaltning har ansvaret för tillsyn för lagen om tillgänglighet till digital offentlig service. Om du inte är nöjd med hur vi hanterar dina synpunkter kan du kontakta Myndigheten för digital förvaltning och berätta det.\n\n**Teknisk information om webbplatsens tillgänglighet**  \nDen här webbplatsen är inte förenlig med lagen om tillgänglighet till digital offentlig service, på grund av de brister som beskrivs nedan.\n\n**Innehåll som inte är tillgängligt**  \nVi är medvetna om följande brister som inte följer lagkraven:\n\n**Hur vi har testat webbplatsen**  \nVi har gjort en självskattning (intern testning) av jobtechdev.se.<br><br><br><br>"
    menu:
        -
            title: Tillgänglighet
            url: /sv/om-webbplatsen/availability
        -
            title: 'Cookie policy'
            url: /sv/om-webbplatsen/cookies
---

