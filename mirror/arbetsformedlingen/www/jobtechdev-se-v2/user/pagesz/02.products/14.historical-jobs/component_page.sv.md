---
title: Historical jobs
custom:
  title: Historiska jobb
  description: Alla jobbannonser som har publicerats hos Arbetsförmedlingen sedan 2006.
  contact_email: josefin.berndtson@arbetsformedlingen.se
  contact_name: Josefin Berndtson
  menu:
  - title: Utforska
    url: http://historik.azurewebsites.net
    showInShort: '1'
  - title: Kom igång
    url: https://github.com/Jobtechdev-content/HistoricalJobs-content/blob/develop/GettingStartedHistoricalJobsSE.md
    showInShort: '1'
  block_1: "Historiska jobb är ett dataset med alla jobbannonser som har publicerats på Platsbanken sedan 2006. Det innehåller även metadata (information om tjänst, arbetsgivare, ort, tidpunkt osv) och fritextinformation. Datasetet uppdateras kontinuerligt.\n \n## Vem kan ha nytta av produkten?\nHistoriska jobb är användbart för alla företag och organisationer som behöver stora mängder annonsdata för statistik och analyser inom arbetsmarknadsområdet. Det stora dataunderlaget kan även användas för att bygga och träna algoritmer i samband med maskininlärning samt för utveckling av nya digitala matchningstjänster."
  block_2: |-
    Namngivning av attribut för annonsobjekt följer namngivningen som används i JobStream. Senare års annonsobjekt innehåller fler attribut än tidigare. Värden för attribut som inte finns är null.

    ### Ladda ner
    Alla: [2006-2020.rar](https://simonbe.blob.core.windows.net/afhistorik/pb_2006_2020.rar) / [2006-2020.zip](https://simonbe.blob.core.windows.net/afhistorik/pb_2006_2020.zip)

    År för år: [2006.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2006.rar), [2007.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2007.rar), [2008.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2008.rar), [2009.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2009.rar), [2010.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2010.rar), [2011.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2011.rar), [2012.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2012.rar), [2013.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2013.rar), [2014.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2014.rar), [2015.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2015.rar), [2016.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2016.rar), [2017.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2017.rar), [2018.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2018.rar), [2019.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2019.rar), [2020.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2020.rar)
  product_info:
  - title: Dataformat
    value: JSON
taxonomy:
  category:
  - Dataset
  type:
  - Öppna data
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pagesz/02.products/14.historical-jobs/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pagesz/02.products/14.historical-jobs/component_page.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/14.historical-jobs/component_page.sv.md
tags:
- user::pagesz::02.products::14.historical-jobs::component_page.sv.md
- pagesz::02.products::14.historical-jobs::component_page.sv.md
- 02.products::14.historical-jobs::component_page.sv.md
- 14.historical-jobs::component_page.sv.md
- component_page.sv.md
---
---
title: 'Historical jobs'
custom:
    title: 'Historiska jobb'
    description: 'Alla jobbannonser som har publicerats hos Arbetsförmedlingen sedan 2006.'
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berndtson'
    menu:
        -
            title: Utforska
            url: 'http://historik.azurewebsites.net'
            showInShort: '1'
        -
            title: 'Kom igång'
            url: 'https://github.com/Jobtechdev-content/HistoricalJobs-content/blob/develop/GettingStartedHistoricalJobsSE.md'
            showInShort: '1'
    block_1: "Historiska jobb är ett dataset med alla jobbannonser som har publicerats på Platsbanken sedan 2006. Det innehåller även metadata (information om tjänst, arbetsgivare, ort, tidpunkt osv) och fritextinformation. Datasetet uppdateras kontinuerligt.\n \n## Vem kan ha nytta av produkten?\nHistoriska jobb är användbart för alla företag och organisationer som behöver stora mängder annonsdata för statistik och analyser inom arbetsmarknadsområdet. Det stora dataunderlaget kan även användas för att bygga och träna algoritmer i samband med maskininlärning samt för utveckling av nya digitala matchningstjänster."
    block_2: "Namngivning av attribut för annonsobjekt följer namngivningen som används i JobStream. Senare års annonsobjekt innehåller fler attribut än tidigare. Värden för attribut som inte finns är null.\n\n### Ladda ner\nAlla: [2006-2020.rar](https://simonbe.blob.core.windows.net/afhistorik/pb_2006_2020.rar) / [2006-2020.zip](https://simonbe.blob.core.windows.net/afhistorik/pb_2006_2020.zip)\n\nÅr för år: [2006.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2006.rar), [2007.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2007.rar), [2008.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2008.rar), [2009.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2009.rar), [2010.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2010.rar), [2011.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2011.rar), [2012.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2012.rar), [2013.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2013.rar), [2014.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2014.rar), [2015.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2015.rar), [2016.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2016.rar), [2017.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2017.rar), [2018.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2018.rar), [2019.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2019.rar), [2020.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2020.rar)"
    product_info:
        -
            title: Dataformat
            value: JSON
taxonomy:
    category:
        - Dataset
    type:
        - 'Öppna data'
---

