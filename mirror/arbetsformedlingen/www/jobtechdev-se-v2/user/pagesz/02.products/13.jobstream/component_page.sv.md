---
title: JobStream
custom:
  title: JobStream
  description: Realtidsdata från alla jobbannonser som är publicerade hos Arbetsförmedlingen.
  block_1: "JobStream ger tillgång till alla jobbannonser som är publicerade i Platsbanken, inklusive realtidsinformation om förändringar som sker runt eller i dessa annonser, t.ex. publiceringar, avpubliceringar eller uppdateringar av annonstexter.\n \n## Vem kan ha nytta av produkten?\nJobStream är användbart för alla företag och organisationer som har en egen sökmotor och vill förbättra sin tjänst med realtidsuppdaterad annonsdata. API:et kan också användas för att bygga och träna algoritmer i samband med maskininlärning eller för att identifiera och analysera trender på arbetsmarknaden utifrån annonsdata."
  menu:
  - title: Användargränssnitt
    url: https://jobstream.api.jobtechdev.se
    showInShort: '1'
  - title: API-nyckel
    url: https://apirequest.jobtechdev.se
    showInShort: '1'
  - title: Kom igång
    url: https://github.com/Jobtechdev-content/Jobstream-content/blob/develop/GettingStartedJobStreamSE.md
    showInShort: '1'
  - title: Kodexempel
    url: https://github.com/JobtechSwe/getting-started-code-examples
    showInShort: null
  product_info:
  - title: Version
    value: 1.17.1
  - title: Dataformat
    value: JSON
  contact_email: josefin.berntson@arbetsformedlingen.se
  contact_name: Josefin Berndtson
taxonomy:
  category:
  - API
  type:
  - Öppna data
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pagesz/02.products/13.jobstream/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pagesz/02.products/13.jobstream/component_page.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/13.jobstream/component_page.sv.md
tags:
- user::pagesz::02.products::13.jobstream::component_page.sv.md
- pagesz::02.products::13.jobstream::component_page.sv.md
- 02.products::13.jobstream::component_page.sv.md
- 13.jobstream::component_page.sv.md
- component_page.sv.md
---
---
title: JobStream
custom:
    title: JobStream
    description: 'Realtidsdata från alla jobbannonser som är publicerade hos Arbetsförmedlingen.'
    block_1: "JobStream ger tillgång till alla jobbannonser som är publicerade i Platsbanken, inklusive realtidsinformation om förändringar som sker runt eller i dessa annonser, t.ex. publiceringar, avpubliceringar eller uppdateringar av annonstexter.\n \n## Vem kan ha nytta av produkten?\nJobStream är användbart för alla företag och organisationer som har en egen sökmotor och vill förbättra sin tjänst med realtidsuppdaterad annonsdata. API:et kan också användas för att bygga och träna algoritmer i samband med maskininlärning eller för att identifiera och analysera trender på arbetsmarknaden utifrån annonsdata."
    menu:
        -
            title: Användargränssnitt
            url: 'https://jobstream.api.jobtechdev.se'
            showInShort: '1'
        -
            title: API-nyckel
            url: 'https://apirequest.jobtechdev.se'
            showInShort: '1'
        -
            title: 'Kom igång'
            url: 'https://github.com/Jobtechdev-content/Jobstream-content/blob/develop/GettingStartedJobStreamSE.md'
            showInShort: '1'
        -
            title: Kodexempel
            url: 'https://github.com/JobtechSwe/getting-started-code-examples'
            showInShort: null
    product_info:
        -
            title: Version
            value: 1.17.1
        -
            title: Dataformat
            value: JSON
    contact_email: josefin.berntson@arbetsformedlingen.se
    contact_name: 'Josefin Berndtson'
taxonomy:
    category:
        - API
    type:
        - 'Öppna data'
---

