---
title: JobSearch
custom:
  title: JobSearch
  block_1: |-
    JobSearch is useful for all companies and organizations that want to use job ads from Platsbanken but do not have their own search engine.

    It is also useful for players who have access to large amounts of structured data and want to be able to offer special search functions or niche advertising platforms.
  description: Search engine that makes it possible to search and filter among all job ads on the Arbetsförmedlingen advertising platform Platsbanken.
  menu:
  - title: User interface
    url: https://jobsearch.api.jobtechdev.se
    showInShort: '1'
  - title: API key
    url: https://apirequest.jobtechdev.se
    showInShort: '1'
  - title: Getting started
    url: https://github.com/Jobtechdev-content/Jobsearch-content/blob/master/GettingStartedJobSearchEN.md
    showInShort: '1'
  block_2: |-
    ### Notice
    For this API you need to register an API key. We may invalidate your key if you make large numbers of calls that do not fit the intended purpose of JobSearch.
  product_info:
  - title: Version
    value: '1.0'
  contact_email: josefin.berndtson@arbetsformedlingen.se
  contact_name: Josefin Berndtson
taxonomy:
  category:
  - API
  type:
  - Open data
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pagesz/02.products/11.jobsearch/component_page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pagesz/02.products/11.jobsearch/component_page.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/11.jobsearch/component_page.en.md
tags:
- user::pagesz::02.products::11.jobsearch::component_page.en.md
- pagesz::02.products::11.jobsearch::component_page.en.md
- 02.products::11.jobsearch::component_page.en.md
- 11.jobsearch::component_page.en.md
- component_page.en.md
---
---
title: JobSearch
custom:
    title: JobSearch
    block_1: "JobSearch is useful for all companies and organizations that want to use job ads from Platsbanken but do not have their own search engine.\n\nIt is also useful for players who have access to large amounts of structured data and want to be able to offer special search functions or niche advertising platforms."
    description: 'Search engine that makes it possible to search and filter among all job ads on the Arbetsförmedlingen advertising platform Platsbanken.'
    menu:
        -
            title: 'User interface'
            url: 'https://jobsearch.api.jobtechdev.se'
            showInShort: '1'
        -
            title: 'API key'
            url: 'https://apirequest.jobtechdev.se'
            showInShort: '1'
        -
            title: 'Getting started'
            url: 'https://github.com/Jobtechdev-content/Jobsearch-content/blob/master/GettingStartedJobSearchEN.md'
            showInShort: '1'
    block_2: "### Notice\nFor this API you need to register an API key. We may invalidate your key if you make large numbers of calls that do not fit the intended purpose of JobSearch."
    product_info:
        -
            title: Version
            value: '1.0'
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berndtson'
taxonomy:
    category:
        - API
    type:
        - 'Open data'
---

