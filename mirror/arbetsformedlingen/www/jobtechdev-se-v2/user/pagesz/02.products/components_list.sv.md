---
title: Produkter
custom:
  showLeftMenu: false
  components:
  - page: /products/alljobads
  - page: /products/connect-once
  - page: /products/ekosystem_foer_annonser
  - page: /products/etik-och-digital-matchning
  - page: /products/giglab-sverige
  - page: /products/historical-jobs
  - page: /products/jobad-enrichments
  - page: /products/jobsearch
  - page: /products/jobstream
  - page: /products/jobtech-atlas
  - page: /products/jobtech-taxonomy
  - page: /products/open-plattforms
  - page: /products/yrkesprognoser
  text: '##### Här presenterar vi de öppna data och tekniska lösningar som är fritt tillgängliga för vem som helst att använda och skapa nytta för fler. Kontakta oss om du vill hjälpa till och bidra tillbaka.'
routes:
  default: /produkter
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pagesz/02.products/components_list.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pagesz/02.products/components_list.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/components_list.sv.md
tags:
- user::pagesz::02.products::components_list.sv.md
- pagesz::02.products::components_list.sv.md
- 02.products::components_list.sv.md
- components_list.sv.md
---
---
title: Produkter
custom:
    showLeftMenu: false
    components:
        -
            page: /products/alljobads
        -
            page: /products/connect-once
        -
            page: /products/ekosystem_foer_annonser
        -
            page: /products/etik-och-digital-matchning
        -
            page: /products/giglab-sverige
        -
            page: /products/historical-jobs
        -
            page: /products/jobad-enrichments
        -
            page: /products/jobsearch
        -
            page: /products/jobstream
        -
            page: /products/jobtech-atlas
        -
            page: /products/jobtech-taxonomy
        -
            page: /products/open-plattforms
        -
            page: /products/yrkesprognoser
    text: '##### Här presenterar vi de öppna data och tekniska lösningar som är fritt tillgängliga för vem som helst att använda och skapa nytta för fler. Kontakta oss om du vill hjälpa till och bidra tillbaka.'
routes:
    default: /produkter
---

