---
title: Work with us
custom:
  content: |-
    ## Vacancies / open application
    Right now we have no vacancies, but feel free to send a [open application](mailto:recruitment-jobtech@arbetsformedlingen.se) with a short description of yourself and what you can do, and we will contact you when needed.
routes:
  default: /work-with-us
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pagesz/05.work/page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pagesz/05.work/page.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/05.work/page.en.md
tags:
- user::pagesz::05.work::page.en.md
- pagesz::05.work::page.en.md
- 05.work::page.en.md
- page.en.md
---
---
title: 'Work with us'
custom:
    content: "## Vacancies / open application\nRight now we have no vacancies, but feel free to send a [open application](mailto:recruitment-jobtech@arbetsformedlingen.se) with a short description of yourself and what you can do, and we will contact you when needed."
routes:
    default: /work-with-us
---

