---
title: Onboarding and open forum
custom:
  title: Onboarding and open forum
  date: 19-05-2021 10:30
  endtime: '11:00'
  description: During the autumn, JobTech Development will arrange open webinars for anyone who wants help getting started with our APIs.
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pagesz/04.calendar/test-event/events_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pagesz/04.calendar/test-event/events_blog_post.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/04.calendar/test-event/events_blog_post.en.md
tags:
- user::pagesz::04.calendar::test-event::events_blog_post.en.md
- pagesz::04.calendar::test-event::events_blog_post.en.md
- 04.calendar::test-event::events_blog_post.en.md
- test-event::events_blog_post.en.md
- events_blog_post.en.md
---
---
title: 'Onboarding and open forum'
custom:
    title: 'Onboarding and open forum'
    date: '19-05-2021 10:30'
    endtime: '11:00'
    description: 'During the autumn, JobTech Development will arrange open webinars for anyone who wants help getting started with our APIs.'
---

