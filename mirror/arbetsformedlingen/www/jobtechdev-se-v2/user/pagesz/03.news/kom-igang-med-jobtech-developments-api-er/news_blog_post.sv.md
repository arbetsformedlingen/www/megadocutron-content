---
title: Kom igång med JobTech Developments API:er
custom:
  title: Kom igång med JobTech Developments API:er
  date: 21-06-2021 10:06
  content: |-
    ###### Under hösten arrangerar JobTech Development öppna webbinarier för alla som vill ha hjälp att komma igång med våra API:er.

    För att göra det ännu enklare att använda våra API:er anordnar vi fyra webbinarier under hösten där vi ger en teknisk introduktion och möjlighet att ställa frågor direkt till våra utvecklare. Webbinarierna vänder sig i första hand till nya och befintliga användare, men är öppna för alla som vill veta mer om hur våra API:er kan användas för utveckling av digitala tjänster för arbetsmarknaden.

    På webbinarierna kommer vårt utvecklingsteam, representerat av Johan Brymér Dahlhielm och Henrik Suzuki bl.a att ge information och praktisk vägledning för API:er som ”JobSearch”, ”JobStream” och ”JobTech Taxonomy”. Därefter följer ett öppet samtal med möjlighet att ställa frågor, ge feedback samt framföra idéer och önskemål för nya tekniska lösningar från JobTech Development.

    ”Vi ser fram emot att kunna hjälpa och inspirera nya användare genom höstens webbinarier. Genom mer dialog och feedback får vi dessutom större möjlighet att förbättra våra produkter och bli mer träffsäkra när vi utvecklar nya tekniska lösningar för arbetsmarknaden”, säger Henrik Suzuki.

    Samtliga webbinarier kommer att hållas enligt ”open space”-modell, som innebär att agenda och samtalsämnen i hög grad styrs utifrån deltagarnas specifika intressen och behov.
  short: För att göra det ännu enklare att använda våra API:er anordnar vi fyra webbinarier under hösten där vi ger en teknisk introduktion och möjlighet att ställa frågor direkt till våra utvecklare. Webbinarierna vänder sig i första hand till nya och befintliga användare, men är öppna för alla som vill veta mer om hur våra API:er kan användas för utveckling av digitala tjänster för arbetsmarknaden.
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pagesz/03.news/kom-igang-med-jobtech-developments-api-er/news_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pagesz/03.news/kom-igang-med-jobtech-developments-api-er/news_blog_post.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/kom-igang-med-jobtech-developments-api-er/news_blog_post.sv.md
tags:
- user::pagesz::03.news::kom-igang-med-jobtech-developments-api-er::news_blog_post.sv.md
- pagesz::03.news::kom-igang-med-jobtech-developments-api-er::news_blog_post.sv.md
- 03.news::kom-igang-med-jobtech-developments-api-er::news_blog_post.sv.md
- kom-igang-med-jobtech-developments-api-er::news_blog_post.sv.md
- news_blog_post.sv.md
---
---
title: 'Kom igång med JobTech Developments API:er'
custom:
    title: 'Kom igång med JobTech Developments API:er'
    date: '21-06-2021 10:06'
    content: "###### Under hösten arrangerar JobTech Development öppna webbinarier för alla som vill ha hjälp att komma igång med våra API:er.\n\nFör att göra det ännu enklare att använda våra API:er anordnar vi fyra webbinarier under hösten där vi ger en teknisk introduktion och möjlighet att ställa frågor direkt till våra utvecklare. Webbinarierna vänder sig i första hand till nya och befintliga användare, men är öppna för alla som vill veta mer om hur våra API:er kan användas för utveckling av digitala tjänster för arbetsmarknaden.\n\nPå webbinarierna kommer vårt utvecklingsteam, representerat av Johan Brymér Dahlhielm och Henrik Suzuki bl.a att ge information och praktisk vägledning för API:er som ”JobSearch”, ”JobStream” och ”JobTech Taxonomy”. Därefter följer ett öppet samtal med möjlighet att ställa frågor, ge feedback samt framföra idéer och önskemål för nya tekniska lösningar från JobTech Development.\n\n”Vi ser fram emot att kunna hjälpa och inspirera nya användare genom höstens webbinarier. Genom mer dialog och feedback får vi dessutom större möjlighet att förbättra våra produkter och bli mer träffsäkra när vi utvecklar nya tekniska lösningar för arbetsmarknaden”, säger Henrik Suzuki.\n\nSamtliga webbinarier kommer att hållas enligt ”open space”-modell, som innebär att agenda och samtalsämnen i hög grad styrs utifrån deltagarnas specifika intressen och behov."
    short: 'För att göra det ännu enklare att använda våra API:er anordnar vi fyra webbinarier under hösten där vi ger en teknisk introduktion och möjlighet att ställa frågor direkt till våra utvecklare. Webbinarierna vänder sig i första hand till nya och befintliga användare, men är öppna för alla som vill veta mer om hur våra API:er kan användas för utveckling av digitala tjänster för arbetsmarknaden.'
---

