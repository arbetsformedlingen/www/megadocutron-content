---
title: New AI solution improves job ads and streamlines digital matching services
custom:
  title: New AI solution improves job ads and streamlines digital matching services
  date: 14-09-2020 11:25
  short: New AI solution improves job ads and streamlines digital matching services
  content: |-
    Irrelevant search results and strange job proposal are something many jobseekers recognize - and are happy to avoid. With the AI ​​solution JobAd Enrichments, which is free for all matching actors to use, the problem can be avoided, while at the same time contributing to in-depth insights into the labor market.

    The JobAd Enrichments API, which is based on AI technology, automatically identifies words in ads that have real meaning in a search situation, while filtering out redundant information.

    With the help of JobAd Enrichments, it becomes much easier to navigate and quickly find your way among job advertisements in digital advertising platforms. The jobseeker does not have to spend time sorting search results and reviewing long ad texts in detail to see if they contain information that is interesting and relevant.

    The API has been tested over a year in the Swedish Public Employment Service's own platform Platsbanken. It is now also available to all players who want to improve search services or advertising platforms.

    In addition to improved matching, JobAd Enrichment can be used to gain in-depth insights into the labor market and to guide jobseekers. By compiling and analyzing structured ad data, it becomes possible to identify:
    * Skills and abilities that are in demand for different professional roles.
    * Occupations that are suitable for those who have certain skills / abilities, or which skills should be invested in to become more in demand in a particular profession.
    * If certain skills / abilities are in demand at all, and whether the interest in them increases or decreases.

    The API can also be used in the development of digital services aimed at employers. For example, there may be services that analyze and improve content in job ads:
    * How much of the ad actually consists of relevant and in-demand words?
    * What concepts are usually requested for a profession? Missing important words in our ad?
    * How unique is our ad compared to other ads in the same area? If we use words and expressions that are inflationary and thus not so useful.

    Are you a developer and want to use our new AI solution? Here you get started with JobAd Enrichments, which is a complement to our other range of open APIs for language, individual and labor market data.

    In our forum you can ask questions and participate in discussions about JobAd Enrichments or one of our other APIs.
    Welcome to join!

    You can also contact us via Email
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pagesz/03.news/10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pagesz/03.news/10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.en.md
tags:
- user::pagesz::03.news::10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster::news_blog_post.en.md
- pagesz::03.news::10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster::news_blog_post.en.md
- 03.news::10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster::news_blog_post.en.md
- 10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster::news_blog_post.en.md
- news_blog_post.en.md
---
---
title: 'New AI solution improves job ads and streamlines digital matching services'
custom:
    title: 'New AI solution improves job ads and streamlines digital matching services'
    date: '14-09-2020 11:25'
    short: 'New AI solution improves job ads and streamlines digital matching services'
    content: "Irrelevant search results and strange job proposal are something many jobseekers recognize - and are happy to avoid. With the AI ​​solution JobAd Enrichments, which is free for all matching actors to use, the problem can be avoided, while at the same time contributing to in-depth insights into the labor market.\n\nThe JobAd Enrichments API, which is based on AI technology, automatically identifies words in ads that have real meaning in a search situation, while filtering out redundant information.\n\nWith the help of JobAd Enrichments, it becomes much easier to navigate and quickly find your way among job advertisements in digital advertising platforms. The jobseeker does not have to spend time sorting search results and reviewing long ad texts in detail to see if they contain information that is interesting and relevant.\n\nThe API has been tested over a year in the Swedish Public Employment Service's own platform Platsbanken. It is now also available to all players who want to improve search services or advertising platforms.\n\nIn addition to improved matching, JobAd Enrichment can be used to gain in-depth insights into the labor market and to guide jobseekers. By compiling and analyzing structured ad data, it becomes possible to identify:\n* Skills and abilities that are in demand for different professional roles.\n* Occupations that are suitable for those who have certain skills / abilities, or which skills should be invested in to become more in demand in a particular profession.\n* If certain skills / abilities are in demand at all, and whether the interest in them increases or decreases.\n\nThe API can also be used in the development of digital services aimed at employers. For example, there may be services that analyze and improve content in job ads:\n* How much of the ad actually consists of relevant and in-demand words?\n* What concepts are usually requested for a profession? Missing important words in our ad?\n* How unique is our ad compared to other ads in the same area? If we use words and expressions that are inflationary and thus not so useful.\n\nAre you a developer and want to use our new AI solution? Here you get started with JobAd Enrichments, which is a complement to our other range of open APIs for language, individual and labor market data.\n\nIn our forum you can ask questions and participate in discussions about JobAd Enrichments or one of our other APIs.\nWelcome to join!\n\nYou can also contact us via Email"
---

