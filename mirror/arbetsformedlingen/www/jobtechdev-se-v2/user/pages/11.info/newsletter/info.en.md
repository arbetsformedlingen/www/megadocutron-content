---
title: Newsletter
custom:
  short: 'Do you want to know when something interesting is happening in our community? Subscribe to our newsletter and stay tuned! '
  content: "A welcome email will be sent to the address you provided below. You will receive invitations, news, inspiration and tips via email. \nYou can unsubscribe at any time."
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/11.info/newsletter/info.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/11.info/newsletter/info.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/11.info/newsletter/info.en.md
tags:
- user::pages::11.info::newsletter::info.en.md
- pages::11.info::newsletter::info.en.md
- 11.info::newsletter::info.en.md
- newsletter::info.en.md
- info.en.md
---
---
title: Newsletter
custom:
    short: 'Do you want to know when something interesting is happening in our community? Subscribe to our newsletter and stay tuned! '
    content: "A welcome email will be sent to the address you provided below. You will receive invitations, news, inspiration and tips via email. \nYou can unsubscribe at any time."
---

