---
title: JobEd Connect
custom:
  title: JobEd Connect
  description: A matching solution that automatically links education programs to job occupations, based on the labour market’s demands
  block_1: "The API JobEd Connect is a solution that makes it possible to provide digital career guidance by linking education programs to job occupations. The link between education programs and job occupations is generated automatically with the help of the informative text in the knowledge goal objectives, appearing in the description of education programs and competencies in job ads. It is also possible to find out which education program is related to a greater extent to a specific job occupation and its most particular in-demand skills. The solution is developed primarily on data from enriched historical job ads and the National Education Database SUSA Hub (in Swedish[ SUSA-navet](https://utbildningsguiden.skolverket.se/om-oss/susa---data-om-utbildningar)). \n\n## What problem does the product solve?\nThe product is solving two major problems. \n<br></br> 1. It is time-consuming to manually link education programs to job occupations in a mainly unregulated labour market. The product addresses and solves it by automatically linking education programs to the most relevant occupations. When a relevant occupation has been matched, the occupation contains the so-called occupation code (in relation to the Swedish Standard Classification of Occupations (SSYK)); a code that can be used to fetch additional information from other APIs, for example, occupational forecasts and an average salary for the matched occupation.\n[For more information about SSYK (in Swedish).](https://www.scb.se/dokumentation/klassifikationer-och-standarder/standard-for-svensk-yrkesklassificering-ssyk/)\n<br></br>2. It can be challenging to find out which education is related to a greater extent to a specific occupation and job. JobEd Connect provides a solution to search for education programs based on in-demand competencies, which most often appear in job postings. \n\n![](./ill_JobEd_Connect_eng.png)\n\n## For whom is the product created?\nJobEd Connect can be helpful for companies and organisations that provide education and skills development on digital platforms and who want to avoid manually linking education programs to job occupations in the labour market. As a result, it can be useful to facilitate digital career guidance, helping the individual to find a relevant education and to secure future employment while advancing in his career."
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
  menu:
  - title: API
    url: https://jobed-connect-api.jobtechdev.se
    showInShort: '1'
  - title: Demo (sve)
    url: https://demo-jobed-connect.jobtechdev.se
    showInShort: '1'
  - title: Source code
    url: https://gitlab.com/arbetsformedlingen/education
    showInShort: '1'
  - title: Getting Started
    url: https://gitlab.com/arbetsformedlingen/education/education-api/-/blob/main/GETTING_STARTED.md
    showInShort: null
  - title: Read me
    url: https://gitlab.com/arbetsformedlingen/education/education-api/-/blob/main/README.md
    showInShort: null
  - title: Discuss
    url: https://forum.jobtechdev.se/c/vara-api-er-dataset/jobed-connect/45
    showInShort: null
  product_info:
  - title: Version
    value: 1.1.0
  block_2: "### Are you interested in trying the APIs functions?\nHere, you can find the [test environment ](https://demo-jobed-connect.jobtechdev.se) for the APIs functions. \nThe API JobEd Connect is free of charge and available to anyone to use."
taxonomy:
  category:
  - API
  type:
  - Open data
  - Open source
media_order: ill_JobEd_Connect3.png,ill_JobEd_Connect_eng.png
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/02.components/22.jobed-connect/component_page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/02.components/22.jobed-connect/component_page.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/22.jobed-connect/component_page.en.md
tags:
- user::pages::02.components::22.jobed-connect::component_page.en.md
- pages::02.components::22.jobed-connect::component_page.en.md
- 02.components::22.jobed-connect::component_page.en.md
- 22.jobed-connect::component_page.en.md
- component_page.en.md
---
---
title: 'JobEd Connect'
custom:
    title: 'JobEd Connect'
    description: 'A matching solution that automatically links education programs to job occupations, based on the labour market’s demands'
    block_1: "The API JobEd Connect is a solution that makes it possible to provide digital career guidance by linking education programs to job occupations. The link between education programs and job occupations is generated automatically with the help of the informative text in the knowledge goal objectives, appearing in the description of education programs and competencies in job ads. It is also possible to find out which education program is related to a greater extent to a specific job occupation and its most particular in-demand skills. The solution is developed primarily on data from enriched historical job ads and the National Education Database SUSA Hub (in Swedish[ SUSA-navet](https://utbildningsguiden.skolverket.se/om-oss/susa---data-om-utbildningar)). \n\n## What problem does the product solve?\nThe product is solving two major problems. \n<br></br> 1. It is time-consuming to manually link education programs to job occupations in a mainly unregulated labour market. The product addresses and solves it by automatically linking education programs to the most relevant occupations. When a relevant occupation has been matched, the occupation contains the so-called occupation code (in relation to the Swedish Standard Classification of Occupations (SSYK)); a code that can be used to fetch additional information from other APIs, for example, occupational forecasts and an average salary for the matched occupation.\n[For more information about SSYK (in Swedish).](https://www.scb.se/dokumentation/klassifikationer-och-standarder/standard-for-svensk-yrkesklassificering-ssyk/)\n<br></br>2. It can be challenging to find out which education is related to a greater extent to a specific occupation and job. JobEd Connect provides a solution to search for education programs based on in-demand competencies, which most often appear in job postings. \n\n![](./ill_JobEd_Connect_eng.png)\n\n## For whom is the product created?\nJobEd Connect can be helpful for companies and organisations that provide education and skills development on digital platforms and who want to avoid manually linking education programs to job occupations in the labour market. As a result, it can be useful to facilitate digital career guidance, helping the individual to find a relevant education and to secure future employment while advancing in his career."
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    menu:
        -
            title: API
            url: 'https://jobed-connect-api.jobtechdev.se'
            showInShort: '1'
        -
            title: 'Demo (sve)'
            url: 'https://demo-jobed-connect.jobtechdev.se'
            showInShort: '1'
        -
            title: 'Source code'
            url: 'https://gitlab.com/arbetsformedlingen/education'
            showInShort: '1'
        -
            title: 'Getting Started'
            url: 'https://gitlab.com/arbetsformedlingen/education/education-api/-/blob/main/GETTING_STARTED.md'
            showInShort: null
        -
            title: 'Read me'
            url: 'https://gitlab.com/arbetsformedlingen/education/education-api/-/blob/main/README.md'
            showInShort: null
        -
            title: Discuss
            url: 'https://forum.jobtechdev.se/c/vara-api-er-dataset/jobed-connect/45'
            showInShort: null
    product_info:
        -
            title: Version
            value: 1.1.0
    block_2: "### Are you interested in trying the APIs functions?\nHere, you can find the [test environment ](https://demo-jobed-connect.jobtechdev.se) for the APIs functions. \nThe API JobEd Connect is free of charge and available to anyone to use."
taxonomy:
    category:
        - API
    type:
        - 'Open data'
        - 'Open source'
media_order: 'ill_JobEd_Connect3.png,ill_JobEd_Connect_eng.png'
---

