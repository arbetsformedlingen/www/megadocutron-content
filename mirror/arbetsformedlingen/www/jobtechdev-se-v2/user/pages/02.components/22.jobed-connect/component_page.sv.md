---
title: JobEd Connect
custom:
  title: JobEd Connect
  description: Matchningslösning som automatiskt kopplar ihop utbildningar med relaterade yrken, utifrån vad som efterfrågas på arbetsmarknaden
  block_1: "API:et JobEd Connect är en lösning som gör det möjligt att se vilka yrken som är mest relaterade till en viss utbildning. Kopplingen mellan utbildning och yrke görs automatiskt med hjälp av de kunskapsmål som nämns i utbildningsbeskrivningen och de kompetenser som arbetsgivare oftast frågar efter för yrket. Det går också att få fram vilka utbildningar som bäst matchar ett visst yrke och dess mest efterfrågade kompetenser. Lösningen bygger primärt på data från berikade historiska platsannonser, samt data från utbildningssystemet [SUSA-navet](https://utbildningsguiden.skolverket.se/om-oss/susa---data-om-utbildningar).\n\n## Vilket problem löser produkten?\nProdukten löser två problem. \n<br></br>1. Det är tidskrävande att manuellt koppla ihop utbildningar och yrken på en mestadels oreglerad arbetsmarknad. Produkten löser detta genom att automatiskt koppla utbildningar till de mest relevanta yrkena. När man fått en matchning med ett relevant yrke så får man också yrkeskoden (så kallat SSYK) och kan använda detta för att presentera ytterligare information, till exempel yrkesprognos och genomsnittslön för yrket, se [Standard för svensk yrkesklassificering (SSYK)](https://www.scb.se/dokumentation/klassifikationer-och-standarder/standard-for-svensk-yrkesklassificering-ssyk/). \n<br></br>2. Det kan vara svårt att hitta vilka utbildningar som är mest relevanta för ett visst yrke. JobEd Connect gör det möjligt att söka efter utbildningar utifrån de kompetenser som oftast efterfrågas för yrket. \n\n![](./ill_JobEd_Connect3.png)\n\n## Vem kan ha nytta av produkten?\nJobEd Connect är användbart för alla företag och organisationer som arbetar med digitala plattformar för utbildningar och kompetensutveckling, där man vill slippa att manuellt koppla utbildningarna till arbetsmarknaden. Som ett resultat, kan det vara användbart för att underlätta den digitala karriärvägledningen genom att hjälpa individen att hitta relevant utbildning för önskat yrke, samt säkra sin framtida karriär genom kompetensutveckling.\n"
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
  menu:
  - title: API
    url: https://jobed-connect-api.jobtechdev.se
    showInShort: '1'
  - title: Demo
    url: https://demo-jobed-connect.jobtechdev.se
    showInShort: '1'
  - title: Källkod (eng)
    url: https://gitlab.com/arbetsformedlingen/education
    showInShort: '1'
  - title: Getting Started (eng)
    url: https://gitlab.com/arbetsformedlingen/education/education-api/-/blob/main/GETTING_STARTED.md
    showInShort: null
  - title: Read me (eng)
    url: https://gitlab.com/arbetsformedlingen/education/education-api/-/blob/main/README.md
    showInShort: null
  - title: Diskutera
    url: https://forum.jobtechdev.se/c/vara-api-er-dataset/jobed-connect/45
    showInShort: null
  product_info:
  - title: Version
    value: 1.1.0
  block_2: |-
    ### Vill du prova funktionerna i API:et?
    Här finns en [testmiljö](https://demo-jobed-connect.jobtechdev.se) där du kan testa funktionerna i JobEd Connect. API:et är kostnadsfritt och tillgängligt för alla.
taxonomy:
  category:
  - API
  type:
  - Öppna data
  - Öppen källkod
media_order: ill_JobEd_Connect3.png
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/02.components/22.jobed-connect/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/02.components/22.jobed-connect/component_page.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/22.jobed-connect/component_page.sv.md
tags:
- user::pages::02.components::22.jobed-connect::component_page.sv.md
- pages::02.components::22.jobed-connect::component_page.sv.md
- 02.components::22.jobed-connect::component_page.sv.md
- 22.jobed-connect::component_page.sv.md
- component_page.sv.md
---
---
title: 'JobEd Connect'
custom:
    title: 'JobEd Connect'
    description: 'Matchningslösning som automatiskt kopplar ihop utbildningar med relaterade yrken, utifrån vad som efterfrågas på arbetsmarknaden'
    block_1: "API:et JobEd Connect är en lösning som gör det möjligt att se vilka yrken som är mest relaterade till en viss utbildning. Kopplingen mellan utbildning och yrke görs automatiskt med hjälp av de kunskapsmål som nämns i utbildningsbeskrivningen och de kompetenser som arbetsgivare oftast frågar efter för yrket. Det går också att få fram vilka utbildningar som bäst matchar ett visst yrke och dess mest efterfrågade kompetenser. Lösningen bygger primärt på data från berikade historiska platsannonser, samt data från utbildningssystemet [SUSA-navet](https://utbildningsguiden.skolverket.se/om-oss/susa---data-om-utbildningar).\n\n## Vilket problem löser produkten?\nProdukten löser två problem. \n<br></br>1. Det är tidskrävande att manuellt koppla ihop utbildningar och yrken på en mestadels oreglerad arbetsmarknad. Produkten löser detta genom att automatiskt koppla utbildningar till de mest relevanta yrkena. När man fått en matchning med ett relevant yrke så får man också yrkeskoden (så kallat SSYK) och kan använda detta för att presentera ytterligare information, till exempel yrkesprognos och genomsnittslön för yrket, se [Standard för svensk yrkesklassificering (SSYK)](https://www.scb.se/dokumentation/klassifikationer-och-standarder/standard-for-svensk-yrkesklassificering-ssyk/). \n<br></br>2. Det kan vara svårt att hitta vilka utbildningar som är mest relevanta för ett visst yrke. JobEd Connect gör det möjligt att söka efter utbildningar utifrån de kompetenser som oftast efterfrågas för yrket. \n\n![](./ill_JobEd_Connect3.png)\n\n## Vem kan ha nytta av produkten?\nJobEd Connect är användbart för alla företag och organisationer som arbetar med digitala plattformar för utbildningar och kompetensutveckling, där man vill slippa att manuellt koppla utbildningarna till arbetsmarknaden. Som ett resultat, kan det vara användbart för att underlätta den digitala karriärvägledningen genom att hjälpa individen att hitta relevant utbildning för önskat yrke, samt säkra sin framtida karriär genom kompetensutveckling.\n"
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    menu:
        -
            title: API
            url: 'https://jobed-connect-api.jobtechdev.se'
            showInShort: '1'
        -
            title: Demo
            url: 'https://demo-jobed-connect.jobtechdev.se'
            showInShort: '1'
        -
            title: 'Källkod (eng)'
            url: 'https://gitlab.com/arbetsformedlingen/education'
            showInShort: '1'
        -
            title: 'Getting Started (eng)'
            url: 'https://gitlab.com/arbetsformedlingen/education/education-api/-/blob/main/GETTING_STARTED.md'
            showInShort: null
        -
            title: 'Read me (eng)'
            url: 'https://gitlab.com/arbetsformedlingen/education/education-api/-/blob/main/README.md'
            showInShort: null
        -
            title: Diskutera
            url: 'https://forum.jobtechdev.se/c/vara-api-er-dataset/jobed-connect/45'
            showInShort: null
    product_info:
        -
            title: Version
            value: 1.1.0
    block_2: "### Vill du prova funktionerna i API:et?\nHär finns en [testmiljö](https://demo-jobed-connect.jobtechdev.se) där du kan testa funktionerna i JobEd Connect. API:et är kostnadsfritt och tillgängligt för alla."
taxonomy:
    category:
        - API
    type:
        - 'Öppna data'
        - 'Öppen källkod'
media_order: ill_JobEd_Connect3.png
---

