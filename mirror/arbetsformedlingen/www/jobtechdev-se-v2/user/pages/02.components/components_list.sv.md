---
title: Komponenter
custom:
  showLeftMenu: false
  components:
  - page: /components/alljobads
  - page: /components/connect-once
  - page: /components/digital-vaegledning-och-laenkad-data
  - page: /components/digital-skills
  - page: /components/ekosystem_foer_annonser
  - page: /components/etik-och-digital-matchning
  - page: /components/giglab-sverige
  - page: /components/historical-ads
  - page: /components/jobad-enrichments
  - page: /components/jobad-links
  - page: /components/jobed-connect
  - page: /components/jobsearch
  - page: /components/jobsearch-trends
  - page: /components/jobstream
  - page: /components/jobtech-atlas
  - page: /components/jobtech-taxonomy
  - page: /components/kompetensmatchning
  - page: /components/open-plattforms
  - page: /components/yrkesprognoser
  text: "Öppna data och tekniska lösningar som är fritt tillgängliga för vem som helst att använda och skapa nytta för fler. \nKontakta oss om du vill hjälpa till och bidra tillbaka."
  title: Öppna data och tekniska lösningar
routes:
  default: /komponenter
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/02.components/components_list.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/02.components/components_list.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/components_list.sv.md
tags:
- user::pages::02.components::components_list.sv.md
- pages::02.components::components_list.sv.md
- 02.components::components_list.sv.md
- components_list.sv.md
---
---
title: Komponenter
custom:
    showLeftMenu: false
    components:
        -
            page: /components/alljobads
        -
            page: /components/connect-once
        -
            page: /components/digital-vaegledning-och-laenkad-data
        -
            page: /components/digital-skills
        -
            page: /components/ekosystem_foer_annonser
        -
            page: /components/etik-och-digital-matchning
        -
            page: /components/giglab-sverige
        -
            page: /components/historical-ads
        -
            page: /components/jobad-enrichments
        -
            page: /components/jobad-links
        -
            page: /components/jobed-connect
        -
            page: /components/jobsearch
        -
            page: /components/jobsearch-trends
        -
            page: /components/jobstream
        -
            page: /components/jobtech-atlas
        -
            page: /components/jobtech-taxonomy
        -
            page: /components/kompetensmatchning
        -
            page: /components/open-plattforms
        -
            page: /components/yrkesprognoser
    text: "Öppna data och tekniska lösningar som är fritt tillgängliga för vem som helst att använda och skapa nytta för fler. \nKontakta oss om du vill hjälpa till och bidra tillbaka."
    title: 'Öppna data och tekniska lösningar'
routes:
    default: /komponenter
---

