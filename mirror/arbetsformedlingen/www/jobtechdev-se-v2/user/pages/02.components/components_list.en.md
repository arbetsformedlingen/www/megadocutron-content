---
title: Components
custom:
  showLeftMenu: false
  components:
  - page: /components/alljobads
  - page: /components/connect-once
  - page: /components/digital-vaegledning-och-laenkad-data
  - page: /components/digital-skills
  - page: /components/digital-yrkesvaegledning
  - page: /components/ekosystem_foer_annonser
  - page: /components/etik-och-digital-matchning
  - page: /components/giglab-sverige
  - page: /components/historical-ads
  - page: /components/jobad-enrichments
  - page: /components/jobad-links
  - page: /components/jobed-connect
  - page: /components/jobsearch
  - page: /components/jobsearch-trends
  - page: /components/jobstream
  - page: /components/jobtech-atlas
  - page: /components/jobtech-taxonomy
  - page: /components/kompetensmatchning
  - page: /components/yrkesprognoser
  - page: /components/open-plattforms
  - page: /components/individdata-och-dataportabilitet
  - page: /components/taxonomi-och-begreppsstruktur
  text: "Open data and technical solutions that are free of charge and available for anyone to use. Our components create benefits for many. \n\nContact us if you would like to help us and contribute with a technical solution/component on the platform. "
  title: Open Data and Technical Solutions
routes: {}
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/02.components/components_list.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/02.components/components_list.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/components_list.en.md
tags:
- user::pages::02.components::components_list.en.md
- pages::02.components::components_list.en.md
- 02.components::components_list.en.md
- components_list.en.md
---
---
title: Components
custom:
    showLeftMenu: false
    components:
        -
            page: /components/alljobads
        -
            page: /components/connect-once
        -
            page: /components/digital-vaegledning-och-laenkad-data
        -
            page: /components/digital-skills
        -
            page: /components/digital-yrkesvaegledning
        -
            page: /components/ekosystem_foer_annonser
        -
            page: /components/etik-och-digital-matchning
        -
            page: /components/giglab-sverige
        -
            page: /components/historical-ads
        -
            page: /components/jobad-enrichments
        -
            page: /components/jobad-links
        -
            page: /components/jobed-connect
        -
            page: /components/jobsearch
        -
            page: /components/jobsearch-trends
        -
            page: /components/jobstream
        -
            page: /components/jobtech-atlas
        -
            page: /components/jobtech-taxonomy
        -
            page: /components/kompetensmatchning
        -
            page: /components/yrkesprognoser
        -
            page: /components/open-plattforms
        -
            page: /components/individdata-och-dataportabilitet
        -
            page: /components/taxonomi-och-begreppsstruktur
    text: "Open data and technical solutions that are free of charge and available for anyone to use. Our components create benefits for many. \n\nContact us if you would like to help us and contribute with a technical solution/component on the platform. "
    title: 'Open Data and Technical Solutions'
routes: {  }
---

