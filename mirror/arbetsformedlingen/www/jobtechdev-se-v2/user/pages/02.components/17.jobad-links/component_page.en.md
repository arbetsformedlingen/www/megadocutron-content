---
title: JobAd Links
custom:
  title: JobAd Links
  description: It collects, organizes, navigates, and links to job advertisements from labour market actors
  contact_name: JobTech Development
  contact_email: jobtechdev@arbetsformedlingen.se
  block_1: "The API JobAd Links has been developed in a collaboration with Sweden's largest advertising and matching actors, which share job advertisements data free of charge in a [common digital ecosystem](https://jobtechdev.se/en/components/ekosystem_foer_annonser). JobAd Links is a search API, supporting free text search and filters. Generally, it provides access to all job advertisements on the market and enables it for users to search among the entire labour market's range of advertisements.\n\n## How does it work?\nThe ads are collected and converted into a common standard in order to be adapted to several services. Then, they are enriched with professional and industry codes, as well as with other metadata if needed to make the ads searchable and matchable in a machine method way. The ad text is edited and shortened to a few sentences, which are made accessible and available, in accordance with the principle back-to-source. Thus, the traffic can be steered back to the source of origin.\n\n## What problem does the product solve?\nThe API JobAd Links generates an additional 30% of unique ads, in addition to those, already published on [the Swedish Public Employment Service Job Board -Platsbanken](https://arbetsformedlingen.se/platsbanken/). This makes it easier for users to search among the entire labour market's range of ads, no matter which platform they are published on. Moreover, the API handles and selects duplicate ads, which have been a problem for many users.\n\n## For whom is the product created? \nThe API can be useful for anyone, who would like to have access to job advertisements from the entire market's range, for matching services or for research and statistics, among others."
  block_2: |-
    ## Advertising and matching actors, which participated and collaborated
    * [arbetsformedlingen.se](https://arbetsformedlingen.se)
    * [jobb.blocket.se](https://jobb.blocket.se)
    * [careerbuilder.se](https://www.careerbuilder.se/?cbRecursionCnt=1)
    * [offentligajobb.se](https://www.offentligajobb.se)
    * [monster.se](https://www.monster.se)
    * [netjobs.com](https://www.netjobs.com)
    * [ingenjorsjobb.se](https://ingenjorsjobb.se)
    * [studentjob.se](https://www.studentjob.se)
    * [lararguiden.se](https://lararguiden.se)
    * [gronajobb.se](http://gronajobb.se/jobb/)
    * [thehub.io](https://thehub.io)
    * [jobbdirekt.se](https://jobbdirekt.se)
    * [onepartnergroup.se](https://www.onepartnergroup.se)
    * [saljpoolen.se](https://www.saljpoolen.se/sv)
    * [traineeguiden.se](https://www.traineeguiden.se)
    * [ingenjorsguiden.se](https://ingenjorsguiden.se)
    * [intenso.se](https://www.intenso.se/sv)
  menu:
  - title: API
    url: https://links.api.jobtechdev.se
    showInShort: '1'
  - title: Getting Started
    url: https://gitlab.com/arbetsformedlingen/joblinks
    showInShort: null
taxonomy:
  category:
  - API
  type:
  - Open data
media_order: image.png
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/02.components/17.jobad-links/component_page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/02.components/17.jobad-links/component_page.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/17.jobad-links/component_page.en.md
tags:
- user::pages::02.components::17.jobad-links::component_page.en.md
- pages::02.components::17.jobad-links::component_page.en.md
- 02.components::17.jobad-links::component_page.en.md
- 17.jobad-links::component_page.en.md
- component_page.en.md
---
---
title: 'JobAd Links'
custom:
    title: 'JobAd Links'
    description: 'It collects, organizes, navigates, and links to job advertisements from labour market actors'
    contact_name: 'JobTech Development'
    contact_email: jobtechdev@arbetsformedlingen.se
    block_1: "The API JobAd Links has been developed in a collaboration with Sweden's largest advertising and matching actors, which share job advertisements data free of charge in a [common digital ecosystem](https://jobtechdev.se/en/components/ekosystem_foer_annonser). JobAd Links is a search API, supporting free text search and filters. Generally, it provides access to all job advertisements on the market and enables it for users to search among the entire labour market's range of advertisements.\n\n## How does it work?\nThe ads are collected and converted into a common standard in order to be adapted to several services. Then, they are enriched with professional and industry codes, as well as with other metadata if needed to make the ads searchable and matchable in a machine method way. The ad text is edited and shortened to a few sentences, which are made accessible and available, in accordance with the principle back-to-source. Thus, the traffic can be steered back to the source of origin.\n\n## What problem does the product solve?\nThe API JobAd Links generates an additional 30% of unique ads, in addition to those, already published on [the Swedish Public Employment Service Job Board -Platsbanken](https://arbetsformedlingen.se/platsbanken/). This makes it easier for users to search among the entire labour market's range of ads, no matter which platform they are published on. Moreover, the API handles and selects duplicate ads, which have been a problem for many users.\n\n## For whom is the product created? \nThe API can be useful for anyone, who would like to have access to job advertisements from the entire market's range, for matching services or for research and statistics, among others."
    block_2: "## Advertising and matching actors, which participated and collaborated\n* [arbetsformedlingen.se](https://arbetsformedlingen.se)\n* [jobb.blocket.se](https://jobb.blocket.se)\n* [careerbuilder.se](https://www.careerbuilder.se/?cbRecursionCnt=1)\n* [offentligajobb.se](https://www.offentligajobb.se)\n* [monster.se](https://www.monster.se)\n* [netjobs.com](https://www.netjobs.com)\n* [ingenjorsjobb.se](https://ingenjorsjobb.se)\n* [studentjob.se](https://www.studentjob.se)\n* [lararguiden.se](https://lararguiden.se)\n* [gronajobb.se](http://gronajobb.se/jobb/)\n* [thehub.io](https://thehub.io)\n* [jobbdirekt.se](https://jobbdirekt.se)\n* [onepartnergroup.se](https://www.onepartnergroup.se)\n* [saljpoolen.se](https://www.saljpoolen.se/sv)\n* [traineeguiden.se](https://www.traineeguiden.se)\n* [ingenjorsguiden.se](https://ingenjorsguiden.se)\n* [intenso.se](https://www.intenso.se/sv)"
    menu:
        -
            title: API
            url: 'https://links.api.jobtechdev.se'
            showInShort: '1'
        -
            title: 'Getting Started'
            url: 'https://gitlab.com/arbetsformedlingen/joblinks'
            showInShort: null
taxonomy:
    category:
        - API
    type:
        - 'Open data'
media_order: image.png
---

