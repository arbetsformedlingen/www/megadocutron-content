---
title: JobSearch Trends
custom:
  title: JobSearch Trends
  description: Visar sökbeteenden på Arbetsförmedlingens annonsplattform Platsbanken över tid.
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
  product_info:
  - title: Dataformat
    value: JSON
  - title: Version
    value: 0.1.0 Beta
  menu:
  - title: Dataset
    url: https://data.jobtechdev.se/annonser/search-trends/index.html
    showInShort: '1'
  - title: Källkod
    url: https://gitlab.com/arbetsformedlingen/job-ads/search-trends
    showInShort: '1'
  - title: Diskutera
    url: https://forum.jobtechdev.se/t/jobsearch-trends/629
    showInShort: null
  block_1: "JobSearch Trends är ett dataset med filer som innehåller de mest använda sökorden på Arbetsförmedlingens annonsplattform [Platsbanken](https://arbetsformedlingen.se/platsbanken/). Nya filer genereras varje dag med de vanligaste sökorden inom till exempel yrke, språk, kompetens, ort och arbetsgivare. Datasetet består i dag av JSON-filer som tillgängliggör rådata på ett strukturerat sätt. Den rådata som tillgängliggörs kvalitetssäkras och rensas från personuppgifter. Datasetet kan användas till att förenkla processer för att bygga och träna algoritmer i samband med maskininlärning.\n\n## Vilket problem löser produkten? \nMed hjälp av datasetet är det möjligt att följa upp trender och analysera användarbeteenden inom arbetsmarknadsområdet.\n\n## Vem kan ha nytta av produkten?\nDatasetet är användbart för alla företag, organisationer och privatpersoner som vill förstå användarbeteenden inom arbetsmarknadsområdet bättre, genom strukturerad och aggregerad rådata."
  block_2: "### Kostnadsfritt och tillgängligt för alla. \n\nDatasetet är i en utvecklingsfas och vi vill gärna ha feedback och synpunkter från våra användare.\nVill du veta mer om JobSearch Trends, kontakta oss gärna eller diskutera på vårt forum!"
taxonomy:
  category:
  - Dataset
  type:
  - Öppna data
  status:
  - Beta
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/02.components/19.jobsearch-trends/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/02.components/19.jobsearch-trends/component_page.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/19.jobsearch-trends/component_page.sv.md
tags:
- user::pages::02.components::19.jobsearch-trends::component_page.sv.md
- pages::02.components::19.jobsearch-trends::component_page.sv.md
- 02.components::19.jobsearch-trends::component_page.sv.md
- 19.jobsearch-trends::component_page.sv.md
- component_page.sv.md
---
---
title: 'JobSearch Trends'
custom:
    title: 'JobSearch Trends'
    description: 'Visar sökbeteenden på Arbetsförmedlingens annonsplattform Platsbanken över tid.'
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    product_info:
        -
            title: Dataformat
            value: JSON
        -
            title: Version
            value: '0.1.0 Beta'
    menu:
        -
            title: Dataset
            url: 'https://data.jobtechdev.se/annonser/search-trends/index.html'
            showInShort: '1'
        -
            title: Källkod
            url: 'https://gitlab.com/arbetsformedlingen/job-ads/search-trends'
            showInShort: '1'
        -
            title: Diskutera
            url: 'https://forum.jobtechdev.se/t/jobsearch-trends/629'
            showInShort: null
    block_1: "JobSearch Trends är ett dataset med filer som innehåller de mest använda sökorden på Arbetsförmedlingens annonsplattform [Platsbanken](https://arbetsformedlingen.se/platsbanken/). Nya filer genereras varje dag med de vanligaste sökorden inom till exempel yrke, språk, kompetens, ort och arbetsgivare. Datasetet består i dag av JSON-filer som tillgängliggör rådata på ett strukturerat sätt. Den rådata som tillgängliggörs kvalitetssäkras och rensas från personuppgifter. Datasetet kan användas till att förenkla processer för att bygga och träna algoritmer i samband med maskininlärning.\n\n## Vilket problem löser produkten? \nMed hjälp av datasetet är det möjligt att följa upp trender och analysera användarbeteenden inom arbetsmarknadsområdet.\n\n## Vem kan ha nytta av produkten?\nDatasetet är användbart för alla företag, organisationer och privatpersoner som vill förstå användarbeteenden inom arbetsmarknadsområdet bättre, genom strukturerad och aggregerad rådata."
    block_2: "### Kostnadsfritt och tillgängligt för alla. \n\nDatasetet är i en utvecklingsfas och vi vill gärna ha feedback och synpunkter från våra användare.\nVill du veta mer om JobSearch Trends, kontakta oss gärna eller diskutera på vårt forum!"
taxonomy:
    category:
        - Dataset
    type:
        - 'Öppna data'
    status:
        - Beta
---

