---
title: Ecosystem for JobAds
custom:
  title: Ecosystem for JobAds
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
  block_1: "The collaborative initiative \"Ecosystem for job ads\" refers to Sweden's largest advertising and matching actors, which decide to open up and share job advertisement data in a common digital ecosystem. The aim is to make it easier for jobseekers to navigate among job advertisements, regardless of the job board/platform, in order to find jobs. Moreover, it aims at providing Statistics Sweden's labour market statistics with a broader material. Since February 2021, the technical solution has been tested and run in a pilot project at [Platsbanken](https://arbetsformedlingen.se/platsbanken).\n\n\nImportant collaboration benefits: \n\n* We provide and contribute to better statistics on the Swedish labour market\n* We make it easier for our common users to search among the entire labour market's range of advertisements\n* We guide users through/to external websites, which are part of this collaborative initiative"
  block_2: |
    ## Would you like to join us?
    Are you and your organization interested in joining hands in this collaborative initiative? Read more on the [Wiki page](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home) or [contact us for more information](mailto:jobtechdev@arbetsformedlingen.se).
  description: It collects job advertisement data from private and public actors in a common digital ecosystem for better labour market statistics.
  menu:
  - title: Technical Information
    url: https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home
    showInShort: '1'
taxonomy:
  category:
  - Project
  type:
  - Open data
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/02.components/02.ekosystem_foer_annonser/component_page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/02.components/02.ekosystem_foer_annonser/component_page.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/02.ekosystem_foer_annonser/component_page.en.md
tags:
- user::pages::02.components::02.ekosystem_foer_annonser::component_page.en.md
- pages::02.components::02.ekosystem_foer_annonser::component_page.en.md
- 02.components::02.ekosystem_foer_annonser::component_page.en.md
- 02.ekosystem_foer_annonser::component_page.en.md
- component_page.en.md
---
---
title: 'Ecosystem for JobAds'
custom:
    title: 'Ecosystem for JobAds'
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    block_1: "The collaborative initiative \"Ecosystem for job ads\" refers to Sweden's largest advertising and matching actors, which decide to open up and share job advertisement data in a common digital ecosystem. The aim is to make it easier for jobseekers to navigate among job advertisements, regardless of the job board/platform, in order to find jobs. Moreover, it aims at providing Statistics Sweden's labour market statistics with a broader material. Since February 2021, the technical solution has been tested and run in a pilot project at [Platsbanken](https://arbetsformedlingen.se/platsbanken).\n\n\nImportant collaboration benefits: \n\n* We provide and contribute to better statistics on the Swedish labour market\n* We make it easier for our common users to search among the entire labour market's range of advertisements\n* We guide users through/to external websites, which are part of this collaborative initiative"
    block_2: "## Would you like to join us?\nAre you and your organization interested in joining hands in this collaborative initiative? Read more on the [Wiki page](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home) or [contact us for more information](mailto:jobtechdev@arbetsformedlingen.se).\n"
    description: 'It collects job advertisement data from private and public actors in a common digital ecosystem for better labour market statistics.'
    menu:
        -
            title: 'Technical Information'
            url: 'https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home'
            showInShort: '1'
taxonomy:
    category:
        - Project
    type:
        - 'Open data'
---

