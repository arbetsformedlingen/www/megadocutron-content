---
title: digitalskills-se
custom:
  title: Digitalskills.se
  description: A forecasting tool for occupational profiling by data-driven analysis of job advertisements. It provides both historical trends and projections of digital skills and professional titles.
  contact_email: kontakt@assedon.se
  contact_name: Assedon
  block_1: "Which tech skills are trending right now? Which professional titles in Data Science / IT have grown the most in recent times and which are estimated to be the most in-demand ones in 18 months? \n\nDigitalskills.se is a forecasting tool for occupational profiling by data-driven analysis of job advertisements. It provides both historical trends and projections of digital skills and professional titles. The job ads analysis is designed and presented in a user-friendly interface with top lists of professional titles-in demand, information about the employer demand as well as how this demand looks like in different parts of Sweden. Digitalskills.se is developed by the company Assedon and produced within a [government assignment to collaborate on ensuring access on cutting-edge digital expertise](https://digitalspetskompetens.se), carried out by the Swedish Agency for Economic and Regional Growth (Tillväxtverket) and the Swedish Higher Education Authority (UKÄ).\n\n## For whom is the product created?\nThere are various target groups for the tool. Primarily, the tool addresses vocational training providers, sector skills councils, study and career counsellors, which in various ways need to embed and respond to the labour market's demand in its organisation. \n\n## How does the product work? \n**1. Extracting ads from raw data**<BR>\n[Historical ads](https://jobtechdev.se/en/components/historical-ads) are picked up every quater. Ads that are categorized as Data / IT according to [Jobtech taxonomy](https://jobtechdev.se/en/components/jobtech-taxonomy) are extracted from the dataset. \n\n**2. Enrichment of ad data**<BR>\n[JobAd Enrichments](https://jobtechdev.se/en/components/jobad-enrichments) is making \"a call\" to the API, which further provides an accessible way to request dataset on current job ads in order to extract relevant competences, occupations, information about the geographical location of the job, as well and soft skills. The extraction of competencies does not consider that the request is related only to Data Science / IT. Therefore, the result should be filtered through the so called \"black list\", which provides a short list of words of relevant competences. The process has a threshold value, which provides a statistical validation through [JobAd Enrichments](https://jobtechdev.se/en/components/jobad-enrichments), which furthermore considers to provide accuracy of the extraction of the various data points.\n\n**3. Projection of data**<BR>\nBy using open libraries in Python, the advertisements go through a function of applying algorithms for forecasting time series. The applied function is exponential, taking into consideration the equalization and the seasonal variation of the time series. By sorting out the data first, a bulletproof data is taken into consideration in the future projection. Projections for 6, 12 and 18 months are produced and saved as time series in the result examples. Parallelly, values for historical trends are also selected and saved at the same interval.\n    \n\nIf you want to read more about how digitalskills.se is developed, download [the Development Report (in Swedish)](https://jobtechdev.se/user/pages/02.components/18.digital-skills/Utvecklingsrapport.pdf)."
  menu:
  - title: Website
    url: https://www.digitalskills.se
    showInShort: '1'
  - title: API
    url: https://dig-api-kbrvfttzua-uc.a.run.app/
    showInShort: '1'
  - title: Getting Started
    url: 'https://github.com/Assedon-AB/digitalskills.se '
    showInShort: null
  block_2: "## Important\nIt is important to remember that forecasts are fundamentally uncertain and should be considered and used with extra caution. There are remaining challenges with the underlying dataset as well as the applied method/function on which the tool is built on. Thus, it can lead to providing misleading information and/or potential inaccuracies. Therefore, we recommend that you use the tool as a compliment to one of the various methods in your competitive intelligence. Feel free to compliment such data-driven methods with other qualitative methods such as industry reports, interviews with employers, etc. "
  product_info:
  - title: Version
    value: 1.0.0 Beta
  - title: Frontend
    value: Next.js
  - title: Backend
    value: Python
media_order: Utvecklingsrapport.pdf
taxonomy:
  category:
  - API
  type:
  - Open data
  - Open source
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/02.components/18.digital-skills/component_page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/02.components/18.digital-skills/component_page.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/18.digital-skills/component_page.en.md
tags:
- user::pages::02.components::18.digital-skills::component_page.en.md
- pages::02.components::18.digital-skills::component_page.en.md
- 02.components::18.digital-skills::component_page.en.md
- 18.digital-skills::component_page.en.md
- component_page.en.md
---
---
title: digitalskills-se
custom:
    title: Digitalskills.se
    description: 'A forecasting tool for occupational profiling by data-driven analysis of job advertisements. It provides both historical trends and projections of digital skills and professional titles.'
    contact_email: kontakt@assedon.se
    contact_name: Assedon
    block_1: "Which tech skills are trending right now? Which professional titles in Data Science / IT have grown the most in recent times and which are estimated to be the most in-demand ones in 18 months? \n\nDigitalskills.se is a forecasting tool for occupational profiling by data-driven analysis of job advertisements. It provides both historical trends and projections of digital skills and professional titles. The job ads analysis is designed and presented in a user-friendly interface with top lists of professional titles-in demand, information about the employer demand as well as how this demand looks like in different parts of Sweden. Digitalskills.se is developed by the company Assedon and produced within a [government assignment to collaborate on ensuring access on cutting-edge digital expertise](https://digitalspetskompetens.se), carried out by the Swedish Agency for Economic and Regional Growth (Tillväxtverket) and the Swedish Higher Education Authority (UKÄ).\n\n## For whom is the product created?\nThere are various target groups for the tool. Primarily, the tool addresses vocational training providers, sector skills councils, study and career counsellors, which in various ways need to embed and respond to the labour market's demand in its organisation. \n\n## How does the product work? \n**1. Extracting ads from raw data**<BR>\n[Historical ads](https://jobtechdev.se/en/components/historical-ads) are picked up every quater. Ads that are categorized as Data / IT according to [Jobtech taxonomy](https://jobtechdev.se/en/components/jobtech-taxonomy) are extracted from the dataset. \n\n**2. Enrichment of ad data**<BR>\n[JobAd Enrichments](https://jobtechdev.se/en/components/jobad-enrichments) is making \"a call\" to the API, which further provides an accessible way to request dataset on current job ads in order to extract relevant competences, occupations, information about the geographical location of the job, as well and soft skills. The extraction of competencies does not consider that the request is related only to Data Science / IT. Therefore, the result should be filtered through the so called \"black list\", which provides a short list of words of relevant competences. The process has a threshold value, which provides a statistical validation through [JobAd Enrichments](https://jobtechdev.se/en/components/jobad-enrichments), which furthermore considers to provide accuracy of the extraction of the various data points.\n\n**3. Projection of data**<BR>\nBy using open libraries in Python, the advertisements go through a function of applying algorithms for forecasting time series. The applied function is exponential, taking into consideration the equalization and the seasonal variation of the time series. By sorting out the data first, a bulletproof data is taken into consideration in the future projection. Projections for 6, 12 and 18 months are produced and saved as time series in the result examples. Parallelly, values for historical trends are also selected and saved at the same interval.\n    \n\nIf you want to read more about how digitalskills.se is developed, download [the Development Report (in Swedish)](https://jobtechdev.se/user/pages/02.components/18.digital-skills/Utvecklingsrapport.pdf)."
    menu:
        -
            title: Website
            url: 'https://www.digitalskills.se'
            showInShort: '1'
        -
            title: API
            url: 'https://dig-api-kbrvfttzua-uc.a.run.app/'
            showInShort: '1'
        -
            title: 'Getting Started'
            url: 'https://github.com/Assedon-AB/digitalskills.se '
            showInShort: null
    block_2: "## Important\nIt is important to remember that forecasts are fundamentally uncertain and should be considered and used with extra caution. There are remaining challenges with the underlying dataset as well as the applied method/function on which the tool is built on. Thus, it can lead to providing misleading information and/or potential inaccuracies. Therefore, we recommend that you use the tool as a compliment to one of the various methods in your competitive intelligence. Feel free to compliment such data-driven methods with other qualitative methods such as industry reports, interviews with employers, etc. "
    product_info:
        -
            title: Version
            value: '1.0.0 Beta'
        -
            title: Frontend
            value: Next.js
        -
            title: Backend
            value: Python
media_order: Utvecklingsrapport.pdf
taxonomy:
    category:
        - API
    type:
        - 'Open data'
        - 'Open source'
---

