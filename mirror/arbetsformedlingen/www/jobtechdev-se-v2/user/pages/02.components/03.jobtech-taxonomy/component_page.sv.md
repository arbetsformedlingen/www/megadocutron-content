---
title: Jobtech Taxonomy
custom:
  title: Jobtech Taxonomy
  description: Yrkesbenämningar, kompetensbegrepp och kopplingen mellan yrken och kompetenser.
  block_1: "<iframe allow=\"camera; microphone; fullscreen; display-capture; autoplay\" src=\"https://jitsi.jobtechdev.se/shark\" style=\"height: 100%; width: 100%; border: 0px;\"></iframe> \ns\nJobTech Taxonomy ger tillgång till de ord och begrepp som används på arbetsmarknaden samt information om hur dessa är kopplade till varandra. Det kan till exempel vara yrkesbenämningar, kompetensbegrepp eller kopplingen mellan yrken och kompetenser. Yrkesbenämningarna är strukturerade enligt Standard för Svensk Yrkesklassificering (SSYK).\n \n## Vilket problem löser produkten?\nOtydligt eller felaktigt formulerade jobbannonser och CV:n är ett problem som kan leda till att arbetsgivare och arbetssökande missar varandra. Med API:et skapas ett gemensamt språk för arbetsmarknaden, som gör det lättare för arbetsgivare och arbetssökande att förstå och hitta varandra i digitala kanaler.\n \n## Vem kan ha nytta av produkten?\nJobTech Taxonomy är användbart för alla företag och organisationer som erbjuder digitala matchningstjänster men som inte har möjlighet att bygga upp en informationsstruktur på egen hand. Aktörer som studerar och analyserar utvecklingen på arbetsmarknaden har också nytta av API:et.\n"
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
  menu:
  - title: API
    url: https://taxonomy.api.jobtechdev.se/v1/taxonomy/swagger-ui/index.html
    showInShort: '1'
  - title: Källkod
    url: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/blob/develop/GETTINGSTARTED.md
    showInShort: '1'
  - title: Diskutera
    url: https://forum.jobtechdev.se/c/vara-api-er-dataset/jobtech-taxonomy/11
    showInShort: null
  block_2: |
    ### Notera
    Innehållet i taxonomierna uppdateras ständigt och ändringar släpps på ett kontrollerat sätt.<BR>
  product_info:
  - title: Version
    value: 1.0 (2.0 beta)
  title_block_2: s
  title_block_3: s
taxonomy:
  category:
  - API
  type:
  - Öppna data
  - Öppen källkod
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/02.components/03.jobtech-taxonomy/component_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/02.components/03.jobtech-taxonomy/component_page.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/03.jobtech-taxonomy/component_page.sv.md
tags:
- user::pages::02.components::03.jobtech-taxonomy::component_page.sv.md
- pages::02.components::03.jobtech-taxonomy::component_page.sv.md
- 02.components::03.jobtech-taxonomy::component_page.sv.md
- 03.jobtech-taxonomy::component_page.sv.md
- component_page.sv.md
---
---
title: 'Jobtech Taxonomy'
custom:
    title: 'Jobtech Taxonomy'
    description: 'Yrkesbenämningar, kompetensbegrepp och kopplingen mellan yrken och kompetenser.'
    block_1: "<iframe allow=\"camera; microphone; fullscreen; display-capture; autoplay\" src=\"https://jitsi.jobtechdev.se/shark\" style=\"height: 100%; width: 100%; border: 0px;\"></iframe> \ns\nJobTech Taxonomy ger tillgång till de ord och begrepp som används på arbetsmarknaden samt information om hur dessa är kopplade till varandra. Det kan till exempel vara yrkesbenämningar, kompetensbegrepp eller kopplingen mellan yrken och kompetenser. Yrkesbenämningarna är strukturerade enligt Standard för Svensk Yrkesklassificering (SSYK).\n \n## Vilket problem löser produkten?\nOtydligt eller felaktigt formulerade jobbannonser och CV:n är ett problem som kan leda till att arbetsgivare och arbetssökande missar varandra. Med API:et skapas ett gemensamt språk för arbetsmarknaden, som gör det lättare för arbetsgivare och arbetssökande att förstå och hitta varandra i digitala kanaler.\n \n## Vem kan ha nytta av produkten?\nJobTech Taxonomy är användbart för alla företag och organisationer som erbjuder digitala matchningstjänster men som inte har möjlighet att bygga upp en informationsstruktur på egen hand. Aktörer som studerar och analyserar utvecklingen på arbetsmarknaden har också nytta av API:et.\n"
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    menu:
        -
            title: API
            url: 'https://taxonomy.api.jobtechdev.se/v1/taxonomy/swagger-ui/index.html'
            showInShort: '1'
        -
            title: Källkod
            url: 'https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/blob/develop/GETTINGSTARTED.md'
            showInShort: '1'
        -
            title: Diskutera
            url: 'https://forum.jobtechdev.se/c/vara-api-er-dataset/jobtech-taxonomy/11'
            showInShort: null
    block_2: "### Notera\nInnehållet i taxonomierna uppdateras ständigt och ändringar släpps på ett kontrollerat sätt.<BR>\n"
    product_info:
        -
            title: Version
            value: '1.0 (2.0 beta)'
    title_block_2: s
    title_block_3: s
taxonomy:
    category:
        - API
    type:
        - 'Öppna data'
        - 'Öppen källkod'
---

