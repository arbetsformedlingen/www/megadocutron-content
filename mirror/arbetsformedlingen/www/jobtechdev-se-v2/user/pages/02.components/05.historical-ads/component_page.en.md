---
title: Historical Ads
custom:
  title: Historical Ads
  description: It gives a helicopter overview on how the need on the labour market has changed over time.
  contact_email: jobtechdev@arbetsformedlingen.se
  contact_name: JobTech Development
  menu:
  - title: API
    url: https://historical.api.jobtechdev.se
    showInShort: '1'
  - title: Dataset
    url: https://data.jobtechdev.se/annonser/historiska/index.html
    showInShort: '1'
  - title: Getting Started
    url: https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples/historical-ads-info
    showInShort: '1'
  - title: Discuss
    url: https://forum.jobtechdev.se/c/vara-api-er-dataset/historiska-annonser/30
    showInShort: null
  block_1: "## Dataset\nHistorical Jobs is a dataset with all the job abs, which have been published on the Swedish Public Employment Service’s Job Board ([Platsbanken](https://arbetsformedlingen.se/platsbanken/)) since 2006. It contains data from job ads, for instance, information about occupation, employer, place, time, etc). The dataset includes approximately 6,9 million job ads nowadays. The dataset is continuously updated. It simplifies processes for building and training algorithms in relation to machine learning.\n\n## API \nThe API uses the dataset for historical ads and re-uses the code for [JobSearch](https://jobtechdev.se/en/components/jobsearch) to build a new search for all job ads, which have been published on the Swedish Public Employment Service’s job board ([Platsbanken](https://arbetsformedlingen.se/platsbanken/)) since 2006. The API’s first released beta version includes data between 2016-2021. All the job ads are enriched and include data about skills and remote work. The API Historical Ads makes it easier for those, who would like to make a practical use of labour market data. It allows it to be used in a more simple, comprehensive, and structured way.  The API is easy to test and allows quickly download of large amounts of data. Nowadays, the API includes approximately 3.3 million job ads. \n\n## Which solutions does the product provide? \nBy using this product, it is possible to compare time periods, analyse, and follow trends within the labour market area.\n\n## For whom is the product created? \nThe API can be useful for companies, organisations and even private persons, who would like to use job ads’ data for analysis, as well to capture and follow trends within the labour market area. "
  block_2: "The API and dataset Historical ads are free of charge and are available for anyone to use.\nDo you want to know more? Feel free to contact us. "
  product_info:
  - title: Data format
    value: JSON
  - title: Version
    value: 1.23.1 Beta
taxonomy:
  category:
  - API
  - Dataset
  type:
  - Open data
  status:
  - Beta
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/02.components/05.historical-ads/component_page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/02.components/05.historical-ads/component_page.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/05.historical-ads/component_page.en.md
tags:
- user::pages::02.components::05.historical-ads::component_page.en.md
- pages::02.components::05.historical-ads::component_page.en.md
- 02.components::05.historical-ads::component_page.en.md
- 05.historical-ads::component_page.en.md
- component_page.en.md
---
---
title: 'Historical Ads'
custom:
    title: 'Historical Ads'
    description: 'It gives a helicopter overview on how the need on the labour market has changed over time.'
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    menu:
        -
            title: API
            url: 'https://historical.api.jobtechdev.se'
            showInShort: '1'
        -
            title: Dataset
            url: 'https://data.jobtechdev.se/annonser/historiska/index.html'
            showInShort: '1'
        -
            title: 'Getting Started'
            url: 'https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples/historical-ads-info'
            showInShort: '1'
        -
            title: Discuss
            url: 'https://forum.jobtechdev.se/c/vara-api-er-dataset/historiska-annonser/30'
            showInShort: null
    block_1: "## Dataset\nHistorical Jobs is a dataset with all the job abs, which have been published on the Swedish Public Employment Service’s Job Board ([Platsbanken](https://arbetsformedlingen.se/platsbanken/)) since 2006. It contains data from job ads, for instance, information about occupation, employer, place, time, etc). The dataset includes approximately 6,9 million job ads nowadays. The dataset is continuously updated. It simplifies processes for building and training algorithms in relation to machine learning.\n\n## API \nThe API uses the dataset for historical ads and re-uses the code for [JobSearch](https://jobtechdev.se/en/components/jobsearch) to build a new search for all job ads, which have been published on the Swedish Public Employment Service’s job board ([Platsbanken](https://arbetsformedlingen.se/platsbanken/)) since 2006. The API’s first released beta version includes data between 2016-2021. All the job ads are enriched and include data about skills and remote work. The API Historical Ads makes it easier for those, who would like to make a practical use of labour market data. It allows it to be used in a more simple, comprehensive, and structured way.  The API is easy to test and allows quickly download of large amounts of data. Nowadays, the API includes approximately 3.3 million job ads. \n\n## Which solutions does the product provide? \nBy using this product, it is possible to compare time periods, analyse, and follow trends within the labour market area.\n\n## For whom is the product created? \nThe API can be useful for companies, organisations and even private persons, who would like to use job ads’ data for analysis, as well to capture and follow trends within the labour market area. "
    block_2: "The API and dataset Historical ads are free of charge and are available for anyone to use.\nDo you want to know more? Feel free to contact us. "
    product_info:
        -
            title: 'Data format'
            value: JSON
        -
            title: Version
            value: '1.23.1 Beta'
taxonomy:
    category:
        - API
        - Dataset
    type:
        - 'Open data'
    status:
        - Beta
---

