---
custom:
  content: |2+

  block_1: |-
    The occupations in the model are generally based on the Swedish Standard Classification of Occupations (SSYK 2012), although certain professions are combined into broader groups, such as legal professions, for example. Management roles and occupations with fewer than 3000 gainfully employed individuals as of November 2019 are merged into one category "others". The forecast covers the years 2023 and 2026, and encompasses approximately 170 occupations, including the "others" category. The model employs two distinct projections - one for the supply of job vacancies and the other for in-demand roles within each occupation.

    The gainfully employed population is categorized into different skill groups based on their education level, educational orientation, and age. Using historical data, the Swedish Public Employment Service calculates the inflows and outflows of these skill groups and projects the number of employed individuals in each group using population projections from Statistics Sweden. The proportion of newly employed individuals in each forecasted occupation within each skill group is then calculated by the Swedish Public Employment Service. These proportions are multiplied with the corresponding forecasted number of individuals in each group for 2023 and 2026. This process produces an estimate of how many individuals could potentially fill positions in each forecasted profession for 2023 and 2026.

    The model's labour demand encompasses the number of job advertisements and gainfully employed individuals for each occupation and year. The overall demand per industry is projected based on historical data and Statistics Sweden's population forecasts and is manually adjusted based on qualitative assessments of the economic situation. The total demand for each industry is allocated among the various occupations based on their projected proportions within the industry.

    After allocating the demand, a competition index is generated for each profession and year by comparing it to the supply. This index reflects the profession's level of competition relative to other occupations. For more details, please contact the Swedish Public Employment Service's Analysis Department [via the designated function mailbox.](mailto:analysavdelningen@arbetsformedlingen.se)

    Currently, we are refining the methodology for generating occupational forecasts and, as such, will not be releasing any new forecasts. However, we continuously update our assessments for occupations that are impacted by economic changes, typically once per quarter.
  title: Method Description for Occupational Forecasts
  description: Method Description to Produce Occupational Forecasts
  menu:
  - title: Back to Occupational Forecasts
    url: /en/components/yrkesprognoser
    showInShort: null
published: true
title: Method Description for Occupational Forecasts
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/02.components/06.yrkesprognoser/01.metodbeskrivning-yrkesprognoser/component_page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/02.components/06.yrkesprognoser/01.metodbeskrivning-yrkesprognoser/component_page.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/06.yrkesprognoser/01.metodbeskrivning-yrkesprognoser/component_page.en.md
tags:
- user::pages::02.components::06.yrkesprognoser::01.metodbeskrivning-yrkesprognoser::component_page.en.md
- pages::02.components::06.yrkesprognoser::01.metodbeskrivning-yrkesprognoser::component_page.en.md
- 02.components::06.yrkesprognoser::01.metodbeskrivning-yrkesprognoser::component_page.en.md
- 06.yrkesprognoser::01.metodbeskrivning-yrkesprognoser::component_page.en.md
- 01.metodbeskrivning-yrkesprognoser::component_page.en.md
- component_page.en.md
---
---
custom:
    content: "\n"
    block_1: "The occupations in the model are generally based on the Swedish Standard Classification of Occupations (SSYK 2012), although certain professions are combined into broader groups, such as legal professions, for example. Management roles and occupations with fewer than 3000 gainfully employed individuals as of November 2019 are merged into one category \"others\". The forecast covers the years 2023 and 2026, and encompasses approximately 170 occupations, including the \"others\" category. The model employs two distinct projections - one for the supply of job vacancies and the other for in-demand roles within each occupation.\n\nThe gainfully employed population is categorized into different skill groups based on their education level, educational orientation, and age. Using historical data, the Swedish Public Employment Service calculates the inflows and outflows of these skill groups and projects the number of employed individuals in each group using population projections from Statistics Sweden. The proportion of newly employed individuals in each forecasted occupation within each skill group is then calculated by the Swedish Public Employment Service. These proportions are multiplied with the corresponding forecasted number of individuals in each group for 2023 and 2026. This process produces an estimate of how many individuals could potentially fill positions in each forecasted profession for 2023 and 2026.\n\nThe model's labour demand encompasses the number of job advertisements and gainfully employed individuals for each occupation and year. The overall demand per industry is projected based on historical data and Statistics Sweden's population forecasts and is manually adjusted based on qualitative assessments of the economic situation. The total demand for each industry is allocated among the various occupations based on their projected proportions within the industry.\n\nAfter allocating the demand, a competition index is generated for each profession and year by comparing it to the supply. This index reflects the profession's level of competition relative to other occupations. For more details, please contact the Swedish Public Employment Service's Analysis Department [via the designated function mailbox.](mailto:analysavdelningen@arbetsformedlingen.se)\n\nCurrently, we are refining the methodology for generating occupational forecasts and, as such, will not be releasing any new forecasts. However, we continuously update our assessments for occupations that are impacted by economic changes, typically once per quarter."
    title: 'Method Description for Occupational Forecasts'
    description: 'Method Description to Produce Occupational Forecasts'
    menu:
        -
            title: 'Back to Occupational Forecasts'
            url: /en/components/yrkesprognoser
            showInShort: null
published: true
title: 'Method Description for Occupational Forecasts'
---

