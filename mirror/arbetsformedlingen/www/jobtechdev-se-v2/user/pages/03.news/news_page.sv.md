---
title: Aktuellt
routes:
  default: /nyheter
admin:
  children_display_order: collection
twig_first: false
custom:
  text: 'Ta del av aktuella nyheter, artiklar, insikter, kommande event och webinarier, som JobTech Development arrangerar, deltar i, eller är av intresse för det digitala ekosystemet. '
  title: Aktuellt
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/news_page.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/news_page.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/news_page.sv.md
tags:
- user::pages::03.news::news_page.sv.md
- pages::03.news::news_page.sv.md
- 03.news::news_page.sv.md
- news_page.sv.md
---
---
title: Aktuellt
routes:
    default: /nyheter
admin:
    children_display_order: collection
twig_first: false
custom:
    text: 'Ta del av aktuella nyheter, artiklar, insikter, kommande event och webinarier, som JobTech Development arrangerar, deltar i, eller är av intresse för det digitala ekosystemet. '
    title: Aktuellt
---

