---
title: Supporting Lifelong Learning through New Technology
custom:
  title: Supporting Lifelong Learning through New Technology
  short: On May 9th 2023, Maria Mindhammar, the Director-General of the Swedish Public Employment Service (SPES), participated in a panel discussion focused on the theme of Jobtech vs. Edtech. The event was organised by the interest association, Swedish JobTech, in collaboration with the trade association for Swedish Edtech. We seized the opportunity to pose a few questions to the Director-General.
  content: |-
    **On May 9th 2023, Maria Mindhammar, the Director-General of the Swedish Public Employment Service (SPES), participated in a panel discussion focused on the theme of Jobtech vs. Edtech. The event was organised by the interest association, Swedish JobTech, in collaboration with the trade association for Swedish Edtech. We seized the opportunity to pose a few questions to the Director-General.**

    ![Director-General Maria Mindhammar](./maria-mindhammar-2278.jpg)
    _Director-General Maria Mindhammar_

    **How does the Swedish Public Employment Service (SPES) address challenges in skills supply and labour market skill shortages through the use of new technology?** <BR>
    "SPES has been at the forefront of utilizing data-driven methods to enhance the effectiveness of matchmaking and promote the development of matching services. Over five years ago, we established the innovative unit "Jobtech", which manages the open platform known as JobTech Development. This platform provides access to labour market data, facilitating its seamless sharing and utilisation to benefit all actors in the labour market, completely free of charge. Currently, the platform boasts a thriving digital ecosystem, bringing together approximately 200 companies and organisations."


    **What does the development process entail?**<BR>
    "Presently, SPES, in its coordinating role, plays an important role in a significant government assignment spanning three policy areas. This assignment focuses on the development of an integrated and cohesive data infrastructure dedicated to skills supply and lifelong learning. The ultimate objective is to boost the capabilities of authorities and other stakeholders in designing and delivering digital services that enhance individuals' standing in the labour market, all while effectively addressing the skills requirements of both the private and public sectors."


    **How does connecting data between the labour market and education improve conditions for sustainable matching in the labor market?**<BR>
    "The integration of data between the labour and education markets paves the way for a new technology area, which we have identified and called JobEdTech. Through this connection, JobEdTech leverages the link between education and skills required for various occupations, ultimately supporting lifelong learning, a crucial foundation for Sweden's future skill supply."


    **Why is SPES the platform leader for jobtechdev.se?**<BR>
    "SPES assumes the role of a platform leader for jobtechdev.se due to its position as the largest player in the market, bringing extensive expertise and experience, and the trust it commands. As an authority, SPES brings a long-term perspective to the table, facilitating the development of an open digital infrastructure with common technical structures, solutions, and standards. SPES plays a crucial role in fostering collaborations, encouraging data sharing, and generating innovative ideas. Through collaborative efforts and data sharing, we can collectively work towards creating a sustainable labour market that benefits everyone."

    [Here, you can watch the entire panel discussion "Jobtech vs Edtech" (in Swedish).](https://www.youtube.com/watch?app=desktop&v=3V2SSbXUlIg)
  infobox: "**Learn more about JobTech Developments ongoing projects: \n**\n* [**Personal Data and Data Portability**](https://jobtechdev.se/en/components/individdata-och-dataportabilitet)\n* [**Taxonomy and Common Language**](https://jobtechdev.se/en/components/taxonomi-och-begreppsstruktur)\n* [**Digital Guidance and Linked Data**](https://jobtechdev.se/en/components/digital-vaegledning-och-laenkad-data) \n* [**Digital Vocational Career Guidance**](https://jobtechdev.se/en/components/digital-yrkesvaegledning) \n* [**API JobEdConnect JobEd Connect**](https://jobtechdev.se/en/components/jobed-connect) \n"
date: 2023-05-15 11:58
taxonomy:
  category:
  - News
  type:
  - Interview
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/52.ny-teknik-stoettar-det-livslanga-laerandet/news_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/52.ny-teknik-stoettar-det-livslanga-laerandet/news_blog_post.en.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/52.ny-teknik-stoettar-det-livslanga-laerandet/news_blog_post.en.md
tags:
- user::pages::03.news::52.ny-teknik-stoettar-det-livslanga-laerandet::news_blog_post.en.md
- pages::03.news::52.ny-teknik-stoettar-det-livslanga-laerandet::news_blog_post.en.md
- 03.news::52.ny-teknik-stoettar-det-livslanga-laerandet::news_blog_post.en.md
- 52.ny-teknik-stoettar-det-livslanga-laerandet::news_blog_post.en.md
- news_blog_post.en.md
---
---
title: 'Supporting Lifelong Learning through New Technology'
custom:
    title: 'Supporting Lifelong Learning through New Technology'
    short: 'On May 9th 2023, Maria Mindhammar, the Director-General of the Swedish Public Employment Service (SPES), participated in a panel discussion focused on the theme of Jobtech vs. Edtech. The event was organised by the interest association, Swedish JobTech, in collaboration with the trade association for Swedish Edtech. We seized the opportunity to pose a few questions to the Director-General.'
    content: "**On May 9th 2023, Maria Mindhammar, the Director-General of the Swedish Public Employment Service (SPES), participated in a panel discussion focused on the theme of Jobtech vs. Edtech. The event was organised by the interest association, Swedish JobTech, in collaboration with the trade association for Swedish Edtech. We seized the opportunity to pose a few questions to the Director-General.**\n\n![Director-General Maria Mindhammar](./maria-mindhammar-2278.jpg)\n_Director-General Maria Mindhammar_\n\n**How does the Swedish Public Employment Service (SPES) address challenges in skills supply and labour market skill shortages through the use of new technology?** <BR>\n\"SPES has been at the forefront of utilizing data-driven methods to enhance the effectiveness of matchmaking and promote the development of matching services. Over five years ago, we established the innovative unit \"Jobtech\", which manages the open platform known as JobTech Development. This platform provides access to labour market data, facilitating its seamless sharing and utilisation to benefit all actors in the labour market, completely free of charge. Currently, the platform boasts a thriving digital ecosystem, bringing together approximately 200 companies and organisations.\"\n\n\n**What does the development process entail?**<BR>\n\"Presently, SPES, in its coordinating role, plays an important role in a significant government assignment spanning three policy areas. This assignment focuses on the development of an integrated and cohesive data infrastructure dedicated to skills supply and lifelong learning. The ultimate objective is to boost the capabilities of authorities and other stakeholders in designing and delivering digital services that enhance individuals' standing in the labour market, all while effectively addressing the skills requirements of both the private and public sectors.\"\n\n\n**How does connecting data between the labour market and education improve conditions for sustainable matching in the labor market?**<BR>\n\"The integration of data between the labour and education markets paves the way for a new technology area, which we have identified and called JobEdTech. Through this connection, JobEdTech leverages the link between education and skills required for various occupations, ultimately supporting lifelong learning, a crucial foundation for Sweden's future skill supply.\"\n\n\n**Why is SPES the platform leader for jobtechdev.se?**<BR>\n\"SPES assumes the role of a platform leader for jobtechdev.se due to its position as the largest player in the market, bringing extensive expertise and experience, and the trust it commands. As an authority, SPES brings a long-term perspective to the table, facilitating the development of an open digital infrastructure with common technical structures, solutions, and standards. SPES plays a crucial role in fostering collaborations, encouraging data sharing, and generating innovative ideas. Through collaborative efforts and data sharing, we can collectively work towards creating a sustainable labour market that benefits everyone.\"\n\n[Here, you can watch the entire panel discussion \"Jobtech vs Edtech\" (in Swedish).](https://www.youtube.com/watch?app=desktop&v=3V2SSbXUlIg)"
    infobox: "**Learn more about JobTech Developments ongoing projects: \n**\n* [**Personal Data and Data Portability**](https://jobtechdev.se/en/components/individdata-och-dataportabilitet)\n* [**Taxonomy and Common Language**](https://jobtechdev.se/en/components/taxonomi-och-begreppsstruktur)\n* [**Digital Guidance and Linked Data**](https://jobtechdev.se/en/components/digital-vaegledning-och-laenkad-data) \n* [**Digital Vocational Career Guidance**](https://jobtechdev.se/en/components/digital-yrkesvaegledning) \n* [**API JobEdConnect JobEd Connect**](https://jobtechdev.se/en/components/jobed-connect) \n"
date: '2023-05-15 11:58'
taxonomy:
    category:
        - News
    type:
        - Interview
---

