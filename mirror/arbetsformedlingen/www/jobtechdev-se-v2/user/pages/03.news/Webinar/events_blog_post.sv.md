---
title: Kom igång med JobTechs öppna API:er.
process:
  markdown: true
  twig: true
cache_enable: true
custom:
  title: Kom igång med JobTechs öppna API:er.
  date: 2022-03-25 10:00
  short: Vi vill göra det enkelt att använda våra apier, därför bjuder vi in dig och alla andra användare till ett kom-igång webinarie.
  content: "Vi vill göra det enkelt att använda våra apier, därför bjuder vi in dig och alla andra som är nyfikna på vad vi gör, till ett kom-igång webinarie. \n  \nGenom att använda våra api:er bidrar du till att vi tillsammans kan jobba för en bättre och effektivare arbetsmarknad. Oavsett om syftet är att labba på egen kammare, jobba med att utveckla nästa optimala matchningstjänst eller förbättra en befintlig produkt – är du varmt välkommen till oss.\n\nPå mötet kommer du att träffa utvecklarna bakom Jobtech Search/Jobtech Stream, Historiska annonser, Jobtech Taxonomy, JobAd Enrichments och Individdata, där vi driver ett spännande pilotprojekt.\n\nEfter presentationen finns det möjlighet att ställa frågor, ge feedback och framföra idéer och önskemål för nya tekniska lösningar från JobTech Development.\n \nVi serverar ingen färdig agenda för mötet utan innehållet styrs utifrån dina specifika intressen och behov enligt sk. ”open space”-modell.\n\nVarmt välkommen!\n \nJobTech Development team\n\n\n\n\n"
  endtime: 2022-03-25 11:00
taxonomy:
  category:
  - Event
  status:
  - Avslutad
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/Webinar/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/Webinar/events_blog_post.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/Webinar/events_blog_post.sv.md
tags:
- user::pages::03.news::Webinar::events_blog_post.sv.md
- pages::03.news::Webinar::events_blog_post.sv.md
- 03.news::Webinar::events_blog_post.sv.md
- Webinar::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'Kom igång med JobTechs öppna API:er.'
process:
    markdown: true
    twig: true
cache_enable: true
custom:
    title: 'Kom igång med JobTechs öppna API:er.'
    date: '2022-03-25 10:00'
    short: 'Vi vill göra det enkelt att använda våra apier, därför bjuder vi in dig och alla andra användare till ett kom-igång webinarie.'
    content: "Vi vill göra det enkelt att använda våra apier, därför bjuder vi in dig och alla andra som är nyfikna på vad vi gör, till ett kom-igång webinarie. \n  \nGenom att använda våra api:er bidrar du till att vi tillsammans kan jobba för en bättre och effektivare arbetsmarknad. Oavsett om syftet är att labba på egen kammare, jobba med att utveckla nästa optimala matchningstjänst eller förbättra en befintlig produkt – är du varmt välkommen till oss.\n\nPå mötet kommer du att träffa utvecklarna bakom Jobtech Search/Jobtech Stream, Historiska annonser, Jobtech Taxonomy, JobAd Enrichments och Individdata, där vi driver ett spännande pilotprojekt.\n\nEfter presentationen finns det möjlighet att ställa frågor, ge feedback och framföra idéer och önskemål för nya tekniska lösningar från JobTech Development.\n \nVi serverar ingen färdig agenda för mötet utan innehållet styrs utifrån dina specifika intressen och behov enligt sk. ”open space”-modell.\n\nVarmt välkommen!\n \nJobTech Development team\n\n\n\n\n"
    endtime: '2022-03-25 11:00'
taxonomy:
    category:
        - Event
    status:
        - Avslutad
---

