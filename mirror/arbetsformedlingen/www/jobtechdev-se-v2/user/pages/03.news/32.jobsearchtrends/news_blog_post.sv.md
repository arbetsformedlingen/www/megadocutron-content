---
title: 'Nytt dataset: JobSearch Trends'
custom:
  title: 'Nytt dataset: JobSearch Trends'
  short: JobSearch Trends är ett nytt dataset som visar sökbeteenden över tid på Arbetsförmedlingens annonsplattform Platsbanken.
  content: "## JobSearch Trends är ett nytt dataset som visar sökbeteenden över tid på Arbetsförmedlingens annonsplattform [Platsbanken](https://arbetsformedlingen.se/platsbanken).\n## \n\nNya filer generas varje dag och innehåller de mest använda sökorden inom till exempel yrken, språk, kompetenser, orter och arbetsgivare. JobSearch Trends är användbart för alla som vill träna algoritmer i samband med maskininlärning, och förstå användarbeteenden på arbetsmarknaden bättre, genom strukturerad och aggregerad rådata.\n\n**Oskar Drenske, utvecklare på JobTech, vad var upphovet till JobSearch Trends?**\n\n”Att kunna sammanställa vad våra användare söker på har diskuterats länge internt på JobTech, men det var inte förrän jag deltog i ett samverkansmöte om digitalisering, kompetensförsörjning och livslångt lärande i mars som jag insåg att det behövde prioriteras och genomföras. Vi diskuterade annonser och jobbsökningar, och om det saknas kunskap om vad arbetssökande efterfrågar så är det svårt att överbrygga gapet mellan efterfrågan och utbud.”\n\n**Vad kan JobSearch Trends användas till?**\n\n”Datasetet svarar på vad som efterfrågas av de sökande idag, och när vi samlat data tillräckligt länge så kan det användas för att se förändringar – söktrender helt enkelt.”\n\n**Går det att använda datasetet i kombination med andra API:er på JobTech Development-plattformen?**\n\n”JobSearch Trends skulle kunna användas i kombination med komponenter som visar trender baserat på annonser, till exempel API:et Digital Skills.\nJag hoppas att någon kommer på en kombination som inte jag tänkt på!\"\n\nDatasetet är i en utvecklingsfas och vi välkomnar feedback och synpunkter från alla användare. \n\n\nLäs mer om [JobSearch Trends](https://www.jobtechdev.se/sv/komponenter/jobsearch-trends). \n\nDiskutera gärna datasetet i vårt [forum](https://forum.jobtechdev.se/t/jobsearch-trends/629). \n "
date: 2022-09-13 10:44
taxonomy:
  category:
  - Nyhet
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/32.jobsearchtrends/news_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/32.jobsearchtrends/news_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/32.jobsearchtrends/news_blog_post.sv.md
tags:
- user::pages::03.news::32.jobsearchtrends::news_blog_post.sv.md
- pages::03.news::32.jobsearchtrends::news_blog_post.sv.md
- 03.news::32.jobsearchtrends::news_blog_post.sv.md
- 32.jobsearchtrends::news_blog_post.sv.md
- news_blog_post.sv.md
---
---
title: 'Nytt dataset: JobSearch Trends'
custom:
    title: 'Nytt dataset: JobSearch Trends'
    short: 'JobSearch Trends är ett nytt dataset som visar sökbeteenden över tid på Arbetsförmedlingens annonsplattform Platsbanken.'
    content: "## JobSearch Trends är ett nytt dataset som visar sökbeteenden över tid på Arbetsförmedlingens annonsplattform [Platsbanken](https://arbetsformedlingen.se/platsbanken).\n## \n\nNya filer generas varje dag och innehåller de mest använda sökorden inom till exempel yrken, språk, kompetenser, orter och arbetsgivare. JobSearch Trends är användbart för alla som vill träna algoritmer i samband med maskininlärning, och förstå användarbeteenden på arbetsmarknaden bättre, genom strukturerad och aggregerad rådata.\n\n**Oskar Drenske, utvecklare på JobTech, vad var upphovet till JobSearch Trends?**\n\n”Att kunna sammanställa vad våra användare söker på har diskuterats länge internt på JobTech, men det var inte förrän jag deltog i ett samverkansmöte om digitalisering, kompetensförsörjning och livslångt lärande i mars som jag insåg att det behövde prioriteras och genomföras. Vi diskuterade annonser och jobbsökningar, och om det saknas kunskap om vad arbetssökande efterfrågar så är det svårt att överbrygga gapet mellan efterfrågan och utbud.”\n\n**Vad kan JobSearch Trends användas till?**\n\n”Datasetet svarar på vad som efterfrågas av de sökande idag, och när vi samlat data tillräckligt länge så kan det användas för att se förändringar – söktrender helt enkelt.”\n\n**Går det att använda datasetet i kombination med andra API:er på JobTech Development-plattformen?**\n\n”JobSearch Trends skulle kunna användas i kombination med komponenter som visar trender baserat på annonser, till exempel API:et Digital Skills.\nJag hoppas att någon kommer på en kombination som inte jag tänkt på!\"\n\nDatasetet är i en utvecklingsfas och vi välkomnar feedback och synpunkter från alla användare. \n\n\nLäs mer om [JobSearch Trends](https://www.jobtechdev.se/sv/komponenter/jobsearch-trends). \n\nDiskutera gärna datasetet i vårt [forum](https://forum.jobtechdev.se/t/jobsearch-trends/629). \n "
date: '2022-09-13 10:44'
taxonomy:
    category:
        - Nyhet
---

