---
title: 'New Dataset: JobSearch Trends'
custom:
  title: 'New Dataset: JobSearch Trends'
  short: JobSearch Trends is a new dataset, which shows search behavior on the Swedish Public Employment Service’s Job Board Platsbanken over time.
  content: "## JobSearch Trends is a new dataset, which shows search behavior on the Swedish Public Employment Service’s Job Board [Platsbanken](https://arbetsformedlingen.se/platsbanken) over time.\n## \n\nNew files are to be generated daily with the most common keywords within profession, language, skills, location, and employer, among others. JobSearch Trends can be useful for anyone, who would like to simplify processes for building and training algorithms concerning machine learning, as well as to understand better the search behavior within the labour market area, by using structured and aggregated raw data.\n\n**Oskar Drenske, a developer at JobTech. How have the idea and the need been born for JobSearch Trends?**\n\n\"We, at JobTech, are constantly asking ourselves what is relevant for our users and what our users are looking for? Being able to map the relevant needs of our users is part of our ongoing development work. When in March, I attended a joint seminar dedicated to digitization, skills supply, and lifelong learning, I realized that it is right on time to prioritize and implement an idea we have had for some time. At the conference, we have discussed advertisements and job searches, and if there is a lack of knowledge about what the job seekers are looking for, it is difficult to bridge the gap between demand and supply.\"\n\n**How JobSearch Trends can be used for?**\n\n\"The dataset gives answers to what job seekers are searching for today. When we have collected enough data, we can then use this data to follow up changes. Simply, it gives an overview of search trends.\"\n\n**Is it possible to use the dataset in combination with other APIs on the JobTech Development platform?**\n\n\"JobSearch Trends could be used in combination with components that show trends, based on job advertisements, for example, the API Digital Skills. I am hoping that someone else will come up with another idea or combination I haven’t thought of!”\n\n\nThe dataset is under ongoing development, and we are happy to get feedback and suggestions for improvements from our users. \n\nRead more about [JobSearch Trends here](https://www.jobtechdev.se/en/components/jobsearch-trends). \n\nFeel free to contact us or discuss it in our [Community Forum here](https://forum.jobtechdev.se/t/jobsearch-trends/629). \n\n"
date: 2022-09-13 10:44
taxonomy:
  category:
  - News
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/32.jobsearchtrends/news_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/32.jobsearchtrends/news_blog_post.en.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/32.jobsearchtrends/news_blog_post.en.md
tags:
- user::pages::03.news::32.jobsearchtrends::news_blog_post.en.md
- pages::03.news::32.jobsearchtrends::news_blog_post.en.md
- 03.news::32.jobsearchtrends::news_blog_post.en.md
- 32.jobsearchtrends::news_blog_post.en.md
- news_blog_post.en.md
---
---
title: 'New Dataset: JobSearch Trends'
custom:
    title: 'New Dataset: JobSearch Trends'
    short: 'JobSearch Trends is a new dataset, which shows search behavior on the Swedish Public Employment Service’s Job Board Platsbanken over time.'
    content: "## JobSearch Trends is a new dataset, which shows search behavior on the Swedish Public Employment Service’s Job Board [Platsbanken](https://arbetsformedlingen.se/platsbanken) over time.\n## \n\nNew files are to be generated daily with the most common keywords within profession, language, skills, location, and employer, among others. JobSearch Trends can be useful for anyone, who would like to simplify processes for building and training algorithms concerning machine learning, as well as to understand better the search behavior within the labour market area, by using structured and aggregated raw data.\n\n**Oskar Drenske, a developer at JobTech. How have the idea and the need been born for JobSearch Trends?**\n\n\"We, at JobTech, are constantly asking ourselves what is relevant for our users and what our users are looking for? Being able to map the relevant needs of our users is part of our ongoing development work. When in March, I attended a joint seminar dedicated to digitization, skills supply, and lifelong learning, I realized that it is right on time to prioritize and implement an idea we have had for some time. At the conference, we have discussed advertisements and job searches, and if there is a lack of knowledge about what the job seekers are looking for, it is difficult to bridge the gap between demand and supply.\"\n\n**How JobSearch Trends can be used for?**\n\n\"The dataset gives answers to what job seekers are searching for today. When we have collected enough data, we can then use this data to follow up changes. Simply, it gives an overview of search trends.\"\n\n**Is it possible to use the dataset in combination with other APIs on the JobTech Development platform?**\n\n\"JobSearch Trends could be used in combination with components that show trends, based on job advertisements, for example, the API Digital Skills. I am hoping that someone else will come up with another idea or combination I haven’t thought of!”\n\n\nThe dataset is under ongoing development, and we are happy to get feedback and suggestions for improvements from our users. \n\nRead more about [JobSearch Trends here](https://www.jobtechdev.se/en/components/jobsearch-trends). \n\nFeel free to contact us or discuss it in our [Community Forum here](https://forum.jobtechdev.se/t/jobsearch-trends/629). \n\n"
date: '2022-09-13 10:44'
taxonomy:
    category:
        - News
---

