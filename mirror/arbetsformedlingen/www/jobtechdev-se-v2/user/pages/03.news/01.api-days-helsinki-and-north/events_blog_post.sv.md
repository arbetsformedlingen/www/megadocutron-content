---
title: 'JobTech Development deltar på API days Helsinki & North '
custom:
  title: 'JobTech Development deltar på API days Helsinki & North '
  date: 2023-06-05 08:00
  endtime: 2023-06-06 17:00
  short: 'JobTech Development deltar på API days Helsinki & North den 5-6 juni. Eventet är en i raden av de enormt populära apidays-evenemangen globalt.  I år kommer JobTech Developments Carl Lernberg att presentera ett av våra senast utvecklade APIer "JobEd Connect." '
  content: "**JobTech Development deltar på API days Helsinki & North den 5-6 juni. Eventet är en i raden av de enormt populära apidays-evenemangen globalt. \n**\nI år kommer JobTech Developments Carl Lernberg att presentera ett av våra senast utvecklade APIer \"JobEd Connect\". \n\nAPIet är en matchningslösning som automatiskt kopplar ihop utbildningar med relaterade yrken, utifrån vad som efterfrågas på arbetsmarknaden.\nData kommer från utbildningssystemet SUSA-navet och Arbetsförmedlingens egen arbetsmarknadsdata.\n\nMissa inte tillfället att lyssna på Carl och lär dig mer om hur du kan använda \"JobEd Connect\":\n\nPresentationen som har titeln: **\"Open matching solution linking educations to occupations\"** \nkan ni lyssna på dag 1 den 5 juni kl 13:00 lokal tid (kl 12:00 svensk tid) \n\n[För agenda och registrering(eng)](https://www.apidays.global/helsinki_and_north/)\n\n"
  minititle: 5-6 Juni
date: 2023-05-23 13:05
taxonomy:
  category:
  - Event
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/01.api-days-helsinki-and-north/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/01.api-days-helsinki-and-north/events_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/01.api-days-helsinki-and-north/events_blog_post.sv.md
tags:
- user::pages::03.news::01.api-days-helsinki-and-north::events_blog_post.sv.md
- pages::03.news::01.api-days-helsinki-and-north::events_blog_post.sv.md
- 03.news::01.api-days-helsinki-and-north::events_blog_post.sv.md
- 01.api-days-helsinki-and-north::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'JobTech Development deltar på API days Helsinki & North '
custom:
    title: 'JobTech Development deltar på API days Helsinki & North '
    date: '2023-06-05 08:00'
    endtime: '2023-06-06 17:00'
    short: 'JobTech Development deltar på API days Helsinki & North den 5-6 juni. Eventet är en i raden av de enormt populära apidays-evenemangen globalt.  I år kommer JobTech Developments Carl Lernberg att presentera ett av våra senast utvecklade APIer "JobEd Connect." '
    content: "**JobTech Development deltar på API days Helsinki & North den 5-6 juni. Eventet är en i raden av de enormt populära apidays-evenemangen globalt. \n**\nI år kommer JobTech Developments Carl Lernberg att presentera ett av våra senast utvecklade APIer \"JobEd Connect\". \n\nAPIet är en matchningslösning som automatiskt kopplar ihop utbildningar med relaterade yrken, utifrån vad som efterfrågas på arbetsmarknaden.\nData kommer från utbildningssystemet SUSA-navet och Arbetsförmedlingens egen arbetsmarknadsdata.\n\nMissa inte tillfället att lyssna på Carl och lär dig mer om hur du kan använda \"JobEd Connect\":\n\nPresentationen som har titeln: **\"Open matching solution linking educations to occupations\"** \nkan ni lyssna på dag 1 den 5 juni kl 13:00 lokal tid (kl 12:00 svensk tid) \n\n[För agenda och registrering(eng)](https://www.apidays.global/helsinki_and_north/)\n\n"
    minititle: '5-6 Juni'
date: '2023-05-23 13:05'
taxonomy:
    category:
        - Event
---

