---
title: API:et JobStream vann Sweden API Award
custom:
  title: API:et JobStream vann Sweden API Award
  date: 2021-11-22 16:04
  short: Ett dedikerat team på JobTech Development har lyssnat på användarnas behov och utvecklat API:et.
  content: "**API:et JobStream vann Sweden API Award. Ett dedikerat team på JobTech Development har lyssnat på användarnas behov och utvecklat API:et. **\n \nNomineringen lyder: _\"Arbetsförmedlingen revolutionerar matchningen på arbetsmarknaden. JobStream är användbart för alla företag och organisationer som har en egen sökmotor och vill förbättra sin tjänst med realtidsuppdaterade annonsdata. API:et kan också användas för att bygga och träna algoritmer i samband med maskininlärning eller för att identifiera och analysera trender på arbetsmarknaden utifrån annonsdata.”_\n \n**Hur kom idén till? **\n\n_”Efter att ha lyssnat på användarna fick man en insikt att värdet låg i datan.  Då byggdes ett API, som förser Sverige med de annonser som finns för tillfället. Lika enkelt som att klicka på en webblänk för att ta del av Platsbankens alla annonser. Idag får vi cirka 25 miljoner anrop om jobbannonser per månad från externa partner. Sluteffekten med att tillhandahålla annonser öppet är att flera ska kunna hjälpa arbetsgivare och arbetssökande att enklare hitta varandra på nya sätt”,_ säger Jonas Södergren från JobTech Development. \n\nVi går från ego till eko och delar med oss. API:et är kostnadsfritt och tillgängligt för alla. Om du vill kopiera utvecklarnas framgångsrecept, gör så här: _”Lyssna på användaren, gör en sak och gör den bra”._\n\n\nLänk till [API:et.](https://jobtechdev.se/sv/komponenter/jobstream)\n\n"
date: 2021-11-22 16:04
taxonomy:
  category:
  - Nyhet
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/18.api-et-jobstream-aer-nominerat-till-sweden-api-award/news_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/18.api-et-jobstream-aer-nominerat-till-sweden-api-award/news_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/18.api-et-jobstream-aer-nominerat-till-sweden-api-award/news_blog_post.sv.md
tags:
- user::pages::03.news::18.api-et-jobstream-aer-nominerat-till-sweden-api-award::news_blog_post.sv.md
- pages::03.news::18.api-et-jobstream-aer-nominerat-till-sweden-api-award::news_blog_post.sv.md
- 03.news::18.api-et-jobstream-aer-nominerat-till-sweden-api-award::news_blog_post.sv.md
- 18.api-et-jobstream-aer-nominerat-till-sweden-api-award::news_blog_post.sv.md
- news_blog_post.sv.md
---
---
title: 'API:et JobStream vann Sweden API Award'
custom:
    title: 'API:et JobStream vann Sweden API Award'
    date: '2021-11-22 16:04'
    short: 'Ett dedikerat team på JobTech Development har lyssnat på användarnas behov och utvecklat API:et.'
    content: "**API:et JobStream vann Sweden API Award. Ett dedikerat team på JobTech Development har lyssnat på användarnas behov och utvecklat API:et. **\n \nNomineringen lyder: _\"Arbetsförmedlingen revolutionerar matchningen på arbetsmarknaden. JobStream är användbart för alla företag och organisationer som har en egen sökmotor och vill förbättra sin tjänst med realtidsuppdaterade annonsdata. API:et kan också användas för att bygga och träna algoritmer i samband med maskininlärning eller för att identifiera och analysera trender på arbetsmarknaden utifrån annonsdata.”_\n \n**Hur kom idén till? **\n\n_”Efter att ha lyssnat på användarna fick man en insikt att värdet låg i datan.  Då byggdes ett API, som förser Sverige med de annonser som finns för tillfället. Lika enkelt som att klicka på en webblänk för att ta del av Platsbankens alla annonser. Idag får vi cirka 25 miljoner anrop om jobbannonser per månad från externa partner. Sluteffekten med att tillhandahålla annonser öppet är att flera ska kunna hjälpa arbetsgivare och arbetssökande att enklare hitta varandra på nya sätt”,_ säger Jonas Södergren från JobTech Development. \n\nVi går från ego till eko och delar med oss. API:et är kostnadsfritt och tillgängligt för alla. Om du vill kopiera utvecklarnas framgångsrecept, gör så här: _”Lyssna på användaren, gör en sak och gör den bra”._\n\n\nLänk till [API:et.](https://jobtechdev.se/sv/komponenter/jobstream)\n\n"
date: '2021-11-22 16:04'
taxonomy:
    category:
        - Nyhet
---

