---
title: Tech Awards Sweden 2023
custom:
  title: Tech Awards Sweden 2023
  date: 2023-03-22 13:00
  endtime: 2023-03-22 22:00
  short: Innovators and influential forces gather to transform traditional industries and create new paths. To recognize these efforts, Foundry and TechSverige hold the annual Tech Awards ceremony, where individuals, companies, organizations, and initiatives are honored with awards and celebration.
  content: "## Innovators and influential forces gather to transform traditional industries and create new paths. To recognize these efforts, Foundry and TechSverige hold the annual Tech Awards ceremony, where individuals, companies, organizations, and initiatives are honored with awards and celebration.\n## \nOn March 22, 2023, there will be a mingling and party event to honor heroes in tech and celebrate their achievements with an award ceremony in six categories.\nAwards to be given out in the categories:\n\n* The Year's Security Prize\n* The Year's Competence Prize\n* Tech50 - Most Influential in Tech\n* The Year's Most Valuable Use of Tech\n* The Year's Sustainability Prize\n* The Year's Tech Company\n\nWelcome to Tech Awards 2023!\n\n[Details and enrollment](https://techawards.se/)\n\n\n"
date: 2023-02-03 10:50
taxonomy:
  category:
  - Event
publish_date: 2023-02-03 10:53
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/09.tech-awards-sweden-2023/events_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/09.tech-awards-sweden-2023/events_blog_post.en.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/09.tech-awards-sweden-2023/events_blog_post.en.md
tags:
- user::pages::03.news::09.tech-awards-sweden-2023::events_blog_post.en.md
- pages::03.news::09.tech-awards-sweden-2023::events_blog_post.en.md
- 03.news::09.tech-awards-sweden-2023::events_blog_post.en.md
- 09.tech-awards-sweden-2023::events_blog_post.en.md
- events_blog_post.en.md
---
---
title: 'Tech Awards Sweden 2023'
custom:
    title: 'Tech Awards Sweden 2023'
    date: '2023-03-22 13:00'
    endtime: '2023-03-22 22:00'
    short: 'Innovators and influential forces gather to transform traditional industries and create new paths. To recognize these efforts, Foundry and TechSverige hold the annual Tech Awards ceremony, where individuals, companies, organizations, and initiatives are honored with awards and celebration.'
    content: "## Innovators and influential forces gather to transform traditional industries and create new paths. To recognize these efforts, Foundry and TechSverige hold the annual Tech Awards ceremony, where individuals, companies, organizations, and initiatives are honored with awards and celebration.\n## \nOn March 22, 2023, there will be a mingling and party event to honor heroes in tech and celebrate their achievements with an award ceremony in six categories.\nAwards to be given out in the categories:\n\n* The Year's Security Prize\n* The Year's Competence Prize\n* Tech50 - Most Influential in Tech\n* The Year's Most Valuable Use of Tech\n* The Year's Sustainability Prize\n* The Year's Tech Company\n\nWelcome to Tech Awards 2023!\n\n[Details and enrollment](https://techawards.se/)\n\n\n"
date: '2023-02-03 10:50'
taxonomy:
    category:
        - Event
publish_date: '2023-02-03 10:53'
---

