---
title: Jobnet Wants to Become a Natural Destination for Job Seekers
custom:
  title: Jobnet Wants to Become a Natural Destination for Job Seekers
  short: 'We have read on their website: "At Jobnet, you search on your terms, anonymously and smoothly." We became curious and kindly asked for an interview with Kalle Bodestig, CEO and Founder of the Jobnet platform.'
  content: "**We have read on their website: \"At Jobnet, you search on your terms, anonymously and smoothly.\" This intrigued us and we kindly asked for conducting an interview with Kalle Bodestig, CEO and Founder of the Jobnet platform.**\n\n![Kalle Bodestig](kalle-kontor-liten.jpg)\n_Kalle Bodestig, CEO and founder of Jobnet_\n\n**Tell us about Jobnet! What do you do?** \n\n\"Jobnet is a platform with the ambition to become the equivalent of Hemnet (e.g. Sweden’s largest real estate platform) in the job market; a place where job seekers can look after their next workplace without the need to create a profile. We believe in the idea that the job seeker chooses his next workplace and not the other way around. The companies on the platform have space for employer branding and can attract candidates by showcasing their companies. This results in better matching with candidates who understand better what the future workplace offers.\"\n\n**Which target groups do you address? **\n\n\"Our main focus is on the job seeker. If we succeed in attracting many job seekers to our platform, recruiters and companies will follow suit, as they want to be where the talents are.\"\n\n**Which JobTech APIs do you use on the Jobnet platform?**\n\n\"[Jobstream](https://www.jobtechdev.se/en/components/jobstream), [JobAd Enrichments](https://www.jobtechdev.se/en/components/jobad-enrichments), and [JobTech Taxonomy](https://www.jobtechdev.se/en/components/jobad-enrichments) are the ones we are currently utilizing.\"\n\n**Can you describe how you work and how the APIs have helped you in your business to achieve better results?\n**\n\n\"We utilize the APIs in several different ways. Our search structure and categorisation inherited the same structure of the data already read in; a workload that would otherwise have taken several hundred hours. Our latest application area features publicly accessible statistical sites. These sites showcase the fusion of diverse data sources, enabling us to provide valuable insights such as profession and competency forecasts. These interactive platforms have gained rapid popularity and serve as a testament to the power of the APIs in enabling us to create innovative solutions that would have otherwise been unattainable.\"\n\n**What is the next step for Jobnet, and where do you see yourselves in five years?**\n\n\"We are now opening up our platform to external recruitment agencies so that they can bring in their companies. It allows us to scale our business up. Our long-term goal is to become the largest destination for job seekers after Arbetsförmedlingen and LinkedIn. Given the time to advance further, we have been able to develop modern tools for the last couple of years. It will be now rewarding for us to allow more recruiters to use them!\"\n\n[Kindly,visit Jobnet (SE)](https://jobnet.se/)"
date: 2023-05-02 15:56
taxonomy:
  category:
  - News
  type:
  - Interview
media_order: kalle-kontor-liten.jpg
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/02.jobnet-vill-bli-en-sjaelvklar-destination-foer-jobbsoekare/news_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/02.jobnet-vill-bli-en-sjaelvklar-destination-foer-jobbsoekare/news_blog_post.en.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/02.jobnet-vill-bli-en-sjaelvklar-destination-foer-jobbsoekare/news_blog_post.en.md
tags:
- user::pages::03.news::02.jobnet-vill-bli-en-sjaelvklar-destination-foer-jobbsoekare::news_blog_post.en.md
- pages::03.news::02.jobnet-vill-bli-en-sjaelvklar-destination-foer-jobbsoekare::news_blog_post.en.md
- 03.news::02.jobnet-vill-bli-en-sjaelvklar-destination-foer-jobbsoekare::news_blog_post.en.md
- 02.jobnet-vill-bli-en-sjaelvklar-destination-foer-jobbsoekare::news_blog_post.en.md
- news_blog_post.en.md
---
---
title: 'Jobnet Wants to Become a Natural Destination for Job Seekers'
custom:
    title: 'Jobnet Wants to Become a Natural Destination for Job Seekers'
    short: 'We have read on their website: "At Jobnet, you search on your terms, anonymously and smoothly." We became curious and kindly asked for an interview with Kalle Bodestig, CEO and Founder of the Jobnet platform.'
    content: "**We have read on their website: \"At Jobnet, you search on your terms, anonymously and smoothly.\" This intrigued us and we kindly asked for conducting an interview with Kalle Bodestig, CEO and Founder of the Jobnet platform.**\n\n![Kalle Bodestig](kalle-kontor-liten.jpg)\n_Kalle Bodestig, CEO and founder of Jobnet_\n\n**Tell us about Jobnet! What do you do?** \n\n\"Jobnet is a platform with the ambition to become the equivalent of Hemnet (e.g. Sweden’s largest real estate platform) in the job market; a place where job seekers can look after their next workplace without the need to create a profile. We believe in the idea that the job seeker chooses his next workplace and not the other way around. The companies on the platform have space for employer branding and can attract candidates by showcasing their companies. This results in better matching with candidates who understand better what the future workplace offers.\"\n\n**Which target groups do you address? **\n\n\"Our main focus is on the job seeker. If we succeed in attracting many job seekers to our platform, recruiters and companies will follow suit, as they want to be where the talents are.\"\n\n**Which JobTech APIs do you use on the Jobnet platform?**\n\n\"[Jobstream](https://www.jobtechdev.se/en/components/jobstream), [JobAd Enrichments](https://www.jobtechdev.se/en/components/jobad-enrichments), and [JobTech Taxonomy](https://www.jobtechdev.se/en/components/jobad-enrichments) are the ones we are currently utilizing.\"\n\n**Can you describe how you work and how the APIs have helped you in your business to achieve better results?\n**\n\n\"We utilize the APIs in several different ways. Our search structure and categorisation inherited the same structure of the data already read in; a workload that would otherwise have taken several hundred hours. Our latest application area features publicly accessible statistical sites. These sites showcase the fusion of diverse data sources, enabling us to provide valuable insights such as profession and competency forecasts. These interactive platforms have gained rapid popularity and serve as a testament to the power of the APIs in enabling us to create innovative solutions that would have otherwise been unattainable.\"\n\n**What is the next step for Jobnet, and where do you see yourselves in five years?**\n\n\"We are now opening up our platform to external recruitment agencies so that they can bring in their companies. It allows us to scale our business up. Our long-term goal is to become the largest destination for job seekers after Arbetsförmedlingen and LinkedIn. Given the time to advance further, we have been able to develop modern tools for the last couple of years. It will be now rewarding for us to allow more recruiters to use them!\"\n\n[Kindly,visit Jobnet (SE)](https://jobnet.se/)"
date: '2023-05-02 15:56'
taxonomy:
    category:
        - News
    type:
        - Interview
media_order: kalle-kontor-liten.jpg
---

