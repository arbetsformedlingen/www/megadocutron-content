---
title: '"Innovationsledning för grön och digital omställning" 16 nov'
custom:
  date: 2022-11-16 08:30
  endtime: 2022-11-16 17:00
  short: 'Konferensen är kostnadsfri och riktar sig till personer inom offentlig sektor, näringsliv och civilsamhälle som driver innovationsarbete eller är nyfiken på hur innovationsledning kan bidra till en grön och digital omställning. '
  content: "###### Snart är det dags för årets innovationsledningskonferens, i år med fokus på grön och digital omställning.\n\n\nAgendan består av innovationsledning i praktiken, framtidsspaning inom forskningen och workshops. Dessutom kommer Lisa Olsson från Helsingborg Stad som blev \"Innovationsledare 2021\" att dela med sig av sina erfarenheter och slutligen kommer \"Årets innovationsledare 2022” att koras. \n\nKonferensen är kostnadsfri och riktar sig till personer inom offentlig sektor, näringsliv och civilsamhälle som driver innovationsarbete eller är nyfiken på hur innovationsledning kan bidra till en grön och digital omställning. \n\nNär: Onsdagen den 16 november 8:30 – 17:00\n\nVar: Life city, Solnavägen 3 i Stockholm samt digitalt\n\n[läs mer och anmäl dig](https://event.trippus.net/Home/Index/AEAKgIMbGm0RNT-hrRS-WgB23PEaMRrOeubZ_gvs12FgCBa6jO0HKE95hWuF8cPv4u7jzxZP7rSn/AEAKgINNGvOGnEERJxdEXlTuHcLawCGc4eJIazx_WbMOM0nqGqxUDP7IWvHev4fpiJ1GJ7UAZisl/swe)\n"
  title: '"Innovationsledning för grön och digital omställning" 16 nov'
taxonomy:
  category:
  - Event
  status:
  - Avslutad
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022/events_blog_post.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022/events_blog_post.sv.md
tags:
- user::pages::03.news::09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022::events_blog_post.sv.md
- pages::03.news::09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022::events_blog_post.sv.md
- 03.news::09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022::events_blog_post.sv.md
- 09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: '"Innovationsledning för grön och digital omställning" 16 nov'
custom:
    date: '2022-11-16 08:30'
    endtime: '2022-11-16 17:00'
    short: 'Konferensen är kostnadsfri och riktar sig till personer inom offentlig sektor, näringsliv och civilsamhälle som driver innovationsarbete eller är nyfiken på hur innovationsledning kan bidra till en grön och digital omställning. '
    content: "###### Snart är det dags för årets innovationsledningskonferens, i år med fokus på grön och digital omställning.\n\n\nAgendan består av innovationsledning i praktiken, framtidsspaning inom forskningen och workshops. Dessutom kommer Lisa Olsson från Helsingborg Stad som blev \"Innovationsledare 2021\" att dela med sig av sina erfarenheter och slutligen kommer \"Årets innovationsledare 2022” att koras. \n\nKonferensen är kostnadsfri och riktar sig till personer inom offentlig sektor, näringsliv och civilsamhälle som driver innovationsarbete eller är nyfiken på hur innovationsledning kan bidra till en grön och digital omställning. \n\nNär: Onsdagen den 16 november 8:30 – 17:00\n\nVar: Life city, Solnavägen 3 i Stockholm samt digitalt\n\n[läs mer och anmäl dig](https://event.trippus.net/Home/Index/AEAKgIMbGm0RNT-hrRS-WgB23PEaMRrOeubZ_gvs12FgCBa6jO0HKE95hWuF8cPv4u7jzxZP7rSn/AEAKgINNGvOGnEERJxdEXlTuHcLawCGc4eJIazx_WbMOM0nqGqxUDP7IWvHev4fpiJ1GJ7UAZisl/swe)\n"
    title: '"Innovationsledning för grön och digital omställning" 16 nov'
taxonomy:
    category:
        - Event
    status:
        - Avslutad
---

