---
title: Innovation Management Conference
custom:
  date: 2022-11-16 08:30
  endtime: 2022-11-16 17:00
  short: The conference is free of charge and is targeted at professionals in both the public and the private sectors, the civil society, who are leading innovation projects or are interested in learning how innovation management can contribute to a green and digital transition.
  content: "###### It is time for the yearly Innovation Management Conference with focus on the green and digital transition. <BR><BR>\n\nInnovation management practices, what has been researched in the area and various workshops are part of this year's agenda. In addition, Lisa Olsson from Helsingborg Municipality, who has been awarded as the \"2021 Innovation Leader\" will be sharing her experiences on stage. The new \"2022 Innovation Leader\" will be crowned as well. \n\nThe conference is free of charge and is targeted at professionals in both the public and the private sectors, the civil society, who are leading innovation projects or are interested in learning how innovation management can contribute to a green and digital transition.\n\n**When:** Wednesday, November 16th, 2022, 8:30 – 17:00 <BR>\n**Where:** Hybrid. Digitally <BR>\n**Venue:** Life city, Solnavägen 3 in Stockholm\n\n[Learn more and register here](https://event.trippus.net/Home/Index/AEAKgIMbGm0RNT-hrRS-WgB23PEaMRrOeubZ_gvs12FgCBa6jO0HKE95hWuF8cPv4u7jzxZP7rSn/AEAKgINNGvOGnEERJxdEXlTuHcLawCGc4eJIazx_WbMOM0nqGqxUDP7IWvHev4fpiJ1GJ7UAZisl/swe). \n\n_The information is provided in Swedish._ \n"
  title: '2022 Innovation Management Conference with Focus on Green and Digital Transition '
taxonomy:
  category:
  - Event
  status:
  - Completed
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022/events_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022/events_blog_post.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022/events_blog_post.en.md
tags:
- user::pages::03.news::09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022::events_blog_post.en.md
- pages::03.news::09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022::events_blog_post.en.md
- 03.news::09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022::events_blog_post.en.md
- 09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022::events_blog_post.en.md
- events_blog_post.en.md
---
---
title: 'Innovation Management Conference'
custom:
    date: '2022-11-16 08:30'
    endtime: '2022-11-16 17:00'
    short: 'The conference is free of charge and is targeted at professionals in both the public and the private sectors, the civil society, who are leading innovation projects or are interested in learning how innovation management can contribute to a green and digital transition.'
    content: "###### It is time for the yearly Innovation Management Conference with focus on the green and digital transition. <BR><BR>\n\nInnovation management practices, what has been researched in the area and various workshops are part of this year's agenda. In addition, Lisa Olsson from Helsingborg Municipality, who has been awarded as the \"2021 Innovation Leader\" will be sharing her experiences on stage. The new \"2022 Innovation Leader\" will be crowned as well. \n\nThe conference is free of charge and is targeted at professionals in both the public and the private sectors, the civil society, who are leading innovation projects or are interested in learning how innovation management can contribute to a green and digital transition.\n\n**When:** Wednesday, November 16th, 2022, 8:30 – 17:00 <BR>\n**Where:** Hybrid. Digitally <BR>\n**Venue:** Life city, Solnavägen 3 in Stockholm\n\n[Learn more and register here](https://event.trippus.net/Home/Index/AEAKgIMbGm0RNT-hrRS-WgB23PEaMRrOeubZ_gvs12FgCBa6jO0HKE95hWuF8cPv4u7jzxZP7rSn/AEAKgINNGvOGnEERJxdEXlTuHcLawCGc4eJIazx_WbMOM0nqGqxUDP7IWvHev4fpiJ1GJ7UAZisl/swe). \n\n_The information is provided in Swedish._ \n"
    title: '2022 Innovation Management Conference with Focus on Green and Digital Transition '
taxonomy:
    category:
        - Event
    status:
        - Completed
---

