---
title: Three New Pilot Projects
custom:
  title: Three New Pilot Projects
  date: 2022-02-28 13:37
  short: 'JobTech Development is working on three major pilot projects in relation to skills supply and lifelong learning. '
  content: "JobTech Development is working on three major pilot projects in relation to skills supply and lifelong learning. The purpose is to enable digital transformation and to contribute to a more inclusive, skills-based and data-driven labour and education market.\n\nThe projects refer to: \n1. development of digital guidance through linked and enriched data\n2. coherent nomenclature in the field of education and labour market \n3. digital infrastructure that gives the individual control over his own data \n\nDoes it sound interesting? You can learn more about our projects below: \n\n* [Digital guidance and linked data](https://jobtechdev.se/en/components/digital-vaegledning-och-laenkad-data) \n* [Concept recognition and nomenclature](https://jobtechdev.se/en/components/taxonomi-och-begreppsstruktur)\n* [Personal data and data portability ](https://jobtechdev.se/en/components/individdata-och-dataportabilitet)\n"
date: 2022-02-28 13:39
taxonomy:
  category:
  - News
  type:
  - Research
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/32.tre-pilotprojekt/news_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/32.tre-pilotprojekt/news_blog_post.en.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/32.tre-pilotprojekt/news_blog_post.en.md
tags:
- user::pages::03.news::32.tre-pilotprojekt::news_blog_post.en.md
- pages::03.news::32.tre-pilotprojekt::news_blog_post.en.md
- 03.news::32.tre-pilotprojekt::news_blog_post.en.md
- 32.tre-pilotprojekt::news_blog_post.en.md
- news_blog_post.en.md
---
---
title: 'Three New Pilot Projects'
custom:
    title: 'Three New Pilot Projects'
    date: '2022-02-28 13:37'
    short: 'JobTech Development is working on three major pilot projects in relation to skills supply and lifelong learning. '
    content: "JobTech Development is working on three major pilot projects in relation to skills supply and lifelong learning. The purpose is to enable digital transformation and to contribute to a more inclusive, skills-based and data-driven labour and education market.\n\nThe projects refer to: \n1. development of digital guidance through linked and enriched data\n2. coherent nomenclature in the field of education and labour market \n3. digital infrastructure that gives the individual control over his own data \n\nDoes it sound interesting? You can learn more about our projects below: \n\n* [Digital guidance and linked data](https://jobtechdev.se/en/components/digital-vaegledning-och-laenkad-data) \n* [Concept recognition and nomenclature](https://jobtechdev.se/en/components/taxonomi-och-begreppsstruktur)\n* [Personal data and data portability ](https://jobtechdev.se/en/components/individdata-och-dataportabilitet)\n"
date: '2022-02-28 13:39'
taxonomy:
    category:
        - News
    type:
        - Research
---

