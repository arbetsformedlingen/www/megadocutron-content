---
title: foss-north 23-25 april
custom:
  title: Foss-north 23-25 april
  date: 2023-04-23 09:00
  endtime: 2023-04-25 16:00
  short: Foss-north är en kostnadsfri, öppen källkodskonferens med både hård- och mjukvarufokus  med destination Göteborg denna gång. JobTech Developments tekniskt ansvarige Jonas Södergren kommer presentera i ett av föredragen.
  content: "Foss-north är en kostnadsfri, öppen källkodskonferens med både hård- och mjukvarufokus. Konferensen äger rum i Göteborg, där vi erbjuder en mötesplats för den nordiska foss-gemenskapen och kombinerar intressanta talare med en entusiastisk publik.\n\nJobTech Developments tekniskt ansvarige Jonas Södergren kommer presentera i ett av föredragen med titeln: **\"Arbetsförmedlingens erfarenheter av jobtech och öppen källkod: att dela 400 källkodsrepon och exempel på nya samarbeten\".**\n\n\nPresentationen innehåller framgångar, utmaningar och samarbeten med andra organisationer inom JobTech och öppen källkod communities. \nMålet är att publiken ska få en bättre förståelse för hur öppen källkod kan användas mer effektivt inom offentlig sektor. \n\nKonferensen kommer att äga rum den 24-25 april på Chalmers konferenscenter i Göteborg.\nDagarna inleds med \"Community-dagen\" den 23 april på samma plats.\n\n[För mer information och anmälan (eng)](https://foss-north.se/2023/index.html)\n\n\n\n"
date: 2023-04-12 13:25
taxonomy:
  category:
  - Event
  status:
  - Avslutad
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/05.foss-north-2023/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/05.foss-north-2023/events_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/05.foss-north-2023/events_blog_post.sv.md
tags:
- user::pages::03.news::05.foss-north-2023::events_blog_post.sv.md
- pages::03.news::05.foss-north-2023::events_blog_post.sv.md
- 03.news::05.foss-north-2023::events_blog_post.sv.md
- 05.foss-north-2023::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'foss-north 23-25 april'
custom:
    title: 'Foss-north 23-25 april'
    date: '2023-04-23 09:00'
    endtime: '2023-04-25 16:00'
    short: 'Foss-north är en kostnadsfri, öppen källkodskonferens med både hård- och mjukvarufokus  med destination Göteborg denna gång. JobTech Developments tekniskt ansvarige Jonas Södergren kommer presentera i ett av föredragen.'
    content: "Foss-north är en kostnadsfri, öppen källkodskonferens med både hård- och mjukvarufokus. Konferensen äger rum i Göteborg, där vi erbjuder en mötesplats för den nordiska foss-gemenskapen och kombinerar intressanta talare med en entusiastisk publik.\n\nJobTech Developments tekniskt ansvarige Jonas Södergren kommer presentera i ett av föredragen med titeln: **\"Arbetsförmedlingens erfarenheter av jobtech och öppen källkod: att dela 400 källkodsrepon och exempel på nya samarbeten\".**\n\n\nPresentationen innehåller framgångar, utmaningar och samarbeten med andra organisationer inom JobTech och öppen källkod communities. \nMålet är att publiken ska få en bättre förståelse för hur öppen källkod kan användas mer effektivt inom offentlig sektor. \n\nKonferensen kommer att äga rum den 24-25 april på Chalmers konferenscenter i Göteborg.\nDagarna inleds med \"Community-dagen\" den 23 april på samma plats.\n\n[För mer information och anmälan (eng)](https://foss-north.se/2023/index.html)\n\n\n\n"
date: '2023-04-12 13:25'
taxonomy:
    category:
        - Event
    status:
        - Avslutad
---

