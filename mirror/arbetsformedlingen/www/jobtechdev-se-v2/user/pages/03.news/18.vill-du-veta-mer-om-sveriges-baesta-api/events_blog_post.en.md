---
title: Sweden's Best API
custom:
  title: Eager To Know More About Sweden's Best API?
  date: 2021-12-03 13:30
  endtime: 2021-12-03 14:00
  short: We present the API JobStream, which won recently Sweden API Award 2021.
  register: ' https://jitsi.jobtechdev.se/jobstream'
  content: "JobTech Developments Johan Brymér Dahlhielm will be talking and sharing more about our [API JobStream](https://jobtechdev.se/en/components/jobstream). JobStream has recently won the Sweden API Award 2021. \n\nJobStream can be useful for companies and organizations that have their own search engine and are eager to improve their service with real-time updated job ads data. The API can also be used to build and train algorithms in relation to machine learning in order to identify and analyze trends on the labour market, based on job ads data.\n\n[Welcome to an open digital demo of the Job Stream API](https://jitsi.jobtechdev.se/jobstream)"
taxonomy:
  category:
  - Event
  status:
  - Completed
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/18.vill-du-veta-mer-om-sveriges-baesta-api/events_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/18.vill-du-veta-mer-om-sveriges-baesta-api/events_blog_post.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/18.vill-du-veta-mer-om-sveriges-baesta-api/events_blog_post.en.md
tags:
- user::pages::03.news::18.vill-du-veta-mer-om-sveriges-baesta-api::events_blog_post.en.md
- pages::03.news::18.vill-du-veta-mer-om-sveriges-baesta-api::events_blog_post.en.md
- 03.news::18.vill-du-veta-mer-om-sveriges-baesta-api::events_blog_post.en.md
- 18.vill-du-veta-mer-om-sveriges-baesta-api::events_blog_post.en.md
- events_blog_post.en.md
---
---
title: 'Sweden''s Best API'
custom:
    title: 'Eager To Know More About Sweden''s Best API?'
    date: '2021-12-03 13:30'
    endtime: '2021-12-03 14:00'
    short: 'We present the API JobStream, which won recently Sweden API Award 2021.'
    register: ' https://jitsi.jobtechdev.se/jobstream'
    content: "JobTech Developments Johan Brymér Dahlhielm will be talking and sharing more about our [API JobStream](https://jobtechdev.se/en/components/jobstream). JobStream has recently won the Sweden API Award 2021. \n\nJobStream can be useful for companies and organizations that have their own search engine and are eager to improve their service with real-time updated job ads data. The API can also be used to build and train algorithms in relation to machine learning in order to identify and analyze trends on the labour market, based on job ads data.\n\n[Welcome to an open digital demo of the Job Stream API](https://jitsi.jobtechdev.se/jobstream)"
taxonomy:
    category:
        - Event
    status:
        - Completed
---

