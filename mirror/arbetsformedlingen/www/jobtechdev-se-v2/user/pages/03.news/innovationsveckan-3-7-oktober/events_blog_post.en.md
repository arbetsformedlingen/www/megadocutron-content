---
title: 'Innovation Week, October 3rd-7th '
custom:
  date: 2022-10-03 09:00
  endtime: 2022-10-07 16:30
  short: Do not miss to sign up for the forthcoming Innovation Week, organised by the Agency for Digital Government (Digg), Sweden's Innovation Agency (Vinnova) and the Swedish Association of Local Authorities and Regions (SALAR) among others.
  content: "###### Do not miss to sign up for the forthcoming Innovation Week, which is taking place on October 3rd – 7th, 2022, organised by the Agency for Digital Government (Digg), Sweden's Innovation Agency (Vinnova) and the Swedish Association of Local Authorities and Regions (SALAR) among others. <BR><BR>\n\nThe program is designed by actors all over Sweden. Broad perspectives are outlined in various presentations, lectures, and seminars, most of which are digital. \n\nThe Innovation Week is dedicated to innovation by and for the public sector. The focus during this week is on sharing and spreading smart solutions between different industries and actors. \n    \nA taster seminar is: [\"This is how Ena and the building blocks My cases and My representatives can create concrete benefit for municipalities and their residents\"](https://innovationsveckan.nu/program/sa-kan-ena-och-byggblocken-mina-arenden-och-mina-ombud-skapa-konkret-nytta-for-kommuner-och-dess-invanare/). \n\nWarm welcome to sign up and participate. \n    \nPlease, bear in mind, that most of the seminars are held in Swedish. \n\n[To sign up ](https://innovationsveckan.nu/program/?tribe_paged=1&tribe_event_display=list)\n\n "
  title: 'Innovation Week, October 3rd-7th '
taxonomy:
  category:
  - Event
  status:
  - Completed
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/innovationsveckan-3-7-oktober/events_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/innovationsveckan-3-7-oktober/events_blog_post.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/innovationsveckan-3-7-oktober/events_blog_post.en.md
tags:
- user::pages::03.news::innovationsveckan-3-7-oktober::events_blog_post.en.md
- pages::03.news::innovationsveckan-3-7-oktober::events_blog_post.en.md
- 03.news::innovationsveckan-3-7-oktober::events_blog_post.en.md
- innovationsveckan-3-7-oktober::events_blog_post.en.md
- events_blog_post.en.md
---
---
title: 'Innovation Week, October 3rd-7th '
custom:
    date: '2022-10-03 09:00'
    endtime: '2022-10-07 16:30'
    short: 'Do not miss to sign up for the forthcoming Innovation Week, organised by the Agency for Digital Government (Digg), Sweden''s Innovation Agency (Vinnova) and the Swedish Association of Local Authorities and Regions (SALAR) among others.'
    content: "###### Do not miss to sign up for the forthcoming Innovation Week, which is taking place on October 3rd – 7th, 2022, organised by the Agency for Digital Government (Digg), Sweden's Innovation Agency (Vinnova) and the Swedish Association of Local Authorities and Regions (SALAR) among others. <BR><BR>\n\nThe program is designed by actors all over Sweden. Broad perspectives are outlined in various presentations, lectures, and seminars, most of which are digital. \n\nThe Innovation Week is dedicated to innovation by and for the public sector. The focus during this week is on sharing and spreading smart solutions between different industries and actors. \n    \nA taster seminar is: [\"This is how Ena and the building blocks My cases and My representatives can create concrete benefit for municipalities and their residents\"](https://innovationsveckan.nu/program/sa-kan-ena-och-byggblocken-mina-arenden-och-mina-ombud-skapa-konkret-nytta-for-kommuner-och-dess-invanare/). \n\nWarm welcome to sign up and participate. \n    \nPlease, bear in mind, that most of the seminars are held in Swedish. \n\n[To sign up ](https://innovationsveckan.nu/program/?tribe_paged=1&tribe_event_display=list)\n\n "
    title: 'Innovation Week, October 3rd-7th '
taxonomy:
    category:
        - Event
    status:
        - Completed
---

