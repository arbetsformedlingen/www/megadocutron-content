---
title: Sveriges bästa API
custom:
  title: Vill du veta mer om Sveriges bästa API?
  date: 2021-12-03 13:30
  endtime: 2021-12-03 14:00
  short: 'Vi presenterar JobStream, som nyligen utsågs till vinnare i Sweden API Award 2021. '
  description: 'JobTech Developments Johan Brymér Dahlhielm presenterar vårt API JobStream, som nyligen utsågs till vinnare i Sweden API Award 2021. JobStream är användbart för alla som vill förbättra sin tjänst med annonsdata i realtid. API:et kan också användas för att träna algoritmer med maskininlärning, eller för att identifiera och analysera trender på arbetsmarknaden.  Välkommen till en öppen digital demo här: https://jitsi.jobtechdev.se/jobstream'
  register: ' https://jitsi.jobtechdev.se/jobstream'
  content: "JobTech Developments Johan Brymér Dahlhielm presenterar vårt [API JobStream](https://jobtechdev.se/sv/komponenter/jobstream), som nyligen utsågs till vinnare i Sweden API Award 2021. \n\nJobStream är användbart för alla företag och organisationer som har en egen sökmotor och vill förbättra sin tjänst med realtidsuppdaterad annonsdata. API:et kan också användas för att bygga och träna algoritmer i samband med maskininlärning eller för att identifiera och analysera trender på arbetsmarknaden utifrån annonsdata.\n\n[Välkommen till en öppen digital demo av Job Stream API](https://jitsi.jobtechdev.se/jobstream)"
taxonomy:
  category:
  - Event
  status:
  - Avslutad
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/26.vill-du-veta-mer-om-sveriges-baesta-api/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/26.vill-du-veta-mer-om-sveriges-baesta-api/events_blog_post.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/26.vill-du-veta-mer-om-sveriges-baesta-api/events_blog_post.sv.md
tags:
- user::pages::03.news::26.vill-du-veta-mer-om-sveriges-baesta-api::events_blog_post.sv.md
- pages::03.news::26.vill-du-veta-mer-om-sveriges-baesta-api::events_blog_post.sv.md
- 03.news::26.vill-du-veta-mer-om-sveriges-baesta-api::events_blog_post.sv.md
- 26.vill-du-veta-mer-om-sveriges-baesta-api::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'Sveriges bästa API'
custom:
    title: 'Vill du veta mer om Sveriges bästa API?'
    date: '2021-12-03 13:30'
    endtime: '2021-12-03 14:00'
    short: 'Vi presenterar JobStream, som nyligen utsågs till vinnare i Sweden API Award 2021. '
    description: 'JobTech Developments Johan Brymér Dahlhielm presenterar vårt API JobStream, som nyligen utsågs till vinnare i Sweden API Award 2021. JobStream är användbart för alla som vill förbättra sin tjänst med annonsdata i realtid. API:et kan också användas för att träna algoritmer med maskininlärning, eller för att identifiera och analysera trender på arbetsmarknaden.  Välkommen till en öppen digital demo här: https://jitsi.jobtechdev.se/jobstream'
    register: ' https://jitsi.jobtechdev.se/jobstream'
    content: "JobTech Developments Johan Brymér Dahlhielm presenterar vårt [API JobStream](https://jobtechdev.se/sv/komponenter/jobstream), som nyligen utsågs till vinnare i Sweden API Award 2021. \n\nJobStream är användbart för alla företag och organisationer som har en egen sökmotor och vill förbättra sin tjänst med realtidsuppdaterad annonsdata. API:et kan också användas för att bygga och träna algoritmer i samband med maskininlärning eller för att identifiera och analysera trender på arbetsmarknaden utifrån annonsdata.\n\n[Välkommen till en öppen digital demo av Job Stream API](https://jitsi.jobtechdev.se/jobstream)"
taxonomy:
    category:
        - Event
    status:
        - Avslutad
---

