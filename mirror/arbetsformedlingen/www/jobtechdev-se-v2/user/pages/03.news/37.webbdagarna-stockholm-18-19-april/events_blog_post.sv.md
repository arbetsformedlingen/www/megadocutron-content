---
title: Webbdagarna Stockholm 18-19 april
custom:
  title: Webbdagarna Stockholm 18-19 april
  date: 2023-04-18 09:00
  endtime: 2023-04-19 16:00
  short: Webbdagarna 2023 är ett live event med fokus på hur man lyckas på digitala plattförmar.
  content: |-
    För att lyckas på digitala plattformar i dag behövs:

    • Transparens
    • Trovärdighet
    • Tillit
    • Flexibilitet

    Och det handlar Webbdagarna 18 – 19 april 2023 om.

    Du som jobbar med digitala plattformar står inför stora utmaningar just nu. Marknaden är osäker, kunderna ställer höga krav på såväl tjänster som upplevelse, hackare är ute efter dina data och digitala kanaler är fulla av desinformation.

    Samtidigt är möjligheterna fantastiska! Vi har aldrig varit så digitala som nu och vi vill använda högkvalitativa, digitala tjänster.

    [Anmälan och mer info: ](https://webbdagarna.se/event/stockholm-2023/)
date: 2023-01-16 11:04
taxonomy:
  category:
  - Event
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/37.webbdagarna-stockholm-18-19-april/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/37.webbdagarna-stockholm-18-19-april/events_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/37.webbdagarna-stockholm-18-19-april/events_blog_post.sv.md
tags:
- user::pages::03.news::37.webbdagarna-stockholm-18-19-april::events_blog_post.sv.md
- pages::03.news::37.webbdagarna-stockholm-18-19-april::events_blog_post.sv.md
- 03.news::37.webbdagarna-stockholm-18-19-april::events_blog_post.sv.md
- 37.webbdagarna-stockholm-18-19-april::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'Webbdagarna Stockholm 18-19 april'
custom:
    title: 'Webbdagarna Stockholm 18-19 april'
    date: '2023-04-18 09:00'
    endtime: '2023-04-19 16:00'
    short: 'Webbdagarna 2023 är ett live event med fokus på hur man lyckas på digitala plattförmar.'
    content: "För att lyckas på digitala plattformar i dag behövs:\n\n• Transparens\n• Trovärdighet\n• Tillit\n• Flexibilitet\n\nOch det handlar Webbdagarna 18 – 19 april 2023 om.\n\nDu som jobbar med digitala plattformar står inför stora utmaningar just nu. Marknaden är osäker, kunderna ställer höga krav på såväl tjänster som upplevelse, hackare är ute efter dina data och digitala kanaler är fulla av desinformation.\n\nSamtidigt är möjligheterna fantastiska! Vi har aldrig varit så digitala som nu och vi vill använda högkvalitativa, digitala tjänster.\n\n[Anmälan och mer info: ](https://webbdagarna.se/event/stockholm-2023/)"
date: '2023-01-16 11:04'
taxonomy:
    category:
        - Event
---

