---
title: Open Space workshop del 2, den 16-17 mars
published: true
custom:
  title: Open Space workshop del 2, den 16-17 mars
  date: 2022-03-16 10:30
  endtime: 2022-03-17 15:00
  short: ' JobTech tillsammans med Sunet fortsätter serien workshops i formatet "Open Space" om digitalisering, kompetensförsörjning och livslångt lärande.'
  content: "###### Välkommen till Open Space nummer 2 om digitalisering, kompetensförsörjning och livslångt lärande\n\nEventet kommer att äga rum den 16-17 mars i Internetstiftelsens lokaler, Hammarby Kaj 10D, Stockholm (dvs fysiskt på plats). \n\nVi välkomnar alla inom såväl offentlig som privat sektor att delta och bidra med insikter, förslag och tankar.\n\n[Här hittar du information om eventet och anmälan. ](https://www.eventbrite.se/e/open-space-2-digitalisering-kompetensforsorjning-och-livslangt-larande-registrering-268852453927)\n\nVi har endast 75 platser, så skynda - det är först till kvarn. Sista dagen för anmälan är måndag den 7 mars.\n\n"
taxonomy:
  category:
  - Event
  status:
  - Avslutad
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/open-space-workshop-16-17-mars/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/open-space-workshop-16-17-mars/events_blog_post.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/open-space-workshop-16-17-mars/events_blog_post.sv.md
tags:
- user::pages::03.news::open-space-workshop-16-17-mars::events_blog_post.sv.md
- pages::03.news::open-space-workshop-16-17-mars::events_blog_post.sv.md
- 03.news::open-space-workshop-16-17-mars::events_blog_post.sv.md
- open-space-workshop-16-17-mars::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'Open Space workshop del 2, den 16-17 mars'
published: true
custom:
    title: 'Open Space workshop del 2, den 16-17 mars'
    date: '2022-03-16 10:30'
    endtime: '2022-03-17 15:00'
    short: ' JobTech tillsammans med Sunet fortsätter serien workshops i formatet "Open Space" om digitalisering, kompetensförsörjning och livslångt lärande.'
    content: "###### Välkommen till Open Space nummer 2 om digitalisering, kompetensförsörjning och livslångt lärande\n\nEventet kommer att äga rum den 16-17 mars i Internetstiftelsens lokaler, Hammarby Kaj 10D, Stockholm (dvs fysiskt på plats). \n\nVi välkomnar alla inom såväl offentlig som privat sektor att delta och bidra med insikter, förslag och tankar.\n\n[Här hittar du information om eventet och anmälan. ](https://www.eventbrite.se/e/open-space-2-digitalisering-kompetensforsorjning-och-livslangt-larande-registrering-268852453927)\n\nVi har endast 75 platser, så skynda - det är först till kvarn. Sista dagen för anmälan är måndag den 7 mars.\n\n"
taxonomy:
    category:
        - Event
    status:
        - Avslutad
---

