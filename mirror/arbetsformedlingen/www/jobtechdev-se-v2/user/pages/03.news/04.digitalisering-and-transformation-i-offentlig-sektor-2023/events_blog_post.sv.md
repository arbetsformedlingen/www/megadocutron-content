---
title: Digitalisering & transformation i offentlig sektor 2023
custom:
  title: Digitalisering & transformation i offentlig sektor 2023
  date: 2023-09-20 08:30
  endtime: 2023-09-20 16:55
  short: 'Välkommen till årets viktigaste konferens om digitalisering & transformation i offentlig sektor kopplat till digitala strategier, organisationsutveckling och förändringsledning. JobTech Developments  Jonas Södergren och Maria Dalhage  är inbjudna att prata om "Kundfokuserad digitalisering av Arbetsförmedlingen – för en bättre kundupplevelse och ökad effektivitet". '
  content: "Välkommen till årets viktigaste konferens om digitalisering & transformation i offentlig sektor kopplat till digitala strategier, organisationsutveckling och förändringsledning. \n\nPå konferensen får du ta del av lyckade digitala strategier och förändringsprocesser för att flytta transformationen framåt.\n\n\nI ett av praktikfallen kommer JobTech Developments Jonas Södergren och Maria Dalhage att prata om:\n\n\"Kundfokuserad digitalisering av Arbetsförmedlingen – för en bättre kundupplevelse och ökad effektivitet\". \n\nÄmnen som kommer diskuteras:\n\n* Så kan nya processer och arbetssätt möta det ökade behovet av tjänster och förbättra kundupplevelsen\n* Hur kan smart teknik frigöra tid för att effektivisera tjänsterna till arbetssökande och arbetsgivare?\n* Hur delning av kunskap ökar takten i den digitala transformationen av offentlig sektor\n\n[För mer info och anmälan](https://www.abilitypartner.se/konferenser_kurser/digitalisering-och-transformation-i-offentlig-sektor/\n)"
  minititle: 20 september
taxonomy:
  category:
  - Event
sitemap:
  ignore: false
date: 2023-05-09 09:48
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/04.digitalisering-and-transformation-i-offentlig-sektor-2023/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/04.digitalisering-and-transformation-i-offentlig-sektor-2023/events_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/04.digitalisering-and-transformation-i-offentlig-sektor-2023/events_blog_post.sv.md
tags:
- user::pages::03.news::04.digitalisering-and-transformation-i-offentlig-sektor-2023::events_blog_post.sv.md
- pages::03.news::04.digitalisering-and-transformation-i-offentlig-sektor-2023::events_blog_post.sv.md
- 03.news::04.digitalisering-and-transformation-i-offentlig-sektor-2023::events_blog_post.sv.md
- 04.digitalisering-and-transformation-i-offentlig-sektor-2023::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'Digitalisering & transformation i offentlig sektor 2023'
custom:
    title: 'Digitalisering & transformation i offentlig sektor 2023'
    date: '2023-09-20 08:30'
    endtime: '2023-09-20 16:55'
    short: 'Välkommen till årets viktigaste konferens om digitalisering & transformation i offentlig sektor kopplat till digitala strategier, organisationsutveckling och förändringsledning. JobTech Developments  Jonas Södergren och Maria Dalhage  är inbjudna att prata om "Kundfokuserad digitalisering av Arbetsförmedlingen – för en bättre kundupplevelse och ökad effektivitet". '
    content: "Välkommen till årets viktigaste konferens om digitalisering & transformation i offentlig sektor kopplat till digitala strategier, organisationsutveckling och förändringsledning. \n\nPå konferensen får du ta del av lyckade digitala strategier och förändringsprocesser för att flytta transformationen framåt.\n\n\nI ett av praktikfallen kommer JobTech Developments Jonas Södergren och Maria Dalhage att prata om:\n\n\"Kundfokuserad digitalisering av Arbetsförmedlingen – för en bättre kundupplevelse och ökad effektivitet\". \n\nÄmnen som kommer diskuteras:\n\n* Så kan nya processer och arbetssätt möta det ökade behovet av tjänster och förbättra kundupplevelsen\n* Hur kan smart teknik frigöra tid för att effektivisera tjänsterna till arbetssökande och arbetsgivare?\n* Hur delning av kunskap ökar takten i den digitala transformationen av offentlig sektor\n\n[För mer info och anmälan](https://www.abilitypartner.se/konferenser_kurser/digitalisering-och-transformation-i-offentlig-sektor/\n)"
    minititle: '20 september'
taxonomy:
    category:
        - Event
sitemap:
    ignore: false
date: '2023-05-09 09:48'
---

