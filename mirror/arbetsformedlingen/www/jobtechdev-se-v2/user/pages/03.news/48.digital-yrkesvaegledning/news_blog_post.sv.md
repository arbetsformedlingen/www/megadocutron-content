---
title: digital yrkesvägledning
custom:
  title: 'Digital yrkesvägledning: nytt utvecklingsprojekt'
  short: Jobtech Development leder ett nytt spännande utvecklingsprojekt inom JobEdTech-området med utgångspunkt i att använda berikad CV-information för att bedöma vilka utbildningar som kan fylla glappet mellan en individs erfarenheter och arbetsmarknadens behov.
  content: |-
    **Jobtech Development leder ett nytt spännande utvecklingsprojekt inom JobEdTech-området med utgångspunkt i att använda berikad CV-information för att bedöma vilka utbildningar som kan fylla glappet mellan en individs erfarenheter och arbetsmarknadens behov.**

    En yrkesomställning mitt i livet kan vara en tuff utmaning för många människor. Ändå är det just vad arbetsmarknaden kommer att kräva av allt fler i framtiden. Redan nu ser vi att det livslånga lärandet är nödvändigt för att klara kompetensförsörjningen i Sverige.

    Syftet med projektet "Digital yrkesvägledning" är att digitalt vägleda individen till att hitta den kortaste vägen till jobb eller karriärväxling genom kompetensutveckling.

    Projektet är ett samarbete mellan Arbetsförmedlingen, RISE Research Institutes of Sweden, Myndigheten för yrkeshögskolan och Skolverket.

    [Läs mer om projektet. ](https://jobtechdev.se/sv/komponenter/digital-yrkesvaegledning)
date: 2023-03-30 14:58
taxonomy:
  category:
  - Nyhet
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/48.digital-yrkesvaegledning/news_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/48.digital-yrkesvaegledning/news_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/48.digital-yrkesvaegledning/news_blog_post.sv.md
tags:
- user::pages::03.news::48.digital-yrkesvaegledning::news_blog_post.sv.md
- pages::03.news::48.digital-yrkesvaegledning::news_blog_post.sv.md
- 03.news::48.digital-yrkesvaegledning::news_blog_post.sv.md
- 48.digital-yrkesvaegledning::news_blog_post.sv.md
- news_blog_post.sv.md
---
---
title: 'digital yrkesvägledning'
custom:
    title: 'Digital yrkesvägledning: nytt utvecklingsprojekt'
    short: 'Jobtech Development leder ett nytt spännande utvecklingsprojekt inom JobEdTech-området med utgångspunkt i att använda berikad CV-information för att bedöma vilka utbildningar som kan fylla glappet mellan en individs erfarenheter och arbetsmarknadens behov.'
    content: "**Jobtech Development leder ett nytt spännande utvecklingsprojekt inom JobEdTech-området med utgångspunkt i att använda berikad CV-information för att bedöma vilka utbildningar som kan fylla glappet mellan en individs erfarenheter och arbetsmarknadens behov.**\n\nEn yrkesomställning mitt i livet kan vara en tuff utmaning för många människor. Ändå är det just vad arbetsmarknaden kommer att kräva av allt fler i framtiden. Redan nu ser vi att det livslånga lärandet är nödvändigt för att klara kompetensförsörjningen i Sverige.\n\nSyftet med projektet \"Digital yrkesvägledning\" är att digitalt vägleda individen till att hitta den kortaste vägen till jobb eller karriärväxling genom kompetensutveckling.\n\nProjektet är ett samarbete mellan Arbetsförmedlingen, RISE Research Institutes of Sweden, Myndigheten för yrkeshögskolan och Skolverket.\n\n[Läs mer om projektet. ](https://jobtechdev.se/sv/komponenter/digital-yrkesvaegledning)"
date: '2023-03-30 14:58'
taxonomy:
    category:
        - Nyhet
---

