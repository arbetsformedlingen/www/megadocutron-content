---
title: Data Spaces in an Interoperable Europe.Conference
custom:
  title: JobTech Development at SEMIC 2022
  date: 2022-12-06 08:30
  endtime: 2022-12-06 18:30
  short: 'The EU Commission has extended an invitation to JobTech Development to participate in Brussels at SEMIC 2022 "Data Spaces in an Interoperable Europe". JobTech Development will be presenting a use case in the session "Personal data space implementations – interoperability challenges". '
  content: "###### The EU Commission has extended an invitation to JobTech Development to participate at SEMIC 2022, which is dedicated this year to \"Data Spaces in an Interoperable Europe\". The conference is taking place on December 6th, 2022, on site in Brussels and is organised by the Interoperability Unit of DIGIT of the European Commission, in collaboration with the Czech Presidency of the Council of the EU.<BR><BR>\n    \nJobTech Development will be presenting a use case in the session _\"Personal data space implementations – interoperability challenges\"_. The idea is together with the other participants **Lahteenoja Viivi**, PhD researcher, practical philosophy, Faculty of Social Sciences, University of Helsinki and **De Loof Esther**, Operational Director, SolidLab to trigger a discussion around the use case and general interoperability challenges.\n\nThe central focus of SEMIC2022 revolves around the implementation of data spaces, bringing together concrete use cases from both the public and the private sectors. The goal is that during the conference, it will be offered support in overcoming obstacles in implementing data spaces and will be clarified how interoperability within and across sectors can be achieved through for example: \n\n* Success stories\n* Use cases\n* Knowledge sharing\n* Academic angel\n* Networking \n\n[Learn more about the event.](https://semic2022.eu/)\n\nIf you would like to know more, feel free to contact our colleague, [Marcello Gritta](mailto:marcello.grita@arbetsformedlingen.se), Project Lead.  \n\n\n"
taxonomy:
  category:
  - Event
  status:
  - Completed
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/data-spaces-in-an-interoperable-europe-6-dec-i-bryssel/events_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/data-spaces-in-an-interoperable-europe-6-dec-i-bryssel/events_blog_post.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/data-spaces-in-an-interoperable-europe-6-dec-i-bryssel/events_blog_post.en.md
tags:
- user::pages::03.news::data-spaces-in-an-interoperable-europe-6-dec-i-bryssel::events_blog_post.en.md
- pages::03.news::data-spaces-in-an-interoperable-europe-6-dec-i-bryssel::events_blog_post.en.md
- 03.news::data-spaces-in-an-interoperable-europe-6-dec-i-bryssel::events_blog_post.en.md
- data-spaces-in-an-interoperable-europe-6-dec-i-bryssel::events_blog_post.en.md
- events_blog_post.en.md
---
---
title: 'Data Spaces in an Interoperable Europe.Conference'
custom:
    title: 'JobTech Development at SEMIC 2022'
    date: '2022-12-06 08:30'
    endtime: '2022-12-06 18:30'
    short: 'The EU Commission has extended an invitation to JobTech Development to participate in Brussels at SEMIC 2022 "Data Spaces in an Interoperable Europe". JobTech Development will be presenting a use case in the session "Personal data space implementations – interoperability challenges". '
    content: "###### The EU Commission has extended an invitation to JobTech Development to participate at SEMIC 2022, which is dedicated this year to \"Data Spaces in an Interoperable Europe\". The conference is taking place on December 6th, 2022, on site in Brussels and is organised by the Interoperability Unit of DIGIT of the European Commission, in collaboration with the Czech Presidency of the Council of the EU.<BR><BR>\n    \nJobTech Development will be presenting a use case in the session _\"Personal data space implementations – interoperability challenges\"_. The idea is together with the other participants **Lahteenoja Viivi**, PhD researcher, practical philosophy, Faculty of Social Sciences, University of Helsinki and **De Loof Esther**, Operational Director, SolidLab to trigger a discussion around the use case and general interoperability challenges.\n\nThe central focus of SEMIC2022 revolves around the implementation of data spaces, bringing together concrete use cases from both the public and the private sectors. The goal is that during the conference, it will be offered support in overcoming obstacles in implementing data spaces and will be clarified how interoperability within and across sectors can be achieved through for example: \n\n* Success stories\n* Use cases\n* Knowledge sharing\n* Academic angel\n* Networking \n\n[Learn more about the event.](https://semic2022.eu/)\n\nIf you would like to know more, feel free to contact our colleague, [Marcello Gritta](mailto:marcello.grita@arbetsformedlingen.se), Project Lead.  \n\n\n"
taxonomy:
    category:
        - Event
    status:
        - Completed
---

