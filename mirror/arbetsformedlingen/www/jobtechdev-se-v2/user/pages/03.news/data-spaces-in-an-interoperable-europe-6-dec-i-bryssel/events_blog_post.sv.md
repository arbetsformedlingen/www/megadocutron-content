---
title: EU kommissionen har bjudit in JobTech Development att presentera på konferensen “Data Spaces in an Interoperable Europe.” 6 dec i Bryssel
custom:
  title: EU kommissionen har bjudit in JobTech Development att presentera på konferensen “Data Spaces in an Interoperable Europe.” 6 dec i Bryssel
  date: 2022-12-06 08:30
  endtime: 2022-12-06 18:30
  short: EU kommissionen har bjudit in JobTech Development och teamet som jobbar med interoperabilitet på SEMICs konferens i Bryssel för en presentation och att delta i en paneldiskussion med temat "Personal data space implementations – interoperability challenges"
  content: "###### EU kommissionen har bjudit in JobTech Development att presentera och delta i paneldiskussion på konferensen “Data Spaces in an Interoperable Europe.”i Bryssel den 6 december. \n\n\n\"SEMIC2022\" kommer att erbjuda stöd för att övervinna hinder i genomförandet av datarum och klargöra hur interoperabilitet inom och mellan sektorer kan uppnås.\n\nI temaspåret där JobTech ingår är ämnet: \n\"Personal data space implementations – interoperability challenges\"\n\nFöljande personer kommer att delta i paneldiskussionen:\n•\tLahteenoja Viivi, PhD researcher, practical philosophy, Faculty of Social Sciences, University of Helsinki\n•\tDe Loof Esther, Operational Director, SolidLab\n•\tMarcello Grita, Project Leader, JobTech\n\n[läs mer om eventet](https://semic2022.eu/)\n\nvill du veta mer, kontakta  [Marcello Grita](mailto:marcello.grita@arbetsformedlingen.se), Projektledare. \n\n"
taxonomy:
  category:
  - Event
  status:
  - Avslutad
date: 2022-11-27 10:49
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/data-spaces-in-an-interoperable-europe-6-dec-i-bryssel/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/data-spaces-in-an-interoperable-europe-6-dec-i-bryssel/events_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/data-spaces-in-an-interoperable-europe-6-dec-i-bryssel/events_blog_post.sv.md
tags:
- user::pages::03.news::data-spaces-in-an-interoperable-europe-6-dec-i-bryssel::events_blog_post.sv.md
- pages::03.news::data-spaces-in-an-interoperable-europe-6-dec-i-bryssel::events_blog_post.sv.md
- 03.news::data-spaces-in-an-interoperable-europe-6-dec-i-bryssel::events_blog_post.sv.md
- data-spaces-in-an-interoperable-europe-6-dec-i-bryssel::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'EU kommissionen har bjudit in JobTech Development att presentera på konferensen “Data Spaces in an Interoperable Europe.” 6 dec i Bryssel'
custom:
    title: 'EU kommissionen har bjudit in JobTech Development att presentera på konferensen “Data Spaces in an Interoperable Europe.” 6 dec i Bryssel'
    date: '2022-12-06 08:30'
    endtime: '2022-12-06 18:30'
    short: 'EU kommissionen har bjudit in JobTech Development och teamet som jobbar med interoperabilitet på SEMICs konferens i Bryssel för en presentation och att delta i en paneldiskussion med temat "Personal data space implementations – interoperability challenges"'
    content: "###### EU kommissionen har bjudit in JobTech Development att presentera och delta i paneldiskussion på konferensen “Data Spaces in an Interoperable Europe.”i Bryssel den 6 december. \n\n\n\"SEMIC2022\" kommer att erbjuda stöd för att övervinna hinder i genomförandet av datarum och klargöra hur interoperabilitet inom och mellan sektorer kan uppnås.\n\nI temaspåret där JobTech ingår är ämnet: \n\"Personal data space implementations – interoperability challenges\"\n\nFöljande personer kommer att delta i paneldiskussionen:\n•\tLahteenoja Viivi, PhD researcher, practical philosophy, Faculty of Social Sciences, University of Helsinki\n•\tDe Loof Esther, Operational Director, SolidLab\n•\tMarcello Grita, Project Leader, JobTech\n\n[läs mer om eventet](https://semic2022.eu/)\n\nvill du veta mer, kontakta  [Marcello Grita](mailto:marcello.grita@arbetsformedlingen.se), Projektledare. \n\n"
taxonomy:
    category:
        - Event
    status:
        - Avslutad
date: '2022-11-27 10:49'
---

