---
title: Internetdagarna 21-22 november
custom:
  title: Internetdagarna 21-22 november
  date: 2022-11-21 08:30
  endtime: 2022-11-22 15:45
  short: 'En tvådagars konferens för kunskap om internet och digitaliseringens påverkan på individ och samhälle. '
  content: "###### En tvådagars konferens för kunskap om internet och digitaliseringens påverkan på individ och samhälle. \n\nEtt event som är perfekt för alla som vill få de senaste trendspaningarna inom teknik och internet samt kunskap om digitaliseringens påverkan på människa och samhälle. \n\nVarje dag inleds och avslutas med aktuella trendspaningar från världsledande keynotetalare.\n\n[läs mer och anmäl dig](https://internetdagarna.se/)"
taxonomy:
  category:
  - Event
  status:
  - Avslutad
published: true
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/internetdagarna-21-22-november/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/internetdagarna-21-22-november/events_blog_post.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/internetdagarna-21-22-november/events_blog_post.sv.md
tags:
- user::pages::03.news::internetdagarna-21-22-november::events_blog_post.sv.md
- pages::03.news::internetdagarna-21-22-november::events_blog_post.sv.md
- 03.news::internetdagarna-21-22-november::events_blog_post.sv.md
- internetdagarna-21-22-november::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'Internetdagarna 21-22 november'
custom:
    title: 'Internetdagarna 21-22 november'
    date: '2022-11-21 08:30'
    endtime: '2022-11-22 15:45'
    short: 'En tvådagars konferens för kunskap om internet och digitaliseringens påverkan på individ och samhälle. '
    content: "###### En tvådagars konferens för kunskap om internet och digitaliseringens påverkan på individ och samhälle. \n\nEtt event som är perfekt för alla som vill få de senaste trendspaningarna inom teknik och internet samt kunskap om digitaliseringens påverkan på människa och samhälle. \n\nVarje dag inleds och avslutas med aktuella trendspaningar från världsledande keynotetalare.\n\n[läs mer och anmäl dig](https://internetdagarna.se/)"
taxonomy:
    category:
        - Event
    status:
        - Avslutad
published: true
---

