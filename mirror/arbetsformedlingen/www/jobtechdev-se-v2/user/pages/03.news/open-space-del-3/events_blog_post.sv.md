---
title: Open Space del 3
custom:
  title: Open Space del 3
  date: 2022-06-01 10:30
  endtime: 2022-06-02 15:00
  short: JobTech tillsammans med Sunet fortsätter serien workshops i formatet "Open Space" om digitalisering, kompetensförsörjning och livslångt lärande.
  content: "**Det är dags igen! Välkommen att samverka i framtidens community! Nu arrangerar vi tredje upplagan av Open Space: Digitalisering, kompetensförsörjning och livslångt lärande den 1–2 juni i Stockholm. **\n\nVi välkomnar alla inom såväl offentlig som privat sektor att delta och bidra. Har du kollegor eller andra kontakter som kan vara intresserade av att delta, bjud gärna in dem också. Vi behöver både dina och andras kloka insikter, förslag och tankar för ett event som genererar nytta och värde för alla. Vi vet att samverkan ger konkreta resultat. \n\nVälkommen att delta i workshopen och vara den som skapar agendan. Det blir lärorikt, givande och kul.\n\n\n**Plats och tid:**\n\nEventet kommer att äga rum under två dagar, den 1–2 juni, på plats i Stockholm i Internetstiftelsens lokaler, Hammarby Kaj 10D. \n\n[Anmäl dig](https://www.eventbrite.se/e/open-space-3-digitalisering-kompetensforsorjning-och-livslangt-larande-registrering-311068152217) och läs mer om eventet.\n\nSista dagen för anmälan är måndag den 23 maj. Vi har ett begränsat antal platser, så först till kvarn gäller. Vi bjuder på lunch och kaffe båda dagarna.\n\nVi hoppas att vi ses där! \n"
taxonomy:
  category:
  - Event
  status:
  - Avslutad
date: 2022-05-09 08:00
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/open-space-del-3/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/open-space-del-3/events_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/open-space-del-3/events_blog_post.sv.md
tags:
- user::pages::03.news::open-space-del-3::events_blog_post.sv.md
- pages::03.news::open-space-del-3::events_blog_post.sv.md
- 03.news::open-space-del-3::events_blog_post.sv.md
- open-space-del-3::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'Open Space del 3'
custom:
    title: 'Open Space del 3'
    date: '2022-06-01 10:30'
    endtime: '2022-06-02 15:00'
    short: 'JobTech tillsammans med Sunet fortsätter serien workshops i formatet "Open Space" om digitalisering, kompetensförsörjning och livslångt lärande.'
    content: "**Det är dags igen! Välkommen att samverka i framtidens community! Nu arrangerar vi tredje upplagan av Open Space: Digitalisering, kompetensförsörjning och livslångt lärande den 1–2 juni i Stockholm. **\n\nVi välkomnar alla inom såväl offentlig som privat sektor att delta och bidra. Har du kollegor eller andra kontakter som kan vara intresserade av att delta, bjud gärna in dem också. Vi behöver både dina och andras kloka insikter, förslag och tankar för ett event som genererar nytta och värde för alla. Vi vet att samverkan ger konkreta resultat. \n\nVälkommen att delta i workshopen och vara den som skapar agendan. Det blir lärorikt, givande och kul.\n\n\n**Plats och tid:**\n\nEventet kommer att äga rum under två dagar, den 1–2 juni, på plats i Stockholm i Internetstiftelsens lokaler, Hammarby Kaj 10D. \n\n[Anmäl dig](https://www.eventbrite.se/e/open-space-3-digitalisering-kompetensforsorjning-och-livslangt-larande-registrering-311068152217) och läs mer om eventet.\n\nSista dagen för anmälan är måndag den 23 maj. Vi har ett begränsat antal platser, så först till kvarn gäller. Vi bjuder på lunch och kaffe båda dagarna.\n\nVi hoppas att vi ses där! \n"
taxonomy:
    category:
        - Event
    status:
        - Avslutad
date: '2022-05-09 08:00'
---

