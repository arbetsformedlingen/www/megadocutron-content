---
title: Sweden in Europe's Data Ecosystem
custom:
  date: 2022-12-15 09:00
  endtime: 2022-12-15 16:00
  short: 'The conference is arranged by the Agency for Digital Government, together with AI Sweden, RISE and Vinnova as part of the joint work to promote the Swedish participation in the Digital Europe Programme. '
  content: "###### Sweden in Europe's Data Ecosystem: Fostering EU's Collaborations on All Levels <BR><BR>\n\nThe conference is arranged by the Agency for Digital Government (DIGG), together with AI Sweden, Research Institutes of Sweden (RISE) and Vinnova (Sweden’s Innovation Agency) as part of the joint work to promote the Swedish participation in the Digital Europe Programme. \n\nRepresentatives of the academia, the private and the public sectors are meeting the conference _Sweden in Europe's Data Ecosystem_ to join hands, explore and pave a common path forward in relation to EU's data ecosystem.  \n\nThe work related to Europe's internal market for data has been expanded through increasingly close collaborations. It gives strategic opportunities for Sweden to get more involved and thus contributing and having an impact on the development of the EU's AI and data ecosystem.\n\n**When:** Thursday, December 15th, 2022 <BR>\n**Time:** 09:00 - 16:00 <BR>\n**Where:** Vasagatan 22T, 111 20 Stockholm, Sweden\n\n[Register by December 14th, 2022. ](https://www.vinnova.se/en/events-calendar/2022/12/sweden-in-europes-data-ecosystem/)\n \n\n\n"
  title: Sweden in Europe's Data Ecosystem
taxonomy:
  category:
  - Event
  status:
  - Completed
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/sverige-i-europas-dataekosystem/events_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/sverige-i-europas-dataekosystem/events_blog_post.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/sverige-i-europas-dataekosystem/events_blog_post.en.md
tags:
- user::pages::03.news::sverige-i-europas-dataekosystem::events_blog_post.en.md
- pages::03.news::sverige-i-europas-dataekosystem::events_blog_post.en.md
- 03.news::sverige-i-europas-dataekosystem::events_blog_post.en.md
- sverige-i-europas-dataekosystem::events_blog_post.en.md
- events_blog_post.en.md
---
---
title: 'Sweden in Europe''s Data Ecosystem'
custom:
    date: '2022-12-15 09:00'
    endtime: '2022-12-15 16:00'
    short: 'The conference is arranged by the Agency for Digital Government, together with AI Sweden, RISE and Vinnova as part of the joint work to promote the Swedish participation in the Digital Europe Programme. '
    content: "###### Sweden in Europe's Data Ecosystem: Fostering EU's Collaborations on All Levels <BR><BR>\n\nThe conference is arranged by the Agency for Digital Government (DIGG), together with AI Sweden, Research Institutes of Sweden (RISE) and Vinnova (Sweden’s Innovation Agency) as part of the joint work to promote the Swedish participation in the Digital Europe Programme. \n\nRepresentatives of the academia, the private and the public sectors are meeting the conference _Sweden in Europe's Data Ecosystem_ to join hands, explore and pave a common path forward in relation to EU's data ecosystem.  \n\nThe work related to Europe's internal market for data has been expanded through increasingly close collaborations. It gives strategic opportunities for Sweden to get more involved and thus contributing and having an impact on the development of the EU's AI and data ecosystem.\n\n**When:** Thursday, December 15th, 2022 <BR>\n**Time:** 09:00 - 16:00 <BR>\n**Where:** Vasagatan 22T, 111 20 Stockholm, Sweden\n\n[Register by December 14th, 2022. ](https://www.vinnova.se/en/events-calendar/2022/12/sweden-in-europes-data-ecosystem/)\n \n\n\n"
    title: 'Sweden in Europe''s Data Ecosystem'
taxonomy:
    category:
        - Event
    status:
        - Completed
---

