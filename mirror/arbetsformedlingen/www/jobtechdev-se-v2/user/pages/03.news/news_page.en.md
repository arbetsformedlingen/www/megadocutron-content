---
title: News & Events
custom:
  title: News & Events
  text: 'You can read about current news, articles, insights, upcoming events and webinars, which JobTech Development is arranging, participating in, or in general are interesting for the digital ecosystem. '
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/news_page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/news_page.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/news_page.en.md
tags:
- user::pages::03.news::news_page.en.md
- pages::03.news::news_page.en.md
- 03.news::news_page.en.md
- news_page.en.md
---
---
title: 'News & Events'
custom:
    title: 'News & Events'
    text: 'You can read about current news, articles, insights, upcoming events and webinars, which JobTech Development is arranging, participating in, or in general are interesting for the digital ecosystem. '
---

