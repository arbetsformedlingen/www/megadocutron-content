---
title: Sweden Innovation Days 21-23 mars
custom:
  date: 2023-03-21 14:00
  endtime: 2023-03-23 17:00
  short: 'En kostnadsfri digital konferens med målet att visa upp Sverige som innovationsland med fokus på samhällets, miljöns och ekonomins inverkan. '
  content: "Välkommen till en kostnadsfri digital konferens med målet att visa upp Sverige som innovationsland med fokus på samhällets, miljöns och ekonomins inverkan. \n\nI år medverkar arbetsförmedlingen och Business Area Director Linda Schön Dorotsi med flera under temat: \n\n\"Switch to Sweden - New ways to connect\"\n\nFram till år 2024 kommer vi att behöva mer än 70 000 grymma talanger för att arbeta inom tekniska industrier i Sverige. Switch to Sweden är ett initiativ för att hjälpa till att lösa denna utmaning. Utifrån kompetenser och motivationsfaktorer matchar detta initiativ företag och internationella akademiska talanger som redan finns i Sverige.\n\n[Du hittar talarsloten 15:50 under dag 3 på agendan. ](https://swedeninnovationdays.se/mainstage/creating-real-impact/)\n\nKonferensen ger besökare möjlighet att dela information, utbyta kunskap och knyta kontakter med internationella partners.\n\nOavsett om du är en startup, företag, arbetar inom offentlig sektor, civilsamhället, akademin eller en statlig myndighet, finns det något för alla på detta evenemang. \n\nMissa inte Sweden Innovation Days 2023!\n\n[Anmälan och mer info](https://swedeninnovationdays.se/svenska/)"
  title: Sweden Innovation Days 21-23 mars
date: 2023-01-13 11:11
taxonomy:
  category:
  - Event
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/11.sweden-innovation-days/events_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/11.sweden-innovation-days/events_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/11.sweden-innovation-days/events_blog_post.sv.md
tags:
- user::pages::03.news::11.sweden-innovation-days::events_blog_post.sv.md
- pages::03.news::11.sweden-innovation-days::events_blog_post.sv.md
- 03.news::11.sweden-innovation-days::events_blog_post.sv.md
- 11.sweden-innovation-days::events_blog_post.sv.md
- events_blog_post.sv.md
---
---
title: 'Sweden Innovation Days 21-23 mars'
custom:
    date: '2023-03-21 14:00'
    endtime: '2023-03-23 17:00'
    short: 'En kostnadsfri digital konferens med målet att visa upp Sverige som innovationsland med fokus på samhällets, miljöns och ekonomins inverkan. '
    content: "Välkommen till en kostnadsfri digital konferens med målet att visa upp Sverige som innovationsland med fokus på samhällets, miljöns och ekonomins inverkan. \n\nI år medverkar arbetsförmedlingen och Business Area Director Linda Schön Dorotsi med flera under temat: \n\n\"Switch to Sweden - New ways to connect\"\n\nFram till år 2024 kommer vi att behöva mer än 70 000 grymma talanger för att arbeta inom tekniska industrier i Sverige. Switch to Sweden är ett initiativ för att hjälpa till att lösa denna utmaning. Utifrån kompetenser och motivationsfaktorer matchar detta initiativ företag och internationella akademiska talanger som redan finns i Sverige.\n\n[Du hittar talarsloten 15:50 under dag 3 på agendan. ](https://swedeninnovationdays.se/mainstage/creating-real-impact/)\n\nKonferensen ger besökare möjlighet att dela information, utbyta kunskap och knyta kontakter med internationella partners.\n\nOavsett om du är en startup, företag, arbetar inom offentlig sektor, civilsamhället, akademin eller en statlig myndighet, finns det något för alla på detta evenemang. \n\nMissa inte Sweden Innovation Days 2023!\n\n[Anmälan och mer info](https://swedeninnovationdays.se/svenska/)"
    title: 'Sweden Innovation Days 21-23 mars'
date: '2023-01-13 11:11'
taxonomy:
    category:
        - Event
---

