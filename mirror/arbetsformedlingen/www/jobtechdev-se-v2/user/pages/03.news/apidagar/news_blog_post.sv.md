---
title: Jobtech deltar i API Days Helsinki & North 2022
custom:
  title: Jobtech deltar i API Days Helsinki & North 2022
  short: Vår kollega Jonas Södergren kommer att prata om Sweden API Awards 2021-vinnaren JobStream den 16 mars, kl. 10.05 CET.
  content: |
    > "Jag är fascinerad av nya teknologier, öppna data och de oändliga möjligheterna som finns att göra skillnad för vårt samhälle genom samarbeten och delad data", säger Jonas Södergren, teknisk ansvarig och programmerare på JobTech Development.

    JobTech Development kommer att delta i den virtuella konferensen API Days Helsinki & North 2022. Tvådagarseventet äger rum den 16–17 mars, och vår kollega Jonas Södergren har en talarslott den 16 mars, kl. 10.05 CET. Han kommer att prata om Sweden API Awards 2021-vinnaren JobStream – ett öppet API som innehåller realtidsdata från alla jobbannonser som är publicerade hos Arbetsförmedlingen. Läs mer om [API:et JobStream här](https://jobtechdev.se/sv/komponenter/jobstream).

    > ”Vi arbetar för att skapa en hållbar och gemensam digital infrastruktur för matchningstjänster i Sverige. Vårt syfte är att underlätta en träffsäker matchning mellan arbetssökande och arbetsgivare genom delad data och öppen källkod, samt att bidra till en mer kompetensbaserad och datadriven arbetsmarknad”, säger Jonas Södergren.

    Delta i konferensen [här.](https://hopin.com/events/apidays-helsinki-north-2022)

    **Om API Days Helsinki & North**

    API Days Helsinki & North är ett av världens mest populära API-evenemang. Den årliga konferensen samlar deltagare från stora och små företag i olika branscher, mestadels från Norden, Europa och USA.

    Evenemanget är kostnadsfritt för alla deltagare och samlar över 1000 deltagare från Norra Europa och runt om i världen. Mer info om [eventet.](https://www.apidays.global/helsinki_and_north/)
date: 2022-03-09 11:29
taxonomy:
  category:
  - Nyhet
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/apidagar/news_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/apidagar/news_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/apidagar/news_blog_post.sv.md
tags:
- user::pages::03.news::apidagar::news_blog_post.sv.md
- pages::03.news::apidagar::news_blog_post.sv.md
- 03.news::apidagar::news_blog_post.sv.md
- apidagar::news_blog_post.sv.md
- news_blog_post.sv.md
---
---
title: 'Jobtech deltar i API Days Helsinki & North 2022'
custom:
    title: 'Jobtech deltar i API Days Helsinki & North 2022'
    short: 'Vår kollega Jonas Södergren kommer att prata om Sweden API Awards 2021-vinnaren JobStream den 16 mars, kl. 10.05 CET.'
    content: "> \"Jag är fascinerad av nya teknologier, öppna data och de oändliga möjligheterna som finns att göra skillnad för vårt samhälle genom samarbeten och delad data\", säger Jonas Södergren, teknisk ansvarig och programmerare på JobTech Development.\n\nJobTech Development kommer att delta i den virtuella konferensen API Days Helsinki & North 2022. Tvådagarseventet äger rum den 16–17 mars, och vår kollega Jonas Södergren har en talarslott den 16 mars, kl. 10.05 CET. Han kommer att prata om Sweden API Awards 2021-vinnaren JobStream – ett öppet API som innehåller realtidsdata från alla jobbannonser som är publicerade hos Arbetsförmedlingen. Läs mer om [API:et JobStream här](https://jobtechdev.se/sv/komponenter/jobstream).\n\n> ”Vi arbetar för att skapa en hållbar och gemensam digital infrastruktur för matchningstjänster i Sverige. Vårt syfte är att underlätta en träffsäker matchning mellan arbetssökande och arbetsgivare genom delad data och öppen källkod, samt att bidra till en mer kompetensbaserad och datadriven arbetsmarknad”, säger Jonas Södergren.\n\nDelta i konferensen [här.](https://hopin.com/events/apidays-helsinki-north-2022)\n\n**Om API Days Helsinki & North**\n\nAPI Days Helsinki & North är ett av världens mest populära API-evenemang. Den årliga konferensen samlar deltagare från stora och små företag i olika branscher, mestadels från Norden, Europa och USA.\n\nEvenemanget är kostnadsfritt för alla deltagare och samlar över 1000 deltagare från Norra Europa och runt om i världen. Mer info om [eventet.](https://www.apidays.global/helsinki_and_north/)\n"
date: '2022-03-09 11:29'
taxonomy:
    category:
        - Nyhet
---

