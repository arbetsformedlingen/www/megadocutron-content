---
title: Onboarding and Open Forum Webinar, May 27th
custom:
  title: Onboarding and Open Forum Webinar, May 27th
  date: 2022-05-27 10:00
  endtime: 2022-05-27 11:00
  short: JobTech Development arrange open getting-started webinars for anyone who would like to use our APIs.
  content: "###### Getting-started with JobTech's open APIs at our webinar on the 27th of May\nSince we want to make it easy for you to use our APIs, we invite you to getting-started webinars series. \n\nBy using our APIs, we join hands to work together for a better and more efficient labour market. Whether the purpose is to work in your own room, work on developing the next optimal matching service or improve an existing product - you are warmly welcome to use our services or just get in touch. \n\nAt the webinar, you will be meeting the developers behind JobtechSearch / JobtechStream, Historial Ads, JobtechTaxonomy, JobAd Enrichments and Individdata, leading an exciting pilot project on personal data and data portability.\n\nAfter the presentation, there is an opportunity to ask questions, give feedback, share ideas and extend request for new technical solutions from JobTech Development.\n\nWe do not serve a ready-made agenda for the meeting. The content is outlined by your specific interests and needs, following the so-called \"Open space\" model.\n\nWarm welcome!\n\nthe JobTech Development team\n\n"
login:
  visibility_requires_access: false
taxonomy:
  category:
  - Event
  status:
  - Completed
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/kom-igang-och-oeppet-forum-webbinarium-27-maj/events_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/kom-igang-och-oeppet-forum-webbinarium-27-maj/events_blog_post.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/kom-igang-och-oeppet-forum-webbinarium-27-maj/events_blog_post.en.md
tags:
- user::pages::03.news::kom-igang-och-oeppet-forum-webbinarium-27-maj::events_blog_post.en.md
- pages::03.news::kom-igang-och-oeppet-forum-webbinarium-27-maj::events_blog_post.en.md
- 03.news::kom-igang-och-oeppet-forum-webbinarium-27-maj::events_blog_post.en.md
- kom-igang-och-oeppet-forum-webbinarium-27-maj::events_blog_post.en.md
- events_blog_post.en.md
---
---
title: 'Onboarding and Open Forum Webinar, May 27th'
custom:
    title: 'Onboarding and Open Forum Webinar, May 27th'
    date: '2022-05-27 10:00'
    endtime: '2022-05-27 11:00'
    short: 'JobTech Development arrange open getting-started webinars for anyone who would like to use our APIs.'
    content: "###### Getting-started with JobTech's open APIs at our webinar on the 27th of May\nSince we want to make it easy for you to use our APIs, we invite you to getting-started webinars series. \n\nBy using our APIs, we join hands to work together for a better and more efficient labour market. Whether the purpose is to work in your own room, work on developing the next optimal matching service or improve an existing product - you are warmly welcome to use our services or just get in touch. \n\nAt the webinar, you will be meeting the developers behind JobtechSearch / JobtechStream, Historial Ads, JobtechTaxonomy, JobAd Enrichments and Individdata, leading an exciting pilot project on personal data and data portability.\n\nAfter the presentation, there is an opportunity to ask questions, give feedback, share ideas and extend request for new technical solutions from JobTech Development.\n\nWe do not serve a ready-made agenda for the meeting. The content is outlined by your specific interests and needs, following the so-called \"Open space\" model.\n\nWarm welcome!\n\nthe JobTech Development team\n\n"
login:
    visibility_requires_access: false
taxonomy:
    category:
        - Event
    status:
        - Completed
---

