---
title: Summary of eGoverment Days (eFörvaltningsdagarna)
custom:
  content: |-
    **Forces of change are needed at all levels for a successful digital transformation**

    JobTech Development participated in the eGovernment Days October 27-28, where we exchanged experiences about open data and open source code. Lecturers and panelists from both the public and private sectors agreed upon the importance of collaboration on all levels to boost innovation towards digitalization.

    Jonas Södergren from JobTech Development was among the speakers on stage, addressing tomorrow’s labour market and how and with what JobTech contributes in this direction.

    There are a lot of takeaways from these intensive days. However, one key takeaway is that nothing can compare being at an event and meeting people in reality.

    Stay tuned!
  title: Summary of eGoverment Days (eFörvaltningsdagarna)
  date: 2021-10-29 11:41
  short: 'JobTech Development participated in the eGovernment Days October 27-28, where we exchanged experiences about open data and open source code. '
date: 2021-10-29 11:41
taxonomy:
  category:
  - News
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/13.summering-av-efoervaltningsdagarna/news_blog_post.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/13.summering-av-efoervaltningsdagarna/news_blog_post.en.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/13.summering-av-efoervaltningsdagarna/news_blog_post.en.md
tags:
- user::pages::03.news::13.summering-av-efoervaltningsdagarna::news_blog_post.en.md
- pages::03.news::13.summering-av-efoervaltningsdagarna::news_blog_post.en.md
- 03.news::13.summering-av-efoervaltningsdagarna::news_blog_post.en.md
- 13.summering-av-efoervaltningsdagarna::news_blog_post.en.md
- news_blog_post.en.md
---
---
title: 'Summary of eGoverment Days (eFörvaltningsdagarna)'
custom:
    content: "**Forces of change are needed at all levels for a successful digital transformation**\n\nJobTech Development participated in the eGovernment Days October 27-28, where we exchanged experiences about open data and open source code. Lecturers and panelists from both the public and private sectors agreed upon the importance of collaboration on all levels to boost innovation towards digitalization.\n\nJonas Södergren from JobTech Development was among the speakers on stage, addressing tomorrow’s labour market and how and with what JobTech contributes in this direction.\n\nThere are a lot of takeaways from these intensive days. However, one key takeaway is that nothing can compare being at an event and meeting people in reality.\n\nStay tuned!"
    title: 'Summary of eGoverment Days (eFörvaltningsdagarna)'
    date: '2021-10-29 11:41'
    short: 'JobTech Development participated in the eGovernment Days October 27-28, where we exchanged experiences about open data and open source code. '
date: '2021-10-29 11:41'
taxonomy:
    category:
        - News
---

