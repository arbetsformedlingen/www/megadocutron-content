---
title: Kompetensmatchning.se
custom:
  short: 'Kompetensmatchning.se är en marknadsplats för kompetensutveckling där arbetsgivare och utbildningsanordnare kan samverka kring kompetens- och utbildningsbehov för yrkesverksamma. Plattformen är utvecklad av det statligt ägda forskningsinstitutet RISE, och är en nu tillgänglig på jobtechdev.se. '
  content: "   \n###### Kompetensmatchning.se är en marknadsplats för kompetensutveckling där arbetsgivare och utbildningsanordnare kan samverka kring kompetens- och utbildningsbehov för yrkesverksamma. Plattformen är utvecklad av det statligt ägda forskningsinstitutet RISE, och är en nu tillgänglig på plattformen. \n<BR>\n\n![Olle Nyman på RISE](./Olle_small.png)\n\n_Vi ställde några frågor till ansvariga projektledaren Olle Nyman._\n\n## Vad var upphovet till tjänsten Kompetensmatchning.se?\n”Upphovet var ett projekt RISE gjorde tillsammans med bland annat Västra Götalandsregionen och Business Region Göteborg. Projektet utgick från behovet för industrin att lättare kunna hitta relevanta utbildningar för sina anställda. Detta på grund av omställningen till elfordon och kompetensgapet som blev följden. Initialt utvecklades Kompetensmatchning.se primärt för arbetsgivares och utbildningsanordnares behov.”\n\n## Vad var målet med projektet och hur ser framtiden ut?\n”Målet var att bygga en plattform som underlättar för personer i arbetslivet att få tillgång till relevanta utbildningar, och möjliggöra enklare sätt för utbildningsanordnare att marknadsföra utbildningar som vänder sig till människor i arbetslivet. \nEfter det initiala projektet har vi pivoterat sidan till att bli en testbädd för den digitala infrastrukturen; för att testa automatiserade lösningar och ha en skarp testyta för pilotprojekt. Planen är att fortsätta med det.”\n\n## Hur kan det bidra till det digitala ekosystemet?\n”Genom att ha möjlighet att testa idéer och behov i en skarp miljö så kan vi bidra med insikter, behovsanalys och lösnings- och förbättringsförslag inom det digitala ekosystemet. Vi vet att vi behöver testa våra idéer med riktig data för att få bra insikter – det räcker inte att bara prata om behov.”"
  title: 'En plattform för kompetensutveckling    '
  infobox: |-
    ###Kompetensmatchning.se – en plattform för kompetensutveckling </h3>
    Kompetensmatchning.se är en marknadsplats för kompetensutveckling där arbetsgivare och utbildningsanordnare kan samverka kring kompetens- och utbildningsbehov för yrkesverksamma. Plattformen är utvecklad av det statligt ägda forskningsinstitutet RISE, och är en nu tillgänglig på [plattformen jobtechdev.se](https://jobtechdev.se/sv/komponenter/kompetensmatchning)<a href="https://jobtechdev.se/sv/komponenter/kompetensmatchning">.
date: 2022-11-28 12:38
taxonomy:
  category:
  - Nyhet
  type:
  - Intervju
media_order: Olle_Nyman_RISE.jpg
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/03.news/05.kompetensmatchning-se-en-plattform-foer-kompetensutveckling/news_blog_post.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/03.news/05.kompetensmatchning-se-en-plattform-foer-kompetensutveckling/news_blog_post.sv.md
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/05.kompetensmatchning-se-en-plattform-foer-kompetensutveckling/news_blog_post.sv.md
tags:
- user::pages::03.news::05.kompetensmatchning-se-en-plattform-foer-kompetensutveckling::news_blog_post.sv.md
- pages::03.news::05.kompetensmatchning-se-en-plattform-foer-kompetensutveckling::news_blog_post.sv.md
- 03.news::05.kompetensmatchning-se-en-plattform-foer-kompetensutveckling::news_blog_post.sv.md
- 05.kompetensmatchning-se-en-plattform-foer-kompetensutveckling::news_blog_post.sv.md
- news_blog_post.sv.md
---
---
title: Kompetensmatchning.se
custom:
    short: 'Kompetensmatchning.se är en marknadsplats för kompetensutveckling där arbetsgivare och utbildningsanordnare kan samverka kring kompetens- och utbildningsbehov för yrkesverksamma. Plattformen är utvecklad av det statligt ägda forskningsinstitutet RISE, och är en nu tillgänglig på jobtechdev.se. '
    content: "   \n###### Kompetensmatchning.se är en marknadsplats för kompetensutveckling där arbetsgivare och utbildningsanordnare kan samverka kring kompetens- och utbildningsbehov för yrkesverksamma. Plattformen är utvecklad av det statligt ägda forskningsinstitutet RISE, och är en nu tillgänglig på plattformen. \n<BR>\n\n![Olle Nyman på RISE](./Olle_small.png)\n\n_Vi ställde några frågor till ansvariga projektledaren Olle Nyman._\n\n## Vad var upphovet till tjänsten Kompetensmatchning.se?\n”Upphovet var ett projekt RISE gjorde tillsammans med bland annat Västra Götalandsregionen och Business Region Göteborg. Projektet utgick från behovet för industrin att lättare kunna hitta relevanta utbildningar för sina anställda. Detta på grund av omställningen till elfordon och kompetensgapet som blev följden. Initialt utvecklades Kompetensmatchning.se primärt för arbetsgivares och utbildningsanordnares behov.”\n\n## Vad var målet med projektet och hur ser framtiden ut?\n”Målet var att bygga en plattform som underlättar för personer i arbetslivet att få tillgång till relevanta utbildningar, och möjliggöra enklare sätt för utbildningsanordnare att marknadsföra utbildningar som vänder sig till människor i arbetslivet. \nEfter det initiala projektet har vi pivoterat sidan till att bli en testbädd för den digitala infrastrukturen; för att testa automatiserade lösningar och ha en skarp testyta för pilotprojekt. Planen är att fortsätta med det.”\n\n## Hur kan det bidra till det digitala ekosystemet?\n”Genom att ha möjlighet att testa idéer och behov i en skarp miljö så kan vi bidra med insikter, behovsanalys och lösnings- och förbättringsförslag inom det digitala ekosystemet. Vi vet att vi behöver testa våra idéer med riktig data för att få bra insikter – det räcker inte att bara prata om behov.”"
    title: 'En plattform för kompetensutveckling    '
    infobox: "###Kompetensmatchning.se – en plattform för kompetensutveckling </h3>\nKompetensmatchning.se är en marknadsplats för kompetensutveckling där arbetsgivare och utbildningsanordnare kan samverka kring kompetens- och utbildningsbehov för yrkesverksamma. Plattformen är utvecklad av det statligt ägda forskningsinstitutet RISE, och är en nu tillgänglig på [plattformen jobtechdev.se](https://jobtechdev.se/sv/komponenter/kompetensmatchning)<a href=\"https://jobtechdev.se/sv/komponenter/kompetensmatchning\">."
date: '2022-11-28 12:38'
taxonomy:
    category:
        - Nyhet
    type:
        - Intervju
media_order: Olle_Nyman_RISE.jpg
---

![Olle_Nyman_RISE](Olle_Nyman_RISE.jpg "Olle_Nyman_RISE")