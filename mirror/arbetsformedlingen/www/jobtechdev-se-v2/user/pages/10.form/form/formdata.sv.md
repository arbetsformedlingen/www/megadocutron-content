---
simplesearch:
  process: false
title: Form
form:
  id: newsletter
  layout: newsletter
  name: newsletter
  fields:
    honeypot:
      type: honeypot
    email:
      label: E-postadress
      placeholder: E-postadress
      type: email
      validate:
        required: true
    checkbox:
      label: Jag samtycker till att Arbetsförmedlingen lagrar ovanstående uppgifter och delar information till mig via epost
      type: checkbox
      validate:
        required: true
  buttons:
    submit:
      type: submit
      value: Prenumerera
  process:
    email:
      from: '{{ config.plugins.email.from }}'
      to:
      - '{{ config.plugins.email.to }}'
      - '{{ form.value.email }}'
      subject: '[Nyhetsbrev] {{ form.value.email|e }}'
      body: '{% include ''partials/mailnewslettercontent.html.twig'' %} {% include ''forms/data.html.twig'' %} {{form|s}}'
    save:
      fileprefix: feedback-
      dateformat: Ymd-His-u
      extension: txt
      body: '{% include ''forms/data.txt.twig'' %}}'
    display: thankyou
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/10.form/form/formdata.sv.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/10.form/form/formdata.sv.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/10.form/form/formdata.sv.md
tags:
- user::pages::10.form::form::formdata.sv.md
- pages::10.form::form::formdata.sv.md
- 10.form::form::formdata.sv.md
- form::formdata.sv.md
- formdata.sv.md
---
---
simplesearch:
    process: false
title: Form
form:
    id: newsletter
    layout: newsletter
    name: newsletter
    fields:
        honeypot:
            type: honeypot
        email:
            label: E-postadress
            placeholder: E-postadress
            type: email
            validate:
                required: true
        checkbox:
            label: 'Jag samtycker till att Arbetsförmedlingen lagrar ovanstående uppgifter och delar information till mig via epost'
            type: checkbox
            validate:
                required: true
    buttons:
        submit:
            type: submit
            value: Prenumerera
    process:
        email:
            from: '{{ config.plugins.email.from }}'
            to:
                - '{{ config.plugins.email.to }}'
                - '{{ form.value.email }}'
            subject: '[Nyhetsbrev] {{ form.value.email|e }}'
            body: '{% include ''partials/mailnewslettercontent.html.twig'' %} {% include ''forms/data.html.twig'' %} {{form|s}}'
        save:
            fileprefix: feedback-
            dateformat: Ymd-His-u
            extension: txt
            body: '{% include ''forms/data.txt.twig'' %}}'
        display: thankyou
---

