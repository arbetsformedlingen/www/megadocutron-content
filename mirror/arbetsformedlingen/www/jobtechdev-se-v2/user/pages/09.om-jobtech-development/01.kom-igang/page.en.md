---
title: Collaboration
custom:
  content: "## Using the APIs and Open Data\nOne way to collaborate with JobTech Development is to use the APIs, technology and open data that are available via our platform. \nWe welcome anyone to use all components on the platform, which are completely free of charge. \n* Check out our APIs\n* Contact us if you need help to getting started\n    \n## Share Knowledge, Experience, Technology or Data  \nAnother way of collaborating with us is to share knowledge and experience. We welcome you and your organization to share with us challenges, opportunities and requests, so we get a better understanding of what are the needs on the market and how we can meet them in the best possible way. You can also use the platfrom to share technology or data with others. No paid compensations for collbarations. \n\n* Feel free to contact our [Community team](mailto:jobtechdevelopment@arbetsformedlingen.se) for further discussion and information. \n\n## Feedback\nProviding us with feedback on what works well / less well. This is an excellent collaboration and an important contribution to the whole digital ecosystem as well as for the further improvement of the platform JobTech Development. \n\n* Report a bug or make a suggestion directly to the team via [Gitlab](https://gitlab.com/groups/arbetsformedlingen/-/issues)\n* Tell us what you are missing or what you are in need in our [Community Forum](https://forum.jobtechdev.se) or submit a request via the [form](https://link.webropolsurveys.com/S/C1F32BE6730E1171).\n<br><br><br><br>"
  menu:
  - title: Collaborate with us
    url: /en/about-jobtech-development/kom-igang
  - title: Get inspired by others
    url: /en/about-jobtech-development/inspireras-av-andra
  - title: Contact us
    url: /en/about-jobtech-development/kontakta-oss
  title: Cooperate with us
  ingress: By collaborating, we make it easier for jobseekers and employers to find each other in digital channels and thus contribute to a better labour market. This is among the driving forces for JobTech Development. The collaboration can look in many different ways and take place at different levels.
routes: {}
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/09.om-jobtech-development/01.kom-igang/page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/09.om-jobtech-development/01.kom-igang/page.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.om-jobtech-development/01.kom-igang/page.en.md
tags:
- user::pages::09.om-jobtech-development::01.kom-igang::page.en.md
- pages::09.om-jobtech-development::01.kom-igang::page.en.md
- 09.om-jobtech-development::01.kom-igang::page.en.md
- 01.kom-igang::page.en.md
- page.en.md
---
---
title: Collaboration
custom:
    content: "## Using the APIs and Open Data\nOne way to collaborate with JobTech Development is to use the APIs, technology and open data that are available via our platform. \nWe welcome anyone to use all components on the platform, which are completely free of charge. \n* Check out our APIs\n* Contact us if you need help to getting started\n    \n## Share Knowledge, Experience, Technology or Data  \nAnother way of collaborating with us is to share knowledge and experience. We welcome you and your organization to share with us challenges, opportunities and requests, so we get a better understanding of what are the needs on the market and how we can meet them in the best possible way. You can also use the platfrom to share technology or data with others. No paid compensations for collbarations. \n\n* Feel free to contact our [Community team](mailto:jobtechdevelopment@arbetsformedlingen.se) for further discussion and information. \n\n## Feedback\nProviding us with feedback on what works well / less well. This is an excellent collaboration and an important contribution to the whole digital ecosystem as well as for the further improvement of the platform JobTech Development. \n\n* Report a bug or make a suggestion directly to the team via [Gitlab](https://gitlab.com/groups/arbetsformedlingen/-/issues)\n* Tell us what you are missing or what you are in need in our [Community Forum](https://forum.jobtechdev.se) or submit a request via the [form](https://link.webropolsurveys.com/S/C1F32BE6730E1171).\n<br><br><br><br>"
    menu:
        -
            title: 'Collaborate with us'
            url: /en/about-jobtech-development/kom-igang
        -
            title: 'Get inspired by others'
            url: /en/about-jobtech-development/inspireras-av-andra
        -
            title: 'Contact us'
            url: /en/about-jobtech-development/kontakta-oss
    title: 'Cooperate with us'
    ingress: 'By collaborating, we make it easier for jobseekers and employers to find each other in digital channels and thus contribute to a better labour market. This is among the driving forces for JobTech Development. The collaboration can look in many different ways and take place at different levels.'
routes: {  }
---

