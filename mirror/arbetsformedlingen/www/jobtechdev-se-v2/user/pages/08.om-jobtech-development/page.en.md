---
title: About JobTech Development
custom:
  content: "Today's job market offers plenty of digital matching and guidance services. A fragmented labour market with so many employment tools is difficult to navigate through for both jobseekers and employers.\n\nJobTech Development has been implemented through a lean start-up method and is now an established unit at the Swedish Public Employment Service, working on sustainable and common infrastructure for digital matching services in Sweden. It is based on the idea of fewer silos and more common technical solutions and standards. Through collaborative initiatives, we would like to contribute to innovation and to building sustainable solutions, which are foolproof, accessible, and easy to use for anyone. We go from ego to eco!\n\nAt Jobtech Development, we welcome both public and private sector in an open digital ecosystem, where we join forces to foster collaboration efforts around data and data sharing. Currently, the ecosystem is enriched with approximately 200 companies and organizations that actively share data, knowledge, and code on our platform. All components and datasets are free of charge and available for anyone to use. We also collaborate with researchers and other stakeholders who would like to get an in-depth insight into the labour market through the utilization of open data or technical innovation.\n\nWe are making together a stronger and inclusive labour market through collaborative initiatives and data sharing. \n\n<br><br><br><br>\n\n"
  menu:
  - title: Collaborate with us
    url: /en/about-jobtech-development/kom-igang
  - title: Get inspired by others
    url: /en/about-jobtech-development/inspireras-av-andra
  - title: Contact us
    url: /en/about-jobtech-development/kontakta-oss
  title: From ego to eco
  ingress: Making a stronger labour market through collaboration and open data
routes:
  default: /about-jobtech-development
gitlaburl: https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-v2/-/blob/main//user/pages/08.om-jobtech-development/page.en.md
gitdir: /arbetsformedlingen/www/jobtechdev-se-v2
gitdir-file-path: /user/pages/08.om-jobtech-development/page.en.md
date: '2023-10-03 13:02:40'
path: /arbetsformedlingen/www/jobtechdev-se-v2/user/pages/08.om-jobtech-development/page.en.md
tags:
- user::pages::08.om-jobtech-development::page.en.md
- pages::08.om-jobtech-development::page.en.md
- 08.om-jobtech-development::page.en.md
- page.en.md
---
---
title: 'About JobTech Development'
custom:
    content: "Today's job market offers plenty of digital matching and guidance services. A fragmented labour market with so many employment tools is difficult to navigate through for both jobseekers and employers.\n\nJobTech Development has been implemented through a lean start-up method and is now an established unit at the Swedish Public Employment Service, working on sustainable and common infrastructure for digital matching services in Sweden. It is based on the idea of fewer silos and more common technical solutions and standards. Through collaborative initiatives, we would like to contribute to innovation and to building sustainable solutions, which are foolproof, accessible, and easy to use for anyone. We go from ego to eco!\n\nAt Jobtech Development, we welcome both public and private sector in an open digital ecosystem, where we join forces to foster collaboration efforts around data and data sharing. Currently, the ecosystem is enriched with approximately 200 companies and organizations that actively share data, knowledge, and code on our platform. All components and datasets are free of charge and available for anyone to use. We also collaborate with researchers and other stakeholders who would like to get an in-depth insight into the labour market through the utilization of open data or technical innovation.\n\nWe are making together a stronger and inclusive labour market through collaborative initiatives and data sharing. \n\n<br><br><br><br>\n\n"
    menu:
        -
            title: 'Collaborate with us'
            url: /en/about-jobtech-development/kom-igang
        -
            title: 'Get inspired by others'
            url: /en/about-jobtech-development/inspireras-av-andra
        -
            title: 'Contact us'
            url: /en/about-jobtech-development/kontakta-oss
    title: 'From ego to eco'
    ingress: 'Making a stronger labour market through collaboration and open data'
routes:
    default: /about-jobtech-development
---

