---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/security/vuln-report/-/blob/main//README.md
gitdir: /arbetsformedlingen/security/vuln-report
gitdir-file-path: /README.md
date: '2023-11-07 13:08:46'
path: /arbetsformedlingen/security/vuln-report/README.md
tags:
- README.md
---
This script gather all images in FROM construction of Dockerfile in every provided repo.

Then Snyk scans them against vulnerabilities with level of critical.

Then script sends report in text and json format to Mattermost channel and to Minio.
