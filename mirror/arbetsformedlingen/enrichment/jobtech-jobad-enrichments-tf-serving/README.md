---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/enrichment/jobtech-jobad-enrichments-tf-serving/-/blob/main//README.md
gitdir: /arbetsformedlingen/enrichment/jobtech-jobad-enrichments-tf-serving
gitdir-file-path: /README.md
date: '2023-09-27 11:29:44'
path: /arbetsformedlingen/enrichment/jobtech-jobad-enrichments-tf-serving/README.md
tags:
- README.md
---
README - jobtech-jobad-enrichments-tf-serving
==============================================



Build
-------------------
Master:
sudo docker build -t jobadenrichmentstfserving:latest .


Run
-------------------
gRPC:
sudo docker run -d -p 8500:8500 --name jobadenrichmentstfserving jobadenrichmentstfserving

REST:
sudo docker run -d -p 8501:8501 --name jobadenrichmentstfserving jobadenrichmentstfserving


Check log file
-------------------
sudo docker logs jobadenrichmentstfserving --details -f

Stop & remove image
-------------------
sudo docker stop jobadenrichmentstfserving;sudo docker rm jobadenrichmentstfserving || true


Debug Docker
-----------
sudo docker logs jobadenrichmentstfserving --details -f
sudo docker exec -t -i jobadenrichmentstfserving /bin/bash
sudo docker run -it --rm jobadenrichmentstfserving:latest

Mac, remove previous docker-builds + cointainers
--------------------------------------
docker system prune

Git
--------
git lfs track "resources/prediction_models_tf_keras/models/**"
