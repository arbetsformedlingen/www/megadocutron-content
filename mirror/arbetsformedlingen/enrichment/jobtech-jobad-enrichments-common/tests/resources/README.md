---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/enrichment/jobtech-jobad-enrichments-common/-/blob/main//tests/resources/README.md
gitdir: /arbetsformedlingen/enrichment/jobtech-jobad-enrichments-common
gitdir-file-path: /tests/resources/README.md
date: '2023-08-15 13:13:47'
path: /arbetsformedlingen/enrichment/jobtech-jobad-enrichments-common/tests/resources/README.md
tags:
- tests::resources::README.md
- resources::README.md
- README.md
---
Folder for test resources