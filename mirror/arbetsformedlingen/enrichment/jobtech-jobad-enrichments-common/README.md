---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/enrichment/jobtech-jobad-enrichments-common/-/blob/main//README.md
gitdir: /arbetsformedlingen/enrichment/jobtech-jobad-enrichments-common
gitdir-file-path: /README.md
date: '2023-08-15 13:13:47'
path: /arbetsformedlingen/enrichment/jobtech-jobad-enrichments-common/README.md
tags:
- README.md
---
JOBTECH-JOBAD-ENRICHMENTS-COMMON
----------------------------------

Setup dev environment
-----------------------
1. Install dependencies
----------------------------------
pip install -r requirements.txt


2. Install local develop package
----------------------------------
cd to jobtech-jobad-enrichments-common and run
python setup.py develop


Example import
----------------------------------
from jjec.nlp import Document


Nice to have
=================================================
Autogenerate requirements.txt
------------------------
https://www.idiotinside.com/2015/05/10/python-auto-generate-requirements-txt/
pip install pipreqs
pipreqs /path/to/project
