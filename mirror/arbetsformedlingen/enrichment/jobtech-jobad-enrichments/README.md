---
title: Install developer environment
gitlaburl: https://gitlab.com/arbetsformedlingen/enrichment/jobtech-jobad-enrichments/-/blob/main//README.md
gitdir: /arbetsformedlingen/enrichment/jobtech-jobad-enrichments
gitdir-file-path: /README.md
date: '2023-10-02 16:24:00'
path: /arbetsformedlingen/enrichment/jobtech-jobad-enrichments/README.md
tags:
- README.md
---
README - jobtech-jobad-enrichments
==================================

jobtech-jobad-enrichments - Getting started, see:
https://github.com/JobtechSwe/docs/blob/master/GettingStartedJobAdEnrichments.md

## Install developer environment
1. Install a virtual environment for Python (for example Conda) with python version: 3.10  

2. Clone necessary git repos.
Note: The git clone mentioned below should be so you have the folders beside each other, for example:
```
<your git home folder>/jobtech-jobad-enrichments
<your git home folder>/jobtech-jobad-enrichments-common
```
3.
Do git clone on:
git@gitlab.com:af-group/jobtech-jobad-enrichments-common.git

cd to jobtech-jobad-enrichments-common and run:
pip install -r requirements.txt
python setup.py develop

4. Install requirements for jobtech-jobad-enrichments
cd to jobtech-jobad-enrichments and run:
pip install -r requirements.txt

5. Set the needed local environment variables before running the app (see jobtech-jobad-enrichments/jobadenrichments/settings.py for additional variables).
```
FLASK_ENV='development'
ES_ONTOLOGY_HOST='https://<address to elasticsearch with index jae-synonym-dictionary here>'
ES_ONTOLOGY_USER=''
ES_ONTOLOGY_PWD=''
ES_ONTOLOGY_INDEX='jae-synonym-dictionary'
JAE_TF_SERVING_URL='https://jobad-enrichments-tf-serving.test.services.jtech.se'
JOB_SEARCH_URL='https://jobsearch.api.jobtechdev.se'
```
---

## RUN APP without Docker

Execute:
python jobadenrichments/__init__.py

## Swagger
Go to http://localhost:6358/ to test with the Swagger-API.

## DOCKER

### Build
docker build -t jobtechjobadenrichments:latest .

### Run
docker run -d -p 6358:6358 -v C:\data\git\enrichment\jobtech-jobad-enrichments-secrets:/tmp/jobtech-jobad-enrichments-secrets:z --env-file=docker.env --name jobtechjobadenrichments jobtechjobadenrichments

### Check log file
docker logs jobtechjobadenrichments --details -f

### Stop & remove image
docker stop jobtechjobadenrichments;docker rm jobtechjobadenrichments || true

#### Windows
docker stop jobtechjobadenrichments && docker rm jobtechjobadenrichments || true

### Debug Docker
docker logs jobtechjobadenrichments --details -f
docker exec -t -i jobtechjobadenrichments /bin/bash
docker run -it --rm jobtechjobadenrichments:latest

### Mac, remove previous docker-builds + cointainers
docker system prune


### Run, unit/integration tests
See instructions in tests/README.md

### Predictions/ ML model files
REST-call is done to separate tf serving server for predictions.

---

#### Nice to have
Autogenerate requirements.txt
https://www.idiotinside.com/2015/05/10/python-auto-generate-requirements-txt/
pip install pipreqs
To generate requirements.txt:
pipreqs /path/to/project

---

In your README, try to answer the following questions:
- What does this project do?
- Why is this project useful?
- How do I get started?
- Where can I get more help, if I need it?






https://opensource.guide/starting-a-project/
https://choosealicense.com/licenses/mit/
