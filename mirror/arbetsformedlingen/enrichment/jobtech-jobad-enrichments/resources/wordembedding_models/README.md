---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/enrichment/jobtech-jobad-enrichments/-/blob/main//resources/wordembedding_models/README.md
gitdir: /arbetsformedlingen/enrichment/jobtech-jobad-enrichments
gitdir-file-path: /resources/wordembedding_models/README.md
date: '2023-10-02 16:24:00'
path: /arbetsformedlingen/enrichment/jobtech-jobad-enrichments/resources/wordembedding_models/README.md
tags:
- resources::wordembedding_models::README.md
- wordembedding_models::README.md
- README.md
---
Base resource folder for wordembedding models. 