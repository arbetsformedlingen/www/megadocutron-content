---
title: DEV
gitlaburl: https://gitlab.com/arbetsformedlingen/enrichment/jobtech-jobad-enrichments/-/blob/main//tests/README.md
gitdir: /arbetsformedlingen/enrichment/jobtech-jobad-enrichments
gitdir-file-path: /tests/README.md
date: '2023-10-02 16:24:00'
path: /arbetsformedlingen/enrichment/jobtech-jobad-enrichments/tests/README.md
tags:
- tests::README.md
- README.md
---
**Configure env variables for tests**
Create local file
tests/pytest_secrets2.env

...with content:
** pytest_secrets2.env - content begin **
ES_ONTOLOGY_HOST=https://<host>:9243
ES_ONTOLOGY_USER=
ES_ONTOLOGY_PWD=

JAE_TF_SERVING_URL=https://jobad-enrichments-tf-serving.test.services.jtech.se

# DEV
ES_HOST=<host without protocol>
ES_PORT=9243
ES_USER=
ES_PWD=

** pytest_secrets2.env - content end **

...and fill in missing values for host, user and pwd.

**Run tests**
cd tests
python3 -m pytest -svv

Run only tests marked as integration tests
python3 -m pytest -svv -m integration 

Run specific file
python3 -m pytest -svv -m integration test_jobadenricher.py 

Or run testfile in IntelliJ
Rightclick, run
 
