---
title: 1.2.3
gitlaburl: https://gitlab.com/arbetsformedlingen/enrichment/jobtech-jobad-enrichments/-/blob/main//CHANGELOG.md
gitdir: /arbetsformedlingen/enrichment/jobtech-jobad-enrichments
gitdir-file-path: /CHANGELOG.md
date: '2023-10-02 16:24:00'
path: /arbetsformedlingen/enrichment/jobtech-jobad-enrichments/CHANGELOG.md
tags:
- CHANGELOG.md
---
Changelog jobtech-jobad-enrichments API
=======================================
https://semver.org/

## 1.2.3
* Adjust Docker image to support Python 3.10.

## 1.2.1
* New CI/CD infrastructure

## 1.2.0
* Removes requirement for header parameter api-key.
* Changes dependency for jobtech-common

## 1.1.7
* Activates/reverts uwsgi attribute: max-worker-lifetime after problems with response time

## 1.1.6
* Fixes bug with multiple punkts at the end of urls in the description text, that caused an eternal loop in the regex.sub.

## 1.1.5
* Inactivates uwsgi attributes: max-requests and max-worker-lifetime

## 1.1.4
* Bugfix: Switches so demo ads are fetched from JobSearch API instead of being fetched from Elastic.

## 1.1.3
* Improvement: Removed legacy result "concept_label" : "UNKNOWN" when no occupations could be found for an ad.

## 1.1.2
* Bugfix: Calls with ads that don't contain any keywords at all caused a runtime exception.

## 1.1.1
* Calls retrained model for OCCUPATIONS 

## 1.1.0
* Added flag for sorting candidates by prediction score: ASC, DESC, NOT_SORTED (default).

## 1.0.5 to 1.0.9 
* Attempt to fix day by day increasing response times.
    - Removed static method 
    - Lowered nginx client_max_body_size from 10m to 2m.

## 1.0.4
* Fixes increasing memory problem with Beaker cache (removed). Replaced with instance variables for memory and speed.

## 1.0.3
* Fixes problem with competencies containing '.net', not being predicted at all.

## 1.0.2
* Adjusts code and tests to be able to run integration tests without a local elastic server

## 1.0.1
* Adds APM functionality for measuring usage by API-key.
* Fixes problem with duplicate ids and merged values.

## 1.0.0
* Initial release with enrichment of type OCCUPATIONS, COMPETENCIES, TRAITS and GEO (workplace)
* Flag to include_synonyms, for returning synonyms for enriched candidates
* Flag include_misspelled_synonyms, for returning misspelled synonyms for enriched candidates
* Flag include_similar_spelled_synonyms, for returning similar spelled synonyms for enriched candidates