---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/giggi-motivationsmatchning/-/blob/main//README.md
gitdir: /arbetsformedlingen/giggi-motivationsmatchning
gitdir-file-path: /README.md
date: '2023-10-12 09:30:33'
path: /arbetsformedlingen/giggi-motivationsmatchning/README.md
tags:
- README.md
---
<h1>Vad är lärandeförsöket "Giggi"?</h1>
I lärandeförsöket Giggi testas motivation som underlag för matchning till korta uppdrag inom staten. Testet Giggi leds av Arbetsförmedlingen och Skatteverket och är en del av eSams verksamhetsplan för 2023-24. <br>

**Syfte och frågeställningar** <br>
Syftet med testet är att undersöka om matchning mot ett kortare uppdrag (”gig”) inom staten skulle vara möjligt när formella krav på meriter och erfarenhet byts ut mot den sökandes lust, motivation och driv för en viss fråga. En bakomliggande fråga som försöket också syftar till att utforska är vad hållbar matchning egentligen skulle kunna innebära – till skillnad från tidseffektiv matchning.
De huvudsakliga frågorna som vi testar och vill ha svar på är om det går att matcha på motivation (det vill säga om uppdraget blir utfört efter motivationsmatchningen), och vilka effekter det får på uppdragstagare och uppdragsgivare. Finns det intresse att matcha och matchas på motivation?

**Testgruppen**<br>
Testgruppen består av tjänstemän från de i projektet deltagande myndigheterna.

**Testdesign:**<br>
I testet Giggi testas motivationsdriven matchning mot kortare uppdrag. Genom att kontinuerligt erbjuda arbetstagare möjligheten att ta kortare uppdrag inom ramen för sin nuvarande anställning understödjs förutsättningarna för de anställda att uppleva autonomi (de kan testa något nytt om de vill) och kompetens (de kan utnyttja och utmana sin kompetens), det vill säga två av de grundläggande psykologiska behoven enligt Självbestämmandeteorin (SDT). Viktigt att understryka är att uppdragen inte tillsätts utifrån arbets- eller utbildningsmeriter utan de är öppna för alla anställda som har en nyfikenhet på problemet som formuleras i uppdraget. Genom att öppna uppdragen på detta sätt ökar möjligheterna för de anställda att känna självbestämmande och autonomi.

**Läs mer om Självbestämmandeteorin och motivation i rapporten**
"Motivationsmatchning - vägen mot hållbar matchning?":** Motivationsmatchning_sep_2023.docx

**Uppdragssidan:** <br>
https://giggi.jobtechdev.se/sok-uppdrag.html

**Kontakta oss gärna om du har några frågor om Giggi:** <br>
My Svensson: my.x.svensson@arbetsformedlingen.se
Nils Cederberg Ahlsten: nils.ahlsten@arbetsformedlingen.se
eller skapa ett Issue här i Gitlab!

