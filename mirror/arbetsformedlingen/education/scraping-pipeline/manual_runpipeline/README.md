---
title: README - manual_runpipeline
gitlaburl: https://gitlab.com/arbetsformedlingen/education/scraping-pipeline/-/blob/main//manual_runpipeline/README.md
gitdir: /arbetsformedlingen/education/scraping-pipeline
gitdir-file-path: /manual_runpipeline/README.md
date: '2023-09-07 11:16:10'
path: /arbetsformedlingen/education/scraping-pipeline/manual_runpipeline/README.md
tags:
- manual_runpipeline::README.md
- README.md
---
# README - manual_runpipeline

The purpose of this folder and README is to make it possible to start a 
[pipelinerun](https://openshift.github.io/pipelines-docs/docs/0.10.5/op-pipelinerun-definition-reference.html) 
manually and be able to increase the total timeout, wich is default 1h hour otherwise.  

## Start a manual PipelineRun
1. Download the [Openshift CLI (oc)](https://docs.openshift.com/container-platform/4.7/cli_reference/openshift_cli/getting-started-cli.html)
2. Login to Openshift with oc  (first login to https://console-openshift-console.test.services.jtech.se and click on your logged in username in the upper right corner, and click "Copy login command")
3. Cd to folder scraping-pipeline/manual_runpipeline
4. oc create -n education-scraping-pipeline-[environment] -f startpipeline.yaml
e.g. `oc create -n education-scraping-pipeline-prod -f startpipeline.yaml`