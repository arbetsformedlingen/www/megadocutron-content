---
title: Education scraping pipeline
gitlaburl: https://gitlab.com/arbetsformedlingen/education/scraping-pipeline/-/blob/main//README.md
gitdir: /arbetsformedlingen/education/scraping-pipeline
gitdir-file-path: /README.md
date: '2023-09-07 11:16:10'
path: /arbetsformedlingen/education/scraping-pipeline/README.md
tags:
- README.md
---
# Education scraping pipeline

Pipeline to scheduled to scrape courses and educations from SUSA-navet and store them
in Minio and expose them via API using OpenSearch.

The current environment runs images tagged with the corresponding environment, e.g. `test`, `prod`, `onprem`,
e.g. `https://gitlab.com/arbetsformedlingen/education/education-scraping/-/tags/prod`
See `docs/developer_setup.md` for more info. 

The steps in the pipeline are:
- Scraping = scrapes educations in SUSA-navet and also scrapes education plans and saves in Minio.
- Merge educations = connects educations from SUSA-navet with corresponding education plans and saves in Minio.
- Enrich educations = enriches the education descriptions and titles with job titles, competencies and traits and saves in Minio.
- Minio to Opensearch =  Reads from Minio and saves in Opensearch-index.
- Prune = Prunes indexes in Opensearch and removes old files from Minio.

