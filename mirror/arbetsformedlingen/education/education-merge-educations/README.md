---
title: Education merge educations
gitlaburl: https://gitlab.com/arbetsformedlingen/education/education-merge-educations/-/blob/main//README.md
gitdir: /arbetsformedlingen/education/education-merge-educations
gitdir-file-path: /README.md
date: '2023-06-07 10:06:50'
path: /arbetsformedlingen/education/education-merge-educations/README.md
tags:
- README.md
---
# Education merge educations

Merge educations by combining results from different sources (e.g. from scrapy spiders).


## Build docker
docker build -t educationmergeeducations:latest .

### Run command with local env-file.
docker run -d --env-file=docker-localhost.env --name educationmergeeducations educationmergeeducations

## Check log file
docker logs educationmergeeducations --details -f

## Stop & remove image
docker stop educationmergeeducations; docker rm educationmergeeducations || true

## Debug Docker
docker logs educationmergeeducations --details -f
docker exec -t -i educationmergeeducations /bin/bash
docker run -it --rm educationmergeeducations:latest
