---
title: Education enrich occupations
gitlaburl: https://gitlab.com/arbetsformedlingen/education/education-enrich-occupations/-/blob/main//README.md
gitdir: /arbetsformedlingen/education/education-enrich-occupations
gitdir-file-path: /README.md
date: '2023-10-26 11:18:29'
path: /arbetsformedlingen/education/education-enrich-occupations/README.md
tags:
- README.md
---
# Education enrich occupations

Project for enriching aggregated occupations with demanded competencies.


## Installation in local dev environment
1. Choose your python environment
2. In a terminal, run:
```
pip install -r requirements.txt
```
3. Adjust environment variables in educationenrichoccupations/settings.py
4. To install as a python package, in a terminal, run:
```
python setup.py develop
```

## Step 1. Prepare ads data to enrich
1. Download historical ads file, e.g. from https://data.jobtechdev.se/annonser/historiska/index.html
2. Unzip the ads files and put into tools/ads_resources folder.
3. Convert the files from json format to ndjson format.
   1. Run convert_json_ads_to_ndjson.py if the whole year is represented in one year.
      Verify source and dest path. Dont change pattern in dest path.
   2. Run convert_json_ads_from_multiple_files_to_ndjson.py if the year is split
      in multiple files (e.g. 20xxq1.json). Verify source and dest path. Dont change pattern in dest path.
4. Split ndjson files into 4 files by running convert_historical_ads_to_ndjson_parts. Verify source and dest path.
5. Upload files (the output from previous step) to minio. Verify env.variables for aws/s3. Run upload_files_to_minio.py.
   Note that files with same name will be overwritten.
6. The files are ready to be enriched from, see "Run the enriching and persisting of the dataset with enriched occupations"

## Step 2. Run the enriching and persisting of the dataset with enriched occupations
Execute trigger_enrich_occupations.py to run locally.

When running trigger_enrich_occupations.py the following will happen:
1. Historical ads are automatically downloaded from Minio (files have to be in ndjson-format, like .jsonl)
2. The ads are enriched with JobAd Enrichments API
3. The enriched result is added to each occupation
4. A complete list with enriched occupations are saved in a file, according to settings.OCCUPATIONS_RELATIVE_FILE_PATH  
   Note:
   - If the settings.OCCUPATIONS_SAVE_RESULT_IN_FILE_ONLY is set to True (default), the dataset is
     saved to file according to settings.OCCUPATIONS_RELATIVE_FILE_PATH.
   -  If the settings.OCCUPATIONS_SAVE_RESULT_IN_FILE_ONLY
      is set to False, the dataset will be persisted in Opensearch instead of saving to file.
5. Use tools/persist_jsonfile_occupations_in_opensearch.py to persist the dataset (the file from the previous step) 
in Opensearch.


## Docker

### Build
docker build -t educationenrichoccupations:latest .

### Run
docker run -d --env-file=docker.env --name educationenrichoccupations educationenrichoccupations

### Check log file
docker logs educationenrichoccupations --details -f

### Stop & remove image
docker stop educationenrichoccupations;docker rm educationenrichoccupations || true

Windows:
docker stop educationenrichoccupations && docker rm educationenrichoccupations || true

### Debug Docker
docker logs educationenrichoccupations --details -f
docker exec -t -i educationenrichoccupations /bin/bash
docker run -it --rm educationenrichoccupations:latest

### Mac, remove previous docker-builds + cointainers
docker system prune

