---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/education/education-enrich-occupations/-/blob/main//tools/ads_resources/README.md
gitdir: /arbetsformedlingen/education/education-enrich-occupations
gitdir-file-path: /tools/ads_resources/README.md
date: '2023-10-26 11:18:29'
path: /arbetsformedlingen/education/education-enrich-occupations/tools/ads_resources/README.md
tags:
- tools::ads_resources::README.md
- ads_resources::README.md
- README.md
---
Folder for historical ads, for conversion and enrichment