---
title: Education scraping
gitlaburl: https://gitlab.com/arbetsformedlingen/education/education-scraping/-/blob/main//README.md
gitdir: /arbetsformedlingen/education/education-scraping
gitdir-file-path: /README.md
date: '2023-08-30 15:59:24'
path: /arbetsformedlingen/education/education-scraping/README.md
tags:
- README.md
---
# Education scraping

Project for scraping education data.

# Install and run localhost
## Set up virtual environment for this repo:
- Make sure Python 3.8 or higher is installed
- Activate a virtual env (e.g. conda or virtualenv):
  - Using virtualenv: `File -> Project Structure -> SDKs -> + -> Add Python SDK... -> Virtual ENV Environment -(a Python 3.8 installation as base interpretator)`
  
## Install this repo:
- Make sure you are in the root of this repo and run:
```
    pip install -r requirements.txt
```
## Set up Minio:
- The default env.variable values (i.e. that are used as fallback if values not provided) uses Minio at localhost.
  Minio is used as file storage for the resulting educations that the spiders scrapes.

### Instructions how to set up localhost Minio in docker:

Run command:

```sh
docker run -p 19000:9000 -p 9001:9001 --name minio1 -v c:\minio_data:/data -e "MINIO_ROOT_USER=kll" -e "MINIO_ROOT_PASSWORD=abcd1234" quay.io/minio/minio server /data --console-address ":9001"
```
```sh
docker run -p 19000:9000 -p 9001:9001 --name minio1 -v /Users/penms/Documents/git/education-scraping/local_minio_data:/data -e "MINIO_ROOT_USER=kll" -e "MINIO_ROOT_PASSWORD=abcd1234" quay.io/minio/minio server /data --console-address ":9001"
```

Where c:\minio_data is the folder on the local filesystem where minio data are stored. Change to a working path and make sure the folder exist.

Show minio logs:
```sh
docker logs minio1
```
Minio web admingui is located on: `http://localhost:9001/`
(Login using the values for MINIO_ROOT_USER and MINIO_ROOT_PASSWORD in the docker run script above)

Start docker container next time (i.e. after installation as previous command):
```sh
docker start minio1
```
Stop docker container:
```sh
docker stop minio1
```
Remove docker container:
```sh
docker rm minio1
```

### Remote Minio:
An alternative is to set env.variables with prefix 'AWS_' to point to another Minio.
Make sure bucket exist on Minio! The default bucket to be used should be named `kll-test`. 
  Bucket can be created from Minio web admin gui.

# Running scrapy splash.
Some spiders (e.g. MDH) rely on that scrapy splash is running.
Start splash localhost in docker:
```sh
docker run -it -p 8050:8050 --rm scrapinghub/splash
```

Scraper doc:
`
https://github.com/scrapy-plugins/scrapy-splash/
`

# Running scrapers in Docker
## Build docker
docker build -t educationscraping:latest .

## Run docker
Note that Dockerfile calls `go_spiders_docker.py` where the spiders to run is defined. Change spiders to run if needed.
### Environment variables on local machine
To run on local machine, make sure the file docker-dev.env contain correct values for secrets (e.g. AWS_SECRET_ACCESS_KEY)

### Run command with local env-file.
docker run -d --env-file=docker-localhost.env --name educationscraping educationscraping

## Check log file
docker logs educationscraping --details -f

## Stop & remove image
docker stop educationscraping; docker rm educationscraping || true

## Debug Docker
docker logs educationscraping --details -f
docker exec -t -i educationscraping /bin/bash
docker run -it --rm educationscraping:latest

### Mac, remove previous docker-builds + cointainers
docker system prune

# Running scrapers in IntelliJ or PyCharm:
- Set up a run configuration (Edit configurations) with the following options:
    - Working directory = `[path to base dir]education-scraping\venv`
      (On Mac set working directory = `[path to base dir, education-scraping]`)
    - Choose target to run = `Module name` (instead of script path)
    - Module name = `scrapy.cmdline`
    - Example parameters (i.e. when scraping only susanavet). date_from part is mandatory:
      ``crawl -a date_from=01/12/2019 -s LOG_LEVEL=INFO susanavet -o "..\`filename.json"``
    - Example environment variables when storing to remote Minio in dev (localhost Minio is default):
```
    S3_URL=https://minio.arbetsformedlingen.se
    S3_BUCKET_NAME=kll-test
    AWS_ACCESS_KEY=[ask for]
    AWS_SECRET_ACCESS_KEY=[ask for]
```
Note that only one spider is run; in the crawl example above it's susanavet. Change to other spider if needed.
      
- Additional env.variables:
```
      CRAWLERA_ENABLED=False;
      CLOSESPIDER_ITEMCOUNT=20;
      CLOSESPIDER_PAGECOUNT=20;
      ES_EDUCATIONS_INDEX_AFFIX=educations-localhost;
```
