---
title: JobEd Connect API
gitlaburl: https://gitlab.com/arbetsformedlingen/education/education-api/-/blob/main//README.md
gitdir: /arbetsformedlingen/education/education-api
gitdir-file-path: /README.md
date: '2023-11-06 08:21:34'
path: /arbetsformedlingen/education/education-api/README.md
tags:
- README.md
---
# JobEd Connect API   

Endpoints for searching and matching related items in the labor market and the field of education

# Install and run localhost
## Set up virtual environment for this repo:
- Make sure Python 3.8 or higher is installed
- Activate a virtual env (e.g. conda or virtualenv):
    - Using virtualenv: `File -> Project Structure -> SDKs -> + -> Add Python SDK... -> Virtual ENV Environment -(a Python 3.8 installation as base interpretator)`
 
## Install dependency to the external repos:

### Education-opensearch
```
    git clone git@gitlab.com:arbetsformedlingen/education/education-opensearch.git
```
- Change dir to education-opensearch
- Install with the virtual env activated:
```
    pip install -r requirements.txt
```
- Run setup with the virtual env activated:
```
    python setup.py develop
```
- Follow the education-opensearch readme for how to set up open search db localhost or connect to remote db.

## Install this repo:
- Make sure you are in the root of this repo and run:
```
    pip install -r requirements.txt
```

# Running api in IntelliJ or PyCharm:
- Run `educationapi/__init__.py`
- Example environment variables to provide when running against remote Open Search on AWS:
```
      ES_HOST=search-jobtech-opensearch-7zrtdhvq4feysy3peubhzsweai.eu-central-1.es.amazonaws.com;
      ES_USER=[ask for];
      ES_PORT=443;
      ES_PWD=[ask for]
      ES_EDUCATIONS_INDEX=educations-susanavet-20211112-08.23
```
- Example environment variables to provide when running against localhost Open Search:
```
      ES_USER=admin
      ES_PWD=admin
      ES_EDUCATIONS_INDEX=educations-susanavet-20211112-08.23
      ES_USE_SSL=False
```

Swagger is available on `http://localhost:5000/`

## Podman

### Build
You might first need to login to jobtechdev docker mirror:
podman login docker-mirror.jobtechdev.se

Build container:
podman build -t educationapi:latest .

### Run
- Make sure there is a docker.env file in the root of repo (i.e. same level as the dockerfile). The docker.env  
  could contain the following env.variables:
  ```
  JAE_API_URL=https://jobad-enrichments-test-api.jobtechdev.se
  # Localhost
  ES_HOST=host.containers.internal
  ES_PORT=19200
  ES_USER=
  ES_PWD=
  ES_USE_SSL=
  ES_VERIFY_CERTS=
  ```

podman run -d --env-file=docker.env -p 5000:5000 --name educationapi educationapi

Swagger is available on `http://localhost:5000/`

### Check log file
podman logs educationapi --details -f

### Stop & remove image
podman stop educationapi;docker rm educationapi && true

Windows:
podman stop educationapi && docker rm educationapi || true

### Debug Podman
podman logs educationapi --details -f
podman exec -t -i educationapi /bin/bash
podman run -it --rm educationapi:latest

### Mac, remove previous podman-builds + cointainers
podman system prune
