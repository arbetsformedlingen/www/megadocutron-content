---
title: Education minio to opensearch
gitlaburl: https://gitlab.com/arbetsformedlingen/education/education-minio-to-opensearch/-/blob/main//README.md
gitdir: /arbetsformedlingen/education/education-minio-to-opensearch
gitdir-file-path: /README.md
date: '2023-06-07 10:08:09'
path: /arbetsformedlingen/education/education-minio-to-opensearch/README.md
tags:
- README.md
---
# Education minio to opensearch 

Stores file data into opensearch as documents in an index


## Docker

### Build
docker build -t educationminiotoopensearch:latest .

### Run
docker run -d --env-file=docker-localhost.env --name educationminiotoopensearch educationminiotoopensearch

### Check log file
docker logs educationminiotoopensearch --details -f

### Stop & remove image
docker stop educationminiotoopensearch && docker rm educationminiotoopensearch || true

### Debug Docker
docker logs educationminiotoopensearch --details -f
docker exec -t -i educationminiotoopensearch /bin/bash
docker run -it --rm educationminiotoopensearch:latest

### Mac, remove previous docker-builds + cointainers
docker system prune