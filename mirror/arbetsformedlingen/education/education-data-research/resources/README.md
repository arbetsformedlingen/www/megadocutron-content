---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/education/education-data-research/-/blob/main//resources/README.md
gitdir: /arbetsformedlingen/education/education-data-research
gitdir-file-path: /resources/README.md
date: '2023-02-09 11:26:09'
path: /arbetsformedlingen/education/education-data-research/resources/README.md
tags:
- resources::README.md
- README.md
---
This folder is for needed data files that are not managed in git, for example scraped data from susa-navet and mdh.se.