---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/education/education-data-research/-/blob/main//enriched_educations/resources/README.md
gitdir: /arbetsformedlingen/education/education-data-research
gitdir-file-path: /enriched_educations/resources/README.md
date: '2023-02-09 11:26:09'
path: /arbetsformedlingen/education/education-data-research/enriched_educations/resources/README.md
tags:
- enriched_educations::resources::README.md
- resources::README.md
- README.md
---
Resource files here