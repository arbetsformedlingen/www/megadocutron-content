---
title: Education prune educations
gitlaburl: https://gitlab.com/arbetsformedlingen/education/education-prune-educations/-/blob/main//README.md
gitdir: /arbetsformedlingen/education/education-prune-educations
gitdir-file-path: /README.md
date: '2023-10-05 10:17:12'
path: /arbetsformedlingen/education/education-prune-educations/README.md
tags:
- README.md
---
# Education prune educations 


Pipeline step that prunes indexes in Opensearch and removes too old pipeline files from Minio.

## Docker

### Build
podman build -t educationpruneeducations:latest .

### Run
podman run -d --env-file=podman-localhost.env --name educationpruneeducations educationpruneeducations

### Check log file
podman logs educationpruneeducations --details -f

### Stop & remove image
podman stop educationpruneeducations && podman rm educationpruneeducations || true

### Debug podman
podman logs educationpruneeducations --details -f
podman exec -t -i educationpruneeducations /bin/bash
podman run -it --rm educationpruneeducations:latest


