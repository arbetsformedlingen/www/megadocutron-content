---
title: Education enrich educations
gitlaburl: https://gitlab.com/arbetsformedlingen/education/education-enrich-educations/-/blob/main//README.md
gitdir: /arbetsformedlingen/education/education-enrich-educations
gitdir-file-path: /README.md
date: '2023-06-07 10:07:38'
path: /arbetsformedlingen/education/education-enrich-educations/README.md
tags:
- README.md
---
# Education enrich educations

Project for enriching educations with found keywords in education titles, descriptions and education plans.  

When running the following will happen:  
1. Merged educations are downloaded from Minio (files have to be in ndjson-format, like .jsonl)
2. The educations are enriched with JobAd Enrichments API
3. The enriched result is added to each education
4. A file with a complete list of enriched educations is uploaded to Minio with the following filname format:   
merged_and_enriched_educations_2022-03-07_11_09_25.jsonl

## Installation in local dev environment
1. Choose your python environment
2. In a terminal, run:
```
pip install -r requirements.txt
```
3. Adjust environment variables in educationenricheducations/settings.py
4. To install as a python package, in a terminal, run:
```
python setup.py develop
```

## Run the enriching and persisting of occupations
Execute trigger_enrich_educations.py to run locally.

For execution in other project, see example in trigger_enrich_educations.py


## Docker

### Build
docker build -t educationenricheducations:latest .

### Run
docker run -d --env-file=docker.env --name educationenricheducations educationenricheducations

### Check log file
docker logs educationenricheducations --details -f

### Stop & remove image
docker stop educationenricheducations;docker rm educationenricheducations || true

### Debug Docker
docker logs educationenricheducations --details -f
docker exec -t -i educationenricheducations /bin/bash
docker run -it --rm educationenricheducations:latest

### Mac, remove previous docker-builds + cointainers
docker system prune

