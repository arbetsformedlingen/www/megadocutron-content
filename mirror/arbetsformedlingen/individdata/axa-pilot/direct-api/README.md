---
title: Direct API
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/direct-api/-/blob/main//README.md
gitdir: /arbetsformedlingen/individdata/axa-pilot/direct-api
gitdir-file-path: /README.md
date: '2022-10-27 11:51:52'
path: /arbetsformedlingen/individdata/axa-pilot/direct-api/README.md
tags:
- README.md
---
# Direct API

This repository contains the following:

* a sample `swagger.json` describing the expected api interface exposing 3 endpoints (`/token`, `/refresh`, `/status`).
* a sample project `demo-encrypt-sign` with unit tests showcasing how the `encryptedContent` is to be signed, verified, encrypted and decrypted.

All HTTP requests and responses have the same of model.

The model contains a single property called `encryptedContent`, which is a JSON Web Token(JWT) of type JSON Web Encryption(JWE).
Nested inside this JWE, is a JWT of type JSON Web Signature(JWS), which contains the details(claims) of the request/response.

The requesting party will always sign the nested JWS with its own private key, and then encrypt the JWE with the receiving parties public key.

The responding party will always sign the nested JWS with its own private key, and then encrypt the JWE with the receiving parties public key.

This means that both parties will need to exchange public keys.

Please see the provided examples(junit test) demonstrating signing, verification, encryption and decryption here: `/demo-encrypt-sign/src/test/java/se/jobtechdev/demoencryptsign/EncryptionDecryptionTest`

# Demo Encrypt Sign

The example mock API handles decryption, verification and performs some basic checks to ensure that the JWE and nested JWS contains the expected claims.
The example mock API also responds back with a JWE with nested JWS containing the expected claims of a response.

Launch the Spring Boot application and try the example API:

## Get a sample request and try the /token endpoint

```
$ curl localhost:8080/sampleTokenRequest
{"encryptedContent": "eyJl...8XiA"}

$ curl -X POST -H "Content-Type: application/json" -H "Accept: application/json" -d '{"encryptedContent": "eyJl...8XiA"}' localhost:8080/token
{"encryptedContent": "eyJl...qZaA"}
```

## Get a sample request and try the /refresh endpoint

```
$ curl localhost:8080/sampleRefreshRequest
{"encryptedContent": "eyJl...XIyQ"}

$ curl -X POST -H "Content-Type: application/json" -H "Accept: application/json" -d '{"encryptedContent": "eyJl...XIyQ"}' localhost:8080/refresh
{"encryptedContent": "eyJl...pNJg"}
```

## Get a sample request and try the /status endpoint

```
$ curl localhost:8080/sampleRefreshRequest
Response body: {"encryptedContent": "eyJl...3Y6Q"}

$ curl -X POST -H "Content-Type: application/json" -H "Accept: application/json" -d '{"encryptedContent": "eyJl...3Y6Q"}' localhost:8080/status
Response body: {"encryptedContent": "eyJl...AmGw"}
```
