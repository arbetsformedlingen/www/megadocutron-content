---
title: Fake Backend
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/fake-backend/-/blob/main//README.md
gitdir: /arbetsformedlingen/individdata/axa-pilot/fake-backend
gitdir-file-path: /README.md
date: '2023-10-24 15:45:03'
path: /arbetsformedlingen/individdata/axa-pilot/fake-backend/README.md
tags:
- README.md
---
# Fake Backend

This is a small application for use in development. It is intended to be used
as a replacement to a proper application backend during development.

## Usage

Build the container:

```shell
podman build -t fakebackend:latest .
```

Once built, the container image can be started with:

```shell
podman run -p 8000:8000 fakebackend:latest
```
