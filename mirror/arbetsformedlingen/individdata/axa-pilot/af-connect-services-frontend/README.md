---
title: Mitt inskrivningsintyg, frontend
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-frontend/-/blob/main//README.md
gitdir: /arbetsformedlingen/individdata/axa-pilot/af-connect-services-frontend
gitdir-file-path: /README.md
date: '2023-11-10 13:50:42'
path: /arbetsformedlingen/individdata/axa-pilot/af-connect-services-frontend/README.md
tags:
- README.md
---
# Mitt inskrivningsintyg, frontend

This is the frontend component of the _Mitt inskrivningsintyg_ service.
It's implemented with NextJS and tRPC.

The frontend expects a backend to be available and configured, see [Af Connect Services Backend](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend).

## Getting started

The project can be built as a container or the container built by and published by our CI system can be used (location `docker-images.jobtechdev.se/af-connect-services-frontend/af-connect-services-frontend:nextjs`).

### Authentication

Authentication is assumed to be handled by a reverse proxy in front of the application.
The signed in users personal identity number is expected to be passed as a HTTP header (i.e. `Remote-User`).

For development purposes our fake [auth proxy](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/auth-proxy) can be used in combination with Caddy.

### Configuration

Configuration is expected through the environment (either explicitly or with a `.env` file).
Have a look at `.env.example`.

## Development

The repository contains a devcontainer configuration that provides a consistent development environment.

To run the development server use `npm run dev`.

The reverse proxy container runs Caddy on port 4000 and uses the fakeauth container to authenticate requests before sending them to the devcontainer at port 3000.

### Tests

Tests are run with Vitest which is fast, Jest compatible and works well with TypeScript.

To run the tests use `npm run test` or, if you want test coverage and UI `npm run test:ui`.

#### End-to-end with Playwright

For tests with Playwright the application server needs to be started before running the tests with `npm run test:e2e`.
You should preferrably use a production build for these tests (`npm run build` followed by `npm run start`) as the tests will run faster and more reliably.

#### Visual testing with Storybook

For development and visual testing of the project components start Storybook with `npm run storybook`.

### Podman-compose

Different podman-compose commands can be run from within the project directory (from outside the devcontainer) if you specify `-f` and `-p`.
For instance if you want to show the logs from the backend (`logs backend`):

```bash
podman-compose \
-f .devcontainer/docker-compose.development.yml \
-p afconnectservicesfrontenddevcontainer \
logs backend
```

This can also be used to update one of the containers with `pull` and `up -d <service>`.
