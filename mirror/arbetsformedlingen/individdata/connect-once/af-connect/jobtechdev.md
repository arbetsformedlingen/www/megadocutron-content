---
title: Projectname
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-connect/-/blob/main//jobtechdev.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-connect
gitdir-file-path: /jobtechdev.md
date: '2023-10-31 13:47:33'
path: /arbetsformedlingen/individdata/connect-once/af-connect/jobtechdev.md
tags:
- jobtechdev.md
---
### Projectname

AF Connect

##### Description

The AF Connect service is responsible for obtaining informed consent from the user before any user data is transferred to third parties.

##### Versions, current dev state and future

1.1.0-beta

##### Getting started

No getting started guidelines yet.

##### Source code

https://github.com/MagnumOpuses/af-connect

##### Demo

No demo yet.
