---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-connect/-/blob/main//app/lib/common-cv-model/HROpen-v4.2-1/wellness/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-connect
gitdir-file-path: /app/lib/common-cv-model/HROpen-v4.2-1/wellness/CONTRIBUTORS.md
date: '2023-10-31 13:47:33'
path: /arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/wellness/CONTRIBUTORS.md
tags:
- app::lib::common-cv-model::HROpen-v4.2-1::wellness::CONTRIBUTORS.md
- lib::common-cv-model::HROpen-v4.2-1::wellness::CONTRIBUTORS.md
- common-cv-model::HROpen-v4.2-1::wellness::CONTRIBUTORS.md
- HROpen-v4.2-1::wellness::CONTRIBUTORS.md
- wellness::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.1 Wellness specification.  
* Nigel King, Oracle, Project Lead;  
* Fred Voltmer, Oracle, Diagram Editor;  
* Todd Newton, Independent;  
* Kim Bartkus, HR Open
