---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-connect/-/blob/main//app/lib/common-cv-model/HROpen-v4.2-1/assessments/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-connect
gitdir-file-path: /app/lib/common-cv-model/HROpen-v4.2-1/assessments/CONTRIBUTORS.md
date: '2023-10-31 13:47:33'
path: /arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/assessments/CONTRIBUTORS.md
tags:
- app::lib::common-cv-model::HROpen-v4.2-1::assessments::CONTRIBUTORS.md
- lib::common-cv-model::HROpen-v4.2-1::assessments::CONTRIBUTORS.md
- common-cv-model::HROpen-v4.2-1::assessments::CONTRIBUTORS.md
- HROpen-v4.2-1::assessments::CONTRIBUTORS.md
- assessments::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.1 Assessment specification.   
* David Steckbeck, Birkman International, Project Lead;  
* Chris Pauley, Independent, Schema Editor;  
* Jim Elder, DDI;  
* Rick Barfoot, HRNX  
