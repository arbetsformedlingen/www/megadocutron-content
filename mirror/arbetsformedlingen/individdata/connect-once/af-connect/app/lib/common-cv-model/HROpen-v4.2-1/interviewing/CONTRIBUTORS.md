---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-connect/-/blob/main//app/lib/common-cv-model/HROpen-v4.2-1/interviewing/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-connect
gitdir-file-path: /app/lib/common-cv-model/HROpen-v4.2-1/interviewing/CONTRIBUTORS.md
date: '2023-10-31 13:47:33'
path: /arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/interviewing/CONTRIBUTORS.md
tags:
- app::lib::common-cv-model::HROpen-v4.2-1::interviewing::CONTRIBUTORS.md
- lib::common-cv-model::HROpen-v4.2-1::interviewing::CONTRIBUTORS.md
- common-cv-model::HROpen-v4.2-1::interviewing::CONTRIBUTORS.md
- HROpen-v4.2-1::interviewing::CONTRIBUTORS.md
- interviewing::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.1 Interviewing specification.  
* Roby Hyde, HireVue, Project Lead; 
* Greg Meyers, Montage, Sample Editor;  
* Kim Bartkus, HR Open, Schema Editor;  
* Chris Pauley, Independent, Schema Editor;  
* Tony Ray, SAP/Successfactors  
