---
title: Projectname
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-connect/-/blob/main//app/lib/common-cv-model/jobtechdev.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-connect
gitdir-file-path: /app/lib/common-cv-model/jobtechdev.md
date: '2023-10-31 13:47:33'
path: /arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/jobtechdev.md
tags:
- app::lib::common-cv-model::jobtechdev.md
- lib::common-cv-model::jobtechdev.md
- common-cv-model::jobtechdev.md
- jobtechdev.md
---
### Projectname

Gravity Specs

##### Description

Gravity Specs is the public space where all Gravity specifications are published. This repository should contain all details required for your service to become compliant with the Gravity infrastructure.

##### Versions, current dev state and future

No versions yet.

##### Getting started

No getting started guidelines yet.

##### Source code

https://github.com/MagnumOpuses/gravity-specs

##### Demo

No demo yet.
