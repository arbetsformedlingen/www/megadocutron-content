---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-connect/-/blob/main//app/lib/common-cv-model/HROpen-v4.2-1/payroll/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-connect
gitdir-file-path: /app/lib/common-cv-model/HROpen-v4.2-1/payroll/CONTRIBUTORS.md
date: '2023-10-31 13:47:33'
path: /arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/payroll/CONTRIBUTORS.md
tags:
- app::lib::common-cv-model::HROpen-v4.2-1::payroll::CONTRIBUTORS.md
- lib::common-cv-model::HROpen-v4.2-1::payroll::CONTRIBUTORS.md
- common-cv-model::HROpen-v4.2-1::payroll::CONTRIBUTORS.md
- HROpen-v4.2-1::payroll::CONTRIBUTORS.md
- payroll::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
The following volunteers contributed to the HR Open 4.1 Screening specification.  
* Dave Garrett, NGA HR, Project Lead;  
* Chris Pauley, Independent, Schema Editor; 
* Kim Bartkus, HR Open;  
* Wendy Dodds, ADP;  
* Eric Bouldalier, Talentia;  
* Marco Galieni, Zucchetti;  
* Jan-Willem van der Boom, Manus;  
* Marcel Jemio, US Office of Personnel Management;  
* Ruben Drong, Papaya Global;  
* Lyndon Shelby, Insperity;  
