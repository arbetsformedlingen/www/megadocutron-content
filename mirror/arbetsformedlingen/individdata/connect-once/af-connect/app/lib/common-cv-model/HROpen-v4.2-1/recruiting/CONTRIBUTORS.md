---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-connect/-/blob/main//app/lib/common-cv-model/HROpen-v4.2-1/recruiting/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-connect
gitdir-file-path: /app/lib/common-cv-model/HROpen-v4.2-1/recruiting/CONTRIBUTORS.md
date: '2023-10-31 13:47:33'
path: /arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/recruiting/CONTRIBUTORS.md
tags:
- app::lib::common-cv-model::HROpen-v4.2-1::recruiting::CONTRIBUTORS.md
- lib::common-cv-model::HROpen-v4.2-1::recruiting::CONTRIBUTORS.md
- common-cv-model::HROpen-v4.2-1::recruiting::CONTRIBUTORS.md
- HROpen-v4.2-1::recruiting::CONTRIBUTORS.md
- recruiting::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.2 Recruiting specification.   
* Andrew Cunsolo, Talemetry, Project Lead;  
* Sascha Juchem, Milch & Zucker, Schema Editor;  
* Chris Pauley, Independent;  
* Bruce Anderson, Swedish Public Employment Services;  
* Siegy Wiens, HRNX;  
* Jack Spain, Arya  
* Sayali Parkhi, Harbinger Group;
* Shreyas Joshi, Harbinger Group;
* Eric Im, Independant;
* Kim Bartkus, HR Open Standards Consortium
