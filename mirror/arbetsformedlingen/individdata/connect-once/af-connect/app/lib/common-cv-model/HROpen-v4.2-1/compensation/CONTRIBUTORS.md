---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-connect/-/blob/main//app/lib/common-cv-model/HROpen-v4.2-1/compensation/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-connect
gitdir-file-path: /app/lib/common-cv-model/HROpen-v4.2-1/compensation/CONTRIBUTORS.md
date: '2023-10-31 13:47:33'
path: /arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/compensation/CONTRIBUTORS.md
tags:
- app::lib::common-cv-model::HROpen-v4.2-1::compensation::CONTRIBUTORS.md
- lib::common-cv-model::HROpen-v4.2-1::compensation::CONTRIBUTORS.md
- common-cv-model::HROpen-v4.2-1::compensation::CONTRIBUTORS.md
- HROpen-v4.2-1::compensation::CONTRIBUTORS.md
- compensation::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.1 Salary Survey specification.  
* David B. Turetsky, ADP, Project Lead;  
* Amit Sharma, ADP, Schema Editor; 
* Kim Bartkus, HR Open, Recording Secretary;  
* Chad Atwell, Oracle;  
* Eric Zitaner, ADP;  
* Rob Jasper, Payscale;  
* Katie Bardaro, Payscale;
* Chris Fusco, Salary.com; 
* Nancy Lamborn, eVestment;  
* Mark Avery, Marketpay;  
* Eric Hurst, Individual;  
