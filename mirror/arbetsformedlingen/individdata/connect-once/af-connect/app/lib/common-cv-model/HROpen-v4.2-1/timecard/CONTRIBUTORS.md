---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-connect/-/blob/main//app/lib/common-cv-model/HROpen-v4.2-1/timecard/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-connect
gitdir-file-path: /app/lib/common-cv-model/HROpen-v4.2-1/timecard/CONTRIBUTORS.md
date: '2023-10-31 13:47:33'
path: /arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/timecard/CONTRIBUTORS.md
tags:
- app::lib::common-cv-model::HROpen-v4.2-1::timecard::CONTRIBUTORS.md
- lib::common-cv-model::HROpen-v4.2-1::timecard::CONTRIBUTORS.md
- common-cv-model::HROpen-v4.2-1::timecard::CONTRIBUTORS.md
- HROpen-v4.2-1::timecard::CONTRIBUTORS.md
- timecard::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.1 Timecard specification.
* Jan-Willem van der Boom, Manus, Project Lead;  
* Dimitar Draganov, Manus, Schema Editor;  
* Scott Peter, Insperity, Recording Secretary; 
* Kim Bartkus, HR Open;  
* Tony Ray, IQ Navigator;
* Sino Jos, EasyClocking;
* Jean Handle-Bailey, SAP;
* Nancy Lawson, Independent;  
