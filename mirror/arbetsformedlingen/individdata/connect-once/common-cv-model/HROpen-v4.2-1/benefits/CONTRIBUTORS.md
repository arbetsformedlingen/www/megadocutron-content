---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/common-cv-model/-/blob/main//HROpen-v4.2-1/benefits/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/common-cv-model
gitdir-file-path: /HROpen-v4.2-1/benefits/CONTRIBUTORS.md
date: '2023-08-17 18:10:13'
path: /arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/benefits/CONTRIBUTORS.md
tags:
- HROpen-v4.2-1::benefits::CONTRIBUTORS.md
- benefits::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.1 Recruiting specification.  
* Brian Frazier, Sunlife, Project Lead;  
* Leslie Erwin, ADP, Schema Editor;  
* Michael Volpe, SS&C Technologies;  
* Karen Lindokken, The Standard;  
* Terry Sycamore, Unum, Diagram Editor;  
* Aaron Miller, Voya Financial;  
*	Raj Moorkath, The Hartford;  
*	Austin Bordelon, Private Exchange Coalition (PES);  
*	Ryan Howells, Private Exchange Coalition (PES);  
*	Yolanda Austin, ACORD;  
* Matthew Prisco, Betafits; 
*	Kenneth Bradshaw, Voya Financial;  
* Alan Frye, Benefitfocus;  
*	Angela Stegall, Benefitfocus  
