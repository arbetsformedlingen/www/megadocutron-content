---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/common-cv-model/-/blob/main//HROpen-v4.2-1/wellness/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/common-cv-model
gitdir-file-path: /HROpen-v4.2-1/wellness/CONTRIBUTORS.md
date: '2023-08-17 18:10:13'
path: /arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/wellness/CONTRIBUTORS.md
tags:
- HROpen-v4.2-1::wellness::CONTRIBUTORS.md
- wellness::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.1 Wellness specification.  
* Nigel King, Oracle, Project Lead;  
* Fred Voltmer, Oracle, Diagram Editor;  
* Todd Newton, Independent;  
* Kim Bartkus, HR Open
