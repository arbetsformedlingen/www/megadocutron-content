---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/common-cv-model/-/blob/main//HROpen-v4.2-1/assessments/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/common-cv-model
gitdir-file-path: /HROpen-v4.2-1/assessments/CONTRIBUTORS.md
date: '2023-08-17 18:10:13'
path: /arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/assessments/CONTRIBUTORS.md
tags:
- HROpen-v4.2-1::assessments::CONTRIBUTORS.md
- assessments::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.1 Assessment specification.   
* David Steckbeck, Birkman International, Project Lead;  
* Chris Pauley, Independent, Schema Editor;  
* Jim Elder, DDI;  
* Rick Barfoot, HRNX  
