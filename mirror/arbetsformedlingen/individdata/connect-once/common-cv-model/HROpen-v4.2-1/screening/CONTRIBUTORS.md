---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/common-cv-model/-/blob/main//HROpen-v4.2-1/screening/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/common-cv-model
gitdir-file-path: /HROpen-v4.2-1/screening/CONTRIBUTORS.md
date: '2023-08-17 18:10:13'
path: /arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/screening/CONTRIBUTORS.md
tags:
- HROpen-v4.2-1::screening::CONTRIBUTORS.md
- screening::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.1 Screening specification.  
* Kim Bartkus, HR Open, Project Lead;  
* Tom Adams, Applicant Insight, Schema Developer;  
* Eric Andersen, Cisive, Schema Developer;   
* Kelli Davis, Castle Branch, Recording Secretary;   
* Shelby Brayton, Castle Branch, Schema Developer;   
* Michael McManus, Proforma;    
* Ravi Sathish, First Advantage;   
* Ray Hargraves, Independent;    
* Jim Cross, Proforma;   
* Katie Hartley, Accurate Background;   
* Chris Fong, Universal Background;   
* Theresa Cassidy, Vertical Screen;   
