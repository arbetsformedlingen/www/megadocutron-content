---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/common-cv-model/-/blob/main//HROpen-v4.2-1/payroll/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/common-cv-model
gitdir-file-path: /HROpen-v4.2-1/payroll/CONTRIBUTORS.md
date: '2023-08-17 18:10:13'
path: /arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/payroll/CONTRIBUTORS.md
tags:
- HROpen-v4.2-1::payroll::CONTRIBUTORS.md
- payroll::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
The following volunteers contributed to the HR Open 4.1 Screening specification.  
* Dave Garrett, NGA HR, Project Lead;  
* Chris Pauley, Independent, Schema Editor; 
* Kim Bartkus, HR Open;  
* Wendy Dodds, ADP;  
* Eric Bouldalier, Talentia;  
* Marco Galieni, Zucchetti;  
* Jan-Willem van der Boom, Manus;  
* Marcel Jemio, US Office of Personnel Management;  
* Ruben Drong, Papaya Global;  
* Lyndon Shelby, Insperity;  
