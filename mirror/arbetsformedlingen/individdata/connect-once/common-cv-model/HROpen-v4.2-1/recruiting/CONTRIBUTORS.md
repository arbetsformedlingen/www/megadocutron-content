---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/common-cv-model/-/blob/main//HROpen-v4.2-1/recruiting/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/common-cv-model
gitdir-file-path: /HROpen-v4.2-1/recruiting/CONTRIBUTORS.md
date: '2023-08-17 18:10:13'
path: /arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/recruiting/CONTRIBUTORS.md
tags:
- HROpen-v4.2-1::recruiting::CONTRIBUTORS.md
- recruiting::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.2 Recruiting specification.   
* Andrew Cunsolo, Talemetry, Project Lead;  
* Sascha Juchem, Milch & Zucker, Schema Editor;  
* Chris Pauley, Independent;  
* Bruce Anderson, Swedish Public Employment Services;  
* Siegy Wiens, HRNX;  
* Jack Spain, Arya  
* Sayali Parkhi, Harbinger Group;
* Shreyas Joshi, Harbinger Group;
* Eric Im, Independant;
* Kim Bartkus, HR Open Standards Consortium
