---
title: Projectname
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-portability/-/blob/main//jobtechdev.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-portability
gitdir-file-path: /jobtechdev.md
date: '2023-11-07 13:33:59'
path: /arbetsformedlingen/individdata/connect-once/af-portability/jobtechdev.md
tags:
- jobtechdev.md
---
### Projectname

Gravity Portability

##### Description

Gravity Portability is the web service that enables the end-user to extract/export their own Arbetsförmedlingen CV directly to their personal Gravity storage.

##### Versions, current dev state and future

1.1.0-beta

##### Getting started

No getting started guidelines yet.

##### Source code

https://github.com/MagnumOpuses/gravity-portability

##### Demo

No demo yet.
