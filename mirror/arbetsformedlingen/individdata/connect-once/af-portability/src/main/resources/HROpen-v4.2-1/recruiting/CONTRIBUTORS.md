---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-portability/-/blob/main//src/main/resources/HROpen-v4.2-1/recruiting/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-portability
gitdir-file-path: /src/main/resources/HROpen-v4.2-1/recruiting/CONTRIBUTORS.md
date: '2023-11-07 13:33:59'
path: /arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/recruiting/CONTRIBUTORS.md
tags:
- src::main::resources::HROpen-v4.2-1::recruiting::CONTRIBUTORS.md
- main::resources::HROpen-v4.2-1::recruiting::CONTRIBUTORS.md
- resources::HROpen-v4.2-1::recruiting::CONTRIBUTORS.md
- HROpen-v4.2-1::recruiting::CONTRIBUTORS.md
- recruiting::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.2 Recruiting specification.   
* Andrew Cunsolo, Talemetry, Project Lead;  
* Sascha Juchem, Milch & Zucker, Schema Editor;  
* Chris Pauley, Independent;  
* Bruce Anderson, Swedish Public Employment Services;  
* Siegy Wiens, HRNX;  
* Jack Spain, Arya  
* Sayali Parkhi, Harbinger Group;
* Shreyas Joshi, Harbinger Group;
* Eric Im, Independant;
* Kim Bartkus, HR Open Standards Consortium
