---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-portability/-/blob/main//src/main/resources/HROpen-v4.2-1/timecard/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-portability
gitdir-file-path: /src/main/resources/HROpen-v4.2-1/timecard/CONTRIBUTORS.md
date: '2023-11-07 13:33:59'
path: /arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/timecard/CONTRIBUTORS.md
tags:
- src::main::resources::HROpen-v4.2-1::timecard::CONTRIBUTORS.md
- main::resources::HROpen-v4.2-1::timecard::CONTRIBUTORS.md
- resources::HROpen-v4.2-1::timecard::CONTRIBUTORS.md
- HROpen-v4.2-1::timecard::CONTRIBUTORS.md
- timecard::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.1 Timecard specification.
* Jan-Willem van der Boom, Manus, Project Lead;  
* Dimitar Draganov, Manus, Schema Editor;  
* Scott Peter, Insperity, Recording Secretary; 
* Kim Bartkus, HR Open;  
* Tony Ray, IQ Navigator;
* Sino Jos, EasyClocking;
* Jean Handle-Bailey, SAP;
* Nancy Lawson, Independent;  
