---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-portability/-/blob/main//src/main/resources/HROpen-v4.2-1/screening/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-portability
gitdir-file-path: /src/main/resources/HROpen-v4.2-1/screening/CONTRIBUTORS.md
date: '2023-11-07 13:33:59'
path: /arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/screening/CONTRIBUTORS.md
tags:
- src::main::resources::HROpen-v4.2-1::screening::CONTRIBUTORS.md
- main::resources::HROpen-v4.2-1::screening::CONTRIBUTORS.md
- resources::HROpen-v4.2-1::screening::CONTRIBUTORS.md
- HROpen-v4.2-1::screening::CONTRIBUTORS.md
- screening::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.1 Screening specification.  
* Kim Bartkus, HR Open, Project Lead;  
* Tom Adams, Applicant Insight, Schema Developer;  
* Eric Andersen, Cisive, Schema Developer;   
* Kelli Davis, Castle Branch, Recording Secretary;   
* Shelby Brayton, Castle Branch, Schema Developer;   
* Michael McManus, Proforma;    
* Ravi Sathish, First Advantage;   
* Ray Hargraves, Independent;    
* Jim Cross, Proforma;   
* Katie Hartley, Accurate Background;   
* Chris Fong, Universal Background;   
* Theresa Cassidy, Vertical Screen;   
