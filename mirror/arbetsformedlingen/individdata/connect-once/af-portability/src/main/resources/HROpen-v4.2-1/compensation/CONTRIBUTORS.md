---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-portability/-/blob/main//src/main/resources/HROpen-v4.2-1/compensation/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-portability
gitdir-file-path: /src/main/resources/HROpen-v4.2-1/compensation/CONTRIBUTORS.md
date: '2023-11-07 13:33:59'
path: /arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/compensation/CONTRIBUTORS.md
tags:
- src::main::resources::HROpen-v4.2-1::compensation::CONTRIBUTORS.md
- main::resources::HROpen-v4.2-1::compensation::CONTRIBUTORS.md
- resources::HROpen-v4.2-1::compensation::CONTRIBUTORS.md
- HROpen-v4.2-1::compensation::CONTRIBUTORS.md
- compensation::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.1 Salary Survey specification.  
* David B. Turetsky, ADP, Project Lead;  
* Amit Sharma, ADP, Schema Editor; 
* Kim Bartkus, HR Open, Recording Secretary;  
* Chad Atwell, Oracle;  
* Eric Zitaner, ADP;  
* Rob Jasper, Payscale;  
* Katie Bardaro, Payscale;
* Chris Fusco, Salary.com; 
* Nancy Lamborn, eVestment;  
* Mark Avery, Marketpay;  
* Eric Hurst, Individual;  
