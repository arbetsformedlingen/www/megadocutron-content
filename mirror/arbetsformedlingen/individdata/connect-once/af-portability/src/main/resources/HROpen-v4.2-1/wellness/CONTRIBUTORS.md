---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-portability/-/blob/main//src/main/resources/HROpen-v4.2-1/wellness/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-portability
gitdir-file-path: /src/main/resources/HROpen-v4.2-1/wellness/CONTRIBUTORS.md
date: '2023-11-07 13:33:59'
path: /arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/wellness/CONTRIBUTORS.md
tags:
- src::main::resources::HROpen-v4.2-1::wellness::CONTRIBUTORS.md
- main::resources::HROpen-v4.2-1::wellness::CONTRIBUTORS.md
- resources::HROpen-v4.2-1::wellness::CONTRIBUTORS.md
- HROpen-v4.2-1::wellness::CONTRIBUTORS.md
- wellness::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.1 Wellness specification.  
* Nigel King, Oracle, Project Lead;  
* Fred Voltmer, Oracle, Diagram Editor;  
* Todd Newton, Independent;  
* Kim Bartkus, HR Open
