---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-portability/-/blob/main//src/test/resources/schemas/HROpen-v4.2-1/payroll/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-portability
gitdir-file-path: /src/test/resources/schemas/HROpen-v4.2-1/payroll/CONTRIBUTORS.md
date: '2023-11-07 13:33:59'
path: /arbetsformedlingen/individdata/connect-once/af-portability/src/test/resources/schemas/HROpen-v4.2-1/payroll/CONTRIBUTORS.md
tags:
- src::test::resources::schemas::HROpen-v4.2-1::payroll::CONTRIBUTORS.md
- test::resources::schemas::HROpen-v4.2-1::payroll::CONTRIBUTORS.md
- resources::schemas::HROpen-v4.2-1::payroll::CONTRIBUTORS.md
- schemas::HROpen-v4.2-1::payroll::CONTRIBUTORS.md
- HROpen-v4.2-1::payroll::CONTRIBUTORS.md
- payroll::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
The following volunteers contributed to the HR Open 4.1 Screening specification.  
* Dave Garrett, NGA HR, Project Lead;  
* Chris Pauley, Independent, Schema Editor; 
* Kim Bartkus, HR Open;  
* Wendy Dodds, ADP;  
* Eric Bouldalier, Talentia;  
* Marco Galieni, Zucchetti;  
* Jan-Willem van der Boom, Manus;  
* Marcel Jemio, US Office of Personnel Management;  
* Ruben Drong, Papaya Global;  
* Lyndon Shelby, Insperity;  
