---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-portability/-/blob/main//src/test/resources/schemas/HROpen-v4.2-1/assessments/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-portability
gitdir-file-path: /src/test/resources/schemas/HROpen-v4.2-1/assessments/CONTRIBUTORS.md
date: '2023-11-07 13:33:59'
path: /arbetsformedlingen/individdata/connect-once/af-portability/src/test/resources/schemas/HROpen-v4.2-1/assessments/CONTRIBUTORS.md
tags:
- src::test::resources::schemas::HROpen-v4.2-1::assessments::CONTRIBUTORS.md
- test::resources::schemas::HROpen-v4.2-1::assessments::CONTRIBUTORS.md
- resources::schemas::HROpen-v4.2-1::assessments::CONTRIBUTORS.md
- schemas::HROpen-v4.2-1::assessments::CONTRIBUTORS.md
- HROpen-v4.2-1::assessments::CONTRIBUTORS.md
- assessments::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.1 Assessment specification.   
* David Steckbeck, Birkman International, Project Lead;  
* Chris Pauley, Independent, Schema Editor;  
* Jim Elder, DDI;  
* Rick Barfoot, HRNX  
