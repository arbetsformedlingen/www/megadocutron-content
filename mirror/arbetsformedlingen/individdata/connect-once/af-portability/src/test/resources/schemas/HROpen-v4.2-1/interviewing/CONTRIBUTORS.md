---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-portability/-/blob/main//src/test/resources/schemas/HROpen-v4.2-1/interviewing/CONTRIBUTORS.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-portability
gitdir-file-path: /src/test/resources/schemas/HROpen-v4.2-1/interviewing/CONTRIBUTORS.md
date: '2023-11-07 13:33:59'
path: /arbetsformedlingen/individdata/connect-once/af-portability/src/test/resources/schemas/HROpen-v4.2-1/interviewing/CONTRIBUTORS.md
tags:
- src::test::resources::schemas::HROpen-v4.2-1::interviewing::CONTRIBUTORS.md
- test::resources::schemas::HROpen-v4.2-1::interviewing::CONTRIBUTORS.md
- resources::schemas::HROpen-v4.2-1::interviewing::CONTRIBUTORS.md
- schemas::HROpen-v4.2-1::interviewing::CONTRIBUTORS.md
- HROpen-v4.2-1::interviewing::CONTRIBUTORS.md
- interviewing::CONTRIBUTORS.md
- CONTRIBUTORS.md
---
If you would like to contribute to the standards, contact us at info@hropenststandards.org.

The following volunteers contributed to the HR Open 4.1 Interviewing specification.  
* Roby Hyde, HireVue, Project Lead; 
* Greg Meyers, Montage, Sample Editor;  
* Kim Bartkus, HR Open, Schema Editor;  
* Chris Pauley, Independent, Schema Editor;  
* Tony Ray, SAP/Successfactors  
