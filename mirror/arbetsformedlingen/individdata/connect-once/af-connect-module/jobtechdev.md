---
title: Projectname
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-connect-module/-/blob/main//jobtechdev.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-connect-module
gitdir-file-path: /jobtechdev.md
date: '2023-10-30 12:30:08'
path: /arbetsformedlingen/individdata/connect-once/af-connect-module/jobtechdev.md
tags:
- jobtechdev.md
---
### Projectname

AF Connect Module

##### Description

The purpose of AF Connect Module is to ease the integration procedure for CV consumer services.

The AF Connect Module provides an interactive button for the end-user to initiate the authentication and CV extraction procedure.

##### Versions, current dev state and future

No versions yet.

##### Getting started

No getting started guidelines yet.

##### Source code

https://github.com/MagnumOpuses/af-connect-module

##### Demo

No demo yet.
