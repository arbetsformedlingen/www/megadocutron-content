---
title: Projectname
gitlaburl: https://gitlab.com/arbetsformedlingen/individdata/connect-once/af-connect-demo/-/blob/main//jobtechdev.md
gitdir: /arbetsformedlingen/individdata/connect-once/af-connect-demo
gitdir-file-path: /jobtechdev.md
date: '2023-10-27 08:57:18'
path: /arbetsformedlingen/individdata/connect-once/af-connect-demo/jobtechdev.md
tags:
- jobtechdev.md
---
### Projectname

AF Connect Demo

##### Description

AF Connect Demo is an example website that demonstrates the use-case of pre-filling forms with CV data from [AF Connect](https://github.com/MagnumOpuses/af-connect).

##### Versions, current dev state and future

1.2.0-beta

##### Getting started

No getting started guidelines yet.

##### Source code

https://github.com/MagnumOpuses/af-connect-demo

##### Demo

Well this kind of is the demo.
