---
title: Uppdatera Metadatafiler som publiceras på dataportal.se
gitlaburl: https://gitlab.com/arbetsformedlingen/metadata-test/-/blob/main//README.md
gitdir: /arbetsformedlingen/metadata-test
gitdir-file-path: /README.md
date: '2023-04-19 15:03:34'
path: /arbetsformedlingen/metadata-test/README.md
tags:
- README.md
---
# Uppdatera Metadatafiler som publiceras på dataportal.se

Alla metadatafiler som visas på dataportal.se finns i repot, https://gitlab.com/arbetsformedlingen/metadata

Om metadatafilerna behöver uppdateras, gör så här:
*	Öppna den metadatafil det gäller
*	Klicka på edit
*	Ändra det som skall ändras
*	Klicka på commit changes
*	Meddela Ulrika Hägqvist ( perui) eller Mats Löfstrand ( lofms) för att skapa och skapa den .rdf fil som behövs för att uppdatera dataportal.se  
Tanken framöver är att verktyget DCAT-AP-Processor automatiskt ska läsa av filerna i gitlab repot och därefter skapa upp .rdf filen och att den automatiskt laddas upp på AWS. Efter det kommer dataportal.se att ”skörda” filen och visa upp den på dataportal.se
