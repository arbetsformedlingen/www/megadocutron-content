---
title: Debug Pods in OpenShift
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//OpenShift/DebugPods.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /OpenShift/DebugPods.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/OpenShift/DebugPods.md
tags:
- OpenShift::DebugPods.md
- DebugPods.md
---
# Debug Pods in OpenShift

From these [instructions](https://blog.openshift.com/oc-command-newbies/)

1. Get login token from OpenShift GUI. Click your username and “Copy login command”
2. Run command in terminal
  ```
  oc projects
  oc project YOUR_PROJECT_NAME
  oc get pods
  # get a shell inside a pod for dc/jorge
  oc debug dc/jorge
  # as root user
  oc debug --as-root=true dc/jorge
  # or using the pod name
  od debug pod/<pod name>
  ```
