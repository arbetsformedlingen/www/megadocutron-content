---
title: On-Boarding for Taxonomy Dev
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//OnBoarding.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /OnBoarding.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/OnBoarding.md
tags:
- OnBoarding.md
---
# On-Boarding for Taxonomy Dev

Here is a list of things to get acquainted with.

Your guide will go through the following with you:

* Practical details in the daily work
  * Daily sync meeting at 9.30 (start videocall either with Android app Jitsi, or in a browser with this url: https://jitsi.jobtechdev.se/taxonomy-dev).
  * Biweekly sprint planning here on the issue board. 
  * Most communication is done using [Mattermost channel taxonomy-dev](https://mattermost.jobtechdev.se/batfish/channels/taxonomydev), the AF mail (your.name@arbetsformedlingen.se), [Jobtech Development forum](https://forum.jobtechdev.se/) for interaction with the community and arbetsformedlingen-it for communication with all of AF.
  * The teamwork is done decentralized
* Key tag to our office, the Castle – contact Carina Karlsson to get it – You can get into the Castle at any time of day, but evenings and weekends you need to use a code as well.
* Panorama is the place where a lot of AF services can be found http://start.arbetsformedlingen.se. You need to have VPN enabled.
  * Add the following to your favourites
    * Egenrapportering, report vacation, parental leave
    * Tajma, no time reporting necessary for employees
    * Måna, the benefits portal
    * ServiceNow
    * Vis
* Måna is the benefits system, here you can see your salary and get benefits such as friskvårdsersättning
  * Some things you can’t find here like the fact that you are allowed to exercise 1 hour on worktime per week. This you can find out about on Vis.
* ServiceNow is the system used by the service desk for incidents
* [Vis](http://start.arbetsformedlingen.se) is the AF information system, if you’re looking for general info about the agency, this is were you’ll look first.
* You are now a civil servant, that means you need to do a three day course in government stuff. But it’s only in swedish so you might be able to skip it. If you have to do it, there’s an online part first https://arbetsformedlingen.sabacloud.com/
* Travel expenses - just ask Carina when the time comes.
* How to book rooms: Tim at the Castle (https://www.thecastle.nu/sv/contact/) can create an account for you for the booking system: https://members.thecastle.nu/membership/dashboard
* Meetings
  * Get your guide to forward all the necessary meetings to your email
* The People: Team, personer, [kontaktuppgifter](https://af-ecosystem.atlassian.net/wiki/spaces/AMBE/pages/1876885538/Team+och+kontaktuppgifter)
* Accesses you’ll need:
  * [Gitlab](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend)   
    create a user and any team member can add it to the project
  * Openshift Jobtech AF Internal (you have to use vpn) - https://openshift.ams.se/console/catalog
    Ask Fredrik, Håkan or Jocke to create user
  * AF-internal confluence https://confluence.ams.se (request an account via mail: fixme@fixme) (reached using VPN)
  * AF: You need to set up a webpassword, this is done in ???
  * AWS
    Set up user in [AWS](https://942742542444.signin.aws.amazon.com/console)
  * Openshift Frankfurt Cluster (future develop and test cluster) - https://console-openshift-console.test.services.jtech.se/k8s/cluster/projects
    * Use http://ssp.jobtechdev.se/ to create user (ask guide for user/pass for the LDAP. If he/she cant remember it, ask Fredrik or Håkan)
  * Openshift Stockholm Cluster (future prod cluster) - https://console-openshift-console.prod.services.jtech.se/k8s/cluster/projects
    * Use http://ssp.jobtechdev.se/ to create user (ask guide for user/pass for the LDAP. If he/she cant remember it, ask Fredrik or Håkan)
* To access the internal AF network, you need an AF computer, either Mac or Windows. Mac stuff if applicable: https://confluence.ams.se/pages/viewpage.action?pageId=42176227 (use VPN to access).
* You will be issued a phone. To use it for reading work mail and accessing your work calendary, you need to install the Blackberry software on it. There is information about this on VIS.
* Security information
  * Keep your emplyee’s smart card card with you, as it contains credentials.
  * Lock your screen when you leave your computer.
* Jobtech Taxonomy - a new incarnation of AF’s taxonomy system.
  * The old system: problematic ID-codes, not updated for three years, confusing forks and copies.
  * The new system uses Datomic, and is in the process of switching to Datahike.
  * Try it out with Swagger: https://taxonomy.api.jobtechdev.se/v1/taxonomy/swagger-ui/
  * The web client [Atlas](https://atlas.jobtechdev.se/page/taxonomy.html)
* Clone the following important repositories:
  * [API](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api)
  * [DevOps](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-gitops)
  * [Taxonomy Common](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-common)
  * [Varnish Cache](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/openshift-varnish)
* Developer environment
  * Clojure, Datahike, AWS-cli. You probably won’t need the whole Datomic local installation, only the datomic-socks-proxy scripts - see this file for how to setup and execute the script:   - run with argument “prod-jobtech-taxonomy-db”, and use this for ~/.aws/config: [default]region = eu-west-1output = json
* Team development principles
  We use free and open source as much as possible.

## Interesting link treasure trove:

* https://mattermost.jobtechdev.se/batfish/channels/taxonomydev
* https://taxonomy.api.jobtechdev.se/
* https://atlas.jobtechdev.se/page/taxonomy.html
* https://elk.jobtechdev.se/
* https://grafana-grafana.opservices.jtech.se/
* https://connectstage.arbetsformedlingen.se/
* https://openshift.ams.se/console/
* https://pilot.arbetsformedlingen.se/beta/
* https://github.com/replikativ/datahike
* https://dhall-lang.org/
* https://github.com/kubernetes-sigs/kustomize
