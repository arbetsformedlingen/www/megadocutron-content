---
title: Seminars held
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//LifelongLearning/Seminars.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /LifelongLearning/Seminars.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/LifelongLearning/Seminars.md
tags:
- LifelongLearning::Seminars.md
- Seminars.md
---
# Seminars held

## Seminariepresentation om hur taxonomins användning - februari 2022
Inom ramen för regeringsuppdraget för kompetensförsörjning och livslångt lärande har det så kallade semantikprojektet påbörjats (länk behövs här). I ett första steg ska olika myndigheter inblandade i detta projekt presentera vilka data de har att dela med sig och vi vill där presentera hur taxonomin används inom AF och hur den kan användas av andra. Här är bilder till en sådan presentation.

[Slides](../presentations/20220216-kll-seminar/kll_presentation_taxonomin.odp)
