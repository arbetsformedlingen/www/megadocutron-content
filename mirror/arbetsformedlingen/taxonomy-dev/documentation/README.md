---
title: taxonomy-dev
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /README.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/README.md
tags:
- README.md
---
# taxonomy-dev

This is a collection of documents related to the work of the taxonomy-dev team.

## Daily and Weekly Tasks

See [Recurrent Tasks](RecurrentTasks.md) for details.

## Infrastructure

See [Infrastructure](Infrastructure.md) for details.

See [Torch with GPU](Tutorials/torch_with_gpu.md) for instructions on how to set up Torch with GPU support on e.g. Ubuntu.

## Meeting Notes

This table contains notes taken during meetings.

| Date | Title |
|-|-|
| 2021-02-01 | [KPIer](OtherMeetings/TB-KPIer-040322-1314.pdf) |
| 2021-11-25 | [Lifelong learning discussion](LifelongLearning/ideas.md) |
| 2022-02-03 | [Diskussion on mappning synonymordlistan och taxonomin](OtherMeetings/TB-2022-02-03-Diskussiononmappningsynonymordlistanochtaxonomin-040322-1307.pdf) |
| 2022-02-08 | [Diskussion med RISE om kompetensmatchning och taxonomin](OtherMeetings/TB-2022-02-08-DiskussionmedRISEomkompetensmatchningochtaxonomin-040322-1307.pdf) |
| 2022-02-20 | [Long term planning](LongTermPlanning/longtermplanning.org) |
| 2022-03-22 | [Taxonomy Mapping Tools Workshop](OtherMeetings/20220322-taxonomy-mapping-tools.md) |
| 2022-03-23 | [Konvertera annonser till version 1 av taxonomin](OtherMeetings/20220323-konvertera-annonser.md) |
| 2022-04-06 | [Workshop med RISE och MDU om önskat resultat kring SUSA-navsdata och kursinformation](OtherMeetings/susa_workshop.md) |
| 2022-04-12 | [Diskussion med MYH om kvalifikationsdatabasen](OtherMeetings/20220412-myhkval.md) |
| 2022-04-29 | [Discussion about SKOS export](OtherMeetings/20220429-skosdiscussion.md) |
| 2022-05-04 | [*Siggesta*: Mål och nytta (Objectives and utility)](OtherMeetings/20220504-siggesta-purpose.md) |
| 2022-05-04 | [*Siggesta*: Extern samverkan (External collaboration)](OtherMeetings/20220504-siggesta-external.md) |
| 2022-06-22 | [Allmän diskussion om annoteringsverktyget och begrepps definitioner](OtherDiscussions/20220622-taxonomy-annotation-discussion.md) |
| 2022-09-07 | [Meeting to outline a presentation of the taxonomy](OtherMeetings/20220902-taxonomy-presentation-outline/README.md) |
| 2022-09-27 | [Meeting to discuss onboarding of new employees](OtherMeetings/20220927-onboarding-discussion.md) |
| 2022-10-07 | [Meeting to discuss example GUI for CV based on the taxonomy](OtherMeetings/20221007-taxonomy-gui.odt) |
| 2022-10-21 | [Diskussion om att återuppliva `text2ssyk`](OtherMeetings/20221021-text2ssyk.md) |
| 2023-01-18 | [Diskutera AUB](OtherMeetings/20230118-diskutera-aub.md) |
| 2023-01-24 | [Diskussion om data.jobtechdev.se](OtherMeetings/20230124-djs-discussion/notes.md) |

## Projects

Here is a list completed and ongoing projects. Every project has a brief description that summarizes the purpose of the project, objectives and status. These descriptions can be used both internally within taxonomy-dev and be shared with others who want to understand what a project is about.

| Name                                                  | Description                                                                           | Status    |
|-------------------------------------------------------|---------------------------------------------------------------------------------------|-----------|
| [Concept Recogntion](projects/concept-recognition.md) | Detect taxonomy concepts mentioned in text.                                           | Ongoing   |
| [text2ssyk](projects/text2ssyk.md)                    | Classify jobads into occupation classes (SSYK).                                       | Ongoing   |
| [Datahike](projects/datahike.md)                      | Swap the database from Datomic to Datahike                                            | Ongoing   |
| [Publish on data.jobtechdev.se](projects/djs.md)      | Making the taxonomy available as downloadable files on data.jobtechdev.se             | Completed |
| [Taxonomy as software libraries](projects/libs.md)    | Producing software libraries for using the taxonomy in various programming languages. | Ongoing   |

## Areas of Interest

Here are specific pages about various areas of interest that we are working on.

* [Tools using natural language processing](projects/nlp/README.md)

## Presentations

| Date | Title | Slideshow | Text | Description |
|-|-|-|-|-|
| 2021-11-05 | Unit day | [nlp unitday.pdf](presentations/20211105-unitday/nlp unitday.pdf) | | Presentation about NLP on the unit day |
| 2022-02-16 | Seminarium KLL | [kll_presentation_taxonomin.pdf](presentations/20220216-kll-seminar/kll_presentation_taxonomin.pdf) | [text.org](presentations/20220216-kll-seminar/text.org) | Presentation for other authorities about how the taxonomy is used within Arbetsförmedlingen and how it can be consumed. |
| 2022-02-18 | Demodag | [pres.pdf](presentations/20220218-demoday/pres.pdf) | [text.org](presentations/20220218-demoday/text.org) | [We demonstrated](projects/nlp/logs.md#log-2022-02-18-demo-day) applications of SBERT to annotating text and mapping concept structures. |
| 2022-03-22 | Taxonomy mapping tools | [pres.pdf](presentations/20220322-mapping-tools-workshop/pres.pdf). See also [Redaktionens suggestion](presentations/20220322-mapping-tools-workshop/redaktionen_suggestion.pdf). | | We discuss the development of a tool to facilitate mapping taxonomy concepts to other concepts, such as concepts from ESCO. Redaktionen presents their ideas. |
| 2022-05-12 | Taxonomy working group Europe | [pres.pdf](presentations/20220512-tax-europe/pres.pdf) |
| 2022-09-16 | JobTech Taxonomy | [Part 1](presentations/20220916-taxonomy-dev/Jobtech_Taxonomy_Presentation_2022.odp) and [Part 2](presentations/20220916-taxonomy-dev-part2/concept_recognition/pres.odp) | | General presentation about the taxonomy: What it is, what it is used for, related projects. |
| 2022-09-16 | Begreppsigenkänning | [pres.odp](presentations/20220916-taxpres/concept_recognition/pres.odp) | [pres.md](presentations/20220916-taxpres/concept_recognition/pres.md) | Presentation about what we are doing in the field of concept recognition. |
| 2022-12-20 | conrec-utils walktrough | [pres.odp](presentations/20221220-conrec-utils-walkthrough/pres.odp) | [talk.md](presentations/20221220-conrec-utils-walkthrough/talk.md), [code examples](presentations/20221220-conrec-utils-walkthrough/code.md) and [**video recording**](https://data.jobtechdev.se/videos/221220-taxonomy-dev-instruktionsfilm-conrec-utils-walkthrough-1920x1080.mp4) | **Tutorial** on how to use the [conrec-utils](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils) Python library. |
| 2023-03-01 | taxonomy-mapping-tools walkthrough | [pres.odp](presentations/20230301-taxonomy-mapping-tools-walkthrough/pres.odp) | [manus.md](presentations/20230301-taxonomy-mapping-tools-walkthrough/manus.md), [**video recording**](https://data.jobtechdev.se/videos/230301-taxonomy-dev-instruktionsfilm-taxonomy-mapping-tools-walkthrough-1920x1080.mp4) | **Tutorial** on how to use the [taxonomy-mapping-tools](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-mapping-tools) library. |

## Reports

* [ClojureD 2022](Reports/clojured2022.md)

## Exporting documentation to other formats

1. Install Pandoc
2. `cd utils`
3. `make docx`

## Onboarding

See [this page](OnBoarding.md) for detailed instructions.

See [this page](SummerworkerOnboarding.md) for instructions on how to onboard summerworkers.
