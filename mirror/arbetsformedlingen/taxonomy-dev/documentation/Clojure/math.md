---
title: Doing math in Clojure
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//Clojure/math.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /Clojure/math.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/Clojure/math.md
tags:
- Clojure::math.md
- math.md
---
# Doing math in Clojure


## Libraries

Here are some libraries that seem interesting.

### EJML

A linear algebra library written in Java targeting the JVM.

[http://ejml.org/wiki/index.php?title=Main_Page](http://ejml.org/wiki/index.php?title=Main_Page)

### libpython-clj

Python interop for Clojure. Allows the use of Python libraries in Clojure code

* [https://github.com/clj-python/libpython-clj](https://github.com/clj-python/libpython-clj)

#### Usage examples

* [https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/test-clojure-python-bert-api](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/test-clojure-python-bert-api)

### MXNet

Clojure interop layer targeting MXNet.

MXNet seems to have a relationship with [tvm-clj](#tvm-clj)

* [https://mxnet.apache.org/versions/1.9.1/api/clojure](https://mxnet.apache.org/versions/1.9.1/api/clojure)
* [https://github.com/apache/mxnet](https://github.com/apache/mxnet)

### Neanderthal

Clojure interop layer targeting native CPU or GPU libraries.

[https://neanderthal.uncomplicate.org/](https://neanderthal.uncomplicate.org/)

### tvm-clj

Clojure interop layer targeting [tvm](https://github.com/apache/tvm). The tvm system compiles to native code on various platforms.

[https://github.com/techascent/tvm-clj](https://github.com/techascent/tvm-clj)
