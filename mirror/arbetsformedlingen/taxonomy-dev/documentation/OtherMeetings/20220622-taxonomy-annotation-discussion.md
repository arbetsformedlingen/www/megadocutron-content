---
title: Allmän diskussion om taxonomin och annoteringsverktyget
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//OtherMeetings/20220622-taxonomy-annotation-discussion.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /OtherMeetings/20220622-taxonomy-annotation-discussion.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20220622-taxonomy-annotation-discussion.md
tags:
- OtherMeetings::20220622-taxonomy-annotation-discussion.md
- 20220622-taxonomy-annotation-discussion.md
---
# Allmän diskussion om taxonomin och annoteringsverktyget

## Närvarande

* Björn Alvinge
* David Norman
* Jonas Östlund

## Diskussion

Diskussion började i samband med att vi gick igenom hur man annoterar platsannonser med hjälp av annoteringsverktyget (http://jobtech-annotation-editor.jobtechdev.se/): När man lägger till en ny annotering (knappen `New annotation`) så kan man välja mellan alternativen *"Krav"*, *"Meriterande"*, *"Erbjudande"* och *"Inte relevant"*. Tanken är att "sentiment" ska ange begreppets *sammanhang* eller vilken funktion det annoterade begreppet fyller i meningen: Med "Krav" menas troligtvis att det annoterade begreppet är en kompetens som en arbetsgivare kräver av en arbetstagare som en förutsättning för att få jobbet. Men det behöver förtydligas vad innebörden av de olika "sentimenten" är. Det vore också bra att ha en djupare diskussion kring vilka andra så sentiment som behövs.

I den här diskussionen berördes också om det kan behövs begrepp för *arbetsuppgifter* i taxonomin. I platsannonser står det ofta något i stil med att "du kommer att arbeta med ...". Kan dessa arbetsuppgifter fångas med begrepp i taxonomin? I dagsläget har vi i första hand "yrkesbenämningar" och "kompetenser" men det är inte samma sak som arbetsuppgifter, även om det är ganska närliggande.

För många av begreppen i taxonomin finns det i dagsläget ingen definition eller beskrivning. Frågan är: hur bestämmer man ett begrepps definition? Rimligtvis bör en lämplig definition bestämmas utifrån taxonomins ändamål eller det problem som man vill lösa med hjälp av taxonomin. I dagsläget är ett viktigt ändamål att koda innehållet i platsannonser så att dessa kan organiseras, inordnas i en informationsstruktur och göras sökbara, eller användas för statistiska ändamål. Begreppen i taxonomin är i stor utsträckning färgade av *arbetsgivarens* perspektiv eftersom det är arbetsgivaren som skriver de platsannonser som taxomin är byggd för att koda. Men om vi även vill använda taxonomin för matchningsändamål för vi också få med *arbetstagarens* perspektiv. Det är nämligen så att även om två personer använder samma begrepp och inbillar sig att de använder samma språk, kan begreppet ha olika betydelse för dem. Hur ska man hantera denna osäkerhet i ett begrepps betydelse när man bygger en taxonomi? Hur gör man när man bygger ESCO?
