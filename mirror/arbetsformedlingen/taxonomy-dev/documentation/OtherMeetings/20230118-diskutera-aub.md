---
title: Diskutera AUB
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//OtherMeetings/20230118-diskutera-aub.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /OtherMeetings/20230118-diskutera-aub.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20230118-diskutera-aub.md
tags:
- OtherMeetings::20230118-diskutera-aub.md
- 20230118-diskutera-aub.md
---
# Diskutera AUB

**Petter** needs help with associating concepts with labour market training programmes.

**Petter**: Arbetsförmedlingen offers these training programmes to job seekers. Each training programme concists of modules and every module has a description. And we want to map the content of the description to skill concepts in the taxonomy.

## Description of the data

Let me show you.

Take for instance the module "Ritteknik" which is part of arbetsmarknadsutbildningar.
The modules are sorted alphabetically by name of module.
We have help from three persons full time.

## How to annotate

**In short:** Every module should be associated with some taxonomy skills that encode the contents of the module.

There is a module with description "Senaste programvaran inom Adobe photoshop". That module could be connected with "2D-grafik-Adobe Photoshop" concept. The connection is approximate, it is good enough with a skill that roughly matches the contents in the module.

If we look at "Arbetsplatsförlagt lärande (APL)" with id **22838** I would prefer "Videoredigering" to be mapped.

```
Id:              QBAx_kfV_k7Q
Preferred label: Videoredigering
Type:            Kompetensbegrepp <TYPE skill>
```

Each module also has a link to SSYK-nivå-4, that could be used for guidance.

We have persons doing this manually but they don't see the latest version in the Atlas.


## What to do

* Speed up the process by producing good candidate concepts for each module.
* Use the latest (unpublished) version of the taxonomy, the one in prod-editor.
* The job should be done by May 2023.

To sum up: We want to generate good candidate concepts for each module.
