---
title: 2022-03-22 - Taxonomy Mapping Tools Workshop
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//OtherMeetings/20220322-taxonomy-mapping-tools.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /OtherMeetings/20220322-taxonomy-mapping-tools.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20220322-taxonomy-mapping-tools.md
tags:
- OtherMeetings::20220322-taxonomy-mapping-tools.md
- 20220322-taxonomy-mapping-tools.md
---
# 2022-03-22 - Taxonomy Mapping Tools Workshop

## Participants

* Sruthi Annapureddy
* Sara Binazir
* Jonas Claesson
* Jon Findahl
* Christer Niklasson
* David Norman
* Petter Nygård
* Jonas Nyman
* Tomas Svärd Hörger
* Henrik Suzuki
* Jonas Östlund

## What we discussed

The purpose of this meeting was to discuss the development of a tool to help Redaktionen map concepts between different ontologies, e.g. JobTech Taxonomy and ESCO.

Jonas Östlund presented [a few slides](../presentations/20220322-mapping-tools-workshop/pres.pdf) to start the discussion.

Jonas Claesson added that it would be useful if the tool that we develop *harvests* user interactions that could potentially be used to train machine learning algorithms.

Petter presented some ideas that he had, see [this document](../presentations/20220322-mapping-tools-workshop/redaktionen_suggestion.pdf). Currently, there is a way of importing concepts in the taxonomy editor. In short, Petter suggested that we extend that functionality with Sentence-BERT recommendations.

Jonas Östlund asked more about the typical use cases for mapping concepts. Petter explained two scenarios:

* A new version of ESCO is released. In that case, there can be thousands of new concepts that need to be mapped against concepts in the taxonomy.
* A new JobTech Taxonomy specific concept is added, and we want to map it to the ESCO concepts.

Petter also mentioned that before a new taxonomy concept is added, it would be good to display similar taxonomy concepts in order to avoid adding duplicates or concepts that are very similar. Suppose for example that we consider adding `SQL-utvecklare` to the taxonomy. Such a tool would detect that there is already the concept `Software developer` and as a consequence, we might decide to add the *skill* `SQL` instead to be combined with `Software developer`.

We also discussed how the tool should be built. Petter suggested that something that is not necessarily tied to the editor would be useful. That way, his European colleagues could potentially use it too. But because all our code is public and open source, they can always see the implementation even if we would choose not to go that route. One idea would be to implement it as some sort of web service that the taxonomy editor can use, e.g. through an HTTP API.

Jonas Nyman pointed out that it would be best to start with a narrow specific use case, but also the importance of having this functionality integrated in the editor so that the user (Redaktionen) does not have to switch between multiple tools to accomplish a mapping task. To follow up on this, Jonas Nyman, Tomas Hörger and Redaktionen will meet to discuss more specifically how the editor can be modified to better support the mapping of concepts.

Henrik clarified that even if all the work is done from within the taxonomy editor, implementation wise there can still be a separate web service to come up with the suggestions of how concepts should be mapped.

## Next Steps

* The frontend team (Jonas Nyman and Tomas Hörger) will meet with Redaktionen (Petter) to discuss further how the editor could be extended to facilitate mapping concepts.
* Petter will show Jonas an idea about how to take more complex relations between concepts into account when, for example, mapping ESCO skills. It is the case that ESCO occupations have required and optional skills. That knowledge could be taken into account.
