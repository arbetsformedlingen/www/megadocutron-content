---
title: 2022-11-10---Team agreement **taxonomy-dev** (revised)
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//OtherMeetings/20221110-revised-team-agreement.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /OtherMeetings/20221110-revised-team-agreement.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20221110-revised-team-agreement.md
tags:
- OtherMeetings::20221110-revised-team-agreement.md
- 20221110-revised-team-agreement.md
---
# 2022-11-10---Team agreement **taxonomy-dev** (revised)

## About this document

Any edits to this document require pull (merge) requests.
We should review this document every now and then.

## Our Vision

Everyone should use JobTech Taxonomy so that they can communicate with each other. By "everyone", we mean people who build computer systems related to the labor market. In other words, "a common language for the labor market".

## Our Mission

* Provide the users of the labor market ecosystem with the technology for a common language that can be used to describe their data. The language enables them to communicate with each other and make their data interoperable.
* We have tools that help us elevate non-processed (natural) language into the taxonomy. So that we can add a machine-readable semantic layer to that language.
* Provide the database technology and service for storing the taxonomy and making it accessible for those who want to use it.
* Provide and develop technology for maintaing the taxonomy: e.g. an editor to add, modify and remove concepts and relations.
* Provide and develop technology for consuming the taxonomy: e.g. the Atlas, services that publish the taxonomy in common file formats, services that recognize concepts in free text.

## Our Goal

* The JobTech taxonomy has good quality and widespread adoption.
* Build a system that can detect occurrences of taxonomy concepts in natural text.
* Strive for systems that can compose with other systems.
* Usability for developers and consumers of our systems among the public and among labor market actors.
* Keep the taxonomy up-to-date with today's labor market and provide the necessary tools to make that happen, by for instance facilitating the job of Redaktionen.

## Our Values

* **Knowledge sharing**: so that we can distribute work. So that we develop each other, and grow intellectually. So that we become more productive together.
* **Utility**: What we do should matter. It should provide value.
* **Courage**: We have the courage to try even things that we are not sure will work. But keep in mind that by courage, we don't mean courage in the sense of putting a poorly tested software in production.
* **Work-life balance**: We respect the boundary between personal and professional life. Working overtime is not the norm but can *exceptionally* be tolerated. Working overtime is an indication of poor planning that needs to be addressed.
* **Sustainability**: We consider the long-term impact of what we do and why.
* **Pragmatism**: Doing what works.

## How we treat each other

* We are helpful: We challenge each other and help each other to succeed (from [1]).
* We encourage each other speak out and try not to overtalk.
* We try to respect each other.
* We try to be honest.
* We are open and curious (from [1]).
* We trust each other and take responsibility for the big picture (from [1]).

[1] Team agreement – Servant Leadership Team.

## Decisions

* We talk every morning and inform each other about what we are doing. This is an opportunity to bring up decisions to be made.
* We trust members of the team to take decisions by themselves if the decisions align with the overall design.
* We discuss things thorougly before and after (hard) decisions. We follow up decisions.

## Documentation

* We have a *public documentation repository* here: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation
* Every software component in production should be sufficiently documented so that any member of our team or Calamari can take care of all systems by themself if need be.
* It would be good if our documentation repository could have a list of projects that are in use, and why.
* It is good to have a readme file for every project that we make, with at least the following:
  - What the purpose is
  - Example usage
  - What to install to get it running locally
  - What commands can be invoked to run it
  - License
* We should strive for having a podman/docker-file for every project for the sake of reproducibility. The docker file can be used to reproduce the development environment as well.
* We keep the documentation up-to-date with the software, if possible using some form of automation.

### Technology

* We make code reviews when adding code to Git repositories of production code.
* We write automated tests, in some form, for every project.
* We prefer open source software.
* We strive for containerizable software.

## Internal communication

* Use the *taxonomy-dev* channel for talking about *our work* and things that other colleagues can be interested in.
* We keep [meeting notes](https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation#meeting-notes) for important meetings in our documentation repository so that we can remember what was said and other colleagues who did not attend the meeting can read what was said during the meeting.
* We share [slide show presentations](https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation#presentations) in our repository so that we can reuse it later.
* When there is overlap between projects we communicate.

## Meetings

* For every hour of meeting, we need a pause of around 10 minutes.
* We encourage each other, when it feels useful, to take part in meetings with others from outside of our team.
* It is acceptable to reject meetings that are not useful, e.g. when a meeting is organized in order to ask for something that could be asked in one sentence. It is also acceptable to drop (sneak) out, in a respectful way, from a meeting that is not useful.
* We strive to collect tangible evidence that a meeting was useful, e.g. (actionable) meeting notes.
* We have a stand-up meeting 9:30 every morning where we look at the kanban board and open merge requests.
* We have a retrospective every two weeks on Thursday.
* We have sprint planning every two weeks on Tuesday.
* We are planning to have a social programming meeting every Wednesday in the afternoon.

## Roles

**System developer role**: Everyone in the team fulfills this role.
**Note keeping during meetings role**: Anyone can take this role.
**Scrum master role**: Anyone can take this role.

Different roles require different skills. We do not have all the skills in our team, e.g. UX or security. But there are experts in these domains within JobTech. There may be missing roles in the list above because the skills required for those roles are not present in our team.
