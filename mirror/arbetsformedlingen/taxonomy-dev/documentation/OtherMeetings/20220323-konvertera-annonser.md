---
title: '2022-03-23 - Minnesanteckningar: Konvertera annonser till version 1 av taxonomin'
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//OtherMeetings/20220323-konvertera-annonser.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /OtherMeetings/20220323-konvertera-annonser.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20220323-konvertera-annonser.md
tags:
- OtherMeetings::20220323-konvertera-annonser.md
- 20220323-konvertera-annonser.md
---
# 2022-03-23 - Minnesanteckningar: Konvertera annonser till version 1 av taxonomin

## Närvarande

* Henrik Suzuki
* David Norman
* Jonas Östlund
* Patrik Olsson 
* Pierre Ahlby

## Vad som sades

**Patrik**: Vi sitter både med platsbanken och annonser
* AIS
* Självservice
* Direktöverförda annonser
Vi har gått över till begreppsid:n. Men vi är fortfarande kvar på version 1 av taxonomin (8 eller 9 år gammal). Om vi går upp på en ny version av taxonomin ställer det krav på att andra bakomliggande system kan hantera den versionen. AIS är det stora problemet. JobSearch och AI-center har fixat det redan. Det är AIS som är problemet.

Det kanske går att nedkonvertera annonser till version 1 för att hålla bakåtkompatibilitet.
Det är bakgrunden till mötet.

**Henrik**: Bra sammanfattat! Hur vill ni ha det rent tekniskt?

**Patrik**: Vi har en kö där vi skickar ut annonser och ett publikt interface för att hämta annonser. Vi behöver göra nedkonverteringen. Antingen vid tillfället när annonsen skickas ut. Redan idag lägger vi på taxonomiinformation. Kanske en flagga: “jag vill ha ut det här i version 1”. Vi kanske kan ringa in det till stället där annonserna hämtar det här. Någon slags uppslagstjänst.

Idag slår vi upp hos er vad koncept-id:t heter. “Det ska stå “slöjdlärare””. Eller när det kommer in: man sparar i databasen ett kompatibilitets-id.

**Patrik**: Vi behöver konkreta exempel. Till exempel om någon lägger in “Drönarpilot”, finns det något liknande äldre yrke?

**David**: Det finns 236 nya yrken.

## Vad ska göras?

1. Taxonomy-dev tar fram lite exempel
2. Sedan drar Patrik det med Jenny
3. Jenny kollar när det kan göras tidsmässigt
4. Patrik återkommer för att vi upprättar en plan.

Mer specifikt: De exempel som åsyftas är yrken (occupation-name) som har tillkommit efter version 1. Hur ska de mappas mot version 1? Ska vi ta ett likartat eller bredare yrke, eller inget yrke alls?
