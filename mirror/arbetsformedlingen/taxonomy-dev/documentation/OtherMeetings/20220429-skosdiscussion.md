---
title: Discussion with Metasolutions/DIGG about SKOS
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//OtherMeetings/20220429-skosdiscussion.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /OtherMeetings/20220429-skosdiscussion.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20220429-skosdiscussion.md
tags:
- OtherMeetings::20220429-skosdiscussion.md
- 20220429-skosdiscussion.md
---
# Discussion with Metasolutions/DIGG about SKOS

Regarding exporting the Taxonomy to SKOS:
* Concept *types* in the taxonomy are represented as *scheme* in SKOS. Every concept type gets defined as a scheme.
* Every *scheme* has a set of top concepts. A top concept is a concept that does not have a broader concept.
* Don't use `rdfs:label` for scheme, use `dcterms:title` instead. A `dcterms:decription` would be great as well.
* Use broadMatch to reference across different schemes.
* Use language tags with preferred label, etc.
* Use one file per scheme.
* Visit editera.dataportal.se
