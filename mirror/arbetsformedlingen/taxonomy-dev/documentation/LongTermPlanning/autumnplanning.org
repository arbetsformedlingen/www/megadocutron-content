* Autumn planning

* Vision: Everyone should use the Taxonomy because they can communicate with eachother

* Major Goals

** Match jobseekers with employers and educations

If a jobseeker lacks a skill we should be able to recommend an education


*** We need jobseeker data with taxonomy concepts

Help jobseekers to fill out CV with concepts

*** We need jobpostings with taxonomy concepts

Help employers to fill out job posting with concepts

Automatically tag job postings with taxonomy-concepts

*** We need educations with taxonomy concepts

Help education providers to fill out education description with concepts

Automatically tag education  with taxonomy-concepts

** Abilities

*** Convert text (/cv/ad/education) to concepts

*** Recommend concepts given concepts

*** Recommend concepts given string

*** Search for concepts in the taxonomy fast (typeahead)

*** Store and expose concepts (Taxonomy Database & API)


** Products

*** Taxonomy API

**** Use Datahike in production

**** Help AF to connect to the API and synchronize

*** Mentor API

**** Recommend concept given a string
Given a text string recommend a concept.

Examples: mapping esco to jobtech taxonomy

Idea: Create mapping tools to map different Taxonomies.
Use case: Link data on the dataportal

**** Recognize concepts in a text (concept recognition)
Show where in the text the concepts appear
Token classification. Mapping tokens to
**** Autocomplete (Fast concept lookup)

*** Annotation Editor

**** Create data for test/train AI

*** Joblinks

**** Create pipeline job that adds taxonomy concepts to jobads
