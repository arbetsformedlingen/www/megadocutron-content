---
title: Policy questions
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//TaxonomiInförandet/meeting_2020-03-26.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /TaxonomiInförandet/meeting_2020-03-26.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/TaxonomiInförandet/meeting_2020-03-26.md
tags:
- TaxonomiInförandet::meeting_2020-03-26.md
- meeting_2020-03-26.md
---
# Policy questions

Meeting regarding the integration process with AF concerning the Taxonomy.

## Can we safely remove the relations structure from responses, as they require?

Vlad: Can they ignore data they don't care about?

Per: It seems harsh to remove them, as it will stop other clients from getting that data.

## How should we respond to their request for old taxonomy IDs included in the responses?

### Background about the usage of old IDs in phase one of the integration process
I’ve got the impression that they have a lot of interconnected services, so migrating to the new taxonomy will require them to atomically update every service at once. If they will have both new ID and old ID on the same concept, it will allow them to update these services in isolation one by one, where migration path for every service will look like this:
1. Add support for the new taxonomy, were internally their IDs are our new IDs: at this point, the service in question effectively has 2 APIs: new and old
2. Wait until other services this service talks to support new API
3. Switch to the new API and turn off the old one.

It’s more migration work overall, but every chunk of this work is smaller in scope, less fragile and easier to grasp.

### Our recommendation about how they should manage the old IDs
Problem: integation of a new API to AF needs to have mappings from old IDs to new IDs to keep their integrations with other systems running

Twist #1: old IDs' uniqueness is per `type` only.

Twist #2: old IDs were recycled, so they can mean different things in different systems, depending on how updated they are.

| Solution | Cons | Pros |
| - | - | - |
| Include old IDs in the main/specific endpoints | Only relevant internally, might confuse public users. Can be hidden under deprecated `include-legacy-information` flag that documents semantics of the old IDs | Allows AF to load everything they need in a single request |
| Provide old <-> new ID mapping using legacy endpoints | If these endpoints only return new ID by old ID, requires 2 http requests for every entity to load interesting information. If they return labels etc, they still require 1 http request for every entity | Internal and external APIs are separated |
| Provide old <-> new ID mapping using [json](https://github.com/JobtechSwe/jobtech-taxonomy-database/tree/master/resources) files | Introduces a dependency on our internal files we use for migrations | Does not affect our APIs at all |

Vlad's opinion:
I would recommend 1st option (include old IDs), because:
- 1st option (include old IDs) is AF-friendly, while a little bit inconvenient to public users.
- 2nd option (legacy endpoints) is sort of a middle ground, but it’s still inconvenient to AF (they want to load all entities with both new and old ids, which will be very slow) while adding the same level of inconvenience to public users (they still see that there are legacy endpoints).
- 3rd one (json) adds no inconvenience for public users, but adds more burden on us than other options, and seems to be inconvenient to AF since they have to use 2 different APIs and post-process the data.

## Should we add specific endpoints for all types, even the ones without specific attributes, as they require?

Vlad: Aren't types dynamic? So we can just assert new types whenever we want? Doesn't that make specific endpoint for every type impossible?

Per: True. But for customer AF, there is a finite set. So if we add specific endpoints tailored for AF, we can make it for their types. It is basically all types in the taxonomy as it looks right now.

## Graph capabilities of taxonomy API

Problem: our data is a graph, but graph query capabilities are hard to use:
- hard to query for relations of particular concept: source type is required, so looking for a single concept requires loading a lot of unnecessary data and post-processing it
- hard to query for all types of relationships for a given concept: target type is required, so looking for both narrower and broader relations requires 2 requests
- hard to fetch multiple levels of data: returns only one level of depth at a time, so building a tree of concept -> narrower of this concept -> narrower of these concepts requires multiple requests and non-trivial post-processing.

Possible solutions:
- not doing anything since graph endpoint allows to reach for all relationship information
- extend graph endpoint to not require relation or target type, as well as allow it to specify source concept ids instead of source type
- use GraphQL to allow all of the above plus returning multiple levels of relations

# Candidates for new Gitlab issues

## More specific version of /specific/concepts/region
Vladimir requests a new search parameter (or more specific endpoint) to narrow down results to Sweden.

## Add new-to-old-ID-conversion endpoints that return mappings for all codes at once
Ahmad's request

## Add specific endpoint for employment-duration, that includes EURESCode in the answer.

# Sorting out old and new terminology

THIS TABLE IS DEPRECATED - and incomplete, and possibly wrong. Check David Norman's diagram instead. It can be found on the project confluence.

| Old | New | Svensk benämning | Comment |
| - | - | - | - |
| OccupationName | occupation-name | | |
| LocaleField | occupation-field | | |
| LocaleGroup | ssyk-level-4 | | |
| LocaleCode | ssyk-level-4 | | Is called LocaleCode, at least in the response from GetAllOccupationNames. Needs verification. |
| LocaleLevel3ID | ssyk-level-3 | | |
| LocaleLevel3Code | n/a | | Found this in the response of at least GetAllLocaleLevel3 - don't know what it means. |

Don't use occupation-group.

# Specific questions from Ahmad

Ahmad has sent us a list of the endpoints his system uses in the old taxonomy system.

## 1- Kulturyrken:
```
Endppoint:	GetOccupationNamesArt
Params:		languageId: string
Response:	[
	{"OccupationName": { OccupationNameID: string, Term: string, LocaleCode: string }}
]
```
David: Historically we have flagged certain occupations as "Kulturyrken" since Arbetsförmedlingen handles these differently within the organisation. When such an occupation is registered by a job seeker they get a notification saying that they should contact "Af kultur media" or something along those lines.

Early last year (2019) we made the decision **not ** to incorporate this information in the taxonomy database. The reasoning was that the information is uniquely tied to business logic within Arbetsförmedlingen. The decision was made together with IT and was informed about. Af Kultur Media, who are the owners of the information, are also aware of this. 

Solution: Kulturyrken are deprecated.

## 2- Yrken
```
Endppoint:	GetAllOccupationNames
Params:		languageId: string
Response:	[
	{"OccupationName": { OccupationNameID: string, Term: string, LocaleCode: string }}
]
```

Solution: /v1/taxonomy/main/graph?edge-relation-type=broader&source-concept-type=occupation-name&target-concept-type=ssyk-level-4

## 3- lista med yrkesgrupper
```
Endppoint:	GetAllLocaleGroups 
Params:		languageId: string
Response:	[
	{"LocaleGroup": { LocaleGroupID: string, LocaleCode: string, Term: string, Description: string, LocaleFieldID: string, LocaleLevel3Code: string }}
]
```

David: My guess is that it is ssyk-level-4 he is looking for. "Yrkesgrupp" is, for the most part, Af-lingo for level 4. Here's what i think:

 - LocaleGroupID:       Per: probably the old taxonomy ID for "yrkesgrupp".  David: this is my guess as well
 - LocaleCode:          Per: the taxonomy code for "yrkesgrupp". David: yes which would be the SSYK-level-4 code
 - LocaleLevel3Code:    Per: another taxonomy code. David: presumably the SSYK-level-3-code (not entirely sure why they would use this in inskrivningen though)
 - LocaleFieldID:       Per: a foreign key it seems - is locale field the same as "occupation-field"?  David: yes, at least I cannot think of anything else. They use the relation ssyk-level-4 <-> occupation-field to sort the groups in their interface.

It is possible that they send the ssyk-level-3 to AIS which then sends it to datalagret (if my understanding of the architecture is correct) since that level is mainly used by statisticians.

Status: we need to look a bit closer on this, to understand what the SOAP service returns.

## 4-  Relaterade yrken
```
Endppoint:	GetOccupationNamesByLocaleCode 
Params:		languageId: string, localeCode: string
Response:	[
	{"OccupationName": { OccupationNameID: string, Term: string, LocaleCode: string }}
]
```

LocaleCode Kan skippas eftersom den har kommit in som param.

Partial solution: `http://jobtech-taxonomy-api-develop-jobtech-taxonomy-api.test.services.jtech.se/v1/taxonomy/main/concepts?related-ids=PVgB_Ra8_DE8&relation=related`

That will return all concepts (of any type) that are related to the given ID.

To get only related occupations, a new legacy endpoint has to be created.

## 5-lista varaktigheter
```
Endppoint: GetAllEmploymentDurations
Params: languageId: string
Response: [
	{"EmploymentDuration": { EmploymentDurationID: string, Term: string, LocaleCode: string, EURESCode: string, SortOrder: string }}
]
```

SortOrder Optional och kan sparas lokalt.

Per: LocaleCode is not part of the answer from the SOAP service GetAllEmploymentDurations.

Partial solution: http://jobtech-taxonomy-api-develop-jobtech-taxonomy-api.test.services.jtech.se/v1/taxonomy/main/concepts?type=employment-duration

To get the EURESCode, we need to add a specific endpoint for employment-duration.

## 6-lista Arbetssituationer
```
Endppoint: GetAllJobSituations
Params: languageId: string
Response: [
	{"JobSituation": { JobSituation_AISID: string, JobSituation_AIS: string, Term: string, SortOrder: string}}
]
```
David: Not entirely sure what this is. I would assume it has found its way to the database through some side channel (as in: not something that has been handled through the taxonomy editor). The names seem to indicate this as well. Since we have not seen this list before it has unfortunately never been discussed.

This is what a JobSituation looks like in the response:
```
            <JobSituation>
               <JobSituation_AISID>4</JobSituation_AISID>
               <JobSituation_AIS>22</JobSituation_AIS>
               <Term>Timanställd</Term>
               <SortOrder>4</SortOrder>
            </JobSituation>
```

Judging from the names, can it be some AIS-specific thing?

Solution: unclear source, not this taxonomy.

## 7- lista med sökområden
```
Endppoint: GetEURegionsByCountryID
Params: languageId: string, countryId: string
Response: [
	{"EURegion": { EURegionID: string, Term: string, NationalNUTSLevel3Code: string}}
]
```

Per: when I compare the NUTS-codes in the old and new system, they look different. For example Kronoberg:
```
Old:
               <NationalNUTSLevel3Code>07</NationalNUTSLevel3Code>

New:
    "taxonomy/nuts-level-3-code-2013": "SE231",
    "taxonomy/national-nuts-level-3-code-2019": "13"
```

Solution: http://jobtech-taxonomy-api-develop-jobtech-taxonomy-api.test.services.jtech.se/v1/taxonomy/specific/concepts/region?related-ids=i46j_HmG_v64&relation=narrower

## 8- alla kommuner
```
Endppoint: GetAllMunicipalities
Params: languageId: string
Response: [
	{"Municipality": { MunicipalityID: string, Term: string, NationalNUTSLAU2Code: string, NationalNUTSLevel3Code: string}}
]
```

The SOAP-service seems to have an implicit restriction to Sweden, so we cannot simply list all municipalities from the new taxonomy with a type-query, as that would return all municipalities.

Solution: broken up in multiple queries:

FOR EACH region-id IN list-of-swedish-regions (`http://jobtech-taxonomy-api-develop-jobtech-taxonomy-api.test.services.jtech.se/v1/taxonomy/specific/concepts/region?related-ids=i46j_HmG_v64&relation=narrower`):
  - GET LIST OF MUNICIPALITIES FOR region-id (`http://jobtech-taxonomy-api-develop-jobtech-taxonomy-api.test.services.jtech.se/v1/taxonomy/specific/concepts/municipality?related-ids=wjee_qH2_yb6&relation=narrower`)

To get the result in one query, a new legacy endpoint has to be created.

## 9- lista med yrkesområden
```
Endppoint: GetAllLocaleFields
Params: languageId: string
Response: [
	{"LocaleField": { LocaleFieldID: string, Term: string, Description: string}}
]
```

Solution: http://jobtech-taxonomy-api-develop-jobtech-taxonomy-api.test.services.jtech.se/v1/taxonomy/main/concepts?type=occupation-field

## 10- lista med kompetenser
```
Endppoint: GetSkillsByLocaleCode
Params: languageId: string, localCode: string
Response: [
	{"Skill": { SkillHeadline: { SkillMainHeadline: {SkillMainHeadlineID: string, Term: string} }, Term: string, SkillID: string}}
]
```

Not all occupation <-> skill relations are yet migrated to the new taxonomy (comparing /v1/taxonomy/main/concepts?related-ids=qVHD_Hj5_PXh&type=skill&relation=related with GetSkillsByLocaleCode 1111).

Solution: http://jobtech-taxonomy-api-develop-jobtech-taxonomy-api.test.services.jtech.se/v1/taxonomy/main/concepts?related-ids=qVHD_Hj5_PXh&type=skill&relation=related

## 11- lista med kompetenser
```
Endppoint: GetAllSkills
Params: languageId: string 
Response: [
	{"Skill": { SkillHeadline: { SkillMainHeadline: {SkillMainHeadlineID: string, Term: string} }, Term: string, SkillID: string}}
]
```

Solution: http://jobtech-taxonomy-api-develop-jobtech-taxonomy-api.test.services.jtech.se/v1/taxonomy/main/graph?edge-relation-type=narrower&source-concept-type=skill-headline&target-concept-type=skill&version=1

## 12- svenska utbildningsnivåkoder
```
Endppoint:GetSUNGuideTree
Params: languageId: string 
Response: [
	{"SUNGuideBranch": { SUNLevel1: { SUNLevel1ID: string, SUNLevel1Code: string, Term: string }}}
]
```

The SOAP-service returns a full tree. In the new taxonomy, you retrieve one level per request:

Partial solution: Get the tree in steps, one for each SUN-level-connection:
 - /v1/taxonomy/main/graph?edge-relation-type=narrower&source-concept-type=sun-education-field-1&target-concept-type=sun-education-field-2
 - /v1/taxonomy/main/graph?edge-relation-type=narrower&source-concept-type=sun-education-field-2&target-concept-type=sun-education-field-3
 - ...

To return all levels in the same request, we need to create a new legacy endpoint.

## 13- lista med utbildningsinriktningar
```
Endppoint: GetAllEducationFields
Params: languageId: string 
Response: [
	{"EducationLevel": {EducationLevelID: string, Term: string, Description: string }}
]
```

Solution: http://jobtech-taxonomy-api-develop-jobtech-taxonomy-api.test.services.jtech.se/v1/taxonomy/specific/concepts/sun-education-field?type=sun-education-field-2

## 14- lista med Länder
```
Endppoint: GetAllCountries
Params: languageId: string 
Response: [
	{"Country": {CountryID: string, CountryCode: string, CountryCodeAlpha3: string, Term: string, ContinentID: string }}
]
```

The SOAP service returns the continent ID for each country. That has to be looked up in the new taxonomy in a follow up query.

Solution:
 - Get all countries: http://jobtech-taxonomy-api-develop-jobtech-taxonomy-api.test.services.jtech.se/v1/taxonomy/specific/concepts/country?version=1
 - Get all mappings between countries and continents: http://jobtech-taxonomy-api-develop-jobtech-taxonomy-api.test.services.jtech.se/v1/taxonomy/main/graph?edge-relation-type=broader&source-concept-type=country&target-concept-type=continent&version=1

To get the result in single query, a new legacy endpoint has to be created.


## 15- lista med språk
```
Endppoint: GetAllLanguages
Params: languageId: string 
Response: [
	{"Language": {LanguageID: string, Term: string}}
]
```

Solution: http://jobtech-taxonomy-api-develop-jobtech-taxonomy-api.test.services.jtech.se/v1/taxonomy/main/concepts?type=language&version=1

## 16- lista med postnummer
```
Endppoint: GetAllPostCodes
Params: languageId: string 
Response: [
	{"PostCode": {Code: string, PostLocalityID: string, PostLocality: string, NationalNUTSLAU2Code: string, KeyAccount: string}}
]
```

David: Postnummer will not be included in the new taxonomy database. Also I would really suggest not to use the list within the old database since it could be out of date. The response also includes LAU2 which is the same as Kommunkod. So I assume the list shows the relations between postnummer and kommunkod? The geodata group should have more credible information on this. 


