---
title: ClojureD 2022
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//Reports/clojured2022.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /Reports/clojured2022.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/Reports/clojured2022.md
tags:
- Reports::clojured2022.md
- clojured2022.md
---
# ClojureD 2022

This is a short report from Jonas about his attendance at ClojureD 2022 in Berlin.

## Noteworthy talks

Here are some relevant talks that I found particularly interesting.

### Automated Testing: What's the Point?
**by Paulus Esterhazy**

This was a talk of a more philosophical nature and less technical, about unit testing and why it is useful. This talk is relevant to software engineering in general, not just unit testing.

In the talk, he touched upon the latin expression *festina lente* which means "make haste slowly". In Swedish we say "att skynda långsamt". The essence is that "in order to go fast, go slow". Unit testing can help us accomplish that. In his opinion, the main purpose of unit testing is not to eliminate bugs. Rather, it is a way of **getting high-quality feedback**. He even advocated that a happy sound effect could be attached to passing unit tests in order to make this feedback even more clear. However, writing unit tests slows you down: in a code base where half of the code is unit tests, we write twice as much code as we would for the same functionality which means that "we go slow". But going slow by writing unit tests means that we can achieve a **sustainable pace on the long term**. This can be contrasted with manual testing, where we on the one hand put in less effort in writing tests but instead do manual testing, and because each manual test can take several minutes, the accumulation of testing time can be substantial. Therefore, making changes to a growing code base becomes increasingly costly.

His final point was that **testability correlates with modifiability**. That is, qualities that make code hard to test are qualities that make code hard to modify.

### Automated Correctness Analysis for core.async
**by Sung-Shik Jongmans**

This talk was about correctness analysis of core.async which is an implementation of CSP (Communicating Sequential Processes). He explain common bugs in CSP and how such bugs can be detected through instrumentation, based on a specification expressed in a language similar to spec.alpha. The technique employed to instrument the code is applicable to other problems too.

### Code execution as data with omni-trace
**by Lukas Domagala**

This was one of the most useful talks at the conference because it explains a tool that can be of practical use to anyone working in Clojure. [Omni-trace](https://github.com/Cyrik/omni-trace) is a tool for debugging Clojure code. When running a program, a trace is obtained that can be used to render **a flame-graph where the input arguments and return value of every function call can be inspected**. This tool might be a better alternative than `println` for debugging and worth taking a deeper look at. The tool omni-trace uses [clj-kondo](https://github.com/clj-kondo/clj-kondo) and [orchard](https://github.com/clojure-emacs/orchard) to do the analysis.

## Interesting lightning talks

This will be completed later when the lightning talks are online.

* [clj-test-containers](https://github.com/javahippie/clj-test-containers): Using Docker for running unit tests.
* An interesting talk on how non-tail-recursive iteration can be expressed using `loop` and `recur`.
