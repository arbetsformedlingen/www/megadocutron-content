* Taxonomy Mapping Tools

  Hello everyone,

  My name is Jonas Östlund and I work at JobTech, which is a unit within the Swedish Public Employment Service.

  I work in the software engineering team that builds software that Redaktionen can use. Today, I will talk a little bit about how we have assisted them in their work by making use of natural language processing technologies.
  
** Problem formulation

   So the problem that they are faced with is that they have different ontologies and they want to establish connections between concepts in those ontologies.

   Here we have a tiny example of /skills/. On the left hand side, we have skills from the JobTech Taxonomy that we maintain. On the right, we have skills from ESCO.

   The editorial team (that is Redaktionen) has been working on establishing connections between concepts on the left and concepts on the right. Take for example the skill /Webbutveckling/ (which means "web development"), it sort of matches the ESCO skill /Webbprogrammering/ (which means "web programming"). We have looked into machine learning to help assist with the work of establishing those connections.

** BERT

   We have been looking into a technology called BERT. It appeared around 2018 and was developed at Google. It is a deep neural network that is based on a particular component called /transformers/. The model is first pretrained on a large body of text in order to develop an understanding for natural language.

In short, BERT takes an arbitrary text of some kind as input and produces numbers as output from this text. This way, a general BERT model is obtained. Then this model can be fine tuned for a specific task at hand.

One application for which BERT can be fine tuned is token classification. In that case, BERT will classify every word in a text as being part of a class. For instance, if the text is "We are looking for a software engineer", it could identify that "software engineer" is a job title.

Another application is Sentence-BERT. This version of BERT takes a text as input and produces a numeric representation of the text that encodes the meaning of the text.

** Sentence-BERT

   Let's look a bit more into sentence BERT. So as I just said, Sentence-BERT takes a piece of text and produces a vector that represents that text. A vector is just a list of numbers of some dimension. With Sentence-BERT, the dimension is typically 768. Here I have taken a preferred label from one concept and encoded it to obtain a vector.

   We can now apply this to the problem of establishing mappings between different concepts by measuring the /similarity/ between their corresponding Sentence BERT vectors. Here we have two different concepts, with labels "Plaggskisser, kläder" and "designa kläder". We then use Sentence BERT to encode those labels to vectors X and Y respectively. We can now measure the distance between those two vectors using something called "cosine similarity" and obtain a score. The higher this score, the closer the concepts.

** Cosine similarity: visual explanation

There is a geometric interpretation of what it means for two vectors to be similar. We can think of each vector as an arrow in space. If the direction of two arrows is similar, it means that the meaning of the texts that they represent is similar. So here, we see that the two concepts that we saw before "Designa kläder" and "Plaggskisser, kläder", are close in meaning, whereas "Vårda vilda djur och växter" points in a different direction.

** First prototype

The way we use this is to produce a spreadsheet of suggestions of good mappings. In the leftmost column, we have an ESCO concept. The other two columns are the ids and preferred labels of suggested concepts from our JobTech Taxonomy.

In this particular example, we have "utveckla energibesparingsstrategier". Then we have some matching concepts here. The top one is "energihushållning", which essentially means "managing energy in a resource efficient way".

We produced this spreadsheet for the editorial team to help them in their work to establish a mapping between our taxonomy and ESCO. It can help speed up their work, but it is far from being perfect. There is still manual labour required to get things right.

** Technical details

   I just want to mention some technical details too. Here is a snippet of Python code for using Sentence BERT to encode a piece of text.

   We use a Python library called /sentence-transformers/ to perform Sentence-BERT together with a pretrained model provided by the royal national library.

** Next steps

   We believe there are still plenty of things to improve the results.

   One of my colleagues is working on finetuning SBERT for the job market.
   There is also ongoing work to integrate the suggestions that you saw before into a GUI tool.
   We also want to explore more ways of using BERT and Sentence BERT.

** Thank you for listening

   If you want to get in touch with me, my name is Jonas Östlund and here is my email address. And I work at the JobTech unit at the Swedish Public Employment Service.
   
   That was all from me.
   Thank you very much!
   

   
