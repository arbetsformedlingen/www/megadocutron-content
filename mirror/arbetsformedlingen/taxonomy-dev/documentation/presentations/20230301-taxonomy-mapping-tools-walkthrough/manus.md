---
title: Manus till demo av taxonomy-mapping-tools
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//presentations/20230301-taxonomy-mapping-tools-walkthrough/manus.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /presentations/20230301-taxonomy-mapping-tools-walkthrough/manus.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/presentations/20230301-taxonomy-mapping-tools-walkthrough/manus.md
tags:
- presentations::20230301-taxonomy-mapping-tools-walkthrough::manus.md
- 20230301-taxonomy-mapping-tools-walkthrough::manus.md
- manus.md
---
# Manus till demo av taxonomy-mapping-tools

[**Programmering**](../topics/programmering.md)
[**taxonomy-mapping-tools**](../topics/taxonomy-mapping-tools.md)




Bygg med
```
pandoc -f markdown 2023-02-28-manus-till-demo-av-taxonomy-mapping-tools.md -t latex -o ../2023-02-28-manus-till-demo-av-taxonomy-mapping-tools.pdf
```

## taxonomy-mapping-tools walkthrough

Hello everyone and welcome to my taxonomy-mapping-tools walkthrough. My name is Jonas Östlund. Today, I will present tools for establishing mappings between sets of concepts.

This is a problem that we have solved in different forms multiple times in the past, so I decided to make a tool that we can reuse for that. And this is a presentation about that tool.

## This presentation

The purpose of this presentation is to share knowledge so that you know how this tool works, you can use it and improve on it.

Then we will formulate the problem that this tool solves.

We will talk about what this tool is.

And finally, we will look at a demo of this tool.

## Problem formulation

We have frequently had to establish mappings between different sets of concepts. A mapping consists of *source concepts* and *target concepts*.

To give an example, there are many new concepts in version 16 of the taxonomy that did not exist in version 1 of the taxonomy. If we have a concept that exists in version 16 but not in version 1, it might still be good to find a concept in version 1 that can approximate the concept in version 16. So we could establish a mapping for that. Here we limit ourselves to establish a mapping from occupation names in version 16 to approximate occupation names that exist in version 1.

In order to illustrate what I mean by that, look at this picture.

The source concepts are going to be the occupation names of version 16 of the taxonomy. Here I have drawn two concepts:

* "Arkivutredare", and 
* "Character artist"

The target concepts are occupation names from version 1 of the taxonomy. We want to establish a mapping, that consists of suitable correspondences from the source concepts to the target concepts. Such a mapping can respond to the question: *Given the concept Arkivutredare from version 16, what would be a suitable corresponding concept in version 1?*. Probably, a good answer to that question would be "Arkivarie".

In the same way, we might find that "Game concept artist" is a suitable matching concept to "Character artist".

**taxonomy-mapping-tools** can be used to establish that mapping. 

## What is taxonomy-mapping-tools?

So what is taxonomy-mapping-tools really?

It is implemented as a Python library that you can use.

It contains code to help establish mappings, by generating suggestions based on both natural language processing technologies such as S-BERT as well as other sources of knowledge, for example whether to concepts have common broader concepts.

It is open source and the source code can be found here.

## Demo

Now we are ready to perform a demo.

Börja med

```
poetry new mapping-tools-demo
cd mapping-tools-demo

emacs pyproject.toml
```
gå till https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp .
sedan https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-mapping-tools

Kopiera hashen och git-adressen:

```
taxonomy-mapping-tools = { git = "", rev = "" }
```
och sätt in.


Sedan `poetry update`.

Om det inte funkar, kör

```
pip3 install sentence-transformers
```

Sedan

```
poetry shell
```

Nu är vi klara att börja koda.

```py
 # First start a REPL.

 # Then we import the Element class. It represents the kind of things that we want to map:

from taxonomy_mapping_tools.core import Element

 # We can instantiate an Element like this:

e = Element("119", "Bråkmakare")

 # >>> e
 # Element(id='119', Bråkmakare)
 # >>> e.id()
 # '119'
 # >>> e.preferred_label()
 # 'Bråkmakare'
 # >>>

 # Ok, so that is how an element works.

 # #  # # # # # # # # Loading the data

 # Now, let's load the data that we are going to work on.
 # I have put the data here:

pathfmt = "/home/jonas/prog/tax2skos/build/result/version/{:d}/query/concepts-and-common-relations/concepts-and-common-relations.json"

 # The data is in json format, so we have to import tools for reading json

import conrec_utils.json_utils as json_utils

 # So we load the data for version 16 of the taxonomy.

v16_data = json_utils.read_json(pathfmt.format(16))
v1_data = json_utils.read_json(pathfmt.format(1))

 # #  # # # # # # # # Explore the data

 # This is what the data looks like:

 # >>> len(v16_data["data"]["concepts"])
 # 39851


sample_v16 = v16_data["data"]["concepts"][0]
sample_v1 = v1_data["data"]["concepts"][0]

sample_occupation = [x
                     for x in v16_data["data"]["concepts"]
                     if x["type"] == "occupation-name"][0]

 # #  # # # # # # # # Build a map of all concepts

 # This will let us do quick lookups.

def build_full_map(src_data):
    return {concept["id"]:concept
            for concept in src_data["data"]["concepts"]}

v16_map = build_full_map(v16_data)

 # >>> list(v16_map.keys())[0]
 # '115i_88X_d9i'

 # #  # # # # # # # # Get all broader keys to find overlap

def follow_broader(concept_map, init):
    stack = init.copy()
    dst = set()
    while 0 < len(stack):
        item = stack.pop()
        concept = concept_map[item["id"]]
        dst.add((concept["type"], concept["id"]))
        stack.extend(concept["broader"])
    return dst

 # >>> follow_broader(v16_map, sample_occupation["broader"])
 # {('ssyk-level-1', '2xXe_2KC_Nf2'), ('ssyk-level-3', 'WXzn_BcV_MVA'), ('ssyk-level-2', 'cS2C_4ZH_5ef'), ('occupation-field', 'whao_Q6A_ScE'), ('ssyk-level-4', 'idjo_2Wr_a3E'), ('isco-level-4', 'rk7d_opL_3oW')}

for x in follow_broader(v16_map, sample_occupation["broader"]):
    print(x)

 # #  # # # # # # # # Prepare the data

def prepare_elements(src_data, excluded_ids):
    full_map = build_full_map(src_data)
    return [Element.from_graphql_data({
        **c,
        "all_broader": follow_broader(full_map, c["broader"])
    })
            for c in src_data["data"]["concepts"]
            if c["type"] == "occupation-name" and not(c["id"] in excluded_ids)]

target_elements = prepare_elements(v1_data, set())

source_elements = prepare_elements(
    v16_data,
    {c.data()["id"] for c in target_elements})

 # len(all_dst_elements)
 # len(all_src_elements)

 # Example of element not in version 1:

 # all_src_elements[0]

 # #  # # # # # # # # Define the problem

from taxonomy_mapping_tools.core import
MappingProblem, JaccardDistanceFunction
from taxonomy_mapping_tools.sbert import SbertDistanceFunction,
KBLab_sentence_bert_swedish_cased_factory

 # Define the mapping problem *declaratively*:

class MapV16ToV1(MappingProblem):
    def mapping_problem_name(self):
        return "mapping_v16_to_v1"

    def source_elements(self):
        return source_elements

    def target_elements(self):
        return target_elements

    def distance_function(self):
        sbert_df = SbertDistanceFunction(
            KBLab_sentence_bert_swedish_cased_factory.build(self.env()))
        broader_df = JaccardDistanceFunction(
            lambda e: e.data()["all_broader"])

        return sbert_df + 0.1*broader_df

problem = MapV16ToV1()
    
 # #  # # # # # # # # Generate matches

from taxonomy_mapping_tools.core import generate_matches

generate_matches(problem)

 # #  # # # # # # # # Edit it

from taxonomy_mapping_tools.core import edit

edit(problem)
```

## Editing it

```
help(set_candidate_count)
```

| Vad               | Kommando                                                        |
|-------------------|-----------------------------------------------------------------|
| Arkivutredare     | `pick(1)`                                                       |
| Scrum master      | `strgen("någon som leder utvecklingsteam i mjukvara"), pick(5)` |
| HR-strateg        | `pick(0)`                                                       |
| Projektledare, el | `consider(1)`                                                   |

`save()`

Titta på resultatet.

## How it works

https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-mapping-tools/-/blob/master/taxonomy_mapping_tools/core.py#L618


