---
title: text2ssyk
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//projects/text2ssyk.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /projects/text2ssyk.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/projects/text2ssyk.md
tags:
- projects::text2ssyk.md
- text2ssyk.md
---
# text2ssyk

The objective of this task is to classify jobads into occupation classes known as SSYK level 4.

## Tasks

Here is a breakdown of this project into tasks that have been completed or that we are working on.

### Tools for assessing algorithms [**COMPLETED**]

A [Python module][102] has been created for evaluating and comparing text2ssyk algorithms.

### Prototype algorithms [**ONGOING**]

We are currently experimenting with [BERT][102]-based algorithms for classifying jobads.


## Links

| Name                                           | Description                                                               |
|------------------------------------------------|---------------------------------------------------------------------------|
| [`conrec-utils`][100]                          | A Python utility library that has functionality for evaluating text2ssyk. |
| [`conrec-utils: text2ssyk_evaluation.py`][102] | A module for assessing text2ssyk algorithms.                              |
| [`text-2-ssyk`][101]                           | An algorithm for text-2-ssyk currently used in production.                |
| [BERT][103]                                    | The original BERT paper.                                                  |


[100]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils
[101]: https://gitlab.com/arbetsformedlingen/joblinks/text-2-ssyk
[102]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils/-/blob/master/conrec_utils/text2ssyk_evaluation.py
[103]: https://arxiv.org/abs/1810.04805
