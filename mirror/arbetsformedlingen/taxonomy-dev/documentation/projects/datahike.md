---
title: Datahike
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//projects/datahike.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /projects/datahike.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/projects/datahike.md
tags:
- projects::datahike.md
- datahike.md
---
# Datahike

The objective is to migrate the database from Datomic to Datahike.
