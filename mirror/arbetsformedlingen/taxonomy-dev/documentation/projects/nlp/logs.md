---
title: Project log
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//projects/nlp/logs.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /projects/nlp/logs.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/projects/nlp/logs.md
tags:
- projects::nlp::logs.md
- nlp::logs.md
- logs.md
---
# Project log

Logs are in *chronological order*. Include the *date* in the subsection header of the log entry.

## Log 2022-02-18: Demo day

We made a demo, see branch [experimental/osdjn/demo-dev](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/nlp-education-analyzer/-/tree/experimental/osdjn/demo-dev).
To run the demo, first start the web server from the file [src/py/main_demo.py](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/nlp-education-analyzer/-/blob/experimental/osdjn/demo-dev/src/py/main_demo.py).
Then open [index.html](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/nlp-education-analyzer/-/blob/experimental/osdjn/demo-dev/src/web/index.html).
See also [index_mapping.html](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/nlp-education-analyzer/-/blob/experimental/osdjn/demo-dev/src/web/index_mapping.html) for a demo of ESCO mappings.

## Log 2022-02-18: Pushed current state of master to a separate branch

See branch [experimental/osdjn/master-2022-02-18](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/nlp-education-analyzer/-/commits/experimental/osdjn/master-2022-02-18).
