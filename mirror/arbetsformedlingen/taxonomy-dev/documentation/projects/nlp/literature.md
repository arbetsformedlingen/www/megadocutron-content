---
title: Literature
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//projects/nlp/literature.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /projects/nlp/literature.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/projects/nlp/literature.md
tags:
- projects::nlp::literature.md
- nlp::literature.md
- literature.md
---
# Literature

Here we refer to the relevant literature in the field.

* Liz Gallagher, India Kerle, Cath Sleeman, George Richardson (Nesta UKd, nesta.org.uk, Twitter @nesta_uk): *A New Approach to Building a Skills Taxonomy*. February 22 2022. This is a set of slides about how Nesta UK uses BERT to build a taxonomy about skills. In short, they identify segments in a text that contains skills, mask out irrelevant words in each segment using `[mask]`, build BERT sentence embeddings for each segment, perform dimensionality reduction, then clustering, then build a taxonomy from that. Here is a [link](https://mattermost.jobtechdev.se/jobtechdev/pl/ki6g8ib6cid97dao4rg7brasee) to where it was posted in our chat.
