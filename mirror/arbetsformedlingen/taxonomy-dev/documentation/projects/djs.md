---
title: Publish on data.jobtechdev.se
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//projects/djs.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /projects/djs.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/projects/djs.md
tags:
- projects::djs.md
- djs.md
---
# Publish on data.jobtechdev.se

The taxonomy has been published as json and skos files on [data.jobtechdev.se][100]. This is done by a [cron job][101] every hour.

[100]: https://data.jobtechdev.se/taxonomy/version/
[101]: https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-tax2skos
