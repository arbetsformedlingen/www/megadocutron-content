---
title: Clojure
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//Tutorials/clojure.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /Tutorials/clojure.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/Tutorials/clojure.md
tags:
- Tutorials::clojure.md
- clojure.md
---
# Clojure


## Installing Clojure

https://clojure.org/guides/getting_started


## Learning


https://gist.github.com/yogthos/be323be0361c589570a6da4ccc85f58f


### Clojure for the brave and true

This is really good home page to learn concepts in clojure.

https://www.braveclojure.com/introduction/

https://www.braveclojure.com/do-things/


## Clojure slack

https://app.slack.com/client/T03RZGPFR/C053AK3F9

## Build

https://youtu.be/OgV-ALpmXUI



## Structural editing

https://shaunlebron.github.io/parinfer/


## Datalog

http://www.learndatalogtoday.org/
