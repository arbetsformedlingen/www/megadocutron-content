---
title: Remote Clojure Developer at the Swedish Public Employment Service
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//Ads/clojuredev2022.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /Ads/clojuredev2022.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/Ads/clojuredev2022.md
tags:
- Ads::clojuredev2022.md
- clojuredev2022.md
---
# Remote Clojure Developer at the Swedish Public Employment Service

This ad is also published here (in Swedish):  
https://arbetsformedlingen.se/platsbanken/annonser/25944112

**Location**: Remote, anywhere in Sweden, *but we will consider applicants from other countries as well, especially countries within Europe, the EU and **countries in the proximity of Sweden** *. Possibility to have physical workplace at one of our offices in Sweden.

**Position:** System Developer/Programmer

**Employer**: Swedish Public Employment Service, VO Direct, Jobtech Unit.

**Form of employment**: Permanent

**Number of jobs**: 1

**Qualifications**: Work experience required

**Salary**: Individual salary setting

**Salary type**: Fixed monthly, weekly or hourly wage

**Employment**: 100%

**Starting date**: By agreement

## About the job

Welcome to the job that gets Sweden to work! Here you work on the government's mission for a labour market that helps people move forward. We free up opportunities so that job seekers find employment and employers their next employee. In order to achieve this goal, we need to cooperate, in the authority but also with various
external actors throughout the country. Together we drive change, develop new ways of working and create solutions that enable Sweden to continue moving forward.

## You want to be one of us?

JobTech Development is a distributed unit within the Swedish Public Employment Service that offers open APIs and open source solutions to those who, together with us, want to work for an improved labour market.

Jobtech Development is organized into autonomous teams, which means that each team is self-governing, where the members themselves decide how they will work, what conditions are required for everyone's skills to be best utilized and how the work can be done most efficiently. We are spread out across the country but have frequent contacts digitally.

In our self-governing teams, everyone takes responsibility, which
creates a commitment and a drive. From this commitment arises a learning that promotes development and innovation. In other words, the self-governing team has the potential to go further than traditional teams can handle. And employees have more fun in the meantime.

We make classification systems available externally via REST APIs with container-based micro-services. We have a strong focus on ensuring that consuming systems can painlessly keep up with changes in the values in the classification systems. We program mainly in **Clojure**.

At JobTech Development, there are great opportunities for development within the team, or to try new things
within the other other teams or the Public Employment Service in general. Our deliveries are characterized by openness and transparency and as much of our code as possible is made into free software so that the tax money that funds us can benefit as many people as possible.

## Are you curious about how we code?

All we do is free software and is available in our public git repos such as https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend.

## Who are we looking for?
We are now looking for a skilled Clojure developer to our team working with classification and classification systems such as taxonomies and ontologies across the labour market. In concrete terms, we work with the values used in the matching between job advertisements and CVs such as professional names, skills words, abilities and tasks. Classification systems are built up manually in close cooperation with an editorial team and mechanically by processing large amounts of text. Developers who work with us in the team especially appreciate being able to work freely with how to choose to solve tasks, technical as well as how we organize the team. The team is characterized by humor and trust, we think.

## Qualifications
* You are passionate about contributing to open source
* You have several years of experience in system development
* You can design and develop
robust and changeable systems and components
* You know functional programming and have a grasp of the advantage of immutability in distributed systems
* You have a strong interest in working in Clojure and want to contribute to the Clojure community
* You enjoy and manage to work remotely

We see it as a merit if you know *Datomic, Datahike, logic programming, Luminus, AWS, Docker/Kubernetes/OpenShift, Tekton, Emacs*.

## Who are you?
Great importance will be placed on your personal qualities. We are therefore looking for you who can work in a flat organization where you get great degrees of freedom but where there are also high demands on being able to take responsibility for yourself. You have a personal maturity and ability to collaborate and communicate with different kinds of people. You also have a drive and commitment and are prestigeless and result-focused. An interest in language and words helps, but is not a requirement.

## Our offer
Our department has a great focus on openness and societal benefits so you will contribute to Open source and open data. You have the opportunity to use part of your working hours each year to further your education and can learn the latest technology in e.g. machine learning via distance learning courses online.

The Swedish Public Employment Service offers an attractive workplace characterised by the desire to be free from discrimination and provide equal opportunities for all. We welcome all applicants regardless of gender, gender identity or expression, ethnicity, religion, disability, sexual orientation and age.

## The position

The position is a full-time permanent position that starts with a six-month probationary period. Access as soon as possible or as agreed.

We have good benefits, with good holiday agreements, flex time, the possibility of wellness during working hours and wellness allowance. With us there is a great commitment, everyone who works here wants to make an effort. Read more about our organization and our mission on our website: https://arbetsformedlingen.se/om-oss

## Application
Please send your application to us no later than 2022-06-26.

You apply by clicking on the button **Ansök här** (*Apply*) on the page of the job ad (in Swedish): https://arbetsformedlingen.se/platsbanken/annonser/25944112

Attach your CV and a cover letter describing why you are applying for the job, as well as grades/certificates proving the requirements for education and/or work experience in your application. The Swedish Public Employment Service is subject to an evidence-based recruitment process. You can read more about our recruitment process on our website https://arbetsformedlingen.se/om-oss/jobba-hos-oss

Selection tests may occur in our recruitment process and we will give you more information about the
process via email after the application period has expired. We work actively to be an authority free from discrimination. It is important to us that all skills are utilised and we welcome all applicants. We are positive if you have knowledge of any of our national minority languages.

## Postal address

Arbetsförmedlingen  
Elektrogatan 4  
11399 Stockholm

## Contact

*See full contact details (phone and email) on the page of the ad:*  
https://arbetsformedlingen.se/platsbanken/annonser/25944112

## Apply for the job

Apply by June 26 here:  
https://arbetsformedlingen.varbi.com/what:job/jobID:500228/type:job/where:1/apply:1

Enter reference Af-2022/0032 7296 in your application.
