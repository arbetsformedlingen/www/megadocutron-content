---
title: Recurrent Tasks for the Taxonomy Dev Team
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation/-/blob/main//RecurrentTasks.md
gitdir: /arbetsformedlingen/taxonomy-dev/documentation
gitdir-file-path: /RecurrentTasks.md
date: '2023-10-13 10:27:53'
path: /arbetsformedlingen/taxonomy-dev/documentation/RecurrentTasks.md
tags:
- RecurrentTasks.md
---
# Recurrent Tasks for the Taxonomy Dev Team

## Daily

These tasks should be done by at least one team member daily:
- Check communications; e-mail and the [taxonomy-dev channel](https://mattermost.jobtechdev.se/batfish/channels/taxonomydev)
- Check errors from the APIs in [Bröl](https://mattermost.jobtechdev.se/batfish/channels/brol) and the [Statping channel](https://mattermost.jobtechdev.se/batfish/channels/alert-i1)

## Weekly

These tasks should be done by at least one team member weekly:
- Check communications in the [Forum](https://forum.jobtechdev.se/)
