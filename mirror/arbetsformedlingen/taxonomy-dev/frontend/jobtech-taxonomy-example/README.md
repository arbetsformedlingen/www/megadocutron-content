---
title: jobtech-taxonomy-example
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-example/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-example
gitdir-file-path: /README.md
date: '2023-10-04 15:13:32'
path: /arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-example/README.md
tags:
- README.md
---
# jobtech-taxonomy-example

The front end is using React with JSX developed using node.js.
Download the "recommended for most users" version: https://nodejs.org/en/

## First time opening and installing the project
npm install

## Run developer mode with hotloading
npm run build

## Create a release bundle js (this is a temporary solution)
npm run release
