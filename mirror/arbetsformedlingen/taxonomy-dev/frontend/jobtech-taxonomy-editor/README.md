---
title: jobtech-taxonomy-frontend
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-editor/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-editor
gitdir-file-path: /README.md
date: '2023-10-19 13:39:59'
path: /arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-editor/README.md
tags:
- README.md
---
# jobtech-taxonomy-frontend 

The front end is using React with JSX developed using node.js. 
Download the "recommended for most users" version: https://nodejs.org/en/

## First time opening and installing the project
npm install

## Run developer mode with hotloading
npm run build

## Create a release bundle js (this is a temporary solution)
npm run release
