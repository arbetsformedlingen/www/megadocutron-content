---
title: jobtech-taxonomy-highscore
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-highscore/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-highscore
gitdir-file-path: /README.md
date: '2023-09-15 17:20:56'
path: /arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-highscore/README.md
tags:
- README.md
---
# jobtech-taxonomy-highscore

Målet är att ta fram en tabell som listar frekvensen för hur ofta en arbetsgivare efterfrågar vissa ord. Med stor sannolikhet är orden en kompetens. För att ge ytterligare sammanhang så används en naiv metod för att se om orden är listade i den officiella registret för yrkesklassificering. Angreppssättet utgår från publicerade platsannonser från Platsbanken, Monster, Blocket Jobb mm som har blivit berikade med efterfrågade ord från tjänsten JobAdEnrichment.

Se dokumentet "Vanligt efterfrågade ord.pdf" i repot för en mer utförlig beskrivning.

## Installera och kör
```sh
git clone https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-highscore &&
cd jobtech-taxonomy-highscore/ &&
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1czb5s9mJqtT9HDvssRgMnmSrwFQWiSK4' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1czb5s9mJqtT9HDvssRgMnmSrwFQWiSK4" -O ads.json && rm -rf /tmp/cookies.txt &&
docker build -t jobtechdev/highscore . &&
cat ads.json | docker run -i jobtechdev/highscore -y 2512 > highscore.txt
```

## Annonser

Filen med annonser är i formatet JobPosting som används i https://gitlab.com/arbetsformedlingen/joblinks.

Bifogar en fil från 1/9-2021, [ladda ner filen](https://drive.google.com/file/d/1czb5s9mJqtT9HDvssRgMnmSrwFQWiSK4/view?usp=sharing).

## Prova med ett annat yrke
Yrkeskoder hittas [här](https://www.h5.scb.se/yreg/ssyk2012.asp)
```
cat ads.json | docker run -i jobtechdev/highscore -y 2351
```

## Resultatet

Första kolumnen anger hur många gånger ordet i andra kolumnen har förekommit som ett efterfrågat ord bland annonserna. Tredje kolumnen anger hur många gånger ordet hittades i taxonomin.

|Frekvens|Ord|Finns i yrkesregistret|
|-----|-----|----|
|6299| data  |     1
|6225| design |    1
|4763| can |       0
|3614| svenska |   0
|2966| engelska |  0
|2963 |cloud |     0
|2769| not   |     0
|2743 |teknik |    0
|2646 |agile  |    0
|2259 |java   |    0
