---
title: jobtechdev/taxonomy-autocomplete
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-autocomplete/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/taxonomy-autocomplete
gitdir-file-path: /README.md
date: '2022-12-16 15:04:12'
path: /arbetsformedlingen/taxonomy-dev/backend/taxonomy-autocomplete/README.md
tags:
- README.md
---
# jobtechdev/taxonomy-autocomplete

A library that creates a lucene index for all concepts in the Jobtech Taxonomy.
Uses datahike as a database.


``` clojure
(autocomplete
   {:query-string "Dagis"
    })
```

``` clojure
(autocomplete
   {:query-string "s"
    :types ["region" #_"occupation-field"]
    })
```



## Usage

FIXME: write usage documentation!

Invoke a library API function from the command-line:

    $ clojure -X jobtechdev.taxonomy-autocomplete/foo :a 1 :b '"two"'
    {:a 1, :b "two"} "Hello, World!"

Run the project's tests (they'll fail until you edit them):

    $ clojure -T:build test

Run the project's CI pipeline and build a JAR (this will fail until you edit the tests to pass):

    $ clojure -T:build ci

This will produce an updated `pom.xml` file with synchronized dependencies inside the `META-INF`
directory inside `target/classes` and the JAR in `target`. You can update the version (and SCM tag)
information in generated `pom.xml` by updating `build.clj`.

Install it locally (requires the `ci` task be run first):

    $ clojure -T:build install

Deploy it to Clojars -- needs `CLOJARS_USERNAME` and `CLOJARS_PASSWORD` environment
variables (requires the `ci` task be run first):

    $ clojure -T:build deploy

Your library will be deployed to net.clojars.jobtechdev/taxonomy-autocomplete on clojars.org by default.

## License

Copyright © 2022 Jobtechdev

Distributed under the Eclipse Public License version 1.0.
