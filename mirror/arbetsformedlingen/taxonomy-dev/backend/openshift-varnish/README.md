---
title: Varnish docker container for jobtech-taxonomy-api
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/openshift-varnish/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/openshift-varnish
gitdir-file-path: /README.md
date: '2023-11-10 07:35:43'
path: /arbetsformedlingen/taxonomy-dev/backend/openshift-varnish/README.md
tags:
- README.md
---
# Varnish docker container for jobtech-taxonomy-api  
 
Varnish is used to cache requests to the Taxonomy API in order to decrease response times. This repository mainly contains the Docker configuration for such a Varnish instance.

## Quick reference

**Docker:**
* `make docker-image`: Make demo image for a Varnish container configured against the *non-cached* prod API.
* `make docker-run`: Start the demo image just built.
* `make docker-stop`: Stop the demo image

**Testing:**
* `make test-varnish-local`: Send test queries to *local* Varnish
* `make test-varnish-prod`: Send test queries to *production* Varnish

**Bucket generation:**
* `make generate-buckets`: Regenerate a bucket configuration.

## Testing

To test this code locally, do
```
make docker-image
make docker-run
```
and in a new terminal window, do
```
make test-varnish-local
```

To test the current production instance of Varnish, do
```
make test-varnish-prod
```

## Build Container

```
docker build . -t varnish-demo
```

## Starting and Stopping a Container

Make sure that the two environment variables are set correctly.

Example command:
```
docker run -p 8083:8083 -e TAXONOMYSERVICEPORT=80 -e TAXONOMYSERVICE=api-prod-taxonomy-api-gitops.prod.services.jtech.se --name varnish-demo --rm varnish-demo
```

Add parameter `--network host` if varnish is configured to have localhost as backend.

**Warning:** *Does not work for macOS.*

What works on macOS:
```
docker run -p 8083:8083 -e TAXONOMYSERVICEPORT=3000 -e TAXONOMYSERVICE=host.docker.internal --name varnish-demo --rm varnish-demo
```

To stop a running container:
```
docker container stop varnish-demo
```

## Open a shell in a running container

```
docker exec -it varnish-demo bash
```

## Testing

Test in a browser or curl, api key 111

Curl:
```
curl -X GET "http://localhost:8083/v1/taxonomy/main/concepts?preferred-label=Applikationsprogrammerare" -H  "accept: application/json" -H  "api-key: 111"
curl -IL -X GET "http://localhost:8083/v1/taxonomy/main/concepts?preferred-label=Applikationsprogrammerare" -H  "accept: application/json" -H  "api-key: 111"
```

Browser:
```
http://localhost:8083/v1/taxonomy/main/concepts?preferred-label=Applikationsprogrammerare
```

## Debugging
To get all requests to varnish that bypassed the cache, use:
```
oc exec pod/<pod name> -- varnishlog -d -i "Req*,VCL*" -x ReqAcct,ReqStart -q "VCL_call eq 'PASS'" -g request
```
## Bucket configuration

This Varnish configuration classifies backend responses into "buckets" depending on the size of the response. Each bucket is a storage with an interval of response sizes that are accepted. This is to make sure that responses of very different sizes don't get placed in the same storage. For an investigation on this, see [this issue](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/issues/439).

The problem with a single storage for all requests is that if the cache is full of many small backend responses and space needs to be freed up in order to make room for a new large response, then there is a risk that the number of old cache items that gets deleted reaches `nuke_limit`. If that happens, the Varnish response body of the large response gets truncated and returned to the client. To solve this, we use a bucketing strategy. Every bucket is such that the ratio between the largest and smallest possible size is less than the `nuke_limit`.

The buckets configuration is inside the file [`buckets_config.edn`](buckets_config.edn). The `:span-factor` parameter controls the ratio between the largest and smallest size of responses stored in a bucket. There are other settings too, explained with comments in that file. To generate a bucket configuration, call `make generate-buckets`.
