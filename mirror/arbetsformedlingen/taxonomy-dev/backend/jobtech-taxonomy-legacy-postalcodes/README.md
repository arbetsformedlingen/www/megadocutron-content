---
title: jobtech-taxonomy-legacy-postalcodes
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-legacy-postalcodes/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-legacy-postalcodes
gitdir-file-path: /README.md
date: '2023-10-04 09:40:01'
path: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-legacy-postalcodes/README.md
tags:
- README.md
---
# jobtech-taxonomy-legacy-postalcodes

FIXME: description

## Installation

Download from http://example.com/FIXME.

## Usage

FIXME: explanation

    $ java -jar jobtech-taxonomy-legacy-postalcodes-0.1.0-standalone.jar [args]

## Options

FIXME: listing of options this app accepts.

## Examples

...

### Bugs

...

### Any Other Sections
### That You Think
### Might be Useful

## License

Copyright © 2020 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
