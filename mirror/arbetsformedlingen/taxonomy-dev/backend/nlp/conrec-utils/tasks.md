---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils/-/blob/main//tasks.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils
gitdir-file-path: /tasks.md
date: '2023-10-05 11:55:32'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils/tasks.md
tags:
- tasks.md
---
| Task key | Description |
|-|-|
| `evaluate` | None |
| `generate-reports` | None |
| `comparison-report` | None |
| `generate-new-indices` | None |
| `evaluate-validation` | None |