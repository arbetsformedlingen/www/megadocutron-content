---
title: conrec-utils
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils
gitdir-file-path: /README.md
date: '2023-10-05 11:55:32'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils/README.md
tags:
- README.md
---
# conrec-utils

Python3 utilities for concept recognition.

## Consuming this library

Assuming you are using Poetry to manage your project, add a git dependency to your `pyproject.toml` file. Use the following code to refer to a *specific commit* of the lirary.
```
[tool.poetry.dependencies]
conrec-utils = { git = "https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils.git", rev = "b270b0344a0966c6dd7ec40ce6e9fe533fcbaa5f" }
```
Whenever a new version of the library is released, you will have to call `poetry update` to fetch that version.

If you are doing *local development*, you can instead refer to the library directly using a relative path:

```
[tool.poetry.dependencies]
conrec-utils = { path = "../conrec-utils/", develop = true }
```

## Developing this library

Initialize the environment with the following commands:
```
poetry install
poetry shell
```
If it blocks on `poetry install`, you may have this issue, maybe?: https://unix.stackexchange.com/a/635597

Whenever you make changes that should force an update at consumers of this library, **increase the version number** in `pyproject.toml` and push to master.

## Features

### Working with annotated document datasets

For an example of a repository of annotated documents, see https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-prod.

Terminology: When talking about train, validation and test datasets, we follow [Brian Ripley, page 354, Pattern Recognition and Neural Networks, 1996](https://machinelearningmastery.com/difference-test-validation-datasets/):

* Training set: A set of examples used for learning, that is to fit the parameters of the classifier.
* Validation set: A set of examples used to tune the parameters of a classifier, for example to choose the number of hidden units in a neural network.
* Test set: A set of examples used only to assess the performance of a fully-specified classifier.

This repository also has references to some existing datasets, in the variable `conrec_utils.annotated_document_dataset.known_repository_list`:
```py
known_repository_list = [dataset_from_git("https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-prod.git", "c701b275d41581796c2f14cdcded8671366a86ca")]
```

Note the reference to a specific commit sha of the dataset. You will have to manually update that sha to work with a newer revision of the dataset.

The file [datasets/mentor-api-prod/index.csv](datasets/mentor-api-prod/index.csv) defines the *train*, *validation* and *test* splits when using a dataset for machine learning. To regenerate this index after updating the commit sha, call `make generate-new-indices`.

Here is an example of accessing elements in the dataset `mentor-api-prod`:

```py
import conrec_utils.annotated_document_dataset as dataset
from conrec_utils.env import Env

repo = dataset.load_known_repository(Env.default(), "mentor-api-prod")
print(repo)
full_dataset = repo.full_dataset()
print(full_dataset)
train_dataset = full_dataset.train_subset()
print(train_dataset)
print("First item: " + dataset.summarize_dataset_item(train_dataset[0]))
print("Number of training samples: {:d}".format(len(train_dataset)))
```

which will print out the following:

```
Git pull repository in /home/jonas/.jobtech_conrec_utils_cache/annotated_document_datasets/mentor-api-prod
Already up to date.
Repository(/home/jonas/.jobtech_conrec_utils_cache/annotated_document_datasets/mentor-api-prod)
Dataset(/home/jonas/.jobtech_conrec_utils_cache/annotated_document_datasets/mentor-api-prod, train:1171, validation:338, test:156)
Dataset(/home/jonas/.jobtech_conrec_utils_cache/annotated_document_datasets/mentor-api-prod, train:1171)
First item: splitclass=train, text='Sjuksköterskor till barn- o...' annotations:9
Number of training samples: 1171
```

### Working with the taxonomy

The [`conrec_utils.taxonomy`](conrec_utils/taxonomy.py) module contains functions and classes for working with [JobTech Taxonomy](http://atlas.jobtechdev.se). It contains a class `Taxonomy` to hold the taxonomy data and perform lookups of concepts.

The following code:

```py
from conrec_utils import taxonomy
from conrec_utils.env import Env

tax = taxonomy.load_taxonomy(Env.default())
print(tax)
concept = tax.concept_list[0]
occupations = tax.filter_concepts_by_type({"occupation-name"})
print(occupations)
occupation = occupations.concept_list[0]
print(occupation["preferred_label"])
broader = tax.concept_map[occupation["broader"][0]["id"]]
print(broader["preferred_label"])
```

will produce

```
Taxonomy(concepts:38949, types:44)
Taxonomy(concepts:3136, types:44)
Klottersanerare
Saneringsarbetare m.fl.
```

### Evaluating an algorithm

The [`conrec_utils.annotated_document_evaluation`](conrec_utils/annotated_document_evaluation.py) module implements tools for evaluating algorithms on annotated documents. Those tools can be used for (i) render reports of how an algorithm performs and (ii) compute metrics on validation/test datasets. See the file [`examples/evaluate_algorithm_example.py`](examples/evaluate_algorithm_example.py) for a full minimal example of how to evaluate an algorithm that performs concept recognition on a dataset.

### Working with JAE Synonyms

The module [`conrec_utils.jae_synonyms`](conrec_utils/jae_synonyms.py) provides code for accessing and working with the Jobad Enrichments (JAE) synonym dictionary.

Here is an example of how it can be used:
```py
from conrec_utils import jae_synonyms
from conrec_utils.env import Env

s = jae_synonyms.load_synonyms(Env.default())
print("Loaded {:d} synonyms: {:s}".format(len(s), str(s)))

# Pick the first synonym:
first_synonym = s[0]
print("First synonym: " + str(first_synonym))

# Find an example of a large group of words that mean the same:
for (group_key, group) in s.synonym_groups.items():
    if 16 < len(group):
        print("Synonym group: " + group_key)
        for x in group:
            print("  * " + str(x))
        break
```
will produce this output:
```
Loaded 103067 synonyms: Synonyms(competence:70771, occupation:27013, trait:1445, geo:3838)
First synonym: {'concept': 'Kemisk massatillverkning', 'term': 'kemisk massatillverkning', 'term_misspelled': False, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Kemisk massatillverkning'}
Synonym group: competence/Arbetslivserfarenhet
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetslivserfarenhet', 'term_misspelled': False, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetslivserfarenheter', 'term_misspelled': False, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetslivsvana', 'term_misspelled': False, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'yrkeslivserfarenhet', 'term_misspelled': False, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbestlivserfarenhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetslivserfarnehet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbeslivserfarenhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetslivserfarenehet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetslivserafrenhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetslivserfarnhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbestslivserfarenhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetsliverserfarenhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetsliveserfarenhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetslivserafenhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetslivserfareneht', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'abetslivserfarenhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetslivserfarehet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetslivserarenhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetslivserfarenet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetslivsefarenhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetlivserfarenhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetslivserfrenhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetsliverfarenhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetslivserfaranhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbeteslivserfarenhet', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
  * {'concept': 'Arbetslivserfarenhet', 'term': 'arbetlivserfarenheter', 'term_misspelled': True, 'type': 'KOMPETENS', 'synonym_type': 'competence', 'synonym_key': 'competence/Arbetslivserfarenhet'}
```

### Jobad Enrichments wrapper

There is a small wrapper around the Jobad Enrichments API in [`conrec_utils.jae_api`](conrec_utils/jae_api.py):

Example usage:
```py
from conrec_utils.jae_api import JobadEnricher, jae_document_input

jae = JobadEnricher()
input_docs = [jae_document_input("mjao", "Hej", "Vi söker en javautvecklare")]
enriched_docs = jae.enrich_documents(input_docs)
print(enriched_docs)
```

will produce this output:

```
[{'doc_id': 'mjao', 'doc_headline': 'Hej', 'doc_sentences': ['Hej', 'Vi söker en javautvecklare'], 'enriched_candidates': {'occupations': [{'concept_label': 'Javautvecklare', 'term': 'javautvecklare', 'term_misspelled': False, 'sentence': 'Vi söker en javautvecklare', 'sentence_index': 1, 'prediction': 0.9941486418}], 'competencies': [], 'traits': [], 'geos': []}}]
```

## More on splits

**Validation set**:
* [Why Do We Need a Validation Set in Addition to Training and Test Sets?](https://towardsdatascience.com/why-do-we-need-a-validation-set-in-addition-to-training-and-test-sets-5cf4a65550e0): *the validation test is useful for hyperparameter tuning or selecting the best model out of different models. In some contexts, the validation set is also called the Development (Dev) set.*
* [Validation set](https://www.techtarget.com/whatis/definition/validation-set): *The validation set contrasts with training and test sets in that it is an intermediate phase used for choosing the best model and optimizing it. Validation is sometimes considered a part of the training phase. It is in this phase that parameter tuning occurs for optimizing the selected model. Overfitting is checked and avoided in the validation set to eliminate errors that can be caused for future predictions and observations if an analysis corresponds too precisely to a specific dataset.*

## Future plans

* Support comparisons between different algorithms on the same datasets. But do it in a well decoupled way, where we provide one Reporter for each algorithm? How can it be done?

