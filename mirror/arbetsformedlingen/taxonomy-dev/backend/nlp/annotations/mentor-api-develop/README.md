---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-develop/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-develop
gitdir-file-path: /README.md
date: '2023-02-20 11:59:17'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-develop/README.md
tags:
- README.md
---
Annotations repo for develop
