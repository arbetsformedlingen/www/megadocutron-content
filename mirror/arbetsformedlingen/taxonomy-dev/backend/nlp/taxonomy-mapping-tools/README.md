---
title: taxonomy-mapping-tools
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-mapping-tools/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-mapping-tools
gitdir-file-path: /README.md
date: '2023-02-28 16:00:50'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-mapping-tools/README.md
tags:
- README.md
---
# taxonomy-mapping-tools

This is a Python library for establishing mappings from a set of concepts/words to another set of concepts/words.

## Usage

The mapping problem is defined declaratively by implementing the `MappingProblem` class from [`taxonomy_mapping_tools/core.py`](taxonomy_mapping_tools/core.py).

See the [`taxonomy_mapping_tools/problems/map_occupations_v16_to_v1.py`](taxonomy_mapping_tools/problems/map_occupations_v16_to_v1.py) for an example of how to define a `MappingProblem`.

Call `make map_occupations_v16_to_v1` to launch an interactive mapping editor.
