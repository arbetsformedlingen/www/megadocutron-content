---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/deep-diamond/dd-minimal-lein/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/deep-diamond/dd-minimal-lein
gitdir-file-path: /README.md
date: '2021-05-07 16:09:33'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/deep-diamond/dd-minimal-lein/README.md
tags:
- README.md
---
A minimal deep diamond project for experimentation
