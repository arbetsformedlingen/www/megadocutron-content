---
title: Download job postings
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/word2vec/-/blob/main//data/README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/word2vec
gitdir-file-path: /data/README.md
date: '2021-06-18 16:27:03'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/word2vec/data/README.md
tags:
- data::README.md
- README.md
---
## Download job postings

``` shell
wget https://simonbe.blob.core.windows.net/afhistorik/pb_2006_2020.rar

```

``` shell
chmod +x pb_2006_2020
unrar e pb_2006_2020.rar
```
