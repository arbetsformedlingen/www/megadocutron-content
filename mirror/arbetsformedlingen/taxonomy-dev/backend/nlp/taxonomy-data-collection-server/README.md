---
title: Taxonomy ML Data Collection Server
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-data-collection-server/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-data-collection-server
gitdir-file-path: /README.md
date: '2020-10-22 13:56:30'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-data-collection-server/README.md
tags:
- README.md
---
# Taxonomy ML Data Collection Server
A prototype server that collects data for machine-learning algorithms.

## Build

```
stack build
```
## Run

```
stack exec taxonomy-data-collection-server
```

## Test

```
curl http://localhost:8080/users
```
