---
title: Sun Concepts
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/sun-concepts/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/sun-concepts
gitdir-file-path: /README.md
date: '2023-10-30 14:03:39'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/sun-concepts/README.md
tags:
- README.md
---
# Sun Concepts

This repository contains possible relations between sun concepts and other concepts in Taxonomy to help Redaction Team. We used Sentence-BERT model to find relations between them.

## Set up environment

- Make sure python 3.11 is installed.
- Make sure pip is installed.
- Set up a venv (IntelliJ or similar environment):
  - File -> Project Structure -> SDKs -> + -> Add Python SDK... -> Virtual ENV Environment
- Install dependencies:
  `pip install - r requirements.txt`

## Expected result:

`sun_connected_concepts.ods` is the expected result when you run `main.py`.
