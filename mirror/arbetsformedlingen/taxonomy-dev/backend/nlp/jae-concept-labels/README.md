---
title: jae-concept-labels
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-concept-labels/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-concept-labels
gitdir-file-path: /README.md
date: '2022-07-21 10:33:33'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-concept-labels/README.md
tags:
- README.md
---
# jae-concept-labels

Det här projektet handlar om att knyta ihop två begreppsstrukturer *taxonomin* och *synonymordlistan*.

## Bakgrund

Inom JobTech har vi två som fyller olika syften: *taxonomin* och *synonymordlistan*.

**Taxonomin** är en trädstruktur med bredare och smalare begrepp för att koda betydelsen av text med en standardiserad terminologi. Taxonomin används bland annat för att koda innehållet i platsannonser. Varje begrepp i taxonomin har åtminstone ett *id* som identifierar begreppet, en *typ* som kodar vilken sorts begrepp det är och en *preferred label* som är en kortfattad etikett på begreppet.

**Synonymordlistan** utvecklades främst för maskinell analys av text i platsannonser. Synonymordlistan består av *grupper* av ord som betyder samma sak oavsett hur de böjs. Exempelvis finns en grupp av synonymer för allt som betyder något med "personlig assistent". Där ingår bland annat textsträngen "personlig*a* assistent*er*" som är en annan böjning. På så vis kan en platsannons som innehåller strängen "personliga assistenter" matchas mot en sökning på "personlig assistent" i Platsbanken.

Problemet är att vi inte har någon koppling mellan begreppen i taxonomin och ord i synonymordlistan. Syftet med det här projektet är att bygga ett verktyg för att hjälpa till att skapa den kopplingen genom att leta upp synonymordlistans ord i taxonomin-begreppens etiketter (*preferred label* och *alternative labels*). Nedan följer en steg-för-steg-anvisning för att göra det men du får gärna avvika från den!

För att göra kopplingen mellan orden i synonymordlistan och begreppen kommer vi att använda oss av API:et Jobad Enrichments som hittar ord från synonymordlistan i godtycklig text.

## Steg-för-steg

För att göra det lätt att komma igång har jag lagt in ett första litet skript `main.py` med lite exempelkod.

1. Börja med att ladda ned det här projektet *eller* skapa en ny mapp på datorn där du lägger projektet.
2. Kontrollera att du har en fungerande Python-miljö som du är bekväm med (Python3-interpretatorn, en textredigerare t.ex. VSCode).
3. Ladda ner hela taxonomin från [https://data.jobtechdev.se/taxonomy/begrepp-och-vanliga-relationer.json](https://data.jobtechdev.se/taxonomy/begrepp-och-vanliga-relationer.json).
4. Läs in taxonomin i ett Python-skript. Jag skapade funktionen `demo_concepts` i filen [`main.py`](main.py) med lite exempelkod på hur du läser in taxonomin och kommer åt de olika värdena för ett begrepp. Du kan köra filen enkelt genom att anropa `python3 main.py` från kommandoraden.
5. Nu ska vi titta på hur man kan använda API:et *jobad-enrichments* för att hitta synonymord i etiketterna för ett begrepp. Se funktionen `demo_jobad_enrichments` i filen [`main.py`](main.py) för ett kortfattat exempel på hur man använder API:et. Där finns också lite förklaringar till hur du kodar meddelandet till API:et och hur du avkodar svaret.
6. Använda API:et jobad-enrichments för att analysera alla etiketter (`preferred_label` och `alternative_labels`) för alla begrepp. Varje gång du får en träff mellan ett begrepp och ett synonymord, spara den kopplingen i t.ex. en pythonlista.
7. Spara alla kopplingar som du hittade i en fil på lämpligt format, t.ex. CSV eller JSON. För varje koppling kan du inkludera följande värden:
   * Begreppets id, t.ex. `15DU_axL_nRT`
   * Etiketten som synonymen kopplades mot, t.ex. `Klottersanerare`
   * Typ av etikett (preferred-label eller alternative-label): `preferred-label`
   * Nyckel/namn för synonymgruppen (värdet under `concept_label` i API-svaret): `Klottersanerare`
   * Typ av begrepp: `occupations`

Spara även alla begrepp för vilka du *inte* hittade någon koppling mot synonymordlistan i en separat fil. 
8. **Bonus I**: Presentera resultatet av kopplingen på ett lättöverskådligt sätt, kanske med lite statistik på t.ex. hur många begrepp som hittades utan någon koppling mot synonymordlistan, antal begrepp kopplade mot exakt ett synonymord, o.s.v. Låt fantasin flöda. Du kan presentera det här hur du vill, t.ex. som vanlig text som du skriver ut i terminalen, eller kanske som en markdown- eller htmlfil som man enkelt kan läsa.
9. **Bonus II**: Observera att kopplingarna bildar en [bipartit graf](https://sv.wikipedia.org/wiki/Bipartit_graf) mellan taxonomibegreppen och synonymordlistan. Hitta alla sammanhängande komponenter i grafen och prestentera dem på lämpligt sätt.


