---
title: Results for '73fbdd8ef5cb9f2fbfdfb546f4ac83c9e8308e55'
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/-/blob/main//data/report/mentor-api-prod__splitclassvalidation/73fbdd8ef5cb9f2fbfdfb546f4ac83c9e8308e55/README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec
gitdir-file-path: /data/report/mentor-api-prod__splitclassvalidation/73fbdd8ef5cb9f2fbfdfb546f4ac83c9e8308e55/README.md
date: '2022-12-08 08:06:12'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/73fbdd8ef5cb9f2fbfdfb546f4ac83c9e8308e55/README.md
tags:
- data::report::mentor-api-prod__splitclassvalidation::73fbdd8ef5cb9f2fbfdfb546f4ac83c9e8308e55::README.md
- report::mentor-api-prod__splitclassvalidation::73fbdd8ef5cb9f2fbfdfb546f4ac83c9e8308e55::README.md
- mentor-api-prod__splitclassvalidation::73fbdd8ef5cb9f2fbfdfb546f4ac83c9e8308e55::README.md
- 73fbdd8ef5cb9f2fbfdfb546f4ac83c9e8308e55::README.md
- README.md
---
# Results for '73fbdd8ef5cb9f2fbfdfb546f4ac83c9e8308e55'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [73fbdd8ef5cb9f2fbfdfb546f4ac83c9e8308e55](README.md) | 1 | 114 | 2 | 3 | 7/44 = **16%** | 1/4 = **25%** |

## Source text

Köksbiträden God fysisk kondition, snabba rörelser, tidskoncept, ansvarskänsla och laganda.Man kan prata svenska .

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Köksbiträden** God fysisk kondition, snabba ... |  | x |  | 12 | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| 1 | **Köksbiträden** God fysisk kondition, snabba ... | x |  |  | 12 | [Restaurang- och köksbiträden m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/tPox_ie4_X9X) |
| 2 | ...snabba rörelser, tidskoncept, **ansvarskänsla** och laganda.Man kan prata sve... |  | x |  | 13 | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
| 3 | ...sla och laganda.Man kan prata **svenska** . | x | x | 7 | 7 | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | **Overall** | | | **7** | **44** | 7/44 = **16%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
|  | x |  | [visa ansvarstagande, **esco-skill**](http://data.jobtechdev.se/taxonomy/concept/bcRp_hxV_f5G) |
|  | x |  | [Köksbiträde, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/oRB5_b7i_Qzv) |
| x |  |  | [Restaurang- och köksbiträden m.fl., **ssyk-level-4**](http://data.jobtechdev.se/taxonomy/concept/tPox_ie4_X9X) |
| x | x | x | [Svenska, **language**](http://data.jobtechdev.se/taxonomy/concept/zSLA_vw2_FXN) |
| | | **1** | 1/4 = **25%** |