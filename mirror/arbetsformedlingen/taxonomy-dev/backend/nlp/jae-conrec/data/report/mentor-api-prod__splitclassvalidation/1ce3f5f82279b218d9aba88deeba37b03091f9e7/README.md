---
title: Results for '1ce3f5f82279b218d9aba88deeba37b03091f9e7'
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/-/blob/main//data/report/mentor-api-prod__splitclassvalidation/1ce3f5f82279b218d9aba88deeba37b03091f9e7/README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec
gitdir-file-path: /data/report/mentor-api-prod__splitclassvalidation/1ce3f5f82279b218d9aba88deeba37b03091f9e7/README.md
date: '2022-12-08 08:06:12'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/1ce3f5f82279b218d9aba88deeba37b03091f9e7/README.md
tags:
- data::report::mentor-api-prod__splitclassvalidation::1ce3f5f82279b218d9aba88deeba37b03091f9e7::README.md
- report::mentor-api-prod__splitclassvalidation::1ce3f5f82279b218d9aba88deeba37b03091f9e7::README.md
- mentor-api-prod__splitclassvalidation::1ce3f5f82279b218d9aba88deeba37b03091f9e7::README.md
- 1ce3f5f82279b218d9aba88deeba37b03091f9e7::README.md
- README.md
---
# Results for '1ce3f5f82279b218d9aba88deeba37b03091f9e7'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [1ce3f5f82279b218d9aba88deeba37b03091f9e7](README.md) | 1 | 4 | 1 | 0 | 0/0 = **100%** | 0/0 = **100%** |

## Source text

Tert

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| | **Overall** | | | **0** | **0** | 0/0 = **100%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| | | **0** | 0/0 = **100%** |