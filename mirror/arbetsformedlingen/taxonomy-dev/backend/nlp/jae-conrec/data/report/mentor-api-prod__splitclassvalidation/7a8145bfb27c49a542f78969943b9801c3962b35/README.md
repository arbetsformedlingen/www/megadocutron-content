---
title: Results for '7a8145bfb27c49a542f78969943b9801c3962b35'
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/-/blob/main//data/report/mentor-api-prod__splitclassvalidation/7a8145bfb27c49a542f78969943b9801c3962b35/README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec
gitdir-file-path: /data/report/mentor-api-prod__splitclassvalidation/7a8145bfb27c49a542f78969943b9801c3962b35/README.md
date: '2022-12-08 08:06:12'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/7a8145bfb27c49a542f78969943b9801c3962b35/README.md
tags:
- data::report::mentor-api-prod__splitclassvalidation::7a8145bfb27c49a542f78969943b9801c3962b35::README.md
- report::mentor-api-prod__splitclassvalidation::7a8145bfb27c49a542f78969943b9801c3962b35::README.md
- mentor-api-prod__splitclassvalidation::7a8145bfb27c49a542f78969943b9801c3962b35::README.md
- 7a8145bfb27c49a542f78969943b9801c3962b35::README.md
- README.md
---
# Results for '7a8145bfb27c49a542f78969943b9801c3962b35'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [7a8145bfb27c49a542f78969943b9801c3962b35](README.md) | 1 | 264 | 4 | 2 | 14/36 = **39%** | 1/3 = **33%** |

## Source text

Diskare  Hej Jag söker en diskare fast anställning heltid  från onsdag till söndag  från kl 15.00 till 23.00,,, Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Diskare**  Hej Jag söker en diskare fas... | x | x | 7 | 7 | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| 2 | Diskare  Hej Jag söker en **diskare** fast anställning heltid  från... | x | x | 7 | 7 | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| 3 | ...are  Hej Jag söker en diskare **fast anställning** heltid  från onsdag till sönd... | x |  |  | 16 | [Fast anställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/oV9d_Jxy_x14) |
| 4 | ...r en diskare fast anställning **heltid**  från onsdag till söndag  frå... | x |  |  | 6 | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| | **Overall** | | | **14** | **36** | 14/36 = **39%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [Heltid, **worktime-extent**](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) |
| x |  |  | [Fast anställning, **keyword**](http://data.jobtechdev.se/taxonomy/concept/oV9d_Jxy_x14) |
| x | x | x | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| | | **1** | 1/3 = **33%** |