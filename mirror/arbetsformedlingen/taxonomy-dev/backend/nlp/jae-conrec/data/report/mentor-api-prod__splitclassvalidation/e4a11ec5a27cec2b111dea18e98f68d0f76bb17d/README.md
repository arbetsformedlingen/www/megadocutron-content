---
title: Results for 'e4a11ec5a27cec2b111dea18e98f68d0f76bb17d'
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/-/blob/main//data/report/mentor-api-prod__splitclassvalidation/e4a11ec5a27cec2b111dea18e98f68d0f76bb17d/README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec
gitdir-file-path: /data/report/mentor-api-prod__splitclassvalidation/e4a11ec5a27cec2b111dea18e98f68d0f76bb17d/README.md
date: '2022-12-08 08:06:12'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/e4a11ec5a27cec2b111dea18e98f68d0f76bb17d/README.md
tags:
- data::report::mentor-api-prod__splitclassvalidation::e4a11ec5a27cec2b111dea18e98f68d0f76bb17d::README.md
- report::mentor-api-prod__splitclassvalidation::e4a11ec5a27cec2b111dea18e98f68d0f76bb17d::README.md
- mentor-api-prod__splitclassvalidation::e4a11ec5a27cec2b111dea18e98f68d0f76bb17d::README.md
- e4a11ec5a27cec2b111dea18e98f68d0f76bb17d::README.md
- README.md
---
# Results for 'e4a11ec5a27cec2b111dea18e98f68d0f76bb17d'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [e4a11ec5a27cec2b111dea18e98f68d0f76bb17d](README.md) | 1 | 293 | 2 | 3 | 16/23 = **70%** | 2/2 = **100%** |

## Source text

Service/Bartender Bierhuis Uppsala Bierhuis uppsala is looking for new bar staff!!! If you have experience, are enthusiastic and a good team player, English speaking, willing to learn and improve you might be perfect for us.  Please email us your CV or simply drop in the bar. info@bierhuis.se

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | Service/**Bartender** Bierhuis Uppsala Bierhuis upp... | x | x | 9 | 9 | [bartender, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tr5M_uUa_wdz) |
| 2 | Service/Bartender Bierhuis **Uppsala** Bierhuis uppsala is looking f... | x | x | 7 | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| 3 | ...der Bierhuis Uppsala Bierhuis **uppsala** is looking for new bar staff!... |  | x |  | 7 | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| | **Overall** | | | **16** | **23** | 16/23 = **70%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Uppsala, **municipality**](http://data.jobtechdev.se/taxonomy/concept/otaF_bQY_4ZD) |
| x | x | x | [bartender, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/tr5M_uUa_wdz) |
| | | **2** | 2/2 = **100%** |