---
title: Results for '0914e918751875b885a1bbf52d0545519a2a4e5e'
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/-/blob/main//data/report/mentor-api-prod__splitclassvalidation/0914e918751875b885a1bbf52d0545519a2a4e5e/README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec
gitdir-file-path: /data/report/mentor-api-prod__splitclassvalidation/0914e918751875b885a1bbf52d0545519a2a4e5e/README.md
date: '2022-12-08 08:06:12'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0914e918751875b885a1bbf52d0545519a2a4e5e/README.md
tags:
- data::report::mentor-api-prod__splitclassvalidation::0914e918751875b885a1bbf52d0545519a2a4e5e::README.md
- report::mentor-api-prod__splitclassvalidation::0914e918751875b885a1bbf52d0545519a2a4e5e::README.md
- mentor-api-prod__splitclassvalidation::0914e918751875b885a1bbf52d0545519a2a4e5e::README.md
- 0914e918751875b885a1bbf52d0545519a2a4e5e::README.md
- README.md
---
# Results for '0914e918751875b885a1bbf52d0545519a2a4e5e'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [0914e918751875b885a1bbf52d0545519a2a4e5e](README.md) | 1 | 363 | 1 | 1 | 7/7 = **100%** | 1/1 = **100%** |

## Source text

Diskare / kökshjälp  Vi söker en person som kan jobba till största tid  dagtid men även ibland vid kvällsbokningar. Personen ska vara ansvarig för disken men även kunna hjälpa till med lättare sysslor i köket.  Öppen för alla Vi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Diskare** / kökshjälp  Vi söker en pers... | x | x | 7 | 7 | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| | **Overall** | | | **7** | **7** | 7/7 = **100%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x | x | x | [Diskare, **occupation-name**](http://data.jobtechdev.se/taxonomy/concept/pKTY_Kwu_sUw) |
| | | **1** | 1/1 = **100%** |