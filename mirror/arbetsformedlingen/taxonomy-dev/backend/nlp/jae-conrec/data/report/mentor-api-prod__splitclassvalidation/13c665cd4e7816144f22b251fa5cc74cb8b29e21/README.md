---
title: Results for '13c665cd4e7816144f22b251fa5cc74cb8b29e21'
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/-/blob/main//data/report/mentor-api-prod__splitclassvalidation/13c665cd4e7816144f22b251fa5cc74cb8b29e21/README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec
gitdir-file-path: /data/report/mentor-api-prod__splitclassvalidation/13c665cd4e7816144f22b251fa5cc74cb8b29e21/README.md
date: '2022-12-08 08:06:12'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/13c665cd4e7816144f22b251fa5cc74cb8b29e21/README.md
tags:
- data::report::mentor-api-prod__splitclassvalidation::13c665cd4e7816144f22b251fa5cc74cb8b29e21::README.md
- report::mentor-api-prod__splitclassvalidation::13c665cd4e7816144f22b251fa5cc74cb8b29e21::README.md
- mentor-api-prod__splitclassvalidation::13c665cd4e7816144f22b251fa5cc74cb8b29e21::README.md
- 13c665cd4e7816144f22b251fa5cc74cb8b29e21::README.md
- README.md
---
# Results for '13c665cd4e7816144f22b251fa5cc74cb8b29e21'

| Title | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [13c665cd4e7816144f22b251fa5cc74cb8b29e21](README.md) | 1 | 448 | 4 | 4 | 25/56 = **45%** | 2/4 = **50%** |

## Source text

Snickare Vi behöver nu en snickare med minst 5 års erfarenhet. Arbetsplatsen ligger i Stockholm Pågående och kommande stora projekt med tillträde är omgående. Tjänsten innebär att du självgående skall kunna arbeta med traditionellt förekommande snickeriarbeten inom nyproduktion, renovering.   Vi samarbetar enbart med stora aktörer inom byggnads som tänker på kvalité och miljö, samt som ger fina villkor och utvecklingsmöjlighet för de anställda.

## Concept recognition

### Character-wise comparison

| Interval # | Text | Truth | Predicted | Overlap | Width | Class |
|-|-|-|-|-|-|-|
| 1 | **Snickare** Vi behöver nu en snickare med... | x | x | 8 | 8 | [snickare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qhAJ_Euo_tNx) |
| 2 | Snickare Vi behöver nu en **snickare** med minst 5 års erfarenhet. A... | x | x | 8 | 8 | [snickare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qhAJ_Euo_tNx) |
| 3 | ...över nu en snickare med minst **5 års erfarenhet**. Arbetsplatsen ligger i Stock... | x |  |  | 16 | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| 4 | ...enhet. Arbetsplatsen ligger i **Stockholm** Pågående och kommande stora p... | x | x | 9 | 9 | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
| 5 | ...ed traditionellt förekommande **snickeriarbeten** inom nyproduktion, renovering... |  | x |  | 15 | [Snickeriarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jB4B_uED_vQh) |
| | **Overall** | | | **25** | **56** | 25/56 = **45%** |

### Class-wise comparison

| Truth | Predicted | Overlap | Class |
|-|-|-|-|
| x |  |  | [5 års erfarenhet eller mer, **occupation-experience-year**](http://data.jobtechdev.se/taxonomy/concept/8CVf_zRX_aZj) |
| x | x | x | [Stockholm, **municipality**](http://data.jobtechdev.se/taxonomy/concept/AvNB_uwa_6n6) |
|  | x |  | [Snickeriarbetare, **job-title**](http://data.jobtechdev.se/taxonomy/concept/jB4B_uED_vQh) |
| x | x | x | [snickare, **esco-occupation**](http://data.jobtechdev.se/taxonomy/concept/qhAJ_Euo_tNx) |
| | | **2** | 2/4 = **50%** |