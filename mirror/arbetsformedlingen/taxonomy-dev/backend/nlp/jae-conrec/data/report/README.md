---
title: Report for annotated documents
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/-/blob/main//data/report/README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec
gitdir-file-path: /data/report/README.md
date: '2022-12-08 08:06:12'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/README.md
tags:
- data::report::README.md
- report::README.md
- README.md
---
# Report for annotated documents

| Dataset | Document count | Text length | True annotations | Reported annotations | **conrec Charwise Jaccard** | **conrec Overall Jaccard** |
|-|-|-|-|-|-|-|
| [mentor-api-prod: validation](mentor-api-prod__splitclassvalidation/README.md) | 341 | 779348 | 5332 | 4942 | 26790/86481 = **31%** | 1920/5132 = **37%** |
| **Total** | 341 | 779348 | 5332 | 4942 | 26790/86481 = **31%** | 1920/5132 = **37%** |