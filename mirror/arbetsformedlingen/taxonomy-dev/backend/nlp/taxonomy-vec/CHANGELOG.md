---
title: Change Log
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-vec/-/blob/main//CHANGELOG.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-vec
gitdir-file-path: /CHANGELOG.md
date: '2021-07-02 12:06:18'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-vec/CHANGELOG.md
tags:
- CHANGELOG.md
---
# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [Unreleased]
### Changed
- Add a new arity to `make-widget-async` to provide a different widget shape.

## [0.1.1] - 2021-02-15
### Changed
- Documentation on how to make the widgets.

### Removed
- `make-widget-sync` - we're all async, all the time.

### Fixed
- Fixed widget maker to keep working when daylight savings switches over.

## 0.1.0 - 2021-02-15
### Added
- Files from the new template.
- Widget maker public API - `make-widget-sync`.

[Unreleased]: https://github.com/your-name/taxonomy-vec/compare/0.1.1...HEAD
[0.1.1]: https://github.com/your-name/taxonomy-vec/compare/0.1.0...0.1.1
