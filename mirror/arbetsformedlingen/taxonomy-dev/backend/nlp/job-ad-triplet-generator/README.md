---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/job-ad-triplet-generator/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/job-ad-triplet-generator
gitdir-file-path: /README.md
date: '2022-07-21 15:22:06'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/job-ad-triplet-generator/README.md
tags:
- README.md
---
Skapar tripler av historiska jobbannonser genom dataseten från https://data.jobtechdev.se/annonser/historiska/berikade/kompletta/.

I dessa json-filer finns nyckelord till jobbannonser extraherade. Triplets.py använder nyckelorden till att skapa tripler baserade på Jaccard-index. Varje tripel består av tre jobbannonser:

Ankare: En slumpmässigt vald jobbannons, t.ex. en platsannons för att jobba som redovisningskonsult.
Positivt exempel: En jobbannons som visserligen är slumpmässigt vald, men med villkoret att den ska ligga nära ankaret. Exempelvis en platsannons för att jobba som löneadministratör som också är ett kontorsjobb som har med ekonomi att göra.
Ett negativt exempel: En jobbannons som ligger längre ifrån "redovisningskonsult" än vad "löneadministratör" gör. Det skulle kunna vara något utomhusarbete som inte har med ekonomi att göra, förslagsvis en annons för trädgårdsarbete.

Algoritmen för att skapa dessa tripler är följande:
- Läs in en bestämd mängd data från datasetet med jobbannonser.
- Välj ett slumpmässigt urval annonser av längd k från den inlästa datan.
- Sätt en gräns på lägst antal nyckelord en annons måste innehålla.
- Sätt den första annonsen till att vara ankaret, så tillvida den innehåller tillräckligt många nyckelord. Om inte välj ett nytt urval.
- Jämför resten av annonserna i urvalets nyckelord med ankarets och beräkna Jaccard-indexet. 
- Är Jaccard-index högt, används annonsen som positivt exempel i triplen.
- Är Jaccard-indexet lågt, används annonsen som negativt exempel i triplen.
- Skriv sedan exemplen till en csv-fil.
- Upprepa n gånger.

Med ett lägsta antal nyckelord på 3, slumpmässigt urval på k=1000, och antal iterationen n=10000 skapas ca 35000 tripler.

