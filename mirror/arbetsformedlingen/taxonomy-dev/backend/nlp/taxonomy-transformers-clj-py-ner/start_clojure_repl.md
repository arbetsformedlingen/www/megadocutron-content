---
title: First source the  virtual environment
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-transformers-clj-py-ner/-/blob/main//start_clojure_repl.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-transformers-clj-py-ner
gitdir-file-path: /start_clojure_repl.md
date: '2023-02-03 17:00:17'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-transformers-clj-py-ner/start_clojure_repl.md
tags:
- start_clojure_repl.md
---
## First source the  virtual environment

source venv/bin/activate 

## then start main.py

python main.py
