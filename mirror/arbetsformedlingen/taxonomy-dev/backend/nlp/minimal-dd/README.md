---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/minimal-dd/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/minimal-dd
gitdir-file-path: /README.md
date: '2021-05-05 11:46:15'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/minimal-dd/README.md
tags:
- README.md
---
This project is a minimal setup for deep diamond.

You need to add `(set! *print-length* 128)` to avoid Nullpointer excception when printing tensors.
