---
title: ESCO Skills to Taxonomy Skills
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/esco-skills-to-taxonomy-skills/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/nlp/esco-skills-to-taxonomy-skills
gitdir-file-path: /README.md
date: '2023-10-30 11:01:46'
path: /arbetsformedlingen/taxonomy-dev/backend/nlp/esco-skills-to-taxonomy-skills/README.md
tags:
- README.md
---
# ESCO Skills to Taxonomy Skills
This repository contains mapping ESCO Skills to Taxonomy Skills to help Redaction Team to easily find out which
ESCO Skills have a path to Taxonomy Skills and which one not.


## Set up environment

- Make sure python 3.11 is installed.
- Make sure pip is installed.
- Set up a venv (IntelliJ or similar environment):
    - File -> Project Structure -> SDKs -> + -> Add Python SDK... -> Virtual ENV Environment
- Install dependencies:
  ```pip install - r requirements.txt```

### Expected result:
```escoskill_skill_output.ods``` is the expected result when you run ```main.py```.
