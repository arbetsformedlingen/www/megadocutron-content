---
title: jobtechdev/taxonomy-database-dist
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-database-dist/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/taxonomy-database-dist
gitdir-file-path: /README.md
date: '2023-10-02 13:11:12'
path: /arbetsformedlingen/taxonomy-dev/backend/taxonomy-database-dist/README.md
tags:
- README.md
---
# jobtechdev/taxonomy-database-dist

This library holds taxonomy data in the nippy format.
