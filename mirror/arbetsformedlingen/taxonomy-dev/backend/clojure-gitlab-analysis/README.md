---
title: clojure-gitlab-analysis
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/clojure-gitlab-analysis/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/clojure-gitlab-analysis
gitdir-file-path: /README.md
date: '2023-04-26 09:56:43'
path: /arbetsformedlingen/taxonomy-dev/backend/clojure-gitlab-analysis/README.md
tags:
- README.md
---
# clojure-gitlab-analysis


to run this tool you must install Babashka or run docker image.

bb -m analyse "path-to-clojure-repo-you-want-to-analyse"
ex: 
`bb -m analyse ../jobtech-taxonomy-api`

this will output a report file `gl-code-quality-report.json` in the same folder that you run the script from

To push new container image after making commit in this repo: 
1. podman login registry.gitlab.com
2. podman build -t registry.gitlab.com/arbetsformedlingen/taxonomy-dev/backend/clojure-gitlab-analysis .
3. podman push registry.gitlab.com/arbetsformedlingen/taxonomy-dev/backend/clojure-gitlab-analysis
