---
title: jobtech-taxonomy-database
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-database/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-database
gitdir-file-path: /README.md
date: '2023-10-04 09:51:54'
path: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-database/README.md
tags:
- README.md
---
# jobtech-taxonomy-database

Taxonomy database management utilities for JobTech

## ENV
    Add an environment variable named "jobtechtaxonomy" to "PROD" if you want to use the prod datomic

## Creating fresh database

Use `./create-database.sh dev|prod|frontend`

## Adding schema to a fresh database

Use `./setup-database.sh jobtech-taxonomy-...`

## Installation

Download a copy of the legacy database here:
https://gitlab.com/af-group/ams-taxonomy-backup
Follow the instructions in the readme to start local SQL database server

## License

Copyright © 2020 JobTech

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
