---
title: Dotnet Taxonomy Library
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries/-/blob/main//jobtech-taxonomy-library-clr/README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries
gitdir-file-path: /jobtech-taxonomy-library-clr/README.md
date: '2023-05-24 15:38:20'
path: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries/jobtech-taxonomy-library-clr/README.md
tags:
- jobtech-taxonomy-library-clr::README.md
- README.md
---
# Dotnet Taxonomy Library


`dotnet watch test --project src/JobtechTaxonomy.Tests/JobtechTaxonomy.Tests.fsproj`
