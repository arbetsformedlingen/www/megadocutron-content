---
title: Rust Taxonomy Library
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries/-/blob/main//jobtech-taxonomy-library-rust/README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries
gitdir-file-path: /jobtech-taxonomy-library-rust/README.md
date: '2023-05-24 15:38:20'
path: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries/jobtech-taxonomy-library-rust/README.md
tags:
- jobtech-taxonomy-library-rust::README.md
- README.md
---
# Rust Taxonomy Library
