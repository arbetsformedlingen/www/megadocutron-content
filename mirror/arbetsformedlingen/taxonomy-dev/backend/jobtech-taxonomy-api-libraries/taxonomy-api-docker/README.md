---
title: Docker Hub
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries/-/blob/main//taxonomy-api-docker/README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries
gitdir-file-path: /taxonomy-api-docker/README.md
date: '2023-05-24 15:38:20'
path: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries/taxonomy-api-docker/README.md
tags:
- taxonomy-api-docker::README.md
- README.md
---
# Docker Hub

Load a nippy file of the desired taxonomy version into a container based on the latest production image. Then deploy to Docker Hub.
