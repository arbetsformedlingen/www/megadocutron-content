---
title: Taxonomy Libraries
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries
gitdir-file-path: /README.md
date: '2023-05-24 15:38:20'
path: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries/README.md
tags:
- README.md
---
# Taxonomy Libraries

Library versions of the Taxonomy to common library repositories.

## Usage

To perform a full deploy, call

```
clj -T:build deploy
```

But you may find it more useful to perform the deploy one step at a time, that is

```
clj -T:build get-taxonomy-data
clj -T:build build-libraries
```

To run the tests, call

```
clj -M:build:test
```

## Targets

Here are some platforms that we can target.

| Library name                    | Platform            | Languages consuming the library    | Package managers |
|---------------------------------|---------------------|------------------------------------|------------------|
| jobtech-taxonomy-library-python | Python 3            | Python 3                           | PyPI             |
| jobtech-taxonomy-library-jre    | JRE (Java platform) | Java, Clojure, Scala, Kotlin, etc. | Maven            |
| jobtech-taxonomy-library-clr    | CLR (.Net platform) | C#, F#, etc.                       | Nuget            |
| jobtech-taxonomy-library-rust   | Rust                | Rust                               | Crates           |

### Python

*Usage:* If you are using Poetry, include the following line in your `pyproject.toml` file (but the *commit hash* needs to be edited):

```
jobtech-taxonomy-library-python = { git = "git@gitlab.com:arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries.git", rev = "839412d9139d16178ce3818436ebf45b5b87e568", subdirectory = "jobtech-taxonomy-library-python"}
```

#### Code overview

The Python libraries are located at [`jobtech-taxonomy-library-python/`][110].

From that directory, you can check that the generated libraries work using **`make validate-implementation`**.

All the *generated* code is produced at [`jobtech-taxonomy-library-python/jobtech_taxonomy_library_python/generated/`][111]. There, you will find the following files:

| File                        | Explanation                                                                                     |
|-----------------------------|-------------------------------------------------------------------------------------------------|
| [models.py][100]            | Data type definitions for the taxonomy, concepts and concept types.                             |
| [accessors.py][101]         | Data types for accessing fields of concepts and concept types.                                  |
| [defs.py][102]              | Definitions of important values. Notably, it defines `taxonomy` containing the entire taxonomy. |
| [data.py][103]              | Generated data that makes up the contents of the taxonomy.                                      |
| [item_accumulators.py][104] | Helper objects for building the taxonomy.                                                       |

## Docker

For each version of the taxonomy, bake a nippy version of that into the latest image of the taxonomy-api and deploy to Docker Hub.

This will enable users to deploy their own taxonomy-api (with local editing capabilities) wherever they want.

## Dependencies

* A working [Clojure installation][1] along with [Java][2]

[1]: https://clojure.org/guides/install_clojure
[2]: https://clojure.org/guides/install_clojure#java
[100]: jobtech-taxonomy-library-python/jobtech_taxonomy_library_python/generated/models.py
[101]: jobtech-taxonomy-library-python/jobtech_taxonomy_library_python/generated/accessors.py
[102]: jobtech-taxonomy-library-python/jobtech_taxonomy_library_python/generated/defs.py
[103]: jobtech-taxonomy-library-python/jobtech_taxonomy_library_python/generated/data.py
[104]: jobtech-taxonomy-library-python/jobtech_taxonomy_library_python/generated/item_accumulators.py
[110]: jobtech-taxonomy-library-python/
[111]: jobtech-taxonomy-library-python/jobtech_taxonomy_library_python/generated/
