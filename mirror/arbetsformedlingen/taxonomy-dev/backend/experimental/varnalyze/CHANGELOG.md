---
title: Change Log
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/experimental/varnalyze/-/blob/main//CHANGELOG.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/experimental/varnalyze
gitdir-file-path: /CHANGELOG.md
date: '2023-08-11 08:31:53'
path: /arbetsformedlingen/taxonomy-dev/backend/experimental/varnalyze/CHANGELOG.md
tags:
- CHANGELOG.md
---
# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [Unreleased]
### Changed
- Add a new arity to `make-widget-async` to provide a different widget shape.

## [0.1.1] - 2023-08-08
### Changed
- Documentation on how to make the widgets.

### Removed
- `make-widget-sync` - we're all async, all the time.

### Fixed
- Fixed widget maker to keep working when daylight savings switches over.

## 0.1.0 - 2023-08-08
### Added
- Files from the new template.
- Widget maker public API - `make-widget-sync`.

[Unreleased]: https://sourcehost.site/your-name/varnalyze/compare/0.1.1...HEAD
[0.1.1]: https://sourcehost.site/your-name/varnalyze/compare/0.1.0...0.1.1
