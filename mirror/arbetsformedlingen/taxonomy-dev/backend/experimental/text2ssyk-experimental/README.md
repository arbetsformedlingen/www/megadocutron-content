---
title: Usage
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/experimental/text2ssyk-experimental/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/experimental/text2ssyk-experimental
gitdir-file-path: /README.md
date: '2023-10-05 13:24:03'
path: /arbetsformedlingen/taxonomy-dev/backend/experimental/text2ssyk-experimental/README.md
tags:
- README.md
---
## Usage

First, install dependencies:

```
poetry install
```

But Sentence-Transformers may have to be dealt with specially:
```
poetry shell
pip3 install sentence-transformers
```
