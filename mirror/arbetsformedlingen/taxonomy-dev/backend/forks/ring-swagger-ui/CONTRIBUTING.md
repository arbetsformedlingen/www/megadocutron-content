---
title: How to contribute
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/forks/ring-swagger-ui/-/blob/main//CONTRIBUTING.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/forks/ring-swagger-ui
gitdir-file-path: /CONTRIBUTING.md
date: '2022-09-22 20:58:08'
path: /arbetsformedlingen/taxonomy-dev/backend/forks/ring-swagger-ui/CONTRIBUTING.md
tags:
- CONTRIBUTING.md
---
# How to contribute

Contributions are welcome, but please don't send Pull requests which just update
the version. For updates, create issue and we'll handle the update ourselves.

Please file bug reports and feature requests to https://github.com/metosin/ring-swagger-ui/issues.

## Commit messages

1. Separate subject from body with a blank line
2. Limit the subject line to 50 characters
3. Capitalize the subject line
4. Do not end the subject line with a period
5. Use the imperative mood in the subject line
    - "Add x", "Fix y", "Support z", "Remove x"
6. Wrap the body at 72 characters
7. Use the body to explain what and why vs. how

For comprehensive explanation read this [post by Chris Beams](http://chris.beams.io/posts/git-commit/#seven-rules).
