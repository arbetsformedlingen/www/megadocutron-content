---
title: jobtech-cljutils
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-cljutils/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/jobtech-cljutils
gitdir-file-path: /README.md
date: '2023-11-07 14:12:10'
path: /arbetsformedlingen/taxonomy-dev/backend/jobtech-cljutils/README.md
tags:
- README.md
---
# jobtech-cljutils

A library of utilities to use in Clojure at JobTech

## License

Copyright © 2023 Arbetsförmedlingen JobTech

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
