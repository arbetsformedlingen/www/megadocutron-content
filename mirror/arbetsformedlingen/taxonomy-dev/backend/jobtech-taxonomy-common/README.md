---
title: taxonomy-common
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-common/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-common
gitdir-file-path: /README.md
date: '2022-12-20 16:24:28'
path: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-common/README.md
tags:
- README.md
---
# taxonomy-common

## Installation

Use clj git dependency for this repo, e.g.

```clojure
se.jobtechdev.taxonomy/common {:git/url "https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-common.git"
                               :git/tag "..."
                               :git/sha "..."}
```

See [tags](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-common/-/tags) page
for latest git tags and shas.

## New versions

Run `./release.sh` on master branch to publish a new "release". It will output the latest dependency coordinate.

## License

Licensed under the Eclipse Public License, Version 2.0