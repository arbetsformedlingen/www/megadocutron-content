---
title: SOAP
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/legacy-taxonomy-soap-service-java/-/blob/main//readme.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/legacy-taxonomy-soap-service-java
gitdir-file-path: /readme.md
date: '2023-11-07 14:05:56'
path: /arbetsformedlingen/taxonomy-dev/backend/legacy-taxonomy-soap-service-java/readme.md
tags:
- readme.md
---
## SOAP

How to run application locally
./mvnw spring-boot:run

### Docker

podman build -t soap .

podman run -p 8080:8080 soap

### smoke test

curl -v --request POST --header "Content-Type: application/soap+xml;charset=UTF-8;action=\"urn:ams.se:Taxonomy/GetAllOccupationNames\"" --data '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:urn="urn:ams.se:Taxonomy"> <soap:Body> <urn:GetAllOccupationNames> <urn:languageId>502</urn:languageId>  </urn:GetAllOccupationNames>  </soap:Body> </soap:Envelope>' http://localhost:8080/TaxonomiService.asmx


## probe example

livenessProbe:
        exec:
          command:
            - sh
            - '-c'
            - >-
              curl -X POST http://localhost:8080/TaxonomiService.asmx -H
              'Content-Type: text/xml' -d '<soapenv:Envelope
              xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
              xmlns:urn="urn:ams.se:Taxonomy"> <soapenv:Header/>
              <soapenv:Body><urn:GetAllContinents><languageId>?</languageId></urn:GetAllContinents></soapenv:Body></soapenv:Envelope>'
        timeoutSeconds: 10
        periodSeconds: 20
        successThreshold: 1
        failureThreshold: 3

