---
title: Jobtech Taxonomy Dotnet
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtechtaxonomydotnet6/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/jobtechtaxonomydotnet6
gitdir-file-path: /README.md
date: '2023-10-11 16:54:22'
path: /arbetsformedlingen/taxonomy-dev/backend/jobtechtaxonomydotnet6/README.md
tags:
- README.md
---
# Jobtech Taxonomy Dotnet

To build a package run `dotnet pack` in the root directory of this project
and upload the package `JobtechTaxonomyToEuresEsco.1.21.2.nupkg` in `JobtechTaxonomyDotnet6/JobtechTaxonomy2EuresEsco/bin/Debug/`to https://nexus.jobtechdev.se/

Run the file [curling.sh](curling.sh) to fetch the latest data from the public taxonomy api. The queries are in the `.graphql` files with the corresponding names and the resulting JSON ends up in the `JobtechTaxonomy2EuresEsco` project folder.
