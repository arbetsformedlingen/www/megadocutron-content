---
title: Getting up and running using Podman
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/blob/main//PODMAN.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api
gitdir-file-path: /PODMAN.md
date: '2023-11-10 13:37:11'
path: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/PODMAN.md
tags:
- PODMAN.md
---
# Getting up and running using Podman

Ensure that the `aws-secrets-test.txt` and `api-secrets-test.txt` are present.

`podman build -t jt-api .`
`podman run --network="host" --name api --env-file=../jobtech-taxonomy-api-gitops/jobtech-taxonomy-api-deploy-secrets/test/aws-secrets-test.txt --env-file=../jobtech-taxonomy-api-gitops/jobtech-taxonomy-api-deploy-secrets/test/api-secrets-test.txt --env-file=dev.env --rm jt-api`