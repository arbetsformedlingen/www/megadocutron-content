---
title: Popularity Attribute for Autocomplete
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/blob/main//resources/popularity/README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api
gitdir-file-path: /resources/popularity/README.md
date: '2023-11-10 13:37:11'
path: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/resources/popularity/README.md
tags:
- resources::popularity::README.md
- popularity::README.md
- README.md
---
# Popularity Attribute for Autocomplete

The file freq_data.json contains a map from conceptId to a popularity attribute. This attribute has been calculated
using Historical Jobs, https://jobtechdev.se/docs/apis/historical/. Run all historical jobs through the script
create-freq-dict.py. This creats a pickle file that the script calculate-popularity.py uses to calculate popularity.
The popularity is the mean value of the frequencies of the words used in the preferred label.
