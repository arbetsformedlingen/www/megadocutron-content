---
title: GraphQL Examples
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/blob/main//doc/graphql-examples.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api
gitdir-file-path: /doc/graphql-examples.md
date: '2023-11-10 13:37:11'
path: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/doc/graphql-examples.md
tags:
- doc::graphql-examples.md
- graphql-examples.md
---
# GraphQL Examples


Fetch tree of occupation-field -> ssyk-level-4 -> occupation-name in version 1

```graphql
query MyQuery {
  concepts(type: "occupation-field", version: "1") {
    id
    preferred_label
    type
    narrower (type: "ssyk-level-4"){
      id
      preferred_label
      type
      narrower (type: "occupation-name") {
        id
        preferred_label
        type
      }
    }
    
  }
}
```