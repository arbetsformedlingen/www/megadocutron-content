---
title: Ideas on how to solve comparison between Datomic and Datahike
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/blob/main//doc/CHANGE_PROPOSALS.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api
gitdir-file-path: /doc/CHANGE_PROPOSALS.md
date: '2023-11-10 13:37:11'
path: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/doc/CHANGE_PROPOSALS.md
tags:
- doc::CHANGE_PROPOSALS.md
- CHANGE_PROPOSALS.md
---
# Ideas on how to solve comparison between Datomic and Datahike

We want to be able to measure the differences in response from Datomic and Datahike (and possibly other backends or configurations of backends). The differences that we are most interested in is if they diverge in content and if the time to reply varies greatly between the backends.

The solution is to run the two systems in parallel with a timer, compare and log. How we do this is up for debate.

* We would like the fastest backend to provide the response as soon as possible and then let the comparison run it's course.
* The databases (likely) have divergent transaction IDs and entity IDs, so a straight comparison will fail.
* Given the above we cannot chain queries since we reply with IDs from one implementation and apply them to the other.
* We would like the API level of the system to be invariant regardless of the backend in use.
* One of the backend solutions is the current source of truth.

## Solution 0: Use the OpenShift / OpenSearch system

Instead of modifying the system we deploy an extra pod for each backend to test. We log query run-times and results. A lightweight system is set up to coordinate the queries and collect results.

### Advantages

* No changes to the taxonomy-api code.
* Very scaleable.

### Disadvantages

* Not really possible to run locally.
* We need to change the infrastructure.
* Collecting data from various sources.

## Solution 1: Dynamic var / alter-var-root

We compare the responses on the API level for the HTTP server.

The system configures itself around the `db-backend` definition and if we change this when instantiating a query runner for a backend we would likely be able to change the backend during runtime.

### Advantages

* Probably little code that needs to be rewritten.

### Disadvantages

* Much of the system seems to depend on global state and this may make it difficult to anticipate the behaviour.

## Solution 2: Add database / config as parameter

Similarly to solution 1, we want to compare the responses at the API level.

Here we add a configuration or database parameter to all functions that communicate with the database and rely that instead of some previously defined global state.

Ideally we'd remove all or almost all global state or concentrate it to a few sources.

### Advantages

* Making the code more functional and easier to change.
* Chance to review the system and add tests.
* Reloading the system will be easier and this would streamline development.

### Disadvantages

* Potentially a lot of work.