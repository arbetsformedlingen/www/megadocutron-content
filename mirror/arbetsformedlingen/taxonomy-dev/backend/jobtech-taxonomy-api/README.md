---
title: JobTech Taxonomy API
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api
gitdir-file-path: /README.md
date: '2023-11-10 13:37:11'
path: /arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/README.md
tags:
- README.md
---
# JobTech Taxonomy API  

The JobTech Taxonomy API is a REST API for the [JobTech Taxonomy
Database][0]. The JobTech Taxonomy Database contains terms or phrases used
at the Swedish labour market.

## Prerequisites

To run the default development environment using Datahike as database backend all that is needed is Clojure.

However, in order to run tests against `Datomic dev-local` or use a cloud instance of Datomic as a database backend you will need [some more tools](#extra-tools).

The default development environment comes with a copy of the taxonomy stored in a nippy file. This will be used to seed the system when starting unless configured to do otherwise. The data in the file should in no way be considered authoritative or even assumed to be a copy of actual JobTech Taxonomy Data.

## Building

`clj -T:build uber` will create an uber-jar without the Datomic dev-local software.

## Running

`clj -M:dev -m jobtech-taxonomy-api.core` will start the API using the configuration in [config.edn](config.edn).
The path to the configuration file can be set using either the environment variable `TAXONOMY_CONFIG_PATH` or the CLI flag `--config <path>` or `-c <path>`. Similarly the port numbers for the REPL and the server can be set. For the repl using `TAXONOMY_REPL` and `-r NNNN` or `--repl NNNN` and for the server using `TAXONOMY_PORT` and `-p NNNN` or `--port NNNN`.

## Profiling

The `:prof` alias enables [clojure-goes-fast/clj-async-profiler](https://github.com/clojure-goes-fast/clj-async-profiler) if the following kernel settings has been enabled:
```
sudo sysctl -w kernel.perf_event_paranoid=1
sudo sysctl -w kernel.kptr_restrict=0
```
See [env/prof/clj/profiler.clj](env/prof/clj/profiler.clj) for an example of how to use.

Substantial performance gains can be made by [ordering the clauses effectively][10]. For Datomic, the [query-stats flag][11] can be passed with a query to obtain details about how the clauses are executed, e.g:

```
{:query ...
 :args ...
 :offset 0
 :limit -1
 :query-stats true}
```

## Testing

`clj -M:test:kaocha` will run all test on the Datahike backend in memory, and exclude Datomic specific tests.

`DATABASE_BACKEND=:datomic-v.kaocha clj -M:test:kaocha` will run all the tests on the Datomic backend using the `datomic-local` in memory database.

### Useful flags

* `--watch` will re-run tests as soon as you save.
* `--seed 42` can be added to the command in order to stop test randomization temporarily.


### Profiles

Select a test profile by providing the `--profile <keyword>` flag.

#### :dev

The `:dev` profile provides the `:unit`, `:integration` and `:base` tests to focus on.

#### :ci

The `:ci` profile adds some plugins and default arguments for running the tests in Gitlab CI.

## Database backends

The underlying Datalog database can be either [Datomic][2] or a [Datahike][4].
Currently, the API has mainly used Datomic and support for Datahike has been experimental.
But we are migrating towards Datahike being the main choice.

### Datomic Setup

Obs. This is only valid for the PROD environment that hasn't been upgraded yet. The sockproxy script is no
longer needed for the testing environment.

If you use are using Datomic Cloud, you need to open a tunnel to the database that is located.
That can be done in one of two ways.

If you have the [Datomic CLI client][8] installed, you can simply call one of the following
commands depending on whether you are working in a *test* or *production* environment.

| Environment | Command |
|-------------|---------|
| **test** | `datomic client access tax-test-v4` |
| **production** | `datomic client access tax-prod-v4` |

In case you don't have the Datomic CLI tools, you can use the
[`test/scripts/datomic-socks-proxy`](test/scripts/datomic-socks-proxy)
script for that.

| Environment | Command |
|-------------|---------|
| **test** | `./test/scripts/datomic-socks-proxy tax-test-v4` |
| **production** | `./test/scripts/datomic-socks-proxy tax-prod-v4` |

### Controlling from REPL

Change to the project root folder and start your repl (If you use IntelliJ,
don't forget to load the project in your repl).

See the section [Local development](#local-development) for details on how to configure your environment for local development.

To start the development backend database and HTTP server from the REPL run: `(user/load-config)` or `(user/load-config my-config)` and then `(user/start)`.

Use `(user/stop)` to stop it again.

To run queries against the database:

```
(require '[jobtech-taxonomy-api.db.database-connection :as db])
(require '[jobtech-taxonomy-api.config :refer [get-config]])
(def cfg (get-config))
(start)
(pprint (db/q '[:find (pull ?eid [*]) :in $ :where [?eid :concept/preferred-label "Personlig utveckling"]] (db/get-db cfg)))
```

To run GraphQL queries:
```
(require '[jobtech-taxonomy-api.config :refer [get-config]])
(def cfg (get-config))
(start)
(clojure.pprint/pprint (jobtech-taxonomy-api.db.graphql/execute cfg "{ concepts(type: \"wage-type\") { id preferred_label }}" nil nil nil))
```

### Docker

Build the image:
```
podman build . -t api
```

Run the API locally using podman with Datomic as backend, run:
```
podman run -p 3000:3000 --name api --rm api
```

Run the API locally using podman with Datahike as backend, with the database in the folder datahike-file-db, run:
```
podman run -v $PWD/datahike-file-db:/datahike-file-db:Z -e DATABASE_BACKEND=datahike -p 3000:3000 --name api --rm api
```

Stop the API in another terminal, run:
```
podman container stop api
```

## Next step

Then open the following URL in a web browser:

    http://localhost:3000/v1/taxonomy/swagger-ui/index.html

## Authorize

Authorization is only needed to read unpublished taxonomy versions and for DB write access.
To get write access, click the Authorize button, and enter your account code,
defined in `env/dev/resources/config.edn`.

## Running a query

    curl -X GET --header 'Accept: application/json' 'http://localhost:3000/v1/taxonomy/main/concepts?preferred-label=Danska'
    
## Code structure

Here is an overview of some of the most important parts of this project.

| What | Information |
|-|-|
| *Directory* [`src/clj/jobtech_taxonomy_api`](src/clj/jobtech_taxonomy_api) | Source code for most of the implementation of the JobTech Taxonomy API |
| *Directory* [`test/clj/jobtech_taxonomy_api`](test/clj/jobtech_taxonomy_api) | Most of the automated tests. |
| *Directory* [`env/`](env) | Specific configurations [`dev`](env/dev), [`kaocha`](env/kaocha), [`prod`](env/prod) and [`test`](env/test)  |
| *Namespace* [`user`](env/dev/clj/user.clj) | Top-level functions `start` and `stop` are used to start/stop the HTTP server and other components that have been declared using [Mount](https://github.com/tolitius/mount). |
| *Namespace* [`jobtech-taxonomy-api.config`](src/clj/jobtech_taxonomy_api/config.clj) | The `env` var is a [Mount](https://github.com/tolitius/mount) state which contains the system config. |
| *Namespace* [`jobtech-taxonomy-api.core`](src/clj/jobtech_taxonomy_api/core.clj) | This namespace contains the `http-server` that can be started using the `user/start` function. |
| *Namespace* [`jobtech-taxonomy-api.db.database-connection`](src/clj/jobtech_taxonomy_api/db/database_connection.clj) | This file contains functions that abstract the underlying database implementation, *Datomic* or *Datahike*. |
| *Namespace* [`jobtech-taxonomy-api.test.test-utils`](test/clj/jobtech_taxonomy_api/test/test_utils.clj) | This file contains utilities for the tests. Most importantly it contains the function `fixture` that is used to create a temporary database against which unit tests are run. |

## Local development

When developing new functionality for jobtech-taxonomy-api, it is best to run the API against a temporary copy of the database.
To create such a copy, do the following:

1. Clone [jobtech-taxonomy-api-gitops](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-gitops).
2. *This step may be redundant*: Open a tunnel to the production database: `datomic client access tax-prod-v4`
3. From the `jobtech-taxonomy-api-gitops` directory, download the production database: `make download-prodwrite-db`
4. *This step may be redundant*: Close the previous tunnel and open a new one to the test environment: `datomic client access tax-prod-v4`
5. From the `jobtech-taxonomy-api-gitops` directory, publish a new temporary database that you can run code against: `DBNAME_DECORATOR=jonas make publish-dev-local-db` where you replace `jonas` by *your pet's name or your name*.
6. Update the value at the `:datomic-cfg` key in the file [`env/dev/resources/config.edn`](env/dev/resources/config.edn) with details about your database printed at step 5. In addition to copying the config there, *you need to add another key* **`:datomic-name`** *with the same value as `:name`.* You may also need to *leave `:endpoint` unmodified* and also leave `:proxy-port 8182`.

Now your local development setup should be configured. To check that it is really configured, open your editor and a REPL (see [Editor specific instructions](#editor-specific-instructions) and check the configuration from the `jobtech-taxonomy-api.config` namespace:

```clj
(:datomic-cfg env)
;; => {:name "jobtech-taxonomy-ostlund-2022-12-15-10-43-30", :server-type :ion, :region "eu-central-1", :system "tax-test-v4", :endpoint "http://tax-c-loadb-i0orbaqklmiy-22feddff72f82e2d.elb.eu-central-1.amazonaws.com:8182", :config-name :datomic-dev-local-config}
```

### Editor specific instructions

These are some specific instructions for various editors to help you integrate it with this project and get a development REPL up and running in your editor.

| Editor | Instructions |
|-|-|
| Emacs | The file [`.dir-locals.el`](.dir-locals.el) configures the REPL for development. Just call `M-x cider-jack-in` to run the REPL. |
| Vim | ... |
| VSCode, Calva | ... |

### Create dev-config for local development

**Updated instructions:** *See the section* [**Local development**](#local-development) for details on how to configure your system for local development.

**Note:** *The instructions that follow here no longer seem to work*. Is it still true that the local configuration is loaded from `dev-config.edn`? I don't think so, I couldn't find any code in this repo or in the cprops library.

Create the file "dev-config.edn" with this content

```
;; WARNING
;; The dev-config.edn file is used for local environment variables, such as database credentials.
;; This file is listed in .gitignore and will be excluded from version control by Git.

{:dev true
 :port 3000
 ;; when :nrepl-port is set the application starts the nREPL server on load
 :nrepl-port 7000

 ; set your dev database connection URL here
 ; :database-url "datomic:free://localhost:4334/jobtech_taxonomy_api_dev"

 ; alternatively, you can use the datomic mem db for development:
 ; :database-url "datomic:mem://jobtech_taxonomy_api_datomic_dev"
}
```


## Testing

The integration test setup creates a temporary database for each test,
which makes it safe to do any modifications without leaving traces
behind.

Summary:
 - test runner: Kaocha (https://github.com/lambdaisland/kaocha).
 - test command: `clj -A:test -M:kaocha --focus-meta TAG`
   where TAG is the name of one of the test's tags (such as `integration`).
 - status: for integration tests that rely on a live database, only one test
   can be run at a time. This means that you should assign each test a unique
   tag (e.g. `(test/deftest ^:changes-test-2 changes-test-2 ...)`), and then
   run it with `clj -M:test --focus-meta changes-test-2`.

### Run Kaocha tests

To run all tests against a Datomic database, first [open a tunnel](#datomic-setup)
(e.g. `datomic client access tax-test-v4`) and then, in a different terminal, call
```
clj -M:test:kaocha
```
Technically, the `fixture` function in `test/clj/jobtech_taxonomy_api/test/test_utils.clj`
will create a temporary Datomic database in AWS that is used for the tests.

To run the tests against a local Datahike DB, prepend the test with DATABASE_BACKEND=datahike:
```
DATABASE_BACKEND=datahike clj -M:test:kaocha
```

### Running code coverage

The code coverage is based on kaocha-cloverage. Run with:
```
clj -M:test:kaocha --plugin cloverage
```

### Testing in the REPL

Run the following commands in a terminal:
```shell
$ clj -A:dev:test
user=> (use 'kaocha.repl)
user=> (run 'jobtech-taxonomy-api.test.graphql-test/graphql-test-1)
```

### nREPL

If you are using nrepl, you can start it like so:
```shell
$ clj -A:dev:test -M -m nrepl.cmdline
```

Alias `:dev` is required for local development, and `:test` is also useful for creating tests

Typical emacs/cider launch command can look like this:
```shell
 /usr/local/bin/clojure \
 -Sdeps '{:deps {nrepl/nrepl {:mvn/version "0.8.3"} refactor-nrepl/refactor-nrepl {:mvn/version "2.5.1"} cider/cider-nrepl {:mvn/version "0.26.0"}} :aliases {:cider/nrepl {:main-opts ["-m" "nrepl.cmdline" "--middleware" "[refactor-nrepl.middleware/wrap-refactor,cider.nrepl/cider-middleware]"]}}}' \
 -A:dev:test \
 -M:cider/nrepl
```

Connect to a running nREPL (on port 7000):
```
clj -R:nREPL -m nrepl.cmdline -c -p 7000
```

### How to write an integration test

#### File and namespace
Your test should reside in the directory `test/clj/jobtech_taxonomy_api/test/`.

You should either pick an existing file, or create a new file, ending
with `_test.clj`.  It should use a namespace like this: `(ns
jobtech-taxonomy-api.test.FILENAME ...)`, where FILENAME is for example
`changes-test`.

You need to require `[jobtech-taxonomy-api.test.test-utils :as util]`.

#### Define fixtures
Place one occurrence of this line in your test file:
`(test/use-fixtures :each util/fixture)`.

#### Define a test which calls functions directly
Here is a simple example of a test which asserts a skill concept, and
then checks for its existence.

First, require
```
[jobtech-taxonomy-api.db.concept :as c]

```

Then write a test:
```
(test/deftest ^:concept-test-0 concept-test-0
  (test/testing "Test concept assertion."
    (c/assert-concept "skill" "cykla" "cykla")
    (let [found-concept (first (core/find-concept-by-preferred-term "cykla"))]
      (test/is (= "cykla" (get found-concept :preferred-label))))))
```

#### Define a test which calls the Taxonomy REST API
Here is a simple example of a test which asserts a skill concept, and
then checks for its existence via the REST API:

First, require
```
[clojure.test :as test :refer [deftest is testing]]
[jobtech-taxonomy-api.db.concept :as c]

```

Then write a test:
```
(deftest ^:changes-test-1 changes-test-1
  (testing "test event stream"
    (c/assert-concept "skill" "cykla" "cykla")
    (let [[status body] (util/send-request-to-json-service
                         :get "/v0/taxonomy/public/concepts"
                         :query-params [{:key "preferred-label", :val "cykla"}])]
      (is (= "cykla" (get (first body) :preferred-label))))))
```

## Deployment
The deployment of this is handled by [jobtech-taxonomy-api-gitops](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-gitops).

In case the build pipeline fails in OpenShift, images can be pushed manually to Nexus.
```
git checkout 8f815df2047da934d13f724f0d3c0623db5f57d9
docker build . -t api
docker tag api:latest docker-images.jobtechdev.se/batfish/jobtech-taxonomy-api:8f815df2047da934d13f724f0d3c0623db5f57d9
docker push docker-images.jobtechdev.se/batfish/jobtech-taxonomy-api:8f815df2047da934d13f724f0d3c0623db5f57d9
```
### Routes
#### Develop
##### Raw
http://api-jobtech-taxonomy-api-develop.apps.testing.services.jtech.se

##### Cached
https://taxonomy-develop.api.jobtechdev.se

#### Testing - Read
##### Raw
http://api-jobtech-taxonomy-api-testing-read.apps.testing.services.jtech.se
##### Cached
https://taxonomy-u1.api.jobtechdev.se
https://taxonomy-testing-read.api.jobtechdev.se


## Logging

By default, logging functionality is provided by the `timbre` library. This is used together with the `slf4j-timbre` and `slf4j-api` helpers that manages logging from components using `slf4j` logging.

Any Clojure data structures can be logged directly.


### Examples
```
(ns example
 (:require [taoensso.timbre :as log]))

(log/info "Hello")
=>[2015-12-24 09:04:25,711][INFO][myapp.handler] Hello

(log/debug {:user {:id "Anonymous"}})
=>[2015-12-24 09:04:25,711][DEBUG][myapp.handler] {:user {:id "Anonymous"}}
```

### Logging of exceptions

```
(ns example
 (:require [taoensso.timbre :as log]))

(log/error (Exception. "I'm an error") "something bad happened")
=>[2015-12-24 09:43:47,193][ERROR][myapp.handler] something bad happened
  java.lang.Exception: I'm an error
    	at myapp.handler$init.invoke(handler.clj:21)
    	at myapp.core$start_http_server.invoke(core.clj:44)
    	at myapp.core$start_app.invoke(core.clj:61)
    	...
```

### Logging backends

### Configuring logging
Each profile has its own log configuration.

It works like a standard Java log configuration, with appenders and loggers.

The default configuration logs to standard out, and to log files in log/.

## Adding auto code formatting as a pre-commit hook

Run this command to configure git to look for hooks in .githooks/:
`git config --local core.hooksPath .githooks/`

## COMMON ERRORS

If you get :server-type must be :cloud, :peer-server, or :local
you have forgot to start the taxonomy api. Run `(start)` in the `user>` namespace

## Extra tools

You will need to install:
1. For Windows users: [WSL][5]
2. [Clojure CLI][1] 1.10.3.855 or above
3. [AWS CLI][6]
4. [Java][7]
5. [Datomic CLI Tools][8]
6. [Datomic dev-local][9] (For Datomic testing.)

[1]: https://clojure.org/guides/getting_started

You will also need a [Datomic][2] or a [Datahike][4] database with the JobTech Taxonomy Database installed.
See <https://github.com/JobtechSwe/jobtech-taxonomy-database> how to create a Datomic database.

Set your connection details in `env/dev/resources/config.edn`.

For graphviz endpoint to work locally, you'll need to have graphviz installed locally, e.g. with `brew install graphviz` or `apt install graphviz`.

The project is based on [Ring][3], which provides the web server
infrastructure.

You can either run the taxonomy api from either your terminal or from a repl.

## License

EPL-2.0

Copyright © 2019 Jobtech

# Contact Information

Bug reports are issued at [https://gitlab.com/team-batfish/jobtech-taxonomy-api/issues][the repo on GitLab].

Questions about the Taxonomy database, about Jobtech, about the API in
general are best emailed to [jobtechdev@arbetsformedlingen.se][Jobtechdev contact
email address].

Check out our other open APIs at [jobtechdev][Jobtechdev].


[0]: https://github.com/JobtechSwe/jobtech-taxonomy-database "The JobTech Taxonomy Database"
[1]: https://clojure.org/guides/getting_started "Clojure CLI"
[2]: https://www.datomic.com "Datomic"
[3]: https://github.com/ring-clojure/ring "Ring"
[4]: https://datahike.io "datahike"
[5]: https://docs.microsoft.com/en-us/windows/wsl/install-manual "WSL"
[6]: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html "AWS CLI"
[7]: https://openjdk.java.net/ "Java"
[8]: https://docs.datomic.com/cloud/operation/cli-tools.html#cloud "Datomic CLI Tools"
[9]: https://docs.datomic.com/cloud/dev-local.html "Datomic dev-local"
[10]: https://docs.datomic.com/pro/best-practices.html#most-selective-clauses-first "Put the Most Selective Clause First in Query"
[11]: https://building.nubank.com.br/optimizing-datomic-queries/ "Leveraging Datomic query-stats to Optimize Clause Ordering"
