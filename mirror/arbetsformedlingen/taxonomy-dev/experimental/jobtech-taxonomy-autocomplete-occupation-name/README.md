---
title: Autocomplete Occupation name
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/experimental/jobtech-taxonomy-autocomplete-occupation-name/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/experimental/jobtech-taxonomy-autocomplete-occupation-name
gitdir-file-path: /README.md
date: '2023-03-15 14:41:48'
path: /arbetsformedlingen/taxonomy-dev/experimental/jobtech-taxonomy-autocomplete-occupation-name/README.md
tags:
- README.md
---
# Autocomplete Occupation name

Javascript package for the autocompleting skills from jobtech taxonomy version 18

Download this code and run


```
 npm run build
```

```
npm install .

```

Then download jobtech-taxonomy-autocomplete-demo and build it.