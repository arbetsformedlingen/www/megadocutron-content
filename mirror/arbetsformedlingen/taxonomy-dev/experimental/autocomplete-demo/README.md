---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-dev/experimental/autocomplete-demo/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-dev/experimental/autocomplete-demo
gitdir-file-path: /README.md
date: '2023-03-15 15:22:28'
path: /arbetsformedlingen/taxonomy-dev/experimental/autocomplete-demo/README.md
tags:
- README.md
---
Download and build and install jobtech-taxonomy-autocomplete-occupation-name


And the run `npm install` and `npm run build` in this project root. 

open `dist/index.html` in a browser

