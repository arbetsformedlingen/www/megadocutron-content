---
title: 1.4.2
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/joblinks-search-api/-/blob/main//CHANGELOG.md
gitdir: /arbetsformedlingen/job-ads/joblinks-search-api
gitdir-file-path: /CHANGELOG.md
date: '2023-06-09 14:30:02'
path: /arbetsformedlingen/job-ads/joblinks-search-api/CHANGELOG.md
tags:
- CHANGELOG.md
---
Changelog JobAd Links API
===============================

# 1.4.2
* Update dependencies
* APM removed from code
* Minimum Python version 3.11.3
* Introduces setting DELAY_LOAD_SYNONYM_DICTIONARY_STARTUP for delaying loading the synonym dictionary at startup until
  first request

# 1.4.1
* Refactors and reads synonym dictionary from JobAd Enrichments API instead of from Elasticsearch 

# 1.4.0
* Use Loguru for logging
* Update dependencies
* Update Python to 3.10.10
* Remove files related to installation with pip
* Use GitLab CI
* Fix link to getting started in Swagger

# 1.3.0
* Use Poetry instead of pip for dependency management
* Added models to Swagger page
* Refactored Querybuilder
* Update Python version in Dockerfile to 3.10.7

# 1.2.0
* Update Python version in Dockerfile to 3.10.5
* Rename to 'JobAd Links'
* Better documentation in Swagger
* Update Python version in Dockerfile
* Install module 'jobtech-common' from Jobtech's Pypi
* Update dependencies
* Change tests to use specific ads
* When writing english as word in search query, boost ads with english as detected language
* Changed search method to get for ad/id endpoint.
* Removed code from __init__-files and put it in external files

# 1.1.1

* Fix bug when searching with wildcard (*)

# 1.1.0

* internal change for date handling
* Change name to JobTech Links in Swagger

# 1.0.0

* Initial release
