---
title: create-historical-from-stream-ads
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/development-tools/create-historical-from-stream-ads/-/blob/main//README.md
gitdir: /arbetsformedlingen/job-ads/development-tools/create-historical-from-stream-ads
gitdir-file-path: /README.md
date: '2023-04-13 15:19:06'
path: /arbetsformedlingen/job-ads/development-tools/create-historical-from-stream-ads/README.md
tags:
- README.md
---
# create-historical-from-stream-ads

## Description

This project summarizes json files that contains the output from the JobStream API saved as individual files with date
is part of the file name.
It starts with the latest file (file list from `glob`sorted in reverse order).
This order is important to save the latest version of the ad in the summary file.

## Installation

This project uses [Poetry](https://python-poetry.org/) for dependence management.  
Install Poetry with `pip install poetry`
Install dependencies with `poetry install`

## Usage

Change settings.stream_ads_folder to point to the folder where your files are saved
Change settings.year to the year you want to summarize (the files needs this year in the file name)
run `python main.py`
the output will be save in `YYYY_summarized.jsonl`

N.B. The files have personal information, anonymization is done in another project

## Support

Create an issue in this project and use the label `jobbdata`

## License

Apache 2, see the file LICENSE for details
