---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/development-tools/load-test-file-generator/-/blob/main//README.md
gitdir: /arbetsformedlingen/job-ads/development-tools/load-test-file-generator
gitdir-file-path: /README.md
date: '2021-12-30 11:26:22'
path: /arbetsformedlingen/job-ads/development-tools/load-test-file-generator/README.md
tags:
- README.md
---
Creates files that can be used for performance tests with loader.io
