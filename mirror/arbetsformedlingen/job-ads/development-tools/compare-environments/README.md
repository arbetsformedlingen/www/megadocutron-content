---
title: compare-environments
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/development-tools/compare-environments/-/blob/main//README.md
gitdir: /arbetsformedlingen/job-ads/development-tools/compare-environments
gitdir-file-path: /README.md
date: '2023-09-27 16:17:23'
path: /arbetsformedlingen/job-ads/development-tools/compare-environments/README.md
tags:
- README.md
---
# compare-environments

Utility to compare different JobSearch or JobAd Links environments (e.g. development and production) and show result of
the same query against both environments

## Filtering

A combination of difference in absolut number and difference in percent can be used.
This is so that a small absolute diff (e.g. from 1 to 2) isn't included even though it's a big diff in percent.

## Set environment variables

these are found in `settings.py`, adjust if needed. 

## Run comparison

`python main.py`

## View result

The result is saved as a .csv file with `|` as separator (because there are commas in the test cases ). The file has all differences, positive or negative, thhe other only shows test cases where target has fewer hits than
reference, positive and negative. Test cases where there is no diff are not icnluded.
The file name include timestamp and the api that was tested e.g. `jobsearch-results-2023-06-02-10-12-55_diff.csv`

## Add test cases

Test cases are query strings, mosty taken from actual searches (search-trends)
To add more test cases, add them to a relevant test case file or create a new file.
Naming convention for file names: it must be saved in the test_cases folder, include the product name and end
with `.txt`
E.g.:
a file with test cases for jobsearch and joblinks must be have both `jobsearch` and `joblinks` in the file name
`jobsearch_joblinks_freetext_queries.txt`

## How to start apis with different ports / indices

### If changes are done in import:

- Import comparison ads with main branch of jobsearch-importers to `main-index`  
- Import comparison ads with feature branch of jobsearch-importers `branch-index`

### Start first api (main)

in jobsearch-apis (Windows), procedure is similar for JobAd Links:
- checkout `main`branch
- open a terminal

```
set ES_INDEX=main-index
set flask_app=jobsearch
flask run
```

Jobsearch will start on port 5000 (default)

### open another terminal for target.

If changes are done in the api, you must check out that branch:  
- checkout feature-branch  
Otherwise, keep `main` branch

```
set ES_INDEX=branch-index
set flask_app=jobsearch
flask run --port=5100
```

This instance of Jobsearch runs on port 5100.
In some cases you can run on the same index, it depends on if changes are done in importer or api.
