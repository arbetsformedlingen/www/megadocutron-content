---
title: LA Mock Api
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/development-tools/jobsearch-ad-mock-api/-/blob/main//README.md
gitdir: /arbetsformedlingen/job-ads/development-tools/jobsearch-ad-mock-api
gitdir-file-path: /README.md
date: '2023-09-21 10:39:49'
path: /arbetsformedlingen/job-ads/development-tools/jobsearch-ad-mock-api/README.md
tags:
- README.md
---
# LA Mock Api
[[_TOC_]]
## Description
Small flask api that mocks the three Ledigt Arbete endpoints used by the import-platsannons cronjobs.
Code pushed to the repo will automatically deploy to https://console-openshift-console.test.services.jtech.se/k8s/cluster/projects/la-ad-mock-api

### Endpoint example
* https://la-mock-api.test.services.jtech.se/annonser/23396767
* https://la-mock-api.test.services.jtech.se/sokningar/publiceradeannonser
* https://la-mock-api.test.services.jtech.se/sokningar/andradeannonser/1583341616000

## API
Create a virtual environment or environment of choice and activate it.
Install necessary packages:
`pip install -r requirements.txt`
To run the api locally, set the environment variable as
`export FLASK_APP=la_mock`
and then type
`flask run`
Endpoints will be available at:

* http://localhost:5000/sokningar/publiceradeannonser
* http://localhost:5000/annonser/<ad_id>
* http://localhost:5000/sokningar/andradeannonser/<timestamp\>

(timestamp wont matter, always returns empty)
## Add new ads
To add new ads to the api, first find the absolute path to the folder la_mock/resources/ads. Then set an environment variable:
`export AD_PATH=la_mock/resources/ads`
Run:
`python3 setup.py develop`
Import an ad by running:
`import-ad <ad_id>`
The ad will be saved as a json file and the ad text printed out. You do not need to worry about whether ads are depublished, all ads will be treated as active when displayed by the api.

Make sure the ad has anonymized correctly. 
If it has not, check in /la_mock/utils/anonymize/first_name.txt and /la_mock/utils/anonymize/last_name.txt if the first or last name is missing. Add them and run the `import-ad`command again for the same id to check if it worked. Finally store the files with git.

### Add multiple ads
Set the variable NUMBER_OF_RANDOM_ADS in create_new_test_ads.py
run create_new_test_ads.py, it will download all current ad ids, pick the desired number on random and download these ads to individual json files


### Ads for comparison (100k ads)
Use the comparision ads from Minio / jobbdata / testdata
copy them to `la_mock/resources/compare_ads`
set environment variable `TEST_ADS=false`


### Creating new comparison ads (for use with compare-environments):
Run `la_mock/utils/download_all_ads_from_ledigt_arbete.py` it will download all currently active ads from Ledigt Arbete. Re-run it in case of failures, it will pick up where it left. 
When downloaded, they have to be anonymized (anonymizer in this repo is slow, use https://gitlab.com/arbetsformedlingen/libraries/anonymisering)
If you create new comparison ads, the query strings in compare-environments should be updated so that date queries matches the ads.

Todo: instructions and code for anonymization.
