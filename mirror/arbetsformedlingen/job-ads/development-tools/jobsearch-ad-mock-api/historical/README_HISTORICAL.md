---
title: Reduce files
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/development-tools/jobsearch-ad-mock-api/-/blob/main//historical/README_HISTORICAL.md
gitdir: /arbetsformedlingen/job-ads/development-tools/jobsearch-ad-mock-api
gitdir-file-path: /historical/README_HISTORICAL.md
date: '2023-09-21 10:39:49'
path: /arbetsformedlingen/job-ads/development-tools/jobsearch-ad-mock-api/historical/README_HISTORICAL.md
tags:
- historical::README_HISTORICAL.md
- README_HISTORICAL.md
---
# Reduce files

## Why?
To create smaller files to be able to test import and search of historical ads quicker,
while still having ads from every year.


## Description
A program that will take all json files with historical ads in the same directory, 
select a number of ads at random and save as new json files.
The number of ads selected is based on the variable 'percentage'

The new json files (with the name <year>-short.json can be used in the importer for historical ads
