---
title: '2022-02-04'
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/development-tools/historical-ads-file-converter/-/blob/main//CHANGELOG.md
gitdir: /arbetsformedlingen/job-ads/development-tools/historical-ads-file-converter
gitdir-file-path: /CHANGELOG.md
date: '2022-03-21 14:34:56'
path: /arbetsformedlingen/job-ads/development-tools/historical-ads-file-converter/CHANGELOG.md
tags:
- CHANGELOG.md
---
Changelog
===============================
# 2022-02-04

* Added more cleanup of files
* Added language detection (until it's done in jobsearch-importers)
* New file handling for better performance
* 

# 2022-01-20

* conversion of chunks from the importer to jsonlines, 
* conversion of jsonlines to csv, metadata and jsonline with reduced number of ads

