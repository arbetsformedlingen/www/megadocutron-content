---
title: Code for converting historical ad files to different format
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/development-tools/historical-ads-file-converter/-/blob/main//README.md
gitdir: /arbetsformedlingen/job-ads/development-tools/historical-ads-file-converter
gitdir-file-path: /README.md
date: '2022-03-21 14:34:56'
path: /arbetsformedlingen/job-ads/development-tools/historical-ads-file-converter/README.md
tags:
- README.md
---
# Code for converting historical ad files to different format

### Possible conversions:
* Files converted in importer to json lines files (.jsonl)
* jsonl to csv
* jsonl to reduced files, where some fileds (e.g. description, id, headline) have been removed.
* compression (zip) of any file.

### Todo: 
* uploader to AWS S3

