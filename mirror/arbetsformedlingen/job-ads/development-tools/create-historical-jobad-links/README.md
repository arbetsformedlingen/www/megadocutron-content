---
title: Create historical files for JobAd Links
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/development-tools/create-historical-jobad-links/-/blob/main//README.md
gitdir: /arbetsformedlingen/job-ads/development-tools/create-historical-jobad-links
gitdir-file-path: /README.md
date: '2023-08-29 17:23:30'
path: /arbetsformedlingen/job-ads/development-tools/create-historical-jobad-links/README.md
tags:
- README.md
---

# Create historical files for JobAd Links

### Summary
This program downloads files day by day from files from https://data.jobtechdev.se/annonser/jobtechlinks/ 
unpacks them and loads the ads, only the latest version of an ad is included.  
It starts with the last day of the year to make duplicate handling easier (once an ad is found, further occurences of the same ad will be older and ignored)


### How to run
Adjust YEAR in settings.py
run `python main.py`

### Output
Default output folder `output` is created and the following files are saved there. They are re-created as empty files every time the program is run. 
- jobad_links_YYYY.jsonl
- errors_YYYY.txt
- statistics_YYYY.csv (yyyy-mm-dd, number of ads in the file that day)

In case it stops working, extract the ads from the existing ads file and write some code to load them into the set `processed_ads`. 
Adjust starting date to continue where it stopped working.




For 2021, ad files are available from 2021-03-31.  
Some days do not have files, this will be logged and the program will continue with the previous day
If there is some problem (like missing files), errors will be logged and also written to a text file.  
