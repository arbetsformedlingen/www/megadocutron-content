---
title: Install OpenSearch
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/job-ads-env-init/-/blob/main//docs/install-opensearch-aws.md
gitdir: /arbetsformedlingen/job-ads/job-ads-env-init
gitdir-file-path: /docs/install-opensearch-aws.md
date: '2023-10-17 12:28:23'
path: /arbetsformedlingen/job-ads/job-ads-env-init/docs/install-opensearch-aws.md
tags:
- docs::install-opensearch-aws.md
- install-opensearch-aws.md
---
# Install OpenSearch


## AWS

### Create Security group

If there is no security group called `OpenSearch` for the VPC you want to use
for the Open Search cluster, you must create one.

The security group must be named `OpenSearch` it shall allow traffic on port 443(HTTPS)
from the VPC:s jumphost and the kubernetes works in the same VPC.
No outbound traffic is allowed.

## Create Open Search cluster

Go to the opensearch and create domain. Fill in the fields.

* Domain name - the name of your cluster
* Ignore Custom Endpoint
* Development Type
  * Select production for real use, otherwise dev and testing.
  * Select latest version
  * Do not enable compatibility mode.
* Ignore Auto Tune
* Data Nodes can be kept as is. Check that you get enough storage.
* Dedicated master - For production select dedicated master.
* Ignore Warm and cold data storage
* Ignore Snapshot configuration
* Network
  * Select VPC as network type
  * Select VPC to use. This must be the same as you created/selected for the Security group.
  * Select subnet to use
  * Select the security group created or selected above.
* Fine-grained access control
  * Enable Fine-grained access control
  * Create a master user (TODO: We shall have a good admin role for this in future.)
* Ignore SAML authentication for OpenSearch Dashboards/Kibana
* Ignore Amazon Cognito authentication
* Access policy
  Select "Only use fine-grained access control"
* Ignore Encryption
* Ignore tags

Click Create.

Once AWS have created the cluster domain the Domain overview provides
information about the created service.

It shows how to access the dashboard under General information. Since 
we are running within a VPC and your computer is outside you cannot 
access it directly.

Copy the hostname and create a new section in your `.ssh/config` file:

```text
Host DOMAINNAME-tunnel
    User openshift
    Hostname 18.196.63.130
    AddKeysToAgent yes
    CheckHostIP yes
    IdentitiesOnly yes
    IdentityFile ~/.ssh/id_rsa
    LocalForward 9243 DNS-DOMAIN-NAME-OF-DOMAIN:443

```

Replace marks with capital letter with corresponding information.
The Hostname IP-number must be the public IP of the jump host of the VPC the OpenSearch cluster is in.
Note that this expects that you have setup ssh keys in ~/.ssh/id_rsa for the jump host.

To avoid complains on bad certificate change in your `/etc/hosts` to connect the domain
to localhost:
```text
127.0.0.1	localhost DNS-DOMAIN-NAME-OF-DOMAIN
````

Do `ssh DOMAINNAME-tunnel` and keep running. Point your web browser to https://DNS-DOMAIN-NAME-OF-DOMAIN:9243/_dashboards/, accept the security risk.
You shall now be able to login with the master user created above.

 