---
title: Update local synonyms from Jobad enrichment API
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis/-/blob/main//scripts/synonyms/README.md
gitdir: /arbetsformedlingen/job-ads/jobsearch-apis
gitdir-file-path: /scripts/synonyms/README.md
date: '2023-10-30 12:57:29'
path: /arbetsformedlingen/job-ads/jobsearch-apis/scripts/synonyms/README.md
tags:
- scripts::synonyms::README.md
- synonyms::README.md
- README.md
---
# Update local synonyms from Jobad enrichment API
Updates the synonyms file in the scripts/synonyms folder with
the latest synonyms from the JobAd Enrichment API.
This files is used on application startup to speed up the loading of synonyms. 
Stores a file in json format.
To use this file either upload somewhere to be used of http/https,
or use the file from disc.
Specify the the path either as an url(http:// or https://) or as file path (file://)
in settings.py ONTOLOGY_FILE_URL.

## version
Version 1.0.0

## Usage
python scripts/synonyms/update_local_synonyms_list.py
