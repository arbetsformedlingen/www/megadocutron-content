---
title: Start a manual PipelineRun
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/historical-ads-from-dataset-pipeline/-/blob/main//manual_run_pipelines/README_start_import_manually.md
gitdir: /arbetsformedlingen/job-ads/historical-ads-from-dataset-pipeline
gitdir-file-path: /manual_run_pipelines/README_start_import_manually.md
date: '2023-09-19 08:25:35'
path: /arbetsformedlingen/job-ads/historical-ads-from-dataset-pipeline/manual_run_pipelines/README_start_import_manually.md
tags:
- manual_run_pipelines::README_start_import_manually.md
- README_start_import_manually.md
---
How to start import of historical files to a jobsearch environment running in OpenShift


## Start a manual PipelineRun

1. Download the [Openshift CLI (oc)](https://console-openshift-console.apps.testing.services.jtech.se/command-line-tools)

2. Login to Openshift with oc  (first login to https://console-openshift-console.apps.testing.services.jtech.se/ and click on your logged in username in the upper right corner, and click "Copy login command"). Click 'Display Token' in the web browser and copy the "Log in with this token"-command (e.g. oc login --token=sha256...)

3. Paste that line in a terminal and press enter. 

4. Select the correct jobsearch-apis-* project of the available OpenShift projects. Type `oc project PROJECT_NAME` to select project, `oc project` to verify. 

5. Change year in `historical-import-pipelinerun.yaml`, in the examples directory

6. Start pipelinerun: `oc create -f historical-import-pipelinerun.yaml`

7. Check progress in OpenShift  `https://console-openshift-console.apps.testing.services.jtech.se/pipelines/ns/PROJECT_NAME/pipeline-runs`