---
title: ArgoCD
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/historical-ads-from-dataset-pipeline/-/blob/main//argocd/README.md
gitdir: /arbetsformedlingen/job-ads/historical-ads-from-dataset-pipeline
gitdir-file-path: /argocd/README.md
date: '2023-09-19 08:25:35'
path: /arbetsformedlingen/job-ads/historical-ads-from-dataset-pipeline/argocd/README.md
tags:
- argocd::README.md
- README.md
---
# ArgoCD

Aardvark is deployed using ArgoCD at JobTech. File for deployment can be
found in https://gitlab.com/arbetsformedlingen/devops/argocd-infra.

File argocd-deployer-access-to-project.yaml sets the permissions for
ArgoCD to deploy.
