---
title: jobsearch-importers infra
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/historical-ads-from-dataset-pipeline/-/blob/main//README.md
gitdir: /arbetsformedlingen/job-ads/historical-ads-from-dataset-pipeline
gitdir-file-path: /README.md
date: '2023-09-19 08:25:35'
path: /arbetsformedlingen/job-ads/historical-ads-from-dataset-pipeline/README.md
tags:
- README.md
---
# jobsearch-importers infra

This repository contains files regarding build and deployment
pipelines for the
[jobsearch-importers repository](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers/-/tree/develop).

In the example directory are template files to for needed secrets.

## Deployment of your own development environment

You can set up your fully personal environment that have a clean
elastic search.

* First deploy the [jobsearch-apis](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis-infra/-/blob/master/README.md)
* Apply the personal overlay: `kubectl apply -k kustomize/overlays/personal`
* Create a secret for urls to platsbanken based on the example `examples/importer-jobads-secret.yaml`.

There are a couple of things you might adapt for your own needs in
the `kustomize/overlays/personal` directory. Though, do not
check in your personal adaptations.

To point out a specific image. Edit the field `newTag` in
`kustomize/overlays/personal/kustomization.yaml`

When you do not need your environment anymore, delete it with
`oc delete project my-project`.

## Run a historical pipeline import

Go to the `examples` and read `README_start_import_manually.md` 
