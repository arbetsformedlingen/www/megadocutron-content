---
title: Historical importer
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers/-/blob/main//docs/historical_importer.md
gitdir: /arbetsformedlingen/job-ads/jobsearch-importers
gitdir-file-path: /docs/historical_importer.md
date: '2023-11-07 14:11:21'
path: /arbetsformedlingen/job-ads/jobsearch-importers/docs/historical_importer.md
tags:
- docs::historical_importer.md
- historical_importer.md
---
# Historical importer

After having run `setup.py`, the command `historical`is available from a terminal.
It takes a command and file name(s) as parameters

## Case 1 - import of a previously enriched file

1. Download the file from https://data.jobtechdev.se/annonser/historiska/berikade/kompletta/index.html and unzip it.
2. Save the ad to Elastic / OpenSearch with the command `historical elastic yyyy_enriched.jsonl` where "yyyy" matches
   the year in the filename.
3. run `historical refresh_alias yyyy`

## Case 2 - Conversion, enrichment and saving to Elastic / Opensearch

Steps from a "raw" json file to data in Elastic/OpenSearch.

1. Convert from json to jsonlines
   `historical json_to_json_line [input_file.json] raw.jsonl` (e.g. a json file
   from https://data.jobtechdev.se/annonser/historiska/index.html)
   The file list is a leftover from when we split yearly json files into smaller json files for performance reasons.
   There are also jsonlines files available, if you use them you can go to step 2 immediately
2. Do conversion of the ads. This takes care of some things that have changed over the years
   `historical convert1 raw.jsonl --output-file converted.jsonl`
3. Enrich the converted file
   `historical enrich converted.jsonl enriched.jsonl`
4. Save the ad to Elastic / OpenSearch
   `historical elastic yyyy_enriched.jsonl`
5. run `historical refresh_alias yyyy` to set the `historical`alias to the latest index for the year.
