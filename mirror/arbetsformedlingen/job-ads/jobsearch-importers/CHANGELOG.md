---
title: 1.15.0 (October 2023)
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers/-/blob/main//CHANGELOG.md
gitdir: /arbetsformedlingen/job-ads/jobsearch-importers
gitdir-file-path: /CHANGELOG.md
date: '2023-11-07 14:11:21'
path: /arbetsformedlingen/job-ads/jobsearch-importers/CHANGELOG.md
tags:
- CHANGELOG.md
---
Changelog
===============================

# 1.15.0 (October 2023)
* Adds "replaces" in taxonomy index for occupations that are replacing older occupations, 
  when executing command "import-taxonomy"
* Adds older/with "replaced_by" occupations to ads that have a new occupation/with "replaces"

# 1.14.1 (October 2023)
* Changed default value for TAXONOMY_VERSION in settings (v21)

# 1.14.0 (June 2023)
* Improved matching for ads that are written in english

# 1.13.3 (May 2023)
* Update dependencies
* Create two new functions in historical for adding index and refreshing alias

# 1.13.2 (April 2023)
* Fix bug in enrichment of historical ads
* Update Python version in Dockerfile to 3.10.10


# 1.13.1 (February 2023)
* Use UTF8 encoding in historical importer
* Add option to use one of the certificates included for gettings ads from LA
* Update Python version in Dockerfile to 3.10.9
* Update Certifi

# 1.13.0 (May 2022)
* Automation of historical imports
* Support for both OpenSearch and ElasticSearch
* Code cleanup
* Update Python version in Dockerfile

# 1.12.1  (March 2022)
* Fix bug where call to get logo url was disabled

# 1.12.0  (March 2022)
* Refactor of conversion of historical ads, remove entrypoints for historical ads
* Move phrase check to common code
* Use Python 3.10.1
* Remove "index from file"
* Remove usage of api-key for enrichment and taxonomy since those systems don't require it anymore
* New field 'detected_language' with value language in ISO 639-1 format or None if language can't be detected

# 1.11.0  (March 2022)
* Install dependencies from new repository location on GitLab
* Extra check to remove personal data from historical ads
* Add phrase “delvis på distans” to remote phrases

 
# 1.10.1
* Improve error handling 

# 1.10.0
* Phrase check for finding ads where the employee rents workplace (e.g a hairdresser's chair)

# 1.9.0
* Phrase check for finding ads with franchise work

# 1.8.0
* Phrase check for finding ads with 'lärling' work

# 1.7.0
* Phrase check for finding ads with trainee work
* Phrase check fo find ads with text "öppen för alla"

# 1.6.0
* Conversion and import of historical ads files
* Add new phrases for finding ads that allow "remote work" 

# 1.5.2
* Update application contact to application contacts

# 1.5.1
* Add environment variable to control which taxonomy version in used when running 'import-taxonomy'
* Fix bug where region was not included in taxonomy index
* Import contact persons

# 1.5.0
* Changes for new taxonomy version (Version 2)
* Update of third-party libraries
* Update valuestore module

# 1.4.1
* Option to save enriched ads to a file (for development & testing)

# 1.4.0
* Check for phrases indicating remote work and set new field 'remote_work' 
* Check that new index is large enough (percentage of previous) before switching alias to new index

# 1.3.0
* Remove LA v1 logic in code
* Remove code for importing from scraped ads (moved to separate repo)

# 1.2.2
* Bugfix support both legacy ids and concept ids when handling 'arbetstidstyp'

# 1.2.1
* Bugfix related to taxonomy fail

# 1.2.0
* Use LA v2 as default

# 1.1.0
* Add occupation and location concept id fields to deleted index. 
* New scraped ads project preparation

# 1.0.5
* Add fixed reference field type

# 1.0.4
* Bugfix for concept_id

# 1.0.3
* Preparation to LA v2 ad format

# 1.0.2
* Remove old stuff, exit while loop

# 1.0.1
* Remove old stuff

# 1.0.0
* Initial release
