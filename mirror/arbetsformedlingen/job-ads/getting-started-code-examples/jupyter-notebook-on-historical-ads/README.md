---
title: Analys av historiska data med Jupyter Notebook
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples/jupyter-notebook-on-historical-ads/-/blob/main//README.md
gitdir: /arbetsformedlingen/job-ads/getting-started-code-examples/jupyter-notebook-on-historical-ads
gitdir-file-path: /README.md
date: '2022-08-04 14:03:29'
path: /arbetsformedlingen/job-ads/getting-started-code-examples/jupyter-notebook-on-historical-ads/README.md
tags:
- README.md
---
# Analys av historiska data med Jupyter Notebook

Då och då får JobTech development frågor kring annonser som tidigare varit
publicerade på platsbanken. Det kan röra sig om att hitta en specifik annons,
statistik kring en yrkesgrupp för en given tidsperiod mm. 
För dessa ändamål har vi sammanställt de historiska platsannonserna i ett
[arkiv paketerad i filer per år](https://data.jobtechdev.se/annonser/historiska/berikade/). 
Dessa är öppna för vem som helst att använda och göra sina egna analyser.
Här beskriver vi lite kort hur man kan använda Jupyter Notebook för att 
göra sin analys.

## Jupyter Notebook

[Jupyter Notebook](https://jupyter.org/) är troligen ett av de vanligaste
verktygen i en Data Scientists verktygslåda. Enkelt beskrivet så är det 
en anteckningsbok  där man kan blanda beskrivande text, kod för analys 
och resultat i ett. Det gör det enkelt att följa hur analysen är gjord.
I anteckningsboken kan koden också köras och resultat visas i form av 
diagram eller tabeller.
Koden är i språket Python, men det finns stöd för andra populära språk
bland data scientists som R och Scala.

Fördelen med en notebook jämfört med ett traditionellt 
business intelligence(BI) verktyg är att man kan göra mer komplexa analyser,
kombinera med AI och maskinlärning. En notebook är också väldigt enkel att
dela och förstå.

Jupyter Notebook är öppen källkod och fritt att använda.

## Historiska datat

Jobtech development har publicerat data för platsannonser från 2016 och framåt.
Dessa är öppna för vem som helst att använda och göra sina egna analyser.
Datat laddas ner som zip-filer från
[vårt arkiv](https://data.jobtechdev.se/annonser/historiska/berikade/).
När man packar upp de komprimerade zip-filerna så är formatet json med 
en post per rad. Formatet kallas dagligt för jsonl. All information
från platsannonserna finns kvar, förutom information som handlar om individer.
Detta är exempelvis kontaktinformation.

För varje år finns det tre filer. Först har vi kompletta filer som innehåller
all metadata och full annonstext. I många fall behövs inte annons texten.
Därför har vi även publicerat med bara metadata. Dessa är mindre och kräver
därför mindre datorkraft för att analysera.
Slutligen har vi exempel-filer som innehåller en procent av annonserna. Dessa
är bra under tiden man utvecklar sin analys för att verifiera att koden fungerar
och att datat ser ut som förväntat.

Fältnamnen i posterna är förhoppningsvis ganska självförklarande. Vid funderingar
kan man ställa frågor i vårt [forum](https://forum.jobtechdev.se/).

## Fall beskrivning

Det är dags att bli lite mer konkret. Jobtech development fick frågan; 
Hur många annonser för lärartjänster fanns utlysta under Juli 2021?
De skall vara uppdelade på förskol-, grundskol- och gymnasielärare samt
om möjligt grupperat per kommun och region.

## Börja förstå datat

Först måste vi definiera vad är en utannonserad tjänst. Med det menar vi en
platsannons som funnits tillgänglig under Juli månad på platsbanken.
Det innebär att den måste varit publicerad före eller under Juli månad och
sista ansökningsdag måste vara under Juli eller senare.

Nästa steg är att förstå vilka annonser som innehåller en lärartjänst.
Då alla annonser klassificerats enligt 
[taxonomi för yrkesklassificering](https://www.jobtechdev.se/sv/komponenter/jobtech-taxonomy)
kan vi enkelt hitta det. I varje post finns ett fält som heter `occupation`

```json
  "occupation": [
    {
      "concept_id": "iYgm_hfD_Bez",
      "label": "Lärare i grundskolan, årskurs 7-9",
      "legacy_ams_taxonomy_id": "7654",
      "original_value": true
    }
  ],
  "occupation_field": [
    {
      "concept_id": "MVqp_eS8_kDZ",
      "label": "Pedagogiskt arbete",
      "legacy_ams_taxonomy_id": "15"
    }
  ],
  "occupation_group": [
    {
      "concept_id": "oQUQ_D11_HPx",
      "label": "Grundskollärare",
      "legacy_ams_taxonomy_id": "2341"
    }
  ]
```

I fältet finns ett underfält som heter `occupation_group` med fältet label. Vi
använder det för att hitta vilka platsannonser som är lärartjänster.

Sist måste vi också identifiera i vilken region och kommun som tjänsten är 
placerad. På samma sätt som för yrke finns det ett fält `workplace_address`.

```json
  "workplace_address": {
    "municipality": "Örnsköldsvik",
    "municipality_code": "2284",
    "municipality_concept_id": "zBmE_n6s_MnQ",
    "region": "Västernorrlands län",
    "region_code": "22",
    "region_concept_id": "NvUF_SP1_1zo",
    "country": "Sverige",
    "country_code": "199",
    "country_concept_id": "i46j_HmG_v64",
    "street_address": null,
    "postcode": null,
    "city": null,
    "coordinates": [
      18.716616,
      63.290047
    ]
  }
```

Land och region är rätt fram. Men hur skall vi göra med kommun å det finns både
`municipality` och `city`. Tittar man på datat noggrannare så är `municipality`
det med bäst kvalitet då `city` ofta är tomt.

Med placering måste vi vara lite försiktiga då denna information inte alltid finns med.
Om inte land, region eller kommun inte är satt kommer vi i vår analys sätta
dessa till värdet okänt för att inte tappa bort de annonserna.

Vi har nu en tillräckligt bra förståelse för vår data för att kunna göra en djupare
analys.

## Analysen

Som tidigare beskrivit så gör vi analysen i Jupyter Notebook, du hittar hela
[analysfilen på GitLab](https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples/jupyter-notebook-on-historical-ads/-/blob/main/l%C3%A4rare%202021-py.ipynb).

Jupyter användes för att utforska och titta på datat. Men de
delar har vi valt att städa bort i den slutliga versionen för att göra det
lättläst. Koden har också effektiviserats något i samband med att analysen på
hela datasetet kördes.

Först sättes några initiala variabler för att enkelt kunna ändra period för
analysen. 

Steg ett är att läsa in filen och välja ut de poster som behövs. En vanlig
filläsare används för att få varje rad som egen post och att hantera de
nästlade fälten.

Efter att en rad är inläst så avgörs det om den är intressant. Det gör att
minnesbehovet blir lågt. Under tiden man arbetar med exempelfilen spelar
det ingen roll men när man arbetar med ett fullt år så kan det bli tungt
på en laptop. Detta är en orsak till att använda exempelfilen.

För varje post innehållandes en platsannons för lärare sparar vi undan
var den är placerad, samt sätter en etta på fältet för vilken typ av
lärartjänst det är. Genom att göra det så blir det senare enkelt att
sammanställa datat.

När filen är inläst har vi en lista med en post per platsannons. För en
grundskolärartjänst i Örnsköldsvik ser posten ut såhär:

```json
{
  "country": "Sverige", 
  "region": "Västernorrlands län",
  "municipality": "Örnsköldsvik", 
  "gymnasielarare_count": 0,
  "grundskollarare_count": 1, 
  "forskollarare_count": 0
}
```

Nästa steg är att gruppera per kommun, region och land. För exempelvis
Örnsköldsvik vill vi har en rad som ser ut såhär:


| Rike    | Region/Län          | Kommun       | Gymnasielärare | Grundskollärare | Förskollärare |
|---------|---------------------|--------------|----------------|-----------------|---------------|
| Sverige | Västernorrlands län | Örnsköldsvik | 3              | 17              | 5             |

Ett populärt Python bibliotek som har stöd för göra denna pivot tabell är Pandas. 
Pandas är ett snabbt och enkelt bibliotek för data analys och manipulation. Det är
också tåligt för skräpdata. 

För att göra pivottabellen talar vi om för Pandas vilka fält vi vill summera på och
att vi vill summera. Vi talar om vilka fält som är nyckel fält, dvs identifierar 
exempelvis identifierar vilken kommun det handlar om.

```python
municipality = pd.pivot_table(df,
   values=['gymnasielarare_count', 'grundskollarare_count', 'forskollarare_count' ],
   index=["country", "region", "municipality"],
   aggfunc=np.sum)
```

Vi gör på motsvarande sätt för Region och riks nivå.

Slutligen använder vi Pandas även för att skapa ett Excel-ark
med vårat resultat utifrån de tre pivottabellerna vi skapade.

## Sammanfattning

Vi har översiktligt gått igenom den data Jobtech Development har kring platsannonser
och att den är tillgänglig för alla. Utifrån ett konkret fall har vi översiktligt
tittat på Jupyter Notebook som verktyg för att utforska, analysera och dela vår analys.
Det finns självklart mycket mer att säga om både data och verktyg men vi hoppas
detta ger en start för att du skall kunna besvara frågor kring Svensk arbetsmarknad med
hjälp av historiska platsannonser och Jupyter Notebook.