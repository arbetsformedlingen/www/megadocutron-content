---
title: '2022-01-28'
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples/jobstream-example/-/blob/main//CHANGELOG.md
gitdir: /arbetsformedlingen/job-ads/getting-started-code-examples/jobstream-example
gitdir-file-path: /CHANGELOG.md
date: '2022-10-10 15:54:41'
path: /arbetsformedlingen/job-ads/getting-started-code-examples/jobstream-example/CHANGELOG.md
tags:
- CHANGELOG.md
---
# 2022-01-28
* Create columns from ad instead of dumping the whole ad in a single field. 
* Add code for filtering on occupation-related concept ids.

# 1.0
* First version
