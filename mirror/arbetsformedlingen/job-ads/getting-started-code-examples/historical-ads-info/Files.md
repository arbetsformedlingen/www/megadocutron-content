---
title: Working with historical ads files
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples/historical-ads-info/-/blob/main//Files.md
gitdir: /arbetsformedlingen/job-ads/getting-started-code-examples/historical-ads-info
gitdir-file-path: /Files.md
date: '2023-06-28 09:48:12'
path: /arbetsformedlingen/job-ads/getting-started-code-examples/historical-ads-info/Files.md
tags:
- Files.md
---
# Working with historical ads files

## File flavors:

There are 3 'file flavors' of enriched files for different use cases:

|File flavor   | Description  | Example usage | Size| 
|---|---|---|---|---|---|
| Sample  | 1% of the year's total, selected randomly  | trying out your application| 30-50 MB | 
| Metadata  | Description and other text fields removed  | Trends on dates and other kind of data in the ad  | 1-1.5 GB | 
| Full  | All data  | When you really need the ad text  | 3-5 GB | 


### - Sample files

- 1% of original number of ads
- Selected pseudo-randomly 
- All information in the ads are preserved
- Quicker to load
- To test your workflow and algorithms quickly before you run with full-size files
- The included ads will differ the next time we publish a file

### - Metadata files

- All ads from a year
- Text fields removed
- Smaller files, quicker to load
- in beta1, these fields are removed: `'headline', 'description', 'employer', 'keywords', 'application_details', 'hash', 'id', 'external_id',
  'uuid', 'logo_url', 'webpage_url'` 

### - Full files

- All ads from a year
- Big files (3-5 GB)
- Use when you need the ad text


## Download location

https://data.jobtechdev.se/annonser/historiska/berikade/index.html

## Tips for working with files

- The files are big, but since they are in jsonlines/ndjson format you can read one ad at the time.
- Start with the files with reduced number of ads. They are in the same format, but are much smaller. 1% of the ads
  still gives you 5-7000 ads to try on.
- When you have loaded a file, get what you need from the file and save to a file.  
  e.g. get publication dates and save them to a file.
- If you use a serializer like Python's Pickle, there's less risk of encoding errors when saving.
- When you have saved the data you think you need, you can start doing work with that.  
  This will be a much smaller data set, and it will go quicker and need less resources. When you run your code again, it
  will use the extracted data and don't have to read the ad files again.  
  e.g. all publication dates for 2016-2021 are about 45 MB. The full files for those years are 22 GB
- When you have a workflow, and have written the conversions and calculations you need, it's time to check the
  results.  
  If you have used the 1%-files, the results should look reasonable but any sums will of course be too low.
- Run everything again with the metadata files (or full size files if you need the ad text)
- This way you have learned how to use the data and been able to get your program working much quicker and with less
  frustration.
- Check the folder `code-examples` for an example on how to create a csv file with the number of ads published per month based on some criteria.  

## Other file formats
Ads in "original format" (they look like in the api) are available in jsonlines format
https://data.jobtechdev.se/annonser/historiska/index.html
And also as yearly zip-files with 12 jsonlines/ndjson-files inside, one for each month (publication date)
https://data.jobtechdev.se/annonser/historiska/monthly/index.html

The ads are also available in json-format. Since it's a format where you have to load everything, we do not recommend it. It's kept as a legacy format.



## Resources

[Our file conversion code on Gitlab](https://gitlab.com/arbetsformedlingen/job-ads/development-tools/historical-ads-file-converter)
