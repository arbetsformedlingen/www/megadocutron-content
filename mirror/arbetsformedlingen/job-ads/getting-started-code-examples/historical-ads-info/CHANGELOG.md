---
title: Historical Ads
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples/historical-ads-info/-/blob/main//CHANGELOG.md
gitdir: /arbetsformedlingen/job-ads/getting-started-code-examples/historical-ads-info
gitdir-file-path: /CHANGELOG.md
date: '2023-06-28 09:48:12'
path: /arbetsformedlingen/job-ads/getting-started-code-examples/historical-ads-info/CHANGELOG.md
tags:
- CHANGELOG.md
---
## Historical Ads


# 2022-02-03
- Better performance in file handling with explicit garbage collection

# 2022-01-31: First Beta release
- Ads 2016-2021 indexed and used by https://dev-historical-api.jobtechdev.se
- First version of frontend  https://staging-historical-search.jobtechdev.se/
- Files uploaded to https://data.jobtechdev.se/annonser/historiska/berikade/index.html

