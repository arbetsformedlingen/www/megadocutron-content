---
title: Frontend example - JobTech Links
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples/frontend-example/-/blob/main//README.md
gitdir: /arbetsformedlingen/job-ads/getting-started-code-examples/frontend-example
gitdir-file-path: /README.md
date: '2022-01-20 13:16:00'
path: /arbetsformedlingen/job-ads/getting-started-code-examples/frontend-example/README.md
tags:
- README.md
---
## Frontend example - JobTech Links

A simple example of a frontend for a Jobtech API, with pagination. Using FastAPI and the Jinja web template engine.

### Requirements
* Python 3.8 or later  

### Installation and running
Run the following in a terminal window:

    $ pip install -r requirements.txt  
    $ python main.py
    
In PyCharm:  
>Python  
Module name: uvicorn  
Parameters: main:app --reload

Go to http://127.0.0.1:8000 to see the search form.  



