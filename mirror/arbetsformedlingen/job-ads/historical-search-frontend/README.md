---
title: Historical Search Prototype
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/historical-search-frontend/-/blob/main//README.md
gitdir: /arbetsformedlingen/job-ads/historical-search-frontend
gitdir-file-path: /README.md
date: '2023-09-26 14:34:49'
path: /arbetsformedlingen/job-ads/historical-search-frontend/README.md
tags:
- README.md
---
# Historical Search Prototype

To run with podman:

```shell
podman build -t my-develop .
podman run -p 8080:8080  my-develop
```

or Docker:

```shell
podman build -t my-develop .
podman run -p 8080:8080 my-develop
```
