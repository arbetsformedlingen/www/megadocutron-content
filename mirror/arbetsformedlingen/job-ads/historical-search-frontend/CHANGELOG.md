---
title: May 2022
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/historical-search-frontend/-/blob/main//CHANGELOG.md
gitdir: /arbetsformedlingen/job-ads/historical-search-frontend
gitdir-file-path: /CHANGELOG.md
date: '2023-09-26 14:34:49'
path: /arbetsformedlingen/job-ads/historical-search-frontend/CHANGELOG.md
tags:
- CHANGELOG.md
---
Changelog
===============================
### May 2022
* Updated text for which time period we have ads
* Show single ad from search result html-formatted

### February 2022
* Format dates


### January 2022
* Changed css file and removed unused files
* Single page with Javascript search
* Search with free text query and optional from- and to-dates. Default seting to only show number of ads and positions
