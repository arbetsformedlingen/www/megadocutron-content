---
title: Present what searches are done in JobSearch API
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/search-trends/-/blob/main//README.md
gitdir: /arbetsformedlingen/job-ads/search-trends
gitdir-file-path: /README.md
date: '2023-08-21 14:01:09'
path: /arbetsformedlingen/job-ads/search-trends/README.md
tags:
- README.md
---
# Present what searches are done in JobSearch API

Get logs from OpenSearch and summarize them. Check search words and phrases against whitelist to prevent unwanted data
to be included

## Why?

- Get to know how the api:s are being used.
- Make it public what the people searching for jobs are searching for

## Searchlogs

- Collected with OpenSearch queries
- Yesterday's logs are collected and a temp file is created for every hour
- Hourly temp files are summarized and checked
- Result is written to json files

### Output format

Searches are split into their parameters. Right now we only present the parts, not how they are combined
All parameters are sorted by frequency.
concept ids are translated and copied into separate entries, so that a human-readable name is used

For details on the fields, look in `docs/fields.md`

### Limitations

Some free text searches are removed for GDPR reasons. There are searches for names, telephone numbers and similar that
is excluded in the output. Generally, these are not frequent and theoir omission shouln't affect any statistics or
trends.
Complete search combinations are not included, only the parts

## Output file

JSON-format, each parameter is a list of tuples with the search parameter and its frequency.
e.g. the most common free text searches:
`"q_approved": [["stockholms län", 27261], ["undersköterska", 26462], ["personlig assistent", 24078], ["lagerarbetare", 20737],`
"stockholms län" was used in free text searches 27261 times during this day.

When concept ids are used, a translation is also provided. e.g
`"region": [["CifL_Rzy_Mku", 150831], ["CaRE_1nn_cSU", 73236], ["zdoY_6u5_Krt", 48812],`
and its translated companion:
`"region-label": [["Stockholms län", 150831], ["Skåne län", 73236], ["Västra Götalands län", 48812], `

## Running the program

This code is dependent on how Arbetsförmedlingen has configured logging for JobSearch.
You will not be able to run it unless you have access to these logs.
More info can be found in docs/developer_setup.md

## Documentation

More info is found in the `docs` folder
