---
title: Python dependencies
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/search-trends/-/blob/main//docs/developer_setup.md
gitdir: /arbetsformedlingen/job-ads/search-trends
gitdir-file-path: /docs/developer_setup.md
date: '2023-08-21 14:01:09'
path: /arbetsformedlingen/job-ads/search-trends/docs/developer_setup.md
tags:
- docs::developer_setup.md
- developer_setup.md
---
## Python dependencies

This project uses [Poetry](https://python-poetry.org/) to manage dependencies.

### Install poetry

`pip install poetry`

### `Install dependencies

`poetry install`

## Build Docker/Podman image

`podman build -t search-stats:dev .`

Create a file env containing necessary environment variables:

```
OPENSEARCH_HOST=my_opensearch_host
OPENSEARCH_PORT=9200
OPENSEARCH_USER=myuser
OPENSEARCH_PWD=mypassword
OPENSEARCH_INDEX_PREFIX=filebeat-*
WORK_DIR=/tmp
```

Run with: `podman run --env-file=env search-stats:dev collect_stats`

## Running it locally

`python main.py` this will collect logs from yesterday, summarize them and create an output file
