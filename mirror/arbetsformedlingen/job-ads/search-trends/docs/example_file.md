---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/search-trends/-/blob/main//docs/example_file.md
gitdir: /arbetsformedlingen/job-ads/search-trends
gitdir-file-path: /docs/example_file.md
date: '2023-08-21 14:01:09'
path: /arbetsformedlingen/job-ads/search-trends/docs/example_file.md
tags:
- docs::example_file.md
- example_file.md
---
Given the file `example.json``, let´s take a look at some of the content

`{"q_approved": [["stockholms län", 26769], ["personlig assistent", 25706]],`
Thies is the two most common free text queries, using the `q`parameter.
The data is cleaned up so that no personally identifiable information is published this way

```
region": [["CifL_Rzy_Mku", 160613], ["CaRE_1nn_cSU", 77012]], 
"region-label": [["Stockholms län", 160613], ["Skåne län", 77012]],
"mu[jobsearch-2022-11-12-daily-public.json](..%2Fjobsearch-2022-11-12-daily-public.json)nicipality": [["AvNB_uwa_6n6", 107983], ["PVZL_BQT_XtL", 87873]], 
"municipality-label": [["Stockholm", 107983], ["Göteborg", 87873]],
"country": [["-199", 799], ["-i46j_HmG_v64", 458]], 
"country-label": [["-Sverige (old)", 799], ["-Sverige", 458]],
"occupation-name": [["pBhS_6fg_727", 1192], ["sizv_uPq_nWf", 1185]],
"occupation-name-label": [["Butikssäljare, dagligvaror/Medarbetare, dagligvaror", 1192], ["Lagerarbetare", 1185]],
"occupation-group": [["oQUQ_D11_HPx", 23658], ["YKL2_FCB_1yr", 23229]],
"occupation-field-label": [["Administration, ekonomi, juridik", 39892], ["Chefer och verksamhetsledare", 24073]],
"occupation-collection": [["UdVa_jRr_9DE", 50275], ["-UdVa_jRr_9DE", 7304]],
 "occupation-collection-label": [["Yrkessamling, yrken utan krav på utbildning", 50275], ["-Yrkessamling, yrken utan krav på utbildning", 7304]],
```

These are queries using different parameters with concept ids as value. The search is done with concept ids,
they are shown with their label in the second entry `*-label` to make it easier to read

`"driving-license-required": [["false", 41678]],`
Avoiding ads where driving license is required is the most common use of this parameter
}

The other fields in the example file or daily output file follows a similiar pattern:
`field: [ [value1, frequency], [value2, frequency] ]`
All fields with description are found in the file `fields.md`
