---
title: 1.1.2
gitlaburl: https://gitlab.com/arbetsformedlingen/job-ads/search-trends/-/blob/main//CHANGELOG.md
gitdir: /arbetsformedlingen/job-ads/search-trends
gitdir-file-path: /CHANGELOG.md
date: '2023-08-21 14:01:09'
path: /arbetsformedlingen/job-ads/search-trends/CHANGELOG.md
tags:
- CHANGELOG.md
---
# 1.1.2
* Update Python version in Dockerfile

# 1.1.1 (October 2022)
* Updated the handle of certifictes 

# 1.1.0 (October 2022)
* Changed database from Elasticsearch to OpenSearch

# 1.0.1 (September 2022)
* Update Python in Dockerfile to 3.10.7

# 1.0.0 (June 2022)
* First version
