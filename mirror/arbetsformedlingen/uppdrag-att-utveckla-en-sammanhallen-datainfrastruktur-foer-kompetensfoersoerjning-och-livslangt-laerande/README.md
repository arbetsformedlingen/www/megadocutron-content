---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/uppdrag-att-utveckla-en-sammanhallen-datainfrastruktur-foer-kompetensfoersoerjning-och-livslangt-laerande/-/blob/main//README.md
gitdir: /arbetsformedlingen/uppdrag-att-utveckla-en-sammanhallen-datainfrastruktur-foer-kompetensfoersoerjning-och-livslangt-laerande
gitdir-file-path: /README.md
date: '2022-02-01 12:53:39'
path: /arbetsformedlingen/uppdrag-att-utveckla-en-sammanhallen-datainfrastruktur-foer-kompetensfoersoerjning-och-livslangt-laerande/README.md
tags:
- README.md
---
<div>

<h1>Uppdrag att utveckla en sammanhållen datainfrastruktur för kompetensförsörjning och livslångt lärande</h1>

<div>

</div>
</div>
<div>

Här presenteras pågående projekt/piloter/initiativ som finns inom Arbetsförmedlingen för regeringsuppdraget att utveckla en sammanhållen datainfrastruktur för kompetensförsörjning och livslångt lärande. Syftet med denna sida är att ge en översikt och möjlighet att följa det pågående arbetet. Uppdraget är indelat i tre delområden:

1. Tillgängliggöra och länka data om utbildningsutbud, inklusive utveckling av SUSA-navet
2. Utveckling av gemensamma begreppsstrukturer inom utbildnings- och arbetsmarknadsområdet, inklusive att tillgängliggöra data om kvalifikationer
3. Utveckling av säkra metoder för hantering av individdata

<h2>Tillgängliggöra och länka data om utbildningsutbud, inklusive utveckling av SUSA-navet</h2>

I en värld i konstant förändring, med mängder av information och data har myndigheterna en roll att samverka med andra kring öppna data och standarder i ett digitalt ekosystem. Syftet är att möjliggöra transformationen och bidra till en mer inkluderande, kompetensbaserad och datadriven arbets- och utbildningsmarknad. Fokus för aktuell verksamhet ligger bl.a.på individens rätt att äga och kontrollera sin egen data, och att skapa en gemensam begreppsstruktur för arbete och utbildning. Arbetsförmedlingen, via enheten Jobtech, strävar efter att utmana gängse strukturer och arbetssätt, att tänka nytt och vara transparenta.

• **En ansvarsfull teknikutveckling**\
För att säkerställa en ansvarsfull teknikutveckling är det viktigt att noggrant överväga potentiella risker och påverkan på samhället och dess invånare. Därför genomförs kontinuerligt även en etisk granskning tillsammans med pilotverksamheter och annat undersökande arbete. I arbetet används etiska utvärderingsmallar för tillämpad etik (t.ex. Ethics Canvas) som komplement till granskning av dokument och intervjuer med inblandade. Övergripande utgångspunkter är hållbarhetsmålen inom Agenda 2030, jämställdhet, tillgänglighet, icke-diskriminering samt frågor om transparens, ansvar, design och socio-ekonomisk påverkan. Etisk utvärdering sker inom samtliga delområden och inte enbart kopplat till att tillgängliggöra och länka data om utbildningsutbud. Utvärderingar inbegriper men är inte begränsade till underlag för beslut kopplade till frågor om integritetsskydd och konsekvensbedömningar. Perspektivet bör även genomsyra myndigheternas genomförande av uppdraget.

• **Förutsättningskapande insatser för digital vägledning inom utbildnings- och arbetsmarknadsområdet**\
Hur hittar man rätt i mängder av data och information i en alltmer digitaliserad miljö? Hur säkerställer man att man hittar rätt utbildning, kopplad till de yrken där motsvarande kompetenser efterfrågas? Inom uppdraget lägger Arbetsförmedlingen stort fokus på samverkan och digital vägledning i en sammanhållen datainfrastruktur, och jobbar bl.a. med att skapa möjligheter för individen att synliggöra sina kompetenser och hitta rätt utbildning till rätt jobb – en viktig förutsättning för en hållbar arbetsmarknad i konstant förändring. Under en intensiv undersökningsfas har ett team av utvecklare och UX-designers jobbat med att fånga upp individens och marknadens behov ur olika perspektiv för att hitta typfall och konkret uppgift som gynnar individen på en arbetsmarknad i ständig förändring. Ett önskat resultat är att skapa förutsättningar till en kvalitetssäkrad och förbättrad digital vägledning mellan yrken och jobb; en effektivare kartläggning som leder till ökade synergieffekter och utveckling av funktioner som bidrar till en mer hållbar arbetsmarknad. Primärt fokus har varit att hitta bästa möjliga lösning för individen på väg mot karriärskifte och/eller kompetensutveckling. Detta genom att realisera och bistå utvecklingen av tjänster för validering, matchning och vägledning – att tillgängliggöra, dela och nyttiggöra data – för att matcha behoven på arbetsmarknaden med utbildningsutbudet.

\
Innehåll:\
**•Digital vägledning: koppling mellan utbildningar och yrke**\
Att genom ett tvärfunktionellt arbetssätt koppla utbildningsdata i SUSA-navet med arbetsmarknadsdata. Ett önskat resultat skapar förutsättningar för digital vägledning för individen att hitta rätt utbildning, kopplad till de yrken där motsvarande kompetenser efterfrågas.

• **Berika och utveckla Arbetsförmedlingens digitala tjänst ”Hitta yrken”**\
Möjligheter att berika och utveckla Arbetsförmedlingens tjänst ”Hitta yrken” undersöks, vilken kopplar ihop Arbetsförmedlingens yrken och yrkesområden med yrkeshögskoleutbildningarna som tillhandahålls av Myndigheten för yrkeshögskolan (MYH).

• **Återanvända know-how för att skapa synergieffekter: bygga API för utbildningar, liknande JobAd Enrichments**\
Utöver att tillgängliggöra öppna data är ambitionen också att skapa förutsättningar för andra att kunna analysera dem. Ett exempel på detta är API:et JobAd Enrichments, vars syfte är att hjälpa till att strukturera och sortera data för att underlätta för arbetssökande och arbetsgivare att hitta varandra. API:et är byggt på en AI-lösning som väljer ut begrepp som är relevanta och på så sätt gör den viktiga informationen i jobbannonser lättare att hitta. API:et bidrar till en snabbare och mer träffsäker jobbmatchning som sparar tid och resurser, vilket leder till en mer hållbar jobbmatchning.

Baserat på JobAd Enrichments undersöker teamet möjligheten att använda samma teknik för att utveckla ett API för utbildningar; en lösning som identifierar och extraherar relevant information i till exempel kursbeskrivningar och läroplaner. Ett viktigt första steg är att komplettera och kvalitetssäkra information och data i Skolverkets utbildningsdatabas, SUSA-navet.

• **Samverkan med offentliga och privata aktörer: för effektivare kartläggning**\
För att identifiera konkreta behov på marknaden, samt individens behov av karriär- och kompetensutveckling har utvecklarna träffat en rad organisationer: Trygghetsrådet (TRR), Trygghetsfonden (TSL), Kammarkollegiet, Mälardalens högskola (MDH), Universitets- och högskolerådet (UHR, studera.nu), EMG (Educations.com Media Group, allastudier.se), Statistiska centralbyrån (SCB), analytiker från Arbetsförmedlingen, med flera.

Kontaktperson/er:

Följ arbetet här: <https://gitlab.com/arbetsformedlingen/education>

<h2>Utveckling av gemensamma begreppsstrukturer inom utbildnings- och arbetsmarknadsområdet, inklusive att tillgängliggöra data om kvalifikationer</h2>

•**Myndighetsgemensamt projekt – Utredning av förutsättningarna för en sammanhängande semantik**\
Myndigheterna driver tillsammans ett projekt som syftar till att gemensamt utreda förutsättningarna att utveckla gemensamma begrepp eller översättningsnycklar mellan existerande begreppsstrukturer inom utbildnings- och arbetsmarknadsområdet. Målet är att beskriva om, och i så fall vad och hur, det är lämpligt att utveckla datainfrastrukturens gemensamma semantik. Den semantiken kan stärka förutsättningarna att tillhandahålla digitala tjänster inom domänen kompetensförsörjning och livslångt lärande. Projektet väntas leda till en gemensam beskrivning av lämpliga avgränsningar och utvecklingsscenarier, med hänsyn till olika tidshorisont, för en eventuell gemensam begreppsstruktur och/eller översättningsnycklar inom domänen kompetensförsörjning och livslångt lärande.

Kontaktperson/er: [fredrik.ribbing@arbetsformedlingen.se](mailto:fredrik.ribbing@arbetsformedlingen.se)\
Följ arbetet här:

\
• **En konsekvensanalys av en mer sammanhållen begreppsstruktur**\
Som del av den myndighetsgemensamma utredningen av en gemensam semantik utförs även en konsekvensanalys. Den består av:

* En kostnadsanalys
* En mervärdesanalys
* En etisk konsekvensanalys

\
Kontaktperson/er: [fredrik.ribbing@arbetsformedlingen.se](mailto:fredrik.ribbing@arbetsformedlingen.se), [my.x.svensson@arbetsformedlingen.se](mailto:my.x.svensson@arbetsformedlingen.se)\
Följ arbetet här:

**_Utvecklingsinsatser för att testa olika möjligheter att föra samman en sammanhållen begreppsstruktur inom utbildnings- och arbetsmarknadsområdet_**

\
• **Begreppsigenkänning och taxonomin**\
För att taxonomin ska bli användbar för att matcha arbetstagare mot utbildningar samt att göra utbildningar sökbara är ambitionen att koppla begreppen i taxonomin mot utbildningsbeskrivningar där dessa begrepp förekommer.\
Ett område som undersöks är så kallad begreppsigenkänning som innebär att en text av intresse analyseras maskinellt så att ord eller delar av meningar ur texten länkas mot begrepp i taxonomin. I litteraturen är detta känt som ”semantic annotation” eller ”concept recognition” och används bl. a. inom biomedicin. De texter som kan vara intressanta att analysera på detta sätt är utbildningsbeskrivningar med kursplaner. Utöver att känna igen begrepp i en text ligger fokus även på att kategorisera sammanhanget där varje begrepp förekommer: är begreppet ett förkunskapskrav, lärandemål eller någonting annat?\
Det finns flera användningsområden för begreppsigenkänningsanalyser av utbildningsbeskrivningar. Ett exempel är en arbetssökande som inte har alla yrkesfärdigheter som behövs för ett visst jobb. Om de färdigheter som saknas motsvaras av begrepp ur taxonomin kan man hitta de utbildningsbeskrivningar som i störst utsträckning anger dessa begrepp som lärandemål. Det blir också lätt att hitta liknande utbildningar baserat på i vilken grad taxonomibegreppen för två olika utbildningar överlappar. Genom att representera utbildningsbeskrivningar med hjälp av taxonomibegrepp med precisa definitioner fångar man betydelsen av texten på ett sätt som underlättar sökbarhet och matchning.\
Teknik för begreppsigenkänning kan grovt delas in i två grupper: a) metoder som bygger på maskininlärning och b) metoder som bygger på matchning av textsträngar. Metoder som baseras på maskininlärning kan vara effektiva förutsatt att de tränas på tillräckligt mycket data men att förbereda dessa data är arbetskrävande. Därför är även metoder baserat på direkt jämförelse av textsträngar intressanta eftersom dessa inte kräver träningsdata för att fungera. Särskilt intressant för maskininlärning är BERT och Sentence-BERT som är algoritmer för att numeriskt koda innebörden av skriven text. Värt att notera är att BERT tidigare har utvärderats för användning av översättningsnycklar mellan begreppsstrukturer för arbetsmarknaden . Kungliga biblioteket har även offentliggjort färdigtränade BERT-modeller för svenska språket.

Olika metoder för att känna igen begrepp i utbildningsbeskrivningar undersöks, så att de kan göras sök- och matchningsbara, likaså undersöks matematiska representationer av taxonomin för att ytterligare underlätta sökning, matchning och indexering av begrepp. Utöver det byggs verktyg för att kunna mäta hur effektiv en metod för begreppsigenkänning är, för att utvärdera vilken metod som fungerar bäst.

\
Kontaktperson/er:\
Följ arbetet: [här](https://gitlab.com/groups/arbetsformedlingen/taxonomy-dev/backend/-/boards) [och här](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/nlp-education-analyzer)

\
• **Utbildningsdata från Myndigheten för yrkeshögskolan och Skolverket**\
Arbetet är i en inledande fas och utforskar bl.a. vilken utbildningsdata som finns från Myndigheten för yrkeshögskolan (MYH) och Skolverket för att kartlägga vilka utbildningar som kan matcha behovet på arbetsmarknaden, samt vilka av dem leder till jobb i praktiken. Målet är att all tillgängliga utbildningsdata ska användas stegvis för att kunna matcha begrepp i utbildningar och kopplas till taxonomin.

\
Kontaktperson/er:\
Följ arbetet här:

\
• **Sökbegrepp kvalifikationer och kompetenser**

Första steget i det här arbetet syftar till att utforska möjliga justeringar av nuvarande datamängder inom taxonomin. Arbetsförmedlingen kommer under projekttiden delta i ett utforskande pilotprojekt för att kartlägga ESCOs kompetenser till formella kvalifikationer (exempelvis yrkeshögskoleutbildning) och icke-formella kvalifikationer (dvs sådana som finns utanför det formella utbildningssystemet men som ändå erhålls under strukturerad form, exempelvis yrkesbevis).

\
Kontaktperson/er: [david.norman@arbetsformedlingen.se](mailto:david.norman@arbetsformedlingen.se)\
Följ arbetet här:

\
**• En översikt över teorier och bedömningsgrunder för en bra matchning**

Där livslångt lärande möter arbetslivets behov av kompetensförsörjning är frågor om matchning en viktig kärna. Inte minst gäller det då Artificiell intelligens (AI) och andra statistiska analyser används för att analysera vad som krävs för att förbättra matchningen. Arbetsförmedlingen har därför påbörjat ett arbete med att, från olika vetenskapliga perspektiv, översiktligt beskriva vad en bra matchning innebär. Samt att beskriva vilka indikatorer och bedömningsgrunder som används för att i empiriska studier klassificera en matchning som mer eller mindre bra. Översikten innehåller även det etiska perspektivet på digital matchning: Digital teknik och bra matchning. Vilka etiska utmaningar och hinder står vi inför?

\
Kontaktperson/er: [fredrik.ribbing@arbetsformedlingen.se](mailto:fredrik.ribbing@arbetsformedlingen.se), [my.x.svensson@arbetsformedlingen.se](mailto:my.x.svensson@arbetsformedlingen.se)\
Följ arbetet här:


<h2>Utveckling av säkra metoder för hantering av individdata</h2>

* **Utvecklingsprojekt för individcentrerad digital infrastruktur för dataportabilitet**

Data är hårdvaluta i dagens digitala värld. Att få äga sin egen data har blivit lika nödvändigt som att få välja vem man vill dela den med. Med en gemensam datainfrastruktur möjliggörs en större transparens och individens kontroll av personlig data blir ett nåbart mål. I arbetet ska betydelsen av dataportabilitet och spårbarhet uppmärksammas. Långsiktigt kommer infrastrukturen att gynna kompetensförsörjningen i Sverige och det livslånga lärandet för individen. Bristande infrastruktur för datautbyte är ett vanligt förekommande problem i Sverige och Europa. Projektets mål är därför att bygga en infrastruktur för utbyte av individdata mellan offentliga, privata och individuella aktörer. Individen har en central roll i processen genom att äga, kontrollera och hantera sin personliga data. Inom infrastrukturen ska individen kunna begära tillgång till sin personliga data från myndigheter och andra offentliga aktörer (t.ex. skolor, universitet), men även från privata företag (t ex banker, försäkringsbolag), för att sedan kunna dela den med tredje part, antingen privat eller offentligt. Normalt måste individen manuellt samla in och leverera dokument till institutioner och privata aktörer, oavsett ärende. Processen blir än mer ineffektiv när individen behöver dokumentation från flera källor, så att flerfaldiga processer sammanlänkas. Baserat på rekommenderade riktlinjer och de tidigare fallstudierna, startade projektet med en utvärdering av olika teknologier som kan fullgöra en individcentrerad och transparent kunskap och kontroll för individen. Projektet är därför fokuserat på att utveckla en individcentrerad digital infrastruktur där individen har utökad kontroll över sin personliga data och hur den används, och utgör därmed en komponent i utvecklingen av en sammanhängande datainfrastruktur för kompetensförsörjning och livslångt lärande. Komponenten kan troligen även nyttjas inom andra datadomäner.

• **Pilotprojekt: Jobbsprånget**\
Jobbsprånget är ett praktikprogram, som erbjuder praktikplatser åt unga akademiker utanför EU. Programmet är ett samarbete mellan Kungliga vetenskapsakademin (IVA) och Arbetsförmedlingen. Piloten fokuserar på godkännandeprocessen: att möjliggöra för kandidaten att begära och samla all sin personliga data från en myndighet och dela den med tredje part (antingen privat eller offentligt).

\
Målsättningar:

• Att få ut mer av existerande lagstiftning

• Individcentrerat fokus

• Skalbarhet

• Användarvänlighet och enkel integration

• Open Source: All vår källkod är öppen för allmän medverkan Skapandet av en digital plånbok (wallet), som ger individen rätt att begära och dela sin data med andra parter. För full GDPR-förenlighet krävs att all data och dokumentation som delas godkänns av individen före delning.

\
Delar av infrastrukturen:

• En ‘datamottagare’ som via vårt ‘klientbibliotek’ instruerar individen;

• Kontrollpanelen (wallet) där individen kan kontrollera sin data, och godkänna eller neka medgivandet att dela data;

• Databehållaren som via exporter gör data tillgänglig i kontrollpanelen.

\
Kontaktperson/er:\
Följ arbetet här: <https://gitlab.com/arbetsformedlingen/individdata/oak>

</div>
