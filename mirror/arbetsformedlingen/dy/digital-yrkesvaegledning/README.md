---
title: Digital yrkesvägledning
gitlaburl: https://gitlab.com/arbetsformedlingen/dy/digital-yrkesvaegledning/-/blob/main//README.md
gitdir: /arbetsformedlingen/dy/digital-yrkesvaegledning
gitdir-file-path: /README.md
date: '2023-10-02 17:48:42'
path: /arbetsformedlingen/dy/digital-yrkesvaegledning/README.md
tags:
- README.md
---
# Digital yrkesvägledning

Lab repository för att undersöka möjligheten till en
digital yrkesvägledningstjänst baserad på arbetsmarknadsdata,
utbildningskataloger mm.

Läs mer på [Wikin](https://gitlab.com/arbetsformedlingen/dy/digital-yrkesvaegledning/-/wikis/home).
