---
title: digi-list-list
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/package/src/components/_list/list/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/package/src/components/_list/list/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_list/list/readme.md
tags:
- libs::arbetsformedlingen::package::src::components::_list::list::readme.md
- arbetsformedlingen::package::src::components::_list::list::readme.md
- package::src::components::_list::list::readme.md
- src::components::_list::list::readme.md
- components::_list::list::readme.md
- _list::list::readme.md
- list::readme.md
- readme.md
---
# digi-list-list

<!-- Auto Generated Below -->


## Properties

| Property     | Attribute      | Description                                                 | Type                                                       | Default           |
| ------------ | -------------- | ----------------------------------------------------------- | ---------------------------------------------------------- | ----------------- |
| `afListType` | `af-list-type` | Sätter typ. Kan vara 'bullet', 'numbered', eller 'checked'. | `ListType.BULLET \| ListType.CHECKED \| ListType.NUMBERED` | `ListType.BULLET` |


## Slots

| Slot        | Description                |
| ----------- | -------------------------- |
| `"default"` | ska innehålla <li>-element |


## CSS Custom Properties

| Name                           | Description                 |
| ------------------------------ | --------------------------- |
| `--digi--list--margin--bottom` | var(--digi--margin--large); |
| `--digi--list--margin--top`    | initial;                    |


## Dependencies

### Depends on

- [digi-typography](../../../__core/_typography/typography)
- [digi-icon-check](../../../__core/_icon/icon-check)
- [digi-util-mutation-observer](../../../__core/_util/util-mutation-observer)

### Graph
```mermaid
graph TD;
  digi-list --> digi-typography
  digi-list --> digi-icon-check
  digi-list --> digi-util-mutation-observer
  style digi-list fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
