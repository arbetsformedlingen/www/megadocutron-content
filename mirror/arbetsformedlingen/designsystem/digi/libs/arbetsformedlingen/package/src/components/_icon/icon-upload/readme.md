---
title: digi-icon-upload
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/package/src/components/_icon/icon-upload/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/package/src/components/_icon/icon-upload/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-upload/readme.md
tags:
- libs::arbetsformedlingen::package::src::components::_icon::icon-upload::readme.md
- arbetsformedlingen::package::src::components::_icon::icon-upload::readme.md
- package::src::components::_icon::icon-upload::readme.md
- src::components::_icon::icon-upload::readme.md
- components::_icon::icon-upload::readme.md
- _icon::icon-upload::readme.md
- icon-upload::readme.md
- readme.md
---
# digi-icon-upload



<!-- Auto Generated Below -->


## Properties

| Property              | Attribute                | Description                                                                   | Type      | Default     |
| --------------------- | ------------------------ | ----------------------------------------------------------------------------- | --------- | ----------- |
| `afDesc`              | `af-desc`                | Lägger till ett descelement i svg:n                                           | `string`  | `undefined` |
| `afSvgAriaHidden`     | `af-svg-aria-hidden`     | För att dölja ikonen för skärmläsare. Default är satt till true.              | `boolean` | `true`      |
| `afSvgAriaLabelledby` | `af-svg-aria-labelledby` | Referera till andra element på sidan för att definiera ett tillgängligt namn. | `string`  | `undefined` |
| `afTitle`             | `af-title`               | Lägger till ett titleelement i svg:n                                          | `string`  | `undefined` |


## CSS Custom Properties

| Name                   | Description                         |
| ---------------------- | ----------------------------------- |
| `--digi--icon--color`  | var(--digi--color--icons--primary); |
| `--digi--icon--height` | auto;                               |
| `--digi--icon--width`  | initial;                            |


## Dependencies

### Used by

 - [digi-form-file-upload](../../_form/form-file-upload)

### Graph
```mermaid
graph TD;
  digi-form-file-upload --> digi-icon-upload
  style digi-icon-upload fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
