---
title: digi-notification-alert
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/package/src/components/_notification/notification-alert/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/package/src/components/_notification/notification-alert/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_notification/notification-alert/readme.md
tags:
- libs::arbetsformedlingen::package::src::components::_notification::notification-alert::readme.md
- arbetsformedlingen::package::src::components::_notification::notification-alert::readme.md
- package::src::components::_notification::notification-alert::readme.md
- src::components::_notification::notification-alert::readme.md
- components::_notification::notification-alert::readme.md
- _notification::notification-alert::readme.md
- notification-alert::readme.md
- readme.md
---
# digi-notification-alert

An alert component.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { NotificationAlertVariation, NotificationAlertSize, NotificationAlertHeadingLevel } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property           | Attribute             | Description                                                            | Type                                           | Default                                        |
| ------------------ | --------------------- | ---------------------------------------------------------------------- | ---------------------------------------------- | ---------------------------------------------- |
| `afCloseAriaLabel` | `af-close-aria-label` | Stängknappens attribut 'aria-label'                                    | `string`                                       | `'Stäng meddelandet'`                          |
| `afCloseable`      | `af-closeable`        | Gör det möjligt att stänga meddelandet med en stängknapp               | `boolean`                                      | `false`                                        |
| `afHeading`        | `af-heading`          | Rubrikens text                                                         | `string`                                       | `undefined`                                    |
| `afHeadingLevel`   | `af-heading-level`    | Sätter rubrikens vikt. 'h2' är förvalt.                                | `"h1" \| "h2" \| "h3" \| "h4" \| "h5" \| "h6"` | `NotificationAlertHeadingLevel.H2`             |
| `afId`             | `af-id`               | Sätter attributet 'id'. Ges inget värde så genereras ett slumpmässigt. | `string`                                       | `randomIdGenerator('digi-notification-alert')` |
| `afSize`           | `af-size`             | Sätter storlek. Kan vara 'small', 'medium' eller 'large'.              | `"large" \| "medium" \| "small"`               | `NotificationAlertSize.LARGE`                  |
| `afVariation`      | `af-variation`        | Sätter variant. Kan vara 'info', 'success', 'warning' eller 'danger'.  | `"danger" \| "info" \| "success" \| "warning"` | `NotificationAlertVariation.INFO`              |


## Events

| Event       | Description                                                                     | Type                      |
| ----------- | ------------------------------------------------------------------------------- | ------------------------- |
| `afOnClose` | Stängknappens 'onclick'-event                                                   | `CustomEvent<MouseEvent>` |
| `afOnReady` | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>`        |


## Methods

### `afMGetHeadingElement() => Promise<any>`

Hämtar en referens till rubrikelementet. Bra för att t.ex. sätta fokus programmatiskt.

#### Returns

Type: `Promise<any>`




## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


## CSS Custom Properties

| Name                                                        | Description                                                                         |
| ----------------------------------------------------------- | ----------------------------------------------------------------------------------- |
| `--digi--notification-alert--border--danger`                | var(--digi--border-width--secondary) solid var(--digi--color--border--danger);      |
| `--digi--notification-alert--border--informative`           | var(--digi--border-width--secondary) solid var(--digi--color--border--informative); |
| `--digi--notification-alert--border--success`               | var(--digi--border-width--secondary) solid var(--digi--color--border--success);     |
| `--digi--notification-alert--border--warning`               | var(--digi--border-width--secondary) solid var(--digi--color--border--warning);     |
| `--digi--notification-alert--heading--font-size`            | var(--digi--typography--heading-3--font-size--desktop);                             |
| `--digi--notification-alert--heading--font-size--large`     | var(--digi--typography--heading-3--font-size--desktop-large);                       |
| `--digi--notification-alert--heading--font-weight`          | var(--digi--typography--heading-3--font-weight--desktop);                           |
| `--digi--notification-alert--icon--background--danger`      | var(--digi--color--background--danger-1);                                           |
| `--digi--notification-alert--icon--background--informative` | var(--digi--color--background--informative);                                        |
| `--digi--notification-alert--icon--background--success`     | var(--digi--color--background--success-1);                                          |
| `--digi--notification-alert--icon--background--warning`     | var(--digi--color--background--warning-1);                                          |
| `--digi--notification-alert--icon--color--danger`           | var(--digi--color--text--inverted);                                                 |
| `--digi--notification-alert--icon--color--informative`      | var(--digi--color--text--inverted);                                                 |
| `--digi--notification-alert--icon--color--success`          | var(--digi--color--text--inverted);                                                 |
| `--digi--notification-alert--icon--color--warning`          | var(--digi--color--text--primary);                                                  |
| `--digi--notification-alert--icon--padding--large`          | var(--digi--gutter--largest);                                                       |
| `--digi--notification-alert--icon--padding--medium`         | var(--digi--gutter--largest);                                                       |
| `--digi--notification-alert--icon--padding--small`          | var(--digi--gutter--large);                                                         |
| `--digi--notification-alert--icon--width--large`            | 2.25rem;                                                                            |
| `--digi--notification-alert--icon--width--medium`           | 1.4375rem;                                                                          |
| `--digi--notification-alert--icon--width--small`            | 1.4375rem;                                                                          |
| `--digi--notification-alert--max-width--large`              | 100%;                                                                               |
| `--digi--notification-alert--max-width--medium`             | 31.25rem;                                                                           |
| `--digi--notification-alert--max-width--small`              | 18.75rem;                                                                           |


## Dependencies

### Used by

 - [digi-form-error-list](../../_form/form-error-list)

### Depends on

- [digi-layout-media-object](../../../__core/_layout/layout-media-object)
- [digi-icon](../../../__core/_icon/icon)
- [digi-button](../../../__core/_button/button)
- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-notification-alert --> digi-layout-media-object
  digi-notification-alert --> digi-icon
  digi-notification-alert --> digi-button
  digi-notification-alert --> digi-typography
  digi-form-error-list --> digi-notification-alert
  style digi-notification-alert fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
