---
title: header-navigation-item
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/package/src/components/_header/header-navigation-item/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/package/src/components/_header/header-navigation-item/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_header/header-navigation-item/readme.md
tags:
- libs::arbetsformedlingen::package::src::components::_header::header-navigation-item::readme.md
- arbetsformedlingen::package::src::components::_header::header-navigation-item::readme.md
- package::src::components::_header::header-navigation-item::readme.md
- src::components::_header::header-navigation-item::readme.md
- components::_header::header-navigation-item::readme.md
- _header::header-navigation-item::readme.md
- header-navigation-item::readme.md
- readme.md
---
# header-navigation-item



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute         | Description                      | Type      | Default     |
| --------------- | ----------------- | -------------------------------- | --------- | ----------- |
| `afCurrentPage` | `af-current-page` | Sätter vilken sida som är active | `boolean` | `undefined` |


## CSS Custom Properties

| Name                                           | Description                                            |
| ---------------------------------------------- | ------------------------------------------------------ |
| `--digi--header-navigation-item--color`        | var(--digi--color--text--primary);                     |
| `--digi--header-navigation-item--color-active` | var(--digi--color--text--link);                        |
| `--digi--header-navigation-item--color-hover`  | var(--digi--color--text--link);                        |
| `--digi--header-navigation-item--font-family`  | var(--digi--global--typography--font-family--default); |
| `--digi--header-navigation-item--font-size`    | var(--digi--typography--body--font-size--desktop);     |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
