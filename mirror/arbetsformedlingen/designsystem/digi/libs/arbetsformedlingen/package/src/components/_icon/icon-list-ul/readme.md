---
title: digi-icon-list-ul
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/package/src/components/_icon/icon-list-ul/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/package/src/components/_icon/icon-list-ul/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-list-ul/readme.md
tags:
- libs::arbetsformedlingen::package::src::components::_icon::icon-list-ul::readme.md
- arbetsformedlingen::package::src::components::_icon::icon-list-ul::readme.md
- package::src::components::_icon::icon-list-ul::readme.md
- src::components::_icon::icon-list-ul::readme.md
- components::_icon::icon-list-ul::readme.md
- _icon::icon-list-ul::readme.md
- icon-list-ul::readme.md
- readme.md
---
# digi-icon-list-ul



<!-- Auto Generated Below -->


## Properties

| Property              | Attribute                | Description                                                                   | Type      | Default     |
| --------------------- | ------------------------ | ----------------------------------------------------------------------------- | --------- | ----------- |
| `afDesc`              | `af-desc`                | Lägger till ett descelement i svg:n                                           | `string`  | `undefined` |
| `afSvgAriaHidden`     | `af-svg-aria-hidden`     | För att dölja ikonen för skärmläsare. Default är satt till true.              | `boolean` | `true`      |
| `afSvgAriaLabelledby` | `af-svg-aria-labelledby` | Referera till andra element på sidan för att definiera ett tillgängligt namn. | `string`  | `undefined` |
| `afTitle`             | `af-title`               | Lägger till ett titleelement i svg:n                                          | `string`  | `undefined` |


## CSS Custom Properties

| Name                   | Description                         |
| ---------------------- | ----------------------------------- |
| `--digi--icon--color`  | var(--digi--color--icons--primary); |
| `--digi--icon--height` | auto;                               |
| `--digi--icon--width`  | initial;                            |


## Dependencies

### Used by

 - [digi-tag-media](../../_tag/tag-media)

### Graph
```mermaid
graph TD;
  digi-tag-media --> digi-icon-list-ul
  style digi-icon-list-ul fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
