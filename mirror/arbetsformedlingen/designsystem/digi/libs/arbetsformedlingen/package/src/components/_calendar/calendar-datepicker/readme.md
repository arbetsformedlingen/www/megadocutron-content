---
title: digi-calendar-datepicker
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/package/src/components/_calendar/calendar-datepicker/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/package/src/components/_calendar/calendar-datepicker/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_calendar/calendar-datepicker/readme.md
tags:
- libs::arbetsformedlingen::package::src::components::_calendar::calendar-datepicker::readme.md
- arbetsformedlingen::package::src::components::_calendar::calendar-datepicker::readme.md
- package::src::components::_calendar::calendar-datepicker::readme.md
- src::components::_calendar::calendar-datepicker::readme.md
- components::_calendar::calendar-datepicker::readme.md
- _calendar::calendar-datepicker::readme.md
- calendar-datepicker::readme.md
- readme.md
---
# digi-calendar-datepicker

<!-- Auto Generated Below -->


## Properties

| Property                   | Attribute                     | Description                                                                                     | Type                                                                                                                                                         | Default                                         |
| -------------------------- | ----------------------------- | ----------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------- |
| `afCloseOnSelect`          | `af-close-on-select`          | Sätt denna till true om kalendern ska stängas när användaren väljer ett datum. Förvalt är false | `boolean`                                                                                                                                                    | `false`                                         |
| `afId`                     | `af-id`                       | Sätter ett id attribut.                                                                         | `string`                                                                                                                                                     | `randomIdGenerator('digi-calendar-datepicker')` |
| `afInvalid`                | `af-invalid`                  | Sätter valideringsmeddelandet när du använder afInvalid                                         | `boolean`                                                                                                                                                    | `false`                                         |
| `afLabel`                  | `af-label`                    | Texten till labelelementet                                                                      | `string`                                                                                                                                                     | `'Skriv in eller välj datum (åååå-mm-dd)'`      |
| `afLabelDescription`       | `af-label-description`        | Valfri beskrivande text                                                                         | `string`                                                                                                                                                     | `'Exempel: 1992-06-26'`                         |
| `afMaxDate`                | --                            | Senaste valbara datumet                                                                         | `Date`                                                                                                                                                       | `undefined`                                     |
| `afMinDate`                | --                            | Tidigaste valbara datumet                                                                       | `Date`                                                                                                                                                       | `undefined`                                     |
| `afMultipleDates`          | `af-multiple-dates`           | Sätt denna till true för att kunna markera mer en ett datum i kalendern. Förvalt är false       | `boolean`                                                                                                                                                    | `false`                                         |
| `afRequired`               | `af-required`                 | Sätter attributet 'required'.                                                                   | `boolean`                                                                                                                                                    | `undefined`                                     |
| `afRequiredText`           | `af-required-text`            | Sätter text för afRequired.                                                                     | `string`                                                                                                                                                     | `undefined`                                     |
| `afSelectedDates`          | --                            | Valda datum i kalendern                                                                         | `Date[]`                                                                                                                                                     | `[]`                                            |
| `afShowWeekNumber`         | `af-show-week-number`         | Visa veckonummer i kalender. Förvalt är false                                                   | `boolean`                                                                                                                                                    | `false`                                         |
| `afValidation`             | `af-validation`               | Sätter valideringsstatus. Kan vara 'success', 'error', 'warning', 'neutral' eller ingenting.    | `CalendarDatepickerValidation.ERROR \| CalendarDatepickerValidation.NEUTRAL \| CalendarDatepickerValidation.SUCCESS \| CalendarDatepickerValidation.WARNING` | `CalendarDatepickerValidation.NEUTRAL`          |
| `afValidationDisabledDate` | `af-validation-disabled-date` | Sätter valideringsmeddelandet                                                                   | `string`                                                                                                                                                     | `'Ej valbart datum'`                            |
| `afValidationMessage`      | `af-validation-message`       | Sätter valideringsmeddelandet när du använder afInvalid                                         | `string`                                                                                                                                                     | `'Eget meddelande'`                             |
| `afValidationText`         | `af-validation-text`          | Sätter valideringsmeddelandet                                                                   | `string`                                                                                                                                                     | `undefined`                                     |
| `afValidationWrongFormat`  | `af-validation-wrong-format`  | Sätter valideringsmeddelandet                                                                   | `string`                                                                                                                                                     | `'Fel format'`                                  |
| `value`                    | --                            | Sätter attributet 'value'                                                                       | `Date[]`                                                                                                                                                     | `[]`                                            |


## Events

| Event            | Description                | Type               |
| ---------------- | -------------------------- | ------------------ |
| `afOnDateChange` | Sker vid datum uppdatering | `CustomEvent<any>` |


## CSS Custom Properties

| Name                                                 | Description                                                                                                     |
| ---------------------------------------------------- | --------------------------------------------------------------------------------------------------------------- |
| `--digi--datepicker--border-radius`                  | 0.25rem;                                                                                                        |
| `--digi--datepicker--calendar--max-width`            | 20.25rem;                                                                                                       |
| `--digi--datepicker--calendar--show-week--max-width` | 23.375rem;                                                                                                      |
| `--digi--datepicker--calendar-shadow`                | 0px 0px 5px rgba(0, 0, 0, 0.225);                                                                               |
| `--digi--datepicker--focus-outline`                  | solid var(--digi--border-width--secondary) var(--digi--color--border--secondary);                               |
| `--digi--datepicker--input--inactive-background`     | var(--digi--color--background--secondary)                                                                       |
| `--digi--datepicker--input--inline-end`              | calc(var(--digi--datepicker--input-icon--width) + var(--digi--datepicker--input-icon--horizontal-padding) * 2); |
| `--digi--datepicker--input--inline-start`            | var(--digi--gutter--medium);                                                                                    |
| `--digi--datepicker--input-icon--horizontal-padding` | 1.25rem;                                                                                                        |
| `--digi--datepicker--input-icon--vertical-padding`   | var(--digi--global--spacing--smallest-2);                                                                       |
| `--digi--datepicker--input-icon--width`              | 1.125rem;                                                                                                       |
| `--digi--datepicker--max-width`                      | 18.75rem;                                                                                                       |


## Dependencies

### Depends on

- [digi-form-input](../../../__core/_form/form-input)
- [digi-icon](../../../__core/_icon/icon)
- [digi-calendar](../../../__core/_calendar/calendar)

### Graph
```mermaid
graph TD;
  digi-calendar-datepicker --> digi-form-input
  digi-calendar-datepicker --> digi-icon
  digi-calendar-datepicker --> digi-calendar
  digi-form-input --> digi-form-label
  digi-form-input --> digi-form-validation-message
  digi-form-validation-message --> digi-icon
  digi-calendar --> digi-form-select
  digi-calendar --> digi-icon
  digi-calendar --> digi-util-keydown-handler
  digi-form-select --> digi-form-label
  digi-form-select --> digi-icon
  digi-form-select --> digi-util-mutation-observer
  digi-form-select --> digi-form-validation-message
  style digi-calendar-datepicker fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
