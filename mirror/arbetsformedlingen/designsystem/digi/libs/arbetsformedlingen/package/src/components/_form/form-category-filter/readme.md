---
title: digi-form-category-filter
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/package/src/components/_form/form-category-filter/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/package/src/components/_form/form-category-filter/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_form/form-category-filter/readme.md
tags:
- libs::arbetsformedlingen::package::src::components::_form::form-category-filter::readme.md
- arbetsformedlingen::package::src::components::_form::form-category-filter::readme.md
- package::src::components::_form::form-category-filter::readme.md
- src::components::_form::form-category-filter::readme.md
- components::_form::form-category-filter::readme.md
- _form::form-category-filter::readme.md
- form-category-filter::readme.md
- readme.md
---
# digi-form-category-filter

<!-- Auto Generated Below -->


## Properties

| Property                  | Attribute                    | Description                                                                                                                                       | Type      | Default             |
| ------------------------- | ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | ------------------- |
| `afAllCategories`         | `af-all-categories`          | Ifall listan ska börja med en 'Alla kategorier' kategori                                                                                          | `boolean` | `true`              |
| `afAllCategoriesSelected` | `af-all-categories-selected` | Ifall 'Alla kategorier' kategorin ska börja vald                                                                                                  | `boolean` | `true`              |
| `afAllCategoriesText`     | `af-all-categories-text`     | Namnet på 'Alla kategorier' kategorin                                                                                                             | `string`  | `"Alla kategorier"` |
| `afCategories`            | --                           | En lista av kategorier                                                                                                                            | `any[]`   | `[]`                |
| `afMultiselect`           | `af-multiselect`             | Välj om man ska kunna filtrera på flera kategorier åt gången.                                                                                     | `boolean` | `true`              |
| `afStartCollapsed`        | `af-start-collapsed`         | Ifall listan ska var kollapsad från början                                                                                                        | `boolean` | `true`              |
| `afVisibleCollapsed`      | `af-visible-collapsed`       | Antal synliga kategorier när listan är kollapsad (ifall det finns färre valbara kategorier än detta värde så syns inte 'Visa fler/färre'-knappen) | `number`  | `5`                 |


## Events

| Event                        | Description                       | Type               |
| ---------------------------- | --------------------------------- | ------------------ |
| `afOnSelectedCategoryChange` | Sker vid ändrade valda kategorier | `CustomEvent<any>` |


## CSS Custom Properties

| Name                                | Description                  |
| ----------------------------------- | ---------------------------- |
| `--digi--form-category-filter--gap` | var(--digi--padding--small); |


## Dependencies

### Depends on

- [digi-button](../../../__core/_button/button)
- [digi-icon-plus](../../../__core/_icon/icon-plus)
- [digi-icon-minus](../../../__core/_icon/icon-minus)

### Graph
```mermaid
graph TD;
  digi-form-category-filter --> digi-button
  digi-form-category-filter --> digi-icon-plus
  digi-form-category-filter --> digi-icon-minus
  style digi-form-category-filter fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
