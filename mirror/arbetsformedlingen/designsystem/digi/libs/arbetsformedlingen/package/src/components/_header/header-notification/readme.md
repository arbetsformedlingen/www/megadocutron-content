---
title: header-notification
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/package/src/components/_header/header-notification/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/package/src/components/_header/header-notification/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_header/header-notification/readme.md
tags:
- libs::arbetsformedlingen::package::src::components::_header::header-notification::readme.md
- arbetsformedlingen::package::src::components::_header::header-notification::readme.md
- package::src::components::_header::header-notification::readme.md
- src::components::_header::header-notification::readme.md
- components::_header::header-notification::readme.md
- _header::header-notification::readme.md
- header-notification::readme.md
- readme.md
---
# header-notification



<!-- Auto Generated Below -->


## Properties

| Property               | Attribute                | Description                            | Type     | Default     |
| ---------------------- | ------------------------ | -------------------------------------- | -------- | ----------- |
| `afNotificationAmount` | `af-notification-amount` | Numret som ska skrivas ut som innehåll | `number` | `undefined` |


## CSS Custom Properties

| Name                                        | Description                                            |
| ------------------------------------------- | ------------------------------------------------------ |
| `--digi--header-notification--color`        | var(--digi--color--text--primary);                     |
| `--digi--header-notification--color-active` | var(--digi--color--text--link);                        |
| `--digi--header-notification--color-hover`  | var(--digi--color--text--link);                        |
| `--digi--header-notification--font-family`  | var(--digi--global--typography--font-family--default); |
| `--digi--header-notification--font-size`    | var(--digi--typography--body--font-size--desktop);     |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
