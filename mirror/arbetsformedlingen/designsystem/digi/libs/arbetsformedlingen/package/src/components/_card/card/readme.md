---
title: digi-card
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/package/src/components/_card/card/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/package/src/components/_card/card/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_card/card/readme.md
tags:
- libs::arbetsformedlingen::package::src::components::_card::card::readme.md
- arbetsformedlingen::package::src::components::_card::card::readme.md
- package::src::components::_card::card::readme.md
- src::components::_card::card::readme.md
- components::_card::card::readme.md
- _card::card::readme.md
- card::readme.md
- readme.md
---
# digi-card



<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                                                           | Type                                                | Default                 |
| ---------------- | ------------------ | --------------------------------------------------------------------- | --------------------------------------------------- | ----------------------- |
| `afBorder`       | `af-border`        | Sätter ramlinje runt kort. Standard är utan ramlinje.                 | `CardBorder.NONE \| CardBorder.PRIMARY`             | `CardBorder.NONE`       |
| `afBorderRadius` | `af-border-radius` | Sätter rundade hörn på kort. Standard är utan rundade hörn.           | `CardBorderRadius.NONE \| CardBorderRadius.PRIMARY` | `CardBorderRadius.NONE` |
| `afFooterBorder` | `af-footer-border` | Sätter ramlinje uppåt i kortets bottendel. Standard är utan ramlinje. | `CardFooterBorder.NONE \| CardFooterBorder.PRIMARY` | `CardFooterBorder.NONE` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | kan innehålla vad som helst |


## CSS Custom Properties

| Name                                        | Description                                                                     |
| ------------------------------------------- | ------------------------------------------------------------------------------- |
| `--digi--card--background-color--primary`   | var(--digi--color--background--primary);                                        |
| `--digi--card--background-color--secondary` | var(--digi--color--background--secondary);                                      |
| `--digi--card--border`                      | none;                                                                           |
| `--digi--card--border--primary`             | solid var(--digi--border-width--primary) var(--digi--color--border--neutral-2); |
| `--digi--card--border-radius`               | none;                                                                           |
| `--digi--card--border-radius--primary`      | var(--digi--border--radius);                                                    |
| `--digi--card--footer--border-top`          | solid var(--digi--border-width--primary) var(--digi--color--border--neutral-2); |
| `--digi--card--footer--padding`             | var(--digi--padding--medium) var(--digi--padding--large);                       |
| `--digi--card--padding`                     | var(--digi--padding--large);                                                    |


## Dependencies

### Depends on

- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-card --> digi-typography
  style digi-card fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
