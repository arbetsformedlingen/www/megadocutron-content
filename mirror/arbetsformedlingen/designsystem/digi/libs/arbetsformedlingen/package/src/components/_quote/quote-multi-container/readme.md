---
title: digi-quote-multi-container
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/package/src/components/_quote/quote-multi-container/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/package/src/components/_quote/quote-multi-container/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_quote/quote-multi-container/readme.md
tags:
- libs::arbetsformedlingen::package::src::components::_quote::quote-multi-container::readme.md
- arbetsformedlingen::package::src::components::_quote::quote-multi-container::readme.md
- package::src::components::_quote::quote-multi-container::readme.md
- src::components::_quote::quote-multi-container::readme.md
- components::_quote::quote-multi-container::readme.md
- _quote::quote-multi-container::readme.md
- quote-multi-container::readme.md
- readme.md
---
# digi-quote-multi-container

<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                 | Type                                                                                                                                                                                                                               | Default     |
| ---------------- | ------------------ | --------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `afHeading`      | `af-heading`       | Rubrikens text              | `string`                                                                                                                                                                                                                           | `undefined` |
| `afHeadingLevel` | `af-heading-level` | Sätt rubrikens vikt (h1-h6) | `QuoteMultiContainerHeadingLevel.H1 \| QuoteMultiContainerHeadingLevel.H2 \| QuoteMultiContainerHeadingLevel.H3 \| QuoteMultiContainerHeadingLevel.H4 \| QuoteMultiContainerHeadingLevel.H5 \| QuoteMultiContainerHeadingLevel.H6` | `undefined` |


## Slots

| Slot       | Description              |
| ---------- | ------------------------ |
| `"mySlot"` | Slot description, if any |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
