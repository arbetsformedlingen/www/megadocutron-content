---
title: digi-badge-notification
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/package/src/components/_badge/badge-notification/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/package/src/components/_badge/badge-notification/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_badge/badge-notification/readme.md
tags:
- libs::arbetsformedlingen::package::src::components::_badge::badge-notification::readme.md
- arbetsformedlingen::package::src::components::_badge::badge-notification::readme.md
- package::src::components::_badge::badge-notification::readme.md
- src::components::_badge::badge-notification::readme.md
- components::_badge::badge-notification::readme.md
- _badge::badge-notification::readme.md
- badge-notification::readme.md
- readme.md
---
# digi-badge-notification

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute       | Description                                | Type               | Default     |
| ------------- | --------------- | ------------------------------------------ | ------------------ | ----------- |
| `afAriaLabel` | `af-aria-label` | Sätt attributet 'aria-label'.              | `string`           | `undefined` |
| `afValue`     | `af-value`      | Värdet som ska skrivas ut i notifieringen. | `number \| string` | `''`        |


## CSS Custom Properties

| Name                                           | Description                                            |
| ---------------------------------------------- | ------------------------------------------------------ |
| `--digi--badge-notification--background-color` | var(--digi--color--background--danger-4);              |
| `--digi--badge-notification--color--border`    | var(--digi--color--border--inverted);                  |
| `--digi--badge-notification--color--text`      | var(--digi--color--text--inverted);                    |
| `--digi--badge-notification--font-weight`      | var(--digi--typography--button--font-weight--desktop); |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
