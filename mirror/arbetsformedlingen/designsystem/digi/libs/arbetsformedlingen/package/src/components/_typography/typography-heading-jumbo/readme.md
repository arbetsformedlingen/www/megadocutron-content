---
title: digi-typography-heading-jumbo
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/package/src/components/_typography/typography-heading-jumbo/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/package/src/components/_typography/typography-heading-jumbo/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_typography/typography-heading-jumbo/readme.md
tags:
- libs::arbetsformedlingen::package::src::components::_typography::typography-heading-jumbo::readme.md
- arbetsformedlingen::package::src::components::_typography::typography-heading-jumbo::readme.md
- package::src::components::_typography::typography-heading-jumbo::readme.md
- src::components::_typography::typography-heading-jumbo::readme.md
- components::_typography::typography-heading-jumbo::readme.md
- _typography::typography-heading-jumbo::readme.md
- typography-heading-jumbo::readme.md
- readme.md
---
# digi-typography-heading-jumbo

This is a huge heading with a blue line beneath.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { TypographyHeadingJumboLevel } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property  | Attribute  | Description                           | Type                                                                                                                                                                                                       | Default                          |
| --------- | ---------- | ------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------- |
| `afLevel` | `af-level` | Sätt rubrikens vikt. 'h1' är förvalt. | `TypographyHeadingJumboLevel.H1 \| TypographyHeadingJumboLevel.H2 \| TypographyHeadingJumboLevel.H3 \| TypographyHeadingJumboLevel.H4 \| TypographyHeadingJumboLevel.H5 \| TypographyHeadingJumboLevel.H6` | `TypographyHeadingJumboLevel.H1` |
| `afText`  | `af-text`  | Rubrikens text                        | `string`                                                                                                                                                                                                   | `''`                             |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
