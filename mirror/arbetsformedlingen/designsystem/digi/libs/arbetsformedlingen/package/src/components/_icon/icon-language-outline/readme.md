---
title: digi-icon-language-outline
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/package/src/components/_icon/icon-language-outline/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/package/src/components/_icon/icon-language-outline/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-language-outline/readme.md
tags:
- libs::arbetsformedlingen::package::src::components::_icon::icon-language-outline::readme.md
- arbetsformedlingen::package::src::components::_icon::icon-language-outline::readme.md
- package::src::components::_icon::icon-language-outline::readme.md
- src::components::_icon::icon-language-outline::readme.md
- components::_icon::icon-language-outline::readme.md
- _icon::icon-language-outline::readme.md
- icon-language-outline::readme.md
- readme.md
---
# digi-icon-language-outline



<!-- Auto Generated Below -->


## Properties

| Property              | Attribute                | Description                                                                   | Type      | Default     |
| --------------------- | ------------------------ | ----------------------------------------------------------------------------- | --------- | ----------- |
| `afDesc`              | `af-desc`                | Lägger till ett descelement i svg:n                                           | `string`  | `undefined` |
| `afSvgAriaHidden`     | `af-svg-aria-hidden`     | För att dölja ikonen för skärmläsare. Default är satt till true.              | `boolean` | `true`      |
| `afSvgAriaLabelledby` | `af-svg-aria-labelledby` | Referera till andra element på sidan för att definiera ett tillgängligt namn. | `string`  | `undefined` |
| `afTitle`             | `af-title`               | Lägger till ett titleelement i svg:n                                          | `string`  | `undefined` |


## CSS Custom Properties

| Name                   | Description                         |
| ---------------------- | ----------------------------------- |
| `--digi--icon--color`  | var(--digi--color--icons--primary); |
| `--digi--icon--height` | auto;                               |
| `--digi--icon--width`  | initial;                            |


## Dependencies

### Used by

 - [digi-tools-languagepicker](../../_tools/tools-languagepicker)

### Graph
```mermaid
graph TD;
  digi-tools-languagepicker --> digi-icon-language-outline
  style digi-icon-language-outline fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
