---
title: digi-header
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/package/src/components/_header/header/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/package/src/components/_header/header/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_header/header/readme.md
tags:
- libs::arbetsformedlingen::package::src::components::_header::header::readme.md
- arbetsformedlingen::package::src::components::_header::header::readme.md
- package::src::components::_header::header::readme.md
- src::components::_header::header::readme.md
- components::_header::header::readme.md
- _header::header::readme.md
- header::readme.md
- readme.md
---
# digi-header



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute             | Description                             | Type      | Default     |
| ------------------ | --------------------- | --------------------------------------- | --------- | ----------- |
| `afAriaLabel`      | `af-aria-label`       | Sätter attributet 'aria-label'          | `string`  | `undefined` |
| `afHideSystemName` | `af-hide-system-name` | Döljer systemnamnet                     | `boolean` | `undefined` |
| `afMenuButtonText` | `af-menu-button-text` | Sätter text för menyknappen i mobilläge | `string`  | `undefined` |
| `afSystemName`     | `af-system-name`      | Sätter systemnamnet                     | `string`  | `undefined` |


## CSS Custom Properties

| Name                            | Description                                                |
| ------------------------------- | ---------------------------------------------------------- |
| `--digi--header--border-color`  | var(--digi--color--border--neutral-2);                     |
| `--digi--header--border-width`  | var(--digi--border-width--primary);                        |
| `--digi--header--font-size`     | var(--digi--typography--body--font-size--desktop);         |
| `--digi--header--font-weight`   | var(--digi--typography--title-logo--font-weight--desktop); |
| `--digi--header--padding`       | var(--digi--padding--large);                               |
| `--digi--header--padding-right` | var(--digi--padding--medium);                              |


## Dependencies

### Depends on

- [digi-util-breakpoint-observer](../../../__core/_util/util-breakpoint-observer)
- [digi-layout-block](../../../__core/_layout/layout-block)
- [digi-logo](../../_logo/logo)
- [digi-navigation-sidebar-button](../../../__core/_navigation/navigation-sidebar-button)

### Graph
```mermaid
graph TD;
  digi-header --> digi-util-breakpoint-observer
  digi-header --> digi-layout-block
  digi-header --> digi-logo
  digi-header --> digi-navigation-sidebar-button
  digi-layout-block --> digi-layout-container
  digi-navigation-sidebar-button --> digi-button
  digi-navigation-sidebar-button --> digi-icon
  style digi-header fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
