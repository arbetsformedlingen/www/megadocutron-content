---
title: my-component
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/package/src/components/_header/header-avatar/avatars/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/package/src/components/_header/header-avatar/avatars/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_header/header-avatar/avatars/readme.md
tags:
- libs::arbetsformedlingen::package::src::components::_header::header-avatar::avatars::readme.md
- arbetsformedlingen::package::src::components::_header::header-avatar::avatars::readme.md
- package::src::components::_header::header-avatar::avatars::readme.md
- src::components::_header::header-avatar::avatars::readme.md
- components::_header::header-avatar::avatars::readme.md
- _header::header-avatar::avatars::readme.md
- header-avatar::avatars::readme.md
- avatars::readme.md
- readme.md
---
# my-component



<!-- Auto Generated Below -->


## Properties

| Property          | Attribute            | Description | Type      | Default     |
| ----------------- | -------------------- | ----------- | --------- | ----------- |
| `afSvgAriaHidden` | `af-svg-aria-hidden` |             | `boolean` | `undefined` |
| `afSvgName`       | `af-svg-name`        |             | `string`  | `undefined` |
| `afTitle`         | `af-title`           |             | `string`  | `undefined` |


## Dependencies

### Used by

 - [digi-header-avatar](..)

### Graph
```mermaid
graph TD;
  digi-header-avatar --> digi-avatar-illustration
  style digi-avatar-illustration fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
