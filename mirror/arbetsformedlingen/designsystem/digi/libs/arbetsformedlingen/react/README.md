---
title: arbetsformedlingen-react
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/arbetsformedlingen/react/README.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/arbetsformedlingen/react/README.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/react/README.md
tags:
- libs::arbetsformedlingen::react::README.md
- arbetsformedlingen::react::README.md
- react::README.md
- README.md
---
# arbetsformedlingen-react

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test arbetsformedlingen-react` to execute the unit tests via [Jest](https://jestjs.io).
