---
title: skolverket-text
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/text/README.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/text/README.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/text/README.md
tags:
- libs::skolverket::text::README.md
- skolverket::text::README.md
- text::README.md
- README.md
---
# skolverket-text

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build skolverket-text` to build the library.

## Running unit tests

Run `nx test skolverket-text` to execute the unit tests via [Jest](https://jestjs.io).
