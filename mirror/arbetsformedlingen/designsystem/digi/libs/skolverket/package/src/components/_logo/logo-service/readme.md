---
title: digi-logo-service
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_logo/logo-service/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_logo/logo-service/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_logo/logo-service/readme.md
tags:
- libs::skolverket::package::src::components::_logo::logo-service::readme.md
- skolverket::package::src::components::_logo::logo-service::readme.md
- package::src::components::_logo::logo-service::readme.md
- src::components::_logo::logo-service::readme.md
- components::_logo::logo-service::readme.md
- _logo::logo-service::readme.md
- logo-service::readme.md
- readme.md
---
# digi-logo-service

<!-- Auto Generated Below -->


## Properties

| Property                     | Attribute        | Description                                                                                  | Type     | Default                                         |
| ---------------------------- | ---------------- | -------------------------------------------------------------------------------------------- | -------- | ----------------------------------------------- |
| `afDescription` _(required)_ | `af-description` | Tjänsten eller verktygets beskrivning                                                        | `string` | `undefined`                                     |
| `afName` _(required)_        | `af-name`        | Tjänsten eller verktygets namn                                                               | `string` | `undefined`                                     |
| `afNameId`                   | `af-name-id`     | Sätter attributet 'id' på namntextens element. Om inget väljs så skapas ett slumpmässigt id. | `string` | `randomIdGenerator('digi-logo-service__title')` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
