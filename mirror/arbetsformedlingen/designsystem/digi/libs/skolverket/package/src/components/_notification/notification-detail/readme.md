---
title: digi-notification-detail
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_notification/notification-detail/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_notification/notification-detail/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_notification/notification-detail/readme.md
tags:
- libs::skolverket::package::src::components::_notification::notification-detail::readme.md
- skolverket::package::src::components::_notification::notification-detail::readme.md
- package::src::components::_notification::notification-detail::readme.md
- src::components::_notification::notification-detail::readme.md
- components::_notification::notification-detail::readme.md
- _notification::notification-detail::readme.md
- notification-detail::readme.md
- readme.md
---
# digi-notification-detail

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description     | Type                              | Default                            |
| ------------- | -------------- | --------------- | --------------------------------- | ---------------------------------- |
| `afVariation` | `af-variation` | Sätter variant. | `"danger" \| "info" \| "warning"` | `NotificationDetailVariation.INFO` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |
| `"heading"` | Ska innehålla en rubrik     |


## CSS Custom Properties

| Name                                                 | Description                              |
| ---------------------------------------------------- | ---------------------------------------- |
| `--digi--notification-detail--border-color--danger`  | var(--digi--color--border--danger);      |
| `--digi--notification-detail--border-color--info`    | var(--digi--color--border--informative); |
| `--digi--notification-detail--border-color--warning` | var(--digi--color--border--warning);     |


## Dependencies

### Depends on

- [digi-icon](../../../__core/_icon/icon)
- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-notification-detail --> digi-icon
  digi-notification-detail --> digi-typography
  style digi-notification-detail fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
