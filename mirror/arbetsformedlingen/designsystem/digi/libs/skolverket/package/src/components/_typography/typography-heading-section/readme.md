---
title: digi-typography-heading-section
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_typography/typography-heading-section/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_typography/typography-heading-section/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_typography/typography-heading-section/readme.md
tags:
- libs::skolverket::package::src::components::_typography::typography-heading-section::readme.md
- skolverket::package::src::components::_typography::typography-heading-section::readme.md
- package::src::components::_typography::typography-heading-section::readme.md
- src::components::_typography::typography-heading-section::readme.md
- components::_typography::typography-heading-section::readme.md
- _typography::typography-heading-section::readme.md
- typography-heading-section::readme.md
- readme.md
---
# digi-typography-heading-section

<!-- Auto Generated Below -->


## Slots

| Slot        | Description                     |
| ----------- | ------------------------------- |
| `"default"` | ska innehålla ett rubrikelement |


## Dependencies

### Used by

 - [digi-page-block-cards](../../_page/page-block-cards)
 - [digi-page-block-lists](../../_page/page-block-lists)

### Graph
```mermaid
graph TD;
  digi-page-block-cards --> digi-typography-heading-section
  digi-page-block-lists --> digi-typography-heading-section
  style digi-typography-heading-section fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
