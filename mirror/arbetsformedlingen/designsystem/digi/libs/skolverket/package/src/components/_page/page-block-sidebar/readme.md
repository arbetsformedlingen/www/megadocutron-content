---
title: digi-page-block-sidebar
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_page/page-block-sidebar/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_page/page-block-sidebar/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_page/page-block-sidebar/readme.md
tags:
- libs::skolverket::package::src::components::_page::page-block-sidebar::readme.md
- skolverket::package::src::components::_page::page-block-sidebar::readme.md
- package::src::components::_page::page-block-sidebar::readme.md
- src::components::_page::page-block-sidebar::readme.md
- components::_page::page-block-sidebar::readme.md
- _page::page-block-sidebar::readme.md
- page-block-sidebar::readme.md
- readme.md
---
# digi-page-block-sidebar

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type                                                      | Default     |
| ------------- | -------------- | ----------- | --------------------------------------------------------- | ----------- |
| `afVariation` | `af-variation` |             | `"article" \| "process" \| "section" \| "start" \| "sub"` | `undefined` |


## Slots

| Slot        | Description |
| ----------- | ----------- |
| `"default"` |             |
| `"sidebar"` |             |


## Dependencies

### Depends on

- [digi-layout-grid](../../_layout/layout-grid)

### Graph
```mermaid
graph TD;
  digi-page-block-sidebar --> digi-layout-grid
  digi-layout-grid --> digi-layout-container
  style digi-page-block-sidebar fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
