---
title: digi-page-block-cards
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_page/page-block-cards/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_page/page-block-cards/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_page/page-block-cards/readme.md
tags:
- libs::skolverket::package::src::components::_page::page-block-cards::readme.md
- skolverket::package::src::components::_page::page-block-cards::readme.md
- package::src::components::_page::page-block-cards::readme.md
- src::components::_page::page-block-cards::readme.md
- components::_page::page-block-cards::readme.md
- _page::page-block-cards::readme.md
- page-block-cards::readme.md
- readme.md
---
# digi-page-block-cards

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type                                                      | Default     |
| ------------- | -------------- | ----------- | --------------------------------------------------------- | ----------- |
| `afVariation` | `af-variation` |             | `"article" \| "process" \| "section" \| "start" \| "sub"` | `undefined` |


## Slots

| Slot        | Description |
| ----------- | ----------- |
| `"default"` |             |
| `"heading"` | Rubrik      |


## Dependencies

### Depends on

- [digi-layout-container](../../../__core/_layout/layout-container)
- [digi-card-box](../../_card/card-box)
- [digi-typography-heading-section](../../_typography/typography-heading-section)
- [digi-layout-stacked-blocks](../../_layout/layout-stacked-blocks)

### Graph
```mermaid
graph TD;
  digi-page-block-cards --> digi-layout-container
  digi-page-block-cards --> digi-card-box
  digi-page-block-cards --> digi-typography-heading-section
  digi-page-block-cards --> digi-layout-stacked-blocks
  style digi-page-block-cards fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
