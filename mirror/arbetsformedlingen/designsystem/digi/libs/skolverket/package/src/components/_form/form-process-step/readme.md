---
title: digi-form-process-step
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_form/form-process-step/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_form/form-process-step/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_form/form-process-step/readme.md
tags:
- libs::skolverket::package::src::components::_form::form-process-step::readme.md
- skolverket::package::src::components::_form::form-process-step::readme.md
- package::src::components::_form::form-process-step::readme.md
- src::components::_form::form-process-step::readme.md
- components::_form::form-process-step::readme.md
- _form::form-process-step::readme.md
- form-process-step::readme.md
- readme.md
---
# digi-form-process-step

<!-- Auto Generated Below -->


## Properties

| Property               | Attribute    | Description | Type                                     | Default      |
| ---------------------- | ------------ | ----------- | ---------------------------------------- | ------------ |
| `afContext`            | `af-context` |             | `"fallback" \| "regular"`                | `'regular'`  |
| `afHref`               | `af-href`    |             | `string`                                 | `undefined`  |
| `afLabel` _(required)_ | `af-label`   |             | `string`                                 | `undefined`  |
| `afType`               | `af-type`    |             | `"completed" \| "current" \| "upcoming"` | `'upcoming'` |


## Events

| Event     | Description | Type                      |
| --------- | ----------- | ------------------------- |
| `afClick` |             | `CustomEvent<MouseEvent>` |


## Slots

| Slot        | Description                             |
| ----------- | --------------------------------------- |
| `"default"` | Ska innehålla flera <form-process-step> |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
