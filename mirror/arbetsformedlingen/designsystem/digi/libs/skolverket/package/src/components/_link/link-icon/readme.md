---
title: digi-link-icon
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_link/link-icon/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_link/link-icon/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_link/link-icon/readme.md
tags:
- libs::skolverket::package::src::components::_link::link-icon::readme.md
- skolverket::package::src::components::_link::link-icon::readme.md
- package::src::components::_link::link-icon::readme.md
- src::components::_link::link-icon::readme.md
- components::_link::link-icon::readme.md
- _link::link-icon::readme.md
- link-icon::readme.md
- readme.md
---
# digi-link-icon

<!-- Auto Generated Below -->


## Slots

| Slot        | Description                                 |
| ----------- | ------------------------------------------- |
| `"default"` | Ska vara en länk med en ikon och text inuti |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
