---
title: digi-navigation-main-menu
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_navigation/navigation-main-menu/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_navigation/navigation-main-menu/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_navigation/navigation-main-menu/readme.md
tags:
- libs::skolverket::package::src::components::_navigation::navigation-main-menu::readme.md
- skolverket::package::src::components::_navigation::navigation-main-menu::readme.md
- package::src::components::_navigation::navigation-main-menu::readme.md
- src::components::_navigation::navigation-main-menu::readme.md
- components::_navigation::navigation-main-menu::readme.md
- _navigation::navigation-main-menu::readme.md
- navigation-main-menu::readme.md
- readme.md
---
# digi-navigation-main-menu

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                                           | Type     | Default                                          |
| -------- | --------- | --------------------------------------------------------------------- | -------- | ------------------------------------------------ |
| `afId`   | `af-id`   | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id. | `string` | `randomIdGenerator('digi-navigation-main-menu')` |


## Events

| Event       | Description | Type               |
| ----------- | ----------- | ------------------ |
| `afOnRoute` |             | `CustomEvent<any>` |


## Slots

| Slot        | Description                      |
| ----------- | -------------------------------- |
| `"default"` | Se översikten för exakt innehåll |


## Dependencies

### Used by

 - [digi-page-header](../../_page/page-header)

### Depends on

- [digi-layout-container](../../../__core/_layout/layout-container)

### Graph
```mermaid
graph TD;
  digi-navigation-main-menu --> digi-layout-container
  digi-page-header --> digi-navigation-main-menu
  style digi-navigation-main-menu fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
