---
title: digi-form-process-steps
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_form/form-process-steps/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_form/form-process-steps/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_form/form-process-steps/readme.md
tags:
- libs::skolverket::package::src::components::_form::form-process-steps::readme.md
- skolverket::package::src::components::_form::form-process-steps::readme.md
- package::src::components::_form::form-process-steps::readme.md
- src::components::_form::form-process-steps::readme.md
- components::_form::form-process-steps::readme.md
- _form::form-process-steps::readme.md
- form-process-steps::readme.md
- readme.md
---
# digi-form-process-steps

<!-- Auto Generated Below -->


## Properties

| Property                     | Attribute         | Description                                                           | Type     | Default                                        |
| ---------------------------- | ----------------- | --------------------------------------------------------------------- | -------- | ---------------------------------------------- |
| `afCurrentStep` _(required)_ | `af-current-step` |                                                                       | `number` | `undefined`                                    |
| `afId`                       | `af-id`           | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id. | `string` | `randomIdGenerator('digi-form-process-steps')` |


## Slots

| Slot        | Description                                                          |
| ----------- | -------------------------------------------------------------------- |
| `"default"` | Ska innehålla flera <li><form-process-step></form-process-step></li> |


## Dependencies

### Depends on

- [digi-util-resize-observer](../../../__core/_util/util-resize-observer)
- [digi-icon](../../../__core/_icon/icon)
- [digi-button](../../../__core/_button/button)

### Graph
```mermaid
graph TD;
  digi-form-process-steps --> digi-util-resize-observer
  digi-form-process-steps --> digi-icon
  digi-form-process-steps --> digi-button
  style digi-form-process-steps fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
