---
title: digi-logo-sister
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_logo/logo-sister/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_logo/logo-sister/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_logo/logo-sister/readme.md
tags:
- libs::skolverket::package::src::components::_logo::logo-sister::readme.md
- skolverket::package::src::components::_logo::logo-sister::readme.md
- package::src::components::_logo::logo-sister::readme.md
- src::components::_logo::logo-sister::readme.md
- components::_logo::logo-sister::readme.md
- _logo::logo-sister::readme.md
- logo-sister::readme.md
- readme.md
---
# digi-logo-sister

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute    | Description                                                                                  | Type     | Default                                        |
| --------------------- | ------------ | -------------------------------------------------------------------------------------------- | -------- | ---------------------------------------------- |
| `afName` _(required)_ | `af-name`    | Syskonwebbplatsens namn                                                                      | `string` | `undefined`                                    |
| `afNameId`            | `af-name-id` | Sätter attributet 'id' på namntextens element. Om inget väljs så skapas ett slumpmässigt id. | `string` | `randomIdGenerator('digi-logo-sister__title')` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
