---
title: digi-page-block-hero
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_page/page-block-hero/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_page/page-block-hero/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_page/page-block-hero/readme.md
tags:
- libs::skolverket::package::src::components::_page::page-block-hero::readme.md
- skolverket::package::src::components::_page::page-block-hero::readme.md
- package::src::components::_page::page-block-hero::readme.md
- src::components::_page::page-block-hero::readme.md
- components::_page::page-block-hero::readme.md
- _page::page-block-hero::readme.md
- page-block-hero::readme.md
- readme.md
---
# digi-page-block-hero

<!-- Auto Generated Below -->


## Properties

| Property            | Attribute             | Description | Type                                         | Default     |
| ------------------- | --------------------- | ----------- | -------------------------------------------- | ----------- |
| `afBackground`      | `af-background`       |             | `"auto" \| "transparent"`                    | `undefined` |
| `afBackgroundImage` | `af-background-image` |             | `string`                                     | `undefined` |
| `afVariation`       | `af-variation`        |             | `"process" \| "section" \| "start" \| "sub"` | `undefined` |


## Slots

| Slot        | Description |
| ----------- | ----------- |
| `"default"` |             |
| `"heading"` | Rubrik      |


## Dependencies

### Depends on

- [digi-layout-container](../../../__core/_layout/layout-container)
- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-page-block-hero --> digi-layout-container
  digi-page-block-hero --> digi-typography
  style digi-page-block-hero fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
