---
title: digi-logo
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_logo/logo/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_logo/logo/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_logo/logo/readme.md
tags:
- libs::skolverket::package::src::components::_logo::logo::readme.md
- skolverket::package::src::components::_logo::logo::readme.md
- package::src::components::_logo::logo::readme.md
- src::components::_logo::logo::readme.md
- components::_logo::logo::readme.md
- _logo::logo::readme.md
- logo::readme.md
- readme.md
---
# digi-logo

<!-- Auto Generated Below -->


## Properties

| Property          | Attribute            | Description                                                                                                  | Type      | Default                                 |
| ----------------- | -------------------- | ------------------------------------------------------------------------------------------------------------ | --------- | --------------------------------------- |
| `afDesc`          | `af-desc`            | Lägger till ett descelement i svg:n                                                                          | `string`  | `undefined`                             |
| `afSvgAriaHidden` | `af-svg-aria-hidden` | För att dölja logotypen för skärmläsare.                                                                     | `boolean` | `undefined`                             |
| `afTitle`         | `af-title`           | Lägger till ett titleelement i svg:n. Standardtext är 'Skolverket'.                                          | `string`  | `'Skolverket'`                          |
| `afTitleId`       | `af-title-id`        | Sätter attributet 'id' på title-elementet inuti svg-elementet. Om inget väljs så skapas ett slumpmässigt id. | `string`  | `randomIdGenerator('digi-logo__title')` |


## Dependencies

### Used by

 - [digi-page-footer](../../_page/page-footer)

### Graph
```mermaid
graph TD;
  digi-page-footer --> digi-logo
  style digi-logo fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
