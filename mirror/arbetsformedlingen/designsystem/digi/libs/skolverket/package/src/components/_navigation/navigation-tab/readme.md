---
title: digi-navigation-tab
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_navigation/navigation-tab/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_navigation/navigation-tab/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_navigation/navigation-tab/readme.md
tags:
- libs::skolverket::package::src::components::_navigation::navigation-tab::readme.md
- skolverket::package::src::components::_navigation::navigation-tab::readme.md
- package::src::components::_navigation::navigation-tab::readme.md
- src::components::_navigation::navigation-tab::readme.md
- components::_navigation::navigation-tab::readme.md
- _navigation::navigation-tab::readme.md
- navigation-tab::readme.md
- readme.md
---
# digi-navigation-tab

<!-- Auto Generated Below -->


## Properties

| Property                   | Attribute       | Description                                                                             | Type      | Default                                    |
| -------------------------- | --------------- | --------------------------------------------------------------------------------------- | --------- | ------------------------------------------ |
| `afActive`                 | `af-active`     | Sätter aktiv tabb. Detta sköts av digi-navigation-tabs som ska omsluta denna komponent. | `boolean` | `undefined`                                |
| `afAriaLabel` _(required)_ | `af-aria-label` | Sätter attributet 'aria-label'                                                          | `string`  | `undefined`                                |
| `afId`                     | `af-id`         | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                   | `string`  | `randomIdGenerator('digi-navigation-tab')` |


## Events

| Event        | Description                                | Type                   |
| ------------ | ------------------------------------------ | ---------------------- |
| `afOnToggle` | När tabben växlar mellan aktiv och inaktiv | `CustomEvent<boolean>` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


## CSS Custom Properties

| Name                                        | Description                                      |
| ------------------------------------------- | ------------------------------------------------ |
| `--digi--navigation-tab--box-shadow--focus` | solid 2px var(--digi--color--border--secondary); |


## Dependencies

### Used by

 - [digi-navigation-tab-in-a-box](../navigation-tab-in-a-box)

### Graph
```mermaid
graph TD;
  digi-navigation-tab-in-a-box --> digi-navigation-tab
  style digi-navigation-tab fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
