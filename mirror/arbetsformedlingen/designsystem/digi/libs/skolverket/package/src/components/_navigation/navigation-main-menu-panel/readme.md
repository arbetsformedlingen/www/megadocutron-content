---
title: digi-navigation-main-menu-panel
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_navigation/navigation-main-menu-panel/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_navigation/navigation-main-menu-panel/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_navigation/navigation-main-menu-panel/readme.md
tags:
- libs::skolverket::package::src::components::_navigation::navigation-main-menu-panel::readme.md
- skolverket::package::src::components::_navigation::navigation-main-menu-panel::readme.md
- package::src::components::_navigation::navigation-main-menu-panel::readme.md
- src::components::_navigation::navigation-main-menu-panel::readme.md
- components::_navigation::navigation-main-menu-panel::readme.md
- _navigation::navigation-main-menu-panel::readme.md
- navigation-main-menu-panel::readme.md
- readme.md
---
# digi-navigation-main-menu-panel

<!-- Auto Generated Below -->


## Events

| Event        | Description                    | Type                               |
| ------------ | ------------------------------ | ---------------------------------- |
| `afOnClose`  | När komponenten stängs         | `CustomEvent<any>`                 |
| `afOnResize` | När komponenten ändrar storlek | `CustomEvent<ResizeObserverEntry>` |


## Slots

| Slot          | Description                      |
| ------------- | -------------------------------- |
| `"default"`   | Se översikten för exakt innehåll |
| `"main-link"` | Länk ovanför navigationsraderna  |


## Shadow Parts

| Part          | Description |
| ------------- | ----------- |
| `"main-link"` |             |


## Dependencies

### Depends on

- [digi-util-resize-observer](../../../__core/_util/util-resize-observer)
- [digi-layout-container](../../../__core/_layout/layout-container)
- [digi-util-mutation-observer](../../../__core/_util/util-mutation-observer)
- [digi-button](../../../__core/_button/button)
- [digi-icon](../../../__core/_icon/icon)

### Graph
```mermaid
graph TD;
  digi-navigation-main-menu-panel --> digi-util-resize-observer
  digi-navigation-main-menu-panel --> digi-layout-container
  digi-navigation-main-menu-panel --> digi-util-mutation-observer
  digi-navigation-main-menu-panel --> digi-button
  digi-navigation-main-menu-panel --> digi-icon
  style digi-navigation-main-menu-panel fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
