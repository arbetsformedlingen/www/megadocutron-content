---
title: digi-page
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_page/page/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_page/page/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_page/page/readme.md
tags:
- libs::skolverket::package::src::components::_page::page::readme.md
- skolverket::package::src::components::_page::page::readme.md
- package::src::components::_page::page::readme.md
- src::components::_page::page::readme.md
- components::_page::page::readme.md
- _page::page::readme.md
- page::readme.md
- readme.md
---
# digi-page

<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description | Type                                                                                                                            | Default     |
| -------------- | --------------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `afBackground` | `af-background` |             | `"dotted_1" \| "dotted_2" \| "dotted_3" \| "square_1" \| "square_2" \| "square_3" \| "striped_1" \| "striped_2" \| "striped_3"` | `undefined` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | kan innehålla vad som helst |
| `"footer"`  | Sidfot                      |
| `"header"`  | Sidhuvud                    |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
