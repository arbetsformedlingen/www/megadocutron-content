---
title: digi-layout-stacked-blocks
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_layout/layout-stacked-blocks/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_layout/layout-stacked-blocks/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_layout/layout-stacked-blocks/readme.md
tags:
- libs::skolverket::package::src::components::_layout::layout-stacked-blocks::readme.md
- skolverket::package::src::components::_layout::layout-stacked-blocks::readme.md
- package::src::components::_layout::layout-stacked-blocks::readme.md
- src::components::_layout::layout-stacked-blocks::readme.md
- components::_layout::layout-stacked-blocks::readme.md
- _layout::layout-stacked-blocks::readme.md
- layout-stacked-blocks::readme.md
- readme.md
---
# digi-layout-stacked-blocks

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type                      | Default                                |
| ------------- | -------------- | ----------- | ------------------------- | -------------------------------------- |
| `afVariation` | `af-variation` |             | `"enhanced" \| "regular"` | `LayoutStackedBlocksVariation.REGULAR` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | kan innehålla vad som helst |


## Dependencies

### Used by

 - [digi-page-block-cards](../../_page/page-block-cards)
 - [digi-page-block-lists](../../_page/page-block-lists)

### Graph
```mermaid
graph TD;
  digi-page-block-cards --> digi-layout-stacked-blocks
  digi-page-block-lists --> digi-layout-stacked-blocks
  style digi-layout-stacked-blocks fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
