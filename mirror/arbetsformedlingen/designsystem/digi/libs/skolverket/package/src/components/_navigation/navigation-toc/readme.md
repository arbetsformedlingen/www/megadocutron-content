---
title: digi-navigation-toc
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_navigation/navigation-toc/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_navigation/navigation-toc/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_navigation/navigation-toc/readme.md
tags:
- libs::skolverket::package::src::components::_navigation::navigation-toc::readme.md
- skolverket::package::src::components::_navigation::navigation-toc::readme.md
- package::src::components::_navigation::navigation-toc::readme.md
- src::components::_navigation::navigation-toc::readme.md
- components::_navigation::navigation-toc::readme.md
- _navigation::navigation-toc::readme.md
- navigation-toc::readme.md
- readme.md
---
# digi-navigation-toc

<!-- Auto Generated Below -->


## Slots

| Slot        | Description                                  |
| ----------- | -------------------------------------------- |
| `"default"` | Ska inehålla li-element med a-element inuti. |
| `"heading"` | Rubrik                                       |


## Dependencies

### Depends on

- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-navigation-toc --> digi-typography
  style digi-navigation-toc fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
