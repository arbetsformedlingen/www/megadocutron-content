---
title: digi-page-block-lists
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_page/page-block-lists/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_page/page-block-lists/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_page/page-block-lists/readme.md
tags:
- libs::skolverket::package::src::components::_page::page-block-lists::readme.md
- skolverket::package::src::components::_page::page-block-lists::readme.md
- package::src::components::_page::page-block-lists::readme.md
- src::components::_page::page-block-lists::readme.md
- components::_page::page-block-lists::readme.md
- _page::page-block-lists::readme.md
- page-block-lists::readme.md
- readme.md
---
# digi-page-block-lists

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type                                                      | Default     |
| ------------- | -------------- | ----------- | --------------------------------------------------------- | ----------- |
| `afVariation` | `af-variation` |             | `"article" \| "process" \| "section" \| "start" \| "sub"` | `undefined` |


## Slots

| Slot        | Description |
| ----------- | ----------- |
| `"default"` |             |
| `"heading"` | Rubrik      |


## Dependencies

### Depends on

- [digi-layout-container](../../../__core/_layout/layout-container)
- [digi-card-box](../../_card/card-box)
- [digi-typography-heading-section](../../_typography/typography-heading-section)
- [digi-layout-stacked-blocks](../../_layout/layout-stacked-blocks)

### Graph
```mermaid
graph TD;
  digi-page-block-lists --> digi-layout-container
  digi-page-block-lists --> digi-card-box
  digi-page-block-lists --> digi-typography-heading-section
  digi-page-block-lists --> digi-layout-stacked-blocks
  style digi-page-block-lists fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
