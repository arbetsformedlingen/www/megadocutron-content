---
title: digi-layout-grid
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_layout/layout-grid/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_layout/layout-grid/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_layout/layout-grid/readme.md
tags:
- libs::skolverket::package::src::components::_layout::layout-grid::readme.md
- skolverket::package::src::components::_layout::layout-grid::readme.md
- package::src::components::_layout::layout-grid::readme.md
- src::components::_layout::layout-grid::readme.md
- components::_layout::layout-grid::readme.md
- _layout::layout-grid::readme.md
- layout-grid::readme.md
- readme.md
---
# digi-layout-grid

<!-- Auto Generated Below -->


## Properties

| Property            | Attribute             | Description | Type                  | Default                             |
| ------------------- | --------------------- | ----------- | --------------------- | ----------------------------------- |
| `afVerticalSpacing` | `af-vertical-spacing` |             | `"none" \| "regular"` | `LayoutGridVerticalSpacing.REGULAR` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | kan innehålla vad som helst |


## Dependencies

### Used by

 - [digi-page-block-sidebar](../../_page/page-block-sidebar)
 - [digi-page-footer](../../_page/page-footer)

### Depends on

- [digi-layout-container](../../../__core/_layout/layout-container)

### Graph
```mermaid
graph TD;
  digi-layout-grid --> digi-layout-container
  digi-page-block-sidebar --> digi-layout-grid
  digi-page-footer --> digi-layout-grid
  style digi-layout-grid fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
