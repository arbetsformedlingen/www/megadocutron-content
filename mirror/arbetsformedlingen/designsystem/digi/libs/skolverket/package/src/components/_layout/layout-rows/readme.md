---
title: digi-layout-rows
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_layout/layout-rows/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_layout/layout-rows/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_layout/layout-rows/readme.md
tags:
- libs::skolverket::package::src::components::_layout::layout-rows::readme.md
- skolverket::package::src::components::_layout::layout-rows::readme.md
- package::src::components::_layout::layout-rows::readme.md
- src::components::_layout::layout-rows::readme.md
- components::_layout::layout-rows::readme.md
- _layout::layout-rows::readme.md
- layout-rows::readme.md
- readme.md
---
# digi-layout-rows

<!-- Auto Generated Below -->


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | kan innehålla vad som helst |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
