---
title: digi-page-footer
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_page/page-footer/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_page/page-footer/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_page/page-footer/readme.md
tags:
- libs::skolverket::package::src::components::_page::page-footer::readme.md
- skolverket::package::src::components::_page::page-footer::readme.md
- package::src::components::_page::page-footer::readme.md
- src::components::_page::page-footer::readme.md
- components::_page::page-footer::readme.md
- _page::page-footer::readme.md
- page-footer::readme.md
- readme.md
---
# digi-page-footer

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type                                                           | Default                       |
| ------------- | -------------- | ----------- | -------------------------------------------------------------- | ----------------------------- |
| `afVariation` | `af-variation` |             | `PageFooterVariation.PRIMARY \| PageFooterVariation.SECONDARY` | `PageFooterVariation.PRIMARY` |


## Slots

| Slot          | Description                 |
| ------------- | --------------------------- |
| `"bottom"`    |                             |
| `"default"`   | kan innehålla vad som helst |
| `"top"`       |                             |
| `"top-first"` |                             |


## Dependencies

### Depends on

- [digi-typography](../../../__core/_typography/typography)
- [digi-layout-grid](../../_layout/layout-grid)
- [digi-logo](../../_logo/logo)

### Graph
```mermaid
graph TD;
  digi-page-footer --> digi-typography
  digi-page-footer --> digi-layout-grid
  digi-page-footer --> digi-logo
  digi-layout-grid --> digi-layout-container
  style digi-page-footer fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
