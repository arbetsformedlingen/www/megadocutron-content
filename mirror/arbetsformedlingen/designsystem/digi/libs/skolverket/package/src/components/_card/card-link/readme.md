---
title: digi-card-link
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/package/src/components/_card/card-link/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/package/src/components/_card/card-link/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_card/card-link/readme.md
tags:
- libs::skolverket::package::src::components::_card::card-link::readme.md
- skolverket::package::src::components::_card::card-link::readme.md
- package::src::components::_card::card-link::readme.md
- src::components::_card::card-link::readme.md
- components::_card::card-link::readme.md
- _card::card-link::readme.md
- card-link::readme.md
- readme.md
---
# digi-card-link

<!-- Auto Generated Below -->


## Slots

| Slot             | Description                            |
| ---------------- | -------------------------------------- |
| `"default"`      | kan innehålla vad som helst            |
| `"image"`        | Bild                                   |
| `"link-heading"` | Ska innehålla en rubrik med länk inuti |


## Dependencies

### Depends on

- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-card-link --> digi-typography
  style digi-card-link fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
