---
title: skolverket-react
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/react/README.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/react/README.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/react/README.md
tags:
- libs::skolverket::react::README.md
- skolverket::react::README.md
- react::README.md
- README.md
---
# skolverket-react

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test skolverket-react` to execute the unit tests via [Jest](https://jestjs.io).
