---
title: Digi Core Angular
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/skolverket/angular/README.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/skolverket/angular/README.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/skolverket/angular/README.md
tags:
- libs::skolverket::angular::README.md
- skolverket::angular::README.md
- angular::README.md
- README.md
---
# Digi Core Angular

Digi Core Angular är ett bibliotek som wrappar Digi Core. Genom att exportera proxy-filer och value accessors från Digi Core via output-target så kan detta bibliotek exponera en modul som kopplar samman core-komponenterna och Angular.

## Installera

Installera detta paket genom att köra `npm i --save @digi/core-angular`. I och med detta kommer också Digi Core att installera, vilket är en dependency där de faktiska komponenterna ligger.

Tänk på att du fortfarande behöver `@digi/styles` för all styling.

## Använda
Lägg bara till modulen `DigiCoreAngularModule` i `app.module.ts` i din angular-app så kommer du att kunna använda komponenterna.
