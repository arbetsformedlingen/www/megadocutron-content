---
title: digi-media-image
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_media/media-image/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_media/media-image/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_media/media-image/readme.md
tags:
- libs::core::package::src::components::_media::media-image::readme.md
- core::package::src::components::_media::media-image::readme.md
- package::src::components::_media::media-image::readme.md
- src::components::_media::media-image::readme.md
- components::_media::media-image::readme.md
- _media::media-image::readme.md
- media-image::readme.md
- readme.md
---
# digi-media-image

This is an image component with built in lazy-loading. If width and height-props are provided, it also creates a correctly sized placeholder before load, to prevent ugly repaint of the document.

<!-- Auto Generated Below -->


## Properties

| Property             | Attribute       | Description                                                                                                                                   | Type                                         | Default                                            |
| -------------------- | --------------- | --------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- | -------------------------------------------------- |
| `afAlt` _(required)_ | `af-alt`        | Sätter attributet 'alt'.                                                                                                                      | `string`                                     | `undefined`                                        |
| `afAriaLabel`        | `af-aria-label` | Sätter attributet 'aria-label'.                                                                                                               | `string`                                     | `undefined`                                        |
| `afFullwidth`        | `af-fullwidth`  | Sätter bilden till 100% i bredd och anpassar höjden automatiskt efter bredden.                                                                | `boolean`                                    | `undefined`                                        |
| `afHeight`           | `af-height`     | Sätter attributet 'height'. Om både 'width' och 'height' är satta så kommer komponenten att ha rätt aspektratio även innan bilden laddats in. | `string`                                     | `undefined`                                        |
| `afObserverOptions`  | --              | Skicka options till komponentens interna Intersection Observer. T.ex. för att kontrollera när bilden lazy loadas.                             | `{ rootMargin: string; threshold: number; }` | `{     rootMargin: '200px',     threshold: 0,   }` |
| `afSrc`              | `af-src`        | Sätter attributet 'src'.                                                                                                                      | `string`                                     | `undefined`                                        |
| `afSrcset`           | `af-srcset`     | Sätter attributet 'srcset'.                                                                                                                   | `string`                                     | `undefined`                                        |
| `afTitle`            | `af-title`      | Sätter attributet 'title'.                                                                                                                    | `string`                                     | `undefined`                                        |
| `afUnlazy`           | `af-unlazy`     | Tar bort lazy loading av bilden.                                                                                                              | `boolean`                                    | `false`                                            |
| `afWidth`            | `af-width`      | Sätter attributet 'width'. Om både 'width' och 'height' är satta så kommer komponenten att ha rätt aspektratio även innan bilden laddats in.  | `string`                                     | `undefined`                                        |


## Events

| Event      | Description                   | Type               |
| ---------- | ----------------------------- | ------------------ |
| `afOnLoad` | Bildlementets 'onload'-event. | `CustomEvent<any>` |


## Dependencies

### Depends on

- [digi-util-intersection-observer](../../_util/util-intersection-observer)

### Graph
```mermaid
graph TD;
  digi-media-image --> digi-util-intersection-observer
  style digi-media-image fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
