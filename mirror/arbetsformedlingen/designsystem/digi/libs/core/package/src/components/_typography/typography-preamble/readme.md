---
title: digi-typography-preamble
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_typography/typography-preamble/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_typography/typography-preamble/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_typography/typography-preamble/readme.md
tags:
- libs::core::package::src::components::_typography::typography-preamble::readme.md
- core::package::src::components::_typography::typography-preamble::readme.md
- package::src::components::_typography::typography-preamble::readme.md
- src::components::_typography::typography-preamble::readme.md
- components::_typography::typography-preamble::readme.md
- _typography::typography-preamble::readme.md
- typography-preamble::readme.md
- readme.md
---
# digi-typography-preamble

This creates a preamble. It's as basic as it gets (it doesn't have any props at all). Even though it is possible to use any element types as children, it is much preferred to use only a text-node, since the component itself wraps everything in a `<p>`-tag.

<!-- Auto Generated Below -->


## Slots

| Slot        | Description           |
| ----------- | --------------------- |
| `"default"` | Should be a text node |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
