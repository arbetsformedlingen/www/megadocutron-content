---
title: digi-navigation-sidebar-button
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_navigation/navigation-sidebar-button/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_navigation/navigation-sidebar-button/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_navigation/navigation-sidebar-button/readme.md
tags:
- libs::core::package::src::components::_navigation::navigation-sidebar-button::readme.md
- core::package::src::components::_navigation::navigation-sidebar-button::readme.md
- package::src::components::_navigation::navigation-sidebar-button::readme.md
- src::components::_navigation::navigation-sidebar-button::readme.md
- components::_navigation::navigation-sidebar-button::readme.md
- _navigation::navigation-sidebar-button::readme.md
- navigation-sidebar-button::readme.md
- readme.md
---
# digi-navigation-sidebar-button

This is a navigation sidebar button used by navigation-sidebar for toggling it.

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute       | Description                                                                                        | Type     | Default                                               |
| ------------- | --------------- | -------------------------------------------------------------------------------------------------- | -------- | ----------------------------------------------------- |
| `afAriaLabel` | `af-aria-label` | Sätter attributet 'aria-labelledby'.                                                               | `string` | `undefined`                                           |
| `afId`        | `af-id`         | Sätter attributet 'id' på det omslutande elementet. Ges inget värde så genereras ett slumpmässigt. | `string` | `randomIdGenerator('digi-navigation-sidebar-button')` |
| `afText`      | `af-text`       | Sätter texten för toggleknappen.                                                                   | `string` | `undefined`                                           |


## Events

| Event        | Description                    | Type               |
| ------------ | ------------------------------ | ------------------ |
| `afOnToggle` | Toggleknappens 'onclick'-event | `CustomEvent<any>` |


## CSS Custom Properties

| Name                                         | Description  |
| -------------------------------------------- | ------------ |
| `--digi--navigation-sidebar-button--display` | inline-flex; |


## Dependencies

### Depends on

- [digi-button](../../_button/button)
- [digi-icon](../../_icon/icon)

### Graph
```mermaid
graph TD;
  digi-navigation-sidebar-button --> digi-button
  digi-navigation-sidebar-button --> digi-icon
  style digi-navigation-sidebar-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
