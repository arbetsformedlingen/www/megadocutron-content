---
title: digi-navigation-context-menu-item
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_navigation/navigation-context-menu-item/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_navigation/navigation-context-menu-item/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_navigation/navigation-context-menu-item/readme.md
tags:
- libs::core::package::src::components::_navigation::navigation-context-menu-item::readme.md
- core::package::src::components::_navigation::navigation-context-menu-item::readme.md
- package::src::components::_navigation::navigation-context-menu-item::readme.md
- src::components::_navigation::navigation-context-menu-item::readme.md
- components::_navigation::navigation-context-menu-item::readme.md
- _navigation::navigation-context-menu-item::readme.md
- navigation-context-menu-item::readme.md
- readme.md
---
# digi-navigation-context-menu-item

This component is used by `digi-navigation-context-menu` for creating supported elements such as buttons or links.

It needs a afType value of 'button' or 'link' in order to work.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { NavigationContextMenuItemType } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute | Description                                 | Type                                                                         | Default     |
| --------------------- | --------- | ------------------------------------------- | ---------------------------------------------------------------------------- | ----------- |
| `afDir`               | `af-dir`  | Sätter attributet 'dir'.                    | `string`                                                                     | `undefined` |
| `afHref`              | `af-href` | Sätter attributet 'href'.                   | `string`                                                                     | `undefined` |
| `afLang`              | `af-lang` | Sätter attributet 'lang'.                   | `string`                                                                     | `undefined` |
| `afText` _(required)_ | `af-text` | Sätter texten i komponenten                 | `string`                                                                     | `undefined` |
| `afType` _(required)_ | `af-type` | Sätter typ. Kan vara 'link' eller 'button'. | `NavigationContextMenuItemType.BUTTON \| NavigationContextMenuItemType.LINK` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
