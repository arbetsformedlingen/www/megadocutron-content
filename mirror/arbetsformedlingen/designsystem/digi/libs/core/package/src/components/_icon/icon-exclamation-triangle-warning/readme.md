---
title: digi-icon-exclamation-triangle-warning
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_icon/icon-exclamation-triangle-warning/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_icon/icon-exclamation-triangle-warning/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-exclamation-triangle-warning/readme.md
tags:
- libs::core::package::src::components::_icon::icon-exclamation-triangle-warning::readme.md
- core::package::src::components::_icon::icon-exclamation-triangle-warning::readme.md
- package::src::components::_icon::icon-exclamation-triangle-warning::readme.md
- src::components::_icon::icon-exclamation-triangle-warning::readme.md
- components::_icon::icon-exclamation-triangle-warning::readme.md
- _icon::icon-exclamation-triangle-warning::readme.md
- icon-exclamation-triangle-warning::readme.md
- readme.md
---
# digi-icon-exclamation-triangle-warning



<!-- Auto Generated Below -->


## Properties

| Property              | Attribute                | Description                                                                   | Type      | Default     |
| --------------------- | ------------------------ | ----------------------------------------------------------------------------- | --------- | ----------- |
| `afDesc`              | `af-desc`                | Lägger till ett descelement i svg:n                                           | `string`  | `undefined` |
| `afSvgAriaHidden`     | `af-svg-aria-hidden`     | För att dölja ikonen för skärmläsare. Default är satt till true.              | `boolean` | `true`      |
| `afSvgAriaLabelledby` | `af-svg-aria-labelledby` | Referera till andra element på sidan för att definiera ett tillgängligt namn. | `string`  | `undefined` |
| `afTitle`             | `af-title`               | Lägger till ett titleelement i svg:n                                          | `string`  | `undefined` |


## CSS Custom Properties

| Name                                                    | Description                         |
| ------------------------------------------------------- | ----------------------------------- |
| `--digi--icon--color`                                   | var(--digi--color--icons--primary); |
| `--digi--icon--height`                                  | auto;                               |
| `--digi--icon--width`                                   | initial;                            |
| `--digi--icon-exclamation-triangle-warning--background` | var(--digi--color--icons--warning); |
| `--digi--icon-exclamation-triangle-warning--outline`    | var(--digi--color--text--primary);  |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
