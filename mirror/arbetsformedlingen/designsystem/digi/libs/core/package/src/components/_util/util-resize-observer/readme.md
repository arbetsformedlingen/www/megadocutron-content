---
title: digi-util-resize-observer
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_util/util-resize-observer/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_util/util-resize-observer/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_util/util-resize-observer/readme.md
tags:
- libs::core::package::src::components::_util::util-resize-observer::readme.md
- core::package::src::components::_util::util-resize-observer::readme.md
- package::src::components::_util::util-resize-observer::readme.md
- src::components::_util::util-resize-observer::readme.md
- components::_util::util-resize-observer::readme.md
- _util::util-resize-observer::readme.md
- util-resize-observer::readme.md
- readme.md
---
# digi-util-resize-observer

This is a component wrapper around the Mutation Observer API. It is very useful for detecting changes to the document inside of it (when nodes are added or deletect, attributes change etc).

<!-- Auto Generated Below -->


## Events

| Event        | Description                    | Type                               |
| ------------ | ------------------------------ | ---------------------------------- |
| `afOnChange` | När komponenten ändrar storlek | `CustomEvent<ResizeObserverEntry>` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
