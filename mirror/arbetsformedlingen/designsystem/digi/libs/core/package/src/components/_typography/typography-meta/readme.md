---
title: digi-typography-meta
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_typography/typography-meta/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_typography/typography-meta/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_typography/typography-meta/readme.md
tags:
- libs::core::package::src::components::_typography::typography-meta::readme.md
- core::package::src::components::_typography::typography-meta::readme.md
- package::src::components::_typography::typography-meta::readme.md
- src::components::_typography::typography-meta::readme.md
- components::_typography::typography-meta::readme.md
- _typography::typography-meta::readme.md
- typography-meta::readme.md
- readme.md
---
# digi-typography-meta

This component comes in two variations, default and secondary for displaying elements on top of each other through slots.
Each slot is wrapped inside a `</div>` and can contain any element but text elements are preferable.

The slots default and secondary has its style reversed when using the different variations.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { TypographyMetaVariation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                     | Type                       | Default                           |
| ------------- | -------------- | ----------------------------------------------- | -------------------------- | --------------------------------- |
| `afVariation` | `af-variation` | Sätter variant. Kan vara primär eller sekundär. | `"primary" \| "secondary"` | `TypographyMetaVariation.PRIMARY` |


## Slots

| Slot          | Description                                                     |
| ------------- | --------------------------------------------------------------- |
| `"default"`   | Kan användas för olika element, ett text element är föredraget. |
| `"secondary"` | Kan användas för olika element, ett text element är föredraget. |


## CSS Custom Properties

| Name                                                   | Description                                              |
| ------------------------------------------------------ | -------------------------------------------------------- |
| `--digi--typography-meta--font-family`                 | var(--digi--global--typography--font-family--default);   |
| `--digi--typography-meta--font-size`                   | var(--digi--typography--preamble--font-size--desktop);   |
| `--digi--typography-meta--font-weight--semibold`       | var(--digi--typography--preamble--font-weight--desktop); |
| `--digi--typography-meta--meta--font-weight`           | var(--digi--typography--body--font-weight--desktop);     |
| `--digi--typography-meta--meta-secondary--font-weight` | var(--digi--typography--body--font-weight--desktop);     |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
