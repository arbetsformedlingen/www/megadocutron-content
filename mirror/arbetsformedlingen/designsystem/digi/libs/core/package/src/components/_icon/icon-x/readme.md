---
title: digi-icon-x
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_icon/icon-x/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_icon/icon-x/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-x/readme.md
tags:
- libs::core::package::src::components::_icon::icon-x::readme.md
- core::package::src::components::_icon::icon-x::readme.md
- package::src::components::_icon::icon-x::readme.md
- src::components::_icon::icon-x::readme.md
- components::_icon::icon-x::readme.md
- _icon::icon-x::readme.md
- icon-x::readme.md
- readme.md
---
# digi-icon-x



<!-- Auto Generated Below -->


## Properties

| Property              | Attribute                | Description                                                                   | Type      | Default     |
| --------------------- | ------------------------ | ----------------------------------------------------------------------------- | --------- | ----------- |
| `afDesc`              | `af-desc`                | Lägger till ett descelement i svg:n                                           | `string`  | `undefined` |
| `afSvgAriaHidden`     | `af-svg-aria-hidden`     | För att dölja ikonen för skärmläsare. Default är satt till true.              | `boolean` | `true`      |
| `afSvgAriaLabelledby` | `af-svg-aria-labelledby` | Referera till andra element på sidan för att definiera ett tillgängligt namn. | `string`  | `undefined` |
| `afTitle`             | `af-title`               | Lägger till ett titleelement i svg:n                                          | `string`  | `undefined` |


## CSS Custom Properties

| Name                   | Description                         |
| ---------------------- | ----------------------------------- |
| `--digi--icon--color`  | var(--digi--color--icons--primary); |
| `--digi--icon--height` | auto;                               |
| `--digi--icon--width`  | initial;                            |


## Dependencies

### Used by

 - [digi-bar-chart](../../_chart/bar-chart)
 - [digi-chart-line](../../_chart/chart-line)

### Graph
```mermaid
graph TD;
  digi-bar-chart --> digi-icon-x
  digi-chart-line --> digi-icon-x
  style digi-icon-x fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
