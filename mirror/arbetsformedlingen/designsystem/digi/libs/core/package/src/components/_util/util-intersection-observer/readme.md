---
title: digi-util-intersection-observer
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_util/util-intersection-observer/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_util/util-intersection-observer/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_util/util-intersection-observer/readme.md
tags:
- libs::core::package::src::components::_util::util-intersection-observer::readme.md
- core::package::src::components::_util::util-intersection-observer::readme.md
- package::src::components::_util::util-intersection-observer::readme.md
- src::components::_util::util-intersection-observer::readme.md
- components::_util::util-intersection-observer::readme.md
- _util::util-intersection-observer::readme.md
- util-intersection-observer::readme.md
- readme.md
---
# digi-util-intersection-observer

This is a component wrapper around the Intersection Observer API. It emits events when it matches the observer options (default is when it enters and leaves the viewport).

<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description                                                                                                       | Type               | Default                                    |
| ----------- | ------------ | ----------------------------------------------------------------------------------------------------------------- | ------------------ | ------------------------------------------ |
| `afOnce`    | `af-once`    | Stäng av observern efter första intersection                                                                      | `boolean`          | `false`                                    |
| `afOptions` | `af-options` | Skicka options till komponentens interna Intersection Observer. T.ex. för att kontrollera när bilden lazy loadas. | `object \| string` | `{ 		rootMargin: '0px', 		threshold: 0 	}` |


## Events

| Event             | Description                                                                | Type                   |
| ----------------- | -------------------------------------------------------------------------- | ---------------------- |
| `afOnChange`      | Vid statusförändring mellan 'unintersected' och 'intersected'              | `CustomEvent<boolean>` |
| `afOnIntersect`   | Vid statusförändring till 'intersected'                                    | `CustomEvent<any>`     |
| `afOnUnintersect` | Vid statusförändring till 'unintersected' On every change to unintersected | `CustomEvent<any>`     |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


## Dependencies

### Used by

 - [digi-media-image](../../_media/media-image)

### Graph
```mermaid
graph TD;
  digi-media-image --> digi-util-intersection-observer
  style digi-util-intersection-observer fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
