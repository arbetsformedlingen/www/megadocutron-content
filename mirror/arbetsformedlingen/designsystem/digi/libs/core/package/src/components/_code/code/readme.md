---
title: digi-code
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_code/code/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_code/code/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_code/code/readme.md
tags:
- libs::core::package::src::components::_code::code::readme.md
- core::package::src::components::_code::code::readme.md
- package::src::components::_code::code::readme.md
- src::components::_code::code::readme.md
- components::_code::code::readme.md
- _code::code::readme.md
- code::readme.md
- readme.md
---
# digi-code

This creates a syntax highlighted inline `<code>`-element. It supports dark- and light theme and a number of different languages.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { CodeVaration, CodeLanguage } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                                  | Type                                                                                                                                                                             | Default               |
| ------------- | -------------- | ------------------------------------------------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------- |
| `afCode`      | `af-code`      | En sträng med den kod som du vill visa upp.                  | `string`                                                                                                                                                                         | `undefined`           |
| `afLang`      | `af-lang`      | Sätt språk inuti code-elementet. 'en' (engelska) är förvalt. | `string`                                                                                                                                                                         | `'en'`                |
| `afLanguage`  | `af-language`  | Sätt programmeringsspråk. 'html' är förvalt.                 | `CodeLanguage.BASH \| CodeLanguage.CSS \| CodeLanguage.GIT \| CodeLanguage.HTML \| CodeLanguage.JAVASCRIPT \| CodeLanguage.JSON \| CodeLanguage.SCSS \| CodeLanguage.TYPESCRIPT` | `CodeLanguage.HTML`   |
| `afVariation` | `af-variation` | Sätt färgtemat. Kan vara ljust eller mörkt.                  | `CodeVariation.DARK \| CodeVariation.LIGHT`                                                                                                                                      | `CodeVariation.LIGHT` |


## CSS Custom Properties

| Name                       | Description                              |
| -------------------------- | ---------------------------------------- |
| `--digi--code--background` | var(--digi--color--background--primary); |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
