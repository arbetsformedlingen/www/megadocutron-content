---
title: digi-util-detect-focus-outside
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_util/util-detect-focus-outside/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_util/util-detect-focus-outside/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_util/util-detect-focus-outside/readme.md
tags:
- libs::core::package::src::components::_util::util-detect-focus-outside::readme.md
- core::package::src::components::_util::util-detect-focus-outside::readme.md
- package::src::components::_util::util-detect-focus-outside::readme.md
- src::components::_util::util-detect-focus-outside::readme.md
- components::_util::util-detect-focus-outside::readme.md
- _util::util-detect-focus-outside::readme.md
- util-detect-focus-outside::readme.md
- readme.md
---
# digi-util-detect-focus-outside

This monitors focus events and emits events when focus is outside or inside.

<!-- Auto Generated Below -->


## Properties

| Property           | Attribute            | Description                                                                                                                                              | Type     | Default                                                         |
| ------------------ | -------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- | --------------------------------------------------------------- |
| `afDataIdentifier` | `af-data-identifier` | Sätter en unik identifierare som behövs för att kontrollera fokuseventen. Måste vara prefixad med 'data-'. Om inget väljs så skapas ett slumpmässigt id. | `string` | `randomIdGenerator( 		'data-digi-util-detect-focus-outside' 	)` |


## Events

| Event              | Description                   | Type                      |
| ------------------ | ----------------------------- | ------------------------- |
| `afOnFocusInside`  | Vid fokus inuti komponenten   | `CustomEvent<FocusEvent>` |
| `afOnFocusOutside` | Vid fokus utanför komponenten | `CustomEvent<FocusEvent>` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
