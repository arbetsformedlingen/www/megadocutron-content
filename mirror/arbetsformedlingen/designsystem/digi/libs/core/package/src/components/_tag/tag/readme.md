---
title: digi-tag
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_tag/tag/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_tag/tag/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_tag/tag/readme.md
tags:
- libs::core::package::src::components::_tag::tag::readme.md
- core::package::src::components::_tag::tag::readme.md
- package::src::components::_tag::tag::readme.md
- src::components::_tag::tag::readme.md
- components::_tag::tag::readme.md
- _tag::tag::readme.md
- tag::readme.md
- readme.md
---
# digi-tag

This is a tag component. It's basic and built on `digi-button`.
It comes in three different sizes (S, M, L) and has ability to remove the icon.

Head over to the `Knobs` tab to see it in action!

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { TagSize } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute       | Description                                | Type                                               | Default         |
| --------------------- | --------------- | ------------------------------------------ | -------------------------------------------------- | --------------- |
| `afAriaLabel`         | `af-aria-label` | Sätt attributet 'aria-label'.              | `string`                                           | `undefined`     |
| `afNoIcon`            | `af-no-icon`    | Tar bort taggens ikon. Falskt som förvalt. | `boolean`                                          | `false`         |
| `afSize`              | `af-size`       | Sätter taggens storlek.                    | `TagSize.LARGE \| TagSize.MEDIUM \| TagSize.SMALL` | `TagSize.SMALL` |
| `afText` _(required)_ | `af-text`       | Sätter taggens text.                       | `string`                                           | `undefined`     |


## Events

| Event       | Description                     | Type               |
| ----------- | ------------------------------- | ------------------ |
| `afOnClick` | Taggelementets 'onclick'-event. | `CustomEvent<any>` |


## CSS Custom Properties

| Name                         | Description                   |
| ---------------------------- | ----------------------------- |
| `--digi--tag--display`       | inline-block;                 |
| `--digi--tag--margin-bottom` | var(--digi--gutter--smaller); |


## Dependencies

### Depends on

- [digi-button](../../_button/button)
- [digi-icon](../../_icon/icon)

### Graph
```mermaid
graph TD;
  digi-tag --> digi-button
  digi-tag --> digi-icon
  style digi-tag fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
