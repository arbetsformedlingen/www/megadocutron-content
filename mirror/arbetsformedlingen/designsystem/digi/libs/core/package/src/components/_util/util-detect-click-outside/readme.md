---
title: digi-util-detect-click-outside
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_util/util-detect-click-outside/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_util/util-detect-click-outside/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_util/util-detect-click-outside/readme.md
tags:
- libs::core::package::src::components::_util::util-detect-click-outside::readme.md
- core::package::src::components::_util::util-detect-click-outside::readme.md
- package::src::components::_util::util-detect-click-outside::readme.md
- src::components::_util::util-detect-click-outside::readme.md
- components::_util::util-detect-click-outside::readme.md
- _util::util-detect-click-outside::readme.md
- util-detect-click-outside::readme.md
- readme.md
---
# digi-util-detect-click-outside

This monitors click events and emits events when clicks are outside or inside.

<!-- Auto Generated Below -->


## Properties

| Property           | Attribute            | Description                                                                                                                                              | Type     | Default                                                         |
| ------------------ | -------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- | --------------------------------------------------------------- |
| `afDataIdentifier` | `af-data-identifier` | Sätter en unik identifierare som behövs för att kontrollera klickeventen. Måste vara preficad med 'data-'. Om inget väljs så skapas ett slumpmässigt id. | `string` | `randomIdGenerator( 		'data-digi-util-detect-click-outside' 	)` |


## Events

| Event              | Description                   | Type                      |
| ------------------ | ----------------------------- | ------------------------- |
| `afOnClickInside`  | Vid klick inuti komponenten   | `CustomEvent<MouseEvent>` |
| `afOnClickOutside` | Vid klick utanför komponenten | `CustomEvent<MouseEvent>` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
