---
title: digi-form-fieldset
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_form/form-fieldset/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_form/form-fieldset/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_form/form-fieldset/readme.md
tags:
- libs::core::package::src::components::_form::form-fieldset::readme.md
- core::package::src::components::_form::form-fieldset::readme.md
- package::src::components::_form::form-fieldset::readme.md
- src::components::_form::form-fieldset::readme.md
- components::_form::form-fieldset::readme.md
- _form::form-fieldset::readme.md
- form-fieldset::readme.md
- readme.md
---
# digi-form-fieldset

This is a fieldset component including support for `legend` usage.

<!-- Auto Generated Below -->


## Properties

| Property   | Attribute   | Description                                                           | Type     | Default                                   |
| ---------- | ----------- | --------------------------------------------------------------------- | -------- | ----------------------------------------- |
| `afForm`   | `af-form`   | Fieldset-elementets form attribut.                                    | `string` | `undefined`                               |
| `afId`     | `af-id`     | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id. | `string` | `randomIdGenerator('digi-form-fieldset')` |
| `afLegend` | `af-legend` | Legend-elementets text.                                               | `string` | `undefined`                               |
| `afName`   | `af-name`   | Fieldset-elementets namn attribut.                                    | `string` | `undefined`                               |


## Slots

| Slot        | Description                                              |
| ----------- | -------------------------------------------------------- |
| `"default"` | Ska vara formulärselement. Till exempel digi-form-input. |


## CSS Custom Properties

| Name                                         | Description                                                 |
| -------------------------------------------- | ----------------------------------------------------------- |
| `--digi--form-fieldset--border`              | none;                                                       |
| `--digi--form-fieldset--legend--color`       | var(--digi--color--text--primary);                          |
| `--digi--form-fieldset--legend--font-family` | var(--digi--global--typography--font-family--default);      |
| `--digi--form-fieldset--legend--font-size`   | var(--digi--typography--description--font-size--desktop);   |
| `--digi--form-fieldset--legend--font-weight` | var(--digi--typography--description--font-weight--desktop); |
| `--digi--form-fieldset--legend--margin`      | var(--digi--margin--h2-large);                              |
| `--digi--form-fieldset--padding`             | 0;                                                          |


## Dependencies

### Used by

 - [digi-code-example](../../_code/code-example)

### Graph
```mermaid
graph TD;
  digi-code-example --> digi-form-fieldset
  style digi-form-fieldset fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
