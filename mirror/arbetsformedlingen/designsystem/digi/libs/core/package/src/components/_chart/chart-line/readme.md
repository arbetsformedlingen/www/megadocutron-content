---
title: digi-chart-line
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_chart/chart-line/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_chart/chart-line/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_chart/chart-line/readme.md
tags:
- libs::core::package::src::components::_chart::chart-line::readme.md
- core::package::src::components::_chart::chart-line::readme.md
- package::src::components::_chart::chart-line::readme.md
- src::components::_chart::chart-line::readme.md
- components::_chart::chart-line::readme.md
- _chart::chart-line::readme.md
- chart-line::readme.md
- readme.md
---
# digi-chart-line

<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description | Type                      | Default                        |
| ---------------- | ------------------ | ----------- | ------------------------- | ------------------------------ |
| `afChartData`    | `af-chart-data`    |             | `ChartLineData \| string` | `undefined`                    |
| `afHeadingLevel` | `af-heading-level` |             | `string`                  | `undefined`                    |
| `afId`           | `af-id`            |             | `string`                  | `randomIdGenerator('tooltip')` |


## Slots

| Slot       | Description              |
| ---------- | ------------------------ |
| `"mySlot"` | Slot description, if any |


## Dependencies

### Depends on

- [digi-typography](../../_typography/typography)
- [digi-button](../../_button/button)
- [digi-icon-x](../../_icon/icon-x)

### Graph
```mermaid
graph TD;
  digi-chart-line --> digi-typography
  digi-chart-line --> digi-button
  digi-chart-line --> digi-icon-x
  style digi-chart-line fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
