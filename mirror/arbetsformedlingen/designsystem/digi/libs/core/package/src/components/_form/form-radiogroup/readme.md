---
title: digi-form-radiogroup
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_form/form-radiogroup/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_form/form-radiogroup/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_form/form-radiogroup/readme.md
tags:
- libs::core::package::src::components::_form::form-radiogroup::readme.md
- core::package::src::components::_form::form-radiogroup::readme.md
- package::src::components::_form::form-radiogroup::readme.md
- src::components::_form::form-radiogroup::readme.md
- components::_form::form-radiogroup::readme.md
- _form::form-radiogroup::readme.md
- form-radiogroup::readme.md
- readme.md
---
# digi-form-radiogroup

<!-- Auto Generated Below -->


## Properties

| Property  | Attribute  | Description            | Type     | Default                                     |
| --------- | ---------- | ---------------------- | -------- | ------------------------------------------- |
| `afName`  | `af-name`  | Namnet på radiogruppen | `string` | `randomIdGenerator('digi-form-radiogroup')` |
| `afValue` | `af-value` | Värdet på radiogruppen | `string` | `undefined`                                 |
| `value`   | `value`    | Värdet på radiogruppen | `string` | `undefined`                                 |


## Events

| Event             | Description             | Type                  |
| ----------------- | ----------------------- | --------------------- |
| `afOnGroupChange` | Event när värdet ändras | `CustomEvent<string>` |


## Slots

| Slot             | Description |
| ---------------- | ----------- |
| `"radiobuttons"` |             |


## Dependencies

### Depends on

- [digi-util-mutation-observer](../../_util/util-mutation-observer)

### Graph
```mermaid
graph TD;
  digi-form-radiogroup --> digi-util-mutation-observer
  style digi-form-radiogroup fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
