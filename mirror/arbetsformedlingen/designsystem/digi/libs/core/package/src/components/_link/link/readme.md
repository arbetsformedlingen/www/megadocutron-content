---
title: digi-link
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_link/link/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_link/link/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_link/link/readme.md
tags:
- libs::core::package::src::components::_link::link::readme.md
- core::package::src::components::_link::link::readme.md
- package::src::components::_link::link::readme.md
- src::components::_link::link::readme.md
- components::_link::link::readme.md
- _link::link::readme.md
- link::readme.md
- readme.md
---
# digi-link

This is a link component.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { LinkVariation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute           | Description                                                                                                                           | Type                 | Default               |
| --------------------- | ------------------- | ------------------------------------------------------------------------------------------------------------------------------------- | -------------------- | --------------------- |
| `afDescribedby`       | `af-describedby`    | Sätter attributet 'aria-describedby'.                                                                                                 | `string`             | `undefined`           |
| `afHref` _(required)_ | `af-href`           | Sätter attributet 'href'.                                                                                                             | `string`             | `undefined`           |
| `afLinkContainer`     | `af-link-container` | Sätt till true om du använder Angular, se exempelkod under 'Översikt'                                                                 | `boolean`            | `false`               |
| `afOverrideLink`      | `af-override-link`  | Kringgår länkens vanliga beteende. Bör endast användas om det vanliga beteendet är problematiskt pga dynamisk routing eller liknande. | `boolean`            | `false`               |
| `afTarget`            | `af-target`         | Sätter attributet 'target'. Om värdet är '_blank' så lägger komponenten automatikt till ett ´ref´-attribut med 'noopener noreferrer'. | `string`             | `undefined`           |
| `afVariation`         | `af-variation`      | Sätter variant. Kan vara 'small' eller 'large'.                                                                                       | `"large" \| "small"` | `LinkVariation.SMALL` |


## Events

| Event       | Description                                                                     | Type                      |
| ----------- | ------------------------------------------------------------------------------- | ------------------------- |
| `afOnClick` | Länkelementets 'onclick'-event.                                                 | `CustomEvent<MouseEvent>` |
| `afOnReady` | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>`        |


## Methods

### `afMGetLinkElement() => Promise<any>`



#### Returns

Type: `Promise<any>`




## Slots

| Slot        | Description                                   |
| ----------- | --------------------------------------------- |
| `"default"` | Ska vara en textnod                           |
| `"icon"`    | Ska vara en ikon (eller en ikonlik) komponent |


## CSS Custom Properties

| Name                                           | Description                                                |
| ---------------------------------------------- | ---------------------------------------------------------- |
| `--digi--link--color--default`                 | var(--digi--color--text--link);                            |
| `--digi--link--color--hover`                   | var(--digi--color--text--link-hover);                      |
| `--digi--link--color--visited`                 | var(--digi--color--text--link-visited);                    |
| `--digi--link--font-family`                    | var(--digi--global--typography--font-family--default);     |
| `--digi--link--font-size--large`               | var(--digi--typography--link--font-size--desktop-large);   |
| `--digi--link--font-size--small`               | var(--digi--typography--link--font-size--desktop);         |
| `--digi--link--font-weight`                    | var(--digi--typography--link--font-weight--desktop);       |
| `--digi--link--gap`                            | var(--digi--gutter--medium);                               |
| `--digi--link--line-height`                    | var(--digi--typography--link--line-height--desktop);       |
| `--digi--link--text-decoration--default`       | var(--digi--typography--link--text-decoration--desktop);   |
| `--digi--link--text-decoration--hover`         | var(--digi--global--typography--text-decoration--default); |
| `--digi--link--text-decoration--icon--default` | var(--digi--global--typography--text-decoration--default); |
| `--digi--link--text-decoration--icon--hover`   | var(--digi--typography--link--text-decoration--desktop);   |


## Dependencies

### Used by

 - [digi-link-external](../link-external)
 - [digi-link-internal](../link-internal)

### Graph
```mermaid
graph TD;
  digi-link-external --> digi-link
  digi-link-internal --> digi-link
  style digi-link fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
