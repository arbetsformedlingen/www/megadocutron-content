---
title: digi-bar-chart
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_chart/bar-chart/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_chart/bar-chart/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_chart/bar-chart/readme.md
tags:
- libs::core::package::src::components::_chart::bar-chart::readme.md
- core::package::src::components::_chart::bar-chart::readme.md
- package::src::components::_chart::bar-chart::readme.md
- src::components::_chart::bar-chart::readme.md
- components::_chart::bar-chart::readme.md
- _chart::bar-chart::readme.md
- bar-chart::readme.md
- readme.md
---
# digi-bar-chart

<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                                                                                                              | Type                                                         | Default                        |
| ---------------- | ------------------ | ------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------ |
| `afChartData`    | `af-chart-data`    | Datan som ska visas upp i stapeldiagrammet, kan vara av antingen typen ChartLineData, eller strängifierad ChartLineData  | `ChartLineData \| string`                                    | `undefined`                    |
| `afHeadingLevel` | `af-heading-level` | En sträng med rubriknivån du vill ha i diagrammet, default är 'h3'                                                       | `string`                                                     | `'h3'`                         |
| `afId`           | `af-id`            | (Frivillig) Om du vill att komponenten ska ha ett unikt ID, kan du skicka in detta här, annars genereras ett automatiskt | `string`                                                     | `randomIdGenerator('tooltip')` |
| `afVariation`    | `af-variation`     | Sätter orienteringen på diagrammet, kan vara vertikalt eller horisontellt, vertikalt är förvalt.                         | `BarChartVariation.Horizontal \| BarChartVariation.Vertical` | `BarChartVariation.Vertical`   |


## Slots

| Slot       | Description              |
| ---------- | ------------------------ |
| `"mySlot"` | Slot description, if any |


## Dependencies

### Depends on

- [digi-typography](../../_typography/typography)
- [digi-button](../../_button/button)
- [digi-icon-x](../../_icon/icon-x)

### Graph
```mermaid
graph TD;
  digi-bar-chart --> digi-typography
  digi-bar-chart --> digi-button
  digi-bar-chart --> digi-icon-x
  style digi-bar-chart fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
