---
title: digi-typography-time
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/src/components/_typography/typography-time/readme.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/src/components/_typography/typography-time/readme.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_typography/typography-time/readme.md
tags:
- libs::core::package::src::components::_typography::typography-time::readme.md
- core::package::src::components::_typography::typography-time::readme.md
- package::src::components::_typography::typography-time::readme.md
- src::components::_typography::typography-time::readme.md
- components::_typography::typography-time::readme.md
- _typography::typography-time::readme.md
- typography-time::readme.md
- readme.md
---
# digi-typography-time

This creates a formated time and includes three different time variation formats. Head over to the Knobs-tab to see them in action.

This is a basic component and does not include any styling.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { TypographyTimeVariation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property                  | Attribute      | Description                                                                                       | Type                                  | Default                           |
| ------------------------- | -------------- | ------------------------------------------------------------------------------------------------- | ------------------------------------- | --------------------------------- |
| `afDateTime` _(required)_ | `af-date-time` | Ska vara ett datumobjekt, datumsträng eller ett datumformatterat nummer                           | `Date \| number \| string`            | `undefined`                       |
| `afLocale`                | `af-locale`    | Språk-tagg. För svenska, använd 'sv-SE'.                                                          | `string`                              | `'default'`                       |
| `afVariation`             | `af-variation` | Sätter variant. Bestämmer hur det ska formatteras. Kan vara 'primary', 'pretty' eller 'distance'. | `"distance" \| "pretty" \| "primary"` | `TypographyTimeVariation.PRIMARY` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
