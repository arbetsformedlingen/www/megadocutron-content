---
title: '@digi/core'
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/core/package/README.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/core/package/README.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/core/package/README.md
tags:
- libs::core::package::README.md
- core::package::README.md
- package::README.md
- README.md
---
# @digi/core

Designsystemet Digis huvudpaket. Innehåller komponentbibliotek och hjälparfunktioner.

TODO: Fyll på med massa info.
