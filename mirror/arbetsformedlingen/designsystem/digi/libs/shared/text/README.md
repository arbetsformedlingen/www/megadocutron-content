---
title: Text
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/shared/text/README.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/shared/text/README.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/shared/text/README.md
tags:
- libs::shared::text::README.md
- shared::text::README.md
- text::README.md
- README.md
---
# Text

Gemensamma texter, microcopy etc, som kan importeras och användas av komponenter och applikationer
