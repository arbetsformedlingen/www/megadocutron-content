---
title: shared-taxonomies
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/shared/taxonomies/README.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/shared/taxonomies/README.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/shared/taxonomies/README.md
tags:
- libs::shared::taxonomies::README.md
- shared::taxonomies::README.md
- taxonomies::README.md
- README.md
---
# shared-taxonomies

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test shared-taxonomies` to execute the unit tests via [Jest](https://jestjs.io).
