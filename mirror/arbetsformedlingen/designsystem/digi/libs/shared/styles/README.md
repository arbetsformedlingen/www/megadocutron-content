---
title: '@digi/styles'
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/shared/styles/README.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/shared/styles/README.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/shared/styles/README.md
tags:
- libs::shared::styles::README.md
- shared::styles::README.md
- styles::README.md
- README.md
---
# @digi/styles

Det här är CSS och SCSS för Digi Här hittar du variabler, mixins, funktioner och hjälparklasser.