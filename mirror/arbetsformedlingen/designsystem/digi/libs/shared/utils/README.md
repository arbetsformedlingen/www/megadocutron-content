---
title: shared-utils
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi/-/blob/main//libs/shared/utils/README.md
gitdir: /arbetsformedlingen/designsystem/digi
gitdir-file-path: /libs/shared/utils/README.md
date: '2023-11-10 10:52:14'
path: /arbetsformedlingen/designsystem/digi/libs/shared/utils/README.md
tags:
- libs::shared::utils::README.md
- shared::utils::README.md
- utils::README.md
- README.md
---
# shared-utils

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test shared-utils` to execute the unit tests via [Jest](https://jestjs.io).
