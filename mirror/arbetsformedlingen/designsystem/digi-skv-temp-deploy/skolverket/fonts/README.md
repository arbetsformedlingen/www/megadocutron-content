---
title: '@digi/core-fonts'
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi-skv-temp-deploy/-/blob/main//skolverket/fonts/README.md
gitdir: /arbetsformedlingen/designsystem/digi-skv-temp-deploy
gitdir-file-path: /skolverket/fonts/README.md
date: '2023-01-25 12:49:26'
path: /arbetsformedlingen/designsystem/digi-skv-temp-deploy/skolverket/fonts/README.md
tags:
- skolverket::fonts::README.md
- fonts::README.md
- README.md
---
# @digi/core-fonts

Det här är ett paket med typsnitten som ska användas i Arbetsförmedlingens digitala tjänster. På webben och för alla digitala tjänster som vi producerar själva använder vi uteslutande Open Sans. Det gäller rubriker, ingress och brödtext samt komponenter och övriga funktioner där det förekommer text.

## Läs inte in typsnitten via Googles CDN!!

Ibland inkluderas google-typsnitten inte lokalt på webbplatsen, utan bara när sidan nås av webbläsaren från Googles servrar. Om detta görs med en amerikansk Google Server överförs webbläserdata, dvs personliga data för webbplatsbesökaren, till denna Google Server i USA. Därför ska detta paket användas för att kunna inkludera typsnitten lokalt i varje applikation istället.

## Installera

Installera detta paket genom att köra `npm i --save @digi/core-fonts`.

## Användning

För att använda sig av typsnitten så behöver man inkludera dessa från `@digi/core-fonts/assets/fonts/` samt sen peka mot dessa från `css-/scss`-filen.

Detta går att göra på lite olika sätt beroende på vilken plattform man använder sig av, se längre ned för exempel i Angular. I andra plattformar kan man kopiera över dessa t.ex. med webpack eller liknande teknik.

Nästa steg är att läsa in `css-/scss`-filen. Detta kan du göra t.ex. i din global `scss`-fil. Exempel:

`@import '@digi/core-fonts/src/fonts';`

### Användning i Angular-applikation

I filen `angular.json` kan man välja att inkludera filerna från paketet genom att lägga till dessa under `assets`. Exempel:

```
"assets": [
  "src/favicon.ico",
  "src/assets",
  {
    "glob": "**/*",
    "input": "./node_modules/@digi/core-fonts/src/assets/fonts",
    "output": "/assets/fonts"
  }
],
```
