---
title: '@digi/skolverket temporär distribution'
gitlaburl: https://gitlab.com/arbetsformedlingen/designsystem/digi-skv-temp-deploy/-/blob/main//README.md
gitdir: /arbetsformedlingen/designsystem/digi-skv-temp-deploy
gitdir-file-path: /README.md
date: '2023-01-25 12:49:26'
path: /arbetsformedlingen/designsystem/digi-skv-temp-deploy/README.md
tags:
- README.md
---
# @digi/skolverket temporär distribution

Detta är Skolverkets temporära distribution av Skolverkets designsystem: @digi/skolverket och @digi/skolverket-angular.

**_OBS: Detta är en högst tillfällig lösning, enbart syftad till att låta interna utvecklingsteam komma igång tills den riktiga infrastrukturen är på plats. Målet är att distribuera med npm och cdn så snart det bara går._**

## @digi/skolverket

Detta är det grundläggande paketet som innehåller komponentbibliotek, css-variabler och annat smått och gott. Detta är helt teknikagnostiskt, bygger på web components och kan därför användas oavsett teknikstack och ramverk.

Länka in rätt css och javascript så ska det sedan gå att använda biblioteket:

```html
<script
  type="module"
  src="skolverket/dist/digi-skolverket/digi-skolverket.esm.js"
></script>
<script
  nomodule
  src="skolverket/dist/digi-skolverket/digi-skolverket.js"
></script>
<link rel="stylesheet" href="skolverket/dist/digi-skolverket/fonts/fonts.css" />
<link
  rel="stylesheet"
  href="skolverket/dist/digi-skolverket/digi-skolverket.css"
/>
```

OBS: Att publicera en dokumentation av komponenterna är av högsta prio, men för tillfället så får vi hänvisa till Arbetsförmedlingens dokumentationswebb. De använder samma baskomponenter som Skolverket gör, så funktionaliteten är den samma, även om de ser annorlunda ut: https://designsystem.arbetsformedlingen.se/komponenter/om-komponenter

## @digi/skolverket-angular

Detta är en Angularifierad version av komponentbiblioteket. De använder fortfarande web components, men har bindningar för reactive forms etc som borde göra det lite lättare att arbeta med.

Snart kommer allt detta distribueras med npm precis som vilka tredjepartspaket som helst, men tills dess är detta processen för att använda komponenterna i din Angularapplikation:

### 1. Installera beroenden

Detta kommer inte vara nödvändigt när vi distribuerar med npm.

`npm i @stencil/core @stencil/sass`

### 2. Lägg till modulen

Du behöver importera modulen `DigiSkolverketAngularModule` i din applikation. Så här kan ett enkelt exempel se ut i `app.module.ts`

```ts
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { DigiSkolverketAngularModule } from "din-sökväg-till-paketen/skolverket-angular"; // <--- Byt sökvägen till där du lagt till paketet

import { AppComponent } from "./app.component";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, DigiSkolverketAngularModule], // <--- Lägg till importen
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
```

### 3. Lägg till css

Både fontdeklarationer och övrig css finns i `@digi/skolverket`:

- `skolverket/dist/digi-skolverket/digi-skolverket.css`
- `skolverket/dist/digi-skolverket/fonts/src/fonts.css`

Om du använder sass så kan du lägga till dem som importer på följande vis:

```scss
@import "din-sökväg-till-paketen/skolverket/fonts/src/fonts.css";
@import "din-sökväg-till-paketen/skolverket/dist/digi-skolverket/digi-skolverket.css";
```

De går ju även att lägga till som styles i `angular.json` om man vill.

För eventuella frågor, hör av dig till jakob.berglund@skolverket.se (teknik) eller joakim.ekman@skolverket.se (projekt)
