---
title: Automatic conversion of API specs to DCAT
gitlaburl: https://gitlab.com/arbetsformedlingen/devops/api-spec-to-dcat/-/blob/main//README.md
gitdir: /arbetsformedlingen/devops/api-spec-to-dcat
gitdir-file-path: /README.md
date: '2023-03-07 11:50:19'
path: /arbetsformedlingen/devops/api-spec-to-dcat/README.md
tags:
- README.md
---
# Automatic conversion of API specs to DCAT
This is a script that:
 1. Downloads all API specs which are listed in `conf/api-spec-urls.txt`.
 2. Downloads all files called `X.dcat.json` from S3 which have a
    corresponding file `X`.
 2. Each downloaded file is validated by doing a test conversion to
    DCAT format, and files that fail this validation are discarded.
    The output of each file's validation can be inspected in `staging/.processor.out`.
 3. A final DCAT file is assembled based on the validated spec files.
 4. The DCAT file is uploaded to S3 for publication on `data.jobtechdev.se`.

For DCAT conversion, this software is used: https://github.com/DIGGSweden/DCAT-AP-SE-Processor


# Configuration
 - `conf/catalog.json` - The catalog description, general for all APIs.
 - `conf/api-spec-urls.txt` - The list of API URLs.


# Usage
The main intended usage of this script is running it as a cron jon in OpenShift.
See its configuration here:
https://gitlab.com/arbetsformedlingen/devops/api-spec-to-dcat-infra

You can also run it locally, if you have all dependencies installed,
and use compatible versions of the tools (notably, BSD-tools will work
differently and probably fail).


# Known limitations
- Does not handle RAML, YAML or Swagger/OpenAPI < 3.
