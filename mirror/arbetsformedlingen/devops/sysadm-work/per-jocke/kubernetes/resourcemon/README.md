---
title: resourcemon for kubernetes pods
gitlaburl: https://gitlab.com/arbetsformedlingen/devops/sysadm-work/per-jocke/-/blob/main//kubernetes/resourcemon/README.md
gitdir: /arbetsformedlingen/devops/sysadm-work/per-jocke
gitdir-file-path: /kubernetes/resourcemon/README.md
date: '2022-11-16 16:04:47'
path: /arbetsformedlingen/devops/sysadm-work/per-jocke/kubernetes/resourcemon/README.md
tags:
- kubernetes::resourcemon::README.md
- resourcemon::README.md
- README.md
---
# resourcemon for kubernetes pods

A utility to aggregate the requested resources (if any) and the
actually used resources for each pod in a cluster.

## Installation
First, install the `oc` command.

```
apt-get install liblocal-lib-perl curl make jq perl libjson-perl
dnf     install perl-local-lib    curl make jq perl perl-JSON
perl setup.pl
```

## Usage
First login to the cluster with `oc`. The execute the program without arguments.

Example:
```
$ perl root/bin/resourcemon.pl

$ ls -l data/
  -rw-rw-r-- 1 weipe weipe  1283 Nov 16 16:03 connect-once-develop---af-connect-596845cbdd-tc7js
  -rw-rw-r-- 1 weipe weipe  1313 Nov 16 16:03 connect-once-develop---af-connect-demo-6dd4664447-zmngp
...
```

Each result file is a jsonl file which will be appended to, each time the script runs.