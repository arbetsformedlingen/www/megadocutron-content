---
title: vault
gitlaburl: https://gitlab.com/arbetsformedlingen/devops/vault/-/blob/main//README.md
gitdir: /arbetsformedlingen/devops/vault
gitdir-file-path: /README.md
date: '2023-08-01 15:57:56'
path: /arbetsformedlingen/devops/vault/README.md
tags:
- README.md
---
# vault   

The intention of this repo is to extend the upstream Vault helm chart from Hashicorp, extending it to add additional functionality and repackaging it as a new helm chart for use in the Onprem clusters

## Repo layout

### `helm/`

The **helm/** directory contains the manifests needed to wrap the upstream chart (see `helm/Chart.yaml`) with new manifests (see `helm/templates`) that include:

*vault-init.yaml*: This is a Kubernetes `job` that runs when vault is first deployed to perform the actions of...
- initializing vault 
- saving the generated root token and recovery keys as kubernetes secrets in the vault namespace
- executing the terraform code packaged with the container (see `images/init/*.tf`) that configures the kubernetes authentication method

*pvc.yaml*: Persistent storage for the init-job to write the terraform state that is generated and updated on each run of the terraform found in `images/init/`

*vault-rbac.yaml*: adds permissions for the init job to reach and create secrets in the vault namespace
- it creates the `vault-recovery-keys` and `vault-root-token` secrets that are generated when vault is first initialized 
- if the job is deleted and recreated to trigger another run when vault is already unsealed and initialized then reads the token from `vault-root-token` and uses it when executing `terraform apply`

The **Chart.yaml** defines the new helm chart and sources the upstream hashcorp vault helm chart as a dependency 

The **values.yaml** defines all the default overrides for configuring the upstream helm chart as well as any parameters that have been templated in the manifests found in templates/

### `images/`

*backup/*: there is nothing here yet, the intention is to have a docker file that builds a simple image containing the vault cli and any other tooling that is needed to take a snapshot of vault and write it to the onprem minio (or any s3 compatible) storage

see the documentation for the manual backup procedures for inspiration of what should be automated https://developer.hashicorp.com/vault/tutorials/standard-procedures/sop-backup#manual-backup-procedures 

once there is a container with the needed tooling then the Helm chart should be further extended adding a cronjob that runs on your required schedule to run the container, taking the snapshots and writing them to your storage

*init/*: here we have the Dockerfile, init.sh shell script and terraform files that are used by the vault-init job when  vault is first deployed and again anytime the vault-init job is deleted and recreated

**any modifications to the terraform files will be applied only when vault is first deployed or when the vault-init job is deleted and recreated**

*.gitlab-ci.yaml*: this defines the gitlab cicd pipeline that is run when changes are committed to the main branch of this repository 
the jobs that are currently defined are:
- build-init-container
Builds the container used for the vault-init job and pushes the image to nexus
- package-publish-helm
Builds the helm chart and pushes it to the local repos gitlab packages

### TODO

# automate the version incrementing 

When ever a merge to main branch occurs and the pipeline runs new versions of the helm chart and container image are released and need to be tagged, currently there is no automation for that tagging and the current values need to be incremented in each merge:

https://gitlab.com/arbetsformedlingen/devops/vault/-/blob/main/.gitlab-ci.yml#L2
https://gitlab.com/arbetsformedlingen/devops/vault/-/blob/main/helm/Chart.yaml#L12
https://gitlab.com/arbetsformedlingen/devops/vault/-/blob/main/helm/values.yaml#L2

then in the [ocp-gitops-infra](https://gitlab.com/arbetsformedlingen/devops/ocp-infra-gitops) repo the at least the versions of the extended vault chart needs to be incremented to deploy the newest version: https://gitlab.com/arbetsformedlingen/devops/ocp-infra-gitops/-/blob/main/overlays/onprem-test/vault/Chart.yaml#L6 

you may want to have this automatically bumped on successful MR here to automate the release to onprem-test but typically we would suggest to manually bump that value for onprem (prod) unless you have very robust tests

# add a cronjob for backups

add docker file to images/backup, needs logic, vault cli and tooling for accessing and writing the backups to the minio
add a cronjob that uses that backup image

see https://developer.hashicorp.com/vault/tutorials/standard-procedures/sop-backup#manual-backup-procedures for the manual backup procedure that needs to be automated

https://docs.openshift.com/container-platform/4.11/nodes/jobs/nodes-nodes-jobs.html#nodes-nodes-jobs-creating-cron_nodes-nodes-jobs

# validate if PROXY settings are needed and remove if not

early on we added proxy env vars for vault and the init job as there were issues with the init job talking to vault and the vault nodes talking to each other: 

https://gitlab.com/arbetsformedlingen/devops/vault/-/blob/main/helm/values.yaml#L26

https://gitlab.com/arbetsformedlingen/devops/vault/-/blob/main/helm/templates/vault-init.yaml#L28-33

this cluster internal traffic should not go anywhere near the proxy in general and maybe it was due to miss configured clusterwide proxy that has since been resolved.

you should try removing these proxy vars cause vault and the init job does not need to access the internet so in theory they should not need to have the proxy vars and it can possibly cause other issues

