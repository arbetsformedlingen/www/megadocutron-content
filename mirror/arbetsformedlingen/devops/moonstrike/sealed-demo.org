
* briefly
   34  helm repo add sealed-secrets https://bitnami-labs.github.io/sealed-secrets
   35  helm install sealed-secrets -n kube-system --set-string fullnameOverride=sealed-secrets-controller sealed-secrets/sealed-secrets

   41  go install github.com/bitnami-labs/sealed-secrets/cmd/kubeseal@main
   43  mv ~/go/bin/kubeseal ~/bin/
   46      kubectl create secret generic secret-name --dry-run=client --from-literal=foo=bar -o yaml |     kubeseal       --controller-name=sealed-secrets-controller       --controller-namespace=kube-system       --format yaml > mysealedsecret.yaml   
kubectl create -f mysealedsecret.yaml
   50  kubectl get secret secret-name -o json
   51  base64 -d YmFy
   52  echo YmFy |base64 -d 


* more verbose

23:42:30-joakim@tanaka:~/jobtech/ocpsinglenode$ helm repo add sealed-secrets https://bitnami-labs.github.io/sealed-secrets
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /mnt/big/home/joakim/roles/jobtech/ocpsinglenode/config/auth/kubeconfig
"sealed-secrets" has been added to your repositories
16:34:43-joakim@tanaka:~/jobtech/ocpsinglenode$ helm install sealed-secrets -n kube-system --set-string fullnameOverride=sealed-secrets-controller sealed-secrets/sealed-secrets
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /mnt/big/home/joakim/roles/jobtech/ocpsinglenode/config/auth/kubeconfig
W1010 16:35:37.054184  776775 warnings.go:70] would violate PodSecurity "restricted:latest": allowPrivilegeEscalation != false (container "controller" must set securityContext.allowPrivilegeEscalation=false), seccompProfile (pod or container "controller" must set securityContext.seccompProfile.type to "RuntimeDefault" or "Localhost")
NAME: sealed-secrets
LAST DEPLOYED: Tue Oct 10 16:35:29 2023
NAMESPACE: kube-system
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
** Please be patient while the chart is being deployed **

You should now be able to create sealed secrets.

1. Install the client-side tool (kubeseal) as explained in the docs below:

    https://github.com/bitnami-labs/sealed-secrets#installation-from-source

2. Create a sealed secret file running the command below:

    kubectl create secret generic secret-name --dry-run=client --from-literal=foo=bar -o [json|yaml] | \
    kubeseal \
      --controller-name=sealed-secrets-controller \
      --controller-namespace=kube-system \
      --format yaml > mysealedsecret.[json|yaml]

The file mysealedsecret.[json|yaml] is a commitable file.

If you would rather not need access to the cluster to generate the sealed secret you can run:

    kubeseal \
      --controller-name=sealed-secrets-controller \
      --controller-namespace=kube-system \
      --fetch-cert > mycert.pem

to retrieve the public cert used for encryption and store it locally. You can then run 'kubeseal --cert mycert.pem' instead to use the local cert e.g.

    kubectl create secret generic secret-name --dry-run=client --from-literal=foo=bar -o [json|yaml] | \
    kubeseal \
      --controller-name=sealed-secrets-controller \
      --controller-namespace=kube-system \
      --format [json|yaml] --cert mycert.pem > mysealedsecret.[json|yaml]

3. Apply the sealed secret

    kubectl create -f mysealedsecret.[json|yaml]

Running 'kubectl get secret secret-name -o [json|yaml]' will show the decrypted secret that was generated from the sealed secret.

Both the SealedSecret and generated Secret must have the same name and namespace.

* summary
its fairly easy to get started with sealed secrets.

some questions:
- Q: is the sealed privkey the same or different for each ns?
  A: the ns is appended to the key during encryption, so it can only
  be decrypted in the same ns (roughly speaking)
- how are sealed privkey backed up? or should they even be?

