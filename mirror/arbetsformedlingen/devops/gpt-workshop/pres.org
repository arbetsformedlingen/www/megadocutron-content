; org-display-inline-images


 Erfarenheter av LLM (av praktiska skäl i form av OpenAI GPT)
 - <2023-09-01 Fri>

 [[./media/hacker.jpeg]]

* Intro
 - General Pre-trained Transformer (GPT)
 - OpenAI använder Reinforcement Learning from Human Feedback (RLHF)
   för att få modellen att svara i linje med allmänna mänskliga värderingar.
 - LLM är ett genombrott inom AI och en stark trend - viktig omvärldsbevakning för JobTech.

** Varför ens testa OpenAI?

Q: OpenAI producerar inte open source, så vad spelar det för roll för oss?

A: OpenAI levererar en tjänst som gör det enkelt att experimentera
utan egen specialiserad hårdvara. Lärdomarna kan generaliseras till
LLM och användas till att använda open sourcemodeller i ett senare
skede när vi har egen hårdvara.

** Finns LLM som open source?
Ja. Sök på t ex Falcon och Llama2. Kräver dock att du har GPU att köra på.
Grafikkort med minst 48G VRAM verkar vara det rekommenderade just nu, men
med diverse (jobbiga) trick kan det gå att få ned, kanske.

** Hur får man GPU?
 - vast.ai: realtidsmarknad för GPU-sekunder - sök eller ge bud, mycket bra priser
 - petals.dev: kör jobb distribuerat på en pool av GPU-er, lite som bittorrent fast för GPU-jobb
 - runpod.io: ett GPU-moln, lite som AWS EC2
 - flera alternativ finns

** Men kan inte AF/JobTech tillhandahålla GPU-resurser till utvecklarna?
Jo, det borde gå. Det pågår diskussioner med AI-center bl a.

Jonas: "köp inga resurser utan tydligt mål"


* Lite experiment på JobTech

Resten av denna session är mestadels praktisk.

** Demo
[[./media/melt.jpg]][[./media/angry2.jpg]]


** När den fundamentala modellen inte räcker till
 alt 1) tillhandahåll informationskällor i prompten (SSYKGPT)

 alt 2) konstruera en resonerande agent och låt LLM konstruera anrop
 till verktyg/datakällor/APIer etc, och returnera varje svar till
 resonemangskedjan

 alt 3) finetuna den fundamentala modellen

 ...flera alternativ

** Mindhammer
En LLM-bot i Mattermost.
[[file:/home/perweij/proj/mindhammer/src/mmpy_bot/plugins/aibot.py][mindhammer]]

** Programmeringsassistans
Finns en uppsjö av användbara IDE-integrationer, AI Coding Assistant etc.
