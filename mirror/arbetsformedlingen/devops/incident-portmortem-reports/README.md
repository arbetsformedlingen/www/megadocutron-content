---
title: Incident post mortem reporting
gitlaburl: https://gitlab.com/arbetsformedlingen/devops/incident-portmortem-reports/-/blob/main//README.md
gitdir: /arbetsformedlingen/devops/incident-portmortem-reports
gitdir-file-path: /README.md
date: '2023-10-25 21:11:04'
path: /arbetsformedlingen/devops/incident-portmortem-reports/README.md
tags:
- README.md
---
# Incident post mortem reporting
Following an incident, it is useful to document what happened, analyse
the causes and think about how it can be avoided in the future. This
is usually done in a post mortem report.

## Access existing reports
Follow the link *Incident Post Mortem Reports* on [this page](https://gitlab.com/arbetsformedlingen/devops/incident-portmortem-reports/-/releases).

## Add a new report
1. Use one of the existing reports in this repo as template, and
   create a new report file, using the same file naming convention.
2. Write the report, and commit it to the repo, then push.
3. The PDF file will be automatically built by a CI job and
   published. Use the link above when the pipeline is ready.

## Contribute
Yes please, make a merge request.

## License
The very little code in this repo is licensed under GPLv3.
