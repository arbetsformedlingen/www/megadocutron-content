---
title: script-container
gitlaburl: https://gitlab.com/arbetsformedlingen/devops/script-container/-/blob/main//README.md
gitdir: /arbetsformedlingen/devops/script-container
gitdir-file-path: /README.md
date: '2023-10-13 12:44:26'
path: /arbetsformedlingen/devops/script-container/README.md
tags:
- README.md
---
# script-container

Builds a container image with utils often used in scripts within Jobtech. 
Such as curl, git, and minio client.

Workingdir and homedir is `/tmp/`.

Command to get a pod up and running and get the prompt:

```shell
kubectl run -it debug --image=docker-images.jobtechdev.se/script-container/script-container:latest --image-pull-policy=Always
```

