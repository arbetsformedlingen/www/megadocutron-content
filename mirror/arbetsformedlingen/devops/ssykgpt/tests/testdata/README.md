---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/devops/ssykgpt/-/blob/main//tests/testdata/README.md
gitdir: /arbetsformedlingen/devops/ssykgpt
gitdir-file-path: /tests/testdata/README.md
date: '2023-10-18 10:48:26'
path: /arbetsformedlingen/devops/ssykgpt/tests/testdata/README.md
tags:
- tests::testdata::README.md
- testdata::README.md
- README.md
---
input_joblinks.1
input_joblinks.10
	INPUT: add_jobad_enrichments_api_results
	Data fetched here:
	https://gitlab.com/arbetsformedlingen/joblinks/sample-processing-data/-/blob/master/c871f4c/add_jobad_enrichments_api_results.out.gz


input.1
input.10
	Data fetched here:
	https://data.jobtechdev.se/annonser/historiska/berikade/kompletta/2022_beta1_jsonl.zip
