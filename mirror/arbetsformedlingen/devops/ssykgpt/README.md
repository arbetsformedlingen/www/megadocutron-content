---
title: About SSYKGPT
gitlaburl: https://gitlab.com/arbetsformedlingen/devops/ssykgpt/-/blob/main//README.md
gitdir: /arbetsformedlingen/devops/ssykgpt
gitdir-file-path: /README.md
date: '2023-10-18 10:48:26'
path: /arbetsformedlingen/devops/ssykgpt/README.md
tags:
- README.md
---
<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>

[![Arbetsförmedlingens logo](https://arbetsformedlingen.se/webdav/files/logo/logo.svg)](https://arbetsformedlingen.se)

## About SSYKGPT

This program uses the [OpenAI Chat Completion API](https://platform.openai.com/docs/guides/chat) to classify Swedish job ads
according to [SSYK 2012 four digit sub group](https://www.scb.se/dokumentation/klassifikationer-och-standarder/standard-for-svensk-yrkesklassificering-ssyk/).

The policy of the Swedish Public Employment Service's investment in [JobTech Development](https://jobtechdev.se) is to
use open source software, which OpenAI does not provide. We are however investigating
GPT as a means to produce training data, which in turn can be used to produce open source models
and software.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


## Method
The system uses four inputs:

 0. A jsonl file with Swedish job ads, like [this](tests/testdata/input.1).
 1. A [system prompt](https://gitlab.com/arbetsformedlingen/devops/ssykgpt/-/blob/master/src/ssykgpt/cli.py#L35): "Du är SSYKGPT, en språkmodell som analyserar en jobbannons och tilldelar den en yrkeskod i SSYK-standard. Ditt svar skall endast bestå av en fyrställig SSYK-kod.". This is the general instruction to ChatGPT.
 2. A [minor query prompt](https://gitlab.com/arbetsformedlingen/devops/ssykgpt/-/blob/master/src/ssykgpt/cli.py#L36) to indicate the beginning of the ad text.
 3. An [SSYK code table](data/code+labels.txt). This provides both SSYK codes and their respective labels to the system.

A problem which had to be solved was that the currently available OpenAI
model `gpt-3.5-turbo` only has a context window of 4k tokens, which exceeds the
sum of the tokens in the input described above.

We solved this problem by partitioning the SSYK table into multiple parts,
and in a first run letting GPT classify the ad based on the codes in each part.
Then, in a final step, GPT has to choose one code from the results of the
preliminary code selections based on the parts, where the whole SSYK code table has
been considered.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

Python 3.11 and [Python Poetry](https://python-poetry.org/docs/#installation).

Optionally, also install [GNU Make](https://www.gnu.org/software/make).

### Installation

0. Clone this repo:
```sh
git clone https://gitlab.com/arbetsformedlingen/devops/ssykgpt.git
```
1. Create and copy your [OpenAI API Key](https://platform.openai.com/account/api-keys) into a file in the project root folder, called `.env`:
```
OPENAI_API_KEY=sk-...
```
2. Install the dependencies
```sh
poetry config experimental.new-installer false
poetry install
```

<p align="right">(<a href="#readme-top">back to top</a>)</p>



## Usage

Run the provided test job ads (you can download more ad data [here](https://data.jobtechdev.se/annonser/historiska/berikade/kompletta/)):
```sh
poetry run ssykgpt < tests/testdata/input.1 2>log.txt | tee result.jsonl
```
The results are inserted in the json structures as attributes `ssykgpt` and
`ssykgpt_taxonomy_label`:

```sh
jq '{ desc: .description.text, ssykgpt: .ssykgpt, ssykgpt_taxonomy_label: .ssykgpt_taxonomy_label }' < result.jsonl
{
  "desc": "Välkommen till ett snabbt växande bemanningsföretag med hjärtat på rätt ställe och fokus på rätt saker. På SjukvårdsMäklarna har vi en gedigen erfarenhet av att bemanna sjukvårdspersonal, i både Sverige och Norge. Tillsammans har vi ett brett kontaktnät med såväl vårdgivare som personal. Samtliga anställda hos SjukvårdsMäklarna Sverige AB har goda personliga referenser genom tidigare bemanningsverksamhet inom vård och omsorg, företagsutveckling & ledning. Sjukvårdsmäklarna söker en sjuksköterska till ett uppdrag inom hemsjukvård i Finspång. Period och uppdrag\n\nUppdraget startar 2023-03-01 och pågår till 2023-08-31 Tjänstgöring som sjuksköterska med sedvanliga uppgifter inom hemsjukvård. Ansök senast 2023-02-27. Du behöver ha minst två års erfarenhet som sjuksköterska samt relevant erfarenhet",
  "ssykgpt": "2221",
  "ssykgpt_taxonomy_label": "Grundutbildade sjuksköterskor"
}
```


Get help:
```
poetry run ssykgpt -h
Usage: ssykgpt [OPTIONS]

Options:
  -R, --reiterate_sysprompt_interval INTEGER
                                  Resend the system prompt at discussion
                                  intervals.
  --log                           Enable logging
  -f, --sessionfile TEXT          A session file to store traffic.
  -l, --desiredresponselength INTEGER
                                  The desired length of the response.
  -k, --api_key TEXT              API key.
  -t, --openaitemp FLOAT          Model temperature.
  -r, --modelmaxresponse INTEGER  The chosen models max context.
  -m, --openaimodel TEXT          Specify Chat Completion OpenAI model.
  -q, --queryprompt TEXT          Use to prefix prompts.
  -Q, --bigsearchdatafilterrx TEXT
                                  A regex capturing what's important.
  -b, --bigsearchfile TEXT        When searching for data in a set larger than
                                  modelmax, use this.
  -S, --syspromptfile TEXT        The system prompt as a file. Overrides
                                  --sysprompt
  -s, --sysprompt TEXT            The system prompt.
  -h, --help                      Show this message and exit.
```

<p align="right">(<a href="#readme-top">back to top</a>)</p>


## Developing

Some poetry shortcuts are packaged in a Makefile for convenience.

Run `make` for help

    make install             # Run `poetry install`
    make showdeps            # run poetry to show deps
    make lint                # Runs bandit and black in check mode
    make format              # Formats you code with Black
    make test                # run pytest with coverage
    make build               # run `poetry build` to build source distribution and wheel
    make pyinstaller         # Create a binary executable using pyinstaller



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- LICENSE -->
## License

Distributed under the GPLv3 License. See `LICENSE` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTACT -->
## Contact
Developed at the Swedish Public Employment Service's investment in [JobTech Development](https://jobtechdev.se).

Team contact: [calamari@arbetsformedlingen.se](calamari@arbetsformedlingen.se)

Per Weijnitz  - [per.weijnitz@arbetsformedlingen.se](per.weijnitz@arbetsformedlingen.se)

Joakim Verona - [joakim.verona@arbetsformedlingen.se](joakim.verona@arbetsformedlingen.se)

Project Link: [https://gitlab.com/arbetsformedlingen/devops/ssykgpt](https://gitlab.com/arbetsformedlingen/devops/ssykgpt)

<p align="right">(<a href="#readme-top">back to top</a>)</p>
