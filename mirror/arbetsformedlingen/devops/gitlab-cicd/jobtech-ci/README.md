---
title: JobTech CI
gitlaburl: https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/blob/main//README.md
gitdir: /arbetsformedlingen/devops/gitlab-cicd/jobtech-ci
gitdir-file-path: /README.md
date: '2023-11-08 10:55:35'
path: /arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/README.md
tags:
- README.md
---
[![Arbetsförmedlingens logo](https://arbetsformedlingen.se/webdav/files/logo/logo.svg)](https://arbetsformedlingen.se)


# JobTech CI

This program is used for builds and deploys, and replaces Aardvark at
JobTech.

It uses the Gitlab CI/CD functionality.

[![Bild på versionsnummer](https://img.shields.io/badge/dynamic/json?color=green&label=Version%20%28commit%20hash%29&query=%24%5B0%5D.short_id&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F43503997%2Frepository%2Fcommits%3Fref_name%3Dmaster)](https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/commits/master)

[The changelog](CHANGELOG.md)

The version of JobTech CI is bumped at the team's discretion.


## Primarily intended users

The primarily intended users are the developers at
[JobTech](https://gitlab.com/arbetsformedlingen), but we welcome
forking and participation from other places too.

If you are a Gitlab user who use Openshift/Kubernetes with your
configuration in the [kustomize](https://kustomize.io/) style (with a
`base` and `overlays` structure), you could probably have use for
this.


## Configuration instruction for developers

Use these version according to your needs:

Stable: v0.3.9

Testing: v0.3.10



 1. Remove any Aardvark webhooks from your application repo (and infra repo, if applicable).

 2. Verify that `Enable shared runners for this project` is active in the project Settings -> CI/CD -> Runners section.

 3. Add a file `.gitlab-ci.yml` in the root folder of the **application repo** (NOT infra-repo) with this content:
 ```
include:
  remote: 'https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/raw/v0.3.10/jobtech-ci.yml'
```
Please note the version tag in the URL - there you can decide to use
different versions of JobTech CI for your project.

You can add more Gitlab CI to this file if you wish. Please make sure
you understand how common settings are merged of you do so.

### Adding support for a system specific overlay

If the available deploy buttons are not sufficient, you can add more in your local `.gitlab-ci.yml`. Here is an example of a project's `.gitlab-ci.yml` which first includes the global CI file, then adds an extra deploy button called `deploy_internal_test`:

```
include:
  remote: 'https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/raw/v0.3.10/jobtech-ci.yml'

deploy_internal_test:
  extends: .deploy
  variables:
    OVERLAY: "internal-test"
```


## Configuration instruction for Gitlab administrators / organisation managers

Create an access token and configure CD/CD with it:
 - Navigate to the organisation top level, Settings -> Access tokens
 - Create a new token with the following settings:
   - Name: jobtech-ci (please use this to facilitate later administration)
   - Role: Maintainer
   - Scopes: write_repository
   - You don't need to have an expiry date, but you may want to.

 - Copy the new token into your your top level organisation Settings -> CI/CD -> Variables,
   create a new variable called `ACCESS_TOKEN`, paste the token from above into the value field,
   and ***IMPORTANTLY check the Masked checkbox***, then save.
   - Protected: selected
   - Expand variable reference: selected


## Usage


### Build an image

When you push to your application repo, the CI pipeline will
start. You can follow its process in the Gitlab GUI, in the repo's
CI/CD pipeline view.

If the image could be built, it will be pushed to nexus with the
following tags: `latest`, the short commit sha and the branch name.

If the app repo has a corresponding infra repo, its short commit sha
will be written to your application's infra repo, the develop overlay.

To deploy to your various environments that you have defined in your
infra overlays, press the corresponding deploy buttons in the CD/CD pipeline
view.


![Gitlab pipeline view](/img/pipeline.png "View of the deploy buttons")


## License

GPL v3 - see the file in the root directory of the repo.


## By

JobTech, a part of The Swedish Public Employment Agency.



## TODO
 - speed optimisations, for example determining when it's necessary to actually pull
   tool images from remote repos.
