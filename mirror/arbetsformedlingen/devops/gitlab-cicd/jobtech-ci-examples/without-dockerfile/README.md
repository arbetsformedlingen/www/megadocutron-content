---
title: Packaging an application without a Dockerfile
gitlaburl: https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci-examples/without-dockerfile/-/blob/main//README.md
gitdir: /arbetsformedlingen/devops/gitlab-cicd/jobtech-ci-examples/without-dockerfile
gitdir-file-path: /README.md
date: '2023-11-02 17:30:01'
path: /arbetsformedlingen/devops/gitlab-cicd/jobtech-ci-examples/without-dockerfile/README.md
tags:
- README.md
---
# Packaging an application without a Dockerfile

Images can be built without a Dockerfile. This is done using [Cloud
Native Buildpacks](https://buildpacks.io/). They try to detect which
build system you are using (Maven, Pip, ...), and if it matches a
known buildpack pattern, it applies that build pack to produce an
image.

Please note that images are pushed to the image repository of the
Gitlab's project instead of JobTech's Nexus. Make sure you refer to
this repository in your infra files, not Nexus. You find the
repository in your Gitlab project under Deploy -> Container registry.

## Configure CI
Create a `.gitlab-ci.yml` with the following content:
```
include:
 - remote: 'https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/raw/feature/autodevops/jobtech-ci.yml'
```

If you would like to do further configuration, such as specifying the equivalent of a Dockerfile CMD,
check out how to write a [Procfile](https://devcenter.heroku.com/articles/procfile). You can also do lots of stuff with a [project descriptor](https://buildpacks.io/docs/app-developer-guide/using-project-descriptor/).

You can also [customise Gitlab AutoDevops](https://docs.gitlab.com/ee/topics/autodevops/customize.html) including the [audo-builder](https://docs.gitlab.com/ee/topics/autodevops/stages.html#auto-build-using-cloud-native-buildpacks) and [custom buildpacks](https://docs.gitlab.com/ee/topics/autodevops/customize.html#custom-buildpacks).


## Run built image locally
The link to the build image can be copied from your Gitlab repo, Deploy -> Container registry.

Run the packaged application using the built image:
```
podman run --rm -it registry.gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci-examples/without-dockerfile/main
```

## About Python Poetry
The development of a Poetry buildpack is still ongoing. Track the progress here:
 - https://github.com/heroku/heroku-buildpack-python/issues/796
 - https://github.com/heroku/buildpacks-python/issues/7
 - https://github.com/heroku/heroku-buildpack-python/issues/796
