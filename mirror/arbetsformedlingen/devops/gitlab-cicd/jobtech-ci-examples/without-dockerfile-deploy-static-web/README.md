---
title: Integrated build and deploy of a web app within Gitlab
gitlaburl: https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci-examples/without-dockerfile-deploy-static-web/-/blob/main//README.md
gitdir: /arbetsformedlingen/devops/gitlab-cicd/jobtech-ci-examples/without-dockerfile-deploy-static-web
gitdir-file-path: /README.md
date: '2023-11-08 16:06:53'
path: /arbetsformedlingen/devops/gitlab-cicd/jobtech-ci-examples/without-dockerfile-deploy-static-web/README.md
tags:
- README.md
---
# Integrated build and deploy of a web app within Gitlab

Here, we use an existing web app called [Education Frontend
App](https://gitlab.com/arbetsformedlingen/education/education-frontend-app/)
to show how it can be converted from [being served using
Nginx](https://gitlab.com/arbetsformedlingen/education/education-frontend-app/-/blob/eb4672b05c1e0f7d51a5b4c0b99b0cd9bf71e224/Dockerfile)
in Openshift into being served from the builtin web server in Gitlab.

Everything is defined in [.gitlab-ci.yml](.gitlab-ci.yml). Please note that the last part specifies that the pipeline
will only run when stuff is pushed to the default branch (`main` most commonly).

## Conversion process
 1. Remove the [Dockerfile](https://gitlab.com/arbetsformedlingen/education/education-frontend-app/-/blob/eb4672b05c1e0f7d51a5b4c0b99b0cd9bf71e224/Dockerfile).
 2. Add the [.gitlab-ci.yml](.gitlab-ci.yml)


Once you have pushed these changes to Gitlab (the default branch), and
the pipeline has finished, you can browse the web page from the url
shown under [Deploy ->
Pages](https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci-examples/without-dockerfile-deploy-static-web/pages)
in your Gitlab repo.

For this demo repo, that url is: https://without-dockerfile-deploy-static-web-arbetsforme-bd5d7119599d6f.gitlab.io/

## Caveats
Gitlab only provides one web server per project. This means that you cannot have one web server per branch/environment.

Here is one workaround. Since Git is built to be decentralised and supports multiple remotes, you can fork your main
repo using the `fork`-button in the top left corner of your Gitlab project, and create a repo for a second environment,
which will have its own web server. Since it's a fork, Gitlab will help you with tracking the differences, and it's
easy to merge stuff from for example a forked off test repo into its upstream repo.
