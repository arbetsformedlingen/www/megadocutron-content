* Infratron

Infratron is a file within a code repository that operates GitLab CI,
creating an infrastructure repository. The infratron.json file
contains a few straightforward constants; the remaining elements are
developed through convention.

** Origin

The idea of the Infratron has grown over time as we experience
pain-points in our current workflow. It was driven by inputs from the
teams.

Existing solutions are very generic, and can do all sorts of things,
so we dont lack flexibility. Helm, kustomize, even envsubst provides
needed templating fascilities. The problems we have are not so much in
templating, but rather in inheritance, and developer team usage.

- In our experience kustomice works best with argocd, but even that
combination can sometimes be flaky. Therefore we dont want to
complicate argocd.

- Its problematic to mix and match snippets of code
to a coherent kustomize infra.

- sometimes we need great flexibility, and sometimes we do
  not. Currently all projects need to pay the price in complexity that
  kustomize offers, instead of just a few.

A couple of examples to clarify:  

#+begin_src
intratron.json -> gitlab-ci -> helm ->                infrarepo -> argocd -> openshift deploy
intratron.json -> gitlab-ci -> bash/perl/make hack -> infrarepo -> argocd -> openshift deploy
intratron.json -> gitlab-ci -> dhall ->               infrarepo -> argocd -> openshift deploy
                                                      infrarepo -> argocd -> openshift deploy
intratron.json -> gitlab-ci direct deploy                                 -> openshift deploy

#+end_src

a project is always deployed with a kustomize infrarepo, by argocd
a project might or might not have infratron.json. If it has, infrarepo
is generated, otherwise manually maintained, whcih is the situation
today.

But one could also presumably skip the infra repo and argocd entirely
and just use gitlab ci for the deploy.


** The Infratron Workflow

Infratron is designed to accommodate multiple generators, providing
valuable flexibility. It doesn't matter what template generator you
use - Infratron can support multiple. 

** Stages

Infratron works based on the following common stages, which form the fundamental workflow:

- GitLab CI: Where you define the build and deployment process.
- Helm/Bash/perl/make hack/ Dhall: Generates the templated
  infrastructure code, in the form of a conventional kustomize infra repo
- Infrastructure Repository: The generated code resides here.
- ArgoCD: Implements the infrastructure code into Kubernetes.

** Infratron.json

The infratron.json file contains:

- The template identifier, which also implies the template generator.
- Values for the template, such as memory size, routes etc.

If you wish to stop using Infratron, you can merely remove the
infratron.json file and directly edit the generated infrastructure
repository.

** Settings for different clusters

One noteworthy feature is the possibilities of handling different
cluster types more comfortably. That is:

- Including standard settings for proxies if needed
- Variants for different cluster types, like micro-k8s, aws eks, etc
  



** Future Considerations

Moving forward, the project will focus on improving control,
simplicity, flexibility, and robustness. Feedback and contributions
are welcome.

** notes
https://blog.container-solutions.com/using-helm-and-kustomize-to-build-more-declarative-kubernetes-workloads

https://github.com/Spazzy757/helm-to-kustomize

example toolchains
intratron.json -> gitlab-ci -> helm -> infrarepo -> argocd
intratron.json -> gitlab-ci -> bash/perl/make hack -> infrarepo -> argocd
intratron.json -> gitlab-ci -> dhall -> infrarepo -> argocd

