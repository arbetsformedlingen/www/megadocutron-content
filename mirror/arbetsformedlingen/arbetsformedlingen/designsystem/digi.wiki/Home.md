---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/designsystem/digi.wiki/-/blob/main//Home.md
gitdir: /arbetsformedlingen/arbetsformedlingen/designsystem/digi.wiki
gitdir-file-path: /Home.md
date: '2023-05-08 10:26:02'
path: /arbetsformedlingen/arbetsformedlingen/designsystem/digi.wiki/Home.md
tags:
- Home.md
---
Digi är ett monorepo för att gemensamt kunna utveckla ett designsystem. 
Här finns bland annat komponentbibliotek, design tokens, funktionalitet, api:er och annat som hjälper dig att bygga digitala tjänster som följer våra gemensamma riktlinjer. Repot är ett monorepo genererat med [Nx](https://nx.dev).