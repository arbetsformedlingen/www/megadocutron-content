---
title: 'Ansvar, tillit och hållbarhet: dataetik'
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/etik/hallbarhet.wiki/-/blob/main//Ansvarsfull-och-hållbar-teknikutveckling.md
gitdir: /arbetsformedlingen/arbetsformedlingen/etik/hallbarhet.wiki
gitdir-file-path: /Ansvarsfull-och-hållbar-teknikutveckling.md
date: '2023-06-09 10:01:04'
path: /arbetsformedlingen/arbetsformedlingen/etik/hallbarhet.wiki/Ansvarsfull-och-hållbar-teknikutveckling.md
tags:
- Ansvarsfull-och-hållbar-teknikutveckling.md
---
# Ansvar, tillit och hållbarhet: dataetik 

## Är vår teknikutveckling ansvarsfull? 

Ansvarsfull teknikutveckling är att tillämpa ett etiskt förhållningssätt vid utveckling, användning och spridning av ny teknik och att bidra till ett miljömässigt, socialt och ekonomiskt hållbart samhälle. Social hållbarhet ur Arbetsförmedlingens synvinkel: 

Social hållbarhet handlar om allas lika rätt till ett gott liv. Det innebär ett liv fritt från diskriminering, med god hälsa och möjlighet att påverka sin egen livssituation utifrån egna förutsättningar. 

## Vad gör vi för att bidra till ansvarsfull teknikutveckling och social hållbarhet? 

Pågående projekt:

 • Kvalitetssäkrade och testade dataset 

• Utökad metadata/dokumentation

 • Etiska utvärderingar 

• Hållbar matchning 

### Syna oss och bidra till mer tillitsfull teknik! 

Vi tror att samskapande och öppenhet leder till bättre och mer tillitsfull teknikutveckling. Har du någon synpunkt eller upptäckt någon etisk tveksamhet bland Arbetsförmedlingens tjänster och dataset? Skapa ett issue, använd **label** ”ethics” och den label som refererar till den tjänst/dataset din kommentar berör.