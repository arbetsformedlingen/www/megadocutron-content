---
title: '**<span dir="">Ansvar, tillit och hållbarhet</span>**'
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/etik/hallbarhet.wiki/-/blob/main//home.md
gitdir: /arbetsformedlingen/arbetsformedlingen/etik/hallbarhet.wiki
gitdir-file-path: /home.md
date: '2023-06-09 10:01:04'
path: /arbetsformedlingen/arbetsformedlingen/etik/hallbarhet.wiki/home.md
tags:
- home.md
---
# **<span dir="">Ansvar, tillit och hållbarhet</span>**

**<span dir="">Ansvarsfull teknikutveckling och dataetik</span>**

## <span dir="">Är vår teknikutveckling ansvarsfull?</span>

<span dir="">Ansvarsfull teknikutveckling är att tillämpa ett etiskt förhållningssätt vid utveckling, användning och spridning av ny teknik och att bidra till ett miljömässigt, socialt och ekonomiskt hållbart samhälle.</span>

**<span dir="">Social hållbarhet ur Arbetsförmedlingens synvinkel:</span>**

<span dir="">Social hållbarhet handlar om allas lika rätt till ett gott liv. Det innebär ett liv fritt från diskriminering, med god hälsa och möjlighet att påverka sin egen livssituation utifrån egna förutsättningar.</span>[<span dir="">\[1\]</span>](#\_ftn1)

<span dir="">Vad gör vi för att bidra till ansvarsfull teknikutveckling och social hållbarhet?</span>

<span dir="">Pågående projekt:</span>

* <span dir="">Kvalitetssäkrade och testade dataset</span>
* <span dir="">Utökad metadata/dokumentation</span>
* <span dir="">Etiska utvärderingar</span>
* <span dir="">Hållbar matchning</span>

**<span dir="">Syna oss och bidra till mer tillitsfull teknik!</span>**

<span dir="">Vi tror att samskapande och öppenhet leder till bättre och mer tillitsfull teknikutveckling. Har du någon synpunkt eller upptäckt någon etisk tveksamhet bland Arbetsförmedlingens tjänster och dataset? Skapa ett issue, använd **label** ”ethics” och den label som refererar till den tjänst/dataset din kommentar berör.</span>

<div>


---

<div>

[<span dir="">\[1\]</span>](#\_ftnref1) [Social hållbarhet | Intranätet (](https://start.arbetsformedlingen.se/var-myndighet/verksamhet/hallbarhet-och-agenda-2030/social-hallbarhet/)arbetsformedlingen.se[)](https://start.arbetsformedlingen.se/var-myndighet/verksamhet/hallbarhet-och-agenda-2030/social-hallbarhet/)

</div>
</div>