---
title: Etisk AI - Vad är på gång?
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/etik/etisk-ai.wiki/-/blob/main//home.md
gitdir: /arbetsformedlingen/arbetsformedlingen/etik/etisk-ai.wiki
gitdir-file-path: /home.md
date: '2023-06-09 09:57:12'
path: /arbetsformedlingen/arbetsformedlingen/etik/etisk-ai.wiki/home.md
tags:
- home.md
---
## Etisk AI - Vad är på gång?

### Tips:

Etiska utvärderingar:

* [The Ethics Canvas](https://ethicscanvas.org/)
* [Toolkit: The Digital Ethics Compass (](https://ddc.dk/tools/toolkit-the-digital-ethics-compass/)ddc.dk[)](https://ddc.dk/tools/toolkit-the-digital-ethics-compass/)
* https://www.kometinfo.se/vart-arbete/ansvarsfull-teknikutveckling/verktyget-ansvarsfullteknik-se/

Läs mer om förtroendemodellen:

<div>

[<span dir="">Offentlig AI - Sveriges Dataportal</span>](https://beta.dataportal.se/offentligai)

## AI Act - Vilka krav ställer förordningen på högrisk-AI?

High-risk AI systems will be subject to strict obligations before they can be put on the market:

* adequate risk assessment and mitigation systems;
* high quality of the datasets feeding the system to minimise risks and discriminatory outcomes;
* logging of activity to ensure traceability of results;
* detailed documentation providing all information necessary on the system and its purpose for authorities to assess its compliance;
* clear and adequate information to the user;
* appropriate human oversight measures to minimise risk;
* high level of robustness, security and accuracy.

  (Läs mer: [Regulatory framework proposal on artificial intelligence | Shaping Europe’s digital future (](https://digital-strategy.ec.europa.eu/en/policies/regulatory-framework-ai)europa.eu[)](https://digital-strategy.ec.europa.eu/en/policies/regulatory-framework-ai))

</div>