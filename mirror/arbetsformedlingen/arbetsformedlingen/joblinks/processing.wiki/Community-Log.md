---
title: Community Backog
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/joblinks/processing.wiki/-/blob/main//Community-Log.md
gitdir: /arbetsformedlingen/arbetsformedlingen/joblinks/processing.wiki
gitdir-file-path: /Community-Log.md
date: '2022-05-26 20:24:19'
path: /arbetsformedlingen/arbetsformedlingen/joblinks/processing.wiki/Community-Log.md
tags:
- Community-Log.md
---
Welcome to our **Community Backog!**

[Back](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/HOME-ENG) - [Tillbaka](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home) 
 
This page is for those of you who would like to contribute to **JobTech Links**: an enriched data file in the project Ecosystem for Ads. Arbetsförmedlingen (The Swedish unemployment agency) is collaborating with private owned Job boards and Statistics Sweden (SCB) in order to get better statistics of the labour market and better user search experience.
 
**Short description of what we are aiming for**

1. Better statistics - Possible with more data from many actors
2. Better user experience - Possible to build aggregated job search with the API. 

**Our intention**

The intention is to have both internal and external contributors in the project where we allow distributed  and  asynchronous work, a first good step towards an open source community. To simplify the processes with working on the same code base we use a pipe and filter architecture.

We need help. An important use case is to help job seekers that have been out of a job for a longer period of time. When we have around 75 000 ads, how can we present those that a relevant for this segment. Ads are unstructured data. We need help using text analytics to produce metadata for the necessary search and filtering parameters. With AI we think it possible to collect data from different sources and structure it for a better job seeker experience.

# Community Backog

Many of the issues in the community backlog concerns AI and data analysis. We need the following metadata for search and filter

- [ ] Jobs without education requirements
- [ ] Jobs without a driver's license requirement
- [ ] Jobs without previous work experience
- [ ] Duration (tillvidareanställning eller visstidsanställning)
- [ ] 
- [ ] Other? (Please come with suggestions)
- [ ] Translation of  skills taxonomy from Swedish to English.
- [ ] Visualize statistics in new simple ways, the official statistics are based on how many ads there are per occupation and municipality. - - [ ] Ort (Place). Is it possible to get a better match/hit when it comes to municipality?
- [ ] General feedback - We want to be open. But are we doing the right thing. Is it possible to understand our source code, project and the data.


[To our issue tracker.](https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/issues) Please look for issues labeled Community Log.

## Source code
https://gitlab.com/arbetsformedlingen/joblinks

## Manual for creating a step in our pipeline
 
Job Ad Processing with the JobTech Links Pipeline
https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/blob/develop/docs/pres-fossnorth2021.org

## License
Check the file LICENSE in every repository. For most source code we use Apache 2. The data is licensed with CC0.