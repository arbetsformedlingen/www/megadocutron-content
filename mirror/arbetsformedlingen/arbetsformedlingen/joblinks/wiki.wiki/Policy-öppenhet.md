---
title: JobTechs policy om öppenhet
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/-/blob/main//Policy-öppenhet.md
gitdir: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki
gitdir-file-path: /Policy-öppenhet.md
date: '2022-04-28 11:18:43'
path: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Policy-öppenhet.md
tags:
- Policy-öppenhet.md
---
# JobTechs policy om öppenhet
Denna policy omfattar alla projekt som bedrivs av enheten JobTech som är en del av Arbetsförmedlingen. JobTech tillgängliggör öppna data och möjliggöra användande av denna data. Policyn beskriver övergripande de anställda på enheten JobTech ställningstagande om öppenhet gentemot sina användare. Inspiration kommer från Debian Social Contract. Kommentarer är välkomna. Ni hittar våran communitysida [här](https://stage.jobtechdev.se/community/).  
 
**Sammanfattning**
All källkod delas och inget döljs för användarna. Vi förbinder oss vidare att göra det enkelt att bidra och använda den öppna infrastrukturen. 
 
### Skattefinansiering förpliktigar delning 
Resultatet av Jobtechs verksamhet kommer flera till gagn om det är tillgängligt och öppet. Öppen källkod är bra för de som vill bidra till bättre tjänster och för de som vill granska myndighetens verksamhet. Där det finns skäl att begränsa öppenhet ska inte mer än nödvändigt undantas delning.
 
### All källkod är öppen och kommer att förbli öppen
All källkod från delas och inget döljs. Källkoden ska kunna laddas ner, användas, spridas och förändras utan motprestation av alla. Källkoden distribueras under en licens som är godkänd av ”Open Source Initiative” https://opensource.org/licenses. Alla initiativ är öppna för bidrag av källkod eller andra förbättringsförslag från vem som helst. På jobtechdev.se ska det tydligt framgå hur bidrag hanteras. Läs gärna våran contribution policy [här](https://github.com/MagnumOpuses/project-meta/blob/master/CONTRIBUTING_TEMPLATE.md).
 
### Vi döljer inga problem eller planer
Felrapporter och förbättringsförslag kan läsas av vem som helst så fort de blivit rapporterade. Information av värde - ex. dokumentation och roadmap/planer tillgängliggörs tidigt i utvecklingsprojekt.



## Sidor

[Ekosystem i media](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Ekosystem-f%C3%B6r-annonser-i-media)

[Home](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home)

[Om Pågående projekt](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Projektsida-Ekosystem-f%C3%B6r-annonser)

[Referensgrupp med SCB](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Referensgrupp-Ekosystem-f%C3%B6r-annonser)

[Identifiera annonsdubbletter](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Identifiera-annonsdubbletter)

[Pilot API definition](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Pilot-API-definition)

[Till den del av projektet som hanterar berikning av aggregerade annonser.](https://gitlab.com/joblinks/processing/-/wikis/Meta-data-enrichment-process-for-ads)