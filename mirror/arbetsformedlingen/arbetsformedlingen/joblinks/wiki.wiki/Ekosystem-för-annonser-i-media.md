---
title: Sidor
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/-/blob/main//Ekosystem-för-annonser-i-media.md
gitdir: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki
gitdir-file-path: /Ekosystem-för-annonser-i-media.md
date: '2022-04-28 11:18:43'
path: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Ekosystem-för-annonser-i-media.md
tags:
- Ekosystem-för-annonser-i-media.md
---
**AF Pressrelease**

[Pressrelease 15 feb 2021](https://arbetsformedlingen.se/om-oss/press/pressmeddelanden?id=382BA71D5F6B69EE&pageIndex=4&year=2021&uniqueIdentifier=Riket)

[Pressrelease 25 juni 2020](https://arbetsformedlingen.se/om-oss/press/nyheter/nyhetsarkiv/2020-06-26-annonssamarbete-ger-battre-arbetsmarknadsdata)

**Jobtechdev.se**

[Artikel på Jobtechdev.se feb 2021](https://www.linkedin.com/company/jobtech-development/posts/?feedView=all)


**LinkedIn**

https://arbetsformedlingen.se/for-arbetssokande/sa-hittar-du-jobbet/tips-inspiration-och-nyheter/artiklar/2021-02-15-nu-ska-alla-jobbannonser-samlas-pa-samma-stalle

## Sidor

[Ekosystem i media](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Ekosystem-f%C3%B6r-annonser-i-media)

[Home](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home)

[Om Pågående projekt](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Projektsida-Ekosystem-f%C3%B6r-annonser)


[Identifiera annonsdubbletter](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Identifiera-annonsdubbletter)

[Pilot API definition](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Pilot-API-definition)

[Till den del av projektet som hanterar berikning av aggregerade annonser.](https://gitlab.com/joblinks/processing/-/wikis/Meta-data-enrichment-process-for-ads)