---
title: Principles for sustainable collaboration for ads
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/-/blob/main//Project-description.md
gitdir: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki
gitdir-file-path: /Project-description.md
date: '2022-04-28 11:18:43'
path: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Project-description.md
tags:
- Project-description.md
---
The Swedish Public Employment Service (hereafter referred to as Arbetsförmedlingen), together with some of Sweden's largest job-board sites, are collaborating in a pilot project. The project aims to solve one of the major challenges occurring within a fragmented labor market; the difficulties in finding a specific ad when there are several hundred platforms to look through or knowing which job-board site to post an ad on when looking for the right candidate. The collaboration addresses this problem by offering a new dataset, Joblinks. 

Joblinks contains references and metadata linked to the job-ads provided by project-participants. This in turn enables jobseekers easy access to nearly all job-ads as well as generating more traffic to the original source of data. 

**The project aims to provide answers to the following questions:**

1. Is there a sustainable solution in an Ecosystem for job-advertisements?  
  a. on a business level  
  b. on a technical level   
2. As a neutral, public actor what should Arbetsförmedlingen’s role and contribution be?


##  Principles for sustainable collaboration for ads ##
For the ecosystem to be sustainable, the following principles need to be adhered to:

*Back-to-source*: Lead traffic back to the original site in order to respect the source and business model.

*Open Data* - Only job-ad links and metadata are provided as a file and through an open API.

*Open standard*: The result of the scraping will use a standard format (Job posting from schema.org)

*Open Source* – The technology is based on Scrapy and the source code is accessed via Jobtechdev.se

# Project Canvas
![image](uploads/d634b77823cabe1d09580c95c1437d3d/image.png)


## Implementation ##
Arbetsförmedlingen scrapes the various Job-board sites or receives the job-ads though an API. Through an enrichment process, metadata regarding competencies, skills, geographical locations and occupancies are added to the job-ads.

![image](uploads/3803ae95d90463da3357ec545dfb4bcf/image.png)
Parts of the ads (title, URL and the first sentence in the ad) are made available as open data for anyone to use. The reason that only parts of the ad are made available is to preserve the incentives to lead the user back to the source of origin.

To the [Meta data enrichment process for ads](https://gitlab.com/joblinks/processing/-/wikis/Meta-data-enrichment-process-for-ads)

#  Demo

[Linkviewer](https://blog.jobtechdev.se/linkviewer/)


# Timeline ##

![image](uploads/ec0c87b5f71663639662e8135f989a55/image.png)

Since February 15, 2021, Platsbanken is piloting the new Jobtech Links API. On the one hand, the pilot in Platsbanken gives the Job boards an idea of whether the collaboration is sustainable commercially, and on the other hand, users  can evaluate the user interface. The pilot is expected to last 2-3 months where Platsbanken gradually adapts the service to the feedback of users and project partners.

In parallel, a collaboration is ongoing between Statistics Sweden and the Swedish Public Employment Service in order use the new data source in ordinary operations. Statistics Sweden quality assures the technical calculations that form the basis for duplicate management, statistical codes and other metadata.

# Results

In addition to the fact that the new data source with ad links creates better statistics and better decision-making data because the authorities can base their statistics on more ads, the project has led to a better search experience for those looking for work. Job seekers can now get an overview by more external websites presenting their ads in Platsbanken.
Furthermore, the project has found a recipe for private and public collaboration that can be used in other collaborations.

**Criteria for sustainable cooperation**
1.	Reduce collaboration thresholds - Shouldn't cost to collaborate with us
2.	Respect the private sector's business model
3.	Solve common problems together.
4.	Give back: Share transparency and reuse technology.

Right now, about 30% more ads appear in Platsbanken and external websites testify to more traffic as a result of the collaboration. More external actors have contacted us and there is ongoing connection to the ecosystem.


## Participants in the project ##
Blocket

One Partner group

Netjobs
 
Jobbdirekt
 
Careerbuilder
 
Monster
 
Indeed
 
Offentligajob (Visma)
 
Studentjob 

Ingenjörsjobb

## Contact us
For project participation or to learn more about the project, please contact Maria Dalhage, project manager.
[contact the project](mailto:maria.dalhage@arbetsformedlingen.se)

# Pages


[Home eng](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home)

[Identify ad duplicates](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Identifiera-annonsdubbletter)

[Pilot API definition](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Pilot-API-definition)

[Enrichment process.](https://gitlab.com/joblinks/processing/-/wikis/Meta-data-enrichment-process-for-ads)

[The ecosystem in media](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Ekosystem-f%C3%B6r-annonser-i-media)