---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/-/blob/main//Pilot-API-definition.md
gitdir: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki
gitdir-file-path: /Pilot-API-definition.md
date: '2022-04-28 11:18:43'
path: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Pilot-API-definition.md
tags:
- Pilot-API-definition.md
---
**Background**\
The Pilot search API is largely based upon the JobSearch API and made to be compatible for most of the basic queries. This would make it easier to include both API's and result search in a layering API which queries both of them at the same time. 

**Open Questions** 


**Endpoint /id**

|Parameter |Function |
| --- | ---:|
|**/id**| Retrive one ad by hash number|

**Endpoint /joblinks**
 
|Parameter | Function |
| --- | ---:|
|**/occupation-group**| Filtering by occupation-group concept_id added in enrichment pipeline. |
|**/occupation-field**| Filtering by occupation-field concept_id added in enrichment pipeline. |
|**/municipality**| Filtering by municipality concept_id added in enrichment pipeline. |
|**/region**| Filtering by regionconcept_id added in enrichment pipeline. |
|**/country**| Filtering by country concept_id added in enrichment pipeline. |
|**/q**| Freetext query   |
|**/offset**| Offset parameter defines the offset from the first result you want to fetch. |
|**/limit** | Number of hits in result set. |
|**/exclude_source**| Negative filter for one source |


**Example response**

```
{
  "total": {
    "value": 2683
  },
  "hits": [
    {
      "id": "JTL85ec91b8a4bba1f97c911d2ffdf837ee",
      "publication_date': '2020-11-10T08:55:37",
      "source_links":[{"label": "Studentjob.se",
                  "url": "https://www.studentjob.se/lediga-jobb/1332106-ekonomiassistent-i-stockholm"},
                 {"label": "Blocket Jobb",
                  "url": "https://www.blocketjobb.se/lediga-jobb/1363543-ekonomiassistent-i-stockholm"}],
      "headline": "Ekonomiassistent i Stockholm",
      "brief": "Vill du arbeta. På ett bra företag.",
      "employer": {
        "name": "Missar AB"
      },
      "workplace_addresses": [
      {
        "municipality_concept_id": "AvNB_uwa_6n6",
        "municipality": "Stockholm",
        "region_concept_id": "CifL_Rzy_Mku",
        "region": "Stockholms län",
        "country_concept_id": "i46j_HmG_v64",
        "country": "Sverige"
      },
      {
        "municipality_concept_id": "zHxw_uJZ_NJ8",
        "municipality": "Solna",
        "region_concept_id": "CifL_Rzy_Mku",
        "region": "Stockholms län",
        "country_concept_id": "i46j_HmG_v64",
        "country": "Sverige"
      }
      ],
      "occupation_group": {
        "label": null,
        "concept_id": null
      },
      "occupation_field": {
        "label": null,
        "concept_id": null
      }
    },
...
}
```

**workplace_addresses - differences between domestic vs abroad**. 

***Example, domestic***
```
    "workplace_addresses": [
      {
        "municipality_concept_id": "AvNB_uwa_6n6",
        "municipality": "Stockholm",
        "region_concept_id": "CifL_Rzy_Mku",
        "region": "Stockholms län",
        "country_concept_id": "i46j_HmG_v64",
        "country": "Sverige"
      }
    ]
```
***Example, abroad***. 
```
    "workplace_addresses": [
      {
          "municipality_concept_id": "",
          "municipality": "",
          "region_concept_id": "",
          "region": "",
          "country_concept_id": "QJgN_Zge_BzJ",
          "country": "Norge"
      }
    ]
```
