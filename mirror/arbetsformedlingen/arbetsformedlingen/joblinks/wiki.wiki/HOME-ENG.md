---
title: Ecosystem for vacancies – a jobtech collaboration between the Swedish Public Employment Service, Statistics Sweden and private job boards
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/-/blob/main//HOME-ENG.md
gitdir: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki
gitdir-file-path: /HOME-ENG.md
date: '2022-04-28 11:18:43'
path: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/HOME-ENG.md
tags:
- HOME-ENG.md
---


![image](uploads/ebddac60573b5b33ae798b2ea562070f/image.png)


# Ecosystem for vacancies – a jobtech collaboration between the Swedish Public Employment Service, Statistics Sweden and private job boards
[For a Swedish version](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home) 



**Better statistics - better decision basis - better search experience**

The Swedish Public Employment Service together with Statistics Sweden invites private job boards to a collaboration to improve digital matching while creating better data for labour market statistics.

The benefits of the collaboration are many:

1. Creating better statistics in the Swedish labour market
1. Making it easier for our common users to search the entire labour market's range of advertisements.
1. Guiding users to the external websites that participate in the ecosystem.

"We cannot do this ourselves as government agencies, since we need vacancy data from the market's independent parties. Only then will we get an overall picture and can offer better service to our common customers", says Maria Dalhage, project manager for Ecosystem for Ads.

**So far, the collaboration has generated an additional 30% unique ads in Platsbanken.**  To the ads [demo in production](https://arbetsformedlingen.se/platsbanken/annonser?s=2) 

# For those of you who want to participate in the collaboration

**It shouldn't cost anything to work with us.**  If you have an API, we use it, otherwise we will collect your ads in another way.

*We respect your business model.*  In this collaboration, this means that *we only expose your ad links.* On the "back" we will structure ads according to the search parameters that are relevant. We call this principle "Back-to-source" i.e. the user who clicks on a job ad is always directed back to the external website that has shared the ad in the ecosystem.

In Platsbanken(SPES’s public Job board) you can see how your ad links are displayed [to Platsbanken](https://arbetsformedlingen.se/platsbanken/annonser).

# How it works

The Swedish Public Employment Service scrapes or receives ad data from the advertising sites. An enrichment process adds metadata about competencies to the ads.
 
Limited text, URL and metadata from the ads are made available as open data that everyone can use. The reason only limited text of the ad are made available is to preserve the incentives to direct the user back to your website.

*click on the picture to enlarge it*
![image](uploads/5be4ae458f1d173d1caed8f55aa80a62/image.png)
     
**Information displayed through the Jobtech Links API**
1. Link to the ad
1. Title of ads
1. The first two sentences in the ad
1. Metadata (CITY, DATE, OCCUPATION, SSYK, DUPLICATE) for searching and filtering ads.

**Feel free to try the API yourself!**  (Created for communication between IT systems, but it's easy to review as a human)
1. Visit [https://links.api.jobtechdev.se](https://links.api.jobtechdev.se)
1. Click on joblinks
1. Click on "Try it out"
1. Fill in "system developer" or any profession in the q field
1. Click on "execute".



# What you need to do to participate
1. Inform your customers need to know that you share their ads with us.
1. Share your ads with us: Do you have an API? At the lowest level, we need an API with URL, TITLE and ad text. (If you have structured data on LOCATION and OCCUPATION, that's a plus).

If you do not have an API, please contact us and we will collect your ads via scraping technology.
[Contact the project](mailto:maria.dalhage@arbetsformedlingen.se)

# Our philosophy

We believe in going from Ego to Eco. To collaborate on our data in order to better fulfill our mission. By creating conditions to share information and technology, opportunities for innovation are also created -not by starting on a blank sheet of paper but by getting inspiration, data and knowledge from existing initiatives. Our policy on transparency

# Pages

[The ecosystem in media](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Ekosystem-f%C3%B6r-annonser-i-media)

[About the project](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Project-description)

[Identify ad duplicates](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Identifiera-annonsdubbletter)

[Pilot API definition](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Pilot-API-definition)

[Community Backlog](https://gitlab.com/arbetsformedlingen/joblinks/processing/-/wikis/Community-Log)