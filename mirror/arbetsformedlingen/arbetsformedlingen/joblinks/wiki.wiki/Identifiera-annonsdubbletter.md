---
title: Dubbletthantering viktigt för ett hållbart ekosystem
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/-/blob/main//Identifiera-annonsdubbletter.md
gitdir: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki
gitdir-file-path: /Identifiera-annonsdubbletter.md
date: '2022-04-28 11:18:43'
path: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Identifiera-annonsdubbletter.md
tags:
- Identifiera-annonsdubbletter.md
---
> Av Jonas Södergren, Från Arbetsförmedlingens [plattform för öppna data](https://jobtechdev.se).

Så kallade dubbletter förekommer på annonssajter och är något som branschen försöker kommer till rätta med. Här beskrivs ett angreppssätt med en öppen algoritm som lösning.   

Stort tack till följande aktörer som kostnadsfritt delat sin data för jämförelsen!
*Blocket, OnePartnerGroup, Netjobs, Jobbdirekt, CareerBuilder, Monster, Indeed, Offentliga job (Visma), Studentjob*

[Läs mer om projektet](https://gitlab.com/joblinks/wiki)

## Dubbletthantering viktigt för ett hållbart ekosystem
Arbetsförmedlingens enhet JobTech Development har som mål att tillsammans med privata aktörer synliggöra det totala utbudet av annonser på ett hållbart sätt. Med hållbart åsyftas en modell som annonsplattformarna upplever gynnsam för deras tjänst. Vid flertalet intervjuer med bolag som jobbar med annonsering och arbetssökande har följande utmaningar identifierats.
1. **Brokig sökupplevelse** - För en sökande så är det svårt att få en bra överblick över det totala utbudet av annonser. En praktisk utmaning för den sökande blir att veta vilka sajter som finns och vad ett eventuellt överlapp mellan två sajter är. Många sökande rapporterar en upplevelse av dubbletter.
2. **Ofullständig statistik** - För att beräkna statistik från flera annonskällor behöver dubbletter kunna märkas ut. 

Med andra ord syftar arbetet med dubbletter att förbättra sökupplevelsen och förbättra vakansstatistiken i Sverige.

# Data som används
Algoritmen är testad på ett dataset från Februari 2020. Dataseten innehåller ca 100 000 annonser. [Annonser från arbetsförmedlingen](https://jobtechdev.se/docs/apis/jobstream/) (och de [historiska](https://jobtechdev.se/docs/apis/historical/) hittas här) är ca 42 000 för denna tidsperiod.  Antalet dubbletter uppskattas till ca 20 000 annonser. Arbetsförmedlingens Platsbank har enligt denna jämförelse ca 50% av utbudet.

# Algoritmen

Lösningen bygger på att skapa ett [Jaccard Index](https://en.wikipedia.org/wiki/Jaccard_index) och på så sätt kunna avgöra hur stort överlapp det är mellan två mängder med ord. Varje annons betraktas primärt som en mängd med ord. Eftersom ord som ort och yrke är väldigt semantiskt definierande för annonsen kommer de orden märkas som extra viktiga, yrke och ort i löpande text hittas med [följande tjänst](https://jobad-enrichments-api.jobtechdev.se/). För att bestämma likhet mellan dokument [används denna modul](https://github.com/duhaime/minhash) som är licenserad som öppen.
Projektet arbetar i [denna kodbas](https://gitlab.com/joblinks) som är publicerad på internet.

*Algoritmens effektivitet* 

Målet är att kunna jämföra många annonser utan att det skall ta någon nämnvärd tid i anspråk.

Att beräkna ett jaccard index görs genom att jämföra en annons med en annan annons parvis. Resultatet är en uppskattning mellan 0-1 avseende hur liknande annonserna är. I arbetet med dubbletthantering har ca 100 000 annonser används och det skulle betyda att 100 000 x 100 000 jämförelser behöver utföras som tar väsentlig "klock-tid" i anspråk. När antalet annonser blir fler, exempelvis med en beräkning mot historiska annonser (miljoner), så blir algoritmen snabbt opraktisk. Sannolikheten för att en mängd med 100 000 annonser består av enbart en annons med 99 999 dubbletter är låg. Effektiviteten är låg pga alla annonser betraktas som potentiella dubbletter. Om vi istället antar något mer realistiskt som att en annons inte har mer än låt säga och nu gissar vi ca 15 dubbletter så skulle det kanske gå att undvika alla jämförelser. Hur undviks då att missa eventuella dubbletter om vi inte ska jämföra med alla annonser?

Lösningen är att använda [en hashfunktion](https://sv.wikipedia.org/wiki/Hashfunktion) såsom [minhash](https://en.wikipedia.org/wiki/MinHash) som generellt används för att jämföra likhet mellan dokument i sökmotorer. Det särskiljande draget för MinHash just att hashvärdet för liknande texter blir liknande. Det betyder att alla annonser skulle kunna påföras ett MinHash-värde som kan användas sortera alla annonser utifrån hur lika annonserna är. Att ögna igenom exempelvis 1000 annonser går förvånansvärt fort. Men framförallt blir det effektivare för maskinen att sortera alla annonser utifrån potentiella dubbletter.

*Alla ord har inte samma tyngd*

Att betrakta alla ord i en annons som likvärdiga fungerar inte hela vägen. I många fall finns det annonser som skiljer sig på ett ord(speciellt ort) som ändrar hela innebörden av annonsen. För att kunna särskilja ort och yrke som har denna viktiga semantiska innebörd har alla annonser påförts metadata från en [öppen tjänst](). 

*Algoritmen steg för steg*

1. Beräkna ett MinHash-värde för varje annons
2. Sortera alla annonser på detta värde
3. Beräkna en [kontrollsumma](https://sv.wikipedia.org/wiki/MD5) för trippeln <Annonstitel, Yrke, Ort> för varje annons
4. Alla annonser gås igenom och för varje annons så jämförs jaccard-värdet och kontrollsumman med de 15 närliggande annonserna. En träff definieras som 80% överlapp och har samma titel, ort och yrke. Vid träff så märks annonserna som dubbletter.

Denna bloggpost är en del i att skapa en mer öppen myndighet. För den intresserade finns också källkoden tillgänglig. Det pratas mycket om etiska algoritmer, men hur vet man att algoritmen är etisk om den inte är öppen?

## Sidor

[Ekosystem i media](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Ekosystem-f%C3%B6r-annonser-i-media)

[Home](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home)

[Om Pågående projekt](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Projektsida-Ekosystem-f%C3%B6r-annonser)


[Identifiera annonsdubbletter](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Identifiera-annonsdubbletter)

[Pilot API definition](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Pilot-API-definition)

[Till den del av projektet som hanterar berikning av aggregerade annonser.](https://gitlab.com/joblinks/processing/-/wikis/Meta-data-enrichment-process-for-ads)