---
title: Ekosystem för annonser - samarbete mellan Arbetsförmedlingen, SCB och privata annonsaktörer
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/-/blob/main//home.md
gitdir: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki
gitdir-file-path: /home.md
date: '2022-04-28 11:18:43'
path: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/home.md
tags:
- home.md
---
# Ekosystem för annonser - samarbete mellan Arbetsförmedlingen, SCB och privata annonsaktörer
[For an English version](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/HOME-ENG)

![image](uploads/06d5c6712a60c4fa9cb7ba9b019c5db0/image.png)
**Bättre statistik - bättre beslutsunderlag - bättre sökupplevelse**

Arbetsförmedlingen [(Jobtech Development)](https://jobtechdev.se/) tillsammans med SCB bjuder in till samarbete för att förbättra och effektivisera den digitala matchningen och samtidigt skapa bättre underlag till arbetsmarknadsstatistiken.


Vinsterna med samarbetet är många:

1. Vi skapar bättre statistik på svensk arbetsmarknad
1. Vi gör det enklare för våra gemensamma användare att söka bland hela arbetsmarknadens utbud av annonser.
1. Vi lotsar användare till de externa webbplatser som deltar i ekosystemet.

"Vi kan inte göra detta själva som myndigheter utan vi behöver annonsdata från marknadens fristående parter. Endast då får vi en helhetsbild och kan erbjuda bättre service till våra gemensamma kunder", säger Maria Dalhage, projektledare för Ekosystem för annonser.

**Än så länge har samarbetet genererat ytterligare 30% unika annonser i Platsbanken.** [Till annonserna (demo i skarp produktion)](https://arbetsformedlingen.se/platsbanken/annonser?s=2)

# Så här går det till
Arbetsförmedlingen skrapar eller får annonsdata från annonssajterna. Genom en berikningsprocess adderas metadata om kompetenser till annonserna.

![image](uploads/43281395978056afa960456672f75a2b/image.png)



Endast delar av annonsen görs tillgänglig som öppna data via ett API som alla kan använda. Anledningen till att bara delar av annonsen görs tillgänglig är för att bevara incitamenten att leda användaren tillbaka till er webbplats, oavsett vilken plattform som visar upp er annons. 

**Information som visas upp via API:et Jobtech Links**
1. Länk till annonsen
1. Titel på annonser 
1. De första två meningarna i annonsen
1. Metadata (ORT, DATUM, YRKE, SSYK, DUBBLETT) för sök- och filtering av annonser.


**Prova gärna API:et själva!** (Skapat för kommunikation mellan it-system, men det är enkelt granska som människa)

1. Besök [https://links.api.jobtechdev.se](https://links.api.jobtechdev.se)
1. Klicka på joblinks
1. Klicka på ”Try it out”
1. Fyll i "systemutvecklare"  eller valfritt yrke i fältet q
1. Klicka på "execute".



## För er som har jobbannonser

**Det ska inte kosta att samarbeta med oss.** Har ni ett API använder vi oss av det, annars inhämtar vi era annonser på annat sätt.

**Vi respekterar er affärsmodell.** I detta samarbete innebär det att vi endast exponerar era annonslänkar. (Se ovan under *Information som visas upp via API:et Jobtech Links*). Vi kommer på "baksidan" att strukturera annonser efter de sökparametrar som är relevanta. Vi kallar denna princip för "Back-to-source" d.v.s. användaren som klickar på en jobbannons slussas alltid tillbaka till den externa webbplats som delat sin annons i ekosystemet.

I Platsbanken ser ni hur era annonslänkar visas upp [till Platsbanken](https://arbetsformedlingen.se/platsbanken/annonser)

**Vill ni synas med era annonslänkar i Platsbanken?**

1. Era kunder behöver känna till att ni delar deras annonser med oss.
2. Dela era annonslänkar med oss:
Vi kommer att samla in era annonser i dess helhet, även om vi bara visar upp och delar era annonslänkar. Detta gör vi eftersom vi behöver kunna ta fram sök-och filtreringsmetadata. 

Har ni ett API?  På lägsta nivå behöver vi ett API med URL, TITEL och annonstext. (Om ni har strukturerad data om ORT och YRKE är det ett plus).

Saknar ni API? Kontakta oss så inhämtar vi era annonser via skrapningsteknik.

[Maila projektet](mailto:maria.dalhage@arbetsformedlingen.se)


## Community Backog
Annonser är ostrukturerad data. Vi behöver hjälp med att via textanalyser ta fram metadata för de sök- och filtreringsparametrar som behövs.

[Till Community Backlog](https://gitlab.com/arbetsformedlingen/joblinks/processing/-/wikis/Community-Log)


## Vår filosofi

Vi tror på att gå från Ego till Eko. Att samarbeta kring vår information för att bättre kunna fullfölja vårt uppdrag. Våra kunder delas av andra aktörer inom offentlig förvaltning.  Våra kunders livshändelser och affärshändelser spänner sig över flertalet aktörer inom offentlig förvaltning, men också privata aktörer. Genom att skapa förutsättningar att dela information och teknik, skapas också möjligheter för innovation. Inte genom att börja på ett blankt papper men att hämta inspiration, data och kunskap där den redan finns och förvaltas. [Vår policy kring öppenhet](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Policy-%C3%B6ppenhet)


## Sidor
[Ekosystem i media](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Ekosystem-f%C3%B6r-annonser-i-media)

[Om pågående projekt](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Projektsida-Ekosystem-f%C3%B6r-annonser)


[Till källkoden](https://gitlab.com/arbetsformedlingen/joblinks)

[Identifiera annonsdubbletter](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Identifiera-annonsdubbletter)

[Pilot API definition](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Pilot-API-definition)

[Community Backog](https://gitlab.com/arbetsformedlingen/joblinks/processing/-/wikis/Community-Log)