---
title: Om Projektet
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/-/blob/main//Projektsida-Ekosystem-för-annonser.md
gitdir: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki
gitdir-file-path: /Projektsida-Ekosystem-för-annonser.md
date: '2022-04-28 11:18:43'
path: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Projektsida-Ekosystem-för-annonser.md
tags:
- Projektsida-Ekosystem-för-annonser.md
---
# Om Projektet 

NOT. Projektet har i maj 2020 övergått till förvaltning. Arbetet fortsätter därmed i löpande linjeverksamhet.

Ekosystem för annonser startade 2020 och handlar om att landets största annons- och matchningsaktörer öppnar upp och delar med sig av jobbannonsdata i ett gemensamt digitalt ekosystem. Syftet är att skapa en hållbar modell för aktörerna att dela och den sökande att hitta jobbannonser och effektivisera den digitala matchningen.

**Projektet syftar till att ge svar på frågorna:**
1. Finns en hållbar lösning i Ekosystem för annonser?  
a. Affärsmässigt  
b. Tekniskt  
2. Hur ska Arbetsförmedlingens roll och bidrag formuleras?

## Principer för ett hållbart samarbete för annonser 
För att ekosystemet ska vara hållbart behöver det finnas ett antal principer: 
* **Back-to-source**; Leda trafiken tillbaka till ursprungssajten för att kunna respektera källan och affärsmodellen
* **Öppna data** – Endast annonslänkar samt metadata tillhandahålls som fil och via ett öppet API.
* **Öppen standard** Resultatet av skrapningen kommer att använda ett öppet standardformat (jobposting)
* **Öppen källkod** – Tekniken är baserat på scrapy och källkoden nås via Jobtechdev.se. [Varför öppen källkod?](https://gitlab.com/joblinks/wiki/-/wikis/Policy-%C3%B6ppenhet)


# Varför deltar aktörerna?

”Pandemin har gjort att vi har fler kandidater som söker jobb - samtidigt som det finns det färre annonser ute. Just nu är det viktigare än någonsin att underlätta arbetssökandet, och som en av de största kommersiella jobbaktörerna känns det självklart att vara med i det här samarbetet”, säger Matilda Adelborg, pressansvarig på Blocket Jobb.

”Vi på Monster ser fram emot detta samarbete som kommer att göra det så mycket enklare för arbetssökande att hitta ett nytt och spännande jobb. Det här ligger helt i linje med Monsters mål som är att skapa rätt matchning mellan kandidater och arbetsgivare”, säger Per Wadell, country manager på Monster.

”Vi måste möta den digitala omstruktureringen, som bland annat innebär att människor just nu tvingas navigera bland en mängd olika annonsplattformar när de söker jobb, utan att veta om de har hela utbudet. Varken Arbetsförmedlingen eller någon annan aktör kan lösa problemet på egen hand eftersom ingen har tillgång till alla jobbannonser, därför gör vi det tillsammans”, säger Maria Dalhage, projektledare på JobTech Development / Arbetsförmedlingen som står för de tekniska lösningar som krävs för gemensam hantering av annonsdata.

[Läs hela artikeln](https://www.linkedin.com/company/jobtech-development/posts/?feedView=all)

# Tidplan
![image](uploads/85dc551f68e22ac2f0c0669eeaf1d4cd/image.png)

Sedan 15 februari 2021 pågår en pilot i Platsbanken där det nya API:et Jobtech Links testas. Det är annonslänkar från externa webbplatser som testas i Platsbanken, dels ger piloten i Platsbanken projektdeltagarna en uppfattning om samarbetet är hållbart affärsmässigt, dels utvärderar Platsbankens användare användargränssnittet. Piloten förväntas att pågå i 2-3 månader där Platsbanken successivt anpassar tjänsten efter användarnas och pilotaktörernas feedback. 

Parallellt pågår ett samarbete mellan SCB och Arbetsförmedlingen för att kunna ta emot den nya datakällan i ordinarie verksamheter. SCB kvalitetssäkrar de tekniska beräkningar som ligger till grund för Dubbletthantering, Statistiska koder samt annan metadata. 

# Resultat

Förutom att den nya datakällan med annonslänkar skapar *bättre statistik och bättre beslutsunderlag* eftersom myndigheterna kan basera sin statistik på fler annonser så har projektet lett till en *bättre sökupplevelse* för de som söker arbete. Nu kan arbetssökande få en överblick genom att fler externa webbplatser presenterar sina annonser i[Platsbanken.](https://arbetsformedlingen.se/platsbanken/annonser)

Vidare har projektet hittat ett recept på privat och offentlig samverkan som kan användas i andra samarbeten.

**Kriterier för hållbart samarbete**

1. Minska trösklar för samarbete  - Ska inte kosta att samarbeta med oss
1. Respektera privat sektors affärsmodell 
1. Lös gemensamma problem tillsammans. 
1. Ge tillbaka: Dela teknik för transparens och återanvändande.

Just nu visas ca 30% fler annonser i Platsbanken och externa Webbplatser vittnar om mer trafik som ett resultat av samarbetet. Fler externa aktörer har hört av sig och det pågår löpande anslutning till ekosystemet.

# Medverkande i projektet
OnePartnergroup  
Netjobs  
Jobbdirekt  
Careerbuilder  
Monster  
Indeed  
Blocket  
Offentligajob (Visma)  
Studentjob

# Nästa steg
Platsbanken utvärderar feedback från användare. 

Förbättringsförslag från användarna:
1. Går det att slå ihop datakällorna Platsbanknen och Externa webbplatser så att man kan söka på hela utbudet med en sökning?

2. Efterfrågade sök- och filtreringsparameterar:
- Arbete utan krav på utbildning. 
- Arbete utan krav på körkort 
- Heltid och deltid

3. Annonslänkar kommer att tillhandahållas som ett öppen API. Det innebär att annonslänkarna kommer kunna användas av flera tjänster. Bland annat har ett antal kommuner visat intresse av att få tillgång till annonslänkarna för att göra det enklare för sina invånare att få en överblick om vilka vakanser som finns i kommunen.

## Kontakt
För medverkan i projektet eller för mer information, kontakta Maria Dalhage, projektledare.
[Maila projektet](mailto:maria.dalhage@arbetsformedlingen.se)


## Sidor

[Ekosystem i media](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Ekosystem-f%C3%B6r-annonser-i-media)

[Home](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home)


[Identifiera annonsdubbletter](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Identifiera-annonsdubbletter)

[Pilot API definition](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Pilot-API-definition)

[Till den del av projektet som hanterar berikning av aggregerade annonser.](https://gitlab.com/joblinks/processing/-/wikis/Meta-data-enrichment-process-for-ads)