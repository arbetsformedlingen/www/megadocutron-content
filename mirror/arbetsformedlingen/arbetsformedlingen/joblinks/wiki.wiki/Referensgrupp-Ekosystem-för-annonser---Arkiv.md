---
information:
- Referensgruppen har upphört då projektet i maj 2021 ändrat status till förvaltning.
title: Sidor
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/-/blob/main//Referensgrupp-Ekosystem-för-annonser---Arkiv.md
gitdir: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki
gitdir-file-path: /Referensgrupp-Ekosystem-för-annonser---Arkiv.md
date: '2022-04-28 11:18:43'
path: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Referensgrupp-Ekosystem-för-annonser---Arkiv.md
tags:
- Referensgrupp-Ekosystem-för-annonser---Arkiv.md
---
Information: Referensgruppen har upphört då projektet i maj 2021 ändrat status till förvaltning.

En viktig del av projektet Ekosystem för annonser handlar om att samla in arbetsmarknadsstatistik och vakansstatistik som kan utvinnas ur annonserna. Vi ser denna datakälla som en del av det underlag som Arbetsförmedlingen kan dela med SCB då bägge myndigheterna har i uppdrag att förse samhället med information, statistik och analyser om arbetsmarknaden. Därför skapar vi en user group eller referensgrupp för statistik och analys. 

Gruppen kommer bl.a. att bidra med att efterfråga metadata och testa användningen av datasetet Joblinks Statistics.

Mötesanteckningar från referensgruppens möten läggs upp efterhand på denna sida så att alla kan följa arbetet. 

**Mötesanteckningar 20200519**

[Referensgrupp_ekosystem_för_annonser__mötesanteckningar_20200519.pdf](uploads/5274a375f2f130f4dd44c64089509fd3/Referensgrupp_ekosystem_för_annonser__mötesanteckningar_20200519.pdf)

[Ekosystem_för_annonser_referensgrupp_202020519__sparad_automatiskt_.pptx](uploads/1a8437645cb4aa7aa7ab3a7f4adba19b/Ekosystem_för_annonser_referensgrupp_202020519__sparad_automatiskt_.pptx)

[Ekosystem_för_annonser_-_koppling_ontolgi-taxonomi.pdf](uploads/15f332c6083eb21978054915c688352d/Ekosystem_för_annonser_-_koppling_ontolgi-taxonomi.pdf)

[Till projektet Ekosystem för annonser.](https://gitlab.com/joblinks/wiki/-/wikis/Introduktion)

**Mötesanteckningar 20200826**

[Referensgrupp_ekosystem_för_annonser__mötesanteckningar_20200826.docx](uploads/e52509c5279f1ae54b6bdcd19c35d5bd/Referensgrupp_ekosystem_för_annonser__mötesanteckningar_20200826.docx)

[20200826__Kopia_av_ekosystem_för_annonser_aktiviteter__version_1_.xlsb.xlsx](uploads/e77e55dbae8bcd9a2b727371baf8826d/20200826__Kopia_av_ekosystem_för_annonser_aktiviteter__version_1_.xlsb.xlsx)

[Annonsdubbletter__1_.pdf](uploads/a9e636c71e2f7e6766114505f5f65325/Annonsdubbletter__1_.pdf)

**Mötesanteckningar 202021027**

[202021027_referensgrupp_ant_.pdf](uploads/d5b1d011ee55c3eaf8dd0954d1ddbbb3/202021027_referensgrupp_ant_.pdf)

[DEMO_PB_20101019__1_.pptx](uploads/d5d56a8a9197f55855f77069b43755bd/DEMO_PB_20101019__1_.pptx)



**Mötesanteckningar 20201130**

[20201130_referensgrupp_Ekosystem_för_annonser.pdf](uploads/aed5e0890400426612529b82431905f4/20201130_referensgrupp_Ekosystem_för_annonser.pdf)

[Ekosystem_for_annonser_Arbetsmöte_20201130.pptx](uploads/d4bc53c6c2739e42979cef0ca8a0004e/Ekosystem_for_annonser_Arbetsmöte_20201130.pptx)

**Mötesanteckningar 20210115**

[20210115_referensgrupp_Ekosystem_för_annonser.pdf](uploads/66c5335ecdedba8328d9a83af579c3c7/20210115_referensgrupp_Ekosystem_för_annonser.pdf)

**Mötesanteckningar 20210215**

[20210215_referensgrupp_ant___002_.pdf](uploads/149ba7a2581984c5453a38906d177267/20210215_referensgrupp_ant___002_.pdf)

[Ekosystem_för_annonser_Referensgruppsmöte_20210215.pptx](uploads/fce11b3bd2f3033ee31544beeb2ffe4d/Ekosystem_för_annonser_Referensgruppsmöte_20210215.pptx)

**Mötesanteckningar 20210322**

[20210322_referensgrupp_ant_.pdf](uploads/167e311d42588d19b6b5dda66b3aa269/20210322_referensgrupp_ant_.pdf)


## Sidor

[Ekosystem i media](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Ekosystem-f%C3%B6r-annonser-i-media)

[Home](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home)

[Om Pågående projekt](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Projektsida-Ekosystem-f%C3%B6r-annonser)

[Referensgrupp med SCB](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Referensgrupp-Ekosystem-f%C3%B6r-annonser)

[Identifiera annonsdubbletter](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Identifiera-annonsdubbletter)

[Pilot API definition](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Pilot-API-definition)

[Till den del av projektet som hanterar berikning av aggregerade annonser.](https://gitlab.com/joblinks/processing/-/wikis/Meta-data-enrichment-process-for-ads)