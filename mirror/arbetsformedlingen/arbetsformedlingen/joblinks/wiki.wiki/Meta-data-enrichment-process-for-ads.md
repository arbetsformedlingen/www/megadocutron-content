---
title: Sidor
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/-/blob/main//Meta-data-enrichment-process-for-ads.md
gitdir: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki
gitdir-file-path: /Meta-data-enrichment-process-for-ads.md
date: '2022-04-28 11:18:43'
path: /arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Meta-data-enrichment-process-for-ads.md
tags:
- Meta-data-enrichment-process-for-ads.md
---
Click on the following link to access the Meta data enrichment process for ads.

https://gitlab.com/joblinks/processing/-/wikis/Meta-data-enrichment-process-for-ads

## Sidor

[Ekosystem i media](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Ekosystem-f%C3%B6r-annonser-i-media)

[Home](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home)

[Om Pågående projekt](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Projektsida-Ekosystem-f%C3%B6r-annonser)

[Referensgrupp med SCB](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Referensgrupp-Ekosystem-f%C3%B6r-annonser)

[Identifiera annonsdubbletter](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Identifiera-annonsdubbletter)

[Pilot API definition](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/Pilot-API-definition)

[Till den del av projektet som hanterar berikning av aggregerade annonser.](https://gitlab.com/joblinks/processing/-/wikis/Meta-data-enrichment-process-for-ads)