---
title: EMIL
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/-/blob/main//Ordlista,-Standarder.md
gitdir: /arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki
gitdir-file-path: /Ordlista,-Standarder.md
date: '2023-10-16 14:52:21'
path: /arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/Ordlista,-Standarder.md
tags:
- Ordlista,-Standarder.md
---
[[_TOC_]]

# EMIL

[EMIL-standarden](https://www.sis.se/standardutveckling/tksidor/tk400499/sistk450/emil-standarden/) beskriver utbildningar och utbildningstillfällen. Formell beteckning är SS10700:2013.
Standarden används bland annat för att leverera data till Skolverkets SUSA-navet.

# ESCO

[European Skills, Competences, Qualifications and Occupations](https://esco.ec.europa.eu/sv) klassificerar yrken, färdigheter och kvalifikationer. Yrken är relaterade till ISCO.

# EQF

[European Qualification Framework](https://europa.eu/europass/sv/europassfunktioner/den-europeiska-referensramen-kvalifikationer) är framtaget för att underlätta jämförelse och förståelse av 
kvalifikationer från olika nationer. EQF ska göra det lättare att arbeta och studera utomland.

# ISCED

[International Standard Classification of Education](http://uis.unesco.org/sites/default/files/documents/international-standard-classification-of-education-isced-2011-en.pdf)

Standard från UNESCO.

# ISCO

[International Standard Classification of Occupation](https://www.ilo.org/public/english/bureau/stat/isco/) Aktuell version är 08.

# NUTS

[Nomenclature Of Territorial units for Statistics](https://ec.europa.eu/eurostat/web/nuts/background) är uppdelning av regioner för statistik inom EU.

# SeQF

[Sveriges referensram för kvalifikationer](https://www.myh.se/validering-och-seqf/seqf-sveriges-referensram-for-kvalifikationer), SeQF, är ett ramverk för beskriva
kvalifikationer både för utbildning och yrkesbevis. SeQF är den svenska
implementationen av [EQF](#eqf).

# SNI

[Standard för svensk näringsgrensindelning](https://www.scb.se/dokumentation/klassifikationer-och-standarder/standard-for-svensk-naringsgrensindelning-sni/) från SCB. Finns för åren 69, 92, 2002 och 2007.

# SSYK

[Standard för svensk yrkesklassificering](https://www.scb.se/dokumentation/klassifikationer-och-standarder/standard-for-svensk-yrkesklassificering-ssyk/)

Finns från åren 96 och 2012.https://ec.europa.eu/eurostat/statistics-explained/index.php?title=International_Standard_Classification_of_Education\_(ISCED)https://ec.europa.eu/eurostat/statistics-explained/index.php?title=International_Standard_Classification_of_Education\_(ISCED) Den är relaterad till den internationella standarden ISCO.

# SUN

[Svensk utbildningsnomenklatur](https://www.scb.se/dokumentation/klassifikationer-och-standarder/svensk-utbildningsnomenklatur-sun/) från SCB.

Finns från 69, 2000, 2020. Den ät relaterad till den internationella standarden ISCED.

# UNESCO

United Nations Educational, Scientific and Cultural Organization

# Schema.org

[Schema.org](https://schema.org/) is a open community effort, initially launched by Google, Microsoft and and Yahoo. Maintains and promotes schemas for structured data on the Internet, with its vocabulary usable with many encodings including RFDa, JSON-LD. Part of semantic web effort. Some relevant concepts include [JobPosting](https://schema.org/JobPosting), [Occupation](https://schema.org/Occupation), [EducationalOccupationalCredential](https://schema.org/EducationalOccupationalCredential), [Course](https://schema.org/Course).

# HR Open

[HR Open Standards](https://www.hropenstandards.org/) Consortium is a voluntary, consensus-based standards organization founded in the US with a European branch. A community of HR technologists facilitates discussions on global technology concepts and challenges. Members collaborate to develop free standards, which encompass the full HR domain from Hire to Retire, including job advertisement, job descriptions, CV data and more.
