---
title: Ostrukturerade vs strukturerade kompetensdata
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/-/blob/main//Insikter.md
gitdir: /arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki
gitdir-file-path: /Insikter.md
date: '2023-10-16 14:52:21'
path: /arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/Insikter.md
tags:
- Insikter.md
---
Preliminära insikter och noteringar för input till slutrapport
till projektet. Anteckningarna är inte kvalitetsgranskade och kan därför vara ofullständiga eller felaktiga.

Generellt sett kan man säga att datat ofta saknar antingen god täckning eller
har bristande kvalitet. Det gör att det i många fall är svårt att koppla samman flera
dataset eller dra slutsatser från dem. Därmed är det svårt att använda datat utanför det ofta begränsade initiala syftet och/eller organisationen.

Liknande gäller om man tittar på API:er. I många fall är de gjorda för 
ett specifikt syfte och kan därför vara svåra att använda i andra syften.
Dokumentationen brister ofta. Exempelvis saknas i en hel del fall en beskrivning
vad fälten betyder. Om man säger utbildningskod, vilken avses? Kring dokumentationen skulle krävas begränsade insatser som skulle kunna ge stora effekter, detta gäller både den rena API dokumentationen(Swagger) och hemsidan för Jobtech. Hemsidan har ingen naturlig ingång för utvecklare som skall välja API.

Kortfattat så går det beskriva som att det beskrivna användningsfallet inte går genomföra ännu. Det finns både juridiska, tekniska och ekonomiska utmaningar.

## Ostrukturerade vs strukturerade kompetensdata

JobTech arbetar med två sätt att identifiera yrken och kompetenser.

* Strukturerad data genom taxonomin som beskriver begreppsstrukturer
  och dess relationer. Exempel på användning är att arbetsgivare anger
  vilket av dessa yrken platsannonsen söker.
* Ostrukturerad data där berikningen identifierar begrepp i en text och
  placerar dem under vilken typ de är.

Platsannonser som publiceras via JobSearch API:et är en kombination av
dessa två. De båda värdarna har olika styrkor och svagheter men
vi får också ytterligare utmaningar av att kombinera de två världarna då terminologin inte är kopplad.

Ursprungligen var berikningen till för att identifiera söktermer
i platsannonser där dessa lagrades i ElasticSearch.
ElasticSearch söker på begrepp bredare. Ord behöver inte vara
exakt lika dana för att matcha.
Det vi gör nu är att vi behöver matcha kompetenser från dokument
mot varandra(exvis CV mot platsannons) och identifiera vad som
saknas. Det ställer andra krav som inte existerade i
det ursprungliga användningsfallet för berikningen.

Berikningen är inte begränsad till att använda ord från
taxonomin, vilket är en styrka i fallet för sökningar. 
Berikningen identifierar istället ord utifrån
meningen de förekommer i. På detta sätt kan den även
identifiera okända ord.
Samtidigt som man skall koppla samman och formalisera data
är det en fördel om man kan använda taxonomibegrepp.
Med Taxonomin så finns tydliga hierarkier, exvis yrke
och yrkesgrupper. Dessa hierarkier är användbara
för att se om exvis ett CV och en platsannons tillhör
samma yrkesgrupp men är olika yrken. Det går
även se vilka kompetenser som man har som ett yrke
söker. Tyvärr grupperas yrken ihop där yrkesbenämningar kan kräva vitt skilda kompetenser. Exempelvis grupperas yrkesbenämningen UX-designer ihop med Övriga IT-specialister. Kompetenslistan blir därför tämligen orimlig.

En svaghet med taxonomin är att många tjänster använder (eller har länge använt)
gamla versioner. Det gör det svårt att identifiera
nytillkomna yrken. Dessa nya yrken kan ofta ha ökad flexibilitet på
önskad kompetens. De nya yrkena är ofta tillväxtyrken. Berikningen uppdateras i betydligt högre takt med synonymer men också 
på det sätt den identifierar kompetenser utifrån meningsformuleringar. Detta borde kunna vara en allt viktigare funktion för en arbetsmarknad i omställning.

Att få till en mappning mellan taxonomin och berikningens
ordlistor skulle vara värdefullt då man skulle få
tillgång till hierarkierna. Det är dock en större
utmaning då berikningen har en mycket mer omfattande
vokabulär än taxonomin samt fördröjningen av att införa nya
versioner av taxonomin. En mer flexibel taxonomi som innehåller snabbföränderliga begrepp utanför de standarder som redan används, skulle kunna göra taxonomin en central kunskapskälla för att kunna utnyttja grafdata samt kunna följa språket i t.ex. språket i annonser i snabb förändring.

Beriknings ordlistor har begrepp som betyder samma sak
men har av naturliga skäl ingen koppling till varandra.
Exempelvis "hjärt- och lungräddning" och "hjärt-lungräddning",
dessa behöver kopplas samman för vara användbara i
matchningen. Detta finns kopplingar för vissa termer men saknas
för många. Då dessa ordval i platsannonser inte får samma kompetens-ord
identifierade så är det svårt att gruppera att de söker samma kompetens.

Även med dessa synonymer i berikningens ordlistor skulle
taxonomin vara nyttig då det skapas en tydlighet vilken
term som är normerande.

Kan taxonomin täcka en tillräcklig del av den vokabulär
som behövs kring yrkesvägledning? Ett syfte med taxonomin är
att beskriva en gemensam ontologi som är gemensam för
hela arbetsmarknaden. Här har det skett ett arbete inom
KLL semantikprojektet. Kan vi dra nytta av det?
Är taxonomin lämplig för individnivå eller är den endast
användbar på en statistisk nivå?

## Klustring av platsannonser och yrken

Som ett alternativ till den gruppering av yrken som finns inom
taxonomin med yrken, yrkesgrupp och yrkesområde så har vi försökt
klustra platsannonserna utifrån kompetensord. Tanken med experimentet
är att se om vi kan identifiera yrken med liknande kompetenser som i
taxonomin ligger långt ifrån varandra. Ett exempel är en grafisk 
designers kompetens mycket överlappar kompetensen med en designer
av användargränssnitt.

Vi har inte lyckat identifiera rimliga kluster utifrån de ord 
berikningen identifierar hos platsannonser. Detta beror på flera faktorer.
Först det som beskrivits tidigare att många begrepp med samma
betydelse inte har någon gruppering.
Variationen över identifierade kompetenser för ett och samma
yrke varierar stort. Det är få kompetenser som förekommer i flera
platsannonser för samma yrke. Data i mångt och mycket framträder slumpmässigt när man
tittar i ett holistiskt perspektiv.

## CV

För att kunna skapa individcentrerad matchning måste
man ha insikt i individens meriter.
Den naturliga källan för meriter är individens CV.
Denna information är givetvis integritetskänslig och 
måste behandlas därefter. 

Identifiera kompetensord från CV bör vara möjligt på samma sätt som för platsannonser. En omtränning av modellen kan behövas då meningsformuleringarna
i CV är skrivna i ett annat perspektiv än i platsannonser.
En generell uppfattning är att många CV är ganska begränsande skrivet vilket
gör det svårt att hitta matchande ord. 

Därför finns ett behov av att ha bra och riktig CV-data
att testa på för verifiera hur extrahering av kompetenser kan se ut samt
för att eventuellt träna en ny modell.

För att kunna göra en ordentlig utvärdering arbetar vi på
att få tillgång till en större mängd CV. Här finns det 
begränsningar i Juridiken vad AF får göra. De CV(profiler) som arbetssökande gör tillgängliga för 
arbetsgivare via AF är lagrade i ett eget utrymme.
GDPR medgivandet för dessa är endast för att ge 
arbetsgivare möjlighet att hitta kandidaten.

I samband med att en användare vill ha individuell digital yrkesvägledning kan
individen ge ett samtycke för ändamålet. CV kan då delas av individen med hjälp
av JobTechs tjänst [Connect Once](https://jobtechdev.se/sv/komponenter/connect-once). På så sätt behöver individen inte mata in sitt
CV igen, självklart bör/kan den möjligheten också finnas.
Denna väg är dock inte framkomlig för att träna nya modeller.

## Licensyrken eller yrken som kräver yrkesbevis

Ett antal yrken kräver licens för att få utföra yrket, exvis
inom sjukvård och advokater. I många av dessa fall behöver man 
ha gått en specifik utbildning så som sjuksköteskeutbildning eller
juristutbildning. I dessa fall är det svårt att dra nytta av
kunskap rekvirerat på annat sätt.
I andra fall kan det vara yrken som kräver ett yrkesbevis, men
det önskas också andra kompetenser. Yrkesbevis är oftare kortare 
utbildningar. Här kan man troligen kombinera andra kompetenser som
arbetsgivaren söker med det specifikt krävda yrkesbeviset. 
En utbildning till som leder till ett yrkesbevis kan därför vara
en relativt snabb insats för att bli tillgänglig för en ny del
av arbetsmarknaden.

Redaktionen har gjort ett inledande arbete kring licensyrken.

## Kvalifikation och Yrken

Med hjälp av Taxonomin och [Kvalifikationsdatabasen](https://www.myh.se/validering-och-seqf/seqf-sveriges-referensram-for-kvalifikationer)
från MYH så kan man koppla samman yrke med vilka yrkesbevis som finns.
Oavsett om yrkesbeviset är ett krav eller önskemål.
Kvalifikationerna i sig är beskrivna text i SeQF. De borde finnas koppling till exvis ESCO skill
eller taxonomins skills. Då skulle ytterligare mappning komma till som kan
indikera önskad kompetens som inte är med i yrkesbevis mm.
Idag(2022-12-01) listar kvalifikationsdatabasen endast 47 kvalifikationer.
Detta gör nyttan begränsad. Utbildningar från utbildningssystemet är inte listade, där finns bara dokumentation vilka lagar som styr att de ger en
viss SeQF nivå.


## Utbildningar

Redan under 2022 gjorde JobTech en webskrapning av utbildnings beskrivningar 
och berikade dessa med hjälp av berikningen. Det noterades att kvaliteten
på utbildningsbeskrivningarna varierar stort. Susanavet innehåller bara metadata och referenser till utbildningarna på respektive arrangörs hemsida. Varje
arrangörs utbildningar måste därför hanteras specifikt.

### Antagningsstatistik

För en individ som funderar på om det är en rimlig möjlighet att söka en utbildning är antagningsstatistik till utbildningen relevant. Universitets- och
högskolerådet(UHR) har en sida där det går söka på [antagningsstatistik](https://www.uhr.se/studier-och-antagning/antagningsstatistik/). Det finns inget publikt API dokumenterat, i alla fall inte sökbart på UHR eller [dataportalen.se](https://www.dataportal.se/). Däremot ser man vid undersökning av sidans källkod att den har ett bakomliggande API som används.
Om UHR skulle publicera och dokumentera detta API skulle det vara möjligt för
tjänster att integrera antagningsstatistik och ge ökat värde för individen.

## Yrkesprognoser

De yrkesprognoser som AF publicerar är på riksnivå. Det kan ge viss indikation för en individ. Men för att vara riktigt hjälpsamt bör de vara på en regional nivå. Behov kan variera över landet.

Här är jobbometern ett försök på lösning. Den är istället per arbetsgivare och försöker prognostisera anställningsbehov. Det finns dock ingen åtskillnad vilken
typ av kompetens som arbetsgivaren eftersöker. 

# Organisationsspecifika data
Varje organisation anpassar datat för sina behov och tappar ibland kopplingar till andra delar av samhället. Exempelvis [överensstämmer inte SSYK 4 från arbetsförmedlingen helt med SSYK 2012 från SCB](https://forum.jobtechdev.se/t/ssyk4-fran-scb-s-ssyk2012-matchar-inte-med-taxonomy/666).
Denna typ av lokala anpassningar försvårar myndighetsöverskridande samarbeten.

# Erfarenheter som ny tredjepartsutvecklare
Implementation av en variant av beskrivet användningsfall har gjorts inom ramen för KLL PoC. Förutom detaljkunskap som rapporterats på andra ställen såsom forum m.m., så är några allmänna observationer att:
- det finns ej / är tidskrävande att få en överblick över inte bara alla datamängder AF/JobTech har, men även data från de andra aktörerna inom KLL-sfären.
- för vägledningsscenario så hade man önskat större fokus på informationsnivån "ett steg ner" oavsett om det rör skolverket, MYH eller AF, så att t.ex. utbildningar och annonser blir till kompetenser och erfarenheter. Det pågår givetvis redan aktiviteter på detta område.
- för att kunna utnyttja kraften i AFs taxonomi, så krävs en fungerande översättning/koppling mellan fritext (t.ex. CV/platsannons) och taxonomi-begrepp (se kommentarer berikning/synonymord ovan).
- flera funktioner i ett vägledningsscenario skulle kunna gynnas av att använda språkmodeller. Det skulle vara användbart om AF skulle kunna erbjuda en väldigt enkel setup för att kunna köra inferens/finetuning self-hosted, för att komma förbi flera potentiella problem med kapacitet, GDPR m.m.