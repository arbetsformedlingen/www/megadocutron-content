---
title: Platsannonser
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/-/blob/main//Datakällor.md
gitdir: /arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki
gitdir-file-path: /Datakällor.md
date: '2023-10-16 14:52:21'
path: /arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/Datakällor.md
tags:
- Datakällor.md
---
Denna sida innehåller en övergripande kartläggning av datakällor som är av intresse inom projektet. Generellt för datakällor så måste ett flertal parametrar säkerställas:

* innehåll
* kvalitet
  * kompletthet
  * korrekthet
  * uppdaterat
* granularitet - är datat på rätt granulär nivå?
* kompabilitet med varandra - använder de samma begrepp så vi kan koppla samman dem.

# Platsannonser

## Arbetsförmedlingens API:er och dataset för platsannonser

Plattformen jobtechdev.se erbjuder fyra olika tjänster med platsannonser.

Samtliga aktuella platsannonser som finns i platsbanken är sökbara via [Jobsearch](https://jobtechdev.se/sv/komponenter/jobsearch). Är man bara intresserad av uppdateringar så är tjänsten [Jobstream](https://jobtechdev.se/sv/komponenter/jobstream) mer lämpad. Via ett antal samarbetspartners så presentera deras platsannonser på platsbanken under en egen flik(Externa webbplatser). Datainfrastrukturtjänsten för detta heter [JobAd links](https://jobtechdev.se/sv/komponenter/jobad-links).

Jobtech har ett arkiv av [historiska platsannonser](https://jobtechdev.se/sv/komponenter/historical-ads) från platsbanken för drygt fem år tillbaka i tiden. Motsvarande arkiv har börjat skapas för JobAd Links.

## Berikning

[JobAd Enrichment](https://jobtechdev.se/sv/komponenter/jobad-enrichments) extraherar information från ostrukturerade platsannonser. Detta API funkar även mot CV- och utbildnings-beskrivningar.

# Trender

Trenddata för arbetsmarknaden är intressant för att avgöra om ett potentiellt utvecklingsspår för individen har en efterfrågan på arbetsmarknaden.

## JobSearch Trends

[JobSearch Trends](https://jobtechdev.se/sv/komponenter/jobsearch-trends) är ett dataset som visar trender i söktermer på Jobsearch API:et. Det innebär statistik för alla sökningar på Platsbanken och andra som använder Jobsearch API:et.

## Digitalskills.se

[Digitalskills.se](https://digitalskills.se) är en tjänst baserad på historiska platsannonser som visar trender på efterfrågan av olika digitala kompetenser. Det är intressant att inspireras från. För digital yrkesvägledning skulle det behövas trender för samtliga typer av kompetenser.

## Yrkesprognoser

[Yrkesprognoser](https://jobtechdev.se/sv/komponenter/yrkesprognoser) är Arbetsförmedlingens prognoser för yrken som öppna data. Karriärvägledning kommer snart släppa uppdateringar kring detta med mer information.

## Jobbometern

[Jobbometern](https://jobbometern.jobtechdev.se/) är ett prognosverktyg för uppskatta rekryteringsbehovet hos arbetsgivare.

## Matchningskollen

[Matchningskollen](https://matchningskollen.se/) tittar på hur utbildningar matchar mot behov på arbetsmarknaden.

# CV-tjänster

## Connect once

[Connect Once](https://jobtechdev.se/sv/komponenter/connect-once) är en tjänst från JobTech för att överföra CV på individens begäran från arbetsförmedlingen till andra system. Det skulle kunna användas för att överföra CV från AF till ny tjänst för att hitta personens kompetens med hjälp av Jobtechs berikningstjänst.

## Europass

[Europass](https://europa.eu/europass/sv) är en CV tjänst för att förenkla rörlighet inom europas arbetsmarknad. (Vid undersökningstillfället så innehöll tjänsten ganska mycket trasiga länkar.)

# Nomenklaturer

## Taxonomi

JobTech skapar och underhåller en [taxonomi](https://jobtechdev.se/sv/komponenter/jobtech-taxonomy) för svensk arbetsmarknad. Denna binds även samman med dess europeiska motsvarighet. Taxonomin går interaktivt utforska genom tjänsten [Atlas](https://atlas.jobtechdev.se/). Termer som ingår är bland annat kompetenser(ESCO), yrken(SSYK, ESCO, ISCO), utbildningskoder(SUN) och näringslivsindelning(SNI). Se [konceptrelationsdiagram](https://atlas.jobtechdev.se/page/about.html).

## WikiPoc för Semantikprojektet

Pågående projekt under 2023 att låta branschföreträdare, yrkeskunniga mfl beskriva viktiga termer inom arbetsmarknaden.

## Kvalikationsdatabasen

Myndigheten för yrkeshögskolan har en [databas över kvalifikationer](https://www.myh.se/validering-och-seqf/seqf-sveriges-referensram-for-kvalifikationer/sok-kvalifikationer/sa-fungerar-databasen). Den bygger på [SeQF](https://www.myh.se/validering-och-seqf/seqf-sveriges-referensram-for-kvalifikationer). Dessa kvalifikationer kopplas sedan till det europeiska ramverket EQF. Data nås [här](https://www.dataportal.se/sv/datasets/499_2379/kvalifikationer-sverige#ref=?p=1&q=seqf&s=2&t=20&f=&rt=dataset%24esterms_IndependentDataService%24esterms_ServedByDataService&c=false).
Databasen kommer att fyllas på med kvalifikationer från det formella utbildningssystemet. Först YH-utbildningar och senare universitetsutbildningar.
Det finns inga planer på att koppla läranderesultat mot ESCO eller AF-taxonomi. Datat ägs av utbildningsägarna.

# Utbildning

## Antagningsstatistik

UHR ger tillgång till [antagningsstatistik](https://www.uhr.se/studier-och-antagning/antagningsstatistik/) för universitetsutbildningar. Tyvärr har det inget officiellt API.

## Susanavet

Skolverkets aggregering av utbildningsinformation [SUSA-navet](https://utbildningsguiden.skolverket.se/om-oss/susa---data-om-utbildningar). Det har också ett [API](https://susanavet2.skolverket.se/#/api/). SUSA-navet bygger på EMIL-standarden. Tidigare erfarenheter visar att in formationen är ofta knapphändig, för mer information behöver man gå till utbildningsanordnaren.

## Education API

Skrapad och berikad utbildningsinformation via ett [API](https://education-api-test.jobtechdev.se/). Informationen kopplar utbildning till yrke där så är möjligt. [Demo-tjänst](https://demo-education-dev.jobtechdev.se) för education-api.

## JobEd Connect API
Matchningslösning som automatiskt kopplar ihop utbildningar med relaterade yrken, utifrån vad som efterfrågas på arbetsmarknaden

* [JobEd Connect API](https://jobtechdev.se/sv/komponenter/jobed-connect)
* [Demo](https://demo-jobed-connect.jobtechdev.se/)
* [Test API](https://jobed-connect-api-test.jobtechdev.se/)

## Arbetsmarknadsutbildningar

* [Yrkes akademin](https://ya.se/arbetsmarknadsutbildning/)
* [Lernia](https://www.lernia.se/utbildning/arbetsmarknadsutbildning/)
* [AF registrerade utbildningar via Mulesoft](https://eu1.anypoint.mulesoft.com/exchange/1dae6e32-bf25-4b52-aa03-8f32e1f5fc08/tlrs-aub-for-asok/) dessa finns i [EMIL-format på data.jobtechdev.se](https://data.jobtechdev.se/utbildningar/index.html)

## Kompetensmatchning.se

[Kompetensmatching.se](https://kompetensmatchning.se/) är en portal med utbildningar skapad i ett RISE projekt.

# Personlighet

Det öppna testet [Big 5](https://bigfive-test.com/) om personlighetsdrag. Metodiken och förslag på frågor på engeska finns [här](https://ipip.ori.org/new_ipip-50-item-scale.htm).

# Standarder

List av standarder av intresse för projektet.

* [EMIL-standarden](https://www.sis.se/standardutveckling/tksidor/tk400499/sistk450/emil-standarden/) - för information om utbildningar.
* [HR Open Standards](https://www.hropenstandards.org/) - Standarder kring att representera CV, meriter, och annan HR-data.
* [Europass XML](https://joinup.ec.europa.eu/collection/employment-and-working-conditions/solution/europass-xml-schema-v30/about) - Europass XML för CV
* [JSON resume](https://jsonresume.org/) - Enkelt JSON schema för spara CV.

# Blandade efterfrågade data

* Mikromeriter och gradvis lärande.
* Meriter och sammanhang - samma merit men i olika sammanhang kan ha olika betydelse.

# Arbetssökandeprofil REST API

AFs interna API, exempelfil ligger [här](https://gitlab.com/arbetsformedlingen/dy/digital-yrkesvaegledning/-/blob/master/examples/arbetssokandeprofil.json).
<details>
   <summary>Klicka här för att se ett kortare exempel</summary>

```
[
  {
    "id": 0,
    "anvandarId": 0,
    "namn": "string",
    "beskrivning": "string",
    "matchningsprofilId": 0,
    "senastUppdaterad": "2023-02-28T09:09:15.775Z",
    "publicerad": false,
    "profilstatus": "UTKAST",
    "presentation": "string",
    "styrka": 0,
    "euresMedgivande": false,
    "euresExport": false,
    "utbildningar": [
      {
        "id": 0,
        "arbetssokandeprofilId": 0,
        "grundmeritId": 0,
        "rubrik": "string",
        "beskrivning": "string",
        "senastUppdaterad": "2023-02-28T09:09:15.775Z",
        "sorteringsordning": 0,
        "startdatum": {
          "artal": 0,
          "manad": 0
        },
        "slutdatum": {
          "artal": 0,
          "manad": 0
        },
        "pagaende": false,
        "skola": "string",
        "niva": "string",
        "nivaConceptId": "string",
        "inriktning": "string",
        "inriktningConceptId": "string"
      }
    ],
    "sprak": [
      {
        "id": 0,
        "arbetssokandeprofilId": 0,
        "grundkriterieId": 0,
        "namn": 0,
        "namnConceptId": "string",
        "niva": 0,
        "nivaConceptId": "string",
        "senastUppdaterad": "2023-02-28T09:09:15.775Z"
      }
    ],
    "anstallningar": [
      {
        "id": 0,
        "arbetssokandeprofilId": 0,
        "grundmeritId": 0,
        "rubrik": "string",
        "beskrivning": "string",
        "senastUppdaterad": "2023-02-28T09:09:15.775Z",
        "sorteringsordning": 0,
        "startdatum": {
          "artal": 0,
          "manad": 0
        },
        "slutdatum": {
          "artal": 0,
          "manad": 0
        },
        "pagaende": false,
        "arbetsgivare": "string",
        "yrkeserfarenhetIdLista": [
          0
        ]
      }
    ],
    "ovrigaMeriter": [
      {
        "id": 0,
        "arbetssokandeprofilId": 0,
        "grundmeritId": 0,
        "rubrik": "string",
        "beskrivning": "string",
        "senastUppdaterad": "2023-02-28T09:09:15.775Z",
        "sorteringsordning": 0,
        "startdatum": {
          "artal": 0,
          "manad": 0
        },
        "slutdatum": {
          "artal": 0,
          "manad": 0
        },
        "pagaende": false,
        "anordnare": "string"
      }
    ],
    "anstallningsvillkor": {
      "anstallningstyper": [
        0
      ],
      "anstallningstyperConceptId": [
        "string"
      ],
      "omfattningMin": 0,
      "omfattningMax": 0
    },
    "yrkesroller": [
      {
        "id": 0,
        "arbetssokandeprofilId": 0,
        "kod": "string",
        "kodConceptId": "string",
        "kodtyp": "YRKESOMRADE",
        "senastUppdaterad": "2023-02-28T09:09:15.775Z"
      }
    ],
    "kompetenser": [
      {
        "id": 0,
        "arbetssokandeprofilId": 0,
        "taxonomiId": 0,
        "taxonomiConceptId": "string",
        "ovrigaMeriter": [
          {
            "id": 0,
            "arbetssokandeprofilId": 0,
            "grundmeritId": 0,
            "rubrik": "string",
            "beskrivning": "string",
            "senastUppdaterad": "2023-02-28T09:09:15.775Z",
            "sorteringsordning": 0,
            "startdatum": {
              "artal": 0,
              "manad": 0
            },
            "slutdatum": {
              "artal": 0,
              "manad": 0
            },
            "pagaende": false,
            "anordnare": "string"
          }
        ],
        "utbildningar": [
          {
            "id": 0,
            "arbetssokandeprofilId": 0,
            "grundmeritId": 0,
            "rubrik": "string",
            "beskrivning": "string",
            "senastUppdaterad": "2023-02-28T09:09:15.775Z",
            "sorteringsordning": 0,
            "startdatum": {
              "artal": 0,
              "manad": 0
            },
            "slutdatum": {
              "artal": 0,
              "manad": 0
            },
            "pagaende": false,
            "skola": "string",
            "niva": "string",
            "nivaConceptId": "string",
            "inriktning": "string",
            "inriktningConceptId": "string"
          }
        ],
        "anstallningar": [
          {
            "id": 0,
            "arbetssokandeprofilId": 0,
            "grundmeritId": 0,
            "rubrik": "string",
            "beskrivning": "string",
            "senastUppdaterad": "2023-02-28T09:09:15.775Z",
            "sorteringsordning": 0,
            "startdatum": {
              "artal": 0,
              "manad": 0
            },
            "slutdatum": {
              "artal": 0,
              "manad": 0
            },
            "pagaende": false,
            "arbetsgivare": "string",
            "yrkeserfarenhetIdLista": [
              0
            ]
          }
        ],
        "senastUppdaterad": "2023-02-28T09:09:15.775Z"
      }
    ],
    "yrkeserfarenheter": [
      {
        "id": 0,
        "arbetssokandeprofilId": 0,
        "yrkeserfarenhetGpId": 0,
        "yrkesbenamning": 0,
        "yrkesbenamningConceptId": "string",
        "erfarenhet": 0,
        "erfarenhetConceptId": "string",
        "anstallningIdLista": [
          0
        ],
        "senastUppdaterad": "2023-02-28T09:09:15.775Z"
      }
    ],
    "arbetsorter": [
      {
        "id": 0,
        "arbetssokandeprofilId": 0,
        "arbetsortTyp": "LAND",
        "varde1": "string",
        "varde1ConceptId": "string",
        "varde2": "string",
        "varde2ConceptId": "string",
        "senastUppdaterad": "2023-02-28T09:09:15.775Z"
      }
    ],
    "korkort": {
      "korkortsklasser": [
        "string"
      ],
      "korkortsklasserConceptId": [
        "string"
      ],
      "senastUppdaterad": "2023-02-28T09:09:15.775Z"
    },
    "kontaktuppgiftmask": {
      "id": 0,
      "arbetssokandeprofilId": 0,
      "fornamn": false,
      "efternamn": false,
      "epostadress": false,
      "telefonHem": false,
      "telefonMobil": false,
      "telefonOvrig": false,
      "adress": false,
      "postnummer": false,
      "postort": false,
      "coAdress": false,
      "land": false,
      "hemsidaUrl": false,
      "senastUppdaterad": "2023-02-28T09:09:15.775Z"
    }
  }
]
```

</details>