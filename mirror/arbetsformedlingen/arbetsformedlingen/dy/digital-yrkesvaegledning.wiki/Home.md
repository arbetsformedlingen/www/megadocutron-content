---
title: Bakgrund
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/-/blob/main//Home.md
gitdir: /arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki
gitdir-file-path: /Home.md
date: '2023-10-16 14:52:21'
path: /arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/Home.md
tags:
- Home.md
---
## Bakgrund

Samhället pratar om behovet av omställning i mitten av yrkeslivet. Om vi utgår från individen Agne som befinner sig mitt i livet. Agne har ett yrke som det är svårt att få arbete inom, kanske har arbetsskador som gör att hen inte kan fortsätta inom yrket eller bara vill prova något nytt. Agne har massor med kompetens och utbildning inom yrket. Delar av denna kan vara till stor nytta inom andra yrken. Agne är osäker på hur hens kompetens kan komma till nytta i andra yrken.

Utbildningsbeskrivningar kan ofta upplevas abstrakta; Vilka yrken kan man få? Vad innebär det att arbeta inom yrket? Hur ser arbetsmarknaden ut, både nu och i framtiden? Vilka utbildningar tar vara på Agnes kompetens och bygger vidare på den?

Genom att knyta utbildningar till platsannonser och arbetsmarknadstrender kan dessa frågor bli enklare att besvara.

Exempel på frågeställningar och utmaningar som kommer omhändertas inom projektet:

* Men man kan då också vända på frågan; De här platsannonserna ser intressanta att verka inom. Vad behöver jag göra för att kunna få dessa arbeten?
* Vi pratade mycket om datakvalitet och vikten av att säkra kvalitet på den data och de resultat som ges. Vi tror att det saknas mycket data som skulle behövas. Utbildningsbeskrivningar är ofta väldigt grovkorniga (Kan mikromeriter hjälpa?) Vad har man för kultur kring data och hur förhåller man sig till tillgänglighet och kvalitet?
* Meriter är ofta beroende på sammanhang. Kan vi fånga dessa?
* Många utbildningar är inte direkt kopplade till en yrkesroll, exempelvis civilingenjör, litteraturvetare. Hur skall gradvis lärande fångas som man exempelvis får i yrkeslivet? Hur har man lärt sig nya verktyg, tekniker, processer? Hur lär man sig olika kompetenser?
* Nationella planer löser mycket för grund- och gymnasieskolor men det är svårare med högskolekurser, myh etc. Vi har idag bara koppling mellan yrke och kompetens på hög nivå, går det få på en lägre mer granulär nivå. Vilka datamängder behöver vi, hur detaljerade måste de vara?

## Syfte

Syftet är att försöka att ta fram en POC för att:

* verifiera att de olika datamängderna hänger ihop
* bedöma kvalitet av datat
* finna behov av annan data att koppla samman med datamängderna.
* demonstrera hur datat kan användas

## Mål

Att ta fram en POC som svarar upp mot syftet, och som visar aktörer hur de kan använda datamängder och api:er inom KLL-området, för att stärka Agne i sin karriärväg.

Exempel på aktörer:

* Arbetsförmedlingen
* Skolverket
* Studie- och yrkesvägledare
* Omställningsorganisationer
* Matchningsaktörer
* Utbildningsanordnare

## Förväntat resultat

_En POC som utgår från följande användningsfall:_

Agne är inne på Platsbanken och tittar vad det finns för arbeten. Agne hitta några intressanta arbeten men tänker ”Jag är inte behörig till dem”. Då ser Agne en knapp jämt platsannonsen med texten ”Ta reda på hur du kan bli behörig till detta yrke. Agne klickar på knappen.

Hen kommer till sidan kompetensmatchning.se. Sidan visar platsannonsen och också en lista på utbildningar som leder till det yrket. Men Agne tänker ”Jag har ju redan massor av den kunskapen, vad är det jag saknar?”

Agne Klickar på ”Anpassa utbildningar för mig”. Där för Agne skriva in sitt CV, utbildningar och andra meriter. Tillbaka får Agne en personligt anpassad lista av utbildningar.

Agne ser att det inte är så mycket som saknas, det borde gå att fixa.

### Bakom den glättiga ytan

I användningsfallet ovan behöver utbildningsdata, platsannonsdata samverka. Grunden för det bör vara de lärdomar som dragits i semantikprojektet och berikningen. Data kommer behöva valideras.

När Agne kommer till Platsbanken finns redan klassificering av annonsen med yrkeskoder. Dessa tar vi med för att välja utbildningar att visa. Här har Af-Jobtech (team Revival) gjort visst initialt arbete med att klassificera utbildningar med hjälp av berikningen. Det borde ”bara” vara att matcha dessa koder mot varandra för att ge lämpliga utbildningar.

Nästa steg är att personifiera listan. Med några snabba tester så verkar berikningen även kunna berika CV-texter.

Kan vi nu använda den berikade CV informationen för att bedöma vilka utbildningar som fyller gapet mellan Agnes erfarenheter (CV) och den platsannons som beskriver Agnes önskan om nytt yrke?

En viktig aspekt i projektet är att ta hänsyn till individens integritet och minimera datainsamlandet. Användaren själv ska ha kontroll på sin data och behöver dela med sig så lite som möjligt.

![nu_vs_sedan](uploads/e6480a6fb6256a35d70e2fe16f67318f/nu_vs_sedan.jpg)
![nu_vs_sen_plus_utbildning](uploads/08739615f7432689fcbf6eed0da2e3cb/nu_vs_sen_plus_utbildning.jpg)