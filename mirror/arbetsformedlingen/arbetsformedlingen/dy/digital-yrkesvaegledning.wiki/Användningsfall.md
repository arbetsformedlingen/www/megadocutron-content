---
title: Övergripande visionärt projektanvändningsfall
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/-/blob/main//Användningsfall.md
gitdir: /arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki
gitdir-file-path: /Användningsfall.md
date: '2023-10-16 14:52:21'
path: /arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/Användningsfall.md
tags:
- Användningsfall.md
---
# Övergripande visionärt projektanvändningsfall

## Användningsfall
Agne Har jobbat som grafisk designer i 10 år.  är inne på Platsbanken (eller liknande tjänst) och tittar vad det finns för arbeten.  det finns 136 814 jobb. ser knappen få jobb förslag baserad på din CV.

Skapar sin CV med hjälp av färdiga taggar och förslag. han även fyller i sina personliga förmågor och intresse. Då få han 12 förslag på jobb som han kan söka nu,+ 67 närliggande jobb. trycker på närliggande jobb.

Agne hitta några intressanta arbeten men tänker, jag har aldrig tänkt på de. varför har jag fått de som förslag.

trycker på "Om resultat". han har fått resultatet baserad på hans intresse förutom hans jobb färdigheter.
 Han spelar schack (problem lösare)
+ han deltar frivilligt i olika Humanitära nätverk (känner empati)
+ grafisk design kopmpetens
= UX-designer

 ”Jag är inte behörig till yrken”. Då ser Agne en knapp jämt platsannonsen med texten ”Ta reda på hur du kan bli behörig till detta yrke. Agne klickar på knappen.
Hen kommer till sidan kompetensmatchning.se. Sidan visar platsannonsen och Agne Klickar på ”Anpassa utbildningar för mig .Tillbaka får Agne en personligt anpassad lista av utbildningar baserad på hans CV för att bli UXare. Han ser olika alternativ från universitet och yrkesutbildningar. Då ser att det inte är så mycket som saknas, det borde gå att fixa.
han rekommenderar tjänsten till andra.

## Analys
Vi kommer inom projektet inte ha möjlighet att ha knappen på platsbanken som beskrivs ovan. Det skulle kräva en längre kedja med förankring. Vi behöver för ta denna investering veta att kvaliteten är tillräckligt bra.

Långsiktigt skulle man vilja kunna använda Connect Once för att hämta CV från AF, om så önskas av användaren. Steg 1 får man mata in CV manuellt.

# Lämpligt användningsfall för POC

1. Agne hittar en platsannons på Platsbanken.se. Agne kopierar länken till annonsen.
2. Agne går till kompetensmatchning.se/vägledning och klistrar in länken till annonsen. _Bakom kulissen så anropas JobTechs berikning för att identifiera kompetenser mm som förväntas i annonsen._
3. Agne blir ombedd att mata in sitt CV. Fälten är en rubrik och beskrivning av rollen för varje roll han haft. (Steg 2: Det finns också möjlighet att lista andra kunskaper.) _Även CV:t postas till JobTechs berikning för att identifiera Agnes kompetenser utifrån meriter._
4. Webbsiten presenterar vilka kompetenser som platsannonsen söker, vilka av dessa Agne redan har och vilka som saknas. (Hur gör man denna visualisering enkelt förståbar?)
5. Agne klickar på en knapp "Utbildning för saknad kompetens"
6. Bilden från 4 uppdateras med vilka utbildningar ger saknade kompetenser och vilka av de saknade kompetenserna den ger.
7. (Steg 2) För varje utbildning visas antagningskrav, och om möjligt en indikation om Agne klarar dessa.

## Analys

Användningsfallet för projektet är anpassat för att inte vara beroende av AF platsbanken och min profil direkt utan så projektet kan arbeta oberoende.
Utmaning är hur visualisering av kompetenser kan göras. Skall besökaren ha möjlighet att korrigera identifierade kompetenser?
I steg 6 bör det tydligt påpekas att korrektheten inte är garanterat och att man bör ta hjälp av studie/yrkesvägledare. 

Varje punkt går att utveckla som en story med start uppifrån.
