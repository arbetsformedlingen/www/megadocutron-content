---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/-/blob/main//Avgränsning.md
gitdir: /arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki
gitdir-file-path: /Avgränsning.md
date: '2023-10-16 14:52:21'
path: /arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/Avgränsning.md
tags:
- Avgränsning.md
---
För att begränsa problemet så behövs det införas begränsningar
inom projektet. Följande begränsningar är redan gjorda:

* För individer mitt i yrkeslivet - Det förväntas att användaren har
yrkeslivserfarenhet. Det finns redan många tjänster för de som
är nya på arbetsmarknaden.

* Projektet jobbar i snittet mellan individens kompetens, arbetsmarknadens behov
  och utbildningssektorns erbjudande.

* Proof of concept - Det kommer inte byggas en färdig slutanvändartjänst
då vi riktar in oss på demonstrera funktionen och utvärdera
kvalitet på den datainfrastruktur som finns. Det finns ej heller utrymme
för säkerhetsresor mm inom projektet.

* Externa utvecklingspartners - Ett delsyfte är att arbeta med externa utvecklingspartners för att utvärdera om JobTechs infrastruktur och dokumentation är användbara för externa utvecklingsorganisationer.  (Många liknande intiativ handlar om att AF bygger ytterligare egna tjänster.)

* Inspirera andra aktörer(exvis TRR) i hur JobTechs mfl datainfrastruktur kan användas för att bygga tjänster.
* Det är rimligt att begränsa till en ren svensk context. Det finns tillräckligt  med utmaningar att sammanföra data där. Därmed inte prioritera det arbete som sker på Europeisk nivå med [Europass](https://europa.eu/europass/en) och [EuRes](https://eures.ec.europa.eu/index_en).

Möjliga ytterligare begränsningar att införa:

* Många universitetsutbildningar är väldigt generella och ger möjlighet
till många olika områden inom arbetsmarknaden. Det är troligtvis bättre
att fokusera på mer inriktade utbildningar inom MYH, arbetsmarknadsutbildningar etc.



