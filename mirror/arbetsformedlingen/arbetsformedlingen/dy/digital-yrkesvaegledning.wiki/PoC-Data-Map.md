---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/-/blob/main//PoC-Data-Map.md
gitdir: /arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki
gitdir-file-path: /PoC-Data-Map.md
date: '2023-10-16 14:52:21'
path: /arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/PoC-Data-Map.md
tags:
- PoC-Data-Map.md
---
_**(File is located at docs/studievagledningv2.drawio.svg. Gitlab renders well, but best to edit [online](https://app.diagrams.net/) or in [downloaded app](https://get.diagrams.net/))**_

![studievagledning_poc_2023.drawio.svg](https://gitlab.com/arbetsformedlingen/dy/digital-yrkesvaegledning/raw/master/docs/studievagledning_poc_2023.drawio.svg)