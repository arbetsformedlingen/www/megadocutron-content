---
title: About
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main//src/README.md
gitdir: /arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content
gitdir-file-path: /src/README.md
date: '2023-09-13 16:21:19'
path: /arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/src/README.md
tags:
- src::README.md
- README.md
---
## About
Creates one markdown file and one json file describing all concept attributes in the Jobtech Taxonomy. The attributes are mostly used in the Taxonomy GraphQL API. 

When adding or updating an attribute in the Jobtech Taxonomy database, the jobtech-taxonom-common deps will have to be updated for this project to consume the new information. 


## TODO
We should also show possible attribute-concept combinations for each version. 