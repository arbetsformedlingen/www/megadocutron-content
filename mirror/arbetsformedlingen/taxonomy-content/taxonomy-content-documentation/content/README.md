---
title: Om
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content
gitdir-file-path: /README.md
date: '2023-09-13 16:21:19'
path: /arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/README.md
tags:
- README.md
---
## Om


<*All information här är under uppbyggnad*>

Här samlas dokumentation om innehållet i Jobtech Taxonomy.


- [Dokument på svenska](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/tree/main/doc_sv)
    - [Yrkesbenämningar](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar_ssyk.md#yrkesben%C3%A4mningar)
    - [AF-SSYK](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/af_ssyk.md)
- [Metadataattribut som används i Jobtech Taxonomy-databasen](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/tree/main/attributes)
- [Jobtechs forum](https://forum.jobtechdev.se/)
- [Lämna förslag om nya yrkesbenämningar, kompetensbegrepp eller dylikt](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-feedback-channel)
- [Detaljerad versionhistorik för Jobtech Taxonomy](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/taxonomy-version-history)

## Licens
Allt innehåll (begrepp, relationer och strukturer) inom Jobtech Taxonomy, och kringliggande information om innehållet, är licensierat under [CC0](https://creativecommons.org/publicdomain/zero/1.0/)

## Kontakt
Team Redaktionen på Enheten Jobtech, Arbetsförmedlingen: 3912teamredaktionen@arbetsformedlingen.se
