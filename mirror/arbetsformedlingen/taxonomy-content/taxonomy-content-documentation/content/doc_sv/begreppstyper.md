---
title: Begreppstyper
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main//doc_sv/begreppstyper.md
gitdir: /arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content
gitdir-file-path: /doc_sv/begreppstyper.md
date: '2023-09-13 16:21:19'
path: /arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/begreppstyper.md
tags:
- doc_sv::begreppstyper.md
- begreppstyper.md
---
# Begreppstyper

## Definition
[Attribut](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/attribut.md) som används för att kategorisera begrepp enligt typ. Även kallad `type` och `concept-type`. 

## Beskrivning
Samtliga begrepp i Taxonomy har en begreppstyp. En fullständig lista över begreppstyper, i den senaste versionen, kan hittas [via Taxonomy-API:et](https://taxonomy.api.jobtechdev.se/v1/taxonomy/main/concept/types). 

Bland begreppstyperna finns bland annat `occupation-name`, även kallad [yrkesbenämning](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md), och `skill`, även kallad [kompetensbegrepp](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/kompetensbegrepp.md).

Nya begreppstyper kan tillkomma i samband med att nya Taxonomy-versioner skapas.
