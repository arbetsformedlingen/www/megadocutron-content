---
title: Kompetensbegrepp
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main//doc_sv/kompetensbegrepp.md
gitdir: /arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content
gitdir-file-path: /doc_sv/kompetensbegrepp.md
date: '2023-09-13 16:21:19'
path: /arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/kompetensbegrepp.md
tags:
- doc_sv::kompetensbegrepp.md
- kompetensbegrepp.md
---
# Kompetensbegrepp

<*Under uppbyggnad*>


- kopplingar till andra begrepp (ssyk, yrkesbenämning, esco)
- *kompetens* som begrepp
- Kompetensbegrepp utan ESCO-mappningar

## Definition

## Beskrivning

## Begränsningar

- idag mycket bred användning av begreppet "kompetensbegrepp"
- hur ser användningen ut (bland annat vid annonsering)
- brist på struktur och avgränsningar
- brist på definitioner
