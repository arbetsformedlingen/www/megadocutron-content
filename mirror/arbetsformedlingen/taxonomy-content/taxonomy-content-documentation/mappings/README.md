---
title: Files
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/mappings/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/mappings
gitdir-file-path: /README.md
date: '2023-04-17 12:27:41'
path: /arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/mappings/README.md
tags:
- README.md
---
## Files

[AF-SCB-diff.xlsx](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/mappings/-/blob/main/AF-SCB_diff.xlsx): Mappings between SSYK groups that differ between the SCB-SSYK and AF-SSYK. Groups that are not mentioned are identical in the two structures. 
