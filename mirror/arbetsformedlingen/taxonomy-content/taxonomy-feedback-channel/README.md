---
title: Feedback avseende innehållet i Taxonomy
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-feedback-channel/-/blob/main//README.md
gitdir: /arbetsformedlingen/taxonomy-content/taxonomy-feedback-channel
gitdir-file-path: /README.md
date: '2023-03-27 12:52:26'
path: /arbetsformedlingen/taxonomy-content/taxonomy-feedback-channel/README.md
tags:
- README.md
---
# Feedback avseende innehållet i Taxonomy

Under Issues/Board till vänster finns en ärendetavla för feedback gällande innehållet i Taxonomy. 

Se https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-feedback-channel/-/boards/4013239
