---
title: Readme
gitlaburl: https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-lab/occupations-clustering/-/blob/main//readme.md
gitdir: /arbetsformedlingen/taxonomy-content/taxonomy-lab/occupations-clustering
gitdir-file-path: /readme.md
date: '2023-10-02 18:19:39'
path: /arbetsformedlingen/taxonomy-content/taxonomy-lab/occupations-clustering/readme.md
tags:
- readme.md
---
# Readme

## Disclaimer

*The code is very much a work in progress and has thus far only been used for testing ideas.*

## Usage



```
git clone git@gitlab.com:arbetsformedlingen/taxonomy-content/taxonomy-lab/occupations-clustering.git
```



```
pip install -r requirements.txt
```



If gathering new skills and occupations data download ads (json-l) from here:

```
https://data.jobtechdev.se/annonser/historiska/berikade/index.html
```

Extract them to `data/ads`

And then follow the steps outlined under "Data" headline below.

All functions will then have to be run through your IDE. 

For testing I have basically just extracted information from a set of ads and pickled it for ease of access in order for the actual clustering to work more smoothly.

## Overview

### The idea

The idea behind this piece of software is to test the feasibility of using skills (through enriched ads) to cluster occupation-name concepts from the Taxonomy. 

The enriched ads all have an id for an occupation-name concept and potentially one or more skill and/or trait concepts found through the text enrichment. 

Skills "within" an occupation (seen together with an occupation in the ads) will be counted and the count normalised by how often the occupation occurs within the dataset. 

Each occupation will be represented by a vector with the same length as the number of skills and/or traits found in the dataset, where each skill represents a feature. This will result in a _very_ sparse matrix. Some suitable metric (like cosine) will be used for calculating the distance between each pair of occupations in order to construct a distance matrix, outlining the distances between each occupation. 

The idea is to then apply a clustering method to either the feature vectors or the distance matrix to see if the enriched skills can act as an aggregated affinity measurement between the occupations. If clusters are found they will have to be processed manually. Finally skills in each cluster will be ranked and presented to a potential user.

## Structure

Not very well defined as of now but most importantly `init_files`stores files necessary for initiliasing the clustering. Two sets of files can be found there by default: one with a large set of occupations (no suffix) and one with a small (suffixed with "sample") set of occupations chosen manually.

The `data`repository stores ads. 

Three separate directories are used to store individual yearly files containing extracted data per ad file. Files in each directory are then merged and used for initialising the clustering.  These directories are `occupation_files_by_year`,  `skill_files_by_year` and `trait_files_by_year`.

### cluster.py

Stores the clustering class. *Does not actually do any clustering at this point*. Some serious cleaning is needed here. 

### data.py 

Data gathering happens here, see more under "Data" below. Documentation a bit unfinished here.

## Data

The purpose of the functions found here is to create three files:

- A file containing a dictionary with all occupations and skills found in the ads (specified by year), where each skill and occupation 	combination is counted, as well as the skill's total. This file will be used to initialize the clustering
- A file where each occupation is counted. This file will act as a filter for occupations that are specified rarely in the ads
- A file where each unique trait found throughout the ads.

The files used here are the enriched historical ads from the repository found here: https://data.jobtechdev.se/annonser/historiska/berikade/index.html

Since the files containing ads are quite heavy this part of the software is used to aggregate data from them (the amount of data we need is quite small). Ad data from each year is pickled (for simplicity) and then all yearly files are merged to "init_files" used to run the clustering. This will later make the clustering run fast without having to rerun the ad files. Gathering of new ad data (if for instance a filter for occupations needs to be applied) will however take some time (around 5 minutes).

The attribute specify_occupations can be used if it is necessary to use a reduced set of occupations. Note that some occupations that are present in ads from 2016 (and parts of 2017?) will not be found in the "new" Taxonomy since they were removed before the first version of the new Taxonomy was imported. 

All files necessary for the clustering can be found in the init_files directory. These simply contains data from all ads 2016-2022. Occupations that have not been seen more than 50 times during a year are omitted from this dataset (note that this value could probably be lessened). 

### Gathering new data

It is possible to gather new data from the ads through following steps. 

*note that this is a bit clunky at the moment since new data need to be gathered when wanting to i.e. use a subset of occupations*

**Step 1:**

First you need to download the files containing ads: https://data.jobtechdev.se/annonser/historiska/berikade/index.html 

The json-l files for each year needs to be extracted to `data/ads`.

**Step 2:**

```python
create_occupations_count_year_file("2016", specify_occupations=None) # or use a list with concept-ids of occupations to only include them in the dataset
```

Run this function for the years you want to include. Note that the files containing ads are quite heavy so the function should preferably be repeated for one year at a time. When done, the files necessary have populated the directory `occupation_files_by_year`. 

If you want to gather data from a subset of occupations you can pass their concept-ids to the function as a list:

```python
specified_occupations = ["eU1q_zvL_9Rf", "rM7G_ge7_XhP","rQds_YGd_quU", "fg7B_yov_smw", "i1F4_cZZ_PJu", "ZnNn_D2K_pTi", "TCGb_VdF_NSm", "1Y6K_Kdg_i5n", "Zr47_fdg_Cho","usVe_MdP_oJA", "PUhB_62A_bJi", "98j2_zec_Ufr"]

```

And then:

```python
create_occupation_count_year_file("2016",  specify_occupations=specified_occupations)
```

**Step 3:**

```python
merge_occupation_files("init_files/occupations_count.pickle", "occupation_files_by_year")
```

Now the init-file has been created. The function merges all yearly files found in the specified directory. 

**Step 4:**

```python
create_skills_count_year_file("2016")
```

Repeat for the same years used when extracting the occupations above. 

**Step 5:**

```PY
merge_skill_files("init_files/skills_count.pickle", "skill_files_by_year")
```

Acts in a similar way as the occupation-merger above.

**Step 6:**

```PYTHON
merge_trait_files("init_files/traits.pickle", "trait_files_by_year")
```

This function will create a set containing each trait found throughout the ads. The individual yearly files are created along with the yearly skill files. 

## Filtered skills

The enrichment service is primarily built for search functionality, which is why it is quite generous when classifying terms in ads. Many terms classified as skills may not be needed for the objectives of this software. 

Filtered skills are usually of the following types:

- a language (for example "svenska" is used in a lot of ads)
- a qualification such as "gymnasieutbildning"
- highly specified terms that are very commonly found in the ads but not occupation-specific such as driving license
- vague terms found in many ads, such as "arbetslivserfarenhet"

Many of the filtered skills could be of interest for other use cases but here they just seem to skew the data, or overestimate the likeness between two occupations since the filtered terms are usually found in a large amount of ads but might not characterise the occupation. The list should be taken with a grain of salt since it is based on a lot of personal assumptions while trying to reduce the noise of the data. 

## Clustering

### Using the clustering methods

Try something like:

```python
cluster = Cluster(skills_and_occupations="init_files/skills_count_sample.pickle", 
                  occupations_count="init_files/occupations_count_sample.pickle",
                  traits="init_files/all_traits_sample.pickle",
                  metric="cosine",
                  skill_type="skill")
```

```python
cluster.plot_data(cluster.distance_matrix_transformed, cluster.labels)
```

To see how the distance_matrix projects itself in 2 dimensions. 

The call below will return all skills and or traits for the occupation "Fritidsledare".

```python
pprint(cluster.intensities["ZnNn_D2K_pTi"])
```

Calculate the distance between two specific occupations (in this example "Fritidsledare" and "Personlig assistent"):

```python
feature_vectors = cluster.feature_vectors
labels = cluster.labels
# occupation_vectors holds a dict of all occupation-name concept-ids and their feature vectors
occupation_vectors = cluster.map_occupations_to_feature_vectors(feature_vectors, labels)

fritidsledare = occupation_vectors["ZnNn_D2K_pTi"]
personlig_assistent = ["eU1q_zvL_9Rf"]

eucld_distance = u.calculate_eucl_distance(fritidsledare, personlig_assistent)

print(eucld_distance)

# u.calculate_cos_similarity will also work here
```

### Matrix

There are 2 matrices that can be used:  `feature_vectors`and `distance_matrix`

The feature are used in creating the distance_matrix, but can also be used for clustering if the clustering method does not work well with a distance matrix. 

## Known issues

- Occupations added since 2016-2017 (?) are not present in the ads-dataset
- Taxonomy skills can probably not be used since they occur very infrequently throughout the ads
- This software **should for now really not be used on python versions before 3.7** since it uses a lot of operations assuming the order of items in dictionaries stays the same. This needs to be fixed.
- Change to sklearn for the pairwise metrics instead of doing it manually

## TODO 

- Make function(s) to compare only two vectors and give back the distance between them
- Use ordereddict + defaultdict to retain order of dict even in older python versions
- Get all languages from the taxonomy for filtering
- Clean up and document the attributes, most are used for testing
- Improve the data gathering process
- Make it possible to specify occupations in the clustering itself (now it is tied to the data gathering, which makes it a bit cumbersome)
- Some functions in data.py can be improved/simplified quite a bit, especially `create_skills_stats `

## Tested so far

- Dimensional reduction through MDS gave worse results for some reason
- Density based clustering through DBscan seems to give best results so far (also tried Kmeans, Kmedoids and hierarchical clustering) but I am not sure that really means something at this point.

## Ideas for further testing

Main issue at this point seems to be with the data or the processing of it and not with the clustering. 

Some interesting things that came up during a meeting 2022-11-22 (also see meetings dir):


- aggregate skills: pair individual **terms** found throughout the ads with their **concepts** in the synonym dictionary. See "synonymdictionary"-endpoint here: https://jobad-enrichments-api.jobtechdev.se/
- see if PCA can be used in order to reduce the skills vectors

We also talked about ways of testing the findings:

- a way to test the results could be to see how well the findings corresponds with ssyk-level-4 or occupation-field concepts

Also:

- Another possibility would be to explore how the semantic-search service could be used: https://semantic-concept-search-semantic-concept-search-prod.prod.services.jtech.se/
  - There are probably a number of ways in which it could be utilised:
    - Perhaps to find new occupation-name concepts through the enriched occupation terms?
    - Could the semantic vectors be used in some way in tandem with the skills intensities towards occupations?

- adjustText-library can be used to make the plot more readable
- UMap can also be used as a dimensional reduction technique, see here: https://umap-learn.readthedocs.io/en/latest/

