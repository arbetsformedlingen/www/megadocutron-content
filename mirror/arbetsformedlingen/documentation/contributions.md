---
title: Contributions to OpenSource by JobTech
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//contributions.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /contributions.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/contributions.md
tags:
- contributions.md
---
# Contributions to OpenSource by JobTech

This is a list of open source projects not owned by JobTech but where JobTech
and its employees has contributed.

|Project | Project description | Description of contribution |
|-|-|-|
| [Solid Community Server](https://github.com/CommunitySolidServer/CommunitySolidServer) | An open and modular implementation of the Solid specifications | Make resource store return more metadata. |
| [Kube-linter](https://github.com/stackrox/kube-linter) | KubeLinter is a static analysis tool that checks Kubernetes YAML files and Helm charts to ensure the applications represented in them adhere to best practices. | Improvements around HPA linting. |
| [Wanderung](https://github.com/lambdaforge/wanderung) |  Migration tool for [Datahike](https://github.com/replikativ/datahike) | Support migration to Datomic ([#6](https://github.com/lambdaforge/wanderung/pull/6)), minor fixes ([#10](https://github.com/lambdaforge/wanderung/pull/10), [#12](https://github.com/lambdaforge/wanderung/pull/12)). |
