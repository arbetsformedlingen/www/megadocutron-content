---
title: Getting started
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//templates/Readme-template-OSS.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /templates/Readme-template-OSS.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/templates/Readme-template-OSS.md
tags:
- templates::Readme-template-OSS.md
- Readme-template-OSS.md
---
!! Are you not working at Jobtechdev, you will not be able to set up this project !!

## Getting started
Intented target group, like developers, users of API etc.
What is possible to get going without beeing a part of AF
Releated projects/repos

### Pre-requisits and limitations
What do I need to set it up.
dependencies

### Local development setup
For developers of the project. How to set up an environment.
Should be OS (windows, wsl/linux, mac?) dependent but
IDE independent (not going deeper into setting up Intellij, VS-code etc.)

Don´t forget to add environment variables and config.
