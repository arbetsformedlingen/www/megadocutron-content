---
title: Elevator pitch
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//templates/Readme-template-API.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /templates/Readme-template-API.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/templates/Readme-template-API.md
tags:
- templates::Readme-template-API.md
- Readme-template-API.md
---
header links:
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![Relese][]][]
[![Community][]][]
[![OS][Windows, Mac, Linux]][]

#Elevator pitch


## Table of contents
	- About
	- Other documents
	- Project status
	- Using the API
	- Contributing & Contact
	- Licens
	- Acknowledgements


## About <project name>
Description of what the project does

Working at Jobtech, go here - looking for how to set up this service <link>
Link to changelog
Links to demo page
Links to live page
Swagger links?


## Other documents
other documents of interest
e.g. how the API works for external users

## Project status
Beta, active, deprecated, etc. (<= taggarna är bara place-holders)


### Using the API - eller liknande
How do I connect to the API (if any) and how to I get the data
Is there other documentation how to use the documentation,
there could be a link here.
Code examples - link to
Skisser över api:et / systemet.

## Contributing & Contact <link to contribute.md>
Can I contribute? And if so how?
1. Forum?
2. Om du är tekniskt bevandrad så skapa en issues
3. email?

## License
Short about the licens and a link to further reading.
