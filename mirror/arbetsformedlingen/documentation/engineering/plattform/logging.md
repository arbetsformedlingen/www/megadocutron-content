---
title: Logging
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//engineering/plattform/logging.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /engineering/plattform/logging.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/engineering/plattform/logging.md
tags:
- engineering::plattform::logging.md
- plattform::logging.md
- logging.md
---
# Logging

To collect logs from our application we uses the ELK-stack. We have two ELK clusters:

* [Test](https://opensearch-test.jobtechdev.se) - contains logs from our test/dev cluster
* [Prod](https://opensearch.jobtechdev.se) - contains logs from production and standby

Our on-prem cluster sends its logs to Arbetsförmedlingens log-plattform,
which is not discussed here.

## Authentication

Login to the ELK-servers using same credentials as for OpenShift(LDAP).

## What is logged?

All output to stdout and stderr from all services(pods) running on OpenShift
is collected and logged.
The logs are decorated with a large amount of metadata such as:

* timestamp
* node-name
* Kubernetes metadata:
  * namespace
  * pod-name
  * labels

All traffic into the cluster(ingress traffic) is logged. These logs
are parsed to simplify traffic analysis. Read more about this in
section [Request logs](#request-logs)

Logs are kept 7 days.

## Some interesting fields

Table below describe some generic and useful fields.

| Field | Description |
|-------|-------------|
| `@timestamp` | When the log event was captured by ELK. |
| `kubernetes.container.name` | Name of the container sent the event. A pod can contain multiple containers, in such case this is useful. |
| `kubernetes.labels.*` | Labels set on pod. |
| `kubernetes.namespace` | Namespace pod runs in. |
| `kubernetes.pod.name` | Pod name event came from. Useful to know what exact pod was involved. |
| `kubernetes.statefulset.name` | Name of stateful set. |
| `message` | Full unparsed message. |

## Request logs

All traffic into Kubernetes passes an ingress controller.
This ingress controller logs every call. These logs are analyzed
and structured by ELK.

To find requests to your application filter on `calamari.host` set
to the name of your service. For instance, to find all requests to
Jobsearch production filter on `jobsearch.api.jobtechdev.se`.

Within each of these log records from the ingress there are a number
of fields. All fields does not exist for all requests.

* `calamari.client-id` - Calling client id.
* `calamari.host` - Host field in the request
* `calamari.request.method` - Request method, e.g. GET or POST
* `calamari.request.path` - Request path without query parameters
* `calamari.request.query_params` - Query parameters. Each query parameter gets its own field within this object.
* `calamari.request.headers.api-key` - Value of `api-key` header, if set.
* `calamari.request.headers.from` - Value of `from` header, if set.
* `calamari.request.headers.referer` - Value of `referer` header, if set.
* `calamari.request.user-agent` - User agent string
* `calamari.response.status` - Response status code.


## FAQ

### Can I see from what cluster a log event came from?

Logs from both or production and standby environment are stored in production ELK.
Some times you might want to separate these logs.
These two environments can be separated using the `tags` field. If it contains
`aws_prod`, the log event is from the production cluster. If `tags` contains
`standby_aws`, the log event comes from the standby cluster.

### How do I found requests to my service and understand where they came from?

Log in to the ELK instance of interest. Select the hamburger menu in
the left upper corner and select Analytics -> Discover.
We will now filter out log events of interest. The easiest way to
find traffic to a service is to query the [Request logs](#request-logs).
First add a filter to only include traffic to your service. Use the filter `calamari.host: jobsearch.api.jobtechdev.se`. Replace the
DNS name with your service DNS name. Then select the time period
of interest in the top right corner next to the *Refresh* button

![ELK filter on service jobsearch.api.jobtechdev.se](images/ELK-calamari-host.png "ELK filter on service jobsearch.api.jobtechdev.se")

Next step is very much dependent on your needs. You can search
for request with specific path, query parameters etc. The response
code to the request can also be looked
up(`calamari.response.status`)
If you know that the client has set `api-key`, `from` or
`referer` header in their request you can filter on those. See
the fields named `calamari.request.headers.*` in ELK.
Read more about the information extracted in the request in
the section [Request logs](#request-logs).
