---
title: Development plattform
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//engineering/plattform/README.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /engineering/plattform/README.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/engineering/plattform/README.md
tags:
- engineering::plattform::README.md
- plattform::README.md
- README.md
---
# Development plattform

This file gives a brief overview of JobTechs development and operation platform.

JobTech main development and operation plattform is
[RedHat OpenShift](https://www.redhat.com/en/technologies/cloud-computing/openshift)
We have four clusters:

* [Test](https://console-openshift-console.test.services.jtech.se/dashboards) - used for development and testing
* [Production](https://console-openshift-console.prod.services.jtech.se/dashboards) - primary cluster for production, running on AWS
* [Standby](https://console-openshift-console.apps.standby.services.jtech.se/dashboards) - backup cluster for production, running on AWS
* [On-prem](https://console-openshift-console.apps.jocp.arbetsformedlingen.se/dashboards) - cluster for things that cannot run in cloud

We use the ELK-stack to collect and analyze our logs. We
have two instances:

* [Test](https://elk-test.jobtechdev.se) - for logs from our test cluster
* [Prod](https://elk.jobtechdev.se) - for logs from production and standby
* on-prem cluster sends its logs to Arbetsförmedlingens log plattform

What is logged and some of the most common use-cases are
discussed in [logging](logging.md) document

We manage our code on
[GitLab in the Arbetsförmedlingen group](https://gitlab.com/arbetsformedlingen).
GitLab is also used to manage tickets and backlogs.

We aim for automated build and deploy using GitOps principles.
Tekton is used for pipelines(build, test, deploy). To simplify and
solve most of our cases we have an opinionated abstraction called
[Aardvark](https://gitlab.com/arbetsformedlingen/devops/aardvark).
How we use it is described [here](aardvark-simple-cicd.md).
As a complement
[ArgoCD](https://argo-cd.readthedocs.io/en/stable/) is used
to deploy to clusters.

Or build artifact is most often container(docker) images.
These are stored in our container registry.
[Here](docker-registry.md) is more information on how to
use it.

Python projects can be delivered as packagas and the used by
others the same way as external dependencies are used, for
instance the elasticsearch module. We have the ability
to distribute our python packages via our own PyPi server.
Read more how to package your python programs correct
[here](python-packages.md) and how to use our own PyPi server.
