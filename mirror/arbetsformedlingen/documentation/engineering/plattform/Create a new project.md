---
title: Create a new Project/Namespace in Openshift
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//engineering/plattform/Create a new project.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /engineering/plattform/Create a new project.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/engineering/plattform/Create a new project.md
tags:
- engineering::plattform::Create a new project.md
- plattform::Create a new project.md
- Create a new project.md
---
# Create a new Project/Namespace in Openshift

This can be done either via GitLab web interface or local on your machine.

## Do the change on GitLab web interface

1. Go to the right file for the cluster you want to add a project/namespace to.
   * [test](https://gitlab.com/arbetsformedlingen/devops/ns-conf-infra/-/blob/main/kustomize/overlays/test/namespaces.yaml)
   * [production](https://gitlab.com/arbetsformedlingen/devops/ns-conf-infra/-/blob/main/kustomize/overlays/prod/namespaces.yaml)
   * [production standby](https://gitlab.com/arbetsformedlingen/devops/ns-conf-infra/-/blob/main/kustomize/overlays/prod-stdby/namespaces.yaml)
   * [production on-prem](https://gitlab.com/arbetsformedlingen/devops/ns-conf-infra/-/blob/main/kustomize/overlays/onprem/namespaces.yaml)
1. Click Edit.
1. Add a new section to the list. Place it so it is in alphabetic order by name.
   ```yaml
   - name: aardvark-demo-test
     team: calamari
    ```
    `name`is the name of the project. `team` is the name of the team, it must be
    one of calamari, incubator, individdata, revival, taxonomy, walrus, or
    yggdrasil. If it is an individual project, use the field `user` instead of team
    and assign it your AF-signature.
1. If your project will be deployed with ArgoCD do also add the label
   `argocd.argoproj.io/managed-by: openshift-gitops` so it looks like
    ```yaml
    - name: aardvark-demo-test
      team: calamari
      labels:
        argocd.argoproj.io/managed-by: openshift-gitops
    ```
    This makes ArgoCD get the right permissions.
1. Write a commit message in `Commit message`
1. Click `Commit changes`
1. You get a new page. Click `Assign to me`
1. Click `Create merge request` in the bottom of the page.
1. Wait to the build pass. A bot will automatically approve your change if it passes validation. Bots will also do comments on the merge request.
1. Once approved and pipelines have passed, merge your request by click `Merge`. The button does not appear before it is approved and pipelines have passed.
1. Within 10 minutes the namespace is created with permission for you
   and your team to administrate it.

## Do the change local on your machine

1. Clone or update your local copy of
[Namespace configurator infra repository](https://gitlab.com/arbetsformedlingen/devops/ns-conf-infra.git)
1. Create a branch locally
1. Open the `namespace.yaml` file for the cluster you want to create a
   project/namespace for. It is in a subdirectory to `kustomize/overlays` named
   after the cluster.
1. Add a new section to the list. Place it so it is in alphabetic order by name.
   ```yaml
   - name: aardvark-demo-test
     team: calamari
    ```
    `name`is the name of the project. `team` is the name of the team, it must be
    one of calamari, incubator, individdata, revival, taxonomy, walrus, or
    yggdrasil. If it is an individual project, use the field `user` instead of team
    and assign it your AF-signature.
1. If your project will be deployed with ArgoCD do also add the label
   `argocd.argoproj.io/managed-by: openshift-gitops` so it looks like
    ```yaml
    - name: aardvark-demo-test
      team: calamari
      labels:
        argocd.argoproj.io/managed-by: openshift-gitops
    ```
    This makes ArgoCD get the right permissions.
1. Commit your changes.
1. Push your branch to GitLab and create a Merge Request.
1. Wait to the build pass. A bot will automatically approve your change if it passes validation. Bots will also do comments on the merge request.
1. Once approved and pipelines have passed, merge your request.
1. Within 10 minutes the namespace is created with permission for you
   and your team to administrate it.

## Delete a project/namespace

To delete a project/namespace. Remove it from the `namespaces.yaml` file and then ask Calamari to remove it.
It is not removed automatically because errors can give a devastating result.
