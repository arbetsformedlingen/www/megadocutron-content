---
title: Configuring Docker for Windows to work like Podman
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//engineering/plattform/docker-for-windows.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /engineering/plattform/docker-for-windows.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/engineering/plattform/docker-for-windows.md
tags:
- engineering::plattform::docker-for-windows.md
- plattform::docker-for-windows.md
- docker-for-windows.md
---
# Configuring Docker for Windows to work like Podman


## Background
By default, Docker for Windows installs with a configuration which is
incompatible with Podman's configuration. More precisely, is it
configured to use an image building engine which has a different set
of optimisation rules for selecting multi stage build steps.

This causes the interpretation of a Dockerfile to work differently on
Docker for Windows and Podman (which is used for building JobTech's
images in the build pipeline), meaning that their different rule
selecting steps sometimes select different sets of steps to run.


## Problem
A Dockerfile which works differently on a local developer machine
compared to how it works on the build cluster is confusing.


## Solution

One way to avoid the problem is to use Podman for Windows instead of
Docker:

https://www.redhat.com/sysadmin/run-podman-windows


Another way to avoid the problem is to reconfigure Docker for Windows
to work more like Podman. Here is an instruction:

https://stackoverflow.com/questions/66839443/how-to-enable-disable-buildkit-in-docker



## Validation

Here is a Dockerfile which fails to build on Podman and a reconfigured
Docker for Windows, but succeeds on a default Docker for Windows:
```
FROM debian:11-slim AS base

RUN true


FROM base AS step0

RUN false


FROM base AS final

RUN true
```


## Note about Docker

Please verify that you have a valid license for your Docker
installation, since it is no longer free to use for big organisations.