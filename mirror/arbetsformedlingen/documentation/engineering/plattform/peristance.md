---
title: Persistance
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//engineering/plattform/peristance.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /engineering/plattform/peristance.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/engineering/plattform/peristance.md
tags:
- engineering::plattform::peristance.md
- plattform::peristance.md
- peristance.md
---
# Persistance

Out of the box OpenShift supports persistance for Pod:s with
one pod per volume attached. When this is suitable pod can
just claim persistance from StorageClass `gp2`.

In cases where multiple Pod:s need to share one volume, things
get a bit more complicated. OpenShifts in conjuction with AWS
is not as well developed. Fortunatly, Amazon provides a 
filesystem, EFS, that can be mounted as NFS volume. But
using it create som hassles and is not as smooth as using
StorageClass `gp2`.

This document describes how EFS can be used on OpenShift
to enable many pod:s share one volume.

## Some background
RedHat had a technical preview of an [operator using EFS](https://access.redhat.com/articles/5025181),
unfortunatly it was [depricated](https://access.redhat.com/solutions/4591701). 
There are no known plan for a new effort. The operator was 
rather inmature. It did not support dynamic provisioning and 
it also required of a non standard kubernetes resource.

Amazon are also developing a [CSI driver for EFS](https://github.com/kubernetes-sigs/aws-efs-csi-driver).
It is focused to run on EKS and not OpenShift. It might
be worth evaluate it on an temporary OpenShift cluster. 
It recently started to support dynamic provisioning, that means no
fiddeling in AWS console to create new volumes after initial 
setup. 

The easiest way to setup storage to share data between Pod:s
is at the moment to take advantage of EFS:s and OpenShifts 
NFS capability. This means create a EFS file system and mount 
it via NFS. It implies some manual work createing the volume 
in AWS console, before it can be accessed from OpenShift. Next
section describes all the steps.

## First time on a cluster
The security group must have an inbound rule for NFS traffic.
These three steps only needs to be done once per cluster.

1. Go to the security group for the cluster and click on
*Edit inbound rules*.
1. If there are not a rule for NFS, click *Add rule*. Select *Type*
`NFS` and *Source* `Custom` and attribute `10.0.0.0/16`.
1. Click *Save rules*.


## Setup new volume

You need to set up a new volume if many pods need to share
data on a file system. Limit the use of the volume to share data
within one system. 

### Setup EFS filesystem

Ask team Calamari to do this and setting up the PV for you.

First we need to create a file system volume to be used.
Go to [AWS console for EFS](https://eu-central-1.console.aws.amazon.com/efs/home).
Make sure to select the same region.

Click on *Create File System*. Give the file system a name, so you can separate it from other. Suggestion is to prefix with namespace it will be used from.

Select the VPC where the OpenShift cluster resides.
|Cluster |Region       |VPC            |Security group       |
|--------|-------------|---------------|---------------------|
|Test    |eu-central-1 |test-n6w9k-vpc |sg-0776af2b5b31324bb |
|Testing|eu-north-1|testing-wqd86-vpc|sg-01713cad831177c30|
|Prod    |eu-north-1   |prod-m7gqz-vpc |sg-00147340d016880fd |


Let it be `Regional` and click *Create*.

Once created, observe the `File System ID`. You will need it later.

Click on your new file system and then *Network*. You will see
one for each avaliablility zone. Wait until it says all are avaliable.
You might need to reload. Then click *Manage*.

Set *Security group* according to the table above and remove 
the pre-set security group. Do this for each avaliability 
zone. Click *Save*.

### Configure persistent volume

Ask Calamari to do this for you, it requires high priviliges on Kubernetes.

Next is to make the volume avaliable within OpenShift, this is done
by creating a `PersistentVolume` resource pointing to the new 
filesystem volume in EFS.

By creating a persistent volume, the EFS filesystem becomes 
exposed to OpenShift. A persistent volume is cluster global. 
To make it easy to understand the purpose of it, some 
conventions are good to follow. The are some other rules too, 
to follow.

* `name` - same as the name of the file system.
* `storageClassName` - same as the name of the file system, this avoids that someone else claiming your file system. 
* `server` - follow the pattern below and fill in your file system id and region.
* `persistentVolumeReclaimPolicy` - shall in most cases be `Retain` to avoid loosing data.

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: FILE-SYSTEM-NAME 
spec:
  storageClassName: FILE-SYSTEM-NAME
  capacity:
    storage: 5Gi 
  accessModes:
  - ReadWriteMany 
  nfs: 
    path: / 
    server: FILE-SYSTEM-ID.efs.REGION.amazonaws.com
  persistentVolumeReclaimPolicy: Retain
```

### Access from Pod

A `PersistentVolume` must be claimed by its user(i.e. pod), its done 
via a `PersistentVolumeClaim` that will be used by the pods.

There are one claim per volume, but there can be many pods using 
one claim if the access mode is `ReadWriteMany`.

The claim will look like this:
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: MY-CLAIM
spec:
  accessModes:
    - ReadWriteMany
  storageClassName: FILE-SYSTEM-NAME
  resources:
    requests:
      storage: 5Gi
```

To use the claim and mount the volume in a Pod is easily done. In this example the `volumeMounts` and `volumes` are the important:
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-example
spec:
  containers:
  - name: pod-example
    image: docker.io/library/busybox
    args:
    - sleep
    - "1000000"    
    imagePullPolicy: Always 
    volumeMounts:
    - mountPath: /mnt
      name: vol
    resources:
      limits:
        cpu: "100m"
        memory: "64Mi"
      requests:
        cpu: "100m"
        memory: "64Mi"
  volumes:
  - name: vol
    persistentVolumeClaim:
      claimName: MY-CLAIM
```

**Note:** At creation the EFS volume is owned by root and only
writable by the owner. Since pods normally runs with other random
users, one must make the volume world writable.

### Make volume world writable

An easy way to make the volume world writable is to run the following pod.
Once it have ran once, remember to delete it.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: fix-nfs
spec:
  restartPolicy: Never
  containers:
  - name: fix-nfs
    image: docker.io/library/busybox
    command: ["/bin/chmod"]
    args:
    - "777"
    - /mnt
    imagePullPolicy: Always
    volumeMounts:
    - mountPath: /mnt
      name: vol
    resources:
      limits:
        cpu: "100m"
        memory: "64Mi"
      requests:
        cpu: "100m"
        memory: "64Mi"
  volumes:
  - name: vol
    persistentVolumeClaim:
      claimName: MY-CLAIM
```

Here is an example how to apply and delete the pod.

```
kubectl apply -f MY-YAML-TO-FIX-NFS-PERMISSIONS.yaml
kubectl get pod fix-nfs
# Wait to above command to show status is *completed*
kubectl delete pod fix-nfs
```


## Long term 

In an ideal state, we were able to do dynamic provisioning of 
new volumes. This mean there are no manual steps to create
EFS file system and Persistent volume. Instead there is a 
`StorageClass` triggering an AWS EFS provisioner creating the
volume on request.
