---
title: Docker registry
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//engineering/plattform/docker-registry.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /engineering/plattform/docker-registry.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/engineering/plattform/docker-registry.md
tags:
- engineering::plattform::docker-registry.md
- plattform::docker-registry.md
- docker-registry.md
---
# Docker registry

Jobtech has a common container(docker) registry accessible from all clusters
where container images can be stored. We are using a product called Nexus for
this. To share container images between users and/or clusters
use the hostname `docker-images.jobtechdev.se`.

If you want to browse images, go to [nexus.jobtechdev.se](https://nexus.jobtechdev.se).
Click *Browse* on the sidebar to the left. Then click *JobtechdevDockerRegistry* item in the list.
You now see the content of images in `docker-images.jobtechdev.se`.

Images we are building our self shall be named`docker-images.jobtechdev.se/my-project/image-name`.
Where you replace `my-project` with your project name, ideally same as git repository.
Also replace `image-name` with the name of your image, most often it can be the same as project.

To avoid docker-hubs rate-limiting we are mirroring some images. These are
available in `docker-images.jobtechdev.se/mirror/`.
What is mirrored and how to add more to be mirrored can be found in
[container-sync repo](https://gitlab.com/arbetsformedlingen/devops/container-sync/-/tree/main).

When you using `docker-images.jobtechdev.se` use your jobtech-ldap username and password
to access to be able to push. These are the same you use for login to Openshift. If you set up a build pipeline,
ask Calamari for a user. To download images to our clusters you do not need to provide a user,
it is already taken care of.

All images on `docker-images.jobtechdev.se` can be read anonymously.
If we want third parties to be able to upload, they need a read-write account.

## Usage and setup

### On your laptop

On your laptop login to the server using your Jobtech-ldap username and password.
Login with docker:
`docker login docker-images.jobtechdev.se` and if you use
podman: `podman login docker-images.jobtechdev.se`.

After login can you build and publish your container images. Example:

```shell
podman build -t docker-images.jobtechdev.se/my-project/image-name:tag .
podman push docker-images.jobtechdev.se/my-project/image-name:tag
```

With docker, just replace `podman` with `docker`.

### On any OpenShift cluster

Refer images the same way you did on your laptop. All OpenShift clusters has a global pull
secret setup to download from `docker-images.jobtechdev.se`. This mean that all our OpenShift
clusters can download all images that are uploaded there.

#### Building and pushing images

If you are building container images with
[Aardvark](https://gitlab.com/arbetsformedlingen/devops/aardvark) it works out of the box.

If you build images with any other method you need a login to `docker-images.jobtechdev.se`
to be able to upload images. Ask team Calamari for an account. One account per project or
team is probably a good idea.
Earlier we have used a common account for all(username `redhat`), this shall not
be used anymore.

## Legacy references

You may have cases where you use hostname `nexus.jobtechdev.se:5004`, to move to the new scheme just
replace with `docker-images.jobtechdev.se`.

If you use account `redhat`, ask Calamari for a new account.

## Image caching proxy

Docker hub have introduced rate limiting on the number of pulls. To avoid hitting this, Nexus can
be used as an proxy. Unfortunately, this does not yet work on Openshift(expected in 4.10).
But you can use it on your client machine.
Docker is calling the caching proxy mirroring.

We have a domain to be used for caching proxy called `docker-mirror.jobtechdev.se`. Purpose is that
the client calls the mirror instead of `docker.io`, if the proxy has the requested image in
its cache it downloads from there. Otherwise, it downloads from `docker.io` and caches it
for upcoming requests.

### Podman and Skopeo

To take advantage of the repository mirroring of docker.io add the following section to
`~/.config/containers/registries.conf`:

```text
[[registry]]
#prefix = "docker.io/"
insecure = false
blocked = false
location = "docker.io/"
[[registry.mirror]]
location = "docker-mirror.jobtechdev.se"
```

Then do `podman login docker-mirror.jobtechdev.se` and use your ldap
login. Then pull images as usual from `docker.io`.

### Docker

To be done! Contribution welcome
