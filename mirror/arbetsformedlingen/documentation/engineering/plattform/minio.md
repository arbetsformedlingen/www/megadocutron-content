---
title: Minio
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//engineering/plattform/minio.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /engineering/plattform/minio.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/engineering/plattform/minio.md
tags:
- engineering::plattform::minio.md
- plattform::minio.md
- minio.md
---
# Minio

## Administration
The administration URL is https://minio-console.arbetsformedlingen.se/login

Enter valid Minio credentials to login.


## Set a user's policy
```
mc admin policy set local getonlypipeline-dev user=pipeline-dev
```
