---
title: Opensearch (former Elasticsearch)
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//engineering/plattform/opensearch.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /engineering/plattform/opensearch.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/engineering/plattform/opensearch.md
tags:
- engineering::plattform::opensearch.md
- plattform::opensearch.md
- opensearch.md
---
# Opensearch (former Elasticsearch)

## Connect to an Opensearch instance without a public IP

See this instruction:
https://gitlab.com/arbetsformedlingen/devops/https-proxy-in-openshift
