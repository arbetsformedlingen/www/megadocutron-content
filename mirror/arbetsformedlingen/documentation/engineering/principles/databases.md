---
title: Databases
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//engineering/principles/databases.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /engineering/principles/databases.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/engineering/principles/databases.md
tags:
- engineering::principles::databases.md
- principles::databases.md
- databases.md
---
# Databases

Our to main go to for database storage are Postgres and OpenSearch.
It is up to the team to select other solutions, but the DevOps-team
will have limited ability to support.
