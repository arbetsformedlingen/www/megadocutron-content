---
title: Kubernetes principle
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//engineering/principles/kubernetes.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /engineering/principles/kubernetes.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/engineering/principles/kubernetes.md
tags:
- engineering::principles::kubernetes.md
- principles::kubernetes.md
- kubernetes.md
---
# Kubernetes principle

## Table of contents

[[_TOC_]]

## Namespaces

* Suffix your namespace with the type of environemnt. It makes it easy to separate them. Suggested suffices are `-develop`, `-test`, `-staging`, `-i1`, `-t2`, `-prod` and `-prod-stdby`
* Do not deploy multiple different instances sucha as develop and staging in the same namespace. Namespaces are sheep.

## Labels

Labels are good to navigate around resources and understand their role, and place in our environment. One can provide different good metadata. Labels on pods are also following with the logs into ELK, which means you can filter logs based on them.

Try to use at least these labels, you can also use others depending on need.

* `team` - Set the value to the name of your team, for instance `walrus`.
* `environment` - Since we in some cluster has multiple environments this is valuable to set. Possible values are: `develop`, `test`, `staging`, `i1`, `t2`, `prod` and `prod-stdby`

## Pods

To get high availability in deployments we are running multiple pods. It is normally better to have many small pods than fewer large.

To ensure your pods ends up on different nodes, if possible, do:

```yaml
affinity:
# Avoid to run many pods on same physical node. It increases reliability.
  podAntiAffinity:
    preferredDuringSchedulingIgnoredDuringExecution:
    - weight: 1
      podAffinityTerm:
      labelSelector:
        matchLabels:
          app: your-app # This must unique to your pods, within the namespace.
      topologyKey: kubernetes.io/hostname
```
