---
title: Engineering principles
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//engineering/principles/README.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /engineering/principles/README.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/engineering/principles/README.md
tags:
- engineering::principles::README.md
- principles::README.md
- README.md
---
# Engineering principles

This section provide guidance to common practices withing JobTech.
Teams is not obliged to follow them. Though, it is encouraged to
take the principles in consideration to enable collaboration and
lower burden on maintaining infrastructure.

Intension is to increase this section over time. Talk to Calamari
team if you have ideas.
