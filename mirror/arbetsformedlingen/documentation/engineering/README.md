---
title: Engineering
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//engineering/README.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /engineering/README.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/engineering/README.md
tags:
- engineering::README.md
- README.md
---
# Engineering

This section is primarily aimed for engineers.

## Sub-sections

* [Plattform](plattform/)
* [Principles](principles/)
