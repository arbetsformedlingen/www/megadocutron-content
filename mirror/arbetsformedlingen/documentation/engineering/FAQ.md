---
title: FAQ
gitlaburl: https://gitlab.com/arbetsformedlingen/documentation/-/blob/main//engineering/FAQ.md
gitdir: /arbetsformedlingen/documentation
gitdir-file-path: /engineering/FAQ.md
date: '2023-10-12 15:14:47'
path: /arbetsformedlingen/documentation/engineering/FAQ.md
tags:
- engineering::FAQ.md
- FAQ.md
---
# FAQ
Brief FAQ:s with links to clarification.

Just stubs for now.

- use temp mount in yaml instead of chgroup in dockerfile                                                                                                                                                                                                                                                                  
- you can have 1 codebase, several deploys with aardvark                                                                                                                                                                                                                                                                   
- how to construct internal routes names?                                                                                                                                                                                                                                                                                  
- dont use low port numbers, because unix                                                                                                                                                                                                                                                                                  
- check that exposed container ports match the srvice ports 

- to browse URLs ending with .jtech.se, you need to add JobTech's root CA to your browser's list of trusted certificates. See this thread for instructions:
https://gitlab.com/arbetsformedlingen/devops/calamari-documentation/-/issues/95
