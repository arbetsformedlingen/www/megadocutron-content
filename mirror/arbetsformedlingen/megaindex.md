 filtered
# mirror
## arbetsformedlingen
### arbetsformedlingen-security-policy-project
[README]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen-security-policy-project/README.md" >}} )

### arbetsformedlingen
#### designsystem
##### digi.wiki
[Arkitektur]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/designsystem/digi.wiki/Arkitektur.md" >}} )

[Home]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/designsystem/digi.wiki/Home.md" >}} )

#### dy
##### digital-yrkesvaegledning.wiki
[Användningsfall]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/Användningsfall.md" >}} )

[Avgränsning]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/Avgränsning.md" >}} )

[Datakällor]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/Datakällor.md" >}} )

[Home]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/Home.md" >}} )

[Insikter]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/Insikter.md" >}} )

[Ordlista,-Standarder]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/Ordlista,-Standarder.md" >}} )

[PoC-Data-Map]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/dy/digital-yrkesvaegledning.wiki/PoC-Data-Map.md" >}} )

#### etik
##### etisk-ai.wiki
[home]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/etik/etisk-ai.wiki/home.md" >}} )

##### hallbarhet.wiki
[Ansvarsfull-och-hållbar-teknikutveckling]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/etik/hallbarhet.wiki/Ansvarsfull-och-hållbar-teknikutveckling.md" >}} )

[home]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/etik/hallbarhet.wiki/home.md" >}} )

#### joblinks
##### processing.wiki
[Community-Log]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/joblinks/processing.wiki/Community-Log.md" >}} )

##### wiki.wiki
[Ekosystem-för-annonser-i-media]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Ekosystem-för-annonser-i-media.md" >}} )

[HOME-ENG]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/HOME-ENG.md" >}} )

[Identifiera-annonsdubbletter]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Identifiera-annonsdubbletter.md" >}} )

[Meta-data-enrichment-process-for-ads]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Meta-data-enrichment-process-for-ads.md" >}} )

[Pilot-API-definition]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Pilot-API-definition.md" >}} )

[Policy-öppenhet]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Policy-öppenhet.md" >}} )

[Project-description]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Project-description.md" >}} )

[Projektsida-Ekosystem-för-annonser]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Projektsida-Ekosystem-för-annonser.md" >}} )

[Referensgrupp-Ekosystem-för-annonser---Arkiv]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/Referensgrupp-Ekosystem-för-annonser---Arkiv.md" >}} )

[_sidebar]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/_sidebar.md" >}} )

[home]({{< ref "/filtered/mirror/arbetsformedlingen/arbetsformedlingen/joblinks/wiki.wiki/home.md" >}} )

### collaboration
#### decisionlog
[2023-03-28-start-logging]({{< ref "/filtered/mirror/arbetsformedlingen/collaboration/decisionlog/2023-03-28-start-logging.md" >}} )

[2023-03-29-Automatic-Vulnerability-Detection-for-GitLab-Repositories]({{< ref "/filtered/mirror/arbetsformedlingen/collaboration/decisionlog/2023-03-29-Automatic-Vulnerability-Detection-for-GitLab-Repositories.md" >}} )

[2023-03-29-Publish-Daily-Operations-Metrics]({{< ref "/filtered/mirror/arbetsformedlingen/collaboration/decisionlog/2023-03-29-Publish-Daily-Operations-Metrics.md" >}} )

[2023-03-30-Allowing-Anonymous-Access-to-Open-Data]({{< ref "/filtered/mirror/arbetsformedlingen/collaboration/decisionlog/2023-03-30-Allowing-Anonymous-Access-to-Open-Data.md" >}} )

[2023-03-30-Preventing-Sensitive-Information-from-Being-Committed-to-Source-Control]({{< ref "/filtered/mirror/arbetsformedlingen/collaboration/decisionlog/2023-03-30-Preventing-Sensitive-Information-from-Being-Committed-to-Source-Control.md" >}} )

[2023-03-31-Ensuring-Reproducibility-of-Published-Software]({{< ref "/filtered/mirror/arbetsformedlingen/collaboration/decisionlog/2023-03-31-Ensuring-Reproducibility-of-Published-Software.md" >}} )

[2023-08-09-Static-Webapps]({{< ref "/filtered/mirror/arbetsformedlingen/collaboration/decisionlog/2023-08-09-Static-Webapps.md" >}} )

[2023-10-12-Password-Management-Tool]({{< ref "/filtered/mirror/arbetsformedlingen/collaboration/decisionlog/2023-10-12-Password-Management-Tool.md" >}} )

[2023-10-12-Secret-management-for-deploys]({{< ref "/filtered/mirror/arbetsformedlingen/collaboration/decisionlog/2023-10-12-Secret-management-for-deploys.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/collaboration/decisionlog/README.md" >}} )

#### minnesanteckningar
[2022-01-21-Moln i offentligsektor-Opens Source Sweden]({{< ref "/filtered/mirror/arbetsformedlingen/collaboration/minnesanteckningar/2022-01-21-Moln i offentligsektor-Opens Source Sweden.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/collaboration/minnesanteckningar/README.md" >}} )

### designsystem
#### digi-skv-temp-deploy
##### skolverket-angular
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi-skv-temp-deploy/skolverket-angular/README.md" >}} )

##### skolverket
###### dist
####### collection
######## fonts
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi-skv-temp-deploy/skolverket/dist/collection/fonts/README.md" >}} )

####### digi-skolverket
######## fonts
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi-skv-temp-deploy/skolverket/dist/digi-skolverket/fonts/README.md" >}} )

###### fonts
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi-skv-temp-deploy/skolverket/fonts/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi-skv-temp-deploy/README.md" >}} )

#### digi
##### apps
###### arbetsformedlingen
####### docs
[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/apps/arbetsformedlingen/docs/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/apps/arbetsformedlingen/README.md" >}} )

###### skolverket
####### docs
[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/apps/skolverket/docs/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/apps/skolverket/README.md" >}} )

##### libs
###### arbetsformedlingen
####### angular
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/angular/README.md" >}} )

####### fonts
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/fonts/README.md" >}} )

####### package
######## src
######### components
########## _badge
########### badge-notification
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_badge/badge-notification/readme.md" >}} )

########### badge-status
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_badge/badge-status/readme.md" >}} )

########## _calendar
########### calendar-datepicker
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_calendar/calendar-datepicker/readme.md" >}} )

########## _card
########### card
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_card/card/readme.md" >}} )

########## _dialog
########### dialog
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_dialog/dialog/readme.md" >}} )

########## _expandable
########### expandable-faq-item
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_expandable/expandable-faq-item/readme.md" >}} )

########### expandable-faq
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_expandable/expandable-faq/readme.md" >}} )

########## _form
########### form-category-filter
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_form/form-category-filter/readme.md" >}} )

########### form-error-list
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_form/form-error-list/readme.md" >}} )

########### form-file-upload
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_form/form-file-upload/readme.md" >}} )

########### form-receipt
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_form/form-receipt/readme.md" >}} )

########### form-select-filter
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_form/form-select-filter/readme.md" >}} )

########## _header
########### header-avatar
############ avatars
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_header/header-avatar/avatars/readme.md" >}} )

[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_header/header-avatar/readme.md" >}} )

########### header-navigation-item
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_header/header-navigation-item/readme.md" >}} )

########### header-navigation
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_header/header-navigation/readme.md" >}} )

########### header-notification
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_header/header-notification/readme.md" >}} )

########### header
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_header/header/readme.md" >}} )

########## _icon
########### icon-accessibility-deaf
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-accessibility-deaf/readme.md" >}} )

########### icon-accessibility-universal
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-accessibility-universal/readme.md" >}} )

########### icon-archive-outline
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-archive-outline/readme.md" >}} )

########### icon-archive
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-archive/readme.md" >}} )

########### icon-arrow-back
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-arrow-back/readme.md" >}} )

########### icon-arrow-down
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-arrow-down/readme.md" >}} )

########### icon-arrow-left
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-arrow-left/readme.md" >}} )

########### icon-arrow-right
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-arrow-right/readme.md" >}} )

########### icon-arrow-sign-in
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-arrow-sign-in/readme.md" >}} )

########### icon-arrow-sign-out
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-arrow-sign-out/readme.md" >}} )

########### icon-arrow-up
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-arrow-up/readme.md" >}} )

########### icon-at
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-at/readme.md" >}} )

########### icon-bell-filled
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-bell-filled/readme.md" >}} )

########### icon-bell
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-bell/readme.md" >}} )

########### icon-book
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-book/readme.md" >}} )

########### icon-bubble-ellipsis
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-bubble-ellipsis/readme.md" >}} )

########### icon-bubble
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-bubble/readme.md" >}} )

########### icon-building-outline
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-building-outline/readme.md" >}} )

########### icon-building-solid
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-building-solid/readme.md" >}} )

########### icon-calculator-solid
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-calculator-solid/readme.md" >}} )

########### icon-calendar-alt-alert
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-calendar-alt-alert/readme.md" >}} )

########### icon-calendar-alt
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-calendar-alt/readme.md" >}} )

########### icon-calendar
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-calendar/readme.md" >}} )

########### icon-caret-circle-right
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-caret-circle-right/readme.md" >}} )

########### icon-caret-down
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-caret-down/readme.md" >}} )

########### icon-caret-left
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-caret-left/readme.md" >}} )

########### icon-caret-right
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-caret-right/readme.md" >}} )

########### icon-caret-up
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-caret-up/readme.md" >}} )

########### icon-chart
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-chart/readme.md" >}} )

########### icon-chat
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-chat/readme.md" >}} )

########### icon-clock
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-clock/readme.md" >}} )

########### icon-compress-alt
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-compress-alt/readme.md" >}} )

########### icon-comunication-case
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-comunication-case/readme.md" >}} )

########### icon-comunication-flag
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-comunication-flag/readme.md" >}} )

########### icon-comunication-graduate
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-comunication-graduate/readme.md" >}} )

########### icon-comunication-play
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-comunication-play/readme.md" >}} )

########### icon-cooperation
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-cooperation/readme.md" >}} )

########### icon-dxa
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-dxa/readme.md" >}} )

########### icon-edit
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-edit/readme.md" >}} )

########### icon-ellipsis
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-ellipsis/readme.md" >}} )

########### icon-envelope-filled
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-envelope-filled/readme.md" >}} )

########### icon-envelope
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-envelope/readme.md" >}} )

########### icon-exclamation-triangle-filled
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-exclamation-triangle-filled/readme.md" >}} )

########### icon-expand-alt
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-expand-alt/readme.md" >}} )

########### icon-eye-slash
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-eye-slash/readme.md" >}} )

########### icon-eye
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-eye/readme.md" >}} )

########### icon-facebook-square
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-facebook-square/readme.md" >}} )

########### icon-file-document
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-file-document/readme.md" >}} )

########### icon-file-excel
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-file-excel/readme.md" >}} )

########### icon-file-pdf
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-file-pdf/readme.md" >}} )

########### icon-file-powerpoint
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-file-powerpoint/readme.md" >}} )

########### icon-file-word
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-file-word/readme.md" >}} )

########### icon-film
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-film/readme.md" >}} )

########### icon-filter
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-filter/readme.md" >}} )

########### icon-globe-filled
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-globe-filled/readme.md" >}} )

########### icon-globe
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-globe/readme.md" >}} )

########### icon-headphones
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-headphones/readme.md" >}} )

########### icon-heart-solid
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-heart-solid/readme.md" >}} )

########### icon-heart
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-heart/readme.md" >}} )

########### icon-history
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-history/readme.md" >}} )

########### icon-home
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-home/readme.md" >}} )

########### icon-image
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-image/readme.md" >}} )

########### icon-info-circle-solid
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-info-circle-solid/readme.md" >}} )

########### icon-instagram
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-instagram/readme.md" >}} )

########### icon-job-suggestion
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-job-suggestion/readme.md" >}} )

########### icon-language-outline
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-language-outline/readme.md" >}} )

########### icon-language
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-language/readme.md" >}} )

########### icon-lattlast
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-lattlast/readme.md" >}} )

########### icon-licence-bus
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-licence-bus/readme.md" >}} )

########### icon-licence-car
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-licence-car/readme.md" >}} )

########### icon-licence-motorcycle
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-licence-motorcycle/readme.md" >}} )

########### icon-licence-truck
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-licence-truck/readme.md" >}} )

########### icon-lightbulb
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-lightbulb/readme.md" >}} )

########### icon-linkedin-in
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-linkedin-in/readme.md" >}} )

########### icon-list-ul
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-list-ul/readme.md" >}} )

########### icon-lock
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-lock/readme.md" >}} )

########### icon-marker-filled
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-marker-filled/readme.md" >}} )

########### icon-marker
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-marker/readme.md" >}} )

########### icon-media-course
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-media-course/readme.md" >}} )

########### icon-media-podcast
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-media-podcast/readme.md" >}} )

########### icon-microphone-off
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-microphone-off/readme.md" >}} )

########### icon-microphone
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-microphone/readme.md" >}} )

########### icon-news
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-news/readme.md" >}} )

########### icon-notification-error
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-notification-error/readme.md" >}} )

########### icon-notification-info
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-notification-info/readme.md" >}} )

########### icon-open-source
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-open-source/readme.md" >}} )

########### icon-pen
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-pen/readme.md" >}} )

########### icon-phone-hangup
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-phone-hangup/readme.md" >}} )

########### icon-phone
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-phone/readme.md" >}} )

########### icon-print
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-print/readme.md" >}} )

########### icon-redo
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-redo/readme.md" >}} )

########### icon-screensharing-off
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-screensharing-off/readme.md" >}} )

########### icon-screensharing
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-screensharing/readme.md" >}} )

########### icon-settings
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-settings/readme.md" >}} )

########### icon-share-alt
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-share-alt/readme.md" >}} )

########### icon-sign
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-sign/readme.md" >}} )

########### icon-sliders-h
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-sliders-h/readme.md" >}} )

########### icon-sokkandidat
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-sokkandidat/readme.md" >}} )

########### icon-sort-down
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-sort-down/readme.md" >}} )

########### icon-sort-up
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-sort-up/readme.md" >}} )

########### icon-sort
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-sort/readme.md" >}} )

########### icon-star-reg
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-star-reg/readme.md" >}} )

########### icon-star
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-star/readme.md" >}} )

########### icon-suitcase-outline
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-suitcase-outline/readme.md" >}} )

########### icon-suitcase-solid
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-suitcase-solid/readme.md" >}} )

########### icon-table
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-table/readme.md" >}} )

########### icon-toggle-off
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-toggle-off/readme.md" >}} )

########### icon-toggle-on
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-toggle-on/readme.md" >}} )

########### icon-twitter
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-twitter/readme.md" >}} )

########### icon-tyck-till
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-tyck-till/readme.md" >}} )

########### icon-unlock
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-unlock/readme.md" >}} )

########### icon-update
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-update/readme.md" >}} )

########### icon-upload
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-upload/readme.md" >}} )

########### icon-user-alt
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-user-alt/readme.md" >}} )

########### icon-users-solid
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-users-solid/readme.md" >}} )

########### icon-validation-error
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-validation-error/readme.md" >}} )

########### icon-validation-warning
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-validation-warning/readme.md" >}} )

########### icon-videocamera-off
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-videocamera-off/readme.md" >}} )

########### icon-videocamera
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-videocamera/readme.md" >}} )

########### icon-volume
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-volume/readme.md" >}} )

########### icon-web-tv
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-web-tv/readme.md" >}} )

########### icon-webinar
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-webinar/readme.md" >}} )

########### icon-x-button-outline
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-x-button-outline/readme.md" >}} )

########### icon-x-button
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-x-button/readme.md" >}} )

########### icon-youtube
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_icon/icon-youtube/readme.md" >}} )

########## _info-card
########### info-card-multi-container
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_info-card/info-card-multi-container/readme.md" >}} )

########### info-card-multi
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_info-card/info-card-multi/readme.md" >}} )

########### info-card
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_info-card/info-card/readme.md" >}} )

########## _link
########### link-button
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_link/link-button/readme.md" >}} )

########## _list
########### list
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_list/list/readme.md" >}} )

########## _logo
########### logo
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_logo/logo/readme.md" >}} )

########## _navigation
########### navigation-breadcrumbs
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_navigation/navigation-breadcrumbs/readme.md" >}} )

########### navigation-pagination
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_navigation/navigation-pagination/readme.md" >}} )

########### navigation-tab
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_navigation/navigation-tab/readme.md" >}} )

########### navigation-tabs
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_navigation/navigation-tabs/readme.md" >}} )

########## _notification
########### notification-alert
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_notification/notification-alert/readme.md" >}} )

########### notification-error-page
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_notification/notification-error-page/readme.md" >}} )

########## _quote
########### quote-multi-container
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_quote/quote-multi-container/readme.md" >}} )

########### quote-single
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_quote/quote-single/readme.md" >}} )

########## _table
########### table
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_table/table/readme.md" >}} )

########## _tag
########### tag-media
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_tag/tag-media/readme.md" >}} )

########## _tools
########### tools-feedback-banner
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_tools/tools-feedback-banner/readme.md" >}} )

########### tools-feedback
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_tools/tools-feedback/readme.md" >}} )

########### tools-languagepicker
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_tools/tools-languagepicker/readme.md" >}} )

########## _typography
########### typography-heading-jumbo
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/src/components/_typography/typography-heading-jumbo/readme.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/package/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/README.md" >}} )

####### react
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/arbetsformedlingen/react/README.md" >}} )

###### core
####### fonts
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/fonts/README.md" >}} )

####### package
######## src
######### components
########## _button
########### button
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_button/button/readme.md" >}} )

########## _calendar
########### calendar
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_calendar/calendar/readme.md" >}} )

########### week-view
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_calendar/week-view/readme.md" >}} )

########## _chart
########### bar-chart
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_chart/bar-chart/readme.md" >}} )

########### chart-line
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_chart/chart-line/readme.md" >}} )

########## _code
########### code-block
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_code/code-block/readme.md" >}} )

########### code-example
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_code/code-example/readme.md" >}} )

########### code
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_code/code/readme.md" >}} )

########## _expandable
########### expandable-accordion
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_expandable/expandable-accordion/readme.md" >}} )

########## _form
########### form-checkbox
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_form/form-checkbox/readme.md" >}} )

########### form-fieldset
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_form/form-fieldset/readme.md" >}} )

########### form-filter
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_form/form-filter/readme.md" >}} )

########### form-input-search
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_form/form-input-search/readme.md" >}} )

########### form-input
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_form/form-input/readme.md" >}} )

########### form-label
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_form/form-label/readme.md" >}} )

########### form-radiobutton
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_form/form-radiobutton/readme.md" >}} )

########### form-radiogroup
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_form/form-radiogroup/readme.md" >}} )

########### form-select
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_form/form-select/readme.md" >}} )

########### form-textarea
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_form/form-textarea/readme.md" >}} )

########### form-validation-message
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_form/form-validation-message/readme.md" >}} )

########## _icon
########### icon-bars
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-bars/readme.md" >}} )

########### icon-check-circle-reg-alt
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-check-circle-reg-alt/readme.md" >}} )

########### icon-check
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-check/readme.md" >}} )

########### icon-chevron-down
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-chevron-down/readme.md" >}} )

########### icon-chevron-left
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-chevron-left/readme.md" >}} )

########### icon-chevron-right
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-chevron-right/readme.md" >}} )

########### icon-chevron-up
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-chevron-up/readme.md" >}} )

########### icon-copy
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-copy/readme.md" >}} )

########### icon-danger-outline
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-danger-outline/readme.md" >}} )

########### icon-download
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-download/readme.md" >}} )

########### icon-exclamation-circle-filled
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-exclamation-circle-filled/readme.md" >}} )

########### icon-exclamation-triangle-warning
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-exclamation-triangle-warning/readme.md" >}} )

########### icon-exclamation-triangle
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-exclamation-triangle/readme.md" >}} )

########### icon-external-link-alt
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-external-link-alt/readme.md" >}} )

########### icon-minus
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-minus/readme.md" >}} )

########### icon-notification-succes
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-notification-succes/readme.md" >}} )

########### icon-notification-warning
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-notification-warning/readme.md" >}} )

########### icon-paperclip
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-paperclip/readme.md" >}} )

########### icon-plus
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-plus/readme.md" >}} )

########### icon-search
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-search/readme.md" >}} )

########### icon-spinner
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-spinner/readme.md" >}} )

########### icon-trash
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-trash/readme.md" >}} )

########### icon-validation-success
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-validation-success/readme.md" >}} )

########### icon-x
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon-x/readme.md" >}} )

########### icon
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_icon/icon/readme.md" >}} )

########## _layout
########### layout-block
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_layout/layout-block/readme.md" >}} )

########### layout-columns
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_layout/layout-columns/readme.md" >}} )

########### layout-container
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_layout/layout-container/readme.md" >}} )

########### layout-media-object
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_layout/layout-media-object/readme.md" >}} )

########## _link
########### link-external
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_link/link-external/readme.md" >}} )

########### link-internal
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_link/link-internal/readme.md" >}} )

########### link
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_link/link/readme.md" >}} )

########## _loader
########### loader-skeleton
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_loader/loader-skeleton/readme.md" >}} )

########### loader-spinner
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_loader/loader-spinner/readme.md" >}} )

########## _media
########### media-figure
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_media/media-figure/readme.md" >}} )

########### media-image
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_media/media-image/readme.md" >}} )

########## _navigation
########### navigation-context-menu-item
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_navigation/navigation-context-menu-item/readme.md" >}} )

########### navigation-context-menu
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_navigation/navigation-context-menu/readme.md" >}} )

########### navigation-sidebar-button
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_navigation/navigation-sidebar-button/readme.md" >}} )

########### navigation-sidebar
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_navigation/navigation-sidebar/readme.md" >}} )

########### navigation-vertical-menu-item
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_navigation/navigation-vertical-menu-item/readme.md" >}} )

########### navigation-vertical-menu
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_navigation/navigation-vertical-menu/readme.md" >}} )

########## _progress
########### progress-step
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_progress/progress-step/readme.md" >}} )

########### progress-steps
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_progress/progress-steps/readme.md" >}} )

########### progressbar
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_progress/progressbar/readme.md" >}} )

########## _tag
########### tag
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_tag/tag/readme.md" >}} )

########## _typography
########### typography-meta
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_typography/typography-meta/readme.md" >}} )

########### typography-preamble
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_typography/typography-preamble/readme.md" >}} )

########### typography-time
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_typography/typography-time/readme.md" >}} )

########### typography
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_typography/typography/readme.md" >}} )

########## _util
########### util-breakpoint-observer
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_util/util-breakpoint-observer/readme.md" >}} )

########### util-detect-click-outside
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_util/util-detect-click-outside/readme.md" >}} )

########### util-detect-focus-outside
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_util/util-detect-focus-outside/readme.md" >}} )

########### util-intersection-observer
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_util/util-intersection-observer/readme.md" >}} )

########### util-keydown-handler
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_util/util-keydown-handler/readme.md" >}} )

########### util-keyup-handler
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_util/util-keyup-handler/readme.md" >}} )

########### util-mutation-observer
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_util/util-mutation-observer/readme.md" >}} )

########### util-resize-observer
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/src/components/_util/util-resize-observer/readme.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/core/package/README.md" >}} )

###### shared
####### styles
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/shared/styles/README.md" >}} )

####### taxonomies
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/shared/taxonomies/README.md" >}} )

####### text
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/shared/text/README.md" >}} )

####### utils
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/shared/utils/README.md" >}} )

###### skolverket
####### angular
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/angular/README.md" >}} )

####### fonts
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/fonts/README.md" >}} )

####### package
######## src
######### components
########## _card
########### card-box
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_card/card-box/readme.md" >}} )

########### card-link
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_card/card-link/readme.md" >}} )

########## _dialog
########### dialog
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_dialog/dialog/readme.md" >}} )

########## _form
########### form-file-upload
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_form/form-file-upload/readme.md" >}} )

########### form-process-step
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_form/form-process-step/readme.md" >}} )

########### form-process-steps
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_form/form-process-steps/readme.md" >}} )

########## _layout
########### layout-grid
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_layout/layout-grid/readme.md" >}} )

########### layout-rows
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_layout/layout-rows/readme.md" >}} )

########### layout-stacked-blocks
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_layout/layout-stacked-blocks/readme.md" >}} )

########## _link
########### link-icon
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_link/link-icon/readme.md" >}} )

########## _list
########### list-link
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_list/list-link/readme.md" >}} )

########## _logo
########### logo-service
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_logo/logo-service/readme.md" >}} )

########### logo-sister
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_logo/logo-sister/readme.md" >}} )

########### logo
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_logo/logo/readme.md" >}} )

########## _navigation
########### navigation-breadcrumbs
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_navigation/navigation-breadcrumbs/readme.md" >}} )

########### navigation-main-menu-panel
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_navigation/navigation-main-menu-panel/readme.md" >}} )

########### navigation-main-menu
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_navigation/navigation-main-menu/readme.md" >}} )

########### navigation-pagination
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_navigation/navigation-pagination/readme.md" >}} )

########### navigation-tab-in-a-box
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_navigation/navigation-tab-in-a-box/readme.md" >}} )

########### navigation-tab
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_navigation/navigation-tab/readme.md" >}} )

########### navigation-tabs
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_navigation/navigation-tabs/readme.md" >}} )

########### navigation-toc
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_navigation/navigation-toc/readme.md" >}} )

########## _notification
########### notification-alert
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_notification/notification-alert/readme.md" >}} )

########### notification-cookie
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_notification/notification-cookie/readme.md" >}} )

########### notification-detail
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_notification/notification-detail/readme.md" >}} )

########## _page
########### page-block-cards
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_page/page-block-cards/readme.md" >}} )

########### page-block-hero
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_page/page-block-hero/readme.md" >}} )

########### page-block-lists
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_page/page-block-lists/readme.md" >}} )

########### page-block-sidebar
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_page/page-block-sidebar/readme.md" >}} )

########### page-footer
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_page/page-footer/readme.md" >}} )

########### page-header
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_page/page-header/readme.md" >}} )

########### page
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_page/page/readme.md" >}} )

########## _table
########### table
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_table/table/readme.md" >}} )

########## _typography
########### typography-heading-section
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/src/components/_typography/typography-heading-section/readme.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/package/README.md" >}} )

####### react
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/react/README.md" >}} )

####### text
[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/text/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/libs/skolverket/README.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/README.md" >}} )

[storybook-migration-summary]({{< ref "/filtered/mirror/arbetsformedlingen/designsystem/digi/storybook-migration-summary.md" >}} )

### devops
#### api-spec-to-dcat
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/api-spec-to-dcat/README.md" >}} )

#### availability-data
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/availability-data/README.md" >}} )

#### bitwarden-file-tool
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/bitwarden-file-tool/README.md" >}} )

#### crossplane
##### jobtech-configuration
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/crossplane/jobtech-configuration/README.md" >}} )

#### gitlab-cicd
##### example-for-scripts
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/gitlab-cicd/example-for-scripts/README.md" >}} )

##### gitlab-tools
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/gitlab-cicd/gitlab-tools/README.md" >}} )

##### jobtech-ci-examples
###### integration-test-opensearch
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci-examples/integration-test-opensearch/README.md" >}} )

###### without-dockerfile-deploy-static-web
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci-examples/without-dockerfile-deploy-static-web/README.md" >}} )

###### without-dockerfile
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci-examples/without-dockerfile/README.md" >}} )

##### jobtech-ci
[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/README.md" >}} )

#### gpt-workshop
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/gpt-workshop/README.md" >}} )

#### hello-world-static-htmlpage
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/hello-world-static-htmlpage/README.md" >}} )

#### https-proxy-in-openshift
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/https-proxy-in-openshift/README.md" >}} )

#### incident-portmortem-reports
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/incident-portmortem-reports/README.md" >}} )

#### loaderio-token-server
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/loaderio-token-server/README.md" >}} )

#### ns-conf
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/ns-conf/README.md" >}} )

[TODO]({{< ref "/filtered/mirror/arbetsformedlingen/devops/ns-conf/TODO.md" >}} )

#### openaipy
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/openaipy/README.md" >}} )

#### script-container
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/script-container/README.md" >}} )

#### ssykgpt
##### tests
###### testdata
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/ssykgpt/tests/testdata/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/ssykgpt/README.md" >}} )

#### sysadm-work
##### per-jocke
###### kubernetes
####### resourcemon
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/sysadm-work/per-jocke/kubernetes/resourcemon/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/sysadm-work/per-jocke/README.md" >}} )

#### vault
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/vault/README.md" >}} )

#### vaultwarden
[README]({{< ref "/filtered/mirror/arbetsformedlingen/devops/vaultwarden/README.md" >}} )

### documentation
#### development-client
[hyper-V]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/development-client/hyper-V.md" >}} )

#### engineering
##### plattform
[Create a new project]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/plattform/Create a new project.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/plattform/README.md" >}} )

[aardvark-simple-cicd]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/plattform/aardvark-simple-cicd.md" >}} )

[deploying-a-new-application]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/plattform/deploying-a-new-application.md" >}} )

[docker-for-windows]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/plattform/docker-for-windows.md" >}} )

[docker-registry]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/plattform/docker-registry.md" >}} )

[kubernetes-tips]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/plattform/kubernetes-tips.md" >}} )

[logging]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/plattform/logging.md" >}} )

[minio]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/plattform/minio.md" >}} )

[opensearch]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/plattform/opensearch.md" >}} )

[peristance]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/plattform/peristance.md" >}} )

[python-packages]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/plattform/python-packages.md" >}} )

##### principles
[README]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/principles/README.md" >}} )

[databases]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/principles/databases.md" >}} )

[gitlab]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/principles/gitlab.md" >}} )

[kubernetes]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/principles/kubernetes.md" >}} )

##### sourcecode
[README]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/sourcecode/README.md" >}} )

[FAQ]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/FAQ.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/engineering/README.md" >}} )

#### templates
[Readme-example-nystartsjobb-OSS]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/templates/Readme-example-nystartsjobb-OSS.md" >}} )

[Readme-template-API]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/templates/Readme-template-API.md" >}} )

[Readme-template-OSS]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/templates/Readme-template-OSS.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/README.md" >}} )

[contributions]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/contributions.md" >}} )

[getting_started_external]({{< ref "/filtered/mirror/arbetsformedlingen/documentation/getting_started_external.md" >}} )

### dy
#### digital-yrkesvaegledning
[README]({{< ref "/filtered/mirror/arbetsformedlingen/dy/digital-yrkesvaegledning/README.md" >}} )

### education
#### education-api
[GETTING_STARTED]({{< ref "/filtered/mirror/arbetsformedlingen/education/education-api/GETTING_STARTED.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/education/education-api/README.md" >}} )

#### education-data-research
##### enriched_educations
###### resources
[README]({{< ref "/filtered/mirror/arbetsformedlingen/education/education-data-research/enriched_educations/resources/README.md" >}} )

##### resources
[README]({{< ref "/filtered/mirror/arbetsformedlingen/education/education-data-research/resources/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/education/education-data-research/README.md" >}} )

#### education-enrich-educations
[README]({{< ref "/filtered/mirror/arbetsformedlingen/education/education-enrich-educations/README.md" >}} )

#### education-enrich-occupations
##### tools
###### ads_resources
[README]({{< ref "/filtered/mirror/arbetsformedlingen/education/education-enrich-occupations/tools/ads_resources/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/education/education-enrich-occupations/README.md" >}} )

[README_OPENSEARCH_PROXY]({{< ref "/filtered/mirror/arbetsformedlingen/education/education-enrich-occupations/README_OPENSEARCH_PROXY.md" >}} )

#### education-frontend-app
[README]({{< ref "/filtered/mirror/arbetsformedlingen/education/education-frontend-app/README.md" >}} )

#### education-merge-educations
[README]({{< ref "/filtered/mirror/arbetsformedlingen/education/education-merge-educations/README.md" >}} )

#### education-minio-to-opensearch
[README]({{< ref "/filtered/mirror/arbetsformedlingen/education/education-minio-to-opensearch/README.md" >}} )

#### education-opensearch
[README]({{< ref "/filtered/mirror/arbetsformedlingen/education/education-opensearch/README.md" >}} )

#### education-prune-educations
[README]({{< ref "/filtered/mirror/arbetsformedlingen/education/education-prune-educations/README.md" >}} )

#### education-scraping
[README]({{< ref "/filtered/mirror/arbetsformedlingen/education/education-scraping/README.md" >}} )

#### scraping-pipeline
##### docs
[developer_setup]({{< ref "/filtered/mirror/arbetsformedlingen/education/scraping-pipeline/docs/developer_setup.md" >}} )

##### manual_runpipeline
[README]({{< ref "/filtered/mirror/arbetsformedlingen/education/scraping-pipeline/manual_runpipeline/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/education/scraping-pipeline/README.md" >}} )

### enrichment
#### jobtech-jobad-enrichments-common
##### tests
###### resources
####### wordembedding_models
######## 47f10663
[README]({{< ref "/filtered/mirror/arbetsformedlingen/enrichment/jobtech-jobad-enrichments-common/tests/resources/wordembedding_models/47f10663/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/enrichment/jobtech-jobad-enrichments-common/tests/resources/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/enrichment/jobtech-jobad-enrichments-common/README.md" >}} )

#### jobtech-jobad-enrichments-frontend-v2
[README]({{< ref "/filtered/mirror/arbetsformedlingen/enrichment/jobtech-jobad-enrichments-frontend-v2/README.md" >}} )

#### jobtech-jobad-enrichments-tf-serving
##### openshift_scripts
[README]({{< ref "/filtered/mirror/arbetsformedlingen/enrichment/jobtech-jobad-enrichments-tf-serving/openshift_scripts/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/enrichment/jobtech-jobad-enrichments-tf-serving/README.md" >}} )

#### jobtech-jobad-enrichments
##### resources
###### wordembedding_models
####### 47f10663
[README]({{< ref "/filtered/mirror/arbetsformedlingen/enrichment/jobtech-jobad-enrichments/resources/wordembedding_models/47f10663/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/enrichment/jobtech-jobad-enrichments/resources/wordembedding_models/README.md" >}} )

##### tests
[README]({{< ref "/filtered/mirror/arbetsformedlingen/enrichment/jobtech-jobad-enrichments/tests/README.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/enrichment/jobtech-jobad-enrichments/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/enrichment/jobtech-jobad-enrichments/README.md" >}} )

### giggi-motivationsmatchning
[README]({{< ref "/filtered/mirror/arbetsformedlingen/giggi-motivationsmatchning/README.md" >}} )

### individdata
#### axa-pilot
##### af-connect-services-backend
[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/README.md" >}} )

##### af-connect-services-frontend
[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/axa-pilot/af-connect-services-frontend/README.md" >}} )

##### auth-proxy
[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/axa-pilot/auth-proxy/README.md" >}} )

##### direct-api
[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/axa-pilot/direct-api/README.md" >}} )

##### fake-backend
[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/axa-pilot/fake-backend/README.md" >}} )

##### test-db-cleaner
[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/axa-pilot/test-db-cleaner/README.md" >}} )

#### connect-once
##### af-connect-compose
[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-compose/README.md" >}} )

##### af-connect-demo
[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-demo/CHANGELOG.md" >}} )

[CONTRIBUTING]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-demo/CONTRIBUTING.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-demo/README.md" >}} )

[jobtechdev]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-demo/jobtechdev.md" >}} )

##### af-connect-mock
[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-mock/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-mock/README.md" >}} )

##### af-connect-module
[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-module/CHANGELOG.md" >}} )

[CONTRIBUTING]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-module/CONTRIBUTING.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-module/README.md" >}} )

[jobtechdev]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-module/jobtechdev.md" >}} )

##### af-connect-outbox
[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-outbox/CHANGELOG.md" >}} )

[CONTRIBUTING]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-outbox/CONTRIBUTING.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-outbox/README.md" >}} )

##### af-connect-project
[CONTRIBUTING]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-project/CONTRIBUTING.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-project/README.md" >}} )

[jobtechdev]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-project/jobtechdev.md" >}} )

[jobtechdevEn]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect-project/jobtechdevEn.md" >}} )

##### af-connect
###### app
####### lib
######## common-cv-model
######### HROpen-v4.2-1
########## assessments
########### json
############ samples
############# CatalogType
[catalog_ucs]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/assessments/json/samples/CatalogType/catalog_ucs.md" >}} )

[uc001_GetAssessmentCatalog]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/assessments/json/samples/uc001_GetAssessmentCatalog.md" >}} )

[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/assessments/CONTRIBUTORS.md" >}} )

########## benefits
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/benefits/CONTRIBUTORS.md" >}} )

########## common
[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/common/README.md" >}} )

########## compensation
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/compensation/CONTRIBUTORS.md" >}} )

########## interviewing
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/interviewing/CONTRIBUTORS.md" >}} )

########## payroll
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/payroll/CONTRIBUTORS.md" >}} )

########## recruiting
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/recruiting/CONTRIBUTORS.md" >}} )

########## screening
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/screening/CONTRIBUTORS.md" >}} )

########## timecard
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/timecard/CONTRIBUTORS.md" >}} )

########## wellness
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/wellness/CONTRIBUTORS.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/CHANGELOG.md" >}} )

[CONTRIBUTING]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/CONTRIBUTING.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/README.md" >}} )

[jobtechdev]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/jobtechdev.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/CHANGELOG.md" >}} )

[CONTRIBUTING]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/CONTRIBUTING.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/README.md" >}} )

[jobtechdev]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/jobtechdev.md" >}} )

##### af-portability
###### src
####### main
######## resources
######### HROpen-v4.2-1
########## assessments
########### json
############ samples
############# CatalogType
[catalog_ucs]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/assessments/json/samples/CatalogType/catalog_ucs.md" >}} )

[uc001_GetAssessmentCatalog]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/assessments/json/samples/uc001_GetAssessmentCatalog.md" >}} )

[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/assessments/CONTRIBUTORS.md" >}} )

########## benefits
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/benefits/CONTRIBUTORS.md" >}} )

########## common
[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/common/README.md" >}} )

########## compensation
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/compensation/CONTRIBUTORS.md" >}} )

########## interviewing
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/interviewing/CONTRIBUTORS.md" >}} )

########## payroll
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/payroll/CONTRIBUTORS.md" >}} )

########## recruiting
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/recruiting/CONTRIBUTORS.md" >}} )

########## screening
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/screening/CONTRIBUTORS.md" >}} )

########## timecard
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/timecard/CONTRIBUTORS.md" >}} )

########## wellness
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/main/resources/HROpen-v4.2-1/wellness/CONTRIBUTORS.md" >}} )

####### test
######## resources
######### schemas
########## HROpen-v4.2-1
########### assessments
############ json
############# samples
############## CatalogType
[catalog_ucs]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/test/resources/schemas/HROpen-v4.2-1/assessments/json/samples/CatalogType/catalog_ucs.md" >}} )

[uc001_GetAssessmentCatalog]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/test/resources/schemas/HROpen-v4.2-1/assessments/json/samples/uc001_GetAssessmentCatalog.md" >}} )

[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/test/resources/schemas/HROpen-v4.2-1/assessments/CONTRIBUTORS.md" >}} )

########### benefits
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/test/resources/schemas/HROpen-v4.2-1/benefits/CONTRIBUTORS.md" >}} )

########### common
[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/test/resources/schemas/HROpen-v4.2-1/common/README.md" >}} )

########### compensation
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/test/resources/schemas/HROpen-v4.2-1/compensation/CONTRIBUTORS.md" >}} )

########### interviewing
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/test/resources/schemas/HROpen-v4.2-1/interviewing/CONTRIBUTORS.md" >}} )

########### payroll
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/test/resources/schemas/HROpen-v4.2-1/payroll/CONTRIBUTORS.md" >}} )

########### recruiting
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/test/resources/schemas/HROpen-v4.2-1/recruiting/CONTRIBUTORS.md" >}} )

########### screening
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/test/resources/schemas/HROpen-v4.2-1/screening/CONTRIBUTORS.md" >}} )

########### timecard
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/test/resources/schemas/HROpen-v4.2-1/timecard/CONTRIBUTORS.md" >}} )

########### wellness
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/src/test/resources/schemas/HROpen-v4.2-1/wellness/CONTRIBUTORS.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/CHANGELOG.md" >}} )

[CONTRIBUTING]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/CONTRIBUTING.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/README.md" >}} )

[jobtechdev]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/af-portability/jobtechdev.md" >}} )

##### common-cv-model
###### HROpen-v4.2-1
####### assessments
######## json
######### samples
########## CatalogType
[catalog_ucs]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/assessments/json/samples/CatalogType/catalog_ucs.md" >}} )

[uc001_GetAssessmentCatalog]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/assessments/json/samples/uc001_GetAssessmentCatalog.md" >}} )

[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/assessments/CONTRIBUTORS.md" >}} )

####### benefits
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/benefits/CONTRIBUTORS.md" >}} )

####### common
[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/common/README.md" >}} )

####### compensation
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/compensation/CONTRIBUTORS.md" >}} )

####### interviewing
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/interviewing/CONTRIBUTORS.md" >}} )

####### payroll
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/payroll/CONTRIBUTORS.md" >}} )

####### recruiting
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/recruiting/CONTRIBUTORS.md" >}} )

####### screening
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/screening/CONTRIBUTORS.md" >}} )

####### timecard
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/timecard/CONTRIBUTORS.md" >}} )

####### wellness
[CONTRIBUTORS]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/HROpen-v4.2-1/wellness/CONTRIBUTORS.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/CHANGELOG.md" >}} )

[CONTRIBUTING]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/CONTRIBUTING.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/README.md" >}} )

[jobtechdev]({{< ref "/filtered/mirror/arbetsformedlingen/individdata/connect-once/common-cv-model/jobtechdev.md" >}} )

### job-ads
#### development-tools
##### compare-environments
[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/development-tools/compare-environments/README.md" >}} )

##### create-historical-from-stream-ads
[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/development-tools/create-historical-from-stream-ads/README.md" >}} )

##### create-historical-jobad-links
[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/development-tools/create-historical-jobad-links/README.md" >}} )

##### historical-ads-file-converter
[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/development-tools/historical-ads-file-converter/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/development-tools/historical-ads-file-converter/README.md" >}} )

##### jobsearch-ad-mock-api
###### historical
[README_HISTORICAL]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/development-tools/jobsearch-ad-mock-api/historical/README_HISTORICAL.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/development-tools/jobsearch-ad-mock-api/README.md" >}} )

##### load-test-file-generator
[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/development-tools/load-test-file-generator/README.md" >}} )

#### elastic-setup
[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/elastic-setup/README.md" >}} )

#### getting-started-code-examples
##### code-examples-start-here
[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/getting-started-code-examples/code-examples-start-here/README.md" >}} )

##### frontend-example
[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/getting-started-code-examples/frontend-example/README.md" >}} )

##### historical-ads-info
[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/getting-started-code-examples/historical-ads-info/CHANGELOG.md" >}} )

[Files]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/getting-started-code-examples/historical-ads-info/Files.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/getting-started-code-examples/historical-ads-info/README.md" >}} )

##### jobstream-example
[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/getting-started-code-examples/jobstream-example/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/getting-started-code-examples/jobstream-example/README.md" >}} )

##### jupyter-notebook-on-historical-ads
[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/getting-started-code-examples/jupyter-notebook-on-historical-ads/README.md" >}} )

#### historical-ads-from-dataset-pipeline
##### argocd
[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/historical-ads-from-dataset-pipeline/argocd/README.md" >}} )

##### manual_run_pipelines
[README_start_import_manually]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/historical-ads-from-dataset-pipeline/manual_run_pipelines/README_start_import_manually.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/historical-ads-from-dataset-pipeline/README.md" >}} )

#### historical-ads-frontend
[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/historical-ads-frontend/README.md" >}} )

#### historical-search-frontend
[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/historical-search-frontend/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/historical-search-frontend/README.md" >}} )

#### job-ads-env-init
##### docs
[install-opensearch-aws]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/job-ads-env-init/docs/install-opensearch-aws.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/job-ads-env-init/README.md" >}} )

#### joblinks-search-api
##### docs
[gettingStarted]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/joblinks-search-api/docs/gettingStarted.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/joblinks-search-api/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/joblinks-search-api/README.md" >}} )

#### jobsearch-apis
##### docs
[ALPHA Release of search for historical ads - getting started]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-apis/docs/ALPHA Release of search for historical ads - getting started.md" >}} )

[AdFields]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-apis/docs/AdFields.md" >}} )

[GettingStartedJobSearchEN]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-apis/docs/GettingStartedJobSearchEN.md" >}} )

[GettingStartedJobSearchSE]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-apis/docs/GettingStartedJobSearchSE.md" >}} )

[GettingStartedJobStreamEN]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-apis/docs/GettingStartedJobStreamEN.md" >}} )

[GettingStartedJobStreamSE]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-apis/docs/GettingStartedJobStreamSE.md" >}} )

[containers]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-apis/docs/containers.md" >}} )

[feed]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-apis/docs/feed.md" >}} )

[historical_ads]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-apis/docs/historical_ads.md" >}} )

[opensearch-access]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-apis/docs/opensearch-access.md" >}} )

##### scripts
###### synonyms
[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-apis/scripts/synonyms/README.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-apis/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-apis/README.md" >}} )

#### jobsearch-importers
##### docs
[historical_importer]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-importers/docs/historical_importer.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-importers/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/jobsearch-importers/README.md" >}} )

#### search-trends
##### docs
[developer_setup]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/search-trends/docs/developer_setup.md" >}} )

[example_file]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/search-trends/docs/example_file.md" >}} )

[fields]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/search-trends/docs/fields.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/search-trends/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/job-ads/search-trends/README.md" >}} )

### joblinks
#### ad-patcher-data
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/ad-patcher-data/README.md" >}} )

#### ad-patcher
[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/ad-patcher/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/ad-patcher/README.md" >}} )

#### add-application-deadline
##### resources
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/add-application-deadline/resources/README.md" >}} )

##### tests
###### testdata
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/add-application-deadline/tests/testdata/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/add-application-deadline/README.md" >}} )

#### add-id
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/add-id/README.md" >}} )

#### add-jobad-enrichments-api-results
##### resources
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/add-jobad-enrichments-api-results/resources/README.md" >}} )

##### tests
###### testdata
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/add-jobad-enrichments-api-results/tests/testdata/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/add-jobad-enrichments-api-results/README.md" >}} )

#### add-municipality-from-place
##### tests
###### testdata
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/add-municipality-from-place/tests/testdata/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/add-municipality-from-place/README.md" >}} )

#### add-occupation-and-place
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/add-occupation-and-place/README.md" >}} )

#### common-cli-tools
##### tests
###### testdata
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/common-cli-tools/tests/testdata/README.md" >}} )

#### date-processor
##### tests
###### testdata
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/date-processor/tests/testdata/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/date-processor/README.md" >}} )

#### detect-language
##### resources
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/detect-language/resources/README.md" >}} )

##### tests
###### testdata
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/detect-language/tests/testdata/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/detect-language/README.md" >}} )

#### diy-job-board
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/diy-job-board/README.md" >}} )

#### extract-brief-description
##### resources
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/extract-brief-description/resources/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/extract-brief-description/README.md" >}} )

#### filter-missing-fields
##### tests
###### testdata
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/filter-missing-fields/tests/testdata/README.md" >}} )

#### import-to-elastic
##### docs
[developer_setup]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/import-to-elastic/docs/developer_setup.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/import-to-elastic/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/import-to-elastic/README.md" >}} )

#### job_ad_hash
##### tests
###### testdata
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/job_ad_hash/tests/testdata/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/job_ad_hash/README.md" >}} )

#### joblinks-importer
##### docs
[developer_setup]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/joblinks-importer/docs/developer_setup.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/joblinks-importer/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/joblinks-importer/README.md" >}} )

#### joblinks.gitlab.io
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/joblinks.gitlab.io/README.md" >}} )

#### launchotron
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/launchotron/README.md" >}} )

#### pipeline
##### docs
[devops]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/pipeline/docs/devops.md" >}} )

#### processing
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/processing/README.md" >}} )

#### python-processor
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/python-processor/README.md" >}} )

#### remove-description
##### resources
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/remove-description/resources/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/remove-description/README.md" >}} )

#### remove-union-representatives-from-description
##### resources
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/remove-union-representatives-from-description/resources/README.md" >}} )

##### tests
###### testdata
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/remove-union-representatives-from-description/tests/testdata/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/remove-union-representatives-from-description/README.md" >}} )

#### sample-processing-data
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/sample-processing-data/README.md" >}} )

#### scraping
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/scraping/README.md" >}} )

[README_WINDOWS]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/scraping/README_WINDOWS.md" >}} )

#### sourcelinks-processor
##### tests
###### testdata
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/sourcelinks-processor/tests/testdata/README.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/sourcelinks-processor/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/sourcelinks-processor/README.md" >}} )

#### text-2-ssyk
##### data
[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/text-2-ssyk/data/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/joblinks/text-2-ssyk/README.md" >}} )

### labor-market-forecast
#### timeseries-ads-to-timeseries
[README]({{< ref "/filtered/mirror/arbetsformedlingen/labor-market-forecast/timeseries-ads-to-timeseries/README.md" >}} )

[TODO]({{< ref "/filtered/mirror/arbetsformedlingen/labor-market-forecast/timeseries-ads-to-timeseries/TODO.md" >}} )

#### timeseries-data-research
[README]({{< ref "/filtered/mirror/arbetsformedlingen/labor-market-forecast/timeseries-data-research/README.md" >}} )

#### timeseries-merge-timeseries
[README]({{< ref "/filtered/mirror/arbetsformedlingen/labor-market-forecast/timeseries-merge-timeseries/README.md" >}} )

#### timeseries-predict-forecasts
[README]({{< ref "/filtered/mirror/arbetsformedlingen/labor-market-forecast/timeseries-predict-forecasts/README.md" >}} )

### libraries
#### anonymisering
[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/libraries/anonymisering/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/libraries/anonymisering/README.md" >}} )

#### jobtech-common
[README]({{< ref "/filtered/mirror/arbetsformedlingen/libraries/jobtech-common/README.md" >}} )

#### valuestore-module
[README]({{< ref "/filtered/mirror/arbetsformedlingen/libraries/valuestore-module/README.md" >}} )

### metadata-test
[README]({{< ref "/filtered/mirror/arbetsformedlingen/metadata-test/README.md" >}} )

### metadata
[README]({{< ref "/filtered/mirror/arbetsformedlingen/metadata/README.md" >}} )

### security
#### vuln-report
[README]({{< ref "/filtered/mirror/arbetsformedlingen/security/vuln-report/README.md" >}} )

### taxonomy-content
#### taxonomy-content-documentation
##### content
###### attributes
[attribute_definitions]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/attributes/attribute_definitions.md" >}} )

###### doc_sv
[af_ssyk]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/af_ssyk.md" >}} )

[arbetsbeskrivningar]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/arbetsbeskrivningar.md" >}} )

[attribut]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/attribut.md" >}} )

[begreppstyper]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/begreppstyper.md" >}} )

[esco_kompetenser]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/esco_kompetenser.md" >}} )

[esco_yrken]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/esco_yrken.md" >}} )

[jobbtitlar]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/jobbtitlar.md" >}} )

[kompetensbegrepp]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/kompetensbegrepp.md" >}} )

[kompetenssamlingar]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/kompetenssamlingar.md" >}} )

[prognosyrken]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/prognosyrken.md" >}} )

[relationer]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/relationer.md" >}} )

[scenarios]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/scenarios.md" >}} )

[språksamlingar]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/språksamlingar.md" >}} )

[ssyk]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/ssyk.md" >}} )

[ssyk_nivå_4]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/ssyk_nivå_4.md" >}} )

[sökbegrepp]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/sökbegrepp.md" >}} )

[versioner]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/versioner.md" >}} )

[yrkesbenämningar]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/yrkesbenämningar.md" >}} )

[yrkesområden]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/doc_sv/yrkesområden.md" >}} )

###### src
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/src/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/README.md" >}} )

##### mappings
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/mappings/README.md" >}} )

##### taxonomy-version-history
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/taxonomy-version-history/README.md" >}} )

##### version-16-dokumentation
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/version-16-dokumentation/README.md" >}} )

#### taxonomy-feedback-channel
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-feedback-channel/README.md" >}} )

#### taxonomy-lab
##### occupations-clustering
###### meetings
[20221125]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-lab/occupations-clustering/meetings/20221125.md" >}} )

[readme]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-content/taxonomy-lab/occupations-clustering/readme.md" >}} )

### taxonomy-dev
#### backend
##### clojure-gitlab-analysis
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/clojure-gitlab-analysis/README.md" >}} )

##### datomic-cloud-repl-proxy
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/datomic-cloud-repl-proxy/README.md" >}} )

##### experimental
###### af-branch
####### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/experimental/af-branch/doc/intro.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/experimental/af-branch/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/experimental/af-branch/README.md" >}} )

###### datalog-tool
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/experimental/datalog-tool/README.md" >}} )

###### taxonomy-postgres
####### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/experimental/taxonomy-postgres/doc/intro.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/experimental/taxonomy-postgres/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/experimental/taxonomy-postgres/README.md" >}} )

###### taxonomy-scripts
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/experimental/taxonomy-scripts/README.md" >}} )

###### text2ssyk-experimental
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/experimental/text2ssyk-experimental/README.md" >}} )

###### varnalyze
####### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/experimental/varnalyze/doc/intro.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/experimental/varnalyze/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/experimental/varnalyze/README.md" >}} )

##### forks
###### datahike
####### doc
######## development
[pull-api-ns]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/doc/development/pull-api-ns.md" >}} )

[backend-development]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/doc/backend-development.md" >}} )

[benchmarking]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/doc/benchmarking.md" >}} )

[cli]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/doc/cli.md" >}} )

[config]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/doc/config.md" >}} )

[contributing]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/doc/contributing.md" >}} )

[datomic_differences]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/doc/datomic_differences.md" >}} )

[entity_spec]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/doc/entity_spec.md" >}} )

[gc]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/doc/gc.md" >}} )

[logging_and_error_handling]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/doc/logging_and_error_handling.md" >}} )

[schema-migration]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/doc/schema-migration.md" >}} )

[schema]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/doc/schema.md" >}} )

[time_variance]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/doc/time_variance.md" >}} )

[versioning]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/doc/versioning.md" >}} )

####### examples
######## basic
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/examples/basic/README.md" >}} )

####### release-js
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/release-js/README.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/datahike/README.md" >}} )

###### ring-swagger-ui
[CONTRIBUTING]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/ring-swagger-ui/CONTRIBUTING.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/ring-swagger-ui/README.md" >}} )

###### wanderung
####### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/wanderung/doc/intro.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/wanderung/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/forks/wanderung/README.md" >}} )

##### jobtech-cljutils
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-cljutils/README.md" >}} )

##### jobtech-taxonomy-api-libraries
###### jobtech-taxonomy-library-clr
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries/jobtech-taxonomy-library-clr/README.md" >}} )

###### jobtech-taxonomy-library-jre
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries/jobtech-taxonomy-library-jre/README.md" >}} )

###### jobtech-taxonomy-library-rust
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries/jobtech-taxonomy-library-rust/README.md" >}} )

###### taxonomy-api-docker
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries/taxonomy-api-docker/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries/README.md" >}} )

##### jobtech-taxonomy-api
###### doc
[CHANGE_PROPOSALS]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/doc/CHANGE_PROPOSALS.md" >}} )

[REFERENCE-svenska]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/doc/REFERENCE-svenska.md" >}} )

[REFERENCE]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/doc/REFERENCE.md" >}} )

[architecture]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/doc/architecture.md" >}} )

[graphql-examples]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/doc/graphql-examples.md" >}} )

###### resources
####### popularity
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/resources/popularity/README.md" >}} )

###### test
####### datahike
[Plan]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/test/datahike/Plan.md" >}} )

[GETTINGSTARTED]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/GETTINGSTARTED.md" >}} )

[PODMAN]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/PODMAN.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/README.md" >}} )

##### jobtech-taxonomy-backup-files
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-backup-files/README.md" >}} )

##### jobtech-taxonomy-common
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-common/README.md" >}} )

##### jobtech-taxonomy-database
###### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-database/doc/intro.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-database/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-database/README.md" >}} )

##### jobtech-taxonomy-legacy-postalcodes
###### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-legacy-postalcodes/doc/intro.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-legacy-postalcodes/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-legacy-postalcodes/README.md" >}} )

##### jobtechtaxonomydotnet6
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/jobtechtaxonomydotnet6/README.md" >}} )

##### legacy-taxonomy-soap-service-java
[readme]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/legacy-taxonomy-soap-service-java/readme.md" >}} )

##### legacy-taxonomy
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/legacy-taxonomy/README.md" >}} )

##### nlp
###### annotated-document-stats
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/annotated-document-stats/README.md" >}} )

###### annotations
####### mentor-api-develop
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-develop/README.md" >}} )

####### mentor-api-prod
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-prod/README.md" >}} )

####### mentor-api-test
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-test/README.md" >}} )

###### concept-distance-evaluation
####### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/concept-distance-evaluation/doc/intro.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/concept-distance-evaluation/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/concept-distance-evaluation/README.md" >}} )

###### conconopt
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/conconopt/README.md" >}} )

###### conrec-utils
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils/README.md" >}} )

[tasks]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils/tasks.md" >}} )

###### data
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/data/README.md" >}} )

###### deep-diamond
####### dd-minimal-lein
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/deep-diamond/dd-minimal-lein/README.md" >}} )

###### enriched_keywords
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/enriched_keywords/README.md" >}} )

###### esco-skills-to-taxonomy-skills
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/esco-skills-to-taxonomy-skills/README.md" >}} )

###### green-skills-detector
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/green-skills-detector/README.md" >}} )

###### green-skills
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/green-skills/README.md" >}} )

###### jae-concept-labels
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-concept-labels/README.md" >}} )

###### jae-conrec
####### data
######## report
######### mentor-api-prod__splitclassvalidation
########## 00874731c73fdaa7bdf9c809840c4948fa2f3162
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/00874731c73fdaa7bdf9c809840c4948fa2f3162/README.md" >}} )

########## 00bf1cac01cdbbb0273b2a89c9d9c76b0dd32968
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/00bf1cac01cdbbb0273b2a89c9d9c76b0dd32968/README.md" >}} )

########## 034b72f9c90bf64790993b8e0e440c87bd547074
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/034b72f9c90bf64790993b8e0e440c87bd547074/README.md" >}} )

########## 04a403227b39a2d6c99b607b5c4eeb07074f41e4
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/04a403227b39a2d6c99b607b5c4eeb07074f41e4/README.md" >}} )

########## 05bc5a14e773f54283dae1c2aafdd8e63030006d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/05bc5a14e773f54283dae1c2aafdd8e63030006d/README.md" >}} )

########## 05d48306e4d773bbcf53d17bd13956e6cdf3663f
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/05d48306e4d773bbcf53d17bd13956e6cdf3663f/README.md" >}} )

########## 08379d9f4e803cb26eb3563b7374867aa41db88c
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/08379d9f4e803cb26eb3563b7374867aa41db88c/README.md" >}} )

########## 0914e918751875b885a1bbf52d0545519a2a4e5e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0914e918751875b885a1bbf52d0545519a2a4e5e/README.md" >}} )

########## 092ce4c594ce5c222a6a44bc9ad1f5f4c100a18a
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/092ce4c594ce5c222a6a44bc9ad1f5f4c100a18a/README.md" >}} )

########## 095bb668a1280800026893492a0dd62fb8743fa7
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/095bb668a1280800026893492a0dd62fb8743fa7/README.md" >}} )

########## 0a10e6c60ac6ce6679e6e68fa3d281bddcec2c1f
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0a10e6c60ac6ce6679e6e68fa3d281bddcec2c1f/README.md" >}} )

########## 0a59457a07931e4d0daaebe1fc6a0c2deb8ddd44
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0a59457a07931e4d0daaebe1fc6a0c2deb8ddd44/README.md" >}} )

########## 0a6331f8e9b00200bf4f4adcb3744b22822549ad
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0a6331f8e9b00200bf4f4adcb3744b22822549ad/README.md" >}} )

########## 0a8116dfe6388df22bad60d9b7b018ec7d3458b7
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0a8116dfe6388df22bad60d9b7b018ec7d3458b7/README.md" >}} )

########## 0bf85770668b13994ff85fd807c0eb143e9b18d1
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0bf85770668b13994ff85fd807c0eb143e9b18d1/README.md" >}} )

########## 0cf9536fe21388929b227f00698922a078c0cae8
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0cf9536fe21388929b227f00698922a078c0cae8/README.md" >}} )

########## 0d4623604a025f624bf4497576bee0c560e1c3ac
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0d4623604a025f624bf4497576bee0c560e1c3ac/README.md" >}} )

########## 0d54cc46e277f0841bcf620c72e2d08579556496
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0d54cc46e277f0841bcf620c72e2d08579556496/README.md" >}} )

########## 0d6b629f5ea77d17886e50f2cf63a67a58cea158
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0d6b629f5ea77d17886e50f2cf63a67a58cea158/README.md" >}} )

########## 0d7719051a5aa327729f80cd7d76198538c2b298
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0d7719051a5aa327729f80cd7d76198538c2b298/README.md" >}} )

########## 0e0a828683112cba6ba24685bb8b4906cd1eb4bd
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0e0a828683112cba6ba24685bb8b4906cd1eb4bd/README.md" >}} )

########## 0e19694138c5764ea6238f7c1160b1d04fe7edd4
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0e19694138c5764ea6238f7c1160b1d04fe7edd4/README.md" >}} )

########## 0ef1b400d10262c171ea0c7f355d6e6769a833a9
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0ef1b400d10262c171ea0c7f355d6e6769a833a9/README.md" >}} )

########## 0fa5a1e04a8b94357321c2d98d9c33efd649fa05
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0fa5a1e04a8b94357321c2d98d9c33efd649fa05/README.md" >}} )

########## 0faff9de0372b8ff13dbc6a621ce0c25c1003187
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/0faff9de0372b8ff13dbc6a621ce0c25c1003187/README.md" >}} )

########## 1049766996300b0d2cf09258d4b2c76eadab7072
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/1049766996300b0d2cf09258d4b2c76eadab7072/README.md" >}} )

########## 10df515f5ad33c4b628acbd9e74989fe88677be8
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/10df515f5ad33c4b628acbd9e74989fe88677be8/README.md" >}} )

########## 1282b4f3931101b257771c4219d3e65ae943ffc3
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/1282b4f3931101b257771c4219d3e65ae943ffc3/README.md" >}} )

########## 13003fc44636f67830f5c7a3b922757763d4d046
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/13003fc44636f67830f5c7a3b922757763d4d046/README.md" >}} )

########## 1367c1dc10418d1d9f868e2aa87d478fc8e8b03a
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/1367c1dc10418d1d9f868e2aa87d478fc8e8b03a/README.md" >}} )

########## 137c50d5f7152bac98300bed01335840efc756b1
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/137c50d5f7152bac98300bed01335840efc756b1/README.md" >}} )

########## 13c65c752f14f43ef31d5e9a30ee723e47d8d89f
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/13c65c752f14f43ef31d5e9a30ee723e47d8d89f/README.md" >}} )

########## 13c665cd4e7816144f22b251fa5cc74cb8b29e21
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/13c665cd4e7816144f22b251fa5cc74cb8b29e21/README.md" >}} )

########## 144a44a907f299f18664323ea63f2712c1b09207
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/144a44a907f299f18664323ea63f2712c1b09207/README.md" >}} )

########## 1495abc00665e785166ce5fa7ea8654a77a0da14
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/1495abc00665e785166ce5fa7ea8654a77a0da14/README.md" >}} )

########## 14bc61d04ad74b7b9f512539a5ff636490bf1006
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/14bc61d04ad74b7b9f512539a5ff636490bf1006/README.md" >}} )

########## 155626e681195efef0f495406fa22f49cc1bce99
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/155626e681195efef0f495406fa22f49cc1bce99/README.md" >}} )

########## 160d8342af45cdb1484e274875acb1484548dbfc
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/160d8342af45cdb1484e274875acb1484548dbfc/README.md" >}} )

########## 184009bd955113ffa048164676625c64841fa127
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/184009bd955113ffa048164676625c64841fa127/README.md" >}} )

########## 18672331af8bf118c4dedce801affd65f7c389d9
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/18672331af8bf118c4dedce801affd65f7c389d9/README.md" >}} )

########## 1971835565f5c368845f92553e0570b9234b75b6
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/1971835565f5c368845f92553e0570b9234b75b6/README.md" >}} )

########## 19bfc665c3f4297283fd69ad784faa28330de256
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/19bfc665c3f4297283fd69ad784faa28330de256/README.md" >}} )

########## 1aed0777439829031a06af34350e0806c21ba577
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/1aed0777439829031a06af34350e0806c21ba577/README.md" >}} )

########## 1c070ba9333bbd3dc73e0155ab00627ec44783c1
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/1c070ba9333bbd3dc73e0155ab00627ec44783c1/README.md" >}} )

########## 1ce3f5f82279b218d9aba88deeba37b03091f9e7
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/1ce3f5f82279b218d9aba88deeba37b03091f9e7/README.md" >}} )

########## 1d8aebbad421ff1a9d48566724defe6aecc7a652
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/1d8aebbad421ff1a9d48566724defe6aecc7a652/README.md" >}} )

########## 1deb848a1e31fb359feafb6f12eaaafc9ab25409
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/1deb848a1e31fb359feafb6f12eaaafc9ab25409/README.md" >}} )

########## 1df568d2c70ddda50f83b3ebd06bf180d8f0f8a2
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/1df568d2c70ddda50f83b3ebd06bf180d8f0f8a2/README.md" >}} )

########## 1e392120cffc47c1ace75e4d77b0e253ca6ad473
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/1e392120cffc47c1ace75e4d77b0e253ca6ad473/README.md" >}} )

########## 1e407579d99e8b4fb31a37132bbe5dd8b442185f
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/1e407579d99e8b4fb31a37132bbe5dd8b442185f/README.md" >}} )

########## 1eef6beebb71dca2e2e4e1af244e8cbb4e2209a9
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/1eef6beebb71dca2e2e4e1af244e8cbb4e2209a9/README.md" >}} )

########## 20cce8f4b27a836be6fb42767f2f7459999e0da1
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/20cce8f4b27a836be6fb42767f2f7459999e0da1/README.md" >}} )

########## 213e2ebe2f6bd70711e2fb0f560984785aaefe5c
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/213e2ebe2f6bd70711e2fb0f560984785aaefe5c/README.md" >}} )

########## 2178588a48d3bf04096aff9aa8f8ce3d7a7b79d9
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/2178588a48d3bf04096aff9aa8f8ce3d7a7b79d9/README.md" >}} )

########## 21c07e29c01ec244afb0b481817025b2ee4da92c
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/21c07e29c01ec244afb0b481817025b2ee4da92c/README.md" >}} )

########## 21e11075a43ae09e2d860d4cd7ac3eacfd429b5e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/21e11075a43ae09e2d860d4cd7ac3eacfd429b5e/README.md" >}} )

########## 21eebc409d4bea8bd2e060a2454425d4fa0be4fb
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/21eebc409d4bea8bd2e060a2454425d4fa0be4fb/README.md" >}} )

########## 22a154d605860bb21bcc2d5e1a211a3114427bf7
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/22a154d605860bb21bcc2d5e1a211a3114427bf7/README.md" >}} )

########## 2332793eb21259dd3b0f13eced6a326bddde5ba5
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/2332793eb21259dd3b0f13eced6a326bddde5ba5/README.md" >}} )

########## 238a2214088cc75c14445ef380f98f446d7e42e0
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/238a2214088cc75c14445ef380f98f446d7e42e0/README.md" >}} )

########## 23e558a19fd6a812f6f3bc93dbb6aeaf66fa9b53
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/23e558a19fd6a812f6f3bc93dbb6aeaf66fa9b53/README.md" >}} )

########## 25382e7277fdf312b9e66d656bdc0ed8cd559968
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/25382e7277fdf312b9e66d656bdc0ed8cd559968/README.md" >}} )

########## 2572378814659b4b919f4015904ec46d4c03e830
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/2572378814659b4b919f4015904ec46d4c03e830/README.md" >}} )

########## 2638b7573a9c438b473a932ea7e6ada6a72db937
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/2638b7573a9c438b473a932ea7e6ada6a72db937/README.md" >}} )

########## 268fe9dcc7b7d5b2eb6e6102851c49a9e89d69ad
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/268fe9dcc7b7d5b2eb6e6102851c49a9e89d69ad/README.md" >}} )

########## 26a2128e1d6bd70c3a5e5800235cb0aec1c92b7e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/26a2128e1d6bd70c3a5e5800235cb0aec1c92b7e/README.md" >}} )

########## 2722fc9056840fcea766fbd68fd3d50973695c34
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/2722fc9056840fcea766fbd68fd3d50973695c34/README.md" >}} )

########## 2845183ea273d0a8924627949cbb529adf6a5681
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/2845183ea273d0a8924627949cbb529adf6a5681/README.md" >}} )

########## 28dc2838ac582dfabacbc983ea17bd6b5db1f271
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/28dc2838ac582dfabacbc983ea17bd6b5db1f271/README.md" >}} )

########## 28ebb96b7474526cfd61fbf2ef3aecec3e7d18c2
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/28ebb96b7474526cfd61fbf2ef3aecec3e7d18c2/README.md" >}} )

########## 2aed92af507384817ecdd325075f821ea090630d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/2aed92af507384817ecdd325075f821ea090630d/README.md" >}} )

########## 2d2968565e2f881e19f1f6015457ef6d101be9b6
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/2d2968565e2f881e19f1f6015457ef6d101be9b6/README.md" >}} )

########## 2d450972b617096cc24619f30bc6e08cc437c241
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/2d450972b617096cc24619f30bc6e08cc437c241/README.md" >}} )

########## 2e2ffd3f3692a1f4208697616f6e4fdcccc39532
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/2e2ffd3f3692a1f4208697616f6e4fdcccc39532/README.md" >}} )

########## 2e62ae66246fc5e1bd3e89c95479063c276a27c2
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/2e62ae66246fc5e1bd3e89c95479063c276a27c2/README.md" >}} )

########## 2e9abcd8a76bb3f6a198efdf04ba589eaebf0796
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/2e9abcd8a76bb3f6a198efdf04ba589eaebf0796/README.md" >}} )

########## 2f20f13844dfe5604e23bf553e2fc137bc860add
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/2f20f13844dfe5604e23bf553e2fc137bc860add/README.md" >}} )

########## 302ab343c9fc3500af05fcfc20755f87160bcc36
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/302ab343c9fc3500af05fcfc20755f87160bcc36/README.md" >}} )

########## 302ec3944e8160f054efb425020a361b5eacebcd
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/302ec3944e8160f054efb425020a361b5eacebcd/README.md" >}} )

########## 30aa3da472cee97ab5affe923330e07e0f1805f7
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/30aa3da472cee97ab5affe923330e07e0f1805f7/README.md" >}} )

########## 31ba6c0cc9f69c9de589da8aaaecdfd4bcf506ee
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/31ba6c0cc9f69c9de589da8aaaecdfd4bcf506ee/README.md" >}} )

########## 31e638dbf7771764b147d49accbe448ad5df1d80
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/31e638dbf7771764b147d49accbe448ad5df1d80/README.md" >}} )

########## 35316d14ffebf497f63e91dfbf4b18c069143029
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/35316d14ffebf497f63e91dfbf4b18c069143029/README.md" >}} )

########## 35d0de2127d00f201a8907125760eaf1a4b60865
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/35d0de2127d00f201a8907125760eaf1a4b60865/README.md" >}} )

########## 37e0f74e97eb7114f85fe980165f7f16c53ddbf6
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/37e0f74e97eb7114f85fe980165f7f16c53ddbf6/README.md" >}} )

########## 3a4e7f56259052cf42908da85ba1b4627eb9cb3a
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/3a4e7f56259052cf42908da85ba1b4627eb9cb3a/README.md" >}} )

########## 3b210407fa80990e19aeca74b5146e9da2fa1930
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/3b210407fa80990e19aeca74b5146e9da2fa1930/README.md" >}} )

########## 3cc3b17ec336e06974d52cd998e4bb069a9ddf9d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/3cc3b17ec336e06974d52cd998e4bb069a9ddf9d/README.md" >}} )

########## 3cc477946ba535dac80da3318f9aad6a2134109d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/3cc477946ba535dac80da3318f9aad6a2134109d/README.md" >}} )

########## 3e301624d4de621004aad762ba2b9f4fdf8fc4bc
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/3e301624d4de621004aad762ba2b9f4fdf8fc4bc/README.md" >}} )

########## 3efe0af127fd5219a1910d7bfdea4fb2fd6e9dff
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/3efe0af127fd5219a1910d7bfdea4fb2fd6e9dff/README.md" >}} )

########## 3f9131e596d8ca16404932d7a9e322cf9cadd6ff
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/3f9131e596d8ca16404932d7a9e322cf9cadd6ff/README.md" >}} )

########## 3fd6cefff404c81db9af63db3ea37d2b7bfc675d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/3fd6cefff404c81db9af63db3ea37d2b7bfc675d/README.md" >}} )

########## 414754a64b126c8cbda62894a3b7785fcb33e522
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/414754a64b126c8cbda62894a3b7785fcb33e522/README.md" >}} )

########## 4163717d87b581f13937a547445551413bc0e27e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4163717d87b581f13937a547445551413bc0e27e/README.md" >}} )

########## 4165e816e4de0e8e450ba0d5019230d48d5f6e6d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4165e816e4de0e8e450ba0d5019230d48d5f6e6d/README.md" >}} )

########## 417dc0e78bda0039bb1b323e44f71f62830130d4
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/417dc0e78bda0039bb1b323e44f71f62830130d4/README.md" >}} )

########## 41d77150d02d0fe8ef9b73e408cb6c6aa3842612
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/41d77150d02d0fe8ef9b73e408cb6c6aa3842612/README.md" >}} )

########## 4221789873537a4f304c781a5a05e853bed97b08
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4221789873537a4f304c781a5a05e853bed97b08/README.md" >}} )

########## 42334249caccbe4098740846ba1757bbb3d3395f
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/42334249caccbe4098740846ba1757bbb3d3395f/README.md" >}} )

########## 4273512a518237f0261f68f792c83e5ce109313c
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4273512a518237f0261f68f792c83e5ce109313c/README.md" >}} )

########## 4309ddbc927d426adb8c6ac9d8beb97bef52b411
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4309ddbc927d426adb8c6ac9d8beb97bef52b411/README.md" >}} )

########## 454746901320a83ae61b4962314d1beae0b455ea
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/454746901320a83ae61b4962314d1beae0b455ea/README.md" >}} )

########## 454c4d4d198fba391a0695627bcbb1507de3d5f4
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/454c4d4d198fba391a0695627bcbb1507de3d5f4/README.md" >}} )

########## 4614c08cfb36c9db1ead1657a1c7431b29c59814
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4614c08cfb36c9db1ead1657a1c7431b29c59814/README.md" >}} )

########## 4824fbf4bd5339f88c2bef671c10b01cdf937b8b
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4824fbf4bd5339f88c2bef671c10b01cdf937b8b/README.md" >}} )

########## 48d83075b079b934697307b7d4d7778157ec0d79
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/48d83075b079b934697307b7d4d7778157ec0d79/README.md" >}} )

########## 4920231ec7cc1d5bae158096c7d76b097a830ce9
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4920231ec7cc1d5bae158096c7d76b097a830ce9/README.md" >}} )

########## 4a1566fdf6a7313b9cc5995e9147b9530fe25e4d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4a1566fdf6a7313b9cc5995e9147b9530fe25e4d/README.md" >}} )

########## 4a2e97c33065f2517fb15a4d168acd6ae5a5c261
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4a2e97c33065f2517fb15a4d168acd6ae5a5c261/README.md" >}} )

########## 4ce505f9a7a17a146375260972d57cb72833d8f9
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4ce505f9a7a17a146375260972d57cb72833d8f9/README.md" >}} )

########## 4d11d02602671c3efb9bf382c4adebfae28baaa5
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4d11d02602671c3efb9bf382c4adebfae28baaa5/README.md" >}} )

########## 4d54c6089fece6364e969a6d95943d2336d94bbd
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4d54c6089fece6364e969a6d95943d2336d94bbd/README.md" >}} )

########## 4edd712949d3918ba2621db4337ae64cccdedb27
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4edd712949d3918ba2621db4337ae64cccdedb27/README.md" >}} )

########## 4f034eda1474a494f5cc559aa609c3225c58ee1c
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4f034eda1474a494f5cc559aa609c3225c58ee1c/README.md" >}} )

########## 4fc417561c7a6fc79a3760ce980c2acfb1a0444d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/4fc417561c7a6fc79a3760ce980c2acfb1a0444d/README.md" >}} )

########## 5144dc879171e61bb80ff81d15a3f36838ed886e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/5144dc879171e61bb80ff81d15a3f36838ed886e/README.md" >}} )

########## 51477697c59e0af70090001ae6a891995b61b46b
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/51477697c59e0af70090001ae6a891995b61b46b/README.md" >}} )

########## 52434a08efa83d1e967ca399ab6dae27065c8cdf
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/52434a08efa83d1e967ca399ab6dae27065c8cdf/README.md" >}} )

########## 52890ecd1ebc4383c6091095fb204ff6b8dfbaa5
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/52890ecd1ebc4383c6091095fb204ff6b8dfbaa5/README.md" >}} )

########## 52bbbefa4601c31531a9157314c135fa6134eda9
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/52bbbefa4601c31531a9157314c135fa6134eda9/README.md" >}} )

########## 533244c3cd1050bf72a4d37efe81e2a40fdd13f2
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/533244c3cd1050bf72a4d37efe81e2a40fdd13f2/README.md" >}} )

########## 533608935f7abb7526c90cc116cc9faba09b0634
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/533608935f7abb7526c90cc116cc9faba09b0634/README.md" >}} )

########## 53c7908ee5e704bbc224fdff3bbc8042e76ac6c6
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/53c7908ee5e704bbc224fdff3bbc8042e76ac6c6/README.md" >}} )

########## 54817d78dad10d4b211f6d3dd45ab766de5950f2
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/54817d78dad10d4b211f6d3dd45ab766de5950f2/README.md" >}} )

########## 562ac00d3f5f1b9549207dfcb5e47a5136b6543e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/562ac00d3f5f1b9549207dfcb5e47a5136b6543e/README.md" >}} )

########## 56df2674736aa8dfa308f8e1e05a46255101626e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/56df2674736aa8dfa308f8e1e05a46255101626e/README.md" >}} )

########## 57361d3d0641c59e2bd3827b83fe8d6c51b77282
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/57361d3d0641c59e2bd3827b83fe8d6c51b77282/README.md" >}} )

########## 5783c9dc622bad380151f4e18b44d6643668d527
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/5783c9dc622bad380151f4e18b44d6643668d527/README.md" >}} )

########## 57c0a750530ab09b439ab41cb538a54f3fad81b4
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/57c0a750530ab09b439ab41cb538a54f3fad81b4/README.md" >}} )

########## 5892bf11fda649d176599e0fef802ed71fc7241c
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/5892bf11fda649d176599e0fef802ed71fc7241c/README.md" >}} )

########## 58c7195790abff22f480444bcb914b09a21dc665
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/58c7195790abff22f480444bcb914b09a21dc665/README.md" >}} )

########## 5af84e33367dcfb906fdb9c5f069f6afb9272a5e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/5af84e33367dcfb906fdb9c5f069f6afb9272a5e/README.md" >}} )

########## 5c365f7ab47e65acf027a5e0cfd44579a335249c
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/5c365f7ab47e65acf027a5e0cfd44579a335249c/README.md" >}} )

########## 5c71a8ff69856f931cc8826a113c18509bfd7517
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/5c71a8ff69856f931cc8826a113c18509bfd7517/README.md" >}} )

########## 5cad43f114653fa1718d6bb21c4b2549aaf2e0f6
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/5cad43f114653fa1718d6bb21c4b2549aaf2e0f6/README.md" >}} )

########## 5dafc91db69f2097ed767b5511705c81eeca6925
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/5dafc91db69f2097ed767b5511705c81eeca6925/README.md" >}} )

########## 5f00f44c6ad39b2fe0418e91b164fa4c0bffc9f4
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/5f00f44c6ad39b2fe0418e91b164fa4c0bffc9f4/README.md" >}} )

########## 60b09ded6a74e827b226ed533cc7a79e36a4a82d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/60b09ded6a74e827b226ed533cc7a79e36a4a82d/README.md" >}} )

########## 626a6ba53c42915d9bb7a953ecc2a46dd7b53039
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/626a6ba53c42915d9bb7a953ecc2a46dd7b53039/README.md" >}} )

########## 630e1993585434449f91d0ba9acd627715772c4b
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/630e1993585434449f91d0ba9acd627715772c4b/README.md" >}} )

########## 63cea388c08b4b55c210ed71ea13a18f6a50bb3c
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/63cea388c08b4b55c210ed71ea13a18f6a50bb3c/README.md" >}} )

########## 654c5be7b856dcc8b349b028670faf0abc75ad50
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/654c5be7b856dcc8b349b028670faf0abc75ad50/README.md" >}} )

########## 6593e777eae9e83b513b6b6a973b2fbc4f8cd00d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/6593e777eae9e83b513b6b6a973b2fbc4f8cd00d/README.md" >}} )

########## 6603c3fae72df49d1d9d637f017d4060b0199ea5
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/6603c3fae72df49d1d9d637f017d4060b0199ea5/README.md" >}} )

########## 6680eecb134bd571b9d0606820c5d64da56084a0
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/6680eecb134bd571b9d0606820c5d64da56084a0/README.md" >}} )

########## 677128d0054f17538edc14fa74fe8db7b13a58ff
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/677128d0054f17538edc14fa74fe8db7b13a58ff/README.md" >}} )

########## 677869e762dfa8f9722133d1657625cc00484712
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/677869e762dfa8f9722133d1657625cc00484712/README.md" >}} )

########## 67ac666636aea0185ec64a48384dd8b51c22b095
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/67ac666636aea0185ec64a48384dd8b51c22b095/README.md" >}} )

########## 67e3d27efdf87352c67377582678a15587c457f4
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/67e3d27efdf87352c67377582678a15587c457f4/README.md" >}} )

########## 698eee8995206496bc532126434a56177897fb62
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/698eee8995206496bc532126434a56177897fb62/README.md" >}} )

########## 69aadd8df09c070ce02bf117fd58960db758681b
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/69aadd8df09c070ce02bf117fd58960db758681b/README.md" >}} )

########## 6b8d884c16689bce3cebb0091d1de7518089e688
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/6b8d884c16689bce3cebb0091d1de7518089e688/README.md" >}} )

########## 6de9f296d2c24eea893ecd7bfe232b54ab76da53
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/6de9f296d2c24eea893ecd7bfe232b54ab76da53/README.md" >}} )

########## 6df6ada741a00e8081090eb37f0ed34f0f135375
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/6df6ada741a00e8081090eb37f0ed34f0f135375/README.md" >}} )

########## 73fbdd8ef5cb9f2fbfdfb546f4ac83c9e8308e55
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/73fbdd8ef5cb9f2fbfdfb546f4ac83c9e8308e55/README.md" >}} )

########## 74a67ccebb6de28e496b7e0326ee97d442206560
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/74a67ccebb6de28e496b7e0326ee97d442206560/README.md" >}} )

########## 75b19c1f4ae809fe3e128f80dc2e9903f34075e8
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/75b19c1f4ae809fe3e128f80dc2e9903f34075e8/README.md" >}} )

########## 767de45ca2ca217e2b1767befc64fd4e494448f5
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/767de45ca2ca217e2b1767befc64fd4e494448f5/README.md" >}} )

########## 784532fb953c09ba06b0aa25235479447e38598d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/784532fb953c09ba06b0aa25235479447e38598d/README.md" >}} )

########## 79083155f8b027b98cb8b53f93db003df6bb6530
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/79083155f8b027b98cb8b53f93db003df6bb6530/README.md" >}} )

########## 796149eab1f06c4eff2779526045482401a6b523
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/796149eab1f06c4eff2779526045482401a6b523/README.md" >}} )

########## 79a4b109abf66f3e61c45f6e2ea398c8929b86e8
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/79a4b109abf66f3e61c45f6e2ea398c8929b86e8/README.md" >}} )

########## 7a541a9e1142d2a489d55ff4c12b2f57c76c8671
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/7a541a9e1142d2a489d55ff4c12b2f57c76c8671/README.md" >}} )

########## 7a8145bfb27c49a542f78969943b9801c3962b35
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/7a8145bfb27c49a542f78969943b9801c3962b35/README.md" >}} )

########## 7c58146fba7492bdaeaf431927f20020fe814ecf
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/7c58146fba7492bdaeaf431927f20020fe814ecf/README.md" >}} )

########## 7cd013ce7856742a92b587d94645d2db69d4a321
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/7cd013ce7856742a92b587d94645d2db69d4a321/README.md" >}} )

########## 7ea04a2468ea094bf463c1e490fe735e44cb02c1
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/7ea04a2468ea094bf463c1e490fe735e44cb02c1/README.md" >}} )

########## 7f341c008dbdd1db6280cb8b7fe34c552e80a7b9
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/7f341c008dbdd1db6280cb8b7fe34c552e80a7b9/README.md" >}} )

########## 8028cec398a35e305c86a908bc2d85ea7905c939
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/8028cec398a35e305c86a908bc2d85ea7905c939/README.md" >}} )

########## 80644e3f8c24b69108efe48a754363f646dc2681
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/80644e3f8c24b69108efe48a754363f646dc2681/README.md" >}} )

########## 80b16c1566e1438cb3a1392e98ace54d2cbe44d8
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/80b16c1566e1438cb3a1392e98ace54d2cbe44d8/README.md" >}} )

########## 825e0fde57c3129676c6784a288e46ef2a4c2c63
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/825e0fde57c3129676c6784a288e46ef2a4c2c63/README.md" >}} )

########## 82fa2cf84d9a23b59587a62a1014ff0f6fee98f6
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/82fa2cf84d9a23b59587a62a1014ff0f6fee98f6/README.md" >}} )

########## 83127325b0c675f0dea81c2c7cfa783ba0475e3b
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/83127325b0c675f0dea81c2c7cfa783ba0475e3b/README.md" >}} )

########## 83b04a2f6d77e294976f0684e60a2a2dfc721c7d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/83b04a2f6d77e294976f0684e60a2a2dfc721c7d/README.md" >}} )

########## 848348a23f7de451b0a9f8d81b529d6b1b03e7d9
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/848348a23f7de451b0a9f8d81b529d6b1b03e7d9/README.md" >}} )

########## 849cd89027907917be5f16c8c34491badc7af0e9
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/849cd89027907917be5f16c8c34491badc7af0e9/README.md" >}} )

########## 871f5d07292ceb98cc822ed1c356bf1789caa081
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/871f5d07292ceb98cc822ed1c356bf1789caa081/README.md" >}} )

########## 88978f186054863811fc58a9d27db3779723cdc5
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/88978f186054863811fc58a9d27db3779723cdc5/README.md" >}} )

########## 896887092a16ebf2a624c21c76134edb8ffff5a1
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/896887092a16ebf2a624c21c76134edb8ffff5a1/README.md" >}} )

########## 89fbd5b29f4bdbe3603e9fff502254898a7f98d2
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/89fbd5b29f4bdbe3603e9fff502254898a7f98d2/README.md" >}} )

########## 8ac4b3165f08fd6817e0045d4ff33c288ac22b32
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/8ac4b3165f08fd6817e0045d4ff33c288ac22b32/README.md" >}} )

########## 8acc4ede54ccf96f6245e98b98718f2c0f212c0c
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/8acc4ede54ccf96f6245e98b98718f2c0f212c0c/README.md" >}} )

########## 8acd799aec55bc26190fa69f58a54c5ebafccb92
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/8acd799aec55bc26190fa69f58a54c5ebafccb92/README.md" >}} )

########## 8b01a949e352ec8931a1ff8f43b1d6fdbcf47ca8
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/8b01a949e352ec8931a1ff8f43b1d6fdbcf47ca8/README.md" >}} )

########## 8b6fe377d1a67a10f333e00799d9782cab86deac
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/8b6fe377d1a67a10f333e00799d9782cab86deac/README.md" >}} )

########## 8c13b8d18e2d368ca61498651c9a275e5d9b4606
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/8c13b8d18e2d368ca61498651c9a275e5d9b4606/README.md" >}} )

########## 8c37e36b0166ea70a6733a320996f4c1b55ecdf9
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/8c37e36b0166ea70a6733a320996f4c1b55ecdf9/README.md" >}} )

########## 8dc47e8ceedb5d83cb5d18176d5a6334f0677d2d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/8dc47e8ceedb5d83cb5d18176d5a6334f0677d2d/README.md" >}} )

########## 8e5d29f43f18119a1f5eba7dabb253e143a26bc2
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/8e5d29f43f18119a1f5eba7dabb253e143a26bc2/README.md" >}} )

########## 8f060051cafa16c1eb5b72ee86a8713423ec9fc1
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/8f060051cafa16c1eb5b72ee86a8713423ec9fc1/README.md" >}} )

########## 914a69f83622743524a934fbc34935aeb355051c
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/914a69f83622743524a934fbc34935aeb355051c/README.md" >}} )

########## 923510140d7ce2ffdc5618fa84d80d1512e5d093
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/923510140d7ce2ffdc5618fa84d80d1512e5d093/README.md" >}} )

########## 929704a78e007aaadbe4c1833781237621108b7d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/929704a78e007aaadbe4c1833781237621108b7d/README.md" >}} )

########## 93d36bcc8319fe152dca10adffcbc97b68aeb52b
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/93d36bcc8319fe152dca10adffcbc97b68aeb52b/README.md" >}} )

########## 947cc26f2d0b57898bc4daa49d7bdc4d47643034
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/947cc26f2d0b57898bc4daa49d7bdc4d47643034/README.md" >}} )

########## 9491fcb5567f9ac058fc97b72dd249bbb2bd64d9
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/9491fcb5567f9ac058fc97b72dd249bbb2bd64d9/README.md" >}} )

########## 9511f2c1644b8eaede25a996d2281e125f3032e9
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/9511f2c1644b8eaede25a996d2281e125f3032e9/README.md" >}} )

########## 957e21e0c5fcb07f446d34d76b683685a2cb8288
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/957e21e0c5fcb07f446d34d76b683685a2cb8288/README.md" >}} )

########## 9668e4d835cd8d764e894aae59cc4ae2e1a81983
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/9668e4d835cd8d764e894aae59cc4ae2e1a81983/README.md" >}} )

########## 96aa62677422b27a2d1d7e80af6e5d882b05a848
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/96aa62677422b27a2d1d7e80af6e5d882b05a848/README.md" >}} )

########## 96d98f41142fc17b863cc6fdecbb902e8cbaa38f
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/96d98f41142fc17b863cc6fdecbb902e8cbaa38f/README.md" >}} )

########## 9705f62d1933363a0ebbe501a678ce443b30f789
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/9705f62d1933363a0ebbe501a678ce443b30f789/README.md" >}} )

########## 970e89f1793568737aefbbedc4a98087514df6cb
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/970e89f1793568737aefbbedc4a98087514df6cb/README.md" >}} )

########## 975efbd24abd8f41fec487461649aa25dce870b8
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/975efbd24abd8f41fec487461649aa25dce870b8/README.md" >}} )

########## 9789907ab302790a653042d367df08c2dc85df6f
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/9789907ab302790a653042d367df08c2dc85df6f/README.md" >}} )

########## 9817b17766a08ad5155ef0b1ac6663ab8a5cfcfd
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/9817b17766a08ad5155ef0b1ac6663ab8a5cfcfd/README.md" >}} )

########## 98fec7d685f6876a8afb624d069011566b6e306d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/98fec7d685f6876a8afb624d069011566b6e306d/README.md" >}} )

########## 9a5754f7b6d141ed48bb82de698db42b2d0828b4
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/9a5754f7b6d141ed48bb82de698db42b2d0828b4/README.md" >}} )

########## 9a780393f3289fe6180f1dc5474899dc6a509439
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/9a780393f3289fe6180f1dc5474899dc6a509439/README.md" >}} )

########## 9c1b85ec4b4a1e04ef474eaa1111939b1bf928b2
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/9c1b85ec4b4a1e04ef474eaa1111939b1bf928b2/README.md" >}} )

########## 9dae89296b0a802f295f8b6c0a36246d261bb03b
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/9dae89296b0a802f295f8b6c0a36246d261bb03b/README.md" >}} )

########## 9f9956fb8779768c6e3d5d5d91841c8d0f2cf196
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/9f9956fb8779768c6e3d5d5d91841c8d0f2cf196/README.md" >}} )

########## a0d6b383fb834ed5469b29ccb22b1ac0aea0e94c
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/a0d6b383fb834ed5469b29ccb22b1ac0aea0e94c/README.md" >}} )

########## a18ed281b4789020cc2881bc62a825c2645f5972
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/a18ed281b4789020cc2881bc62a825c2645f5972/README.md" >}} )

########## a2e07e72b8ce6012bf74e2e66de5386cf0a64524
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/a2e07e72b8ce6012bf74e2e66de5386cf0a64524/README.md" >}} )

########## a401c9c669084c2a71d0e7a35c0664793b89456c
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/a401c9c669084c2a71d0e7a35c0664793b89456c/README.md" >}} )

########## a4408c6ae6881c2493abfa59b48516389e4447e4
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/a4408c6ae6881c2493abfa59b48516389e4447e4/README.md" >}} )

########## a44cb9e1050b38bf2d668965fa56a4ac972cecdb
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/a44cb9e1050b38bf2d668965fa56a4ac972cecdb/README.md" >}} )

########## a7689f967f011895ecabbc4ab677107088b332ee
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/a7689f967f011895ecabbc4ab677107088b332ee/README.md" >}} )

########## a76dc3af57bfbf4f8419081817c7060a8d140538
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/a76dc3af57bfbf4f8419081817c7060a8d140538/README.md" >}} )

########## aa27decaca5543c99332108914bb2dc2153bca60
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/aa27decaca5543c99332108914bb2dc2153bca60/README.md" >}} )

########## aa94d4705246e9ad2aee0ebd016a06e4802b3cdb
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/aa94d4705246e9ad2aee0ebd016a06e4802b3cdb/README.md" >}} )

########## aaaa8e9a02e26b1cb09376af2487f07038c6e501
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/aaaa8e9a02e26b1cb09376af2487f07038c6e501/README.md" >}} )

########## aab9fe44d567b0c31b6e022428b74b117fad643c
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/aab9fe44d567b0c31b6e022428b74b117fad643c/README.md" >}} )

########## aad3bce95218a0df94c5707b99edfe9ed5c4a8ef
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/aad3bce95218a0df94c5707b99edfe9ed5c4a8ef/README.md" >}} )

########## ac84deed682488768004e722531d540951ba4d07
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ac84deed682488768004e722531d540951ba4d07/README.md" >}} )

########## ac9c6f8c411eea6abb8ee1be5495ecec2b05bc09
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ac9c6f8c411eea6abb8ee1be5495ecec2b05bc09/README.md" >}} )

########## ad568c8173aab7c7673e307f86b0e3e31441c52b
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ad568c8173aab7c7673e307f86b0e3e31441c52b/README.md" >}} )

########## ad59e1f2ae3c95f03045f5daf82696da9ac0179d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ad59e1f2ae3c95f03045f5daf82696da9ac0179d/README.md" >}} )

########## afc4723682481cbaa36b4745532d183bd193c813
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/afc4723682481cbaa36b4745532d183bd193c813/README.md" >}} )

########## b01ca219d6b12e910fa64012a3ddf68cf310e600
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/b01ca219d6b12e910fa64012a3ddf68cf310e600/README.md" >}} )

########## b04d0f63c11afadfe70fc3091f3520f1bc9fb8df
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/b04d0f63c11afadfe70fc3091f3520f1bc9fb8df/README.md" >}} )

########## b142e1264b72aca5154d4981544fc0f765ebec04
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/b142e1264b72aca5154d4981544fc0f765ebec04/README.md" >}} )

########## b1d42214e88526a28fde29ae4dd8495e568d2562
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/b1d42214e88526a28fde29ae4dd8495e568d2562/README.md" >}} )

########## b235287053a7f0e6ecd110db0c2a473ae67b3192
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/b235287053a7f0e6ecd110db0c2a473ae67b3192/README.md" >}} )

########## b307407f8cf0efc70d1e7fdb87f7246e153ab247
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/b307407f8cf0efc70d1e7fdb87f7246e153ab247/README.md" >}} )

########## b35871cfc42cf53a9e67db142cd79a7e341f0de6
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/b35871cfc42cf53a9e67db142cd79a7e341f0de6/README.md" >}} )

########## b3bb118dd3a8537524f8e9d8c5bc0824573c0e81
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/b3bb118dd3a8537524f8e9d8c5bc0824573c0e81/README.md" >}} )

########## b730abeeda7fddbeca5215654b4248c888ea27c3
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/b730abeeda7fddbeca5215654b4248c888ea27c3/README.md" >}} )

########## b76503dd7531a1357cfd1f89510033201661306f
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/b76503dd7531a1357cfd1f89510033201661306f/README.md" >}} )

########## b7b5989c506eba1cdf5575cb441e0535395559f6
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/b7b5989c506eba1cdf5575cb441e0535395559f6/README.md" >}} )

########## b7c7df959e69e118343b8fbb46e3e1c299f6fe9d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/b7c7df959e69e118343b8fbb46e3e1c299f6fe9d/README.md" >}} )

########## b8a1ad018bec478d0ae0c39ce2c477b8f620981b
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/b8a1ad018bec478d0ae0c39ce2c477b8f620981b/README.md" >}} )

########## b8d648396abce24a5ed76efa272ca5f01bc46c03
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/b8d648396abce24a5ed76efa272ca5f01bc46c03/README.md" >}} )

########## b8e8a46f7464ff3662beeacd1396d78ef8a8c3ee
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/b8e8a46f7464ff3662beeacd1396d78ef8a8c3ee/README.md" >}} )

########## ba19648b9fd79536fb5c61eb29cb38c189d176d5
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ba19648b9fd79536fb5c61eb29cb38c189d176d5/README.md" >}} )

########## bb54bc4136315e51ce489fea165664ce9f33e474
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/bb54bc4136315e51ce489fea165664ce9f33e474/README.md" >}} )

########## bbbeedde2d16153674c8255493454e11184e81c2
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/bbbeedde2d16153674c8255493454e11184e81c2/README.md" >}} )

########## bbe2d55264b6a48cd5bde933bc225c0cb963222b
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/bbe2d55264b6a48cd5bde933bc225c0cb963222b/README.md" >}} )

########## bc9ae6c6b21940633329c854231a4fcad787cc4a
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/bc9ae6c6b21940633329c854231a4fcad787cc4a/README.md" >}} )

########## bcacaa69244be43841f1948f842a53817134987b
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/bcacaa69244be43841f1948f842a53817134987b/README.md" >}} )

########## bdd6bd79870f0b8dfa8366a3cb7c32f73168918e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/bdd6bd79870f0b8dfa8366a3cb7c32f73168918e/README.md" >}} )

########## bea728043c7ba79c5a94ac3360a12b9b4fb21d97
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/bea728043c7ba79c5a94ac3360a12b9b4fb21d97/README.md" >}} )

########## bf81c0bf00046ee828ca5584324cf813bc044255
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/bf81c0bf00046ee828ca5584324cf813bc044255/README.md" >}} )

########## c07a4c5582ae590e2a85ac1d1531e56d405869b2
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/c07a4c5582ae590e2a85ac1d1531e56d405869b2/README.md" >}} )

########## c12902ffc81ca0b9e795e401452b6d8b1d1cb99c
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/c12902ffc81ca0b9e795e401452b6d8b1d1cb99c/README.md" >}} )

########## c2ec4de62e2b8664a5061aaec4038768b797b489
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/c2ec4de62e2b8664a5061aaec4038768b797b489/README.md" >}} )

########## c44ea97752ae23a2c4ab6c4c7cf7ba9bb59cc559
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/c44ea97752ae23a2c4ab6c4c7cf7ba9bb59cc559/README.md" >}} )

########## c4f2b9ab93f4fad6deecdbb739c2b7e768e4d689
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/c4f2b9ab93f4fad6deecdbb739c2b7e768e4d689/README.md" >}} )

########## c5e7d0ed1075ca48e09b58e8d9a34e1317834260
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/c5e7d0ed1075ca48e09b58e8d9a34e1317834260/README.md" >}} )

########## c5f50697102b21ee2ef6bae298825cdebe23852e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/c5f50697102b21ee2ef6bae298825cdebe23852e/README.md" >}} )

########## c659aedbfbfe8f15c25e12f7e1ebd29fc3735d61
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/c659aedbfbfe8f15c25e12f7e1ebd29fc3735d61/README.md" >}} )

########## c67d49ffa3e0f878f903a65e2a9d5fcecac44167
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/c67d49ffa3e0f878f903a65e2a9d5fcecac44167/README.md" >}} )

########## c725fd5875ff22f04463040ae132c68853ca86b1
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/c725fd5875ff22f04463040ae132c68853ca86b1/README.md" >}} )

########## c786e76273e2d2fb78f184da7453918ab0ef5d37
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/c786e76273e2d2fb78f184da7453918ab0ef5d37/README.md" >}} )

########## c7a001c72da06c21610466f66996f699e3401efe
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/c7a001c72da06c21610466f66996f699e3401efe/README.md" >}} )

########## c7bb86fde4537e4229df05bd6d34eb90fefae0e5
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/c7bb86fde4537e4229df05bd6d34eb90fefae0e5/README.md" >}} )

########## c8426830cea9b04b6292ef62c63192509c8cf6c7
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/c8426830cea9b04b6292ef62c63192509c8cf6c7/README.md" >}} )

########## ca3d6a197663d9321c4af180445a3832fdd9cb0d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ca3d6a197663d9321c4af180445a3832fdd9cb0d/README.md" >}} )

########## cb8bcf4a36bfa87414b28fb5a57f5caa9a38860b
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/cb8bcf4a36bfa87414b28fb5a57f5caa9a38860b/README.md" >}} )

########## cc73000f4b1270ddd8e7c21a07f078a61e746b54
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/cc73000f4b1270ddd8e7c21a07f078a61e746b54/README.md" >}} )

########## ccfe458b40f814bbe4c327f08cddd7fd910be35b
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ccfe458b40f814bbe4c327f08cddd7fd910be35b/README.md" >}} )

########## cd1cf92ab4fb9b56c9d7c9908e6df6f3ffd5af06
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/cd1cf92ab4fb9b56c9d7c9908e6df6f3ffd5af06/README.md" >}} )

########## ce1638ffc2d7e729a837b516b1a570fe911e5d5f
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ce1638ffc2d7e729a837b516b1a570fe911e5d5f/README.md" >}} )

########## ce4d62ca1c5a086c8f109d388ca79059e330539e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ce4d62ca1c5a086c8f109d388ca79059e330539e/README.md" >}} )

########## cecdfc719d8a29695d160fd2282427f7f3c83c1e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/cecdfc719d8a29695d160fd2282427f7f3c83c1e/README.md" >}} )

########## cf023d9c40b8c06ca506c980c6973adb882b4e59
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/cf023d9c40b8c06ca506c980c6973adb882b4e59/README.md" >}} )

########## d048b1dc2fe1c0ca2be95caed63fa4e20bdc73f8
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/d048b1dc2fe1c0ca2be95caed63fa4e20bdc73f8/README.md" >}} )

########## d0a6d172c4a3cc49c4b4d209440b72f7d360821e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/d0a6d172c4a3cc49c4b4d209440b72f7d360821e/README.md" >}} )

########## d0c3df23018ef3371ebd523dded6dac92d99288e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/d0c3df23018ef3371ebd523dded6dac92d99288e/README.md" >}} )

########## d102243d476863003f4d9961b95f61ec9dbaecd8
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/d102243d476863003f4d9961b95f61ec9dbaecd8/README.md" >}} )

########## d2e9ceb01e91fe69fecc09c29bfacd48c1baab19
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/d2e9ceb01e91fe69fecc09c29bfacd48c1baab19/README.md" >}} )

########## d41a066b2dfc3a2177b67d93c7cc70da2711f91a
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/d41a066b2dfc3a2177b67d93c7cc70da2711f91a/README.md" >}} )

########## d579e8b5b29ce29de680b49b906c0b3f284683b3
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/d579e8b5b29ce29de680b49b906c0b3f284683b3/README.md" >}} )

########## d5f651e960a39899bba6ff9049e64a1a7c942d67
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/d5f651e960a39899bba6ff9049e64a1a7c942d67/README.md" >}} )

########## d6243dfcb2fa8c8c503d188b6c665ccca4c5238e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/d6243dfcb2fa8c8c503d188b6c665ccca4c5238e/README.md" >}} )

########## d6347c06e87ab833593f24d3490b58e442752487
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/d6347c06e87ab833593f24d3490b58e442752487/README.md" >}} )

########## d8d837519dff6e6cb14c0c06155fca21e13ea5b9
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/d8d837519dff6e6cb14c0c06155fca21e13ea5b9/README.md" >}} )

########## d9119a7aaeefaf6cae8f63e5ba3fc407714d2d9b
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/d9119a7aaeefaf6cae8f63e5ba3fc407714d2d9b/README.md" >}} )

########## d92378b6c6aac59dcea5f04fad86ff64f57cf7d2
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/d92378b6c6aac59dcea5f04fad86ff64f57cf7d2/README.md" >}} )

########## da78ae007473eef666443f27c8eaf087fdadf2d7
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/da78ae007473eef666443f27c8eaf087fdadf2d7/README.md" >}} )

########## daa7ae05eecbdfea8a8bcdcff5e7afa930747822
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/daa7ae05eecbdfea8a8bcdcff5e7afa930747822/README.md" >}} )

########## dabb63ba120749ef1afa3275303c9e415487ef53
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/dabb63ba120749ef1afa3275303c9e415487ef53/README.md" >}} )

########## dac97137f9547ca609c8167c1eea916cd3726f5a
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/dac97137f9547ca609c8167c1eea916cd3726f5a/README.md" >}} )

########## db9c1b997e197772a5ddb73ce15d52cb85a0076e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/db9c1b997e197772a5ddb73ce15d52cb85a0076e/README.md" >}} )

########## dbd9af373f346d045f57cb1dba87b2cd3c014527
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/dbd9af373f346d045f57cb1dba87b2cd3c014527/README.md" >}} )

########## dce1eaa2a6542be401b9be5fdcb76b2b53419379
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/dce1eaa2a6542be401b9be5fdcb76b2b53419379/README.md" >}} )

########## dd86d4c87cd36f3f5f76e61ba4fd781f3a72a641
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/dd86d4c87cd36f3f5f76e61ba4fd781f3a72a641/README.md" >}} )

########## de640632c65d8d25ee86bd2e35ddc1b5b8783335
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/de640632c65d8d25ee86bd2e35ddc1b5b8783335/README.md" >}} )

########## ded39da0ee6ccaef66a37739a2398a5aad1bc15e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ded39da0ee6ccaef66a37739a2398a5aad1bc15e/README.md" >}} )

########## dfc4605d2823bb06d2a12fc71b47a7121dc5506e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/dfc4605d2823bb06d2a12fc71b47a7121dc5506e/README.md" >}} )

########## e0b651a529aae104064c248c00c49b30bc5b2cff
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/e0b651a529aae104064c248c00c49b30bc5b2cff/README.md" >}} )

########## e1d12e2766860982a56b8a9283acc04432bd051a
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/e1d12e2766860982a56b8a9283acc04432bd051a/README.md" >}} )

########## e2dafbaad12b3b949e8e8d1039ec115f95f22d4d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/e2dafbaad12b3b949e8e8d1039ec115f95f22d4d/README.md" >}} )

########## e3e8ca8366e2ec9a5aa5542cb4cb3d538ad94368
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/e3e8ca8366e2ec9a5aa5542cb4cb3d538ad94368/README.md" >}} )

########## e421238bbb51c0a6bd0f7c87ee27835c46377c63
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/e421238bbb51c0a6bd0f7c87ee27835c46377c63/README.md" >}} )

########## e4a11ec5a27cec2b111dea18e98f68d0f76bb17d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/e4a11ec5a27cec2b111dea18e98f68d0f76bb17d/README.md" >}} )

########## e50a21a95354dc46c094cf8172a4350b74b49cb5
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/e50a21a95354dc46c094cf8172a4350b74b49cb5/README.md" >}} )

########## e5d87dde158c4f8a17f76dbfe7ba8985291be18d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/e5d87dde158c4f8a17f76dbfe7ba8985291be18d/README.md" >}} )

########## e68210162f6b28344999f30670e5c25e3f279c6f
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/e68210162f6b28344999f30670e5c25e3f279c6f/README.md" >}} )

########## e7fd98363c30f615107d3d3e674194461ae9d794
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/e7fd98363c30f615107d3d3e674194461ae9d794/README.md" >}} )

########## e924f2151b53fc64088c4bf8a201bf7f169ca0c2
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/e924f2151b53fc64088c4bf8a201bf7f169ca0c2/README.md" >}} )

########## ea2da5c4855d01414ab4f3a978da408b274b25d9
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ea2da5c4855d01414ab4f3a978da408b274b25d9/README.md" >}} )

########## ec337ec7712903f9fb1c047ee2d26d19c9a98a43
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ec337ec7712903f9fb1c047ee2d26d19c9a98a43/README.md" >}} )

########## ed27b5bf31a4e8b24f9a8e56ca74688826307953
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ed27b5bf31a4e8b24f9a8e56ca74688826307953/README.md" >}} )

########## ed3d7a339f7d49bb82b0039aedc7c332d64fd8ef
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ed3d7a339f7d49bb82b0039aedc7c332d64fd8ef/README.md" >}} )

########## ed68e1a8829cc7b1cbb8bd6cf0122c5ad22839cb
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ed68e1a8829cc7b1cbb8bd6cf0122c5ad22839cb/README.md" >}} )

########## ed8733270c146936c8098c51cbe0f31d2ff99031
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ed8733270c146936c8098c51cbe0f31d2ff99031/README.md" >}} )

########## ee6a02fdafe52a1b091fa3d6482462a4e847ac6a
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ee6a02fdafe52a1b091fa3d6482462a4e847ac6a/README.md" >}} )

########## efb4ef1451a10f9361446e0545122a46d68c5e72
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/efb4ef1451a10f9361446e0545122a46d68c5e72/README.md" >}} )

########## f065abb6d137b1b4caf11072c4309811e8086a91
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/f065abb6d137b1b4caf11072c4309811e8086a91/README.md" >}} )

########## f08da2f9f09565b0633f8d017db9484a73b82190
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/f08da2f9f09565b0633f8d017db9484a73b82190/README.md" >}} )

########## f1990dd5b604d283ba9477fa6f6c5e8d92c2ef47
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/f1990dd5b604d283ba9477fa6f6c5e8d92c2ef47/README.md" >}} )

########## f2b4984e45378318ea5d80104a310573369bc92d
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/f2b4984e45378318ea5d80104a310573369bc92d/README.md" >}} )

########## f429f242ec18ffab856f54cbeaf35e3d105a8e17
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/f429f242ec18ffab856f54cbeaf35e3d105a8e17/README.md" >}} )

########## f48d2f97abfdb2ecd997485986a07f427052460a
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/f48d2f97abfdb2ecd997485986a07f427052460a/README.md" >}} )

########## f512413f34b9e579ef0bc81781a78549174ed10e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/f512413f34b9e579ef0bc81781a78549174ed10e/README.md" >}} )

########## f60b4e489c9f27f1ce601202d614b80a6c0d35e2
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/f60b4e489c9f27f1ce601202d614b80a6c0d35e2/README.md" >}} )

########## f685941364df99c7932df914b86e756f92db4b9e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/f685941364df99c7932df914b86e756f92db4b9e/README.md" >}} )

########## f82658463b3e8d149b74a33102e110bb10cf79f5
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/f82658463b3e8d149b74a33102e110bb10cf79f5/README.md" >}} )

########## f90926595cc2eef179a685eac382432ef1b2f19a
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/f90926595cc2eef179a685eac382432ef1b2f19a/README.md" >}} )

########## faf21b399723e908a7b69ec625dde60394bc4a90
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/faf21b399723e908a7b69ec625dde60394bc4a90/README.md" >}} )

########## fc913e4787bb4a34dcf088735f2cbd5bae3ea34e
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/fc913e4787bb4a34dcf088735f2cbd5bae3ea34e/README.md" >}} )

########## fcc3dd33f46ef884e0a1dfe4f2af36b55d9ab9b6
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/fcc3dd33f46ef884e0a1dfe4f2af36b55d9ab9b6/README.md" >}} )

########## fcfde7e7a55bfde00c6bd03557c3338ed3f40cf5
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/fcfde7e7a55bfde00c6bd03557c3338ed3f40cf5/README.md" >}} )

########## fd46136192cd05c6bb3dc80b19f2c824bde2e136
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/fd46136192cd05c6bb3dc80b19f2c824bde2e136/README.md" >}} )

########## fdabb4ca65cac67a955d36e405b7597c02c20f68
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/fdabb4ca65cac67a955d36e405b7597c02c20f68/README.md" >}} )

########## feb928e8d14dc2a5c81abc5896b1c807ceb527fc
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/feb928e8d14dc2a5c81abc5896b1c807ceb527fc/README.md" >}} )

########## ff377ae11f8348350d79da46521414aa736ed98a
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/ff377ae11f8348350d79da46521414aa736ed98a/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/mentor-api-prod__splitclassvalidation/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/data/report/README.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/README.md" >}} )

###### job-ad-conll-parser
####### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/job-ad-conll-parser/doc/intro.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/job-ad-conll-parser/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/job-ad-conll-parser/README.md" >}} )

###### job-ad-triplet-generator
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/job-ad-triplet-generator/README.md" >}} )

###### jobads-for-sbert
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jobads-for-sbert/README.md" >}} )

###### jobtech-nlp-evaluation
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jobtech-nlp-evaluation/README.md" >}} )

###### jobtech-nlp-word-embeddings
####### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jobtech-nlp-word-embeddings/doc/intro.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jobtech-nlp-word-embeddings/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jobtech-nlp-word-embeddings/README.md" >}} )

[how_to_run_neanderthal_in_amazon_ec2]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/jobtech-nlp-word-embeddings/how_to_run_neanderthal_in_amazon_ec2.md" >}} )

###### mentor-api
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/mentor-api/README.md" >}} )

###### minimal-dd
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/minimal-dd/README.md" >}} )

###### nlp-mock-gui
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/nlp-mock-gui/README.md" >}} )

###### semantic-concept-search
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/semantic-concept-search/README.md" >}} )

###### simple-tokenizer
####### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/simple-tokenizer/doc/intro.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/simple-tokenizer/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/simple-tokenizer/README.md" >}} )

###### skills-for-aub
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/skills-for-aub/README.md" >}} )

###### sun-concepts
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/sun-concepts/README.md" >}} )

###### synonym-dictionary-to-taxonomy-skills
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/synonym-dictionary-to-taxonomy-skills/README.md" >}} )

###### taxonomy-data-collection-server
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-data-collection-server/README.md" >}} )

###### taxonomy-mapping-tools
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-mapping-tools/README.md" >}} )

###### taxonomy-transformers-clj-py-ner
####### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-transformers-clj-py-ner/doc/intro.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-transformers-clj-py-ner/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-transformers-clj-py-ner/README.md" >}} )

[notes]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-transformers-clj-py-ner/notes.md" >}} )

[start_clojure_repl]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-transformers-clj-py-ner/start_clojure_repl.md" >}} )

###### taxonomy-vec
####### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-vec/doc/intro.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-vec/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-vec/README.md" >}} )

###### text2ssyk-bert
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/text2ssyk-bert/README.md" >}} )

###### word2vec
####### data
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/word2vec/data/README.md" >}} )

####### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/word2vec/doc/intro.md" >}} )

####### resources
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/word2vec/resources/README.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/word2vec/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/nlp/word2vec/README.md" >}} )

##### openshift-varnish
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/openshift-varnish/README.md" >}} )

##### taxonomy-autocomplete
###### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/taxonomy-autocomplete/doc/intro.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/taxonomy-autocomplete/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/taxonomy-autocomplete/README.md" >}} )

##### taxonomy-database-dist
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/taxonomy-database-dist/README.md" >}} )

##### taxonomy-static-builder
###### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/taxonomy-static-builder/doc/intro.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/taxonomy-static-builder/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/taxonomy-static-builder/README.md" >}} )

##### taxonomy-version-db-tool
###### doc
[intro]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/taxonomy-version-db-tool/doc/intro.md" >}} )

[CHANGELOG]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/taxonomy-version-db-tool/CHANGELOG.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/backend/taxonomy-version-db-tool/README.md" >}} )

#### documentation
##### Ads
[clojuredev2022]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/Ads/clojuredev2022.md" >}} )

##### Clojure
[math]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/Clojure/math.md" >}} )

##### LifelongLearning
[Seminars]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/LifelongLearning/Seminars.md" >}} )

[ideas]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/LifelongLearning/ideas.md" >}} )

##### LongTermPlanning
[2021-02-04]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/LongTermPlanning/2021-02-04.md" >}} )

##### OpenShift
[ConnectTo_nREPL]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OpenShift/ConnectTo_nREPL.md" >}} )

[DebugPods]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OpenShift/DebugPods.md" >}} )

##### OtherMeetings
###### 20220902-taxonomy-presentation-outline
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20220902-taxonomy-presentation-outline/README.md" >}} )

###### 20230124-djs-discussion
[notes]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20230124-djs-discussion/notes.md" >}} )

[20220322-taxonomy-mapping-tools]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20220322-taxonomy-mapping-tools.md" >}} )

[20220323-konvertera-annonser]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20220323-konvertera-annonser.md" >}} )

[20220412-myhkval]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20220412-myhkval.md" >}} )

[20220429-skosdiscussion]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20220429-skosdiscussion.md" >}} )

[20220504-siggesta-external]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20220504-siggesta-external.md" >}} )

[20220504-siggesta-purpose]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20220504-siggesta-purpose.md" >}} )

[20220622-taxonomy-annotation-discussion]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20220622-taxonomy-annotation-discussion.md" >}} )

[20220927-onboarding-discussion]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20220927-onboarding-discussion.md" >}} )

[20221021-text2ssyk]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20221021-text2ssyk.md" >}} )

[20221110-revised-team-agreement]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20221110-revised-team-agreement.md" >}} )

[20230118-diskutera-aub]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/20230118-diskutera-aub.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/README.md" >}} )

[susa_workshop]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OtherMeetings/susa_workshop.md" >}} )

##### Reports
[clojured2022]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/Reports/clojured2022.md" >}} )

##### TaxonomiInförandet
[meeting_2020-03-26]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/TaxonomiInförandet/meeting_2020-03-26.md" >}} )

##### Tutorials
[clojure]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/Tutorials/clojure.md" >}} )

[torch_with_gpu]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/Tutorials/torch_with_gpu.md" >}} )

##### presentations
###### 20220916-taxonomy-dev-part2
####### concept_recognition
[pres]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/presentations/20220916-taxonomy-dev-part2/concept_recognition/pres.md" >}} )

###### 20221220-conrec-utils-walkthrough
[code]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/presentations/20221220-conrec-utils-walkthrough/code.md" >}} )

[talk]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/presentations/20221220-conrec-utils-walkthrough/talk.md" >}} )

###### 20230301-taxonomy-mapping-tools-walkthrough
[manus]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/presentations/20230301-taxonomy-mapping-tools-walkthrough/manus.md" >}} )

##### projects
###### nlp
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/projects/nlp/README.md" >}} )

[literature]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/projects/nlp/literature.md" >}} )

[logs]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/projects/nlp/logs.md" >}} )

[concept-recognition]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/projects/concept-recognition.md" >}} )

[datahike]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/projects/datahike.md" >}} )

[djs]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/projects/djs.md" >}} )

[libs]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/projects/libs.md" >}} )

[text2ssyk]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/projects/text2ssyk.md" >}} )

[Infrastructure]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/Infrastructure.md" >}} )

[OnBoarding]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/OnBoarding.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/README.md" >}} )

[RecurrentTasks]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/RecurrentTasks.md" >}} )

[SummerworkerOnboarding]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/documentation/SummerworkerOnboarding.md" >}} )

#### experimental
##### autocomplete-demo
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/experimental/autocomplete-demo/README.md" >}} )

##### jobtech-taxonomy-autocomplete-occupation-name
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/experimental/jobtech-taxonomy-autocomplete-occupation-name/README.md" >}} )

#### frontend
##### jobtech-annotation-editor
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/frontend/jobtech-annotation-editor/README.md" >}} )

##### jobtech-atlas-landingpage
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/frontend/jobtech-atlas-landingpage/README.md" >}} )

##### jobtech-taxonomy-atlas
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-atlas/README.md" >}} )

##### jobtech-taxonomy-editor
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-editor/README.md" >}} )

##### jobtech-taxonomy-example
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-example/README.md" >}} )

##### jobtech-taxonomy-highscore
[README]({{< ref "/filtered/mirror/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-highscore/README.md" >}} )

### uppdrag-att-utveckla-en-sammanhallen-datainfrastruktur-foer-kompetensfoersoerjning-och-livslangt-laerande
[README]({{< ref "/filtered/mirror/arbetsformedlingen/uppdrag-att-utveckla-en-sammanhallen-datainfrastruktur-foer-kompetensfoersoerjning-och-livslangt-laerande/README.md" >}} )

### wikipoc-gemensam-k-lla
#### documentation
##### documentation
[README]({{< ref "/filtered/mirror/arbetsformedlingen/wikipoc-gemensam-k-lla/documentation/documentation/README.md" >}} )

#### frontend
##### wikipoc
[README]({{< ref "/filtered/mirror/arbetsformedlingen/wikipoc-gemensam-k-lla/frontend/wikipoc/README.md" >}} )

### www
#### alljobadswidget
[CONTRIBUTING]({{< ref "/filtered/mirror/arbetsformedlingen/www/alljobadswidget/CONTRIBUTING.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/www/alljobadswidget/README.md" >}} )

[TODO]({{< ref "/filtered/mirror/arbetsformedlingen/www/alljobadswidget/TODO.md" >}} )

#### arbetsgivare-test
[README]({{< ref "/filtered/mirror/arbetsformedlingen/www/arbetsgivare-test/README.md" >}} )

#### arbetsmarknadsutbildningar-susa
[README]({{< ref "/filtered/mirror/arbetsformedlingen/www/arbetsmarknadsutbildningar-susa/README.md" >}} )

#### datainfra-kll
[License]({{< ref "/filtered/mirror/arbetsformedlingen/www/datainfra-kll/License.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/www/datainfra-kll/README.md" >}} )

#### docs
[README]({{< ref "/filtered/mirror/arbetsformedlingen/www/docs/README.md" >}} )

#### etableringsjobb-test
[README]({{< ref "/filtered/mirror/arbetsformedlingen/www/etableringsjobb-test/README.md" >}} )

#### jobtechdev-se-v2
##### docs
[theme]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/docs/theme.md" >}} )

[upgrade]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/docs/upgrade.md" >}} )

##### user
###### pages
####### 01.home
[frontpage.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/01.home/frontpage.en.md" >}} )

[frontpage]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/01.home/frontpage.md" >}} )

[frontpage.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/01.home/frontpage.sv.md" >}} )

####### 02.components
######## 01.jobsearch
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/01.jobsearch/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/01.jobsearch/component_page.sv.md" >}} )

######## 02.ekosystem_foer_annonser
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/02.ekosystem_foer_annonser/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/02.ekosystem_foer_annonser/component_page.sv.md" >}} )

######## 03.jobtech-taxonomy
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/03.jobtech-taxonomy/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/03.jobtech-taxonomy/component_page.sv.md" >}} )

######## 04.jobstream
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/04.jobstream/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/04.jobstream/component_page.sv.md" >}} )

######## 05.historical-ads
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/05.historical-ads/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/05.historical-ads/component_page.sv.md" >}} )

######## 06.yrkesprognoser
######### 01.metodbeskrivning-yrkesprognoser
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/06.yrkesprognoser/01.metodbeskrivning-yrkesprognoser/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/06.yrkesprognoser/01.metodbeskrivning-yrkesprognoser/component_page.sv.md" >}} )

[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/06.yrkesprognoser/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/06.yrkesprognoser/component_page.sv.md" >}} )

######## 07.connect-once
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/07.connect-once/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/07.connect-once/component_page.sv.md" >}} )

######## 08.jobtech-atlas
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/08.jobtech-atlas/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/08.jobtech-atlas/component_page.sv.md" >}} )

######## 09.etik-och-digital-matchning
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/09.etik-och-digital-matchning/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/09.etik-och-digital-matchning/component_page.sv.md" >}} )

######## 10.alljobads
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/10.alljobads/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/10.alljobads/component_page.sv.md" >}} )

######## 11.giglab-sverige
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/11.giglab-sverige/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/11.giglab-sverige/component_page.sv.md" >}} )

######## 12.open-plattforms
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/12.open-plattforms/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/12.open-plattforms/component_page.sv.md" >}} )

######## 13.jobad-enrichments
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/13.jobad-enrichments/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/13.jobad-enrichments/component_page.sv.md" >}} )

######## 14.digital-vaegledning-och-laenkad-data
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/14.digital-vaegledning-och-laenkad-data/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/14.digital-vaegledning-och-laenkad-data/component_page.sv.md" >}} )

######## 17.jobad-links
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/17.jobad-links/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/17.jobad-links/component_page.sv.md" >}} )

######## 18.digital-skills
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/18.digital-skills/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/18.digital-skills/component_page.sv.md" >}} )

######## 19.jobsearch-trends
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/19.jobsearch-trends/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/19.jobsearch-trends/component_page.sv.md" >}} )

######## 21.kompetensmatchning
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/21.kompetensmatchning/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/21.kompetensmatchning/component_page.sv.md" >}} )

######## 22.jobed-connect
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/22.jobed-connect/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/22.jobed-connect/component_page.sv.md" >}} )

[components_list.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/components_list.en.md" >}} )

[components_list.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/02.components/components_list.sv.md" >}} )

####### 03.news
######## 01.api-days-helsinki-and-north
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/01.api-days-helsinki-and-north/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/01.api-days-helsinki-and-north/events_blog_post.sv.md" >}} )

######## 01.tech-awards-sweden-2023
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/01.tech-awards-sweden-2023/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/01.tech-awards-sweden-2023/events_blog_post.sv.md" >}} )

######## 02.ai-loesning-som-laeser-tolkar-och-analyserar-cv-n
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/02.ai-loesning-som-laeser-tolkar-och-analyserar-cv-n/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/02.ai-loesning-som-laeser-tolkar-och-analyserar-cv-n/news_blog_post.sv.md" >}} )

######## 02.jobnet-vill-bli-en-sjaelvklar-destination-foer-jobbsoekare
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/02.jobnet-vill-bli-en-sjaelvklar-destination-foer-jobbsoekare/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/02.jobnet-vill-bli-en-sjaelvklar-destination-foer-jobbsoekare/news_blog_post.sv.md" >}} )

######## 03.pharus-tech-loeser-problemet-pa-arbetsmarknaden
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/03.pharus-tech-loeser-problemet-pa-arbetsmarknaden/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/03.pharus-tech-loeser-problemet-pa-arbetsmarknaden/news_blog_post.sv.md" >}} )

######## 03.sweden-innovation-days
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/03.sweden-innovation-days/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/03.sweden-innovation-days/events_blog_post.sv.md" >}} )

######## 04.digitalisering-and-transformation-i-offentlig-sektor-2023
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/04.digitalisering-and-transformation-i-offentlig-sektor-2023/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/04.digitalisering-and-transformation-i-offentlig-sektor-2023/events_blog_post.sv.md" >}} )

######## 04.malet-aer-en-datadriven-arbetsmarknad-foer-sverige
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/04.malet-aer-en-datadriven-arbetsmarknad-foer-sverige/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/04.malet-aer-en-datadriven-arbetsmarknad-foer-sverige/news_blog_post.sv.md" >}} )

######## 05.foss-north-2023
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/05.foss-north-2023/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/05.foss-north-2023/events_blog_post.sv.md" >}} )

######## 05.kompetensmatchning-se-en-plattform-foer-kompetensutveckling
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/05.kompetensmatchning-se-en-plattform-foer-kompetensutveckling/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/05.kompetensmatchning-se-en-plattform-foer-kompetensutveckling/news_blog_post.sv.md" >}} )

######## 06.loesningar-foer-offentig-sektor
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/06.loesningar-foer-offentig-sektor/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/06.loesningar-foer-offentig-sektor/events_blog_post.sv.md" >}} )

######## 06.skills-meetup-sweden
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/06.skills-meetup-sweden/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/06.skills-meetup-sweden/events_blog_post.sv.md" >}} )

######## 07.inrupt-solid-hackathon
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/07.inrupt-solid-hackathon/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/07.inrupt-solid-hackathon/events_blog_post.sv.md" >}} )

######## 07.semic2022-interoperabilitet
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/07.semic2022-interoperabilitet/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/07.semic2022-interoperabilitet/news_blog_post.sv.md" >}} )

######## 08.open-source-driving-the-european-digital-decade-april-18
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/08.open-source-driving-the-european-digital-decade-april-18/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/08.open-source-driving-the-european-digital-decade-april-18/events_blog_post.sv.md" >}} )

######## 08.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/08.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/08.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er/news_blog_post.sv.md" >}} )

######## 09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022/events_blog_post.sv.md" >}} )

######## 09.tech-awards-sweden-2023
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/09.tech-awards-sweden-2023/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/09.tech-awards-sweden-2023/events_blog_post.sv.md" >}} )

######## 10.ai-loesning-som-laeser-tolkar-och-analyserar-cv-n
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/10.ai-loesning-som-laeser-tolkar-och-analyserar-cv-n/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/10.ai-loesning-som-laeser-tolkar-och-analyserar-cv-n/news_blog_post.sv.md" >}} )

######## 10.api-et-jobstream-aer-nominerat-till-sweden-api-award
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/10.api-et-jobstream-aer-nominerat-till-sweden-api-award/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/10.api-et-jobstream-aer-nominerat-till-sweden-api-award/news_blog_post.sv.md" >}} )

######## 11.jobtech-development-deltar-pa-loesningar-foer-offentlig-sektor
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/11.jobtech-development-deltar-pa-loesningar-foer-offentlig-sektor/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/11.jobtech-development-deltar-pa-loesningar-foer-offentlig-sektor/events_blog_post.sv.md" >}} )

######## 11.sweden-innovation-days
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/11.sweden-innovation-days/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/11.sweden-innovation-days/events_blog_post.sv.md" >}} )

######## 12.malet-aer-en-datadriven-arbetsmarknad-foer-sverige
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/12.malet-aer-en-datadriven-arbetsmarknad-foer-sverige/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/12.malet-aer-en-datadriven-arbetsmarknad-foer-sverige/news_blog_post.sv.md" >}} )

######## 12.vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/12.vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/12.vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data/news_blog_post.sv.md" >}} )

######## 13.kompetensmatchning-se-en-plattform-foer-kompetensutveckling
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/13.kompetensmatchning-se-en-plattform-foer-kompetensutveckling/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/13.kompetensmatchning-se-en-plattform-foer-kompetensutveckling/news_blog_post.sv.md" >}} )

######## 13.summering-av-efoervaltningsdagarna
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/13.summering-av-efoervaltningsdagarna/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/13.summering-av-efoervaltningsdagarna/news_blog_post.sv.md" >}} )

######## 14.loesningar-foer-offentig-sektor
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/14.loesningar-foer-offentig-sektor/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/14.loesningar-foer-offentig-sektor/events_blog_post.sv.md" >}} )

######## 14.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/14.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/14.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.sv.md" >}} )

######## 15.jobagent
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/15.jobagent/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/15.jobagent/news_blog_post.sv.md" >}} )

######## 15.semic2022-interoperabilitet
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/15.semic2022-interoperabilitet/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/15.semic2022-interoperabilitet/news_blog_post.sv.md" >}} )

######## 16.intervju_mattias
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/16.intervju_mattias/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/16.intervju_mattias/news_blog_post.sv.md" >}} )

######## 16.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/16.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/16.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er/news_blog_post.sv.md" >}} )

######## 17.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/17.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/17.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022/events_blog_post.sv.md" >}} )

######## 17.jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/17.jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/17.jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av/news_blog_post.sv.md" >}} )

######## 18.api-et-jobstream-aer-nominerat-till-sweden-api-award
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/18.api-et-jobstream-aer-nominerat-till-sweden-api-award/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/18.api-et-jobstream-aer-nominerat-till-sweden-api-award/news_blog_post.sv.md" >}} )

######## 18.vill-du-veta-mer-om-sveriges-baesta-api
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/18.vill-du-veta-mer-om-sveriges-baesta-api/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/18.vill-du-veta-mer-om-sveriges-baesta-api/events_blog_post.sv.md" >}} )

######## 19.framtid
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/19.framtid/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/19.framtid/news_blog_post.sv.md" >}} )

######## 19.jobtech-development-deltar-pa-loesningar-foer-offentlig-sektor
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/19.jobtech-development-deltar-pa-loesningar-foer-offentlig-sektor/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/19.jobtech-development-deltar-pa-loesningar-foer-offentlig-sektor/events_blog_post.sv.md" >}} )

######## 20.ingen-kan-loesa-problemet-pa-egen-hand-daerfoer-goer-vi-det-tillsammans
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/20.ingen-kan-loesa-problemet-pa-egen-hand-daerfoer-goer-vi-det-tillsammans/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/20.ingen-kan-loesa-problemet-pa-egen-hand-daerfoer-goer-vi-det-tillsammans/news_blog_post.sv.md" >}} )

######## 20.vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/20.vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/20.vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data/news_blog_post.sv.md" >}} )

######## 21.summering-av-efoervaltningsdagarna
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/21.summering-av-efoervaltningsdagarna/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/21.summering-av-efoervaltningsdagarna/news_blog_post.sv.md" >}} )

######## 21.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/21.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/21.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus/news_blog_post.sv.md" >}} )

######## 22.layke-analytics
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/22.layke-analytics/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/22.layke-analytics/news_blog_post.sv.md" >}} )

######## 22.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/22.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/22.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.sv.md" >}} )

######## 23.jobagent
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/23.jobagent/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/23.jobagent/news_blog_post.sv.md" >}} )

######## 23.ny-forskningsrapport-om-oeppna-data-ekosystem
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/23.ny-forskningsrapport-om-oeppna-data-ekosystem/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/23.ny-forskningsrapport-om-oeppna-data-ekosystem/news_blog_post.sv.md" >}} )

######## 24.intervju_mattias
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/24.intervju_mattias/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/24.intervju_mattias/news_blog_post.sv.md" >}} )

######## 24.tre-pilotprojekt
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/24.tre-pilotprojekt/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/24.tre-pilotprojekt/news_blog_post.sv.md" >}} )

######## 25.jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/25.jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/25.jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av/news_blog_post.sv.md" >}} )

######## 25.vi-slutar-anvaenda-api-nycklar
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/25.vi-slutar-anvaenda-api-nycklar/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/25.vi-slutar-anvaenda-api-nycklar/news_blog_post.sv.md" >}} )

######## 26.sammanfattning
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/26.sammanfattning/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/26.sammanfattning/news_blog_post.sv.md" >}} )

######## 26.vill-du-veta-mer-om-sveriges-baesta-api
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/26.vill-du-veta-mer-om-sveriges-baesta-api/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/26.vill-du-veta-mer-om-sveriges-baesta-api/events_blog_post.sv.md" >}} )

######## 27.eu-s-ordfoerandeskapstrio
######### det-franska-eu-ordfoerandeskapets-program
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/27.eu-s-ordfoerandeskapstrio/det-franska-eu-ordfoerandeskapets-program/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/27.eu-s-ordfoerandeskapstrio/det-franska-eu-ordfoerandeskapets-program/news_blog_post.sv.md" >}} )

######### eu-ordfoerandeskapstrions-program
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/27.eu-s-ordfoerandeskapstrio/eu-ordfoerandeskapstrions-program/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/27.eu-s-ordfoerandeskapstrio/eu-ordfoerandeskapstrions-program/news_blog_post.sv.md" >}} )

[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/27.eu-s-ordfoerandeskapstrio/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/27.eu-s-ordfoerandeskapstrio/news_blog_post.sv.md" >}} )

######## 27.framtid
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/27.framtid/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/27.framtid/news_blog_post.sv.md" >}} )

######## 28.ingen-kan-loesa-problemet-pa-egen-hand-daerfoer-goer-vi-det-tillsammans
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/28.ingen-kan-loesa-problemet-pa-egen-hand-daerfoer-goer-vi-det-tillsammans/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/28.ingen-kan-loesa-problemet-pa-egen-hand-daerfoer-goer-vi-det-tillsammans/news_blog_post.sv.md" >}} )

######## 28.summering-loesningar-foer-offentlig-sektor
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/28.summering-loesningar-foer-offentlig-sektor/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/28.summering-loesningar-foer-offentlig-sektor/news_blog_post.sv.md" >}} )

######## 29.open-space-3-en-sammanfattning
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/29.open-space-3-en-sammanfattning/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/29.open-space-3-en-sammanfattning/news_blog_post.sv.md" >}} )

######## 29.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/29.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/29.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus/news_blog_post.sv.md" >}} )

######## 30.cybersecurity
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/30.cybersecurity/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/30.cybersecurity/news_blog_post.sv.md" >}} )

######## 30.layke-analytics
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/30.layke-analytics/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/30.layke-analytics/news_blog_post.sv.md" >}} )

######## 31.ny-forskningsrapport-om-oeppna-data-ekosystem
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/31.ny-forskningsrapport-om-oeppna-data-ekosystem/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/31.ny-forskningsrapport-om-oeppna-data-ekosystem/news_blog_post.sv.md" >}} )

######## 31.viktig-info-om-jobsearch
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/31.viktig-info-om-jobsearch/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/31.viktig-info-om-jobsearch/news_blog_post.sv.md" >}} )

######## 32.jobsearchtrends
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/32.jobsearchtrends/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/32.jobsearchtrends/news_blog_post.sv.md" >}} )

######## 32.tre-pilotprojekt
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/32.tre-pilotprojekt/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/32.tre-pilotprojekt/news_blog_post.sv.md" >}} )

######## 33.digital-infrastruktur
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/33.digital-infrastruktur/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/33.digital-infrastruktur/news_blog_post.sv.md" >}} )

######## 33.vi-slutar-anvaenda-api-nycklar
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/33.vi-slutar-anvaenda-api-nycklar/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/33.vi-slutar-anvaenda-api-nycklar/news_blog_post.sv.md" >}} )

######## 34.sammanfattning
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/34.sammanfattning/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/34.sammanfattning/news_blog_post.sv.md" >}} )

######## 34.tre-experter-digital-infrastruktur
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/34.tre-experter-digital-infrastruktur/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/34.tre-experter-digital-infrastruktur/news_blog_post.sv.md" >}} )

######## 35.eu-s-ordfoerandeskapstrio
######### det-franska-eu-ordfoerandeskapets-program
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/35.eu-s-ordfoerandeskapstrio/det-franska-eu-ordfoerandeskapets-program/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/35.eu-s-ordfoerandeskapstrio/det-franska-eu-ordfoerandeskapets-program/news_blog_post.sv.md" >}} )

######### eu-ordfoerandeskapstrions-program
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/35.eu-s-ordfoerandeskapstrio/eu-ordfoerandeskapstrions-program/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/35.eu-s-ordfoerandeskapstrio/eu-ordfoerandeskapstrions-program/news_blog_post.sv.md" >}} )

[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/35.eu-s-ordfoerandeskapstrio/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/35.eu-s-ordfoerandeskapstrio/news_blog_post.sv.md" >}} )

######## 35.sveriges-eu-ordfoerandeskap
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/35.sveriges-eu-ordfoerandeskap/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/35.sveriges-eu-ordfoerandeskap/news_blog_post.sv.md" >}} )

######## 36.30-ar-sv-eu-medlemskap
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/36.30-ar-sv-eu-medlemskap/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/36.30-ar-sv-eu-medlemskap/news_blog_post.sv.md" >}} )

######## 36.summering-loesningar-foer-offentlig-sektor
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/36.summering-loesningar-foer-offentlig-sektor/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/36.summering-loesningar-foer-offentlig-sektor/news_blog_post.sv.md" >}} )

######## 37.open-space-3-en-sammanfattning
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/37.open-space-3-en-sammanfattning/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/37.open-space-3-en-sammanfattning/news_blog_post.sv.md" >}} )

######## 37.webbdagarna-stockholm-18-19-april
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/37.webbdagarna-stockholm-18-19-april/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/37.webbdagarna-stockholm-18-19-april/events_blog_post.sv.md" >}} )

######## 38.cybersecurity
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/38.cybersecurity/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/38.cybersecurity/news_blog_post.sv.md" >}} )

######## 39.viktig-info-om-jobsearch
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/39.viktig-info-om-jobsearch/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/39.viktig-info-om-jobsearch/news_blog_post.sv.md" >}} )

######## 40.jobsearchtrends
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/40.jobsearchtrends/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/40.jobsearchtrends/news_blog_post.sv.md" >}} )

######## 41.digital-infrastruktur
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/41.digital-infrastruktur/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/41.digital-infrastruktur/news_blog_post.sv.md" >}} )

######## 42.tre-experter-digital-infrastruktur
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/42.tre-experter-digital-infrastruktur/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/42.tre-experter-digital-infrastruktur/news_blog_post.sv.md" >}} )

######## 43.sveriges-eu-ordfoerandeskap
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/43.sveriges-eu-ordfoerandeskap/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/43.sveriges-eu-ordfoerandeskap/news_blog_post.sv.md" >}} )

######## 44.30-ar-sv-eu-medlemskap
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/44.30-ar-sv-eu-medlemskap/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/44.30-ar-sv-eu-medlemskap/news_blog_post.sv.md" >}} )

######## 45.webbdagarna-stockholm-18-19-april
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/45.webbdagarna-stockholm-18-19-april/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/45.webbdagarna-stockholm-18-19-april/events_blog_post.sv.md" >}} )

######## 46.jobedconnect
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/46.jobedconnect/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/46.jobedconnect/news_blog_post.sv.md" >}} )

######## 47.data-spaces-symposium-2023
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/47.data-spaces-symposium-2023/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/47.data-spaces-symposium-2023/news_blog_post.sv.md" >}} )

######## 48.digital-yrkesvaegledning
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/48.digital-yrkesvaegledning/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/48.digital-yrkesvaegledning/news_blog_post.sv.md" >}} )

######## 49.sverige-ansluter-sig-till-international-open-data-charter
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/49.sverige-ansluter-sig-till-international-open-data-charter/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/49.sverige-ansluter-sig-till-international-open-data-charter/news_blog_post.sv.md" >}} )

######## 50.jobtech-development-intar-scenen-pa-foss-north-2023
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/50.jobtech-development-intar-scenen-pa-foss-north-2023/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/50.jobtech-development-intar-scenen-pa-foss-north-2023/news_blog_post.sv.md" >}} )

######## 51.jobtechdev-pa-2023-government-transformation-summit
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/51.jobtechdev-pa-2023-government-transformation-summit/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/51.jobtechdev-pa-2023-government-transformation-summit/news_blog_post.sv.md" >}} )

######## 52.ny-teknik-stoettar-det-livslanga-laerandet
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/52.ny-teknik-stoettar-det-livslanga-laerandet/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/52.ny-teknik-stoettar-det-livslanga-laerandet/news_blog_post.sv.md" >}} )

######## 53.sa-foerenklar-allaloener-se-loenefragan-genom-transparens
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/53.sa-foerenklar-allaloener-se-loenefragan-genom-transparens/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/53.sa-foerenklar-allaloener-se-loenefragan-genom-transparens/news_blog_post.sv.md" >}} )

######## 54.bra-matchning-rapport
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/54.bra-matchning-rapport/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/54.bra-matchning-rapport/news_blog_post.sv.md" >}} )

######## Webinar
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/Webinar/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/Webinar/events_blog_post.sv.md" >}} )

######## apidagar
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/apidagar/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/apidagar/news_blog_post.sv.md" >}} )

######## data-spaces-in-an-interoperable-europe-6-dec-i-bryssel
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/data-spaces-in-an-interoperable-europe-6-dec-i-bryssel/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/data-spaces-in-an-interoperable-europe-6-dec-i-bryssel/events_blog_post.sv.md" >}} )

######## demo-week
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/demo-week/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/demo-week/events_blog_post.sv.md" >}} )

######## innovationsveckan-3-7-oktober
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/innovationsveckan-3-7-oktober/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/innovationsveckan-3-7-oktober/events_blog_post.sv.md" >}} )

######## internetdagarna-21-22-november
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/internetdagarna-21-22-november/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/internetdagarna-21-22-november/events_blog_post.sv.md" >}} )

######## internetdagarna-22-23-november
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/internetdagarna-22-23-november/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/internetdagarna-22-23-november/events_blog_post.sv.md" >}} )

######## kom-igang-och-oeppet-forum-webbinarium-27-maj
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/kom-igang-och-oeppet-forum-webbinarium-27-maj/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/kom-igang-och-oeppet-forum-webbinarium-27-maj/events_blog_post.sv.md" >}} )

######## open-space-del-3
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/open-space-del-3/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/open-space-del-3/events_blog_post.sv.md" >}} )

######## open-space-workshop-16-17-mars
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/open-space-workshop-16-17-mars/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/open-space-workshop-16-17-mars/events_blog_post.sv.md" >}} )

######## open-space-workshop-8-december1
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/open-space-workshop-8-december1/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/open-space-workshop-8-december1/events_blog_post.sv.md" >}} )

######## sverige-i-europas-dataekosystem
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/sverige-i-europas-dataekosystem/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/sverige-i-europas-dataekosystem/events_blog_post.sv.md" >}} )

[news_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/news_page.en.md" >}} )

[news_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/03.news/news_page.sv.md" >}} )

####### 04.calendar
[events_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/04.calendar/events_page.en.md" >}} )

[events_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/04.calendar/events_page.sv.md" >}} )

####### 05.work
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/05.work/news_blog_post.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/05.work/page.sv.md" >}} )

####### 06.status
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/06.status/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/06.status/page.sv.md" >}} )

####### 07.about
######## 01.availability
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/07.about/01.availability/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/07.about/01.availability/page.sv.md" >}} )

######## 02.cookies
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/07.about/02.cookies/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/07.about/02.cookies/page.sv.md" >}} )

[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/07.about/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/07.about/page.sv.md" >}} )

####### 08.about
######## 01.availability
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/08.about/01.availability/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/08.about/01.availability/page.sv.md" >}} )

######## 02.cookies
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/08.about/02.cookies/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/08.about/02.cookies/page.sv.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/08.about/component_page.sv.md" >}} )

[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/08.about/page.en.md" >}} )

####### 08.om-jobtech-development
######## 01.kom-igang
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/08.om-jobtech-development/01.kom-igang/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/08.om-jobtech-development/01.kom-igang/page.sv.md" >}} )

######## 02.kontakta-oss
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/08.om-jobtech-development/02.kontakta-oss/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/08.om-jobtech-development/02.kontakta-oss/page.sv.md" >}} )

######## 03.inspireras-av-andra
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/08.om-jobtech-development/03.inspireras-av-andra/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/08.om-jobtech-development/03.inspireras-av-andra/page.sv.md" >}} )

[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/08.om-jobtech-development/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/08.om-jobtech-development/page.sv.md" >}} )

####### 09.form
######## form
######### thankyou
########## email-sent
[formdata.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.form/form/thankyou/email-sent/formdata.en.md" >}} )

[formdata.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.form/form/thankyou/email-sent/formdata.sv.md" >}} )

[formdata.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.form/form/thankyou/formdata.en.md" >}} )

[formdata.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.form/form/thankyou/formdata.sv.md" >}} )

[formdata.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.form/form/formdata.en.md" >}} )

[formdata.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.form/form/formdata.sv.md" >}} )

[default.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.form/default.en.md" >}} )

[default.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.form/default.sv.md" >}} )

####### 09.om-jobtech-development
######## 01.kom-igang
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.om-jobtech-development/01.kom-igang/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.om-jobtech-development/01.kom-igang/page.sv.md" >}} )

######## 02.kontakta-oss
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.om-jobtech-development/02.kontakta-oss/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.om-jobtech-development/02.kontakta-oss/page.sv.md" >}} )

######## 03.inspireras-av-andra
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.om-jobtech-development/03.inspireras-av-andra/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.om-jobtech-development/03.inspireras-av-andra/page.sv.md" >}} )

[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.om-jobtech-development/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/09.om-jobtech-development/page.sv.md" >}} )

####### 10.form
######## form
######### thankyou
########## email-sent
[formdata.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/10.form/form/thankyou/email-sent/formdata.en.md" >}} )

[formdata.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/10.form/form/thankyou/email-sent/formdata.sv.md" >}} )

[formdata.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/10.form/form/thankyou/formdata.en.md" >}} )

[formdata.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/10.form/form/thankyou/formdata.sv.md" >}} )

[formdata.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/10.form/form/formdata.en.md" >}} )

[formdata.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/10.form/form/formdata.sv.md" >}} )

[default.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/10.form/default.en.md" >}} )

[default.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/10.form/default.sv.md" >}} )

####### 11.info
######## newsletter
[info.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/11.info/newsletter/info.en.md" >}} )

[info.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/11.info/newsletter/info.sv.md" >}} )

####### 11.newsletter
[newsletter.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/11.newsletter/newsletter.en.md" >}} )

[newsletter.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/11.newsletter/newsletter.sv.md" >}} )

####### 15.projekt
######## 02.individdata-och-dataportabilitet
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/15.projekt/02.individdata-och-dataportabilitet/component_page.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/15.projekt/02.individdata-och-dataportabilitet/news_blog_post.sv.md" >}} )

######## 03.taxonomi-och-begreppsstruktur
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/15.projekt/03.taxonomi-och-begreppsstruktur/component_page.en.md" >}} )

[project_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/15.projekt/03.taxonomi-och-begreppsstruktur/project_page.sv.md" >}} )

######## 04.test
[project_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/15.projekt/04.test/project_page.sv.md" >}} )

######## 05.digital-yrkesvaegledning
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/15.projekt/05.digital-yrkesvaegledning/component_page.en.md" >}} )

[project_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/15.projekt/05.digital-yrkesvaegledning/project_page.sv.md" >}} )

######## test2
[project_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/15.projekt/test2/project_page.sv.md" >}} )

[project_list.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/15.projekt/project_list.en.md" >}} )

[project_list.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/15.projekt/project_list.sv.md" >}} )

####### 16.inspire
######## 17.ulrika
[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/16.inspire/17.ulrika/component_page.sv.md" >}} )

[inspire_list.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/16.inspire/inspire_list.sv.md" >}} )

####### files
[assets.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/files/assets.sv.md" >}} )

####### newsletter
[newsletter.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/newsletter/newsletter.en.md" >}} )

[newsletter.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/newsletter/newsletter.sv.md" >}} )

###### pagesz
####### 01.home
[frontpage.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/01.home/frontpage.en.md" >}} )

[frontpage]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/01.home/frontpage.md" >}} )

[frontpage.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/01.home/frontpage.sv.md" >}} )

####### 02.products
######## 11.jobsearch
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/11.jobsearch/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/11.jobsearch/component_page.sv.md" >}} )

######## 12.ekosystem_foer_annonser
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/12.ekosystem_foer_annonser/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/12.ekosystem_foer_annonser/component_page.sv.md" >}} )

######## 12.jobtech-taxonomy
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/12.jobtech-taxonomy/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/12.jobtech-taxonomy/component_page.sv.md" >}} )

######## 13.jobstream
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/13.jobstream/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/13.jobstream/component_page.sv.md" >}} )

######## 14.historical-jobs
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/14.historical-jobs/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/14.historical-jobs/component_page.sv.md" >}} )

######## 15.yrkesprognoser
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/15.yrkesprognoser/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/15.yrkesprognoser/component_page.sv.md" >}} )

######## 16.connect-once
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/16.connect-once/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/16.connect-once/component_page.sv.md" >}} )

######## 17.jobtech-atlas
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/17.jobtech-atlas/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/17.jobtech-atlas/component_page.sv.md" >}} )

######## 18.etik-och-digital-matchning
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/18.etik-och-digital-matchning/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/18.etik-och-digital-matchning/component_page.sv.md" >}} )

######## 19.alljobads
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/19.alljobads/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/19.alljobads/component_page.sv.md" >}} )

######## 20.giglab-sverige
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/20.giglab-sverige/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/20.giglab-sverige/component_page.sv.md" >}} )

######## 21.open-plattforms
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/21.open-plattforms/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/21.open-plattforms/component_page.sv.md" >}} )

######## jobad-enrichments
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/jobad-enrichments/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/jobad-enrichments/component_page.sv.md" >}} )

[components_list.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/components_list.en.md" >}} )

[components_list.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/02.products/components_list.sv.md" >}} )

####### 03.news
######## 10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.sv.md" >}} )

######## 11.layke-analytics
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/11.layke-analytics/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/11.layke-analytics/news_blog_post.sv.md" >}} )

######## 12.jobagent
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/12.jobagent/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/12.jobagent/news_blog_post.sv.md" >}} )

######## 13.framtid
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/13.framtid/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/13.framtid/news_blog_post.sv.md" >}} )

######## intervju_mattias
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/intervju_mattias/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/intervju_mattias/news_blog_post.sv.md" >}} )

######## jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av/news_blog_post.sv.md" >}} )

######## kom-igang-med-jobtech-developments-api-er
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/kom-igang-med-jobtech-developments-api-er/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/kom-igang-med-jobtech-developments-api-er/news_blog_post.sv.md" >}} )

######## kom-igang-med-jobtech-developments-api-er2
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/kom-igang-med-jobtech-developments-api-er2/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/kom-igang-med-jobtech-developments-api-er2/news_blog_post.sv.md" >}} )

######## vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus/news_blog_post.sv.md" >}} )

######## vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data/news_blog_post.sv.md" >}} )

[news_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/news_page.en.md" >}} )

[news_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/03.news/news_page.sv.md" >}} )

####### 04.calendar
######## e-forvaltningsdagarna
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/04.calendar/e-forvaltningsdagarna/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/04.calendar/e-forvaltningsdagarna/events_blog_post.sv.md" >}} )

######## test-event
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/04.calendar/test-event/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/04.calendar/test-event/events_blog_post.sv.md" >}} )

[events_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/04.calendar/events_page.en.md" >}} )

[events_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/04.calendar/events_page.sv.md" >}} )

####### 05.work
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/05.work/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/05.work/page.sv.md" >}} )

####### 06.status
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/06.status/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/06.status/page.sv.md" >}} )

####### 07.contact
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/07.contact/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/07.contact/page.sv.md" >}} )

####### 08.about
######## 01.availability
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/08.about/01.availability/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/08.about/01.availability/page.sv.md" >}} )

######## 02.cookies
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/08.about/02.cookies/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/08.about/02.cookies/page.sv.md" >}} )

[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/08.about/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/08.about/page.sv.md" >}} )

####### 09.om-jobtech-development
######## 01.samarbeta-med-oss
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/09.om-jobtech-development/01.samarbeta-med-oss/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/09.om-jobtech-development/01.samarbeta-med-oss/page.sv.md" >}} )

######## 02.kontakta-oss
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/09.om-jobtech-development/02.kontakta-oss/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/09.om-jobtech-development/02.kontakta-oss/page.sv.md" >}} )

######## 03.inspireras-av-andra
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/09.om-jobtech-development/03.inspireras-av-andra/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/09.om-jobtech-development/03.inspireras-av-andra/page.sv.md" >}} )

[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/09.om-jobtech-development/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/09.om-jobtech-development/page.sv.md" >}} )

####### files
[assets.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pagesz/files/assets.sv.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se-v2/README.md" >}} )

#### jobtechdev-se
##### docs
[theme]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/docs/theme.md" >}} )

[upgrade]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/docs/upgrade.md" >}} )

##### user
###### pages
####### 01.home
[frontpage.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/01.home/frontpage.en.md" >}} )

[frontpage]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/01.home/frontpage.md" >}} )

[frontpage.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/01.home/frontpage.sv.md" >}} )

####### 02.components
######## 01.jobsearch
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/01.jobsearch/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/01.jobsearch/component_page.sv.md" >}} )

######## 02.ekosystem_foer_annonser
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/02.ekosystem_foer_annonser/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/02.ekosystem_foer_annonser/component_page.sv.md" >}} )

######## 03.jobtech-taxonomy
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/03.jobtech-taxonomy/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/03.jobtech-taxonomy/component_page.sv.md" >}} )

######## 04.jobstream
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/04.jobstream/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/04.jobstream/component_page.sv.md" >}} )

######## 05.historical-ads
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/05.historical-ads/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/05.historical-ads/component_page.sv.md" >}} )

######## 06.yrkesprognoser
######### 01.metodbeskrivning-yrkesprognoser
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/06.yrkesprognoser/01.metodbeskrivning-yrkesprognoser/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/06.yrkesprognoser/01.metodbeskrivning-yrkesprognoser/component_page.sv.md" >}} )

[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/06.yrkesprognoser/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/06.yrkesprognoser/component_page.sv.md" >}} )

######## 07.connect-once
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/07.connect-once/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/07.connect-once/component_page.sv.md" >}} )

######## 08.jobtech-atlas
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/08.jobtech-atlas/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/08.jobtech-atlas/component_page.sv.md" >}} )

######## 09.etik-och-digital-matchning
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/09.etik-och-digital-matchning/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/09.etik-och-digital-matchning/component_page.sv.md" >}} )

######## 10.alljobads
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/10.alljobads/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/10.alljobads/component_page.sv.md" >}} )

######## 11.giglab-sverige
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/11.giglab-sverige/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/11.giglab-sverige/component_page.sv.md" >}} )

######## 12.open-plattforms
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/12.open-plattforms/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/12.open-plattforms/component_page.sv.md" >}} )

######## 13.jobad-enrichments
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/13.jobad-enrichments/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/13.jobad-enrichments/component_page.sv.md" >}} )

######## 14.digital-vaegledning-och-laenkad-data
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/14.digital-vaegledning-och-laenkad-data/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/14.digital-vaegledning-och-laenkad-data/component_page.sv.md" >}} )

######## 17.jobad-links
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/17.jobad-links/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/17.jobad-links/component_page.sv.md" >}} )

######## 18.digital-skills
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/18.digital-skills/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/18.digital-skills/component_page.sv.md" >}} )

######## 19.jobsearch-trends
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/19.jobsearch-trends/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/19.jobsearch-trends/component_page.sv.md" >}} )

######## 21.kompetensmatchning
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/21.kompetensmatchning/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/21.kompetensmatchning/component_page.sv.md" >}} )

######## 22.jobed-connect
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/22.jobed-connect/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/22.jobed-connect/component_page.sv.md" >}} )

[components_list.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/components_list.en.md" >}} )

[components_list.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/02.components/components_list.sv.md" >}} )

####### 03.news
######## 01.api-days-helsinki-and-north
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/01.api-days-helsinki-and-north/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/01.api-days-helsinki-and-north/events_blog_post.sv.md" >}} )

######## 01.tech-awards-sweden-2023
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/01.tech-awards-sweden-2023/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/01.tech-awards-sweden-2023/events_blog_post.sv.md" >}} )

######## 02.ai-loesning-som-laeser-tolkar-och-analyserar-cv-n
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/02.ai-loesning-som-laeser-tolkar-och-analyserar-cv-n/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/02.ai-loesning-som-laeser-tolkar-och-analyserar-cv-n/news_blog_post.sv.md" >}} )

######## 02.jobnet-vill-bli-en-sjaelvklar-destination-foer-jobbsoekare
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/02.jobnet-vill-bli-en-sjaelvklar-destination-foer-jobbsoekare/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/02.jobnet-vill-bli-en-sjaelvklar-destination-foer-jobbsoekare/news_blog_post.sv.md" >}} )

######## 03.pharus-tech-loeser-problemet-pa-arbetsmarknaden
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/03.pharus-tech-loeser-problemet-pa-arbetsmarknaden/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/03.pharus-tech-loeser-problemet-pa-arbetsmarknaden/news_blog_post.sv.md" >}} )

######## 03.sweden-innovation-days
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/03.sweden-innovation-days/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/03.sweden-innovation-days/events_blog_post.sv.md" >}} )

######## 04.digitalisering-and-transformation-i-offentlig-sektor-2023
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/04.digitalisering-and-transformation-i-offentlig-sektor-2023/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/04.digitalisering-and-transformation-i-offentlig-sektor-2023/events_blog_post.sv.md" >}} )

######## 04.malet-aer-en-datadriven-arbetsmarknad-foer-sverige
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/04.malet-aer-en-datadriven-arbetsmarknad-foer-sverige/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/04.malet-aer-en-datadriven-arbetsmarknad-foer-sverige/news_blog_post.sv.md" >}} )

######## 05.foss-north-2023
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/05.foss-north-2023/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/05.foss-north-2023/events_blog_post.sv.md" >}} )

######## 05.kompetensmatchning-se-en-plattform-foer-kompetensutveckling
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/05.kompetensmatchning-se-en-plattform-foer-kompetensutveckling/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/05.kompetensmatchning-se-en-plattform-foer-kompetensutveckling/news_blog_post.sv.md" >}} )

######## 06.loesningar-foer-offentig-sektor
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/06.loesningar-foer-offentig-sektor/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/06.loesningar-foer-offentig-sektor/events_blog_post.sv.md" >}} )

######## 06.skills-meetup-sweden
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/06.skills-meetup-sweden/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/06.skills-meetup-sweden/events_blog_post.sv.md" >}} )

######## 07.inrupt-solid-hackathon
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/07.inrupt-solid-hackathon/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/07.inrupt-solid-hackathon/events_blog_post.sv.md" >}} )

######## 07.semic2022-interoperabilitet
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/07.semic2022-interoperabilitet/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/07.semic2022-interoperabilitet/news_blog_post.sv.md" >}} )

######## 08.open-source-driving-the-european-digital-decade-april-18
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/08.open-source-driving-the-european-digital-decade-april-18/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/08.open-source-driving-the-european-digital-decade-april-18/events_blog_post.sv.md" >}} )

######## 08.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/08.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/08.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er/news_blog_post.sv.md" >}} )

######## 09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/09.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022/events_blog_post.sv.md" >}} )

######## 09.tech-awards-sweden-2023
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/09.tech-awards-sweden-2023/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/09.tech-awards-sweden-2023/events_blog_post.sv.md" >}} )

######## 10.ai-loesning-som-laeser-tolkar-och-analyserar-cv-n
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/10.ai-loesning-som-laeser-tolkar-och-analyserar-cv-n/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/10.ai-loesning-som-laeser-tolkar-och-analyserar-cv-n/news_blog_post.sv.md" >}} )

######## 10.api-et-jobstream-aer-nominerat-till-sweden-api-award
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/10.api-et-jobstream-aer-nominerat-till-sweden-api-award/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/10.api-et-jobstream-aer-nominerat-till-sweden-api-award/news_blog_post.sv.md" >}} )

######## 11.jobtech-development-deltar-pa-loesningar-foer-offentlig-sektor
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/11.jobtech-development-deltar-pa-loesningar-foer-offentlig-sektor/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/11.jobtech-development-deltar-pa-loesningar-foer-offentlig-sektor/events_blog_post.sv.md" >}} )

######## 11.sweden-innovation-days
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/11.sweden-innovation-days/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/11.sweden-innovation-days/events_blog_post.sv.md" >}} )

######## 12.malet-aer-en-datadriven-arbetsmarknad-foer-sverige
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/12.malet-aer-en-datadriven-arbetsmarknad-foer-sverige/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/12.malet-aer-en-datadriven-arbetsmarknad-foer-sverige/news_blog_post.sv.md" >}} )

######## 12.vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/12.vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/12.vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data/news_blog_post.sv.md" >}} )

######## 13.kompetensmatchning-se-en-plattform-foer-kompetensutveckling
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/13.kompetensmatchning-se-en-plattform-foer-kompetensutveckling/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/13.kompetensmatchning-se-en-plattform-foer-kompetensutveckling/news_blog_post.sv.md" >}} )

######## 13.summering-av-efoervaltningsdagarna
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/13.summering-av-efoervaltningsdagarna/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/13.summering-av-efoervaltningsdagarna/news_blog_post.sv.md" >}} )

######## 14.loesningar-foer-offentig-sektor
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/14.loesningar-foer-offentig-sektor/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/14.loesningar-foer-offentig-sektor/events_blog_post.sv.md" >}} )

######## 14.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/14.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/14.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.sv.md" >}} )

######## 15.jobagent
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/15.jobagent/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/15.jobagent/news_blog_post.sv.md" >}} )

######## 15.semic2022-interoperabilitet
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/15.semic2022-interoperabilitet/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/15.semic2022-interoperabilitet/news_blog_post.sv.md" >}} )

######## 16.intervju_mattias
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/16.intervju_mattias/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/16.intervju_mattias/news_blog_post.sv.md" >}} )

######## 16.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/16.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/16.trygghetsradets-klienter-far-nytta-av-jobtechs-api-er/news_blog_post.sv.md" >}} )

######## 17.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/17.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/17.innovationsledning-foer-groen-och-digital-omstaellning-16-november-2022/events_blog_post.sv.md" >}} )

######## 17.jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/17.jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/17.jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av/news_blog_post.sv.md" >}} )

######## 18.api-et-jobstream-aer-nominerat-till-sweden-api-award
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/18.api-et-jobstream-aer-nominerat-till-sweden-api-award/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/18.api-et-jobstream-aer-nominerat-till-sweden-api-award/news_blog_post.sv.md" >}} )

######## 18.vill-du-veta-mer-om-sveriges-baesta-api
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/18.vill-du-veta-mer-om-sveriges-baesta-api/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/18.vill-du-veta-mer-om-sveriges-baesta-api/events_blog_post.sv.md" >}} )

######## 19.framtid
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/19.framtid/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/19.framtid/news_blog_post.sv.md" >}} )

######## 19.jobtech-development-deltar-pa-loesningar-foer-offentlig-sektor
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/19.jobtech-development-deltar-pa-loesningar-foer-offentlig-sektor/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/19.jobtech-development-deltar-pa-loesningar-foer-offentlig-sektor/events_blog_post.sv.md" >}} )

######## 20.ingen-kan-loesa-problemet-pa-egen-hand-daerfoer-goer-vi-det-tillsammans
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/20.ingen-kan-loesa-problemet-pa-egen-hand-daerfoer-goer-vi-det-tillsammans/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/20.ingen-kan-loesa-problemet-pa-egen-hand-daerfoer-goer-vi-det-tillsammans/news_blog_post.sv.md" >}} )

######## 20.vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/20.vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/20.vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data/news_blog_post.sv.md" >}} )

######## 21.summering-av-efoervaltningsdagarna
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/21.summering-av-efoervaltningsdagarna/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/21.summering-av-efoervaltningsdagarna/news_blog_post.sv.md" >}} )

######## 21.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/21.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/21.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus/news_blog_post.sv.md" >}} )

######## 22.layke-analytics
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/22.layke-analytics/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/22.layke-analytics/news_blog_post.sv.md" >}} )

######## 22.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/22.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/22.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.sv.md" >}} )

######## 23.jobagent
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/23.jobagent/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/23.jobagent/news_blog_post.sv.md" >}} )

######## 23.ny-forskningsrapport-om-oeppna-data-ekosystem
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/23.ny-forskningsrapport-om-oeppna-data-ekosystem/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/23.ny-forskningsrapport-om-oeppna-data-ekosystem/news_blog_post.sv.md" >}} )

######## 24.intervju_mattias
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/24.intervju_mattias/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/24.intervju_mattias/news_blog_post.sv.md" >}} )

######## 24.tre-pilotprojekt
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/24.tre-pilotprojekt/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/24.tre-pilotprojekt/news_blog_post.sv.md" >}} )

######## 25.jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/25.jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/25.jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av/news_blog_post.sv.md" >}} )

######## 25.vi-slutar-anvaenda-api-nycklar
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/25.vi-slutar-anvaenda-api-nycklar/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/25.vi-slutar-anvaenda-api-nycklar/news_blog_post.sv.md" >}} )

######## 26.sammanfattning
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/26.sammanfattning/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/26.sammanfattning/news_blog_post.sv.md" >}} )

######## 26.vill-du-veta-mer-om-sveriges-baesta-api
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/26.vill-du-veta-mer-om-sveriges-baesta-api/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/26.vill-du-veta-mer-om-sveriges-baesta-api/events_blog_post.sv.md" >}} )

######## 27.eu-s-ordfoerandeskapstrio
######### det-franska-eu-ordfoerandeskapets-program
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/27.eu-s-ordfoerandeskapstrio/det-franska-eu-ordfoerandeskapets-program/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/27.eu-s-ordfoerandeskapstrio/det-franska-eu-ordfoerandeskapets-program/news_blog_post.sv.md" >}} )

######### eu-ordfoerandeskapstrions-program
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/27.eu-s-ordfoerandeskapstrio/eu-ordfoerandeskapstrions-program/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/27.eu-s-ordfoerandeskapstrio/eu-ordfoerandeskapstrions-program/news_blog_post.sv.md" >}} )

[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/27.eu-s-ordfoerandeskapstrio/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/27.eu-s-ordfoerandeskapstrio/news_blog_post.sv.md" >}} )

######## 27.framtid
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/27.framtid/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/27.framtid/news_blog_post.sv.md" >}} )

######## 28.ingen-kan-loesa-problemet-pa-egen-hand-daerfoer-goer-vi-det-tillsammans
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/28.ingen-kan-loesa-problemet-pa-egen-hand-daerfoer-goer-vi-det-tillsammans/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/28.ingen-kan-loesa-problemet-pa-egen-hand-daerfoer-goer-vi-det-tillsammans/news_blog_post.sv.md" >}} )

######## 28.summering-loesningar-foer-offentlig-sektor
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/28.summering-loesningar-foer-offentlig-sektor/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/28.summering-loesningar-foer-offentlig-sektor/news_blog_post.sv.md" >}} )

######## 29.open-space-3-en-sammanfattning
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/29.open-space-3-en-sammanfattning/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/29.open-space-3-en-sammanfattning/news_blog_post.sv.md" >}} )

######## 29.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/29.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/29.vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus/news_blog_post.sv.md" >}} )

######## 30.cybersecurity
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/30.cybersecurity/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/30.cybersecurity/news_blog_post.sv.md" >}} )

######## 30.layke-analytics
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/30.layke-analytics/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/30.layke-analytics/news_blog_post.sv.md" >}} )

######## 31.ny-forskningsrapport-om-oeppna-data-ekosystem
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/31.ny-forskningsrapport-om-oeppna-data-ekosystem/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/31.ny-forskningsrapport-om-oeppna-data-ekosystem/news_blog_post.sv.md" >}} )

######## 31.viktig-info-om-jobsearch
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/31.viktig-info-om-jobsearch/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/31.viktig-info-om-jobsearch/news_blog_post.sv.md" >}} )

######## 32.jobsearchtrends
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/32.jobsearchtrends/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/32.jobsearchtrends/news_blog_post.sv.md" >}} )

######## 32.tre-pilotprojekt
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/32.tre-pilotprojekt/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/32.tre-pilotprojekt/news_blog_post.sv.md" >}} )

######## 33.digital-infrastruktur
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/33.digital-infrastruktur/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/33.digital-infrastruktur/news_blog_post.sv.md" >}} )

######## 33.vi-slutar-anvaenda-api-nycklar
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/33.vi-slutar-anvaenda-api-nycklar/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/33.vi-slutar-anvaenda-api-nycklar/news_blog_post.sv.md" >}} )

######## 34.sammanfattning
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/34.sammanfattning/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/34.sammanfattning/news_blog_post.sv.md" >}} )

######## 34.tre-experter-digital-infrastruktur
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/34.tre-experter-digital-infrastruktur/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/34.tre-experter-digital-infrastruktur/news_blog_post.sv.md" >}} )

######## 35.eu-s-ordfoerandeskapstrio
######### det-franska-eu-ordfoerandeskapets-program
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/35.eu-s-ordfoerandeskapstrio/det-franska-eu-ordfoerandeskapets-program/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/35.eu-s-ordfoerandeskapstrio/det-franska-eu-ordfoerandeskapets-program/news_blog_post.sv.md" >}} )

######### eu-ordfoerandeskapstrions-program
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/35.eu-s-ordfoerandeskapstrio/eu-ordfoerandeskapstrions-program/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/35.eu-s-ordfoerandeskapstrio/eu-ordfoerandeskapstrions-program/news_blog_post.sv.md" >}} )

[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/35.eu-s-ordfoerandeskapstrio/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/35.eu-s-ordfoerandeskapstrio/news_blog_post.sv.md" >}} )

######## 35.sveriges-eu-ordfoerandeskap
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/35.sveriges-eu-ordfoerandeskap/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/35.sveriges-eu-ordfoerandeskap/news_blog_post.sv.md" >}} )

######## 36.30-ar-sv-eu-medlemskap
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/36.30-ar-sv-eu-medlemskap/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/36.30-ar-sv-eu-medlemskap/news_blog_post.sv.md" >}} )

######## 36.summering-loesningar-foer-offentlig-sektor
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/36.summering-loesningar-foer-offentlig-sektor/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/36.summering-loesningar-foer-offentlig-sektor/news_blog_post.sv.md" >}} )

######## 37.open-space-3-en-sammanfattning
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/37.open-space-3-en-sammanfattning/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/37.open-space-3-en-sammanfattning/news_blog_post.sv.md" >}} )

######## 37.webbdagarna-stockholm-18-19-april
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/37.webbdagarna-stockholm-18-19-april/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/37.webbdagarna-stockholm-18-19-april/events_blog_post.sv.md" >}} )

######## 38.cybersecurity
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/38.cybersecurity/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/38.cybersecurity/news_blog_post.sv.md" >}} )

######## 39.viktig-info-om-jobsearch
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/39.viktig-info-om-jobsearch/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/39.viktig-info-om-jobsearch/news_blog_post.sv.md" >}} )

######## 40.jobsearchtrends
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/40.jobsearchtrends/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/40.jobsearchtrends/news_blog_post.sv.md" >}} )

######## 41.digital-infrastruktur
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/41.digital-infrastruktur/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/41.digital-infrastruktur/news_blog_post.sv.md" >}} )

######## 42.tre-experter-digital-infrastruktur
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/42.tre-experter-digital-infrastruktur/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/42.tre-experter-digital-infrastruktur/news_blog_post.sv.md" >}} )

######## 43.sveriges-eu-ordfoerandeskap
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/43.sveriges-eu-ordfoerandeskap/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/43.sveriges-eu-ordfoerandeskap/news_blog_post.sv.md" >}} )

######## 44.30-ar-sv-eu-medlemskap
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/44.30-ar-sv-eu-medlemskap/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/44.30-ar-sv-eu-medlemskap/news_blog_post.sv.md" >}} )

######## 45.webbdagarna-stockholm-18-19-april
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/45.webbdagarna-stockholm-18-19-april/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/45.webbdagarna-stockholm-18-19-april/events_blog_post.sv.md" >}} )

######## 46.jobedconnect
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/46.jobedconnect/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/46.jobedconnect/news_blog_post.sv.md" >}} )

######## 47.data-spaces-symposium-2023
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/47.data-spaces-symposium-2023/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/47.data-spaces-symposium-2023/news_blog_post.sv.md" >}} )

######## 48.digital-yrkesvaegledning
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/48.digital-yrkesvaegledning/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/48.digital-yrkesvaegledning/news_blog_post.sv.md" >}} )

######## 49.sverige-ansluter-sig-till-international-open-data-charter
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/49.sverige-ansluter-sig-till-international-open-data-charter/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/49.sverige-ansluter-sig-till-international-open-data-charter/news_blog_post.sv.md" >}} )

######## 50.jobtech-development-intar-scenen-pa-foss-north-2023
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/50.jobtech-development-intar-scenen-pa-foss-north-2023/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/50.jobtech-development-intar-scenen-pa-foss-north-2023/news_blog_post.sv.md" >}} )

######## 51.jobtechdev-pa-2023-government-transformation-summit
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/51.jobtechdev-pa-2023-government-transformation-summit/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/51.jobtechdev-pa-2023-government-transformation-summit/news_blog_post.sv.md" >}} )

######## 52.ny-teknik-stoettar-det-livslanga-laerandet
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/52.ny-teknik-stoettar-det-livslanga-laerandet/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/52.ny-teknik-stoettar-det-livslanga-laerandet/news_blog_post.sv.md" >}} )

######## 53.sa-foerenklar-allaloener-se-loenefragan-genom-transparens
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/53.sa-foerenklar-allaloener-se-loenefragan-genom-transparens/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/53.sa-foerenklar-allaloener-se-loenefragan-genom-transparens/news_blog_post.sv.md" >}} )

######## 54.bra-matchning-rapport
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/54.bra-matchning-rapport/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/54.bra-matchning-rapport/news_blog_post.sv.md" >}} )

######## Webinar
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/Webinar/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/Webinar/events_blog_post.sv.md" >}} )

######## apidagar
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/apidagar/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/apidagar/news_blog_post.sv.md" >}} )

######## data-spaces-in-an-interoperable-europe-6-dec-i-bryssel
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/data-spaces-in-an-interoperable-europe-6-dec-i-bryssel/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/data-spaces-in-an-interoperable-europe-6-dec-i-bryssel/events_blog_post.sv.md" >}} )

######## demo-week
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/demo-week/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/demo-week/events_blog_post.sv.md" >}} )

######## innovationsveckan-3-7-oktober
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/innovationsveckan-3-7-oktober/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/innovationsveckan-3-7-oktober/events_blog_post.sv.md" >}} )

######## internetdagarna-21-22-november
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/internetdagarna-21-22-november/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/internetdagarna-21-22-november/events_blog_post.sv.md" >}} )

######## internetdagarna-22-23-november
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/internetdagarna-22-23-november/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/internetdagarna-22-23-november/events_blog_post.sv.md" >}} )

######## kom-igang-och-oeppet-forum-webbinarium-27-maj
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/kom-igang-och-oeppet-forum-webbinarium-27-maj/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/kom-igang-och-oeppet-forum-webbinarium-27-maj/events_blog_post.sv.md" >}} )

######## open-space-del-3
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/open-space-del-3/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/open-space-del-3/events_blog_post.sv.md" >}} )

######## open-space-workshop-16-17-mars
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/open-space-workshop-16-17-mars/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/open-space-workshop-16-17-mars/events_blog_post.sv.md" >}} )

######## open-space-workshop-8-december1
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/open-space-workshop-8-december1/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/open-space-workshop-8-december1/events_blog_post.sv.md" >}} )

######## sverige-i-europas-dataekosystem
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/sverige-i-europas-dataekosystem/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/sverige-i-europas-dataekosystem/events_blog_post.sv.md" >}} )

[news_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/news_page.en.md" >}} )

[news_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/03.news/news_page.sv.md" >}} )

####### 04.calendar
[events_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/04.calendar/events_page.en.md" >}} )

[events_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/04.calendar/events_page.sv.md" >}} )

####### 05.work
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/05.work/news_blog_post.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/05.work/page.sv.md" >}} )

####### 06.status
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/06.status/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/06.status/page.sv.md" >}} )

####### 07.about
######## 01.availability
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/07.about/01.availability/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/07.about/01.availability/page.sv.md" >}} )

######## 02.cookies
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/07.about/02.cookies/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/07.about/02.cookies/page.sv.md" >}} )

[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/07.about/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/07.about/page.sv.md" >}} )

####### 08.about
######## 01.availability
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/08.about/01.availability/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/08.about/01.availability/page.sv.md" >}} )

######## 02.cookies
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/08.about/02.cookies/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/08.about/02.cookies/page.sv.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/08.about/component_page.sv.md" >}} )

[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/08.about/page.en.md" >}} )

####### 08.om-jobtech-development
######## 01.kom-igang
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/08.om-jobtech-development/01.kom-igang/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/08.om-jobtech-development/01.kom-igang/page.sv.md" >}} )

######## 02.kontakta-oss
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/08.om-jobtech-development/02.kontakta-oss/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/08.om-jobtech-development/02.kontakta-oss/page.sv.md" >}} )

######## 03.inspireras-av-andra
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/08.om-jobtech-development/03.inspireras-av-andra/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/08.om-jobtech-development/03.inspireras-av-andra/page.sv.md" >}} )

[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/08.om-jobtech-development/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/08.om-jobtech-development/page.sv.md" >}} )

####### 09.form
######## form
######### thankyou
########## email-sent
[formdata.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.form/form/thankyou/email-sent/formdata.en.md" >}} )

[formdata.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.form/form/thankyou/email-sent/formdata.sv.md" >}} )

[formdata.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.form/form/thankyou/formdata.en.md" >}} )

[formdata.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.form/form/thankyou/formdata.sv.md" >}} )

[formdata.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.form/form/formdata.en.md" >}} )

[formdata.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.form/form/formdata.sv.md" >}} )

[default.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.form/default.en.md" >}} )

[default.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.form/default.sv.md" >}} )

####### 09.om-jobtech-development
######## 01.kom-igang
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.om-jobtech-development/01.kom-igang/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.om-jobtech-development/01.kom-igang/page.sv.md" >}} )

######## 02.kontakta-oss
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.om-jobtech-development/02.kontakta-oss/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.om-jobtech-development/02.kontakta-oss/page.sv.md" >}} )

######## 03.inspireras-av-andra
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.om-jobtech-development/03.inspireras-av-andra/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.om-jobtech-development/03.inspireras-av-andra/page.sv.md" >}} )

[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.om-jobtech-development/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/09.om-jobtech-development/page.sv.md" >}} )

####### 10.form
######## form
######### thankyou
########## email-sent
[formdata.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/10.form/form/thankyou/email-sent/formdata.en.md" >}} )

[formdata.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/10.form/form/thankyou/email-sent/formdata.sv.md" >}} )

[formdata.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/10.form/form/thankyou/formdata.en.md" >}} )

[formdata.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/10.form/form/thankyou/formdata.sv.md" >}} )

[formdata.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/10.form/form/formdata.en.md" >}} )

[formdata.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/10.form/form/formdata.sv.md" >}} )

[default.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/10.form/default.en.md" >}} )

[default.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/10.form/default.sv.md" >}} )

####### 11.info
######## newsletter
[info.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/11.info/newsletter/info.en.md" >}} )

[info.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/11.info/newsletter/info.sv.md" >}} )

####### 11.newsletter
[newsletter.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/11.newsletter/newsletter.en.md" >}} )

[newsletter.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/11.newsletter/newsletter.sv.md" >}} )

####### 15.projekt
######## 02.individdata-och-dataportabilitet
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/15.projekt/02.individdata-och-dataportabilitet/component_page.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/15.projekt/02.individdata-och-dataportabilitet/news_blog_post.sv.md" >}} )

######## 03.taxonomi-och-begreppsstruktur
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/15.projekt/03.taxonomi-och-begreppsstruktur/component_page.en.md" >}} )

[project_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/15.projekt/03.taxonomi-och-begreppsstruktur/project_page.sv.md" >}} )

######## 04.test
[project_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/15.projekt/04.test/project_page.sv.md" >}} )

######## 05.digital-yrkesvaegledning
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/15.projekt/05.digital-yrkesvaegledning/component_page.en.md" >}} )

[project_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/15.projekt/05.digital-yrkesvaegledning/project_page.sv.md" >}} )

######## test2
[project_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/15.projekt/test2/project_page.sv.md" >}} )

[project_list.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/15.projekt/project_list.en.md" >}} )

[project_list.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/15.projekt/project_list.sv.md" >}} )

####### 16.inspire
######## 17.ulrika
[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/16.inspire/17.ulrika/component_page.sv.md" >}} )

[inspire_list.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/16.inspire/inspire_list.sv.md" >}} )

####### files
[assets.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/files/assets.sv.md" >}} )

####### newsletter
[newsletter.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/newsletter/newsletter.en.md" >}} )

[newsletter.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pages/newsletter/newsletter.sv.md" >}} )

###### pagesz
####### 01.home
[frontpage.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/01.home/frontpage.en.md" >}} )

[frontpage]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/01.home/frontpage.md" >}} )

[frontpage.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/01.home/frontpage.sv.md" >}} )

####### 02.products
######## 11.jobsearch
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/11.jobsearch/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/11.jobsearch/component_page.sv.md" >}} )

######## 12.ekosystem_foer_annonser
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/12.ekosystem_foer_annonser/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/12.ekosystem_foer_annonser/component_page.sv.md" >}} )

######## 12.jobtech-taxonomy
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/12.jobtech-taxonomy/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/12.jobtech-taxonomy/component_page.sv.md" >}} )

######## 13.jobstream
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/13.jobstream/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/13.jobstream/component_page.sv.md" >}} )

######## 14.historical-jobs
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/14.historical-jobs/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/14.historical-jobs/component_page.sv.md" >}} )

######## 15.yrkesprognoser
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/15.yrkesprognoser/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/15.yrkesprognoser/component_page.sv.md" >}} )

######## 16.connect-once
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/16.connect-once/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/16.connect-once/component_page.sv.md" >}} )

######## 17.jobtech-atlas
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/17.jobtech-atlas/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/17.jobtech-atlas/component_page.sv.md" >}} )

######## 18.etik-och-digital-matchning
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/18.etik-och-digital-matchning/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/18.etik-och-digital-matchning/component_page.sv.md" >}} )

######## 19.alljobads
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/19.alljobads/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/19.alljobads/component_page.sv.md" >}} )

######## 20.giglab-sverige
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/20.giglab-sverige/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/20.giglab-sverige/component_page.sv.md" >}} )

######## 21.open-plattforms
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/21.open-plattforms/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/21.open-plattforms/component_page.sv.md" >}} )

######## jobad-enrichments
[component_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/jobad-enrichments/component_page.en.md" >}} )

[component_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/jobad-enrichments/component_page.sv.md" >}} )

[components_list.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/components_list.en.md" >}} )

[components_list.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/02.products/components_list.sv.md" >}} )

####### 03.news
######## 10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/10.ny-ai-loesning-foerbaettrar-jobbannonser-och-effektiviserar-digitala-matchningstjaenster/news_blog_post.sv.md" >}} )

######## 11.layke-analytics
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/11.layke-analytics/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/11.layke-analytics/news_blog_post.sv.md" >}} )

######## 12.jobagent
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/12.jobagent/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/12.jobagent/news_blog_post.sv.md" >}} )

######## 13.framtid
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/13.framtid/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/13.framtid/news_blog_post.sv.md" >}} )

######## intervju_mattias
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/intervju_mattias/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/intervju_mattias/news_blog_post.sv.md" >}} )

######## jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/jobtech-development-aer-ett-spjutspetsprojekt-som-manga-kan-laera-av/news_blog_post.sv.md" >}} )

######## kom-igang-med-jobtech-developments-api-er
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/kom-igang-med-jobtech-developments-api-er/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/kom-igang-med-jobtech-developments-api-er/news_blog_post.sv.md" >}} )

######## kom-igang-med-jobtech-developments-api-er2
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/kom-igang-med-jobtech-developments-api-er2/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/kom-igang-med-jobtech-developments-api-er2/news_blog_post.sv.md" >}} )

######## vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/vi-far-in-olika-perspektiv-blir-mer-snabbfotade-och-har-alltid-kunden-i-fokus/news_blog_post.sv.md" >}} )

######## vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data
[news_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data/news_blog_post.en.md" >}} )

[news_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/vi-ser-att-det-finns-ett-uppdaemt-behov-av-samverkan-kring-oeppna-data/news_blog_post.sv.md" >}} )

[news_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/news_page.en.md" >}} )

[news_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/03.news/news_page.sv.md" >}} )

####### 04.calendar
######## e-forvaltningsdagarna
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/04.calendar/e-forvaltningsdagarna/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/04.calendar/e-forvaltningsdagarna/events_blog_post.sv.md" >}} )

######## test-event
[events_blog_post.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/04.calendar/test-event/events_blog_post.en.md" >}} )

[events_blog_post.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/04.calendar/test-event/events_blog_post.sv.md" >}} )

[events_page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/04.calendar/events_page.en.md" >}} )

[events_page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/04.calendar/events_page.sv.md" >}} )

####### 05.work
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/05.work/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/05.work/page.sv.md" >}} )

####### 06.status
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/06.status/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/06.status/page.sv.md" >}} )

####### 07.contact
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/07.contact/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/07.contact/page.sv.md" >}} )

####### 08.about
######## 01.availability
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/08.about/01.availability/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/08.about/01.availability/page.sv.md" >}} )

######## 02.cookies
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/08.about/02.cookies/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/08.about/02.cookies/page.sv.md" >}} )

[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/08.about/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/08.about/page.sv.md" >}} )

####### 09.om-jobtech-development
######## 01.samarbeta-med-oss
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/09.om-jobtech-development/01.samarbeta-med-oss/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/09.om-jobtech-development/01.samarbeta-med-oss/page.sv.md" >}} )

######## 02.kontakta-oss
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/09.om-jobtech-development/02.kontakta-oss/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/09.om-jobtech-development/02.kontakta-oss/page.sv.md" >}} )

######## 03.inspireras-av-andra
[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/09.om-jobtech-development/03.inspireras-av-andra/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/09.om-jobtech-development/03.inspireras-av-andra/page.sv.md" >}} )

[page.en]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/09.om-jobtech-development/page.en.md" >}} )

[page.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/09.om-jobtech-development/page.sv.md" >}} )

####### files
[assets.sv]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/user/pagesz/files/assets.sv.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/www/jobtechdev-se/README.md" >}} )

#### megadocs-livedemo
[README]({{< ref "/filtered/mirror/arbetsformedlingen/www/megadocs-livedemo/README.md" >}} )

#### megadocs
##### archetypes
[default]({{< ref "/filtered/mirror/arbetsformedlingen/www/megadocs/archetypes/default.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/www/megadocs/README.md" >}} )

#### nystartsjobb-test
[Changelog]({{< ref "/filtered/mirror/arbetsformedlingen/www/nystartsjobb-test/Changelog.md" >}} )

[README]({{< ref "/filtered/mirror/arbetsformedlingen/www/nystartsjobb-test/README.md" >}} )

#### podcast
[README]({{< ref "/filtered/mirror/arbetsformedlingen/www/podcast/README.md" >}} )

#### pwa-jobstream
[README]({{< ref "/filtered/mirror/arbetsformedlingen/www/pwa-jobstream/README.md" >}} )

#### tips-and-tricks
[README]({{< ref "/filtered/mirror/arbetsformedlingen/www/tips-and-tricks/README.md" >}} )

