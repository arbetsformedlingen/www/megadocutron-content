---
title: November 2022
gitlaburl: https://gitlab.com/arbetsformedlingen/libraries/anonymisering/-/blob/main//CHANGELOG.md
gitdir: /arbetsformedlingen/libraries/anonymisering
gitdir-file-path: /CHANGELOG.md
date: '2023-03-21 14:41:02'
path: /arbetsformedlingen/libraries/anonymisering/CHANGELOG.md
tags:
- CHANGELOG.md
---
Changelog
===============================
# November 2022
* Code cleanup
* Use sets instead of lists for better performance

# October 2022
* Remove "webpage_url"
