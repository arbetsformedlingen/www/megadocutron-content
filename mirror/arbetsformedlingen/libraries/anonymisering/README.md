---
title: Enklare anonymisering av svensk text
gitlaburl: https://gitlab.com/arbetsformedlingen/libraries/anonymisering/-/blob/main//README.md
gitdir: /arbetsformedlingen/libraries/anonymisering
gitdir-file-path: /README.md
date: '2023-03-21 14:41:02'
path: /arbetsformedlingen/libraries/anonymisering/README.md
tags:
- README.md
---
## Enklare anonymisering av svensk text

- Maskerar namn (anligaste svenska för- och efternamnen enligt SCBs namnstatistik.)
- Maskerar epostadresser
- Maskerar telefonnummer som är skrivna i följd ( 08123456 )

- Tar bort hela meningar som innehåller information om fackliga företrädare
- Tar bort meningar där telefonnumret innehåller mellanslag (08-12 34 56)
  Använder nltk för svensk tokenisering av meningar.

Se [demo.py](demo.py) för exempelanvändning.

## Install

pip install -r requirements.txt
python setup.py develop

## Anonymize a jsonl-file

`python main.py input_file.jsonl`
and output-file will be written
