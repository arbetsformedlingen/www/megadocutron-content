---
title: valuestore-module
gitlaburl: https://gitlab.com/arbetsformedlingen/libraries/valuestore-module/-/blob/main//README.md
gitdir: /arbetsformedlingen/libraries/valuestore-module
gitdir-file-path: /README.md
date: '2023-06-16 15:22:26'
path: /arbetsformedlingen/libraries/valuestore-module/README.md
tags:
- README.md
---
# valuestore-module
Pythonmodul för värdeförråd

## Användning

Den här modulen är inte tänkt att köras standalone, utan som ett berorende från applikationer som behöver slå i värdeförråd. Ofta vill man dock utveckla denna modul samtidigt som "huvudapplikationen" och för att göra det behöver man först aktivera den virtuella miljö som tillhör huvudapplikationen och sedan installera modulen i develop-läge. 

### Exempel virtualenv:

    $ workon <virtuell miljö för huvudapplikationen>
    $ python setup.py develop

### Exempel anaconda:

    $ source activate <virtuell miljö för huvudapplikationen>
    $ python setup.py develop

