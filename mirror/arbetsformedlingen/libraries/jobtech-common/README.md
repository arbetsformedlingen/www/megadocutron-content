---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/libraries/jobtech-common/-/blob/main//README.md
gitdir: /arbetsformedlingen/libraries/jobtech-common
gitdir-file-path: /README.md
date: '2023-10-30 10:59:06'
path: /arbetsformedlingen/libraries/jobtech-common/README.md
tags:
- README.md
---
README - Jobtech-common
=============================

Description
-----------
Package for common functions in Jobtech, for example api-key handling and custom log formatting.

Install
-----------
1. cd to jobtech-common
2. pip install -r requirements.txt 
3. python setup.py develop