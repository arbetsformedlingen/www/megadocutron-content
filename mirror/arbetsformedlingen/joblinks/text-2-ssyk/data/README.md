---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/text-2-ssyk/-/blob/main//data/README.md
gitdir: /arbetsformedlingen/joblinks/text-2-ssyk
gitdir-file-path: /data/README.md
date: '2023-10-17 13:11:47'
path: /arbetsformedlingen/joblinks/text-2-ssyk/data/README.md
tags:
- data::README.md
- README.md
---
Model training is using data files from https://data.jobtechdev.se/annonser/historiska/index.html

Download, decompress and put in this directory in order to run training.
