---
title: text-2-ssyk
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/text-2-ssyk/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/text-2-ssyk
gitdir-file-path: /README.md
date: '2023-10-17 13:11:47'
path: /arbetsformedlingen/joblinks/text-2-ssyk/README.md
tags:
- README.md
---
# text-2-ssyk

Classifies a text into SSYK level 1-4 based on one or several multiclass classifiers.\
Training use datasets from [historical jobs](https://jobtechdev.se/docs/apis/historical/).

## Getting Started

1. Make sure you have [Python3](https://www.python.org/downloads/), [Poetry](https://python-poetry.org/) and [Make](https://www.gnu.org/software/make/) properly installed, as well as [Git LFS](https://git-lfs.com/) which is used to store the trained classifier files.
2. Run `poetry shell` to initialize a virtual environment.
3. Run `poetry install` to install the dependencies. In case this command fails, set `export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring` and then retry.
4. *Optional step:* You can call `make download-models` to fetch pretrained models as an independent Make target.
5. Call `make run-example` to run a small example.
6. Call `make test` to run the unit tests.

## Models

### 1. Baseline model

- Source: [training_model_baseline.py](src/training_model_baseline.py). *See `demo_train()` for an example of how to run the code*.
- Bag-of-words (15k features) + random forest classifier (30 trees, max depth 100)

#### Performance 
- 5-fold cross-validation
- Uniformly drawn samples

| Sample size [nr ads] | Exact score SSYK level 4 [%] | In top 3 [%] | In top 5 [%] |
|---------------------:|-----------------------------:|-------------:|-------------:|
|                  50k |                   58.2 ± 0.8 |   71.8 ± 0.8 |   75.7 ± 0.7 |
|                 100k |                   62.5 ± 0.3 |   75.5 ± 0.2 |   79.0 ± 0.2 |
|                 500k |                   72.0 ± 0.1 |   82.5 ± 0.1 |   85.2 ± 0.1 |
|                   1M |                   76.2 ± 0.1 |   85.6 ± 0.1 |   87.8 ± 0.1 |

### 2. Word vectors + backpropagation
- Source: [training_model_vectors.py](src/training_model_vectors.py)
- Word vectors derived during training (not pre-set)

#### Performance

300k train, 100k test: 75.3%

### 3. Enriched model
- Source: [training_model_enrich.py](training_model_enrich.py)
- Bag of words + random forest classifier.
- Input set to all enriched candidates (occupations, competencies and traits) extracted from the free text and headline (using the JobAd Enrichment API).
- Feature size as all available enriched candidates (~36k)

#### Performance

16k train, 4k test: 61.6%  
80k train, 20k test: 67.8%  
160k train, 40k test: 73.1%  
300k train, 100k test: 73.6%

## Model training
  
**Dependencies**: See [`pyproject.toml`][3]. To run model 2 above (Word vectors + backprop) also install preferred tensorflow libraries.  
**Data**: See [`README.md`][1] in [`data/`][2]. If [`training_model_enrich.py`][4] is run, enriched data is also needed. This can be generated (once) by changing to `generate_enrich = True` in source file.  
**Sources**: [src/training_model_...py  ](src/)
**Parameters**: See/change inside source for each model training script.
**Run**: `make train-...`

## Improvements
  
**Data quality**:  
Each model builds a classifier for the occupation (SSYK4 value) given ad texts. The SSYK4 value for each ad comes from the field 'occupation'/'legacy_taxonomy_id' (an occupation name converted into the corresponding SSYK4). There is no guarantee that an individual employer has selected the correct occupation for an ad. This is the case both for the training and any test set evaluation given the data used. Filtering or improving the data for occupations where it is known that employers have a high rate of mis-labeling would increase performance.  
  
**Model evaluations:**  
The dataset properties are 1) a large number of classes/occupations, 2) an uneven data distribution as some occupations are very common and others very rare and 3) uneven noise coming from employers mis-labeling data unevenly across occupations.  
One way to better evaluate, understand how and where to improve performance is to look at the accuracy on a more granular level. Performance on specific occupations can be looked at by e.g. the [confusion matrix](https://en.wikipedia.org/wiki/Confusion_matrix) for a model.  

A confusion matrix for a small enriched model can be found in [confusion_matrix.txt](confusion_matrix.txt) (src: [eval_occs.py](eval_occs.py)).  
![sample confusion matrix](confusion_matrix.png)
(Sample of [confusion_matrix.txt](confusion_matrix.txt))

Note-worthy:
- A high f1 score must beside a good model prediction also mean that employers have a low mis-labeling of the class.
- In the same way, a reduced f1 scores could either be caused by low model performance or a high number of mis-labelings/disagreements among employers on which occupation a set of similar ads belong to.
- One can look further into a single occupation to understand what is the cause of the low performance: see [eval_occs.py](eval_occs.py).

## Commandline interface

The entry point of the program is [`text2ssyk/joblinks_main.py`](text2ssyk/joblinks_main.py). This program reads one json-coded line at a time from `stdin` and decodes into a Python data structure that should be a single dictionary having the format `{"originalJobPosting": { "description":"text to analyze" }}`. For every dictionary `d` it classifies the text at `d["originalJobPosting"]["description"]` to an SSYK level 4 code and puts it at the `ssyk_lvl4` key. Every resulting dictionary is output as a separate json-coded line to `stdout`.

To try this program without installing anything except [Docker](https://www.docker.com/) (or [Podman](https://podman.io/)), do

```sh
docker build -t text2ssyk .
echo '{"originalJobPosting": { "description":"baka pizza" }}' | docker run -i text2ssyk
```
or, using Make:
```sh
make build-docker-image
make run-docker-example
```

This will produce the following to `stdout`:
```
{"originalJobPosting": {"description": "baka pizza"}, "ssyk_lvl4": 9411}
```

### Production model

A larger random forest model is also provided. It requires at least 92 GB of RAM to be executed.

To use it, simply point the `TEXT2SSYK_ROOT` environment variable at the `models/prod/` directory:

```
TEXT2SSYK_ROOT=models/prod/ make run-example
```

## A note on memory usage

This table shows the maximum memory usage of either model. The usage is roughly proportional to the training data size (300000 samples vs 2000000 samples).

| Model       | Command                                                         | Maximum memory usage (GB) |
|-------------|-----------------------------------------------------------------|---------------------------|
| Development | `/usr/bin/time -v make run-example`                             | 14.4                      |
| Production  | `TEXT2SSYK_ROOT=models/prod/ /usr/bin/time -v make run-example` | 92.0                      |

## License

Copyright 2022 Arbetsförmedlingen JobTech.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

[1]: data/README.md
[2]: data/
[3]: pyproject.toml
[4]: src/training_model_enrich.py
