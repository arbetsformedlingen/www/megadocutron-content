---
title: Sample Processing Data from the JobTechLinks Pipeline
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/sample-processing-data/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/sample-processing-data
gitdir-file-path: /README.md
date: '2023-01-31 18:05:19'
path: /arbetsformedlingen/joblinks/sample-processing-data/README.md
tags:
- README.md
---
# Sample Processing Data from the JobTechLinks Pipeline

Here are the first 100 job ads from the open data set of historical ads found
[here](https://minio.arbetsformedlingen.se/historiska-annonser/2006.rar).


Each data folder is named after a git commit and contains the processed job ads
produced by the corresponding version of [the JobTechLinks pipeline](https://gitlab.com/arbetsformedlingen/joblinks/pipeline).

Each file is the output of an ad processor. They have
been run in
[this](https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/tree/master/pipelines/ad-processing)
order.


## License

todo
