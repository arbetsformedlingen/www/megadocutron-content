---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/scraping/-/blob/main//README_WINDOWS.md
gitdir: /arbetsformedlingen/joblinks/scraping
gitdir-file-path: /README_WINDOWS.md
date: '2023-11-08 16:26:14'
path: /arbetsformedlingen/joblinks/scraping/README_WINDOWS.md
tags:
- README_WINDOWS.md
---
Instruktioner för att köra Docker i Windows
==============================================

- Zippa upp dummysecrets.zip i repots rotkatalog

- Starta terminal med Git Bash i IntelliJ

- Kör
export MSYS_NO_PATHCONV=1
(Se https://github.com/docker-archive/toolbox/issues/673 )

- Kör
docker run -v $PWD/dummysecrets:/secrets --env TEST_MODE=true shcrawler --scrape --limit-scrape --secretsdir /secrets --sites arbetsformedlingen.se

Om fel:
/usr/bin/env: ‘bash\r’: No such file or directory
...ändra i IntelliJ från CRLF till LF för radbrytningar för:  
Dockerfile
scripts/pipeline.sh

--------------------------------
docker logs shcrawler --details -f  
docker stop shcrawler && docker rm shcrawler || true  
docker exec -t -i shcrawler /bin/bash  