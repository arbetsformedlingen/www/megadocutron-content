---
title: Jobtech Links Scraping
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/scraping/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/scraping
gitdir-file-path: /README.md
date: '2023-11-08 16:26:14'
path: /arbetsformedlingen/joblinks/scraping/README.md
tags:
- README.md
---
# Jobtech Links Scraping
This repository contains Scrapy-based web crawlers for the **Jobtech Links** project.

## Project Overview
The repo contains 3 apps: scraper part (main purpose of the repo), data aggregator part and same as parser part.
The scraper part uses the Scrapy framework for
scraping various site/api:s that contain job ads. The scraped ads are stored as json files on s3/minio.
The data aggregator part merges the files on minio that contains ads 
into a single file, that is stored on minio.
The repo can be run either in docker container (i.e. as on remote environment) or in IDE (e.g. IntelliJ/PyCharm).
Running in docker container is good for making sure the docker config actually works and running in IDE is good
for debugging and testing while implementing.
## Set up environment

### Running scrapers in docker container
- Build the Docker image:
```sh
docker build -t shcrawler .
```
- Run the spider (i.e. with example parameters and env.variables):
```sh
docker run --env AWS_ACCESS_KEY_ID="$AWS_ACCESS_KEY_ID" \
  --env AWS_SECRET_ACCESS_KEY="$AWS_SECRET_ACCESS_KEY" \
  shcrawler scrapy crawl -a date_from=01/12/2019 -s LOG_LEVEL=INFO jobbsafari_se
```
- See the 'Environment variables' section about how to use the --env parameters.

### Running data aggregator in docker container
-  Run the scripts/data_aggregator.py.
```sh
docker run --env AWS_ACCESS_KEY_ID="$AWS_ACCESS_KEY_ID" \
  --env AWS_SECRET_ACCESS_KEY="$AWS_SECRET_ACCESS_KEY" \
  shcrawler python scripts/data_aggregator.py
```
### Running scrapers in IntelliJ or PyCharm:
- Make sure Python 3.8 or higher is installed
- Install dependencies (optionally in anaconda or similar):
```pip install - r requirements.txt``
  
- Set up a venv (or anaconda env if that was chosen):
  - File -> Project Structure -> SDKs -> + -> Add Python SDK... -> Virtual ENV Environment -(a Python 3.8 installation as base interpretator)
  
- Set up a run configuration (Edit configurations) with the following options:
  - Working directory = `[base dir]scraping\venv`
  - Choose target to run = `Module name` (instead of script path)
  - Module name = `scrapy.cmdline`
  - Example parameters (i.e. when scraping only offentligajobb.se). date_from part is mandatory:
    ``crawl -a date_from=01/12/2019 -s LOG_LEVEL=INFO offentligajobb.se -o "..\`filename.json"``
  - Example environment variables (see 'Environment variables' section for more):
    ``
    AWS_ENDPOINT_URL=https://minio.arbetsformedlingen.se/;
    AWS_DELIVER_BUCKET=scrapinghub-dev;
    AWS_ACCESS_KEY_ID=scrapinghub-dev;
    AWS_SECRET_ACCESS_KEY=[ask for password];
    AWS_CA_BUNDLE= [path\\to\\scraping]\\minio.pem;
    ``

### Running data aggregator in IDE
- Install dependencies and set up a venv as when setting up scrapers.
- Set up a run configuration (Edit configurations) with the following options:
  - Working directory = `[base dir]scraping\venv`
  - Choose target to run = `script path` (instead of module name)
  - Script path = `[repo dir]\scripts\data_aggregator.py`
  - Example environment variables (see 'Environment variables' section for more):
    ``
    AWS_ENDPOINT_URL =https://minio.arbetsformedlingen.se/;
    AWS_AGGREGATOR_FOLDER;
    AWS_DELIVER_BUCKET=pipeline;
    AWS_ACCESS_KEY_ID=scrapinghub;
    AWS_SECRET_ACCESS_KEY=[ask for password];
    AWS_CA_BUNDLE=[path\\to\\scraping]\\minio.pem;
    ``
  
### Running Same as parser in IDE
The depencies are not included in the requirements.txt, but the imports can be installed by IntelliJ.
What the script does is to parse an ad file created by a spider (i.e. typically se.indeed.com) and
parses the sameAs field that is a part of the job posting schema. From that it creates statistics with
number of occurrences for different domains.
- Place the ndjson file to parse in the same directory as same_as_parser and rename it scripts/scraped_ads.json
  (the ndjson file probably has a timestamp as naming before.
  scrape_ads.json is added to gitignore to avoid git commit by accident.)
- Run the script in same_as_parser.
- Statistics and url:s is logged to std.out.

## Environment variables
|Name                       |Description                                              |Example                                |
|:--------------------------|---------------------------------------------------------|:--------------------------------------|
|`AWS_ENDPOINT_URL`|URL to the S3/Minio where the result should be stored.|`https://minio.arbetsformedlingen.se/`|
|`AWS_AGGREGATOR_FOLDER`|Folder on S3/Minio to be used to store the merged file created in dat_aggregator.|`pipeline`|
|`AWS_DELIVER_BUCKET`|Bucket on S3/minio that is being used for reading/writing.|`scrapinghub` for scraper in prod.|
|`AWS_ACCESS_KEY_ID`|User name (account) on S3/minio to be used.|`scrapinghub`|
|`AWS_SECRET_ACCESS_KEY`|Password for the account on S3/minio.||
|`AWS_CA_BUNDLE`|Path to the certificate with full chain for Minio. Necessary when running in IDE on Windows.|`c:\scrapimg\minio.pem`|
|`CRAWLERA_ENABLED`|If crawlera should be enabled (e.g. necessary when scraping offentligajobb.se).|`True`|
|`CRAWLERA_KEY`|Password for crawlera if enabled||
|`JOBSTREAM_APIKEY`|Path to the api key for jobstream. Mandatory if running the spider in arbetsformedlingen_se_rest_ws.py.||
|`CLOSESPIDER_PAGECOUNT`|Limits number of responses to crawl. Optional variable.|`10`|
|`CLOSESPIDER_ITEMCOUNT`|Limits number of items to crawl. Optional variable.|`10`|
|`SPIDERMON_ENABLED`|If validation should be enabled.||
|`SPIDERMON_VALIDATION_DROP_ITEMS_WITH_ERRORS`|Drop items that contain validation errors should be enabled.||
|`SPIDERMON_SLACK_SENDER_TOKEN`|Bot User OAuth Token for validation in Slack.||
|`SPIDERMON_SLACK_SENDER_NAME`|Bot Name for validation in Slack.||
|`SPIDERMON_SLACK_RECIPIENTS`|Recipients for validation in Slack.|`@validation;#scrapy-alerts-dev`|
|`ENABLE_SLACK_NOTIFICATION`|Value True if notification to Slack should be enabled for validation errors.||

More possible env.variables can be found at [scrapy settings](https://doc.scrapy.org/en/latest/topics/settings.html)

## Item Fields
- Item schema is [schema.org JobPosting](https://schema.org/JobPosting).
- Spider-specific schemas as listed [here](schemas).