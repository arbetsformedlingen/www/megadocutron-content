---
input:
- remove_description
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/date-processor/-/blob/main//tests/testdata/README.md
gitdir: /arbetsformedlingen/joblinks/date-processor
gitdir-file-path: /tests/testdata/README.md
date: '2023-10-19 10:17:42'
path: /arbetsformedlingen/joblinks/date-processor/tests/testdata/README.md
tags:
- tests::testdata::README.md
- testdata::README.md
- README.md
---
INPUT: remove_description

Data fetched here:
https://gitlab.com/arbetsformedlingen/joblinks/sample-processing-data/-/blob/master/c871f4c/remove_description.out.gz
