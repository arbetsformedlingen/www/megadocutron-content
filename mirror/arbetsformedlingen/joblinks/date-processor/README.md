---
title: date-processor
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/date-processor/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/date-processor
gitdir-file-path: /README.md
date: '2023-10-19 10:17:42'
path: /arbetsformedlingen/joblinks/date-processor/README.md
tags:
- README.md
---
# date-processor

Handles dates in ads, with option to correct them if they are in the future

new ad field 'date_to_display_as_publish_date' is set to one of these values:

Priority:
1. 'originalJobPosting.datePosted'
2. 'firstSeen' 
3. 'now'

if date is in the future, it can be set to firstSeen (controlled by environment variable)

## Prerequisites

pip install -r requirements.txt

# Environment variables:

USE_STDIN, default value False  
FILE_NAME, default value 'output.json', used if USE_STDIN is False  
LOG_LEVEL, default value 'INFO'  
LOG_FILE, default value "date-processor-log"  
PRINT, default value True. Determines if ads should be written to stdout at completion.  
CHANGE_FUTURE_DATES, default value False. Determines if a date from an ad should be to 'firstSeen' if it is in the future

# logging

Logging is done to file  
Dates in the future will be logged as 'warning'

# Usage

python main.py  
optional argument --chaos-test will cause 5% risk of exiting immediately with error code 1


# TODO:

Docker 




