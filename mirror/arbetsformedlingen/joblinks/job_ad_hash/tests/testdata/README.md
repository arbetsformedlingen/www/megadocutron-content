---
input:
- text_2_ssyk
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/job_ad_hash/-/blob/main//tests/testdata/README.md
gitdir: /arbetsformedlingen/joblinks/job_ad_hash
gitdir-file-path: /tests/testdata/README.md
date: '2023-10-26 11:31:37'
path: /arbetsformedlingen/joblinks/job_ad_hash/tests/testdata/README.md
tags:
- tests::testdata::README.md
- testdata::README.md
- README.md
---
INPUT: text_2_ssyk

Data fetched here:
https://gitlab.com/arbetsformedlingen/joblinks/sample-processing-data/-/blob/master/c871f4c/text_2_ssyk.out.gz
