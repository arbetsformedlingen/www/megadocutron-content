---
input:
- filter_for_import
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/common-cli-tools/-/blob/main//tests/testdata/README.md
gitdir: /arbetsformedlingen/joblinks/common-cli-tools
gitdir-file-path: /tests/testdata/README.md
date: '2023-10-13 12:48:25'
path: /arbetsformedlingen/joblinks/common-cli-tools/tests/testdata/README.md
tags:
- tests::testdata::README.md
- testdata::README.md
- README.md
---
INPUT: filter_for_import

Data fetched here:
https://gitlab.com/arbetsformedlingen/joblinks/sample-processing-data/-/blob/master/c871f4c/filter_for_import.out.gz
