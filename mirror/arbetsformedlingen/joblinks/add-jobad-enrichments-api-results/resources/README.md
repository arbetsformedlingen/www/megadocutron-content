---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/add-jobad-enrichments-api-results/-/blob/main//resources/README.md
gitdir: /arbetsformedlingen/joblinks/add-jobad-enrichments-api-results
gitdir-file-path: /resources/README.md
date: '2023-10-17 14:45:17'
path: /arbetsformedlingen/joblinks/add-jobad-enrichments-api-results/resources/README.md
tags:
- resources::README.md
- README.md
---
During development, put files here with ads data. The data should have the following structure:
{"originalJobPosting": {"id": "52fcd194da9b01ccc042fc98dc1ad0e7", "title": "Rubrik för annons 1 här", "description": "Annonsbeskrivningen för annons 1 här" }}    
{"originalJobPosting": {"id": "37d918ffd5bdffb0723440a41014b844", "title": "Rubrik för annons 2 här",  "description": "Annonsbeskrivningen för annons 2 här" }}  