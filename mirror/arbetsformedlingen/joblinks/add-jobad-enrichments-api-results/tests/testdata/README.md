---
input:
- add_application_deadline
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/add-jobad-enrichments-api-results/-/blob/main//tests/testdata/README.md
gitdir: /arbetsformedlingen/joblinks/add-jobad-enrichments-api-results
gitdir-file-path: /tests/testdata/README.md
date: '2023-10-17 14:45:17'
path: /arbetsformedlingen/joblinks/add-jobad-enrichments-api-results/tests/testdata/README.md
tags:
- tests::testdata::README.md
- testdata::README.md
- README.md
---
INPUT: add_application_deadline

Data fetched here:
https://gitlab.com/arbetsformedlingen/joblinks/sample-processing-data/-/blob/master/c871f4c/add_application_deadline.out.gz
