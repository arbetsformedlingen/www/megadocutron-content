---
input:
- filter_missing0
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/detect-language/-/blob/main//tests/testdata/README.md
gitdir: /arbetsformedlingen/joblinks/detect-language
gitdir-file-path: /tests/testdata/README.md
date: '2023-10-18 14:30:52'
path: /arbetsformedlingen/joblinks/detect-language/tests/testdata/README.md
tags:
- tests::testdata::README.md
- testdata::README.md
- README.md
---
INPUT: filter_missing0

Data fetched here:
https://gitlab.com/arbetsformedlingen/joblinks/sample-processing-data/-/blob/master/c871f4c/filter_missing0.out.gz
