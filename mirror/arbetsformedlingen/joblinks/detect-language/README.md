---
title: detect-language
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/detect-language/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/detect-language
gitdir-file-path: /README.md
date: '2023-10-18 14:30:52'
path: /arbetsformedlingen/joblinks/detect-language/README.md
tags:
- README.md
---
# detect-language
Pipeline step that tries to detect the language used in the ad  

The step creates the attribute "detected_language" to the ads with detected two-letter language code (ISO 639-1) from the ad description.  
https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes  
Example of values for detected language: "sv", "en", "no", "da", "fi"  
Value for ads with no description at all (empty text): "xx"  
Value when detection is not possible (for example with description="..."): "xy"   
  
## Prerequisites  
Docker and a bash shell  
  
## Needed input data for running  
The step needs a list of ads with the attributes/structure:    
{"originalJobPosting": {"description": "Annonsbeskrivningen för annons 1 här" }}  
{"originalJobPosting": {"description": "Annonsbeskrivningen för annons 2 här" }}  

  
## Building Docker image  
./run.sh --build  
  
## Running Docker with ads on stdin  
cat /tmp/ads_input  |  ./run.sh    >/tmp/detect_language_output  
  
## Clean Docker image  
./run.sh --clean  
  
## Mac, remove previous docker-builds + cointainers  
docker system prune  
  
## Start shell in Docker image  
docker run -it joblinks/detect-language sh  
  
## Running with python and file as input (during development)  
If using ads file instead of stdin; put a pipeline file with ads in resources dir and run with:    
python main.py --filepath resources/thepipelinefile  

