---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/detect-language/-/blob/main//resources/README.md
gitdir: /arbetsformedlingen/joblinks/detect-language
gitdir-file-path: /resources/README.md
date: '2023-10-18 14:30:52'
path: /arbetsformedlingen/joblinks/detect-language/resources/README.md
tags:
- resources::README.md
- README.md
---
During development, put files here with ads data. The data should have the following structure:
{"originalJobPosting": {"description": "Annonsbeskrivningen för annons 1 här" }}  
{"originalJobPosting": {"description": "Annonsbeskrivningen för annons 2 här" }} 