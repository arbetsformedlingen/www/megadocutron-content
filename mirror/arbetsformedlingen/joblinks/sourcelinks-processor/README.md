---
title: sourcelinks-processor
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/sourcelinks-processor/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/sourcelinks-processor
gitdir-file-path: /README.md
date: '2023-10-19 10:22:57'
path: /arbetsformedlingen/joblinks/sourcelinks-processor/README.md
tags:
- README.md
---
# sourcelinks-processor

Handles sourcelinks in ads

if no SOURCES_TO_IMPORT are provided, isValid will be set to False for all ads, preventing them from being imported.

If an ad has no source links at all that are in the SOURCES_TO_IMPORT env var,
isValid is set to False, preventing the ad from being imported

If an had has ANY sources that should be imported, these will be in the 'sourceLinks' field.
All source links will be copied to a new field; 'original_source_links' which will NOT be imported. It's kept for troubleshooting.

## Prerequisites

pip install -r requirements.txt

# Environment variables:

USE_STDIN, default value True  
FILE_NAME, default value 'output.json', used if USE_STDIN is False  
LOG_LEVEL, default value 'INFO'  
LOG_FILE, default value "sourcelinks-processor-log"  
PRINT, default value True. Determines if ads should be written to stdout at completion.  
SOURCES_TO_IMPORT, a comma-separated string. Add all the sites you wish to import 'site_1,site2'

# logging

Logging is done to file

# Usage

python main.py
optional argument --chaos-test will cause 5% risk of exiting immediately with error code 1
