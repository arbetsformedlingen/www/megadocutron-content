---
title: 3.0.0
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/sourcelinks-processor/-/blob/main//CHANGELOG.md
gitdir: /arbetsformedlingen/joblinks/sourcelinks-processor
gitdir-file-path: /CHANGELOG.md
date: '2023-10-19 10:22:57'
path: /arbetsformedlingen/joblinks/sourcelinks-processor/CHANGELOG.md
tags:
- CHANGELOG.md
---
## 3.0.0
* Use Python 3.10.7
* Use Poetry for dependency management

## 2.0.0
* Reworked to only include sources that are explicitly allowed
* Sources to import must be a comma-separated string
* Updated Python version in Docker

## 1.0.0
* Initial release
* set 'isValid' to False if ALL (and ONLY unwanted) unwanted source links is in ad
