---
input:
- date_processor
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/sourcelinks-processor/-/blob/main//tests/testdata/README.md
gitdir: /arbetsformedlingen/joblinks/sourcelinks-processor
gitdir-file-path: /tests/testdata/README.md
date: '2023-10-19 10:22:57'
path: /arbetsformedlingen/joblinks/sourcelinks-processor/tests/testdata/README.md
tags:
- tests::testdata::README.md
- testdata::README.md
- README.md
---
INPUT: date_processor

Data fetched here:
https://gitlab.com/arbetsformedlingen/joblinks/sample-processing-data/-/blob/master/c871f4c/date_processor.out.gz
