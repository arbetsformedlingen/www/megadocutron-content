---
title: import-to-elastic
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/import-to-elastic/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/import-to-elastic
gitdir-file-path: /README.md
date: '2023-10-17 10:57:30'
path: /arbetsformedlingen/joblinks/import-to-elastic/README.md
tags:
- README.md
---
# import-to-elastic

The last part (i.e. Processor) of the Joblinks Pipeline.

## Description

Imports joblinks ads into an ElasticSearch index. The ads should be sent in from stdin, but for keeping development and
test simple there is support for reading ads from file too 

## Setup for developers and running as a Docker image

see the file `docs/developer_setup.md`
