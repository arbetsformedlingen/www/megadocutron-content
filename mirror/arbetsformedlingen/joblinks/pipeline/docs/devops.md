---
title: JobTechLinks Pipeline Devops
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/blob/main//docs/devops.md
gitdir: /arbetsformedlingen/joblinks/pipeline
gitdir-file-path: /docs/devops.md
date: '2023-08-18 11:36:16'
path: /arbetsformedlingen/joblinks/pipeline/docs/devops.md
tags:
- docs::devops.md
- devops.md
---
# JobTechLinks Pipeline Devops

    A tool to manage a server running the JobTechLinks Pipeline software.

# SYNOPSIS

    devops.sh [options] operation0 ... operationN

    The variables below are defined in C<conf/ops.sh> and with C<--namespace>.

    Operations:
      checkout           checkout the system to DEPLOYDIR
      push               upload docker/podman images to a Nexus server
      pull               download docker/podman images from a Nexus server
      build              build the system in the current working directory
      clean              delete the system installed in $DEPLOYDIR (be careful)
      clean_workdir      delete the output directory PROJBASE/NAMESPACE
      minio_copy         copy scraped files between minio buckets
      hostdeps           prepare an Ubuntu OS (>=20.10) for the system

    Options for push:
      --deploydir D      checkout in directory D
                         (config file variable: DEPLOYDIR)

    Options for clean:
      --deploydir D      checkout in directory D
                         (config file variable: DEPLOYDIR)
      --force            skip interactive confirmation prompt

    Options for clean_workdir:
      --projbase         the project base directory (under which exist namespace directories)
                         (config file variable: PROJBASE)
      --namespace        the namespace used when the system was run
                         (config file variable: NAMESPACE)

    Options for checkout:
      --repo             the git repository URL
                         (config file variable: REPO)
      --commit C         the commit to checkout from REPO
                         (config file variable: COMMIT)
      --deploydir D      checkout in directory D
                         (config file variable: DEPLOYDIR)

      The following extra options are used when checking out the optional
      closed source parts:

      --closedsource     use closed source - requires env. vars GITLAB_USER and GITLAB_USER,
                         which are deploy secrets in Gitlab.
      --secretsrepo R    the git repository URL
                         (config file variable: REPO)
      --secretsbranch B  which branch to use in SECRETSREPO (this is soon obsolete)

    Other options:
      --help             brief help message
      --verbose|-v       verbose logging

    Options for hostdeps:
      --timezone         set server timezone (accepts values from 'timedatectl list-timezones')
                         (config file variable: TIMEZONE)

# DESCRIPTION

    B<This program> performs some operations tasks on a server running JobTechLinks Pipeline.

# AUTHOR

Written by Per Weijnitz.

# REPORTING BUGS

launchotron help: [https://gitlab.com/arbetsformedlingen/joblinks/pipeline](https://gitlab.com/arbetsformedlingen/joblinks/pipeline)

# COPYRIGHT

Copyright 2021 Arbetsformedlingen.  License Apache License 2.0.  This
is free software: you are free to change and re distribute it. There
is NO WARRANTY, to the extent permitted by law.
