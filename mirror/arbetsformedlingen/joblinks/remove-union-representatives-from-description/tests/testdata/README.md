---
input:
- job_ad_hash
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/remove-union-representatives-from-description/-/blob/main//tests/testdata/README.md
gitdir: /arbetsformedlingen/joblinks/remove-union-representatives-from-description
gitdir-file-path: /tests/testdata/README.md
date: '2023-10-17 13:02:11'
path: /arbetsformedlingen/joblinks/remove-union-representatives-from-description/tests/testdata/README.md
tags:
- tests::testdata::README.md
- testdata::README.md
- README.md
---
INPUT: job_ad_hash

Data fetched here:
https://gitlab.com/arbetsformedlingen/joblinks/sample-processing-data/-/blob/master/c871f4c/job_ad_hash.out.gz
