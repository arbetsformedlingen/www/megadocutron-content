---
title: remove-union-representatives-from-description
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/remove-union-representatives-from-description/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/remove-union-representatives-from-description
gitdir-file-path: /README.md
date: '2023-10-17 13:02:11'
path: /arbetsformedlingen/joblinks/remove-union-representatives-from-description/README.md
tags:
- README.md
---

# remove-union-representatives-from-description  
Pipeline step that removes union representatives from text descriptions (unstructured data).  
  
## Prerequisites  
Docker and a bash shell  
  
## Needed input data for running  
The step needs a list of ads with the attributes/structure:    
{"originalJobPosting": { "description": "Annonsbeskrivningen för annons 1 här" }}    
{"originalJobPosting": { "description": "Annonsbeskrivningen för annons 2 här" }}    
  
## Building Docker image  
./run.sh --build  
  
## Running Docker with ads on stdin  
cat /tmp/ads_input  |  ./run.sh    >/tmp/ads_removed_union_rep_output  
  
## Clean Docker image  
./run.sh --clean  
  
## Mac, remove previous docker-builds + cointainers  
docker system prune  
  
## Start shell in Docker image  
docker run -it joblinks/remove-union-representatives-from-description sh  
  
## Install for development  
### Alternative 1: 
pip install -r requirements.txt  
  
### Alternative 2 (preferred if you will work with imported project 'anonymisering'): 
cd `<your local git home>`    
git clone https://github.com/JobtechSwe/anonymisering.git    
cd anonymisering    
python setup.py develop    
cd ../remove-union-representatives-from-description    
pip install -r requirements.txt    
  
## Running with python and file as input (during development)  
If using ads file instead of stdin; put a pipeline file with ads in resources dir and run with:    
python main.py --filepath resources/thepipelinefile  
  
## Running with python and debug, to see the removed sentences  
Put a pipeline file with ads in resources dir and run with:    
python main.py --filepath resources/thepipelinefile --debug true