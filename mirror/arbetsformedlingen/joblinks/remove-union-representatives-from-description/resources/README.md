---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/remove-union-representatives-from-description/-/blob/main//resources/README.md
gitdir: /arbetsformedlingen/joblinks/remove-union-representatives-from-description
gitdir-file-path: /resources/README.md
date: '2023-10-17 13:02:11'
path: /arbetsformedlingen/joblinks/remove-union-representatives-from-description/resources/README.md
tags:
- resources::README.md
- README.md
---
During development, put files here with ads data. The data should have the following structure:
{"originalJobPosting": { "description": "Annonsbeskrivningen för annons 1 här" }}
{"originalJobPosting": { "description": "Annonsbeskrivningen för annons 2 här" }}