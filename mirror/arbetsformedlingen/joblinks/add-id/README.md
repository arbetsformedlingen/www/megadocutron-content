---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/add-id/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/add-id
gitdir-file-path: /README.md
date: '2023-09-15 17:35:45'
path: /arbetsformedlingen/joblinks/add-id/README.md
tags:
- README.md
---
* Project Title

add-id - A JobLinks Pipeline Processor

** Description

This repository contains a tool to add an id to every object in an JobLinks json file.

Read more about the [[https://gitlab.com/joblinks][JobLinks project here]].

** Getting Started

These instructions will get you a copy of the project up and running
on your local machine for development and testing purposes. See
deployment for notes on how to deploy the project on a live system.

*** Prerequisites

 - [[https://www.docker.com/][Docker]]
 - [[https://www.gnu.org/software/bash/][Bash]]

*** Installing

0. Clone this repository.
1. Build the docker image:
: ./run.sh --build


If you want to remove the Docker image, and clean out unused container
data from your Docker installation, run:
: ./run.sh --clean


*** Running

TO start the processor, run:
: ./run.sh

The program accepts data on stdin, and prints the result to stdout, in the JobLinks Pipeline format. (FIXME: add link here)
