---
title: ad-patcher-data
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/ad-patcher-data/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/ad-patcher-data
gitdir-file-path: /README.md
date: '2023-05-19 13:45:59'
path: /arbetsformedlingen/joblinks/ad-patcher-data/README.md
tags:
- README.md
---
# ad-patcher-data



## This repository has corrections for ads in JobAd Links
In some cases, the detection of occupation or geographical places goes wrong, this is a way to handle that.


## Usage
The following information is required:
- The ad id
- The attribute(s) that should be changed as separate objects in the `"change_list"`attribute.
- The new value(s)
Optional:
- Date when added
- A comment why the change is needed
- A reference to the incident in ServiceNow, if applicable

Example:
```json
{
  "12345 - this is where you put the ad id": {
    "comment": "Comments are ignored, the relevant json key is 'change_list'",
    "INC": "the number from ServiceNow, if applicable",
    "Date": "2023-03-09 or whenever you enter the patch information",
    "Change list information": "it's a list of one or more json objects which will be written to the ad",
    "change_list": [
      {
        "key": "workplace_addresses",
        "value": {
          "municipality_concept_id": "???",
          "municipality": "Ekeby",
          "region_concept_id": "???",
          "region": "Skåne län",
          "country_concept_id": "i46j_HmG_v64",
          "country": "Sverige"
        }
      }
    ]
  }
}
```

The `ad-patcher` processor will pick up the file `patch_data.json` from this GitLab repo´s `main`branch when it runs. 
