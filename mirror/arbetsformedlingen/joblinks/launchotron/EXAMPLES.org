** List running instances

: launchotron -l


** Compute a value from stdin

: $ echo "hello world" | launchotron "tr '[:lower:]' '[:upper:]'"
: HELLO WORLD


** Run a script

: $ echo 'echo "hello world"' > script.sh
: $ launchotron -u script.sh "bash script.sh"
: hello world


** Run a script in the background by using --result, and later collect results and shutdown

: $ ID=$(launchotron -u script.sh --result /tmp/greeting.txt "bash script.sh > /tmp/greeting.txt")
: $ echo $ID
: i-0bfb6d21d162715a9
: $ mkdir results
: $ launchotron --outputdir results --harvest $ID


** Start an instance in verbose mode, to track down problems

: launchotron -v "date"


** Start a new instance of a specific type, for upcoming interactive work

: launchotron --instance-type t3.large --no-shutdown 'echo $HOSTNAME ready'

The echo command is in this case just a dummy, as launchotron expects
a command, but we want to do our work later.


** Connect to a running instance for an interactive session

: launchotron -c

You don't need to supply id if you only have one instance running.


** Run a command non-interactively on a running instance

: $ launchotron -c -- "date"
: Wed Apr 28 13:04:02 UTC 2021

: $ launchotron -c i-0d2291ad0784dc117 "date"
: Wed Apr 28 13:04:47 UTC 2021

You can upload files in the same command too:
: $ echo hi > newfile
: $ launchotron -u newfile -c -- "cat newfile"
: hi

** Active termination

The simplest way is to issue the ~--terminate | -t~ command:

: $ launchotron -t i-0d2291ad0784dc117
: Are you sure you want to terminate instance i-0d2291ad0784dc117? [Enter / Ctrl-c]

The shutdown command can of course also be run from a script, or directly:

: $ launchotron -c i-0d2291ad0784dc117 "sudo shutdown -h now"

The termination flag picks the first instance it finds unless you
specify an id. You can combine it with ~--force~ to get suppress the
confirmation dialog:
: $ launchotron --force -t


** Running in parallel

You can use GNU Parallel to distribute jobs across many EC2 instances:

: ## the input files
: $ ls *.json
: input0.json  input1.json  input3.json
: $
: ## start one instance for each input file
: $ parallel --results outdir --jobs 0 'launchotron -u {} "echo \$HOSTNAME crunches file: \$(wc -l {})"' ::: *.json
: ip-10-1-35-45 crunches file: 100 input0.json
: ip-10-1-147-154 crunches file: 100 input1.json
: ip-10-1-67-232 crunches file: 100 input3.json
: $
: ## proof that all instances terminated
: $ launchotron -l
: $
: ## parallel has saved the results
: $ cat outdir/*/*/stdout
: ip-10-1-35-45 crunches file: 100 input0.json
: ip-10-1-147-154 crunches file: 100 input1.json
: ip-10-1-67-232 crunches file: 100 input3.json
