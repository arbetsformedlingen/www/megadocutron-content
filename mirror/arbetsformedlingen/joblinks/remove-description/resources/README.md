---
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/remove-description/-/blob/main//resources/README.md
gitdir: /arbetsformedlingen/joblinks/remove-description
gitdir-file-path: /resources/README.md
date: '2023-10-19 10:10:50'
path: /arbetsformedlingen/joblinks/remove-description/resources/README.md
tags:
- resources::README.md
- README.md
---
During development, put files here with ads data. The data should have the following structure:
{"originalJobPosting": { "description": "Annonsbeskrivningen för annons 1 här" }}
{"originalJobPosting": { "description": "Annonsbeskrivningen för annons 2 här" }}