---
title: remove-description
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/remove-description/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/remove-description
gitdir-file-path: /README.md
date: '2023-10-19 10:10:50'
path: /arbetsformedlingen/joblinks/remove-description/README.md
tags:
- README.md
---
# remove-description  
Pipeline step that removes the attribue description in the ad  
  
## Prerequisites  
Docker and a bash shell  
  
## Needed input data for running  
The step needs a list of ads with the attributes/structure:    
{"originalJobPosting": { "description": "Annonsbeskrivningen för annons 1 här" }}    
{"originalJobPosting": { "description": "Annonsbeskrivningen för annons 2 här" }}    
  
## Building Docker image  
./run.sh --build  
  
## Running Docker with ads on stdin  
cat /tmp/ads_input  |  ./run.sh    >/tmp/ads_remove_description_output  
  
## Clean Docker image  
./run.sh --clean  
  
## Mac, remove previous docker-builds + cointainers  
docker system prune  
  
## Start shell in Docker image  
docker run -it joblinks/remove-description sh  
  
## Running with python and file as input (during development)  
If using ads file instead of stdin; put a pipeline file with ads in resources dir and run with:    
python main.py --filepath resources/thepipelinefile  
