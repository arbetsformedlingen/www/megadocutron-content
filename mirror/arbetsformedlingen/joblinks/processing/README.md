---
title: processing
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/processing/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/processing
gitdir-file-path: /README.md
date: '2020-03-22 07:03:23'
path: /arbetsformedlingen/joblinks/processing/README.md
tags:
- README.md
---
# processing

Process the scraped data into a open dataset named joblinks.