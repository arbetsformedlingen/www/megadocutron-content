---
title: joblinks-importer
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/joblinks-importer/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/joblinks-importer
gitdir-file-path: /README.md
date: '2023-09-18 16:37:05'
path: /arbetsformedlingen/joblinks/joblinks-importer/README.md
tags:
- README.md
---
# joblinks-importer

The last part (i.e. Processor) of the Joblinks Pipeline.

## Description

Imports joblinks ads into an Opensearch index. The ads should be sent in from stdin, but for keeping development and
test simple there is support for reading ads from file too 

## Setup for developers and running as a Docker image

see the file `docs/developer_setup.md`
