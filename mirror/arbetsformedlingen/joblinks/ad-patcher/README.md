---
title: ad-patcher-processor
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/ad-patcher/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/ad-patcher
gitdir-file-path: /README.md
date: '2023-03-24 15:45:30'
path: /arbetsformedlingen/joblinks/ad-patcher/README.md
tags:
- README.md
---
# ad-patcher-processor

A way of correcting ads when scraping or classification has gone wrong.  
It takes a json file from another GitLab repo and applies the changes listed for a particular ad id.


## Prerequisites
###
Python version 3.11.* 

### Poetry for dependence management
`pip install poetry`
Set up the project as a Poetry project in your editor and installation should be handled. Or run `poetry install`



# Environment variables:

`USE_STDIN`, default value True, used as part of the JobAd Links pipeline.  
`FILE_NAME`, default value 'output.json', used if USE_STDIN is False.     
`LOGURU_LEVEL`, default value 'INFO', log level.  
`PRINT`, default value True. Determines if ads should be written to stdout at completion.    
`PATCH_FILE_URL`, an url to a json file with desired corrections. Default value points to the [ad-patcher-data](https://gitlab.com/arbetsformedlingen/joblinks/ad-patcher-data) repository.  


# Usage

`python main.py`  
optional argument `--chaos-test` will cause 5% risk of exiting immediately with error code 1.


# Run locally with your own patch file in json format for development:
Start Pýthon's http server in a directory with the desired json file `patch.json`in this example. In the example it will listen to port 9000

`python -m http.server 9000`
and the patch file should be accessible on http://localhost:9000/patch.json which can be used in the environment variable `PATCH_FILE_URL`
