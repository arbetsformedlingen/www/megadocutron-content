---
title: 1.0.0
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/ad-patcher/-/blob/main//CHANGELOG.md
gitdir: /arbetsformedlingen/joblinks/ad-patcher
gitdir-file-path: /CHANGELOG.md
date: '2023-03-24 15:45:30'
path: /arbetsformedlingen/joblinks/ad-patcher/CHANGELOG.md
tags:
- CHANGELOG.md
---
## 1.0.0
* Initial release
