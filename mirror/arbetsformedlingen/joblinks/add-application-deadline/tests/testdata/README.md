---
input:
- filter_for_import
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/add-application-deadline/-/blob/main//tests/testdata/README.md
gitdir: /arbetsformedlingen/joblinks/add-application-deadline
gitdir-file-path: /tests/testdata/README.md
date: '2023-10-19 10:16:21'
path: /arbetsformedlingen/joblinks/add-application-deadline/tests/testdata/README.md
tags:
- tests::testdata::README.md
- testdata::README.md
- README.md
---
INPUT: filter_for_import

Data fetched here:
https://gitlab.com/arbetsformedlingen/joblinks/sample-processing-data/-/blob/master/c871f4c/filter_for_import.out.gz
