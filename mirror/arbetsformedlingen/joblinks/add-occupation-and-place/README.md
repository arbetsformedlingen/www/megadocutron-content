---
title: Project Title
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/add-occupation-and-place/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/add-occupation-and-place
gitdir-file-path: /README.md
date: '2023-10-13 12:46:14'
path: /arbetsformedlingen/joblinks/add-occupation-and-place/README.md
tags:
- README.md
---
# Project Title

add-occupation-and-place - A JobLinks Pipeline Processor

## Description

This repository contains a tool that enrich job ads with occupation and place. Both values comes from https://jobad-enrichments-api.jobtechdev.se/. The tool makes 1000 network calls and should not be trusted to be stable.

Read more about the [[https://gitlab.com/joblinks][JobLinks project here]].

## Getting Started

These instructions will get you a copy of the project up and running
on your local machine for development and testing purposes. See
deployment for notes on how to deploy the project on a live system.

### Prerequisites

 - [[https://www.docker.com/][Docker]]
 - [[https://www.gnu.org/software/bash/][Bash]]

### Installing

0. Clone this repository.
1. Build the docker image:
: ./run.sh --build


If you want to remove the Docker image, and clean out unused container
data from your Docker installation, run:
: ./run.sh --clean


### Running

TO start the processor, run:
: ./run.sh

The program accepts data on stdin, and prints the result to stdout, in the JobLinks Pipeline format. (FIXME: add link here)
