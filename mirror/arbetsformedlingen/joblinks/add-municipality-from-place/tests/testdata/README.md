---
input:
- add_jobad_enrichments_api_results
title: ''
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/add-municipality-from-place/-/blob/main//tests/testdata/README.md
gitdir: /arbetsformedlingen/joblinks/add-municipality-from-place
gitdir-file-path: /tests/testdata/README.md
date: '2023-10-19 09:15:16'
path: /arbetsformedlingen/joblinks/add-municipality-from-place/tests/testdata/README.md
tags:
- tests::testdata::README.md
- testdata::README.md
- README.md
---
INPUT: add_jobad_enrichments_api_results

Data fetched here:
https://gitlab.com/arbetsformedlingen/joblinks/sample-processing-data/-/blob/master/c871f4c/add_jobad_enrichments_api_results.out.gz
