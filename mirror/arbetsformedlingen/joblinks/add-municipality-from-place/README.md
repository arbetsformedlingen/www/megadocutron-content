---
title: add-municipality-from-place
gitlaburl: https://gitlab.com/arbetsformedlingen/joblinks/add-municipality-from-place/-/blob/main//README.md
gitdir: /arbetsformedlingen/joblinks/add-municipality-from-place
gitdir-file-path: /README.md
date: '2023-10-19 09:15:16'
path: /arbetsformedlingen/joblinks/add-municipality-from-place/README.md
tags:
- README.md
---
# add-municipality-from-place
A part (i.e. Processor) of the Joblinks Pipeline.

## Description
This processor enrich job ads with taxonomy values for previously found enriched workplaces.
The previously found workplaces are mapped to taxonomy municipality, 
with the help of the file resources/municipalities_geo_to_municipality.csv, so the following data can be added for the ad: 
```
"workplace_addresses" : [
    {
        "municipality_concept_id": "AvNB_uwa_6n6", 
        "municipality": "Stockholm", 
        "region_concept_id": "CifL_Rzy_Mku", 
        "region": "Stockholms län", 
        "country_concept_id": "i46j_HmG_v64", 
        "country": "Sverige"
    }
]
```

This is done from a mapping file.

## Prerequisites
Docker and a bash shell

## Needed input data for running  
The step needs a list of ads where each ad have the attributes/structure: 
```   
{  
    "text_enrichments_results": {  
        "enrichedbinary_result": {  
            "geos" : [  
                 {"concept_label": "Stockholm", "term": "stockholm", "term_misspelled": False},  
                 {"concept_label": "Solna", "term": "solna", "term_misspelled": False}  
            ]  
        }    
    }    
}  
```

## Building Docker image  
./run.sh --build  
  
## Running Docker with ads on stdin  
cat /tmp/ads_input  |  ./run.sh    >/tmp/ads_add_municipality_from_place_output      
  
## Clean Docker image  
./run.sh --clean  
  
## Mac, remove previous docker-builds + cointainers  
docker system prune  
  
## Start shell in Docker image  
docker run -it joblinks/add-municipality-from-place sh  
  
## Install for development   
pip install -r requirements.txt  
  
## Running with python and file as input (during development)  
If using ads file instead of stdin; put a pipeline file with ads in resources dir, set local env variable
export LOCAL_FILEPATH=resources/input_filename
..and run with:    
python main.py  

