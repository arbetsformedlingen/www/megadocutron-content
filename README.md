# About this repository
- mirror is generated from
  https://gitlab.com/arbetsformedlingen/www/megadocutron, so changes
  in this repo where this readme lives, will be removed on the next
  push.
- themes/megadocs is a git submodule
- there is a .gitlab-ci.yml file here that rebuilds the gitlab pages
- see deploy/pages for the link to the generated site
